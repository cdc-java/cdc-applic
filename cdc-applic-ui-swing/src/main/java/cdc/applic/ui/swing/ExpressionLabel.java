package cdc.applic.ui.swing;

import java.awt.Color;

import javax.swing.JLabel;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.LexicalException;
import cdc.applic.expressions.SyntacticException;

/**
 * Specialization of JLabel used to display an expression.
 *
 * @author Damien Carbonne
 */
public class ExpressionLabel extends JLabel {
    private static final long serialVersionUID = 1L;

    /** Optional expression. */
    private Expression expression = null;
    /** Optional formatting. */
    private Formatting formatting = null;

    // TODO syntax highlighting
    // TODO error / warning display
    // TODO writing rules checks
    // TODO dictionary attachment
    // TODO ConstrainedExpression
    public ExpressionLabel(Expression expression,
                           Formatting formatting) {
        this.expression = expression;
        this.formatting = formatting;
        format();
    }

    public ExpressionLabel(Expression expression) {
        this(expression, null);
    }

    public ExpressionLabel(Formatting formatting) {
        this(null, formatting);
    }

    public ExpressionLabel() {
        this(null, null);
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
        format();
    }

    public Formatting getFormatting() {
        return formatting;
    }

    public void setFormatting(Formatting formatting) {
        this.formatting = formatting;
        format();
    }

    private void format() {
        if (expression == null) {
            setText("");
        } else {
            if (formatting == null) {
                setForeground(Color.BLACK);
                setText(expression.getContent());
            } else {
                try {
                    setForeground(Color.BLACK);
                    setText(expression.toInfix(formatting));
                } catch (final LexicalException | SyntacticException e) {
                    setForeground(Color.RED);
                    setText(expression.getContent());
                }
            }
        }
    }
}