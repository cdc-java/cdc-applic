package cdc.applic.dictionaries.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;

class S1000DIdUtilsTest {
    @Test
    void testToIdString() {
        assertSame(null, S1000DIdUtils.toS1000DId((String) null));
        assertEquals("", S1000DIdUtils.toS1000DId(""));
        assertEquals("A", S1000DIdUtils.toS1000DId("A"));
        assertEquals("a", S1000DIdUtils.toS1000DId("a"));
        assertEquals("Z", S1000DIdUtils.toS1000DId("Z"));
        assertEquals("z", S1000DIdUtils.toS1000DId("z"));
        assertEquals("0", S1000DIdUtils.toS1000DId("0"));
        assertEquals("9", S1000DIdUtils.toS1000DId("9"));
        assertEquals("AZaz09", S1000DIdUtils.toS1000DId("AZaz09"));
        assertEquals("-", S1000DIdUtils.toS1000DId("-"));
        assertEquals("_", S1000DIdUtils.toS1000DId("+"));
        assertEquals("A_Z", S1000DIdUtils.toS1000DId("A+Z"));
    }

    @Test
    void testToIdSName() {
        assertSame(null, S1000DIdUtils.toS1000DId((SName) null));
        assertEquals("A_Z", S1000DIdUtils.toS1000DId(SName.of("A+Z")));
    }

    @Test
    void testToIdName() {
        assertSame(null, S1000DIdUtils.toS1000DId((Name) null));
        assertEquals("A-Z", S1000DIdUtils.toS1000DId(Name.of("A.Z")));
    }

}