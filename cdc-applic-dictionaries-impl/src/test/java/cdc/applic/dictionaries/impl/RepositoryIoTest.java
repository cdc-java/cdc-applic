package cdc.applic.dictionaries.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringWriter;
import java.util.List;
import java.util.Locale;
import java.util.function.BiConsumer;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;
import org.xmlunit.builder.Input;
import org.xmlunit.matchers.CompareMatcher;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.DictionaryDeclaredItems;
import cdc.applic.dictionaries.Registry;
import cdc.applic.dictionaries.impl.io.RepositoryIo;
import cdc.applic.dictionaries.impl.io.RepositoryIoFeatures;
import cdc.applic.dictionaries.impl.io.RepositoryXml;
import cdc.applic.dictionaries.impl.io.RepositoryXml.StAXLoader;
import cdc.applic.dictionaries.items.Assertion;
import cdc.applic.dictionaries.items.ConstraintAssertion;
import cdc.applic.dictionaries.items.ContextAssertion;
import cdc.applic.dictionaries.items.DerivedContextAssertion;
import cdc.applic.dictionaries.items.DerivedStandardAssertion;
import cdc.applic.dictionaries.items.UserDefinedAssertion;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.literals.Name;
import cdc.graphs.PartialOrderPosition;
import cdc.io.xml.XmlWriter;
import cdc.util.lang.FailureReaction;

class RepositoryIoTest {
    private static final Logger LOGGER = LogManager.getLogger(RepositoryIoTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    static <A extends Assertion> void assertAssertions(Dictionary dictionary,
                                                       Class<A> cls,
                                                       int expected,
                                                       String message) {
        if (expected >= 0) {
            assertEquals(expected, IterableUtils.size(dictionary.getAssertions(cls)), message);
        }
    }

    static void assertAssertions(Dictionary dictionary,
                                 int expectedContext,
                                 int expectedUserDefined,
                                 int expectedGenerated,
                                 int expectedDerivedContext,
                                 int expectedDerivedStandard,
                                 String context) {
        assertAssertions(dictionary, ContextAssertion.class, expectedContext, context + " context");
        assertAssertions(dictionary, UserDefinedAssertion.class, expectedUserDefined, context + " user defined");
        assertAssertions(dictionary, ConstraintAssertion.class, expectedGenerated, context + " generated");
        assertAssertions(dictionary, DerivedContextAssertion.class, expectedDerivedContext, context + " derived context");
        assertAssertions(dictionary, DerivedStandardAssertion.class, expectedDerivedStandard, context + " derived standard");
    }

    static void assertTypes(Registry registry,
                            int expectedDeclared,
                            int expectedAll,
                            String context) {
        assertEquals(expectedDeclared,
                     registry.getDeclaredTypes().size(),
                     context + " number of declared types in " + registry.getName());
        assertEquals(expectedAll, registry.getAllTypes().size(), context + " number of types in " + registry.getName());
    }

    static void assertItems(Dictionary dictionary,
                            int expectedDeclaredProperties,
                            int expectedDeclaredAliases,
                            int expectedAllProperties,
                            int expectedAllAliases,
                            String context) {
        if (dictionary instanceof DictionaryDeclaredItems) {
            assertEquals(expectedDeclaredProperties,
                         IterableUtils.size(((DictionaryDeclaredItems) dictionary).getDeclaredProperties()),
                         context + " number of declared properties in " + dictionary.getName());
            assertEquals(expectedDeclaredAliases,
                         IterableUtils.size(((DictionaryDeclaredItems) dictionary).getDeclaredAliases()),
                         context + " number of declared aliases in " + dictionary.getName());
            assertEquals(expectedDeclaredProperties + expectedDeclaredAliases,
                         IterableUtils.size(((DictionaryDeclaredItems) dictionary).getDeclaredItems()),
                         context + " number of declared items in " + dictionary.getName());
        }
        assertEquals(expectedAllProperties,
                     IterableUtils.size(dictionary.getAllProperties()),
                     context + " number of properties in " + dictionary.getName());
        assertEquals(expectedAllAliases,
                     IterableUtils.size(dictionary.getAllAliases()),
                     context + " number of Aliases in " + dictionary.getName());
        assertEquals(expectedAllProperties + expectedAllAliases,
                     IterableUtils.size(dictionary.getAllItems()),
                     context + " number of Items in " + dictionary.getName());
    }

    static void checkSaveLoadSave(RepositoryImpl repository,
                                  String basename) throws IOException {
        checkSaveLoadSaveXml(repository, basename);
        checkSaveLoadSaveOffice(repository, basename);
    }

    static void checkSaveLoadSave(RepositoryImpl repository,
                                  String basename,
                                  BiConsumer<RepositoryImpl, String> checker) throws IOException {
        repository.print(OUT);
        checker.accept(repository, "(Original Repository)");
        final RepositoryImpl repository1 = checkSaveLoadSaveXml(repository, basename);
        checker.accept(repository1, "(XML Repository)");
        final RepositoryImpl repository2 = checkSaveLoadSaveOffice(repository, basename);
        checker.accept(repository2, "(Office Repository)");
    }

    static RepositoryImpl checkSaveLoadSaveXml(RepositoryImpl repository,
                                               String basename) throws IOException {
        final File file1 = new File(basename + "-1.xml");
        final File file2 = new File(basename + "-2.xml");
        LOGGER.debug("Generate {}", file1);
        RepositoryIo.save(repository, file1, RepositoryIoFeatures.BEST_DEBUG);

        LOGGER.debug("Load {}", file1);
        final RepositoryImpl repository2 = RepositoryIo.load(file1, FailureReaction.FAIL);

        LOGGER.debug("Generate {}", file2);
        RepositoryIo.save(repository2, file2, RepositoryIoFeatures.BEST_DEBUG);

        LOGGER.debug("=================================");
        LOGGER.debug("Original repository {}", file1);
        LOGGER.debug("=================================");
        repository.print(OUT);
        LOGGER.debug("=================================");
        LOGGER.debug("Restored repository {}", file2);
        LOGGER.debug("=================================");
        repository2.print(OUT);

        LOGGER.debug("Compare {} and {}", file1, file2);
        assertThat(Input.fromFile(file1), CompareMatcher.isSimilarTo(Input.fromFile(file2)));

        return repository2;
    }

    static RepositoryImpl checkSaveLoadSaveOffice(RepositoryImpl repository,
                                                  String basename) throws IOException {
        final File csv1 = new File(basename + "-1.csv");
        final File xlsx1 = new File(basename + "-1.xlsx");
        final File csv2 = new File(basename + "-2.csv");
        final File xlsx2 = new File(basename + "-2.xlsx");

        LOGGER.debug("Generate {}", csv1);
        RepositoryIo.save(repository, csv1, RepositoryIoFeatures.BEST_DEBUG);

        LOGGER.debug("Generate {}", xlsx1);
        RepositoryIo.save(repository, xlsx1, RepositoryIoFeatures.BEST_DEBUG);

        LOGGER.debug("Load {}", xlsx1);
        final RepositoryImpl repository2 = RepositoryIo.load(xlsx1, FailureReaction.FAIL);

        LOGGER.debug("Generate {}", csv2);
        RepositoryIo.save(repository2, csv2, RepositoryIoFeatures.BEST_DEBUG);

        LOGGER.debug("Generate {}", xlsx2);
        RepositoryIo.save(repository2, xlsx2, RepositoryIoFeatures.BEST_DEBUG);

        LOGGER.debug("=================================");
        LOGGER.debug("Original repository {}", xlsx1);
        LOGGER.debug("=================================");
        repository.print(OUT);
        LOGGER.debug("=================================");
        LOGGER.debug("Restored repository {}", xlsx2);
        LOGGER.debug("=================================");
        repository2.print(OUT);

        LOGGER.debug("Compare {} and {}", csv1, csv2);
        assertEquals(FileUtils.readFileToString(csv1, "utf-8"),
                     FileUtils.readFileToString(csv2, "utf-8"),
                     "Files " + csv1 + " and " + csv2 + " differ");
        return repository2;
    }

    @Test
    void testFull() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        repository.getDescription().set(DescriptionImpl.builder()
                                                       .description(Locale.ENGLISH, "Test repository.")
                                                       .build());
        final RegistryImpl global =
                repository.registry()
                          .name("Global")
                          .description(Locale.ENGLISH, "Global registry. No prefix. All declared items are OPTIONAL.")
                          .description(Locale.FRENCH, "Registre global. Pas de préfixe.")
                          // German is not saved with current Office format
                          .description(Locale.GERMAN, "Globales Register. Kein Präfix.")
                          .build();
        final RegistryImpl p1 =
                repository.registry()
                          .name("Program 1")
                          .prefix("P1")
                          .description(Locale.ENGLISH,
                                       "Program 1 registry. P1 prefix.\nAll inherited items are OPTIONAL by default.\nPROD2 is explicitly FORBIDDEN.")
                          .description(Locale.FRENCH, "Registre du programme 1. Utilise P1 comme préfixe.")
                          .parents(global)
                          .build();
        final RegistryImpl p2 =
                repository.registry()
                          .name("Program 2")
                          .prefix("P2")
                          .description(Locale.ENGLISH,
                                       "Program 2 registry.\nAll inherited items are OPTIONAL by default.\nPROD1 is explicitly FORBIDDEN.")
                          .description(Locale.FRENCH, "Registre du programme 2.")
                          .parents(global)
                          .build();
        final RegistryImpl cross =
                repository.registry()
                          .name("Cross")
                          .description(Locale.ENGLISH, "Multi program registry. No prefix.")
                          .description(Locale.FRENCH, "Registre multi-programme.")
                          .parents(p1, p2)
                          .build();

        final PolicyImpl cross1 =
                cross.policy()
                     .name("Policy1")
                     .description("en", "Policy1 is a restriction of the Cross registry.")
                     .build();

        // final RegistryImpl mixed =
        // repository.registry()
        // .name("Mixed")
        // .description(Locale.ENGLISH, "Mixed registry, inheriting from a Policy.")
        // .parents(cross1)
        // .build();

        global.namingConvention()
              .name("C1")
              .description("en", "First naming convention.")
              .build();

        global.booleanType()
              .name("BooleanType")
              .defaultValue(false)
              .build();

        global.integerType()
              .name("IntegerType")
              .domain("1~100")
              .defaultValue(1)
              .build();

        global.realType()
              .name("RealType")
              .domain("1.0~100.0")
              .defaultValue(1.0)
              .build();

        global.patternType()
              .name("PatternType")
              .pattern("[A-Z]+")
              .defaultValue("A")
              .build();

        global.enumeratedType()
              .name("ProductType")
              .synonym("C1", "product_type")
              .description(Locale.ENGLISH, "Enumerated non-frozen type.")
              .frozen(false)
              .value().literal("Prod1").synonym("C1", "prod_1").ordinal(0).description("en", "First product.").back()
              .value().literal("Prod2").synonym("C1", "prod_2").ordinal(1).back()
              .value().literal("Prod3").synonym("C1", "prod_3").ordinal(2).back()
              .lessThan("prod_2", "prod_3")
              .lessThan("Prod1", "prod_2")
              .defaultValue("Prod1")
              .build();

        global.property()
              .name("Product")
              .synonym("C1", "product")
              .description(Locale.ENGLISH, "Enumerated property.")
              .type("ProductType")
              .ordinal(0)
              .build();
        global.alias().name("PROD1").expression("Product=Prod1").build();
        global.alias().name("PROD2").expression("Product=Prod2").build();
        global.setWritingRuleEnabled("WR1", true);
        cross.setWritingRuleEnabled("WR2", true);
        p1.setContextExpression("Product = Prod1");
        p2.setContextExpression("Product = Prod2");
        p1.setItemUsage("PROD2", DItemUsage.FORBIDDEN);
        p2.setItemUsage("PROD1", DItemUsage.FORBIDDEN);

        cross1.setItemUsage("Product", DItemUsage.OPTIONAL);
        cross1.setItemUsage("PROD1", DItemUsage.OPTIONAL);

        // mixed.setItemUsage("Product", DItemUsage.OPTIONAL);
        // mixed.setItemUsage("PROD1", DItemUsage.OPTIONAL);

        checkSaveLoadSave(repository, "target/full");
    }

    @Test
    void testStructure() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();
        r2.policy().name("P1").build();

        checkSaveLoadSave(repository, "target/structure");
    }

    @Test
    void testEarlyTypes() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final Type t = r1.booleanType().name("B").build();
        repository.registry().name("r2").prefix("R2").parents(r1).build();

        checkSaveLoadSave(repository,
                          "target/early-types",
                          (r,
                           s) -> {
                              assertEquals(t, r.getRegistry("/r1").getType("B"), s);
                              assertEquals(t, r.getRegistry("/r1").getType("R1.B"), s);
                              assertEquals(t, r.getRegistry("/r2").getType("R1.B"), s);
                              assertTypes(r.getRegistry("/r1"), 1, 1, s);
                              assertTypes(r.getRegistry("/r2"), 0, 1, s);
                          });
    }

    @Test
    void testLateTypes() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        repository.registry().name("r2").prefix("R2").parents(r1).build();
        r1.booleanType().name("B").build();

        checkSaveLoadSave(repository,
                          "target/late-types",
                          (r,
                           s) -> {
                              assertTypes(r.getRegistry("/r1"), 1, 1, s);
                              assertTypes(r.getRegistry("/r2"), 0, 1, s);
                          });
    }

    @Test
    void testEarlyItems() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        r1.booleanType().name("B").build();
        r1.alias().name("A").expression("true").build();
        r1.property().name("P").type("B").ordinal(0).build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();
        r2.policy().name("p3").build();

        checkSaveLoadSave(repository,
                          "target/early-items",
                          (r,
                           s) -> {
                              assertItems(r.getDictionary("/r1"), 1, 1, 1, 1, s);
                              assertItems(r.getDictionary("/r2"), 0, 0, 1, 1, s);
                              assertItems(r.getDictionary("/r2/p3"), 0, 0, 1, 1, s);
                          });
    }

    @Test
    void testLateItems() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        r1.booleanType().name("B").build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();
        r2.policy().name("p3").build();
        r1.alias().name("A").expression("true").build();
        r1.property().name("P").type("B").ordinal(0).build();

        checkSaveLoadSave(repository,
                          "target/late-items",
                          (r,
                           s) -> {
                              assertItems(r.getDictionary("/r1"), 1, 1, 1, 1, s);
                              assertItems(r.getDictionary("/r2"), 0, 0, 1, 1, s);
                              assertItems(r.getDictionary("/r2/p3"), 0, 0, 1, 1, s);
                          });
    }

    @Test
    void testEarlyContextRegistry() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();
        r2.setContextExpression("not false");
        repository.registry().name("r3").prefix("R3").parents(r2).build();

        checkSaveLoadSave(repository,
                          "target/early-context-registry",
                          (r,
                           s) -> {
                              assertAssertions(r.getDictionary("/r1"), 0, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r2"), 1, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r3"), 0, 0, 0, 1, 0, s);
                          });
    }

    @Test
    void testLateContextRegistry() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();
        repository.registry().name("r3").prefix("R3").parents(r2).build();
        r2.setContextExpression("not false");

        checkSaveLoadSave(repository,
                          "target/late-context-registry",
                          (r,
                           s) -> {
                              assertAssertions(r.getDictionary("/r1"), 0, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r2"), 1, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r3"), 0, 0, 0, 1, 0, s);
                          });
    }

    @Test
    void testEarlyContextPolicy() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final PolicyImpl p2 = r1.policy().name("p2").build();
        p2.setContextExpression("not false");
        p2.policy().name("p3").build();

        checkSaveLoadSave(repository,
                          "target/early-context-policy",
                          (r,
                           s) -> {
                              assertAssertions(r.getDictionary("/r1"), 0, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r1/p2"), 1, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r1/p2/p3"), 0, 0, 0, 1, 0, s);
                          });
    }

    @Test
    void testLateContextPolicy() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final PolicyImpl p2 = r1.policy().name("p2").build();
        p2.policy().name("p3").build();
        p2.setContextExpression("not false");

        checkSaveLoadSave(repository,
                          "target/late-context-policy",
                          (r,
                           s) -> {
                              assertAssertions(r.getDictionary("/r1"), 0, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r1/p2"), 1, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r1/p2/p3"), 0, 0, 0, 1, 0, s);
                          });
    }

    @Test
    void testEarlyAssertionRegistry() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();
        r2.createAssertion("not false");
        repository.registry().name("r3").prefix("R3").parents(r2).build();

        checkSaveLoadSave(repository,
                          "target/early-assertion-registry",
                          (r,
                           s) -> {
                              assertAssertions(r.getDictionary("/r1"), 0, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r2"), 0, 1, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r3"), 0, 0, 0, 0, 1, s);
                          });
    }

    @Test
    void testLateAssertionRegistry() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();
        repository.registry().name("r3").prefix("R3").parents(r2).build();
        r2.createAssertion("not false");

        checkSaveLoadSave(repository,
                          "target/late-assertion-registry",
                          (r,
                           s) -> {
                              assertAssertions(r.getDictionary("/r1"), 0, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r2"), 0, 1, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r3"), 0, 0, 0, 0, 1, s);
                          });

    }

    @Test
    void testEarlyAssertionPolicy() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final PolicyImpl p2 = r1.policy().name("p2").build();
        p2.createAssertion("not false");
        p2.policy().name("p3").build();

        checkSaveLoadSave(repository,
                          "target/early-assertion-policy",
                          (r,
                           s) -> {
                              assertAssertions(r.getDictionary("/r1"), 0, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r1/p2"), 0, 1, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r1/p2/p3"), 0, 0, 0, 0, 1, s);
                          });
    }

    @Test
    void testLateAssertionPolicy() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final PolicyImpl p2 = r1.policy().name("p2").build();
        p2.policy().name("p3").build();
        p2.createAssertion("not false");

        checkSaveLoadSave(repository,
                          "target/late-assertion-policy",
                          (r,
                           s) -> {
                              assertAssertions(r.getDictionary("/r1"), 0, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r1/p2"), 0, 1, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r1/p2/p3"), 0, 0, 0, 0, 1, s);
                          });
    }

    @Test
    void testEarlyContextAssertionRegistry() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();
        r2.setContextExpression("not false");
        r2.createAssertion("not false");
        repository.registry().name("r3").prefix("R3").parents(r2).build();

        checkSaveLoadSave(repository,
                          "target/early-context-assertion-registry",
                          (r,
                           s) -> {
                              assertAssertions(r.getDictionary("/r1"), 0, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r2"), 1, 1, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r3"), 0, 0, 0, 1, 1, s);
                          });
    }

    @Test
    void testLateContextAssertionRegistry() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();
        repository.registry().name("r3").prefix("R3").parents(r2).build();
        r2.setContextExpression("not false");
        r2.createAssertion("not false");

        checkSaveLoadSave(repository,
                          "target/late-context-assertion-registry",
                          (r,
                           s) -> {
                              assertAssertions(r.getDictionary("/r1"), 0, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r2"), 1, 1, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r3"), 0, 0, 0, 1, 1, s);
                          });
    }

    @Test
    void testEarlyContextAssertionPolicy() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final PolicyImpl p2 = r1.policy().name("p2").build();
        p2.setContextExpression("not false");
        p2.createAssertion("not false");
        p2.policy().name("p3").build();

        checkSaveLoadSave(repository,
                          "target/early-context-assertion-policy",
                          (r,
                           s) -> {
                              assertAssertions(r.getDictionary("/r1"), 0, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r1/p2"), 1, 1, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r1/p2/p3"), 0, 0, 0, 1, 1, s);
                          });
    }

    @Test
    void testLateContextAssertionPolicy() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final PolicyImpl p2 = r1.policy().name("p2").build();
        p2.policy().name("p3").build();
        p2.setContextExpression("not false");
        p2.createAssertion("not false");

        checkSaveLoadSave(repository,
                          "target/late-context-assertion-policy",
                          (r,
                           s) -> {
                              assertAssertions(r.getDictionary("/r1"), 0, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r1/p2"), 1, 1, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r1/p2/p3"), 0, 0, 0, 1, 1, s);
                          });
    }

    @Test
    void testEarlyAssertionLateContextRegistry() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();
        r2.createAssertion("not false");
        repository.registry().name("r3").prefix("R3").parents(r2).build();
        r2.setContextExpression("not false");

        checkSaveLoadSave(repository,
                          "target/early-assertion-late-context-registry",
                          (r,
                           s) -> {
                              assertAssertions(r.getDictionary("/r1"), 0, 0, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r2"), 1, 1, 0, 0, 0, s);
                              assertAssertions(r.getDictionary("/r3"), 0, 0, 0, 1, 1, s);
                          });
    }

    @Test
    void testBasicPolicy() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        r1.alias().name("A").expression("not false").build();
        r1.booleanType().name("Bool").build();
        r1.integerType().name("Int").frozen(false).domain("1~100").build();
        r1.property().name("P").type("Bool").ordinal(0).build();
        r1.createAssertion("P");
        final PolicyImpl p1 = r1.policy().name("p1").build();
        p1.setContextExpression("not false");
        p1.policy().name("p1.1").build();
        p1.setContextExpression("not false");

        checkSaveLoadSave(repository,
                          "target/basic-policy",
                          (r,
                           s) -> {
                              assertTrue(r.getPolicy("r1/p1").getWritingRuleNames().isEmpty(), s + " writing rules");
                          });
    }

    @Test
    void testEnumOrderBug139() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        r1.enumeratedType().name("E").frozen(false).literals("A", "B", "C").lessThan("A", "B").build();

        checkSaveLoadSave(repository,
                          "target/enum-order-bug139",
                          (r,
                           s) -> {
                              final EnumeratedTypeImpl t = r.getRegistry("/r1").getType(Name.of("E"), EnumeratedTypeImpl.class);
                              final PartialOrderPosition p = t.partialCompare(StringValue.of("A"), StringValue.of("B"));
                              assertEquals(PartialOrderPosition.LESS_THAN, p, s);
                          });
    }

    @Test
    void testUsage1() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        r1.alias().name("A").expression("true").build();
        final RegistryImpl r11 = repository.registry().name("r11").parents(r1).build();
        final RegistryImpl r111 = repository.registry().name("r111").parents(r11).build();
        repository.registry().name("r1111").parents(r111).build();
        r11.setItemUsage("A", DItemUsage.MANDATORY);
        r111.setItemUsage("A", DItemUsage.FORBIDDEN);

        checkSaveLoadSave(repository,
                          "target/usage-1",
                          (r,
                           s) -> {
                              assertSame(DItemUsage.OPTIONAL, r.getDictionary("/r1").getEffectiveItemUsage("A"), s + " /r1");
                              assertSame(DItemUsage.MANDATORY, r.getDictionary("/r11").getEffectiveItemUsage("A"), s + " /r11");
                              assertSame(DItemUsage.FORBIDDEN, r.getDictionary("/r111").getEffectiveItemUsage("A"), s + " /r111");
                              assertSame(DItemUsage.FORBIDDEN, r.getDictionary("/r1111").getEffectiveItemUsage("A"), s + " /r1111");
                          });
    }

    @Test
    void testUsage2() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        r1.alias().name("A").expression("true").build();
        final RegistryImpl r11 = repository.registry().name("r11").parents(r1).build();
        final RegistryImpl r111 = repository.registry().name("r111").parents(r11).build();
        repository.registry().name("r1111").parents(r111).build();
        r11.setItemUsage("A", DItemUsage.MANDATORY);
        r111.setItemUsage("A", DItemUsage.RECOMMENDED);

        checkSaveLoadSave(repository,
                          "target/usage-2",
                          (r,
                           s) -> {
                              assertSame(DItemUsage.OPTIONAL, r.getDictionary("/r1").getEffectiveItemUsage("A"), s + " /r1");
                              assertSame(DItemUsage.MANDATORY, r.getDictionary("/r11").getEffectiveItemUsage("A"), s + " /r11");
                              assertSame(DItemUsage.RECOMMENDED, r.getDictionary("/r111").getEffectiveItemUsage("A"), s + " /r111");
                              assertSame(DItemUsage.OPTIONAL, r.getDictionary("/r1111").getEffectiveItemUsage("A"), s + " /r1111");
                          });
    }

    @Test
    void testUsage3() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        r1.alias().name("A").expression("true").build();
        final PolicyImpl p11 = r1.policy().name("p11").build();
        final PolicyImpl p111 = p11.policy().name("p111").build();
        p111.policy().name("p1111").build();
        p11.setItemUsage("A", DItemUsage.MANDATORY);
        p111.setItemUsage("A", DItemUsage.FORBIDDEN);

        checkSaveLoadSave(repository,
                          "target/usage-3",
                          (r,
                           s) -> {
                              assertSame(DItemUsage.OPTIONAL, r.getDictionary("/r1").getEffectiveItemUsage("A"), s + " /r1");
                              assertSame(DItemUsage.MANDATORY,
                                         r.getDictionary("/r1/p11").getEffectiveItemUsage("A"),
                                         s + " /r1/p11");
                              assertSame(DItemUsage.FORBIDDEN,
                                         r.getDictionary("/r1/p11/p111").getEffectiveItemUsage("A"),
                                         s + " /r1/p11/p111");
                              assertSame(DItemUsage.FORBIDDEN,
                                         r.getDictionary("/r1/p11/p111/p1111").getEffectiveItemUsage("A"),
                                         s + " /r1/p11/p111/p1111");
                          });
    }

    @Test
    void testUsage4() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        r1.booleanType().name("Bool1").build();
        r1.property().name("B1").type("Bool1").build();
        r1.property().name("B2").type("Bool1").build();
        r1.property().name("B3").type("Bool1").build();
        r1.property().name("B4").type("Bool1").build();
        r1.setTypeUsage("Bool1", DItemUsage.RECOMMENDED);
        r1.setItemUsage("B2", DItemUsage.OPTIONAL);
        r1.setItemUsage("B3", DItemUsage.FORBIDDEN);
        final PolicyImpl p11 = r1.policy().name("p11").build();
        p11.setTypeUsage("Bool1", DItemUsage.OPTIONAL);
        final PolicyImpl p111 = p11.policy().name("p111").build();
        p111.policy().name("p111").build();
        p111.setTypeUsage("Bool1", DItemUsage.FORBIDDEN);

        checkSaveLoadSave(repository,
                          "target/usage-4",
                          (r,
                           s) -> {
                              assertSame(DItemUsage.RECOMMENDED, r.getRegistry("r1").getTypeUsage("Bool1"));
                              assertSame(DItemUsage.OPTIONAL, r.getDictionary("/r1/p11").getTypeUsage("Bool1"));
                              assertSame(DItemUsage.FORBIDDEN, r.getDictionary("/r1/p11/p111").getTypeUsage("Bool1"));
                              assertSame(DItemUsage.RECOMMENDED, r.getDictionary("/r1").getEffectiveItemUsage("B1"));
                              assertSame(DItemUsage.OPTIONAL, r.getDictionary("/r1").getEffectiveItemUsage("B2"));
                              assertSame(DItemUsage.FORBIDDEN, r.getDictionary("/r1").getEffectiveItemUsage("B3"));
                              assertSame(DItemUsage.RECOMMENDED, r.getDictionary("/r1").getEffectiveItemUsage("B4"));
                          });
    }

    @Test
    void testNamingConventions1() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        final NamingConventionImpl nc1 = r1.namingConvention().name("Convention1").build();
        nc1.getDescription().description(Locale.FRENCH, "Description de la convention 1.");
        nc1.getDescription().description(Locale.ENGLISH, "Description of convention 1.");

        checkSaveLoadSave(repository,
                          "target/conventions1",
                          (r,
                           s) -> {
                              assertEquals(nc1, r.getRegistry("/r1").getNamingConvention("Convention1"), s);
                          });
    }

    @Test
    void testTypeSynonyms() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        final NamingConventionImpl nc1 = r1.namingConvention().name("Convention1").build();
        final NamingConventionImpl nc2 = r1.namingConvention().name("Convention2").build();
        final BooleanTypeImpl type1 =
                r1.booleanType()
                  .name("B")
                  .synonym("Convention1", "BBis")
                  .synonym("Convention2", "BBis")
                  .description(Locale.ENGLISH, "Comment")
                  .build();
        r1.integerType()
          .name("I")
          .synonym("Convention1", "IBis")
          .synonym("Convention2", "IBis")
          .domain("1~999")
          .description(Locale.ENGLISH, "Comment")
          .build();
        r1.realType()
          .name("R")
          .synonym("Convention1", "RBis")
          .synonym("Convention2", "RBis")
          .domain("1.0~999.0")
          .description(Locale.ENGLISH, "Comment")
          .build();
        r1.patternType()
          .name("P")
          .synonym("Convention1", "PBis")
          .synonym("Convention2", "PBis")
          .pattern(".*")
          .description(Locale.ENGLISH, "Comment")
          .build();
        r1.enumeratedType()
          .name("E")
          .synonym("Convention1", "EBis")
          .synonym("Convention2", "EBis")
          .literals("A", "B")
          .description(Locale.ENGLISH, "Comment")
          .build();

        checkSaveLoadSave(repository,
                          "target/type-synonyms",
                          (r,
                           s) -> {
                              assertEquals(nc1, r.getRegistry("/r1").getNamingConvention("Convention1"), s);
                              assertEquals(nc2, r.getRegistry("/r1").getNamingConvention("Convention2"), s);
                              assertEquals(type1, r.getRegistry("/r1").getType("B"), s);
                              // assertEquals(t1, r.getRegistry("/r1").getType("BBis"), s); Does not yet work
                              assertEquals(1, r.getRegistry("/r1").getType("B").getNames().getSynonyms().size(), s);
                              assertEquals(3, r.getRegistry("/r1").getType("B").getNames().getNamingConventions().size(), s);
                              assertEquals(1, r.getRegistry("/r1").getType("I").getNames().getSynonyms().size(), s);
                              assertEquals(3, r.getRegistry("/r1").getType("I").getNames().getNamingConventions().size(), s);
                              assertEquals(1, r.getRegistry("/r1").getType("R").getNames().getSynonyms().size(), s);
                              assertEquals(3, r.getRegistry("/r1").getType("R").getNames().getNamingConventions().size(), s);
                              assertEquals(1, r.getRegistry("/r1").getType("P").getNames().getSynonyms().size(), s);
                              assertEquals(3, r.getRegistry("/r1").getType("P").getNames().getNamingConventions().size(), s);
                              assertEquals(1, r.getRegistry("/r1").getType("E").getNames().getSynonyms().size(), s);
                              assertEquals(3, r.getRegistry("/r1").getType("E").getNames().getNamingConventions().size(), s);
                          });
    }

    @Test
    void testItemSynonyms() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        final NamingConventionImpl nc1 = r1.namingConvention().name("Convention1").build();
        final NamingConventionImpl nc2 = r1.namingConvention().name("Convention2").build();
        final BooleanTypeImpl type1 = r1.booleanType().name("B").description(Locale.ENGLISH, "Comment").build();
        r1.alias()
          .name("A")
          .synonym("Convention1", "ABis")
          .synonym("Convention2", "ABis")
          .expression("true")
          .description(Locale.ENGLISH, "Comment")
          .build();
        r1.property()
          .name("P")
          .synonym("Convention1", "PBis")
          .synonym("Convention2", "PBis")
          .type("B")
          .description(Locale.ENGLISH, "Comment")
          .build();

        checkSaveLoadSave(repository,
                          "target/item-synonyms",
                          (r,
                           s) -> {
                              assertEquals(nc1, r.getRegistry("/r1").getNamingConvention("Convention1"), s);
                              assertEquals(nc2, r.getRegistry("/r1").getNamingConvention("Convention2"), s);
                              assertEquals(type1, r.getRegistry("/r1").getType("B"), s);
                              assertEquals(1, r.getRegistry("/r1").getAlias("A").getNames().getSynonyms().size(), s);
                              assertEquals(3, r.getRegistry("/r1").getAlias("A").getNames().getNamingConventions().size(), s);
                              assertEquals(1, r.getRegistry("/r1").getProperty("P").getNames().getSynonyms().size(), s);
                              assertEquals(3, r.getRegistry("/r1").getProperty("P").getNames().getNamingConventions().size(), s);
                          });
    }

    @Test
    void testEnumValueSynonyms() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        final NamingConventionImpl nc1 = r1.namingConvention().name("Convention1").build();
        final NamingConventionImpl nc2 = r1.namingConvention().name("Convention2").build();
        final EnumeratedTypeImpl type =
                r1.enumeratedType().name("E").literals("A", "B").description(Locale.ENGLISH, "Comment").build();
        type.getValue("A").addSynonym("Convention1", "ABis");
        type.getValue("A").addSynonym("Convention2", "ABis");

        checkSaveLoadSave(repository,
                          "target/enum-value-synonyms",
                          (r,
                           s) -> {
                              assertEquals(nc1, r.getRegistry("/r1").getNamingConvention("Convention1"), s);
                              assertEquals(nc2, r.getRegistry("/r1").getNamingConvention("Convention2"), s);
                              assertEquals(1,
                                           r.getRegistry("/r1")
                                            .getType("E", EnumeratedTypeImpl.class)
                                            .getValue("A")
                                            .getLiterals()
                                            .getSynonyms()
                                            .size(),
                                           s);
                              assertEquals(3,
                                           r.getRegistry("/r1")
                                            .getType("E", EnumeratedTypeImpl.class)
                                            .getValue("A")
                                            .getLiterals()
                                            .getNamingConventions()
                                            .size(),
                                           s);
                          });
    }

    @Test
    void testBug188() throws IOException {

        final RepositoryImpl repository = new RepositoryImpl();

        final RegistryImpl alpha = repository.registry()
                                             .name("alpha")
                                             .prefix("ALP")
                                             .build();
        alpha.namingConvention()
             .name("FR")
             .build();

        alpha.patternType()
             .name("Part")
             .pattern(".*")
             .synonym("FR", "Article")
             .build();
        alpha.property()
             .name("Part")
             .type("Part")
             .synonym("FR", "Article")
             .build();
        alpha.alias()
             .name("PART_A")
             .expression("Part = PARTA")
             .synonym("FR", "ARTICLE_A")
             .build();

        final RegistryImpl bravo = repository.registry()
                                             .name("bravo")
                                             .parents(List.of(alpha))
                                             .build();

        bravo.setContextExpression("ALP.ARTICLE_A");
        bravo.namingConvention()
             .name("DE")
             .build();
        bravo.property()
             .name("SubPart")
             .type("ALP.Part")
             .synonym("ALP.FR", "SousArticle")
             .build();

        final StringWriter sw = new StringWriter();
        try (final XmlWriter w = new XmlWriter(sw)) {
            w.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
            RepositoryXml.Printer.write(w, repository);

            LOGGER.debug(sw.toString());

            final RepositoryXml.StAXLoader loader = new StAXLoader(FailureReaction.FAIL);
            loader.load(new ByteArrayInputStream(sw.toString().getBytes()));
        }

        assertTrue(true);
    }
}