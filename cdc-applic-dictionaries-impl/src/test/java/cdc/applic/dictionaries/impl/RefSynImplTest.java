package cdc.applic.dictionaries.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.RefSyn;
import cdc.applic.expressions.literals.Name;

class RefSynImplTest {
    @Test
    void test() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r = repository.registry().name("R").build();
        final NamingConvention nc1 = r.namingConvention().name("Convention1").build();
        final NamingConvention nc2 = r.namingConvention().name("Convention2").build();
        final RefSynImpl<Name> rs1 = new RefSynImpl<>(Name.of("Ref"));
        final RefSynImpl<Name> rs2 = new RefSynImpl<>(Name.of("Ref"));

        assertEquals(rs1, rs1);
        assertEquals(rs1, rs2);
        assertEquals(rs1.hashCode(), rs2.hashCode());
        assertNotEquals(rs1, "Ref");
        assertFalse(rs1.hasSynonyms());
        assertEquals(Name.of("Ref"), rs1.getReferenceValue());
        assertEquals(1, rs1.getNamingConventions().size());
        assertTrue(rs1.getSynonyms().isEmpty());
        assertEquals(Name.of("Ref"), rs1.getValue(nc1));
        assertEquals(Name.of("Ref"), rs1.getValue(nc2));

        assertEquals(RefSyn.DEFAULT_NAMING_CONVENTIONS, rs1.getNamingConventions(Name.of("Ref")));

        rs1.addSynonym(Name.of("S"), nc1);
        assertEquals(2, rs1.getNamingConventions().size());
        assertEquals(Name.of("S"), rs1.getValue(nc1));
        assertEquals(Name.of("Ref"), rs1.getValue(nc2));
        assertEquals(1, rs1.getSynonyms().size());

        rs1.addSynonym(Name.of("S"), nc2);
        assertEquals(3, rs1.getNamingConventions().size());
        assertEquals(1, rs1.getSynonyms().size());
        assertEquals(2, rs1.getNamingConventions(Name.of("S")).size());
        assertTrue(rs1.getNamingConventions(Name.of("X")).isEmpty());

        assertTrue(rs1.hasSynonyms());

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         rs1.addSynonym(Name.of("T"), nc1);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         rs1.addSynonym(Name.of("T"), null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         rs1.addSynonym(null, nc1);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         rs1.addSynonym(Name.of("T"), NamingConvention.DEFAULT);
                     });
    }

    @Test
    void test2() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r = repository.registry().name("R").build();
        final NamingConvention def = NamingConvention.DEFAULT;
        final NamingConvention nc1 = r.namingConvention().name("Convention1").build();
        final NamingConvention nc2 = r.namingConvention().name("Convention2").build();
        final RefSynImpl<Name> rs = new RefSynImpl<>(Name.of("Ref"));
        rs.addSynonym(Name.of("S1A"), nc1);
        rs.addSynonym(Name.of("S1B"), nc2);

        assertEquals(Set.of(def), rs.getNamingConventions(Name.of("Ref")));
        assertEquals(Set.of(nc1), rs.getNamingConventions(Name.of("S1A")));
        assertEquals(Set.of(nc2), rs.getNamingConventions(Name.of("S1B")));
        assertEquals(Set.of(), rs.getNamingConventions(Name.of("Foo")));
    }
}