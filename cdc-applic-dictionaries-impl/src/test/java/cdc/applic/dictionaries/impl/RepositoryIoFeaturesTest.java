package cdc.applic.dictionaries.impl;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.io.RepositoryIoFeatures;

class RepositoryIoFeaturesTest {
    @Test
    void test() {
        assertTrue(RepositoryIoFeatures.BEST.isEnabled(RepositoryIoFeatures.Hint.BEST));
        assertFalse(RepositoryIoFeatures.BEST.isEnabled(RepositoryIoFeatures.Hint.FASTEST));
        assertFalse(RepositoryIoFeatures.BEST.isEnabled(RepositoryIoFeatures.Hint.DEBUG));

        assertTrue(RepositoryIoFeatures.BEST_DEBUG.isEnabled(RepositoryIoFeatures.Hint.BEST));
        assertFalse(RepositoryIoFeatures.BEST_DEBUG.isEnabled(RepositoryIoFeatures.Hint.FASTEST));
        assertTrue(RepositoryIoFeatures.BEST_DEBUG.isEnabled(RepositoryIoFeatures.Hint.DEBUG));

        assertFalse(RepositoryIoFeatures.FASTEST.isEnabled(RepositoryIoFeatures.Hint.BEST));
        assertTrue(RepositoryIoFeatures.FASTEST.isEnabled(RepositoryIoFeatures.Hint.FASTEST));
        assertFalse(RepositoryIoFeatures.FASTEST.isEnabled(RepositoryIoFeatures.Hint.DEBUG));

        assertFalse(RepositoryIoFeatures.FASTEST_DEBUG.isEnabled(RepositoryIoFeatures.Hint.BEST));
        assertTrue(RepositoryIoFeatures.FASTEST_DEBUG.isEnabled(RepositoryIoFeatures.Hint.FASTEST));
        assertTrue(RepositoryIoFeatures.FASTEST_DEBUG.isEnabled(RepositoryIoFeatures.Hint.DEBUG));
    }
}