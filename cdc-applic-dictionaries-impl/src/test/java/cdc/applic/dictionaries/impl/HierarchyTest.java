package cdc.applic.dictionaries.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.Constraint;
import cdc.applic.dictionaries.impl.io.RepositoryIo;
import cdc.applic.dictionaries.impl.io.RepositoryIoFeatures;
import cdc.applic.dictionaries.impl.io.RepositoryXml;
import cdc.applic.dictionaries.items.Assertion;
import cdc.applic.dictionaries.items.LocalAssertion;
import cdc.applic.dictionaries.items.StandardAssertion;
import cdc.io.xml.XmlWriter;
import cdc.util.debug.Verbosity;
import cdc.util.function.IterableUtils;

class HierarchyTest {
    private static final Logger LOGGER = LogManager.getLogger(HierarchyTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    static {
        TestConstraint.elaborate();
    }

    @Test
    void test1() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registryGlobal = repository.registry()
                                                      .name("Global")
                                                      .build();
        registryGlobal.enumeratedType().name("Version").frozen(false).literals("V1", "V2", "V3").build();
        registryGlobal.enumeratedType().name("Program").frozen(false).literals("P1", "P2", "P3", "P4").build();
        registryGlobal.booleanType().name("Change").build();
        registryGlobal.integerType().name("Rank").frozen(false).domain("1~999").build();
        registryGlobal.property().name("Version").type("Version").ordinal(0).build();
        registryGlobal.property().name("Program").type("Program").ordinal(0).build();
        registryGlobal.alias().name("V12").expression("Version in {V1, V2}").build();
        registryGlobal.createAssertion("Program = P1 -> Version = V1");
        assertTrue(registryGlobal.getChildren().isEmpty());
        assertEquals(4, registryGlobal.getDeclaredTypes().size());
        assertEquals(4, registryGlobal.getAllTypes().size());
        assertEquals(3, registryGlobal.getDeclaredItems().size());
        assertEquals(3, registryGlobal.getAllItems().size());
        assertEquals(1, IterableUtils.size(registryGlobal.getAssertions(StandardAssertion.class)));
        assertEquals(1, registryGlobal.getAllAssertions().size());

        final RegistryImpl registryProgram1 = repository.registry()
                                                        .name("Program 1")
                                                        .prefix("P1")
                                                        .parents(registryGlobal)
                                                        .build();
        registryProgram1.setContextExpression("Program = P1");
        registryProgram1.integerType().name("Ch").frozen(true).domain("0,1").build();
        registryProgram1.property().name("M1").type("Ch").ordinal(0).build();
        assertEquals(1, registryGlobal.getChildren().size());
        assertTrue(registryProgram1.getChildren().isEmpty());
        assertEquals(1, registryProgram1.getDeclaredTypes().size());
        assertEquals(5, registryProgram1.getAllTypes().size());
        assertEquals(1, registryProgram1.getDeclaredItems().size());
        assertEquals(4, registryProgram1.getAllItems().size());
        assertEquals(0, IterableUtils.size(registryProgram1.getAssertions(StandardAssertion.class)));
        assertEquals(2, registryProgram1.getAllAssertions().size());

        repository.print(OUT, 0, Verbosity.DETAILED);

        final RegistryImpl registryProgram2 = repository.registry().name("Program 2").prefix("P2").parents(registryGlobal).build();
        registryProgram2.setContextExpression("Program = P2");
        registryProgram2.property().name("M1").type("Change").ordinal(0).build();
        assertEquals(2, registryGlobal.getChildren().size());
        assertTrue(registryProgram2.getChildren().isEmpty());
        assertEquals(0, registryProgram2.getDeclaredTypes().size());
        assertEquals(4, registryProgram2.getAllTypes().size());
        assertEquals(1, registryProgram2.getDeclaredItems().size());
        assertEquals(4, registryProgram2.getAllItems().size());
        assertEquals(0, IterableUtils.size(registryProgram2.getAssertions(StandardAssertion.class)));
        assertEquals(2, registryProgram2.getAllAssertions().size());

        final RegistryImpl registryProgram3 = repository.registry().name("Program 3").prefix("P3").parents(registryGlobal).build();
        registryProgram3.setContextExpression("Program = P3");

        final RegistryImpl registryCrossPrograms = repository.registry()
                                                             .name("Cross Programs")
                                                             .parents(registryProgram1, registryProgram2, registryProgram3)
                                                             .build();
        registryCrossPrograms.property().name("M1").type("Change").ordinal(0).build();
        assertEquals(1, registryProgram1.getChildren().size());
        assertEquals(1, registryProgram2.getChildren().size());
        assertTrue(registryCrossPrograms.getChildren().isEmpty());
        assertEquals(0, registryCrossPrograms.getDeclaredTypes().size());
        assertEquals(5, registryCrossPrograms.getAllTypes().size());
        assertEquals(1, registryCrossPrograms.getDeclaredItems().size());
        assertEquals(6, registryCrossPrograms.getAllItems().size());
        assertEquals(0, IterableUtils.size(registryCrossPrograms.getAssertions(LocalAssertion.class)));
        assertEquals(2, registryCrossPrograms.getAllAssertions().size());

        final PolicyImpl policyCross1 = registryCrossPrograms.policy().name("Policy 1").build();

        repository.print(OUT, 0, Verbosity.DETAILED);

        final Constraint c1 = registryGlobal.addConstraint(TestConstraint.create(registryGlobal, "Hello"));
        policyCross1.addConstraint(TestConstraint.create(policyCross1, "World"));
        repository.print(OUT, 0, Verbosity.DETAILED);

        registryGlobal.removeConstraint(c1);
        repository.print(OUT, 0, Verbosity.DETAILED);
        if (LOGGER.isDebugEnabled()) {
            try (XmlWriter w = new XmlWriter(System.out)) {
                w.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
                RepositoryXml.Printer.write(w, repository);
            }
        }

        RepositoryIo.save(repository, new File("target/hierarchy-test1.xml"));
        RepositoryIo.save(repository, new File("target/hierarchy-test1.xlsx"));
    }

    @Test
    void testBug157() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registryGlobal = repository.registry().name("Global").build();
        final RegistryImpl registryEnv = repository.registry().name("Environment").build();
        registryGlobal.enumeratedType().name("Program").frozen(false).literals("P1", "P2", "P3", "P4").build();
        registryGlobal.property().name("Program").type("Program").ordinal(0).build();

        registryEnv.enumeratedType()
                   .name("Climate")
                   .frozen(true)
                   .literals("Tropical", "Dry", "Temperate", "Con,tinental", "Polar")
                   .build();
        registryEnv.property().name("Climate").type("Climate").ordinal(0).build();

        final RegistryImpl registryCrossPrograms = repository.registry()
                                                             .name("Cross Programs")
                                                             .parents(registryGlobal, registryEnv)
                                                             .build();
        repository.print(OUT, 0, Verbosity.DETAILED);
        RepositoryIo.save(repository,
                          new File("target/hierarchy-test-bug157.xml"),
                          RepositoryIoFeatures.BEST_DEBUG);
        RepositoryIo.save(repository,
                          new File("target/hierarchy-test-bug157.xlsx"),
                          RepositoryIoFeatures.BEST_DEBUG);
        for (final Assertion assertion : registryCrossPrograms.getAssertions(Assertion.class)) {
            LOGGER.debug("assertion: {}", assertion);
            assertFalse(assertion.getExpression().isFalse());
        }
    }
}