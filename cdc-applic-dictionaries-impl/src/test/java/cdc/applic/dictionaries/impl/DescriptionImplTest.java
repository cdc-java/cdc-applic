package cdc.applic.dictionaries.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Locale;

import org.junit.jupiter.api.Test;

class DescriptionImplTest {
    @Test
    void test() {
        final DescriptionImpl d0 = new DescriptionImpl();
        assertTrue(d0.getLocales().isEmpty());
        assertEquals(null, d0.getContent(Locale.ENGLISH));

        d0.description(Locale.ENGLISH, null);

        d0.description(Locale.ENGLISH, "Hello");
        assertEquals("Hello", d0.getContent(Locale.ENGLISH));
        assertEquals(1, d0.getLocales().size());

        d0.description(Locale.ENGLISH, "World");
        assertEquals("World", d0.getContent(Locale.ENGLISH));
        assertEquals(1, d0.getLocales().size());

        d0.description(Locale.FRENCH, "Salut");
        assertEquals(2, d0.getLocales().size());
        d0.description(Locale.ENGLISH, null);
        assertEquals(1, d0.getLocales().size());
        assertEquals(null, d0.getContent(Locale.ENGLISH));
        d0.description(Locale.FRENCH, null);
        assertTrue(d0.getLocales().isEmpty());

        d0.clear();
        d0.description(Locale.FRENCH, "Salut");
        final DescriptionImpl d1 = new DescriptionImpl();
        d1.set(d0);
    }
}