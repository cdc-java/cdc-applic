package cdc.applic.dictionaries.impl;

import java.io.IOException;
import java.util.Locale;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.impl.bindings.DictionariesBindingImpl;
import cdc.applic.dictionaries.s1000d.S1000DProductIdentifier;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.applic.expressions.literals.Name;

class RepositoryXmlTest {
    @Test
    void testIO() throws IOException {
        final RepositoryImpl repository1 = new RepositoryImpl();
        final RegistryImpl registry1 = repository1.registry().name("R1").build();
        repository1.getDescription()
                   .description(Locale.FRENCH, "Description de ce référentiel.")
                   .description(Locale.ENGLISH, "Description of this repository.")
                   .description(Locale.GERMAN, "");
        registry1.booleanType()
                 .name("B1")
                 .description(Locale.ENGLISH, "Boolean type description")
                 .build();
        registry1.booleanType().name("B2").build();
        registry1.integerType()
                 .name("I1")
                 .frozen(false)
                 .domain("1~10,20~100")
                 .description(Locale.ENGLISH, "Integer type description")
                 .build();
        registry1.integerType()
                 .name("I2")
                 .frozen(false)
                 .domain("1~10")
                 .s1000DPropertyType(S1000DPropertyType.PRODUCT_ATTRIBUTE)
                 .s1000DProductIdentifier(S1000DProductIdentifier.PRIMARY)
                 .build();

        registry1.realType().name("R1").frozen(true).domain("1.0~10.0").build();
        registry1.patternType().name("P1").frozen(false).pattern("[a-z]{0,3}").build();
        registry1.enumeratedType()
                 .name("E1")
                 .frozen(false)
                 .value()
                 .literal("A")
                 .description(Locale.ENGLISH, "A comment")
                 .back()
                 .value()
                 .literal("B")
                 .shortLiteral("b")
                 .ordinal(1)
                 .back()
                 .build();
        registry1.getDescription()
                 .description(Locale.ENGLISH, "Registry R1 description");
        registry1.property()
                 .name("P1")
                 .type("B1")
                 .ordinal(0)
                 .description(Locale.ENGLISH, "Property P1 description")
                 .build();
        registry1.property().name("P2").type("I1").ordinal(0).build();
        registry1.alias()
                 .name("A")
                 .expression("PO in {A}")
                 .description(Locale.ENGLISH, "Alias A description")
                 .build();
        registry1.createAssertion("A")
                 .getDescription()
                 .description(Locale.ENGLISH, "Useless assertion");
        registry1.property().name("E1").type("E1").ordinal(0).build();

        final PolicyImpl policy1 = registry1.policy().name("P1").build();
        policy1.getDescription()
               .description(Locale.FRENCH, "Description de cette politique.")
               .description(Locale.ENGLISH, "Description of this policy.");
        policy1.setItemUsage(Name.of("P1"), DItemUsage.OPTIONAL);
        policy1.setItemUsage(Name.of("P2"), DItemUsage.MANDATORY);
        policy1.setItemUsage(Name.of("A"), DItemUsage.RECOMMENDED);
        policy1.createAssertion("true");
        policy1.setWritingRuleEnabled("RULE 1", true);

        final PolicyImpl policy11 = policy1.policy().name("P11").build();
        policy11.setItemUsage(Name.of("P1"), DItemUsage.OPTIONAL);
        policy11.setItemUsage(Name.of("A"), DItemUsage.MANDATORY);
        policy11.createAssertion("true");

        repository1.registry().name("R2").build();

        final RegistryImpl registry3 = repository1.registry().name("R3").prefix("R3").parents(registry1).build();
        registry3.booleanType().name("B2S").build();
        registry3.integerType().name("I1S").frozen(false).domain("1~10,20~90").build();
        registry3.realType().name("R1S").frozen(true).domain("1.0~5.0").build();
        registry3.patternType().name("P1S").frozen(false).pattern("[a-z]{0,2}").build();
        registry3.enumeratedType()
                 .name("E1S")
                 .frozen(false)
                 .value()
                 .literal("A")
                 .description(Locale.ENGLISH, "A comment")
                 .back()
                 .build();

        registry3.property().name("E1S").type("E1S").ordinal(0).build();
        registry3.alias().name("AS").expression("E1S = A").build();

        final DictionariesBindingImpl binding1 = repository1.binding().source(registry1).target(registry3).build();
        binding1.booleanBooleanBinding()
                .source("B2")
                .target("B2S")
                .build();
        binding1.integerIntegerBinding()
                .source("I1")
                .target("I1S")
                .build();
        binding1.realRealBinding()
                .source("R1")
                .target("R1S")
                .build();
        binding1.patternPatternBinding()
                .source("P1")
                .target("P1S")
                .build();
        binding1.enumeratedEnumeratedBinding()
                .source("E1")
                .target("E1S")
                .map("A", "A")
                .build();

        binding1.enumeratedIntegerBinding()
                .source("E1")
                .target("I1S")
                .map("A", 1)
                .build();

        binding1.enumeratedBooleanBinding()
                .source("E1")
                .target("B2S")
                .map("A", true)
                .build();

        binding1.propertyPropertyBinding().source("E1").target("E1S").build();
        binding1.aliasAliasBinding().source("A").target("AS").build();

        RepositoryIoTest.checkSaveLoadSaveXml(repository1, "target/repository");
    }

    @Test
    void testUsage() throws IOException {
        final RepositoryImpl repository1 = new RepositoryImpl();
        final RegistryImpl registry1 = repository1.registry().name("R1").build();
        registry1.booleanType()
                 .name("B1")
                 .build();
        registry1.setTypeUsage("B1", DItemUsage.RECOMMENDED);
        registry1.property().name("P1").type("B1").build();
        registry1.property().name("P2").type("B1").build();
        registry1.setItemUsage("P1", DItemUsage.MANDATORY);
        final RegistryImpl registry11 = repository1.registry().name("R11").parents(registry1).build();
        final PolicyImpl policy111 = registry11.policy().name("P1").build();
        policy111.setTypeUsage("B1", DItemUsage.MANDATORY);

        RepositoryIoTest.checkSaveLoadSaveXml(repository1, "target/repository-usage");
    }
}