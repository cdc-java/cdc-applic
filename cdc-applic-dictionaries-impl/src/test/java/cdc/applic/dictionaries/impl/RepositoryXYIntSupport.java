package cdc.applic.dictionaries.impl;

import cdc.applic.dictionaries.handles.DictionaryHandle;

/**
 * Test data with 2 Integer properties.
 * <ul>
 * <li>X : {1, 2, 3}
 * <li>Y : {1, 2}
 * </ul>
 * Allowed cells are:
 * <pre>
 *   Y
 * R | 0  1  1  1
 * 2 | 0  1  1  1
 * 1 | 0  0  1  1
 *   +----------- X
 *     1  2  3  R
 * </pre>
 * This is modeled with these assertions:
 * <ul>
 * <li>X!=1
 * <li>!(X=2 & Y=1)
 * </ul>
 *
 * @author Damien Carbonne
 */
public class RepositoryXYIntSupport {
    public final RepositoryImpl repository = new RepositoryImpl();
    public final RegistryImpl registry = repository.registry().name("Registry").build();
    public final DictionaryHandle registryHandle = new DictionaryHandle(registry);

    public static final String X = "X";
    public static final String Y = "Y";

    public RepositoryXYIntSupport() {
        registry.integerType().name("X").frozen(false).domain("1~3").build();
        registry.integerType().name("Y").frozen(false).domain("1~2").build();

        registry.property().name("X").type("X").ordinal(0).build();
        registry.property().name("Y").type("Y").ordinal(1).build();

        registry.createAssertion("X!=1");
        registry.createAssertion("!(X=2 & Y=1)");
    }

    public static String toContent(int... values) {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final int value : values) {
            if (first) {
                first = false;
            } else {
                builder.append(",");
            }
            builder.append(value);
        }

        return builder.toString();
    }

    public static boolean filled(int x,
                                 int y,
                                 boolean withReserve) {
        if (withReserve) {
            return x >= 2 && x <= 4
                    && y >= 1 && y <= 3
                    && !(x == 2 && y == 1);

        } else {
            return x >= 1 && x <= 3
                    && y >= 1 && y <= 2;
        }
    }

    public static String x(int x) {
        return "X=" + x;
    }

    public static String x(int... x) {
        return "X in {" + toContent(x) + "}";
    }

    public static String y(int y) {
        return "Y=" + y;
    }

    public static String y(int... y) {
        return "Y in {" + toContent(y) + "}";
    }

    public static String xy(int x,
                            int y) {
        return "X=" + x + " & Y=" + y;
    }
}