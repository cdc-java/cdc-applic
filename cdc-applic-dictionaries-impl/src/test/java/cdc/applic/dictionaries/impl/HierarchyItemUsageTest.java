package cdc.applic.dictionaries.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.DItemUsage;

class HierarchyItemUsageTest {
    private static final Logger LOGGER = LogManager.getLogger(HierarchyItemUsageTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    @Test
    void test1() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl global = repository.registry().name("Global").build();
        final RegistryImpl prog1 = repository.registry().name("Prog1").parents(global).build();
        final RegistryImpl prog2 = repository.registry().name("Prog2").parents(global).build();
        final RegistryImpl cross = repository.registry().name("Cross").parents(prog1, prog2).build();
        final PolicyImpl policy1 = cross.policy().name("Policy1").build();
        final PolicyImpl policy1a = policy1.policy().name("PolicyA").build();
        final PolicyImpl policy1b = policy1.policy().name("PolicyB").build();

        global.alias().name("A").expression("true").ordinal(0).build();

        repository.print(OUT);

        assertSame(DItemUsage.OPTIONAL, global.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, prog1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, prog2.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, cross.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, policy1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, policy1a.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, policy1b.getEffectiveItemUsage("A"));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         policy1b.setItemUsage("A", DItemUsage.OPTIONAL);
                     });

        policy1.setItemUsage("A", DItemUsage.OPTIONAL);
        assertSame(DItemUsage.OPTIONAL, global.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, prog1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, prog2.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, cross.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, policy1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, policy1a.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, policy1b.getEffectiveItemUsage("A"));

        policy1a.setItemUsage("A", DItemUsage.OPTIONAL);
        policy1b.setItemUsage("A", DItemUsage.OPTIONAL);
        assertSame(DItemUsage.OPTIONAL, global.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, prog1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, prog2.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, cross.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, policy1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, policy1a.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, policy1b.getEffectiveItemUsage("A"));

        global.setItemUsage("A", DItemUsage.RECOMMENDED);
        assertSame(DItemUsage.RECOMMENDED, global.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, prog1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, prog2.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, cross.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, policy1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, policy1a.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, policy1b.getEffectiveItemUsage("A"));

        cross.setItemUsage("A", DItemUsage.FORBIDDEN);
        assertSame(DItemUsage.RECOMMENDED, global.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, prog1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, prog2.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, cross.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, policy1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, policy1a.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, policy1b.getEffectiveItemUsage("A"));

        cross.setItemUsage("A", DItemUsage.MANDATORY);
        assertSame(DItemUsage.RECOMMENDED, global.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, prog1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, prog2.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.MANDATORY, cross.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, policy1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, policy1a.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, policy1b.getEffectiveItemUsage("A"));
    }

    @Test
    void test2() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl global = repository.registry().name("Global").build();
        final RegistryImpl prog1 = repository.registry().name("Prog1").parents(global).build();
        final RegistryImpl prog2 = repository.registry().name("Prog2").parents(global).build();
        final RegistryImpl cross = repository.registry().name("Cross").parents(prog1, prog2).build();
        final PolicyImpl policy1 = cross.policy().name("Policy1").build();
        final PolicyImpl policy1a = policy1.policy().name("PolicyA").build();
        final PolicyImpl policy1b = policy1.policy().name("PolicyB").build();

        global.alias().name("A").expression("true").build();
        assertSame(DItemUsage.OPTIONAL, global.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, prog1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, prog2.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, cross.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, policy1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, policy1a.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, policy1b.getEffectiveItemUsage("A"));

        prog1.setItemUsage("A", DItemUsage.FORBIDDEN);
        assertSame(DItemUsage.OPTIONAL, global.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, prog1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, prog2.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, cross.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, policy1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, policy1a.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, policy1b.getEffectiveItemUsage("A"));
    }

    @Test
    void test3() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        r1.alias().name("A").expression("true").build();
        final RegistryImpl r11 = repository.registry().name("r11").parents(r1).build();
        final RegistryImpl r111 = repository.registry().name("r111").parents(r11).build();
        final RegistryImpl r1111 = repository.registry().name("r1111").parents(r111).build();
        assertSame(DItemUsage.OPTIONAL, r1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, r11.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, r111.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, r1111.getEffectiveItemUsage("A"));

        r11.setItemUsage("A", DItemUsage.MANDATORY);
        assertSame(DItemUsage.OPTIONAL, r1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.MANDATORY, r11.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, r111.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.OPTIONAL, r1111.getEffectiveItemUsage("A"));

        r111.setItemUsage("A", DItemUsage.FORBIDDEN);
        assertSame(DItemUsage.OPTIONAL, r1.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.MANDATORY, r11.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, r111.getEffectiveItemUsage("A"));
        assertSame(DItemUsage.FORBIDDEN, r1111.getEffectiveItemUsage("A"));
    }

    @Test
    void testSynonyms() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        r1.namingConvention().name("Convention1").build();
        r1.alias().name("A").synonym("Convention1", "a").expression("true").build();
        r1.setItemUsage("A", DItemUsage.OPTIONAL);
        final PolicyImpl p1 = r1.policy().name("p1").build();
        assertEquals(DItemUsage.OPTIONAL, r1.getEffectiveItemUsage("A"));
        assertEquals(DItemUsage.OPTIONAL, r1.getEffectiveItemUsage("a"));
        assertEquals(DItemUsage.FORBIDDEN, p1.getEffectiveItemUsage("A"));
        assertEquals(DItemUsage.FORBIDDEN, p1.getEffectiveItemUsage("a"));
        p1.setItemUsage("A", DItemUsage.OPTIONAL);

        r1.print(OUT);
        assertEquals(DItemUsage.OPTIONAL, p1.getEffectiveItemUsage("A"));
        assertEquals(DItemUsage.OPTIONAL, p1.getEffectiveItemUsage("a"));
    }
}