package cdc.applic.dictionaries.impl;

import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.items.UserDefinedAssertion;

class CachesSerialTest {

    @Test
    void testTypes() {
        // Creating or modifying a type should modify serial

        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry1 = repository.registry().name("Registry1").build();
        final long serial1_0 = registry1.getCachesSerial();
        final RegistryImpl registry2 = repository.registry().name("Registry2").parents(registry1).build();
        final long serial2_0 = registry2.getCachesSerial();
        final PolicyImpl policy3 = registry2.policy().name("Policy3").build();
        final long serial3_0 = policy3.getCachesSerial();

        registry1.booleanType().name("Boolean").build();
        final long serial1_1 = registry1.getCachesSerial();
        final long serial2_1 = registry2.getCachesSerial();
        final long serial3_1 = policy3.getCachesSerial();
        assertNotSame(serial1_0, serial1_1);
        assertNotSame(serial2_0, serial2_1);
        assertNotSame(serial3_0, serial3_1);

        registry1.integerType().name("I").frozen(false).domain("1~999").build();
        final long serial1_2 = registry1.getCachesSerial();
        final long serial2_2 = registry2.getCachesSerial();
        final long serial3_2 = policy3.getCachesSerial();
        assertNotSame(serial1_1, serial1_2);
        assertNotSame(serial2_1, serial2_2);
        assertNotSame(serial3_1, serial3_2);

        final EnumeratedTypeImpl e = registry1.enumeratedType().name("E").frozen(false).build();
        final long serial1_3 = registry1.getCachesSerial();
        final long serial2_3 = registry2.getCachesSerial();
        final long serial3_3 = policy3.getCachesSerial();
        assertNotSame(serial1_2, serial1_3);
        assertNotSame(serial2_2, serial2_3);
        assertNotSame(serial3_2, serial3_3);

        e.addValue("V1", 0);
        final long serial1_4 = registry1.getCachesSerial();
        final long serial2_4 = registry2.getCachesSerial();
        final long serial3_4 = policy3.getCachesSerial();
        assertNotSame(serial1_3, serial1_4);
        assertNotSame(serial2_3, serial2_4);
        assertNotSame(serial3_3, serial3_4);
    }

    @Test
    void testItems() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry1 = repository.registry().name("Registry1").build();
        final long serial1_0 = registry1.getCachesSerial();
        final RegistryImpl registry2 = repository.registry().name("Registry2").parents(registry1).build();
        final long serial2_0 = registry2.getCachesSerial();
        final PolicyImpl policy3 = registry2.policy().name("Policy3").build();
        final long serial3_0 = policy3.getCachesSerial();

        registry1.alias().name("A").expression("true").ordinal(0).build();
        final long serial1_1 = registry1.getCachesSerial();
        final long serial2_1 = registry2.getCachesSerial();
        final long serial3_1 = policy3.getCachesSerial();
        assertNotSame(serial1_0, serial1_1);
        assertNotSame(serial2_0, serial2_1);
        assertNotSame(serial3_0, serial3_1);

        registry2.alias().name("B").expression("true").ordinal(0).build();
        final long serial1_2 = registry1.getCachesSerial();
        final long serial2_2 = registry2.getCachesSerial();
        final long serial3_2 = policy3.getCachesSerial();
        assertSame(serial1_1, serial1_2);
        assertNotSame(serial2_1, serial2_2);
        assertNotSame(serial3_1, serial3_2);

        registry1.booleanType().name("Boolean").build();
        final long serial1_3 = registry1.getCachesSerial();
        final long serial2_3 = registry2.getCachesSerial();
        final long serial3_3 = policy3.getCachesSerial();
        assertNotSame(serial1_2, serial1_3);
        assertNotSame(serial2_2, serial2_3);
        assertNotSame(serial3_2, serial3_3);

        registry1.property().name("C").type("Boolean").build();
        final long serial1_4 = registry1.getCachesSerial();
        final long serial2_4 = registry2.getCachesSerial();
        final long serial3_4 = policy3.getCachesSerial();
        assertNotSame(serial1_3, serial1_4);
        assertNotSame(serial2_3, serial2_4);
        assertNotSame(serial3_3, serial3_4);

        registry2.property().name("D").type("Boolean").build();
        final long serial1_5 = registry1.getCachesSerial();
        final long serial2_5 = registry2.getCachesSerial();
        final long serial3_5 = policy3.getCachesSerial();
        assertSame(serial1_4, serial1_5);
        assertNotSame(serial2_4, serial2_5);
        assertNotSame(serial3_4, serial3_5);
    }

    @Test
    void testAssertions() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry1 = repository.registry().name("Registry1").build();
        final long serial1_0 = registry1.getCachesSerial();
        final RegistryImpl registry2 = repository.registry().name("Registry2").parents(registry1).build();
        final long serial2_0 = registry2.getCachesSerial();
        final PolicyImpl policy3 = registry2.policy().name("Policy3").build();
        final long serial3_0 = policy3.getCachesSerial();

        final UserDefinedAssertion x1 = registry1.createAssertion("true");
        final long serial1_1 = registry1.getCachesSerial();
        final long serial2_1 = registry2.getCachesSerial();
        final long serial3_1 = policy3.getCachesSerial();
        assertNotSame(serial1_0, serial1_1);
        assertNotSame(serial2_0, serial2_1);
        assertNotSame(serial3_0, serial3_1);

        final UserDefinedAssertion x2 = registry2.createAssertion("true");
        final long serial1_2 = registry1.getCachesSerial();
        final long serial2_2 = registry2.getCachesSerial();
        final long serial3_2 = policy3.getCachesSerial();
        assertSame(serial1_1, serial1_2);
        assertNotSame(serial2_1, serial2_2);
        assertNotSame(serial3_1, serial3_2);

        final UserDefinedAssertion x3 = policy3.createAssertion("true");
        final long serial1_3 = registry1.getCachesSerial();
        final long serial2_3 = registry2.getCachesSerial();
        final long serial3_3 = policy3.getCachesSerial();
        assertSame(serial1_2, serial1_3);
        assertSame(serial2_2, serial2_3);
        assertNotSame(serial3_2, serial3_3);

        registry1.removeAssertion(x1);
        final long serial1_4 = registry1.getCachesSerial();
        final long serial2_4 = registry2.getCachesSerial();
        final long serial3_4 = policy3.getCachesSerial();
        assertNotSame(serial1_3, serial1_4);
        assertNotSame(serial2_3, serial2_4);
        assertNotSame(serial3_3, serial3_4);

        registry2.removeAssertion(x2);
        final long serial1_5 = registry1.getCachesSerial();
        final long serial2_5 = registry2.getCachesSerial();
        final long serial3_5 = policy3.getCachesSerial();
        assertSame(serial1_4, serial1_5);
        assertNotSame(serial2_4, serial2_5);
        assertNotSame(serial3_4, serial3_5);

        policy3.removeAssertion(x3);
        final long serial1_6 = registry1.getCachesSerial();
        final long serial2_6 = registry2.getCachesSerial();
        final long serial3_6 = policy3.getCachesSerial();
        assertSame(serial1_5, serial1_6);
        assertSame(serial2_5, serial2_6);
        assertNotSame(serial3_5, serial3_6);
    }

    @Test
    void testContext() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry1 = repository.registry().name("Registry1").build();
        final long serial1_0 = registry1.getCachesSerial();
        final RegistryImpl registry2 = repository.registry().name("Registry2").parents(registry1).build();
        final long serial2_0 = registry2.getCachesSerial();
        final PolicyImpl policy3 = registry2.policy().name("Policy3").build();
        final long serial3_0 = policy3.getCachesSerial();

        registry2.setContextExpression("true");
        final long serial1_1 = registry1.getCachesSerial();
        final long serial2_1 = registry2.getCachesSerial();
        final long serial3_1 = policy3.getCachesSerial();
        assertSame(serial1_0, serial1_1);
        assertNotSame(serial2_0, serial2_1);
        assertNotSame(serial3_0, serial3_1);

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         registry1.setContextExpression("true");
                     });
    }

    @Test
    void testConstraints() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry1 = repository.registry().name("Registry1").build();
        final long serial1_0 = registry1.getCachesSerial();
        final RegistryImpl registry2 = repository.registry().name("Registry2").parents(registry1).build();
        final long serial2_0 = registry2.getCachesSerial();
        final PolicyImpl policy3 = registry2.policy().name("Policy3").build();
        final long serial3_0 = policy3.getCachesSerial();

        // Constraint creation

        final TestConstraint c1 = registry1.addConstraint(TestConstraint.create(registry1, "World"));
        final long serial1_1 = registry1.getCachesSerial();
        final long serial2_1 = registry2.getCachesSerial();
        final long serial3_1 = policy3.getCachesSerial();
        assertNotSame(serial1_0, serial1_1);
        assertNotSame(serial2_0, serial2_1);
        assertNotSame(serial3_0, serial3_1);

        final TestConstraint c2 = registry2.addConstraint(TestConstraint.create(registry2, "Hello"));
        final long serial1_2 = registry1.getCachesSerial();
        final long serial2_2 = registry2.getCachesSerial();
        final long serial3_2 = policy3.getCachesSerial();
        assertSame(serial1_1, serial1_2);
        assertNotSame(serial2_1, serial2_2);
        assertNotSame(serial3_1, serial3_2);

        final TestConstraint c3 = policy3.addConstraint(TestConstraint.create(policy3, "Foo"));
        final long serial1_3 = registry1.getCachesSerial();
        final long serial2_3 = registry2.getCachesSerial();
        final long serial3_3 = policy3.getCachesSerial();
        assertSame(serial1_2, serial1_3);
        assertSame(serial2_2, serial2_3);
        assertNotSame(serial3_2, serial3_3);

        // Constraint modification

        c1.setParams("WORLD");
        final long serial1_4 = registry1.getCachesSerial();
        final long serial2_4 = registry2.getCachesSerial();
        final long serial3_4 = policy3.getCachesSerial();
        assertNotSame(serial1_3, serial1_4);
        assertNotSame(serial2_3, serial2_4);
        assertNotSame(serial3_3, serial3_4);

        c2.setParams("HELLO");
        final long serial1_5 = registry1.getCachesSerial();
        final long serial2_5 = registry2.getCachesSerial();
        final long serial3_5 = policy3.getCachesSerial();
        assertSame(serial1_4, serial1_5);
        assertNotSame(serial2_4, serial2_5);
        assertNotSame(serial3_4, serial3_5);

        c3.setParams("FOO");
        final long serial1_6 = registry1.getCachesSerial();
        final long serial2_6 = registry2.getCachesSerial();
        final long serial3_6 = policy3.getCachesSerial();
        assertSame(serial1_5, serial1_6);
        assertSame(serial2_5, serial2_6);
        assertNotSame(serial3_5, serial3_6);

        // Constraint removal
        registry1.removeConstraint(c1);
        final long serial1_7 = registry1.getCachesSerial();
        final long serial2_7 = registry2.getCachesSerial();
        final long serial3_7 = policy3.getCachesSerial();
        assertNotSame(serial1_6, serial1_7);
        assertNotSame(serial2_6, serial2_7);
        assertNotSame(serial3_6, serial3_7);

        registry2.removeConstraint(c2);
        final long serial1_8 = registry1.getCachesSerial();
        final long serial2_8 = registry2.getCachesSerial();
        final long serial3_8 = policy3.getCachesSerial();
        assertSame(serial1_7, serial1_8);
        assertNotSame(serial2_7, serial2_8);
        assertNotSame(serial3_7, serial3_8);

        policy3.removeConstraint(c3);
        final long serial1_9 = registry1.getCachesSerial();
        final long serial2_9 = registry2.getCachesSerial();
        final long serial3_9 = policy3.getCachesSerial();
        assertSame(serial1_8, serial1_9);
        assertSame(serial2_8, serial2_9);
        assertNotSame(serial3_8, serial3_9);

    }
}