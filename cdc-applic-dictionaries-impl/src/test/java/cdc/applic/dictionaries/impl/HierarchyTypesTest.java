package cdc.applic.dictionaries.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;

class HierarchyTypesTest {
    private static final Logger LOGGER = LogManager.getLogger(HierarchyTypesTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    @Test
    void testDuplicatesLocalNP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        final Type t1 = r1.booleanType().name("Type1").build();

        assertSame(t1, r1.getType(Name.of("Type1")));
        assertEquals(Name.of("Type1"), t1.getName());

        // Check that we can not declare twice a type locally
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r1.booleanType().name("Type1").build();
                     });
    }

    @Test
    void testDuplicatesLocalP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final Type t1 = r1.booleanType().name("Type1").build();

        assertSame(t1, r1.getType(Name.of("Type1")));
        assertSame(t1, r1.getType(Name.of("R1.Type1")));
        assertEquals(Name.of("R1.Type1"), t1.getName());

        // Check that we can not declare twice a type locally
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r1.booleanType().name("Type1").build();
                     });
    }

    @Test
    void testDuplicatesInheritanceNPNP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        final Type t1 = r1.booleanType().name("Type1").build();
        final RegistryImpl r2 = repository.registry().name("r2").parents(r1).build();

        assertSame(t1, r1.getType(Name.of("Type1")));

        // r1.Type1 would not be accessible from r2
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r2.booleanType().name("Type1").build();
                     });
    }

    @Test
    void testDuplicatesInheritanceNPP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        r1.booleanType().name("Type1").build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();

        // r1.Type1 would not be accessible from r2
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r2.booleanType().name("Type1").build();
                     });
    }

    @Test
    void testDuplicatesInheritancePP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final Type t1 = r1.booleanType().name("Type1").build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();
        final Type t2 = r2.booleanType().name("Type1").build();

        assertEquals(Name.of("R1.Type1"), t1.getName());
        assertEquals(Name.of("R2.Type1"), t2.getName());

        assertNotNull(r2.getType(Name.of("R1.Type1")));
        assertNotNull(r2.getType(Name.of("Type1")));
        assertNotSame(r2.getType(Name.of("R1.Type1")), r2.getType(Name.of("Type1")));

        assertSame(t1, r2.getType(Name.of("R1.Type1")));
        assertSame(t2, r2.getType(Name.of("Type1")));
        assertSame(t2, r2.getType(Name.of("R2.Type1")));

        assertEquals(1, r1.getDeclaredTypes().size());
        assertEquals(1, r1.getAllTypes().size());

        assertEquals(1, r2.getDeclaredTypes().size());
        assertEquals(2, r2.getAllTypes().size());
    }

    @Test
    void testDuplicatesInheritancePNP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final Type t1 = r1.booleanType().name("Type1").build();
        final RegistryImpl r2 = repository.registry().name("r2").parents(r1).build();
        final Type t2 = r2.booleanType().name("Type1").build();

        assertSame(t1, r2.getType(Name.of("R1.Type1")));
        assertSame(t2, r2.getType(Name.of("Type1")));

        assertNotNull(r2.getType(Name.of("R1.Type1")));
        assertNotNull(r2.getType(Name.of("Type1")));
        assertNotSame(r2.getType(Name.of("R1.Type1")), r2.getType(Name.of("Type1")));

        assertEquals(1, r1.getDeclaredTypes().size());
        assertEquals(1, r1.getAllTypes().size());

        assertEquals(1, r2.getDeclaredTypes().size());
        assertEquals(2, r2.getAllTypes().size());
    }

    @Test
    void testCanCreateTypeP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        assertFalse(r1.canCreateType((SName) null));
        assertTrue(r1.canCreateType("Type1"));
        r1.booleanType().name("Type1").build();
        assertFalse(r1.canCreateType("Type1"));
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").build();
        assertTrue(r2.canCreateType("Type1"));
        r2.booleanType().name("Type1").build();
        assertFalse(r2.canCreateType("Type1"));
        final RegistryImpl r3 = repository.registry().name("r3").prefix("R3").parents(r1, r2).build();
        assertTrue(r3.canCreateType("Type1"));
        r3.booleanType().name("Type1").build();
        assertFalse(r3.canCreateType("Type1"));
    }

    @Test
    void testCanCreateTypeNP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        assertFalse(r1.canCreateType((SName) null));
        assertTrue(r1.canCreateType("Type1"));
        r1.booleanType().name("Type1").build();
        assertFalse(r1.canCreateType("Type1"));
        final RegistryImpl r2 = repository.registry().name("r2").build();
        assertTrue(r2.canCreateType("Type1"));
        r2.booleanType().name("Type1").build();
        assertFalse(r2.canCreateType("Type1"));
        final RegistryImpl r3 = repository.registry().name("r3").parents(r1, r2).build();
        assertFalse(r3.canCreateType("Type1"));
        assertTrue(r3.canCreateType("Type2"));
        r3.booleanType().name("Type2").build();
        assertFalse(r1.canCreateType("Type2"));
        assertFalse(r2.canCreateType("Type2"));
    }

    @Test
    void testTypeSynonymsNP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        r1.namingConvention().name("Convention1").build();
        final BooleanTypeImpl t1 = r1.booleanType()
                                     .name("Type1")
                                     .synonym("Convention1", "type1")
                                     .build();
        r1.enumeratedType()
          .name("Type2")
          .synonym("Convention1", "type2")
          .value()
          .literal("V1")
          .synonym("Convention1", "v1")
          .back()
          .value()
          .literal("V2")
          .synonym("Convention1", "v2")
          .back()
          .build();

        r1.policy().name("p1").build();

        repository.print(OUT);

        assertSame(t1, r1.getType("Type1"));
        assertSame(t1, r1.getType("type1"));
        assertNotNull(r1.getType("Type2", EnumeratedTypeImpl.class).getValue("V1"));
        assertNotNull(r1.getType("Type2", EnumeratedTypeImpl.class).getValue("v1"));
        assertSame(r1.getType("Type2", EnumeratedTypeImpl.class).getValue("V1"),
                   r1.getType("Type2", EnumeratedTypeImpl.class).getValue("v1"));
        assertNotNull(r1.getType("Type2", EnumeratedTypeImpl.class).getValue("V2"));
        assertNotNull(r1.getType("Type2", EnumeratedTypeImpl.class).getValue("v2"));
        assertSame(r1.getType("Type2", EnumeratedTypeImpl.class).getValue("V2"),
                   r1.getType("Type2", EnumeratedTypeImpl.class).getValue("v2"));
    }
}