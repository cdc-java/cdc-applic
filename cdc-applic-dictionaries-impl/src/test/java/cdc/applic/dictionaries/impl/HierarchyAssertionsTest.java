package cdc.applic.dictionaries.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.items.ConstraintAssertion;
import cdc.applic.dictionaries.items.ContextAssertion;
import cdc.applic.dictionaries.items.DerivedContextAssertion;
import cdc.applic.dictionaries.items.DerivedStandardAssertion;
import cdc.applic.dictionaries.items.UserDefinedAssertion;

class HierarchyAssertionsTest {
    // private static final Logger LOGGER = LogManager.getLogger(HierarchyAssertionsTest.class);
    // private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.INFO).buildPrintStream();

    @Test
    void testEarly0NPNP() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry()
                                          .name("r1")
                                          .build();
        r1.createAssertion("not false");
        final RegistryImpl r2 = repository.registry()
                                          .name("r2")
                                          .parents(r1)
                                          .build();

        // repository.print(OUT);

        assertEquals(0, IterableUtils.size(r1.getAssertions(ContextAssertion.class)));
        assertEquals(1, IterableUtils.size(r1.getAssertions(UserDefinedAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(ConstraintAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(DerivedStandardAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(DerivedContextAssertion.class)));

        assertEquals(0, IterableUtils.size(r2.getAssertions(ContextAssertion.class)));
        assertEquals(0, IterableUtils.size(r2.getAssertions(UserDefinedAssertion.class)));
        assertEquals(0, IterableUtils.size(r2.getAssertions(ConstraintAssertion.class)));
        assertEquals(1, IterableUtils.size(r2.getAssertions(DerivedStandardAssertion.class)));
        assertEquals(0, IterableUtils.size(r2.getAssertions(DerivedContextAssertion.class)));

        RepositoryIoTest.checkSaveLoadSave(repository, "target/hierarchy-assertions-early0-npnp");
    }

    @Test
    void testEarlyNPNP() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry()
                                          .name("r1")
                                          .build();
        r1.booleanType().name("Type").build();
        r1.property().name("P1").type("Type").ordinal(0).build();
        r1.createAssertion("P1");
        final RegistryImpl r2 = repository.registry()
                                          .name("r2")
                                          .parents(r1)
                                          .build();

        // repository.print(OUT);

        assertEquals(0, IterableUtils.size(r1.getAssertions(ContextAssertion.class)));
        assertEquals(1, IterableUtils.size(r1.getAssertions(UserDefinedAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(ConstraintAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(DerivedStandardAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(DerivedContextAssertion.class)));

        assertEquals(0, IterableUtils.size(r2.getAssertions(ContextAssertion.class)));
        assertEquals(0, IterableUtils.size(r2.getAssertions(UserDefinedAssertion.class)));
        assertEquals(0, IterableUtils.size(r2.getAssertions(ConstraintAssertion.class)));
        assertEquals(1, IterableUtils.size(r2.getAssertions(DerivedStandardAssertion.class)));
        assertEquals(0, IterableUtils.size(r2.getAssertions(DerivedContextAssertion.class)));

        RepositoryIoTest.checkSaveLoadSave(repository, "target/hierarchy-assertions-early-npnp");
    }

    @Test
    void testLateNPNP() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry()
                                          .name("r1")
                                          .build();
        r1.booleanType().name("Type").build();
        r1.property().name("P1").type("Type").ordinal(0).build();
        final RegistryImpl r2 = repository.registry()
                                          .name("r2")
                                          .parents(r1)
                                          .build();
        r1.createAssertion("P1");

        // repository.print(OUT);

        assertEquals(0, IterableUtils.size(r1.getAssertions(ContextAssertion.class)));
        assertEquals(1, IterableUtils.size(r1.getAssertions(UserDefinedAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(ConstraintAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(DerivedStandardAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(DerivedContextAssertion.class)));

        assertEquals(0, IterableUtils.size(r2.getAssertions(ContextAssertion.class)));
        assertEquals(0, IterableUtils.size(r2.getAssertions(UserDefinedAssertion.class)));
        assertEquals(0, IterableUtils.size(r2.getAssertions(ConstraintAssertion.class)));
        assertEquals(1, IterableUtils.size(r2.getAssertions(DerivedStandardAssertion.class)));
        assertEquals(0, IterableUtils.size(r2.getAssertions(DerivedContextAssertion.class)));

        RepositoryIoTest.checkSaveLoadSave(repository, "target/hierarchy-assertions-late-npnp");
    }

    @Test
    void testEarlyNPPolicy() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry()
                                          .name("r1")
                                          .build();
        r1.booleanType().name("Type").build();
        r1.property().name("P1").type("Type").ordinal(0).build();
        r1.createAssertion("P1");
        final PolicyImpl p2 = r1.policy().name("p1").build();

        // repository.print(OUT);

        assertEquals(0, IterableUtils.size(r1.getAssertions(ContextAssertion.class)));
        assertEquals(1, IterableUtils.size(r1.getAssertions(UserDefinedAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(ConstraintAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(DerivedStandardAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(DerivedContextAssertion.class)));

        assertEquals(0, IterableUtils.size(p2.getAssertions(ContextAssertion.class)));
        assertEquals(0, IterableUtils.size(p2.getAssertions(UserDefinedAssertion.class)));
        assertEquals(0, IterableUtils.size(p2.getAssertions(ConstraintAssertion.class)));
        assertEquals(1, IterableUtils.size(p2.getAssertions(DerivedStandardAssertion.class)));
        assertEquals(0, IterableUtils.size(p2.getAssertions(DerivedContextAssertion.class)));

        RepositoryIoTest.checkSaveLoadSave(repository, "target/hierarchy-assertions-early-nppolicy");
    }

    @Test
    void testLateNPPolicy() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry()
                                          .name("r1")
                                          .build();
        r1.booleanType().name("Type").build();
        r1.property().name("P1").type("Type").ordinal(0).build();
        final PolicyImpl p2 = r1.policy().name("p1").build();
        r1.createAssertion("P1");

        // repository.print(OUT);

        assertEquals(0, IterableUtils.size(r1.getAssertions(ContextAssertion.class)));
        assertEquals(1, IterableUtils.size(r1.getAssertions(UserDefinedAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(ConstraintAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(DerivedStandardAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(DerivedContextAssertion.class)));

        assertEquals(0, IterableUtils.size(p2.getAssertions(ContextAssertion.class)));
        assertEquals(0, IterableUtils.size(p2.getAssertions(UserDefinedAssertion.class)));
        assertEquals(0, IterableUtils.size(p2.getAssertions(ConstraintAssertion.class)));
        assertEquals(1, IterableUtils.size(p2.getAssertions(DerivedStandardAssertion.class)));
        assertEquals(0, IterableUtils.size(p2.getAssertions(DerivedContextAssertion.class)));

        RepositoryIoTest.checkSaveLoadSave(repository, "target/hierarchy-assertions-late-nppolicy");
    }

    @Test
    void testEarlyPP() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry()
                                          .name("r1")
                                          .prefix("R1")
                                          .build();
        r1.booleanType().name("Type").build();
        r1.property().name("P1").type("Type").ordinal(0).build();
        r1.createAssertion("P1");
        final RegistryImpl r2 = repository.registry()
                                          .name("r2")
                                          .prefix("R2")
                                          .parents(r1)
                                          .build();

        // repository.print(OUT);

        assertEquals(0, IterableUtils.size(r1.getAssertions(ContextAssertion.class)));
        assertEquals(1, IterableUtils.size(r1.getAssertions(UserDefinedAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(ConstraintAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(DerivedStandardAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(DerivedContextAssertion.class)));

        assertEquals(0, IterableUtils.size(r2.getAssertions(ContextAssertion.class)));
        assertEquals(0, IterableUtils.size(r2.getAssertions(UserDefinedAssertion.class)));
        assertEquals(0, IterableUtils.size(r2.getAssertions(ConstraintAssertion.class)));
        assertEquals(1, IterableUtils.size(r2.getAssertions(DerivedStandardAssertion.class)));
        assertEquals(0, IterableUtils.size(r2.getAssertions(DerivedContextAssertion.class)));

        RepositoryIoTest.checkSaveLoadSave(repository, "target/hierarchy-assertions-early-pp");
    }

    @Test
    void testLatePP() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry()
                                          .name("r1")
                                          .prefix("R1")
                                          .build();
        r1.booleanType().name("Type").build();
        r1.property().name("P1").type("Type").ordinal(0).build();
        final RegistryImpl r2 = repository.registry()
                                          .name("r2")
                                          .prefix("R2")
                                          .parents(r1)
                                          .build();
        r1.createAssertion("P1");

        // repository.print(OUT);

        assertEquals(0, IterableUtils.size(r1.getAssertions(ContextAssertion.class)));
        assertEquals(1, IterableUtils.size(r1.getAssertions(UserDefinedAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(ConstraintAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(DerivedStandardAssertion.class)));
        assertEquals(0, IterableUtils.size(r1.getAssertions(DerivedContextAssertion.class)));

        assertEquals(0, IterableUtils.size(r2.getAssertions(ContextAssertion.class)));
        assertEquals(0, IterableUtils.size(r2.getAssertions(UserDefinedAssertion.class)));
        assertEquals(0, IterableUtils.size(r2.getAssertions(ConstraintAssertion.class)));
        assertEquals(1, IterableUtils.size(r2.getAssertions(DerivedStandardAssertion.class)));
        assertEquals(0, IterableUtils.size(r2.getAssertions(DerivedContextAssertion.class)));

        RepositoryIoTest.checkSaveLoadSave(repository, "target/hierarchy-assertions-late-pp");
    }
}