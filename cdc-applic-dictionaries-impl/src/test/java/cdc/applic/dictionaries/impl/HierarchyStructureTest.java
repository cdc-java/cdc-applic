package cdc.applic.dictionaries.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.literals.SName;

class HierarchyStructureTest {
    @Test
    void testBasic() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("Prefix").build();

        assertEquals("r1", r1.getName());
        assertEquals(SName.of("Prefix"), r1.getPrefix().get());
    }

    @Test
    void testDuplicateRegistryNames() {
        // Check that we can not create 2 registries with the same name
        final RepositoryImpl repository = new RepositoryImpl();
        repository.registry().name("r1").build();

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         repository.registry().name("r1").build();
                     });
    }

    @Test
    void testDuplicateRegistryPrefixes() {
        // Check that we can create 2 registries with the same prefix
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("Prefix").build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("Prefix").build();

        assertEquals(SName.of("Prefix"), r1.getPrefix().get());
        assertEquals(SName.of("Prefix"), r2.getPrefix().get());
    }

    @Test
    void testCompositionDuplicatesNPNP() {
        // Check that the same empty prefix can be used twice in a hierarchy
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        final RegistryImpl r2 = repository.registry().name("r2").parents(r1).build();

        assertEquals(1, r1.getChildren().size());
        assertEquals(0, r1.getParents().size());
        assertEquals(0, r2.getChildren().size());
        assertEquals(1, r2.getParents().size());
    }

    @Test
    void testCompositionDuplicatesPP() {
        // Check that the same prefix can be used twice in a hierarchy
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R").build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R").parents(r1).build();

        assertEquals(1, r1.getChildren().size());
        assertEquals(0, r1.getParents().size());
        assertEquals(0, r2.getChildren().size());
        assertEquals(1, r2.getParents().size());
    }
}