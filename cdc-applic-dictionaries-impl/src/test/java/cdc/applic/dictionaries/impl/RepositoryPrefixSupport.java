package cdc.applic.dictionaries.impl;

import cdc.applic.dictionaries.handles.DictionaryHandle;

public class RepositoryPrefixSupport {
    public enum Registry {
        GLOBAL,
        P1,
        P2,
        ALL
    }

    public final RepositoryImpl repository = new RepositoryImpl();
    public final RegistryImpl registryGobal = repository.registry().name("Global").build();
    public final DictionaryHandle registryGlobalHandle = new DictionaryHandle(registryGobal);
    public final RegistryImpl registryP1 = repository.registry().name("Program 1").prefix("P1").parents(registryGobal).build();
    public final DictionaryHandle registryP1Handle = new DictionaryHandle(registryP1);
    public final RegistryImpl registryP2 = repository.registry().name("Program 2").prefix("P2").parents(registryGobal).build();
    public final DictionaryHandle registryP2Handle = new DictionaryHandle(registryP2);
    public final RegistryImpl registryAll = repository.registry().name("All Programs").parents(registryP1, registryP2).build();
    public final DictionaryHandle registryAllHandle = new DictionaryHandle(registryAll);

    public RepositoryPrefixSupport() {
        registryGobal.integerType().name("Rank").frozen(false).domain("1~999").build();
        registryGobal.enumeratedType().name("Family").frozen(false).literals("F1", "F2", "F3").build();
        registryGobal.property().name("Family").type("Family").ordinal(0).build();

        registryP1.setContextExpression("Family = F1");
        registryP1.property().name("Rank").type("Rank").ordinal(0).build();
        registryP1.alias().name("Rank10").expression("Rank = 10").build();
        registryP2.setContextExpression("Family = F2");
        registryP2.property().name("Rank").type("Rank").ordinal(0).build();
        registryP2.alias().name("Rank10").expression("Rank = 10").build();
    }

    public RegistryImpl getRegistry(Registry registry) {
        return switch (registry) {
        case ALL -> registryAll;
        case GLOBAL -> registryGobal;
        case P1 -> registryP1;
        case P2 -> registryP2;
        default -> null;
        };
    }

    public DictionaryHandle getHandle(Registry registry) {
        return switch (registry) {
        case ALL -> registryAllHandle;
        case GLOBAL -> registryGlobalHandle;
        case P1 -> registryP1Handle;
        case P2 -> registryP2Handle;
        default -> null;
        };
    }
}