package cdc.applic.dictionaries.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.visitors.RemovePrefixes;
import cdc.applic.expressions.Expression;

class RemovePrefixesTest {
    @Test
    void test() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        r1.alias().name("A").expression("true").build();
        final Expression x1 = new Expression("R1.A");
        final Expression y1 = RemovePrefixes.execute(x1, r1);
        assertEquals("A", y1.toString());
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();
        r2.alias().name("B").expression("true").build();
        final Expression x2 = new Expression("R1.A or R2.B");
        final Expression y2 = RemovePrefixes.execute(x2, r2);
        assertEquals("R1.A|B", y2.toString());
    }
}