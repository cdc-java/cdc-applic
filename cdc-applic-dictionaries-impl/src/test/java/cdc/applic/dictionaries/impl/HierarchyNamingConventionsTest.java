package cdc.applic.dictionaries.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;
import java.util.Set;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;

class HierarchyNamingConventionsTest {
    private static final Logger LOGGER = LogManager.getLogger(HierarchyNamingConventionsTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    @Test
    void testDuplicatesLocalNP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        final NamingConvention nc1 = r1.namingConvention().name("Convention1").build();

        assertSame(nc1, r1.getNamingConvention(Name.of("Convention1")));
        assertEquals(Name.of("Convention1"), nc1.getName());

        // Check that we can not declare twice a type locally
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r1.namingConvention().name("Convention1").build();
                     });
    }

    @Test
    void testDuplicatesLocalP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final NamingConvention nc1 = r1.namingConvention().name("Convention1").build();

        assertSame(nc1, r1.getNamingConvention(Name.of("Convention1")));
        assertSame(nc1, r1.getNamingConvention(Name.of("R1.Convention1")));
        assertEquals(Name.of("R1.Convention1"), nc1.getName());

        // Check that we can not declare twice a type locally
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r1.namingConvention().name("Convention1").build();
                     });
    }

    @Test
    void testDuplicatesInheritanceNPNP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        final NamingConvention nc1 = r1.namingConvention().name("Convention1").build();
        final RegistryImpl r2 = repository.registry().name("r2").parents(r1).build();

        assertSame(nc1, r1.getNamingConvention(Name.of("Convention1")));

        // r1.Convention1 would not be accessible from r2
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r2.namingConvention().name("Convention1").build();
                     });
    }

    @Test
    void testDuplicatesInheritanceNPP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        r1.namingConvention().name("Convention1").build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();

        // r1.Convention1 would not be accessible from r2
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r2.namingConvention().name("Convention1").build();
                     });
    }

    @Test
    void testDuplicatesInheritancePP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final NamingConvention nc1 = r1.namingConvention().name("Convention1").build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();
        final NamingConvention nc2 = r2.namingConvention().name("Convention1").build();

        assertEquals(Name.of("R1.Convention1"), nc1.getName());
        assertEquals(Name.of("R2.Convention1"), nc2.getName());

        assertNotNull(r2.getNamingConvention(Name.of("R1.Convention1")));
        assertNotNull(r2.getNamingConvention(Name.of("Convention1")));
        assertNotSame(r2.getNamingConvention(Name.of("R1.Convention1")), r2.getNamingConvention(Name.of("Convention1")));

        assertSame(nc1, r2.getNamingConvention(Name.of("R1.Convention1")));
        assertSame(nc2, r2.getNamingConvention(Name.of("Convention1")));
        assertSame(nc2, r2.getNamingConvention(Name.of("R2.Convention1")));

        assertEquals(1, r1.getDeclaredNamingConventions().size());
        assertEquals(2, r1.getAllNamingConventions().size());

        assertEquals(1, r2.getDeclaredNamingConventions().size());
        assertEquals(3, r2.getAllNamingConventions().size());
    }

    @Test
    void testDuplicatesInheritancePNP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        final NamingConvention nc1 = r1.namingConvention().name("Convention1").build();
        final RegistryImpl r2 = repository.registry().name("r2").parents(r1).build();
        final NamingConvention nc2 = r2.namingConvention().name("Convention1").build();

        assertSame(nc1, r2.getNamingConvention(Name.of("R1.Convention1")));
        assertSame(nc2, r2.getNamingConvention(Name.of("Convention1")));

        assertNotNull(r2.getNamingConvention(Name.of("R1.Convention1")));
        assertNotNull(r2.getNamingConvention(Name.of("Convention1")));
        assertNotSame(r2.getNamingConvention(Name.of("R1.Convention1")), r2.getNamingConvention(Name.of("Convention1")));

        assertEquals(1, r1.getDeclaredNamingConventions().size());
        assertEquals(2, r1.getAllNamingConventions().size());

        assertEquals(1, r2.getDeclaredNamingConventions().size());
        assertEquals(3, r2.getAllNamingConventions().size());
    }

    @Test
    void testCanCreateTypeP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        assertFalse(r1.canCreateNamingConvention((SName) null));
        assertTrue(r1.canCreateNamingConvention("Convention1"));
        r1.namingConvention().name("Convention1").build();
        assertFalse(r1.canCreateNamingConvention("Convention1"));
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").build();
        assertTrue(r2.canCreateNamingConvention("Convention1"));
        r2.namingConvention().name("Convention1").build();
        assertFalse(r2.canCreateNamingConvention("Convention1"));
        final RegistryImpl r3 = repository.registry().name("r3").prefix("R3").parents(r1, r2).build();
        assertTrue(r3.canCreateNamingConvention("Convention1"));
        r3.namingConvention().name("Convention1").build();
        assertFalse(r3.canCreateNamingConvention("Convention1"));
    }

    @Test
    void testCanCreateTypeNP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        assertFalse(r1.canCreateNamingConvention((SName) null));
        assertTrue(r1.canCreateNamingConvention("Convention1"));
        r1.namingConvention().name("Convention1").build();
        assertFalse(r1.canCreateNamingConvention("Convention1"));
        final RegistryImpl r2 = repository.registry().name("r2").build();
        assertTrue(r2.canCreateNamingConvention("Convention1"));
        r2.namingConvention().name("Convention1").build();
        assertFalse(r2.canCreateNamingConvention("Convention1"));
        final RegistryImpl r3 = repository.registry().name("r3").parents(r1, r2).build();
        assertFalse(r3.canCreateNamingConvention("Convention1"));
        assertTrue(r3.canCreateNamingConvention("Convention2"));
        r3.namingConvention().name("Convention2").build();
        assertFalse(r1.canCreateNamingConvention("Convention2"));
        assertFalse(r2.canCreateNamingConvention("Convention2"));
    }

    @Test
    void testNamingConventions() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R").build();
        final NamingConvention conv1 = r1.namingConvention().name("Convention1").build();
        final NamingConvention def = NamingConvention.DEFAULT;

        r1.booleanType().name("BooleanType").build();
        r1.property().name("B1").type("BooleanType").build();
        r1.property().name("B2").type("BooleanType").synonym("Convention1", "b2").build();

        final PolicyImpl p1 = r1.policy().name("p1").build();

        r1.print(OUT);

        assertEquals(Set.of(def), r1.getProperty("B1").getNames().getNamingConventions());
        assertEquals(Set.of(def), r1.getProperty("R.B1").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), r1.getProperty("B2").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), r1.getProperty("R.B2").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), r1.getProperty("b2").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), r1.getProperty("R.b2").getNames().getNamingConventions());

        assertEquals(Set.of(def), p1.getProperty("B1").getNames().getNamingConventions());
        assertEquals(Set.of(def), p1.getProperty("R.B1").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), p1.getProperty("B2").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), p1.getProperty("R.B2").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), p1.getProperty("b2").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), p1.getProperty("R.b2").getNames().getNamingConventions());

        assertEquals(Set.of(def), r1.getItem("B1").getNames().getNamingConventions());
        assertEquals(Set.of(def), r1.getItem("R.B1").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), r1.getItem("B2").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), r1.getItem("R.B2").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), r1.getItem("b2").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), r1.getItem("R.b2").getNames().getNamingConventions());

        assertEquals(Set.of(def), p1.getItem("B1").getNames().getNamingConventions());
        assertEquals(Set.of(def), p1.getItem("R.B1").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), p1.getItem("B2").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), p1.getItem("R.B2").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), p1.getItem("b2").getNames().getNamingConventions());
        assertEquals(Set.of(def, conv1), p1.getItem("R.b2").getNames().getNamingConventions());

        assertEquals(Set.of(), r1.getItemNameNamingConventions("Foo"));

        assertEquals(Set.of(def), r1.getItemNameNamingConventions("B1"));
        assertEquals(Set.of(def), r1.getItemNameNamingConventions("R.B1"));
        assertEquals(Set.of(def), r1.getItemNameNamingConventions("B2"));
        assertEquals(Set.of(def), r1.getItemNameNamingConventions("R.B2"));
        assertEquals(Set.of(conv1), r1.getItemNameNamingConventions("b2"));
        assertEquals(Set.of(conv1), r1.getItemNameNamingConventions("R.b2"));

        assertEquals(Set.of(def), p1.getItemNameNamingConventions("B1"));
        assertEquals(Set.of(def), p1.getItemNameNamingConventions("R.B1"));
        assertEquals(Set.of(def), p1.getItemNameNamingConventions("B2"));
        assertEquals(Set.of(def), p1.getItemNameNamingConventions("R.B2"));
        assertEquals(Set.of(conv1), p1.getItemNameNamingConventions("b2"));
        assertEquals(Set.of(conv1), p1.getItemNameNamingConventions("R.b2"));
    }
}