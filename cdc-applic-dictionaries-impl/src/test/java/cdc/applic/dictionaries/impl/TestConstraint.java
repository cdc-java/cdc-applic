package cdc.applic.dictionaries.impl;

import cdc.applic.dictionaries.Constraint;
import cdc.applic.dictionaries.Constraints;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.expressions.Expression;
import cdc.util.lang.Checks;

/**
 * Implementation of {@link Constraint} dedicated to tests.
 * <p>
 * It is associated to 2 hard-coded expressions.
 *
 * @author Damien Carbonne
 */
class TestConstraint implements Constraint {
    protected final Dictionary dictionary;
    private final DescriptionImpl description = new DescriptionImpl();
    private String params;

    public static final String CONSTRAINT_NAME = "TEST";

    static {
        Constraints.register(CONSTRAINT_NAME, TestConstraint::create);
    }

    public TestConstraint(Dictionary dictionary) {
        Checks.isNotNull(dictionary, "dictionary");
        this.dictionary = dictionary;
    }

    public TestConstraint setParams(String params) {
        this.params = params;
        updateAssertions();
        return this;
    }

    @Override
    public final Dictionary getOwner() {
        return dictionary;
    }

    @Override
    public final DescriptionImpl getDescription() {
        return description;
    }

    @Override
    public String getTypeName() {
        return CONSTRAINT_NAME;
    }

    @Override
    public String getParams() {
        return params;
    }

    private void updateAssertions() {
        dictionary.removeRelatedAndDerivedAssertions(this);
        dictionary.createAssertion(this, "A1", Expression.TRUE);
        dictionary.createAssertion(this, "A2", Expression.TRUE);
    }

    public static TestConstraint create(Dictionary dictionary,
                                        String params) {
        return new TestConstraint(dictionary).setParams(params);
    }

    public static void elaborate() {
        // Ignore
    }
}