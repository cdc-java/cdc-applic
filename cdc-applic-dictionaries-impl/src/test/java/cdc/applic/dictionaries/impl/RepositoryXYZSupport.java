package cdc.applic.dictionaries.impl;

import cdc.applic.dictionaries.handles.DictionaryHandle;

/**
 * Test data with 3 properties.
 * <ul>
 * <li>X : {X1, X2}
 * <li>Y : {Y1, Y2}
 * <li>Z : {Z1, Z2}
 * </ul>
 * Allowed cells are:
 * <pre>
 * Z = Z1:
 *   Y
 * 2 | 0  1
 * 1 | 0  0
 *   +----- X
 *     1  2
 *
 * Z = Z2:
 *   Y
 * 2 | 1  1
 * 1 | 0  1
 *   +----- X
 *     1  2
 * </pre>
 * This is modeled with these assertions:
 * <ul>
 * <li>!(X=X1 & Y=Y1)
 * <li>!(Y=Y1 & Z=Z1)
 * <li>!(Z=Z1 & X=X1)
 * </ul>
 *
 * @author Damien Carbonne
 */
public class RepositoryXYZSupport {
    public final RepositoryImpl repository = new RepositoryImpl();
    public final RegistryImpl registry = repository.registry().name("Registry").build();
    public final DictionaryHandle registryHandle = new DictionaryHandle(registry);

    public static final String X = "X";
    public static final String Y = "Y";
    public static final String Z = "Z";

    public RepositoryXYZSupport() {
        registry.enumeratedType().name("X").frozen(false).literals("X1", "X2").build();
        registry.enumeratedType().name("Y").frozen(false).literals("Y1", "Y2").build();
        registry.enumeratedType().name("Z").frozen(false).literals("Z1", "Z2").build();

        registry.property().name("X").type("X").ordinal(0).build();
        registry.property().name("Y").type("Y").ordinal(1).build();
        registry.property().name("Z").type("Z").ordinal(2).build();

        registry.createAssertion("!(X=X1 & Y=Y1)");
        registry.createAssertion("!(Y=Y1 & Z=Z1)");
        registry.createAssertion("!(Z=Z1 & X=X1)");
    }

    private static String toContent(String name,
                                    int... values) {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final int value : values) {
            if (first) {
                first = false;
            } else {
                builder.append(",");
            }
            builder.append(name + value);
        }

        return builder.toString();
    }

    public static String x(int x) {
        return "X=X" + x;
    }

    public static String x(int... x) {
        return "X in {" + toContent("X", x) + "}";
    }

    public static String y(int y) {
        return "Y=Y" + y;
    }

    public static String y(int... y) {
        return "Y in {" + toContent("Y", y) + "}";
    }

    public static String z(int z) {
        return "Z=Z" + z;
    }

    public static String z(int... z) {
        return "Z in {" + toContent("Z", z) + "}";
    }

    public static String xy(int x,
                            int y) {
        return "X=X" + x + " & Y=Y" + y;
    }

    public static String xz(int x,
                            int z) {
        return "X=X" + x + " & Z=Z" + z;
    }

    public static String yz(int y,
                            int z) {
        return "Y=Y" + y + " & Z=Z" + z;
    }

    public static String xyz(int x,
                             int y,
                             int z) {
        return "X=X" + x + " & Y=Y" + y + "& Z=Z" + z;
    }
}