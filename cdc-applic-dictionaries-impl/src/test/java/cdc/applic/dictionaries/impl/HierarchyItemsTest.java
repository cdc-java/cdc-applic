package cdc.applic.dictionaries.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.literals.Name;

class HierarchyItemsTest {
    private static final Logger LOGGER = LogManager.getLogger(HierarchyItemsTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    @Test
    void testBasicP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry()
                                          .name("r1")
                                          .prefix("Prefix")
                                          .build();
        final Type t1 = r1.booleanType().name("Type").build();
        final Property p1 = r1.property().name("P1").type("Type").ordinal(0).build();

        assertEquals(Name.of("Prefix.P1"), p1.getName());
        assertSame(t1, p1.getType());
        assertSame(p1, r1.getItem(Name.of("P1")));
        assertSame(p1, r1.getItem(Name.of("Prefix.P1")));
    }

    @Test
    void testDuplicateNamesPP() {
        // Check that we can not create duplicate item names

        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry()
                                          .name("r1")
                                          .prefix("Prefix")
                                          .build();
        r1.booleanType().name("Type").build();
        r1.property().name("P1").type("Type").ordinal(0).build();

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r1.property().name("P1").type("Type").ordinal(0).build();
                     });
    }

    @Test
    void testDuplicateNamesPA() {
        // Check that we can not create duplicate item names

        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry()
                                          .name("r1")
                                          .prefix("Prefix")
                                          .build();
        r1.booleanType().name("Type").build();
        r1.property().name("P1").type("Type").ordinal(0).build();

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r1.alias().name("P1").expression("true").build();
                     });
    }

    @Test
    void testDuplicatesInheritanceNPNP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        r1.booleanType().name("Type1").build();
        r1.property().name("P1").type("Type1").ordinal(0).build();
        final RegistryImpl r2 = repository.registry()
                                          .name("r2")
                                          .parents(r1)
                                          .build();

        // r1.P1 would not be accessible from r2
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r2.property().name("P1").type("Type1").ordinal(0).build();
                     });
    }

    @Test
    void testDuplicatesInheritanceNPP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        r1.booleanType().name("Type1").build();
        r1.property().name("P1").type("Type1").ordinal(0).build();
        final RegistryImpl r2 = repository.registry().name("r2").prefix("R2").parents(r1).build();

        // r1.P1 would not be accessible from r2
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r2.property().name("P1").type("Type1").ordinal(0).build();
                     });
    }

    @Test
    void testDuplicatesInheritancePP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        r1.booleanType().name("Type1").build();
        final Property p1 = r1.property().name("P1").type("Type1").ordinal(0).build();
        final RegistryImpl r2 = repository.registry()
                                          .name("r2")
                                          .prefix("R2")
                                          .parents(r1)
                                          .build();
        final Property p2 = r2.property().name("P1").type("R1.Type1").ordinal(0).build();

        assertSame(p1, r2.getItem(Name.of("R1.P1")));
        assertSame(p2, r2.getItem(Name.of("P1")));
        assertSame(p2, r2.getItem(Name.of("R2.P1")));

        assertEquals(1, r1.getDeclaredItems().size());
        assertEquals(1, r1.getAllItems().size());

        assertEquals(1, r2.getDeclaredItems().size());
        assertEquals(2, r2.getAllItems().size());
    }

    @Test
    void testDuplicatesInheritancePNP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R1").build();
        r1.booleanType().name("Type1").build();
        final Property p1 = r1.property().name("P1").type("Type1").ordinal(0).build();
        final RegistryImpl r2 = repository.registry().name("r2").parents(r1).build();
        final Property p2 = r2.property().name("P1").type("R1.Type1").ordinal(0).build();

        assertSame(p1, r2.getItem(Name.of("R1.P1")));
        assertSame(p2, r2.getItem(Name.of("P1")));

        assertEquals(1, r1.getDeclaredItems().size());
        assertEquals(1, r1.getAllItems().size());

        assertEquals(1, r2.getDeclaredItems().size());
        assertEquals(2, r2.getAllItems().size());
    }

    @Test
    void testCanCreateItemP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry()
                                          .name("r1")
                                          .prefix("R1")
                                          .build();
        assertTrue(r1.canCreateItem("A"));
        r1.alias().name("A").expression("true").build();
        assertFalse(r1.canCreateItem("A"));
        final RegistryImpl r2 = repository.registry()
                                          .name("r2")
                                          .prefix("R2")
                                          .build();
        assertTrue(r2.canCreateItem("A"));
        r2.alias().name("A").expression("true").build();
        assertFalse(r2.canCreateItem("A"));
        final RegistryImpl r3 = repository.registry()
                                          .name("r3")
                                          .prefix("R3")
                                          .parents(r1, r2)
                                          .build();
        assertTrue(r3.canCreateItem("A"));
        r3.alias().name("A").expression("true").build();
        assertFalse(r3.canCreateItem("A"));
    }

    @Test
    void testCanCreateItemNP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry()
                                          .name("r1")
                                          .build();
        assertTrue(r1.canCreateItem("A"));
        r1.alias().name("A").expression("true").build();
        assertFalse(r1.canCreateItem("A"));
        final RegistryImpl r2 = repository.registry()
                                          .name("r2")
                                          .build();
        assertTrue(r2.canCreateItem("A"));
        r2.alias().name("A").expression("true").build();
        assertFalse(r2.canCreateItem("A"));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         repository.registry().name("r3").parents(r1, r2).build();
                     });
    }

    @Test
    void testPoliciesNP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        final PolicyImpl p1 = r1.policy().name("p1").build();
        final PolicyImpl p11 = p1.policy().name("p11").build();
        final AliasImpl a = r1.alias().name("A").expression("true").build();

        assertSame(a, r1.getItem(Name.of("A")));
        assertSame(a, p1.getItem(Name.of("A")));
        assertSame(a, p11.getItem(Name.of("A")));
    }

    @Test
    void testPoliciesP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").prefix("R").build();
        final PolicyImpl p1 = r1.policy().name("p1").build();
        final PolicyImpl p11 = p1.policy().name("p11").build();
        final AliasImpl a = r1.alias().name("A").expression("true").build();

        repository.print(OUT);

        assertSame(a, r1.getItem(Name.of("R.A")));
        assertSame(a, r1.getItem(Name.of("A")));
        assertSame(a, p1.getItem(Name.of("R.A")));
        assertSame(a, p1.getItem(Name.of("A")));
        assertSame(a, p11.getItem(Name.of("R.A")));
        assertSame(a, p11.getItem(Name.of("A")));
    }

    @Test
    void testItemSynonymsP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("r1").prefix("R").build();
        registry.namingConvention().name("Convention1").build();
        final AliasImpl alias = registry.alias().name("A").synonym("Convention1", "a").expression("true").build();
        registry.booleanType().name("Type1").build();
        final PropertyImpl property = registry.property().name("P1").synonym("Convention1", "p1").type("Type1").ordinal(0).build();

        final PolicyImpl policy = registry.policy().name("p1").build();
        policy.setItemUsage("A", DItemUsage.OPTIONAL);
        policy.setItemUsage("P1", DItemUsage.OPTIONAL);

        repository.print(OUT);

        assertSame(alias, registry.getAlias("A"));
        assertSame(alias, registry.getAlias("R.A"));
        assertSame(property, registry.getProperty("P1"));
        assertSame(property, registry.getProperty("R.P1"));
        assertSame(alias, registry.getAlias("a"));
        assertSame(alias, registry.getAlias("R.a"));
        assertSame(property, registry.getProperty("p1"));
        assertSame(property, registry.getProperty("R.p1"));

        assertSame(alias, policy.getAlias("A"));
        assertSame(alias, policy.getAlias("R.A"));
        assertSame(property, policy.getProperty("P1"));
        assertSame(property, policy.getProperty("R.P1"));
        assertSame(alias, policy.getAlias("a"));
        assertSame(alias, policy.getAlias("R.a"));
        assertSame(property, policy.getProperty("p1"));
        assertSame(property, policy.getProperty("R.p1"));

        assertTrue(registry.hasAlias("A"));
        assertTrue(registry.hasAlias("R.A"));
        assertFalse(registry.hasAlias("P1"));
        assertFalse(registry.hasAlias("R.P1"));

        assertFalse(registry.hasProperty("A"));
        assertFalse(registry.hasProperty("R.A"));
        assertTrue(registry.hasProperty("P1"));
        assertTrue(registry.hasProperty("R.P1"));

        assertTrue(registry.hasAlias("a"));
        assertTrue(registry.hasAlias("R.a"));
        assertFalse(registry.hasAlias("p1"));
        assertFalse(registry.hasAlias("R.p1"));

        assertFalse(registry.hasProperty("a1"));
        assertFalse(registry.hasProperty("R.a1"));
        assertTrue(registry.hasProperty("p1"));
        assertTrue(registry.hasProperty("R.p1"));

        assertTrue(policy.hasAlias("A"));
        assertTrue(policy.hasAlias("R.A"));
        assertFalse(policy.hasAlias("P1"));
        assertFalse(policy.hasAlias("R.P1"));

        assertFalse(policy.hasProperty("A"));
        assertFalse(policy.hasProperty("R.A"));
        assertTrue(policy.hasProperty("P1"));
        assertTrue(policy.hasProperty("R.P1"));

        assertTrue(policy.hasAlias("a"));
        assertTrue(policy.hasAlias("R.a"));
        assertFalse(policy.hasAlias("p1"));
        assertFalse(policy.hasAlias("R.p1"));

        assertFalse(policy.hasProperty("a1"));
        assertFalse(policy.hasProperty("R.a1"));
        assertTrue(policy.hasProperty("p1"));
        assertTrue(policy.hasProperty("R.p1"));
    }
}