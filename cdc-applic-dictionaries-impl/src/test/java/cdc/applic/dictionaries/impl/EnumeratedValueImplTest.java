package cdc.applic.dictionaries.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.StringValue;

class EnumeratedValueImplTest {
    @Test
    void testConstruction() {
        final EnumeratedValueImpl v = EnumeratedValueImpl.builder().literal("A").build();
        assertEquals(0, v.getOrdinal());
        assertEquals(StringValue.of("A"), v.getLiteral());
        assertEquals(StringValue.of("A"), v.getShortLiteral());
    }

    @Test
    void testEquals() {
        final EnumeratedValueImpl v0 = EnumeratedValueImpl.builder().literal("B").build();
        final EnumeratedValueImpl v1 = EnumeratedValueImpl.builder().literal("A").build();
        final EnumeratedValueImpl v2 = EnumeratedValueImpl.builder().literal("A").build();
        final EnumeratedValueImpl v3 = EnumeratedValueImpl.builder().literal("A").shortLiteral("a").build();
        final EnumeratedValueImpl v4 = EnumeratedValueImpl.builder().literal("A").shortLiteral("a").ordinal(1).build();
        assertEquals(v1, v1);
        assertEquals(v1, v2);
        assertNotEquals(v0, v1);
        assertNotEquals(v1, v3);
        assertNotEquals(v3, v4);
        assertNotEquals(v0, "hello");
    }

    @Test
    void testHashCode() {
        final EnumeratedValueImpl v0 = EnumeratedValueImpl.builder().literal("B").build();
        assertEquals(v0.hashCode(), v0.hashCode());
    }
}