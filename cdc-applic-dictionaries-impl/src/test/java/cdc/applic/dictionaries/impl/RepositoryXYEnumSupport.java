package cdc.applic.dictionaries.impl;

import cdc.applic.dictionaries.handles.DictionaryHandle;

/**
 * Test data with 2 Enumerated properties.
 * <ul>
 * <li>X : {X1, X2, X3}
 * <li>Y : {Y1, Y2}
 * </ul>
 * Allowed cells are:
 * <pre>
 *   Y
 * R | 0  1  1  1
 * 2 | 0  1  1  1
 * 1 | 0  0  1  1
 *   +----------- X
 *     1  2  3  R
 * </pre>
 * This is modeled with these assertions:
 * <ul>
 * <li>X!=X1
 * <li>!(X=X2 & Y=Y1)
 * </ul>
 *
 * @author Damien Carbonne
 */
public class RepositoryXYEnumSupport {
    public final RepositoryImpl repository = new RepositoryImpl();
    public final RegistryImpl registry = repository.registry().name("Registry").build();
    public final DictionaryHandle registryHandle = new DictionaryHandle(registry);

    public static final String X = "X";
    public static final String Y = "Y";

    public RepositoryXYEnumSupport() {
        registry.enumeratedType().name("X").frozen(false).literals("X1", "X2", "X3").build();
        registry.enumeratedType().name("Y").frozen(false).literals("Y1", "Y2").build();

        registry.property().name("X").type("X").ordinal(0).build();
        registry.property().name("Y").type("Y").ordinal(1).build();

        registry.createAssertion("X!=X1");
        registry.createAssertion("!(X=X2 & Y=Y1)");
    }

    public static String toContent(String name,
                                   int... values) {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final int value : values) {
            if (first) {
                first = false;
            } else {
                builder.append(",");
            }
            builder.append(name + value);
        }

        return builder.toString();
    }

    public static boolean filled(int x,
                                 int y,
                                 boolean withReserve) {
        if (withReserve) {
            return x >= 2 && x <= 4
                    && y >= 1 && y <= 3
                    && !(x == 2 && y == 1);

        } else {
            return x >= 1 && x <= 3
                    && y >= 1 && y <= 2;
        }
    }

    public static String x(int x) {
        return "X=X" + x;
    }

    public static String x(int... x) {
        return "X in {" + toContent("X", x) + "}";
    }

    public static String y(int y) {
        return "Y=Y" + y;
    }

    public static String y(int... y) {
        return "Y in {" + toContent("Y", y) + "}";
    }

    public static String xy(int x,
                            int y) {
        return "X=X" + x + " & Y=Y" + y;
    }
}