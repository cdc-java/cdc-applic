package cdc.applic.dictionaries.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.Assertion;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.RealType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.RealRange;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.util.function.IterableUtils;
import cdc.util.lang.CollectionUtils;
import cdc.util.paths.Path;

class RepositoryImplTest {
    private static Path p(String path) {
        return new Path(path);
    }

    @Test
    void testBooleanType() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        assertTrue(registry.getDeclaredTypes().isEmpty());
        final BooleanType b = registry.booleanType().name("B").build();
        assertEquals(1, registry.getDeclaredTypes().size());
        assertEquals(Name.of("B"), b.getName());
        assertEquals("false, true", b.getDefinition());
        assertTrue(b.isCompliant(BooleanValue.TRUE));
        assertTrue(b.isCompliant((SItem) BooleanValue.FALSE));
        assertFalse(b.isCompliant(IntegerValue.of(1)));
        assertEquals(BooleanSet.FALSE_TRUE, b.getDomain());
    }

    @Test
    void testIntegerType() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        assertTrue(registry.getDeclaredTypes().isEmpty());
        final IntegerType i1 = registry.integerType().name("I1").frozen(false).domain("1~10").build();
        final IntegerType i2 = registry.integerType().name(SName.of("I2")).frozen(true).domain(IntegerSet.of("1~10")).build();
        assertEquals(2, registry.getDeclaredTypes().size());
        assertEquals(Name.of("I1"), i1.getName());
        assertFalse(i1.isFrozen());
        assertEquals("1~10", i1.getDefinition());
        assertTrue(i1.isCompliant(IntegerValue.of(10)));
        assertTrue(i1.isCompliant((SItem) IntegerRange.of(1, 10)));
        assertFalse(i1.isCompliant(RealValue.of(1.0)));
        assertEquals(Name.of("I2"), i2.getName());
        assertTrue(i2.isFrozen());
        assertEquals("1~10", i2.getDefinition());
        assertEquals(i1, registry.getType(Name.of("I1")));
        assertEquals(IntegerSet.of("1~10"), i1.getDomain());
    }

    @Test
    void testRealType() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        assertTrue(registry.getDeclaredTypes().isEmpty());
        final RealType r1 = registry.realType().name("R1").frozen(false).domain("1.0~10.0").build();
        final RealType r2 = registry.realType().name(SName.of("R2")).frozen(true).domain(RealSet.of("1.0~10.0")).build();
        assertEquals(2, registry.getDeclaredTypes().size());
        assertEquals(Name.of("R1"), r1.getName());
        assertFalse(r1.isFrozen());
        assertEquals("1.0~10.0", r1.getDefinition());
        assertTrue(r1.isCompliant(RealValue.of(10.0)));
        assertTrue(r1.isCompliant((SItem) RealRange.of(1.0, 10.0)));
        assertFalse(r1.isCompliant(IntegerValue.of(1)));
        assertEquals(Name.of("R2"), r2.getName());
        assertTrue(r2.isFrozen());
        assertEquals("1.0~10.0", r2.getDefinition());
        assertEquals(RealSet.of("1.0~ 10.0"), r1.getDomain());
    }

    @Test
    void testPatternType() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        assertTrue(registry.getDeclaredTypes().isEmpty());
        final PatternType p = registry.patternType().name("P").frozen(false).pattern("[A-Z]{2,5}").build();
        assertEquals(1, registry.getDeclaredTypes().size());
        assertEquals(Name.of("P"), p.getName());
        assertFalse(p.isFrozen());
        assertEquals("[A-Z]{2,5}", p.getDefinition());
        assertEquals("[A-Z]{2,5}", p.getPattern().pattern());
        assertTrue(p.isCompliant(StringValue.of("AA", false)));
        assertTrue(p.isCompliant((SItem) StringValue.of("AA", false)));
        assertFalse(p.isCompliant((SItem) StringValue.of("A", false)));
        assertFalse(p.isCompliant(IntegerValue.of(10)));
    }

    @Test
    void testEnumeratedType() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        assertTrue(registry.getDeclaredTypes().isEmpty());

        final EnumeratedType e0 = registry.enumeratedType()
                                          .name("E0")
                                          .frozen(false)
                                          .literals("A", "B")
                                          .build();
        final EnumeratedType e1 = registry.enumeratedType()
                                          .name("E1")
                                          .frozen(false)
                                          .literals("A")
                                          .build();
        final EnumeratedType e2 = registry.enumeratedType()
                                          .name("E2")
                                          .frozen(true)
                                          .literals("A")
                                          .build();
        assertEquals(3, registry.getDeclaredTypes().size());
        assertFalse(e1.isFrozen());
        assertTrue(e2.isFrozen());
        assertEquals(Name.of("E1"), e1.getName());
        assertEquals(Name.of("E2"), e2.getName());
        assertEquals("A", e1.getDefinition());
        assertEquals("A, B", e0.getDefinition());
        assertEquals(StringSet.of("A,B"), e0.getDomain());
        assertEquals(EnumeratedValueImpl.builder().literal("A").build(), e0.getValue(StringValue.of("A", false)));
        assertEquals(Arrays.asList(EnumeratedValueImpl.builder().literal("A").ordinal(0).build(),
                                   EnumeratedValueImpl.builder().literal("B").ordinal(1).build()),
                     e0.getValues());

        assertTrue(e0.isCompliant(StringValue.of("A")));

        assertFalse(e0.isCompliant(StringValue.of("C")));
        assertTrue(e0.isCompliant((SItem) StringValue.of("A")));
        assertFalse(e0.isCompliant(IntegerValue.of(10)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         registry.enumeratedType().name("E1").frozen(false).literals().build();
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         registry.enumeratedType()
                                 .name("E3")
                                 .frozen(false)
                                 .literals("A", "A")
                                 .build();
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         registry.enumeratedType()
                                 .name("E4")
                                 .frozen(false)
                                 .value()
                                 .literal("A")
                                 .back()
                                 .value()
                                 .literal("B")
                                 .shortLiteral("A")
                                 .back()
                                 .build();
                     });
    }

    @Test
    void testRegistry() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry1 = repository.registry().name("R1").build();
        final RegistryImpl registry2 = repository.registry().name("R2").build();
        assertThrows(IllegalArgumentException.class, () -> {
            repository.registry().name("R2").build();
        });
        assertEquals(registry1, repository.getRegistry("/R1"));
        assertEquals(IterableUtils.toSet(registry1.getDeclaredItems()),
                     IterableUtils.toSet(registry1.getAllowedItems()));
        registry1.booleanType().name("B").build();
        registry2.booleanType().name("B").build();
        final Alias a = registry1.alias().name("A").expression("A").build();
        final Property p = registry1.property().name("B").type("B").ordinal(0).build();
        final Property p2 = registry2.property().name("B").type("B").ordinal(0).build();
        assertEquals(a, registry1.getItem(Name.of("A")));
        assertEquals(p, registry1.getItem(Name.of("B")));

        assertEquals(repository, registry1.getOwner());
        assertTrue(IterableUtils.isEmpty(registry1.getWritingRuleNames()));

        assertEquals(a, registry1.getOptionalAllowedItem(Name.of("A")).orElse(null));
        assertEquals(null, registry1.getOptionalAllowedItem(Name.of("Z")).orElse(null));

        assertEquals(IterableUtils.toSet(registry1.getDeclaredItems()),
                     IterableUtils.toSet(registry1.getAllowedItems(DItemUsage.OPTIONAL)));
        assertTrue(IterableUtils.isEmpty(registry1.getAllowedItems(DItemUsage.RECOMMENDED)));
        assertTrue(IterableUtils.isEmpty(registry1.getAllowedItems(DItemUsage.MANDATORY)));

        assertTrue(registry1.hasItem(Name.of("A")));
        assertTrue(registry1.hasAllowedItem(Name.of("A")));
        assertFalse(registry1.hasItem(Name.of("Z")));
        assertFalse(registry1.hasAllowedItem(Name.of("Z")));
        assertTrue(registry1.isDeclared(a));
        assertTrue(registry1.isAllowed(a));
        assertFalse(registry1.isDeclared(p2));
        assertFalse(registry1.isAllowed(p2));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         registry1.getAllowedItems(DItemUsage.FORBIDDEN);
                     });
    }

    @Test
    void testPolicy() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry1 = repository.registry().name("R1").build();
        final RegistryImpl registry2 = repository.registry().name("R2").build();
        final PolicyImpl policy1P1 = registry1.policy().name("P1").build();
        final PolicyImpl policy1P11 = policy1P1.policy().name("P11").build();
        final PolicyImpl policy2P1 = registry2.policy().name("P1").build();

        assertEquals(policy1P1, repository.getDictionary("/R1/P1"));
        assertEquals(policy2P1, repository.getDictionary("/R2/P1"));
        assertEquals(registry1, policy1P1.getOwner());
        assertEquals(1, registry1.getChildren().size());
        assertTrue(IterableUtils.isEmpty(policy1P1.getAllowedItems()));

        registry1.booleanType().name("B").build();
        registry2.booleanType().name("B").build();
        final Alias a = registry1.alias().name("A").expression("A").build();
        registry1.property().name("B").type("B").ordinal(0).build();
        registry2.property().name("B").type("B").ordinal(0).build();

        policy1P1.setItemUsage(Name.of("A"), DItemUsage.MANDATORY);
        assertEquals(DItemUsage.MANDATORY, policy1P1.getEffectiveItemUsage(Name.of("A")));
        assertEquals(a, policy1P1.getOptionalAllowedItem(Name.of("A")).orElse(null));
        assertEquals(DItemUsage.FORBIDDEN, policy1P1.getEffectiveItemUsage(Name.of("B")));
        assertEquals(null, policy1P1.getOptionalAllowedItem(Name.of("B")).orElse(null));

        policy1P1.setItemUsage(Name.of("A"), DItemUsage.MANDATORY);
        assertEquals(CollectionUtils.toSet(a), IterableUtils.toSet(policy1P1.getAllowedItems(DItemUsage.MANDATORY)));
        assertEquals(CollectionUtils.toSet(), IterableUtils.toSet(policy1P1.getAllowedItems(DItemUsage.RECOMMENDED)));
        assertEquals(CollectionUtils.toSet(), IterableUtils.toSet(policy1P1.getAllowedItems(DItemUsage.OPTIONAL)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         policy1P1.getAllowedItems(DItemUsage.FORBIDDEN);
                     });

        policy1P1.setItemUsage(Name.of("A"), DItemUsage.RECOMMENDED);
        assertEquals(CollectionUtils.toSet(), IterableUtils.toSet(policy1P1.getAllowedItems(DItemUsage.MANDATORY)));
        assertEquals(CollectionUtils.toSet(a), IterableUtils.toSet(policy1P1.getAllowedItems(DItemUsage.RECOMMENDED)));
        assertEquals(CollectionUtils.toSet(), IterableUtils.toSet(policy1P1.getAllowedItems(DItemUsage.OPTIONAL)));
        policy1P1.setItemUsage(Name.of("A"), DItemUsage.OPTIONAL);
        assertEquals(CollectionUtils.toSet(), IterableUtils.toSet(policy1P1.getAllowedItems(DItemUsage.MANDATORY)));
        assertEquals(CollectionUtils.toSet(), IterableUtils.toSet(policy1P1.getAllowedItems(DItemUsage.RECOMMENDED)));
        assertEquals(CollectionUtils.toSet(a), IterableUtils.toSet(policy1P1.getAllowedItems(DItemUsage.OPTIONAL)));
        policy1P1.setItemUsage(Name.of("A"), DItemUsage.FORBIDDEN);
        policy1P1.setItemUsage(Name.of("A"), DItemUsage.FORBIDDEN);
        assertEquals(CollectionUtils.toSet(), IterableUtils.toSet(policy1P1.getAllowedItems(DItemUsage.MANDATORY)));
        assertEquals(CollectionUtils.toSet(), IterableUtils.toSet(policy1P1.getAllowedItems(DItemUsage.RECOMMENDED)));
        assertEquals(CollectionUtils.toSet(), IterableUtils.toSet(policy1P1.getAllowedItems(DItemUsage.OPTIONAL)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         policy1P11.setItemUsage(Name.of("A"), DItemUsage.MANDATORY);
                     });

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         policy1P1.setItemUsage(Name.of("Z"), DItemUsage.MANDATORY);
                     });

        policy1P1.setItemUsage(Name.of("A"), DItemUsage.MANDATORY);
        policy1P11.setItemUsage(Name.of("A"), DItemUsage.MANDATORY);
        // assertThrows(IllegalArgumentException.class,
        // () -> {
        // policy1P11.setItemUsage(Name.of("A"), DItemUsage.OPTIONAL);
        // });
        // assertThrows(IllegalArgumentException.class,
        // () -> {
        // policy1P11.setItemUsage(Name.of("A"), DItemUsage.RECOMMENDED);
        // });
        // assertThrows(IllegalArgumentException.class,
        // () -> {
        // policy1P11.setItemUsage(Name.of("A"), DItemUsage.FORBIDDEN);
        // });
    }

    @Test
    void testPaths() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("R1").build();
        final RegistryImpl r2 = repository.registry().name("R2").build();
        final PolicyImpl p11 = r1.policy().name("P1").build();
        final PolicyImpl p12 = r1.policy().name("P2").build();
        final PolicyImpl p21 = r2.policy().name("P1").build();
        final PolicyImpl p22 = r2.policy().name("P2").build();

        assertEquals(p("/R1"), r1.getPath());
        assertEquals(p("/R1/P1"), p11.getPath());
        assertSame(p11.getOwner(), p11.getDictionary(p("..")));
        assertSame(p11, p11.getDictionary(p(".")));
        assertSame(p11, r1.getDictionary(p("P1")));
        assertSame(p11, p12.getDictionary(p("../P1")));
        assertSame(r2, p22.getDictionary(p("/R2")));
        assertSame(r2, p21.getDictionary(p("/R2")));
    }

    @Test
    void testAssertions() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("R1").build();
        final PolicyImpl p1 = r1.policy().name("P1").build();
        assertEquals(0, IterableUtils.size(r1.getAllAssertions()));
        assertEquals(0, IterableUtils.size(p1.getAllAssertions()));

        final Assertion a1 = r1.createAssertion("A1");
        assertEquals("A1", a1.getExpression().getContent());
        assertEquals(1, IterableUtils.size(r1.getAllAssertions()));
        assertEquals(1, IterableUtils.size(p1.getAllAssertions()));

        p1.createAssertion("B1");
        assertEquals(1, IterableUtils.size(r1.getAllAssertions()));
        assertEquals(2, IterableUtils.size(p1.getAllAssertions()));

        final PolicyImpl p11 = p1.policy().name("P11").build();
        assertEquals(2, IterableUtils.size(p11.getAllAssertions()));
    }

    @Test
    void testAliases() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("R1").build();
        final Alias a = registry.alias().name("A").expression("A").build();
        assertEquals("A", a.getExpression().getContent());
    }

    @Test
    void testProperties() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("R1").build();
        final Type b = registry.booleanType().name("B").build();
        final Property p = registry.property().name("B").type("B").ordinal(0).build();
        assertEquals(Name.of("B"), p.getName());
        assertEquals(b, p.getType());
        assertEquals(0, p.getOrdinal());
    }
}