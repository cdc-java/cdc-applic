package cdc.applic.dictionaries.impl.bindings;

import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;

/**
 * IntegerType/EnumeratedType binding implementation.
 *
 * @author Damien Carbonne
 */
public class IntegerEnumeratedBindingImpl
        extends AbstractExtensionTypesBinding<IntegerType, IntegerValue, IntegerSet, EnumeratedType, StringValue, StringSet> {

    protected IntegerEnumeratedBindingImpl(Builder builder) {
        super(builder,
              IntegerValue.class,
              StringValue.class,
              IntegerSet::of,
              StringSet::of);
    }

    static Builder builder(DictionariesBindingImpl owner) {
        return new Builder(owner);
    }

    public static class Builder extends
            AbstractExtensionTypesBinding.Builder<Builder, IntegerType, IntegerValue, EnumeratedType, StringValue> {

        protected Builder(DictionariesBindingImpl owner) {
            super(owner,
                  IntegerType.class,
                  EnumeratedType.class);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public Builder map(String source,
                           String target) {
            return map(IntegerValue.of(source), StringValue.of(target));
        }

        public Builder map(int source,
                           String target) {
            return map(IntegerValue.of(source), StringValue.of(target));
        }

        @Override
        public IntegerEnumeratedBindingImpl build() {
            return owner.addTypesBinding(new IntegerEnumeratedBindingImpl(this));
        }
    }
}