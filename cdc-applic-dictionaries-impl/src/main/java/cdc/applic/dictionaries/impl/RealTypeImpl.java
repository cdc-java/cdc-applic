package cdc.applic.dictionaries.impl;

import java.util.Objects;
import java.util.Optional;

import cdc.applic.dictionaries.types.RealType;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.RealValue;
import cdc.util.lang.Checks;

public class RealTypeImpl extends AbstractTypeImpl implements RealType {
    private final boolean frozen;
    private final RealSet domain;
    private final Optional<RealValue> defaultValue;

    protected RealTypeImpl(Builder builder) {
        super(builder);
        this.frozen = builder.frozen;
        this.domain = builder.domain;
        this.defaultValue = Optional.ofNullable(builder.defaultValue);

        // Check compliance of default value
        if (defaultValue.isPresent()) {
            Checks.isTrue(isCompliant(defaultValue.get()),
                          "Default value {} is not compliant with {}",
                          this.defaultValue.get(),
                          getDefinition());
        }
    }

    @Override
    public boolean isFrozen() {
        return frozen;
    }

    @Override
    public RealSet getDomain() {
        return domain;
    }

    @Override
    public Optional<RealValue> getDefaultValue() {
        return defaultValue;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(),
                            getDescription(),
                            getS1000DProductIdentifier(),
                            getS1000DPropertyType(),
                            isFrozen(),
                            getDomain());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof RealTypeImpl)) {
            return false;
        }
        final RealTypeImpl other = (RealTypeImpl) object;
        return Objects.equals(getName(), other.getName())
                && Objects.equals(getDescription(), other.getDescription())
                && getS1000DProductIdentifier() == other.getS1000DProductIdentifier()
                && getS1000DPropertyType() == other.getS1000DPropertyType()
                && isFrozen() == other.isFrozen()
                && Objects.equals(getDomain(), other.getDomain());
    }

    static Builder builder(RegistryImpl registry) {
        return new Builder(registry);

    }

    public static class Builder extends AbstractTypeImpl.Builder<Builder> {
        protected boolean frozen = false;
        protected RealSet domain;
        protected RealValue defaultValue = null;

        protected Builder(RegistryImpl registry) {
            super(registry);
        }

        @Override
        public Builder self() {
            return this;
        }

        public Builder frozen(boolean frozen) {
            this.frozen = frozen;
            return self();
        }

        public Builder domain(RealSet domain) {
            this.domain = domain;
            return self();
        }

        public Builder domain(String content) {
            return domain(RealSet.of(content));
        }

        public Builder defaultValue(RealValue defaultValue) {
            this.defaultValue = defaultValue;
            return self();
        }

        public Builder defaultValue(double defaultValue) {
            return defaultValue(RealValue.of(defaultValue));
        }

        @Override
        public RealTypeImpl build() {
            return registry.types.addType(new RealTypeImpl(this));
        }
    }
}