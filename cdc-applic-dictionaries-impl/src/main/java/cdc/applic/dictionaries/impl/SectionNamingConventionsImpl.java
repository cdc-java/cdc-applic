package cdc.applic.dictionaries.impl;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import cdc.applic.dictionaries.DictionaryMembership;
import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.util.debug.Printables;
import cdc.util.debug.Verbosity;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;

public class SectionNamingConventionsImpl implements SectionNamingConventions {
    private static final String CONVENTION = "convention";
    private static final String NAME = "name";

    private final RegistryImpl registry;
    /** Locally declared naming conventions. */
    private final Set<NamingConventionImpl> declared = new HashSet<>();
    /** All usable naming conventions. */
    private final Set<NamingConvention> all = new HashSet<>();
    /** Map from name (local without prefix, local with prefix, inherited with prefix) to naming convention. */
    private final Map<Name, NamingConvention> map = new HashMap<>();

    SectionNamingConventionsImpl(RegistryImpl registry) {
        this.registry = registry;
        this.all.add(NamingConvention.DEFAULT);
    }

    /**
     * Collects and inherits all types from parent registries.
     * <p>
     * This <em>MUST</em> be called when a registry is created.
     */
    void build() {
        // There is no descendant at the time of call
        // This does not matter
        addInheritedConventions();
    }

    void refreshConventions() {
        addInheritedConventions();
    }

    private void addInheritedConventions() {
        // Iterate on all descendants, excluding itself
        for (final AbstractDictionaryImpl descendant : registry.structure.getSortedDescendants(true)) {
            if (descendant instanceof RegistryImpl) {
                // Iterate on all ancestors of descendant.
                for (final AbstractDictionaryImpl ancestor : descendant.structure.getSortedAncestors(false)) {
                    if (ancestor instanceof RegistryImpl) {
                        for (final NamingConventionImpl convention : ((RegistryImpl) ancestor).getDeclaredNamingConventions()) {
                            ((RegistryImpl) descendant).conventions.addInherited(convention);
                        }
                    }
                }
            }
        }
    }

    private void addLocally(NamingConventionImpl convention) {
        // No declared type with same local or qualified name
        Checks.doesNotContainKey(map, convention.getName().removePrefix(), registry.wrapName() + ".map no prefix");
        Checks.doesNotContainKey(map, convention.getName(), registry.wrapName() + ".map with prefix");

        map.put(convention.getName().removePrefix(), convention);
        map.put(convention.getName(), convention);
        declared.add(convention);
        all.add(convention);

        registry.newCachesSerial(false);
    }

    private void addInherited(NamingConventionImpl convention) {
        // It is possible an inherited type has already been added
        if (!all.contains(convention)) {
            Checks.doesNotContainKey(map, convention.getName(), registry.wrapName() + ".map with prefix");

            map.put(convention.getName(), convention);
            all.add(convention);

            registry.newCachesSerial(false);
        }
    }

    NamingConventionImpl addConvention(NamingConventionImpl convention) {
        Checks.isNotNull(convention, CONVENTION);

        // Local processing
        addLocally(convention);

        // Inheritance
        for (final AbstractDictionaryImpl descendant : registry.structure.getSortedDescendants(false)) {
            if (descendant instanceof RegistryImpl) {
                ((RegistryImpl) descendant).conventions.addInherited(convention);
            } else {
                descendant.newCachesSerial(false);
            }
        }

        return convention;
    }

    @Override
    public boolean canCreateNamingConvention(SName name) {
        if (name != null) {
            final Name n = Name.of(registry.getPrefix(), name);
            // Check locally
            if (map.containsKey(n) || map.containsKey(n.removePrefix())) {
                return false;
            }
            // Check descendants
            for (final AbstractDictionaryImpl descendant : registry.structure.getSortedDescendants(false)) {
                // Naming conventions can only be created on registries
                if (descendant instanceof RegistryImpl && ((RegistryImpl) descendant).conventions.map.containsKey(n)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Set<NamingConventionImpl> getDeclaredNamingConventions() {
        return declared;
    }

    @Override
    public Set<NamingConvention> getAllNamingConventions() {
        return all;
    }

    @Override
    public DictionaryMembership getMembership(NamingConvention convention) {
        Checks.isNotNull(convention, CONVENTION);

        if (declared.contains(convention)) {
            return DictionaryMembership.LOCAL;
        } else if (all.contains(convention)) {
            return DictionaryMembership.INHERITED;
        } else {
            return DictionaryMembership.UNRELATED;
        }
    }

    @Override
    public Optional<NamingConvention> getOptionalNamingConvention(Name name) {
        Checks.isNotNull(name, NAME);

        final NamingConvention convention = map.get(name);
        if (convention == null && NamingConvention.DEFAULT_NAME.equals(name)) {
            return Optional.of(NamingConvention.DEFAULT);
        } else {
            return Optional.ofNullable(convention);
        }
    }

    @Override
    public NamingConvention getNamingConvention(Name name) {
        return getOptionalNamingConvention(name).orElseThrow(() -> new NoSuchElementException("No convention named '" + name
                + "' found in "
                + registry.wrapName()));
    }

    @Override
    public NamingConventionImpl.Builder namingConvention() {
        return NamingConventionImpl.builder(registry);
    }

    @Override
    public void printNamingConventions(PrintStream out,
                                       int level,
                                       Verbosity verbosity) {
        Printables.indent(out, level);
        out.println("Naming Conventions (" + getAllNamingConventions().size() + ")");

        if (verbosity != Verbosity.ESSENTIAL) {
            for (final NamingConvention convention : getAllNamingConventions()) {
                Printables.indent(out, level + 1);
                out.print(getMembership(convention) + " ");
                out.print(convention.getName());
                out.println();
            }
            Printables.indent(out, level + 1);
            out.println("Map (" + map.size() + ")");
            for (final Name name : IterableUtils.toSortedList(map.keySet())) {
                Printables.indent(out, level + 2);
                out.println(name + " -> " + map.get(name));
            }
        }
    }
}