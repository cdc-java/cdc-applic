package cdc.applic.dictionaries.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import cdc.applic.dictionaries.Description;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;
import cdc.util.strings.StringUtils;

/**
 * Implementation of {@link Description}.
 *
 * @author Damien Carbonne
 */
public class DescriptionImpl implements Description, DescriptionSetter<DescriptionImpl> {
    private Map<Locale, String> map;

    public DescriptionImpl() {
        super();
    }

    protected DescriptionImpl(Builder builder) {
        if (builder.map.isEmpty()) {
            this.map = null;
        } else {
            this.map = builder.map;
        }
    }

    @Override
    public Set<Locale> getLocales() {
        return map == null ? Collections.emptySet() : map.keySet();
    }

    @Override
    public String getContent(Locale locale) {
        Checks.isNotNull(locale, "locale");
        return map == null ? null : map.get(locale);
    }

    public DescriptionImpl clear() {
        map = null;
        return this;
    }

    @Override
    public DescriptionImpl description(Locale locale,
                                       String content) {
        Checks.isNotNull(locale, "locale");

        if (content == null) {
            if (map != null) {
                map.remove(locale);
                if (map.isEmpty()) {
                    map = null;
                }
            }
        } else {
            if (map == null) {
                map = new HashMap<>();
            }
            map.put(locale, content);
        }
        return this;
    }

    public DescriptionImpl set(DescriptionImpl other) {
        clear();
        for (final Locale locale : other.getLocales()) {
            description(locale, other.getContent(locale));
        }
        return this;
    }

    @Override
    public int hashCode() {
        return map == null ? 0 : map.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof DescriptionImpl)) {
            return false;
        }
        final DescriptionImpl other = (DescriptionImpl) object;
        return Objects.equals(map, other.map);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append('[');
        for (final Locale locale : IterableUtils.toSortedList(getLocales(), LOCALE_LANGUAGE_COMPARATOR)) {
            builder.append('[')
                   .append(locale.toLanguageTag())
                   .append(' ')
                   .append(StringUtils.extract(getContent(locale), 10))
                   .append(']');
        }
        builder.append(']');
        return builder.toString();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements DescriptionSetter<Builder> {
        private final Map<Locale, String> map = new HashMap<>();

        protected Builder() {
        }

        @Override
        public Builder description(Locale locale,
                                   String content) {
            if (content != null) {
                map.put(locale, content);
            }
            return this;
        }

        public DescriptionImpl build() {
            return new DescriptionImpl(this);
        }
    }
}