package cdc.applic.dictionaries.impl;

import java.util.Locale;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.util.lang.Checks;

public class AliasImpl extends AbstractDExpressedImpl implements Alias {
    private final RefSynImpl<Name> names;
    private final int ordinal;
    private final DescriptionImpl description;

    protected AliasImpl(Builder builder) {
        super(builder);
        this.names = Checks.isNotNull(builder.names, "names");
        this.ordinal = builder.ordinal;
        this.description = builder.description.build();
    }

    @Override
    public String getDesignation() {
        return "Alias " + getName().getProtectedLiteral();
    }

    @Override
    public RefSynImpl<Name> getNames() {
        return names;
    }

    public AliasImpl setExpression(Expression expression) {
        setExpressionInt(expression);
        dictionary.newCachesSerial(true);
        return this;
    }

    @Override
    public int getOrdinal() {
        return ordinal;
    }

    @Override
    public DescriptionImpl getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Alias " + getName() + "=[" + getExpression() + "]";
    }

    protected static Builder builder(RegistryImpl registry) {
        return new Builder(registry);
    }

    public static class Builder extends AbstractDExpressedImpl.Builder<Builder>
            implements NameSetter<Builder>, SynonymSetter<SName, Builder>, DescriptionSetter<Builder> {
        private RefSynImpl<Name> names;
        private int ordinal;
        private final DescriptionImpl.Builder description = DescriptionImpl.builder();

        protected Builder(RegistryImpl registry) {
            super(registry);
        }

        @Override
        public Builder self() {
            return this;
        }

        @Override
        public Builder name(SName name) {
            this.names = new RefSynImpl<>(Name.of(dictionary.getPrefix(), name));
            return self();
        }

        @Override
        public Builder synonym(NamingConvention convention,
                               SName synonym) {
            names.addSynonym(Name.of(dictionary.getPrefix(), synonym), convention);
            return this;
        }

        @Override
        public Builder synonym(String convention,
                               String synonym) {
            return synonym(dictionary.getRegistry().getNamingConvention(convention),
                           SName.of(synonym));
        }

        public Builder ordinal(int ordinal) {
            this.ordinal = ordinal;
            return this;
        }

        @Override
        public Builder description(Locale locale,
                                   String content) {
            Checks.isNotNull(locale, "locale");
            description.description(locale, content);
            return this;
        }

        public AliasImpl build() {
            return dictionary.items.addItemInt(new AliasImpl(this));
        }

        public RegistryImpl back() {
            build();
            return (RegistryImpl) dictionary;
        }
    }
}