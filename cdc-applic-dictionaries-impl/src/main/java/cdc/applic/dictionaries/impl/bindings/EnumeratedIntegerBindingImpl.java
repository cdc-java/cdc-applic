package cdc.applic.dictionaries.impl.bindings;

import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;

/**
 * EnumeratedType/IntegerType binding implementation.
 *
 * @author Damien Carbonne
 */
public class EnumeratedIntegerBindingImpl
        extends AbstractExtensionTypesBinding<EnumeratedType, StringValue, StringSet, IntegerType, IntegerValue, IntegerSet> {

    protected EnumeratedIntegerBindingImpl(Builder builder) {
        super(builder,
              StringValue.class,
              IntegerValue.class,
              StringSet::of,
              IntegerSet::of);
    }

    static Builder builder(DictionariesBindingImpl owner) {
        return new Builder(owner);
    }

    public static class Builder extends
            AbstractExtensionTypesBinding.Builder<Builder, EnumeratedType, StringValue, IntegerType, IntegerValue> {

        protected Builder(DictionariesBindingImpl owner) {
            super(owner,
                  EnumeratedType.class,
                  IntegerType.class);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public Builder map(String source,
                           String target) {
            return map(StringValue.of(source), IntegerValue.of(target));
        }

        public Builder map(String source,
                           int target) {
            return map(StringValue.of(source), IntegerValue.of(target));
        }

        @Override
        public EnumeratedIntegerBindingImpl build() {
            return owner.addTypesBinding(new EnumeratedIntegerBindingImpl(this));
        }
    }
}