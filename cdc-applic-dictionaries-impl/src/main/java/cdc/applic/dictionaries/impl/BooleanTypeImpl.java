package cdc.applic.dictionaries.impl;

import java.util.Objects;
import java.util.Optional;

import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;

public class BooleanTypeImpl extends AbstractTypeImpl implements BooleanType {
    private final Optional<BooleanValue> defaultValue;

    protected BooleanTypeImpl(Builder builder) {
        super(builder);
        this.defaultValue = Optional.ofNullable(builder.defaultValue);
        // Default value is necessarily compliant with boolean domain
    }

    @Override
    public BooleanSet getDomain() {
        return BooleanSet.FALSE_TRUE;
    }

    @Override
    public Optional<BooleanValue> getDefaultValue() {
        return defaultValue;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(),
                            getDescription(),
                            getS1000DProductIdentifier(),
                            getS1000DPropertyType());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof BooleanTypeImpl)) {
            return false;
        }
        final BooleanTypeImpl other = (BooleanTypeImpl) object;
        return Objects.equals(getName(), other.getName())
                && Objects.equals(getDescription(), other.getDescription())
                && getS1000DProductIdentifier() == other.getS1000DProductIdentifier()
                && getS1000DPropertyType() == other.getS1000DPropertyType();
    }

    static Builder builder(RegistryImpl registry) {
        return new Builder(registry);

    }

    public static class Builder extends AbstractTypeImpl.Builder<Builder> {
        protected BooleanValue defaultValue = null;

        protected Builder(RegistryImpl registry) {
            super(registry);
        }

        @Override
        public Builder self() {
            return this;
        }

        public Builder defaultValue(BooleanValue defaultValue) {
            this.defaultValue = defaultValue;
            return self();
        }

        public Builder defaultValue(boolean defaultValue) {
            return defaultValue(BooleanValue.of(defaultValue));
        }

        @Override
        public BooleanTypeImpl build() {
            return registry.types.addType(new BooleanTypeImpl(this));
        }
    }
}