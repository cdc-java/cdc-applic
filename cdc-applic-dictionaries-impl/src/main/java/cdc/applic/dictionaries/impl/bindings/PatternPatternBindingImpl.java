package cdc.applic.dictionaries.impl.bindings;

import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;

/**
 * PatternType/PatternType binding implementation.
 *
 * @author Damien Carbonne
 */
public class PatternPatternBindingImpl
        extends AbstractTypesBinding<PatternType, StringValue, PatternType, StringValue> {

    protected PatternPatternBindingImpl(Builder builder) {
        super(builder,
              StringValue.class,
              StringValue.class);
        // Don't know how to check domains.
    }

    @Override
    public StringSet getSourceDomain() {
        return StringSet.UNKNOWN;
    }

    @Override
    public StringSet getTargetDomain() {
        return StringSet.UNKNOWN;
    }

    @Override
    protected StringValue doForward(StringValue sourceItem) {
        return sourceItem;
    }

    @Override
    protected StringValue doBackward(StringValue targetItem) {
        return targetItem;
    }

    static Builder builder(DictionariesBindingImpl owner) {
        return new Builder(owner);
    }

    public static class Builder extends AbstractTypesBinding.Builder<Builder, PatternType, PatternType> {
        protected Builder(DictionariesBindingImpl owner) {
            super(owner,
                  PatternType.class,
                  PatternType.class);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public PatternPatternBindingImpl build() {
            return owner.addTypesBinding(new PatternPatternBindingImpl(this));
        }
    }
}