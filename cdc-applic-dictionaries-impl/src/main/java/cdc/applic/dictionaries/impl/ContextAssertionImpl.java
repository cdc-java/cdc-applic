package cdc.applic.dictionaries.impl;

import cdc.applic.dictionaries.Description;
import cdc.applic.dictionaries.items.AssertionKind;
import cdc.applic.dictionaries.items.ContextAssertion;
import cdc.applic.expressions.Expression;
import cdc.util.lang.ObjectUtils;

public class ContextAssertionImpl extends AbstractDExpressedImpl implements ContextAssertion {
    protected ContextAssertionImpl(AbstractDictionaryImpl dictionary,
                                   Expression expression) {
        super(dictionary, expression);
    }

    @Override
    public final Description getDescription() {
        return Description.EMPTY;
    }

    @Override
    public AssertionKind getKind() {
        return AssertionKind.CONTEXT;
    }

    @Override
    public String toString() {
        return "ContextAssertion@" + ObjectUtils.identityHashString(this)
                + "[" + getExpression() + "]";
    }
}