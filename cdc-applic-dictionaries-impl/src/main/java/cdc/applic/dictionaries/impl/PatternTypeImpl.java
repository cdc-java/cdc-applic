package cdc.applic.dictionaries.impl;

import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;

import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.expressions.content.StringValue;
import cdc.util.lang.Checks;

public class PatternTypeImpl extends AbstractTypeImpl implements PatternType {
    private final boolean frozen;
    private final Pattern pattern;
    private final Optional<StringValue> defaultValue;

    protected PatternTypeImpl(Builder builder) {
        super(builder);
        this.frozen = builder.frozen;
        this.pattern = builder.pattern;
        this.defaultValue = Optional.ofNullable(builder.defaultValue);

        // Check compliance of default value
        if (defaultValue.isPresent()) {
            Checks.isTrue(isCompliant(defaultValue.get()),
                          "Default value {} is not compliant with {}",
                          this.defaultValue.get(),
                          getDefinition());
        }
    }

    @Override
    public boolean isFrozen() {
        return frozen;
    }

    @Override
    public Pattern getPattern() {
        return pattern;
    }

    @Override
    public Optional<StringValue> getDefaultValue() {
        return defaultValue;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(),
                            getDescription(),
                            getS1000DProductIdentifier(),
                            getS1000DPropertyType(),
                            isFrozen(),
                            getPattern().pattern());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof PatternTypeImpl)) {
            return false;
        }
        final PatternTypeImpl other = (PatternTypeImpl) object;
        return Objects.equals(getName(), other.getName())
                && Objects.equals(getDescription(), other.getDescription())
                && getS1000DProductIdentifier() == other.getS1000DProductIdentifier()
                && getS1000DPropertyType() == other.getS1000DPropertyType()
                && isFrozen() == other.isFrozen()
                && Objects.equals(getPattern().pattern(), other.getPattern().pattern());
    }

    static Builder builder(RegistryImpl registry) {
        return new Builder(registry);

    }

    public static class Builder extends AbstractTypeImpl.Builder<Builder> {
        protected boolean frozen = false;
        protected Pattern pattern;
        protected StringValue defaultValue = null;

        protected Builder(RegistryImpl registry) {
            super(registry);
        }

        @Override
        public Builder self() {
            return this;
        }

        public Builder frozen(boolean frozen) {
            this.frozen = frozen;
            return self();
        }

        public Builder pattern(String pattern) {
            this.pattern = Pattern.compile(Checks.isNotNull(pattern, "pattern"));
            return self();
        }

        public Builder defaultValue(StringValue defaultValue) {
            this.defaultValue = defaultValue;
            return self();
        }

        public Builder defaultValue(String defaultValue) {
            return defaultValue(StringValue.of(defaultValue));
        }

        @Override
        public PatternTypeImpl build() {
            return registry.types.addType(new PatternTypeImpl(this));
        }
    }
}