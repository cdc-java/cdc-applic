package cdc.applic.dictionaries.impl;

import java.io.PrintStream;
import java.util.Set;

import cdc.applic.dictionaries.DictionaryNamingConventions;
import cdc.applic.expressions.literals.SName;
import cdc.util.debug.Verbosity;

public interface SectionNamingConventions extends DictionaryNamingConventions {
    @Override
    public Set<NamingConventionImpl> getDeclaredNamingConventions();

    public boolean canCreateNamingConvention(SName name);

    public default boolean canCreateNamingConvention(String name) {
        return canCreateNamingConvention(SName.of(name));
    }

    public NamingConventionImpl.Builder namingConvention();

    public void printNamingConventions(PrintStream out,
                                       int level,
                                       Verbosity verbosity);
}