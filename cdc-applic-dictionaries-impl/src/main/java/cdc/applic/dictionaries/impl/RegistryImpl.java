package cdc.applic.dictionaries.impl;

import java.io.PrintStream;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import cdc.applic.dictionaries.DictionaryMembership;
import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.Registry;
import cdc.applic.dictionaries.s1000d.S1000DType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.util.debug.Verbosity;

public class RegistryImpl extends AbstractDictionaryImpl implements Registry, SectionNamingConventions, SectionTypes, SectionItems {
    final RepositoryImpl owner;
    final SectionNamingConventionsImpl conventions = new SectionNamingConventionsImpl(this);
    final SectionTypesImpl types = new SectionTypesImpl(this);

    protected RegistryImpl(Builder builder) {
        super(builder);
        this.owner = builder.repository;
        this.conventions.build();
        this.types.build();
        this.items.build();
        this.assertions.build();
    }

    // *****************
    // *** STRUCTURE ***
    // *****************

    @Override
    public RepositoryImpl getOwner() {
        return owner;
    }

    public void setParents(List<AbstractDictionaryImpl> parents) {
        structure.setParents(parents);
        assertions.refreshDerivationsRec();
        conventions.refreshConventions();
        types.refreshTypes();
        items.refreshItems();
    }

    // **************************
    // *** NAMING CONVENTIONS ***
    // **************************

    @Override
    public boolean canCreateNamingConvention(SName name) {
        return conventions.canCreateNamingConvention(name);
    }

    @Override
    public Set<NamingConventionImpl> getDeclaredNamingConventions() {
        return conventions.getDeclaredNamingConventions();
    }

    @Override
    public Set<NamingConvention> getAllNamingConventions() {
        return conventions.getAllNamingConventions();
    }

    @Override
    public DictionaryMembership getMembership(NamingConvention convention) {
        return conventions.getMembership(convention);
    }

    @Override
    public Optional<NamingConvention> getOptionalNamingConvention(Name name) {
        return conventions.getOptionalNamingConvention(name);
    }

    @Override
    public NamingConvention getNamingConvention(Name name) {
        return conventions.getNamingConvention(name);
    }

    @Override
    public NamingConvention getNamingConvention(String name) {
        return getNamingConvention(Name.of(name));
    }

    @Override
    public NamingConventionImpl.Builder namingConvention() {
        return conventions.namingConvention();
    }

    @Override
    public void printNamingConventions(PrintStream out,
                                       int level,
                                       Verbosity verbosity) {
        conventions.printNamingConventions(out, level, verbosity);
    }

    // *************
    // *** TYPES ***
    // *************

    @Override
    public boolean canCreateType(SName name) {
        return types.canCreateType(name);
    }

    @Override
    public Collection<AbstractTypeImpl> getDeclaredTypes() {
        return types.getDeclaredTypes();
    }

    @Override
    public Collection<AbstractTypeImpl> getAllTypes() {
        return types.getAllTypes();
    }

    @Override
    public DictionaryMembership getMembership(Type type) {
        return types.getMembership(type);
    }

    @Override
    public Optional<AbstractTypeImpl> getOptionalType(Name name) {
        return types.getOptionalType(name);
    }

    @Override
    public Optional<AbstractTypeImpl> getOptionalType(SName name) {
        return types.getOptionalType(name);
    }

    @Override
    public Optional<AbstractTypeImpl> getOptionalType(String name) {
        return types.getOptionalType(name);
    }

    @Override
    public AbstractTypeImpl getType(Name name) {
        return types.getType(name);
    }

    @Override
    public AbstractTypeImpl getType(SName name) {
        return types.getType(name);
    }

    @Override
    public AbstractTypeImpl getType(String name) {
        return types.getType(name);
    }

    @Override
    public <T extends Type> Optional<? extends T> getOptionalType(Name name,
                                                                  Class<T> cls) {
        return types.getOptionalType(name, cls);
    }

    @Override
    public <T extends Type> T getType(Name name,
                                      Class<T> cls) {
        return types.getType(name, cls);
    }

    @Override
    public <T extends Type> T getType(SName name,
                                      Class<T> cls) {
        return types.getType(name, cls);
    }

    @Override
    public <T extends Type> T getType(String name,
                                      Class<T> cls) {
        return types.getType(name, cls);
    }

    @Override
    public Optional<S1000DType> getOptionalTypeWithS1000DId(String s1000dId) {
        return types.getOptionalTypeWithS1000DId(s1000dId);
    }

    @Override
    public S1000DType getTypeWithS1000DId(String s1000dId) {
        return types.getTypeWithS1000DId(s1000dId);
    }

    @Override
    public BooleanTypeImpl.Builder booleanType() {
        return types.booleanType();
    }

    @Override
    public IntegerTypeImpl.Builder integerType() {
        return types.integerType();
    }

    @Override
    public RealTypeImpl.Builder realType() {
        return types.realType();
    }

    @Override
    public PatternTypeImpl.Builder patternType() {
        return types.patternType();
    }

    @Override
    public EnumeratedTypeImpl.Builder enumeratedType() {
        return types.enumeratedType();
    }

    @Override
    public void printTypes(PrintStream out,
                           int level,
                           Verbosity verbosity) {
        types.printTypes(out, level, verbosity);
    }

    @Override
    public void print(PrintStream out,
                      int level,
                      Verbosity verbosity) {
        printLocalStructure(out, level, verbosity);
        printNamingConventions(out, level + 1, verbosity);
        printTypes(out, level + 1, verbosity);
        printAvailableItems(out, level + 1, verbosity);
        printConstraints(out, level + 1, verbosity);
        printAssertions(out, level + 1, verbosity);
        printWritingRules(out, level + 1, verbosity);
        printChildrenPolicies(out, level + 1, verbosity);
    }

    // **********************
    // *** DECLARED ITEMS ***
    // **********************

    @Override
    public boolean canCreateItem(SName name) {
        return items.canCreateItem(name);
    }

    // @Override
    // public DItemUsage getEffectiveItemUsage(NamedDItem item) {
    // return items.getEffectiveItemUsage(item);
    // }
    //
    // @Override
    // public DItemUsage getEffectiveItemUsage(Name name) {
    // return items.getEffectiveItemUsage(name);
    // }

    @Override
    public PropertyImpl.Builder property() {
        return items.property();
    }

    @Override
    public AliasImpl.Builder alias() {
        return items.alias();
    }

    protected static Builder builder(RepositoryImpl repository) {
        return new Builder(repository);
    }

    public static class Builder extends AbstractDictionaryImpl.Builder<Builder> {
        protected Builder(RepositoryImpl repository) {
            super(repository);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public Builder prefix(SName prefix) {
            return super.prefix(prefix);
        }

        public Builder prefix(String prefix) {
            return prefix(SName.of(prefix));
        }

        @Override
        public Builder parents(List<AbstractDictionaryImpl> parents) {
            return super.parents(parents);
        }

        public Builder parents(AbstractDictionaryImpl... parents) {
            return parents(List.of(parents));
        }

        @Override
        public RegistryImpl build() {
            return addDictionary(new RegistryImpl(this));
        }
    }
}