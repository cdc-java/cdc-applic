package cdc.applic.dictionaries.impl;

import java.io.PrintStream;

import cdc.applic.dictionaries.DictionaryAssertions;
import cdc.applic.expressions.Expression;
import cdc.util.debug.Verbosity;

interface SectionAssertions extends DictionaryAssertions {
    public void setContextExpression(Expression context);

    public default void setContextExpression(String context) {
        setContextExpression(Expression.fromString(context));
    }

    /**
     * Creates a <em>user-defined</em> assertion.
     *
     * @param expression The expression.
     * @return The created <em>user-defined</em> assertion.
     */
    public UserDefinedAssertionImpl createAssertion(Expression expression);

    /**
     * Creates a <em>user-defined</em> assertion.
     *
     * @param expression The expression.
     * @return The created <em>user-defined</em> assertion.
     */
    public UserDefinedAssertionImpl createAssertion(String expression);

    public void printAssertions(PrintStream out,
                                int level,
                                Verbosity verbosity);
}