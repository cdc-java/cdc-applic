package cdc.applic.dictionaries.impl;

import java.io.PrintStream;
import java.util.NoSuchElementException;
import java.util.Optional;

import cdc.applic.dictionaries.DictionaryTypes;
import cdc.applic.dictionaries.s1000d.S1000DType;
import cdc.applic.expressions.literals.SName;
import cdc.util.debug.Verbosity;

interface SectionTypes extends DictionaryTypes {

    public Optional<S1000DType> getOptionalTypeWithS1000DId(String s1000DId);

    /**
     * @param s1000DId The S1000D Id.
     * @return The type that whose S1000D Id is {@code s1000DId}.
     * @throws NoSuchElementException When no type with S1000D Id {@code s1000DId} is found.
     */
    public S1000DType getTypeWithS1000DId(String s1000DId);

    /**
     * Returns {@code true} if a name can be used to create a new type.
     * <p>
     * This is the case when:
     * <ul>
     * <li>{@code name} is not {@code null},
     * <li>and there is no other type with the same <em>local</em> name,
     * <li>and there is no other type with the same <em>qualified</em> name,
     * <li>and there is no descendant containing a type with the same <em>qualified</em> name.
     * </ul>
     *
     * @param name The <em>local</em> name.
     * @return {@code true} if {@code name} can be used to create a new type.
     */
    public boolean canCreateType(SName name);

    /**
     * Returns {@code true} if a name can be used to create a new type.
     * <p>
     * This is the case when:
     * <ul>
     * <li>{@code name} is not {@code null},
     * <li>and there is no other type with the same <em>local</em> name,
     * <li>and there is no other type with the same <em>qualified</em> name,
     * <li>and there is no descendant containing a type with the same <em>qualified</em> name.
     * </ul>
     *
     * @param name The <em>local</em> name.
     * @return {@code true} if {@code name} can be used to create a new type.
     */
    public default boolean canCreateType(String name) {
        return canCreateType(SName.of(name));
    }

    /**
     * @return A new {@link BooleanTypeImpl.Builder} instance.
     */
    public BooleanTypeImpl.Builder booleanType();

    /**
     * @return A new {@link IntegerTypeImpl.Builder} instance.
     */
    public IntegerTypeImpl.Builder integerType();

    /**
     * @return A new {@link RealTypeImpl.Builder} instance.
     */
    public RealTypeImpl.Builder realType();

    /**
     * @return A new {@link PatternTypeImpl.Builder} instance.
     */
    public PatternTypeImpl.Builder patternType();

    /**
     * @return A new {@link EnumeratedTypeImpl.Builder} instance.
     */
    public EnumeratedTypeImpl.Builder enumeratedType();

    public void printTypes(PrintStream out,
                           int level,
                           Verbosity verbosity);
}