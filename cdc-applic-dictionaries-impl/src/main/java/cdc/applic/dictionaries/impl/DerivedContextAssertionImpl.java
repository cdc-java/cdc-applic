package cdc.applic.dictionaries.impl;

import cdc.applic.dictionaries.items.AssertionKind;
import cdc.applic.dictionaries.items.DerivedContextAssertion;
import cdc.applic.expressions.Expression;
import cdc.util.lang.ObjectUtils;

/**
 * Implementation of <em>derived context</em> assertion.
 *
 * @author Damien Carbonne
 */
public final class DerivedContextAssertionImpl extends AbstractDExpressedImpl implements DerivedContextAssertion {
    protected DerivedContextAssertionImpl(AbstractDictionaryImpl dictionary,
                                          Expression expression) {
        super(dictionary, expression);
    }

    @Override
    public AssertionKind getKind() {
        return AssertionKind.DERIVED_CONTEXT;
    }

    @Override
    public String toString() {
        return "DerivedContextAssertion@" + ObjectUtils.identityHashString(this)
                + "[" + getExpression() + "]";
    }
}