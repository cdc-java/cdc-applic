package cdc.applic.dictionaries.impl;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import cdc.applic.dictionaries.DictionaryMembership;
import cdc.applic.dictionaries.impl.BooleanTypeImpl.Builder;
import cdc.applic.dictionaries.s1000d.S1000DProductIdentifier;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.applic.dictionaries.s1000d.S1000DType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.util.debug.Printables;
import cdc.util.debug.Verbosity;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;

final class SectionTypesImpl implements SectionTypes {
    private static final String NAME = "name";
    private static final String S1000D_ID = "s1000DId";
    private static final String TYPE = "type";

    final RegistryImpl registry;
    /** Locally declared types. */
    private final Set<AbstractTypeImpl> declared = new HashSet<>();
    /** All types. */
    private final Set<AbstractTypeImpl> all = new HashSet<>();

    /**
     * Map from name (local without prefix, local with prefix, inherited with prefix) to type.
     * Reference name and synonyms are used as keys.
     */
    private final Map<Name, AbstractTypeImpl> nameToType = new HashMap<>();

    /**
     * Map from S1000D Id to type.
     * Includes local and inherited types.
     */
    private final Map<String, AbstractTypeImpl> s1000DIdToType = new HashMap<>();

    SectionTypesImpl(RegistryImpl registry) {
        this.registry = registry;
    }

    /**
     * Collects and inherits all types from parent registries.
     * <p>
     * This <em>MUST</em> be called when a registry is created.
     */
    void build() {
        // There is no descendant at the time of call
        // This does not matter
        addInheritedTypes();
    }

    void refreshTypes() {
        addInheritedTypes();
    }

    private void addInheritedTypes() {
        // Iterate on all descendants, excluding itself
        for (final AbstractDictionaryImpl descendant : registry.structure.getSortedDescendants(true)) {
            if (descendant instanceof final RegistryImpl d) {
                // Iterate on all ancestors of descendant.
                for (final AbstractDictionaryImpl ancestor : descendant.structure.getSortedAncestors(false)) {
                    if (ancestor instanceof final RegistryImpl a) {
                        for (final AbstractTypeImpl type : a.getDeclaredTypes()) {
                            d.types.addInherited(type);
                        }
                    }
                }
            }
        }
    }

    private void addLocally(AbstractTypeImpl type) {
        // No declared type with same local or qualified name
        Checks.doesNotContainKey(nameToType, type.getName().removePrefix(), registry.wrapName() + ".map without prefix");
        Checks.doesNotContainKey(nameToType, type.getName(), registry.wrapName() + ".map with prefix");
        Checks.doesNotContainKey(s1000DIdToType, type.getS1000DId(), registry.wrapName() + ".map s1000D");

        for (final Name synonym : type.getNames().getSynonyms()) {
            Checks.doesNotContainKey(nameToType, synonym.removePrefix(), registry.wrapName() + ".map without prefix");
            Checks.doesNotContainKey(nameToType, synonym, registry.wrapName() + ".map with prefix");
        }

        nameToType.put(type.getName().removePrefix(), type);
        nameToType.put(type.getName(), type);
        s1000DIdToType.put(type.getS1000DId(), type);
        for (final Name synonym : type.getNames().getSynonyms()) {
            nameToType.put(synonym.removePrefix(), type);
            nameToType.put(synonym, type);
        }

        declared.add(type);
        all.add(type);

        registry.newCachesSerial(false);
    }

    private void addInherited(AbstractTypeImpl type) {
        // It is possible an inherited type has already been added
        if (!all.contains(type)) {
            Checks.doesNotContainKey(nameToType, type.getName(), registry.wrapName() + ".map with prefix");
            Checks.doesNotContainKey(s1000DIdToType, type.getS1000DId(), registry.wrapName() + ".map s1000D");

            // Use only qualified name
            nameToType.put(type.getName(), type);
            s1000DIdToType.put(type.getS1000DId(), type);
            all.add(type);

            registry.newCachesSerial(false);
        }
    }

    <T extends AbstractTypeImpl> T addType(T type) {
        Checks.isNotNull(type, "type");

        // Local processing
        addLocally(type);

        // Inheritance
        for (final AbstractDictionaryImpl descendant : registry.structure.getSortedDescendants(false)) {
            if (descendant instanceof final RegistryImpl r) {
                r.types.addInherited(type);
            } else {
                descendant.newCachesSerial(false);
            }
        }

        return type;
    }

    @Override
    public Collection<AbstractTypeImpl> getDeclaredTypes() {
        return declared;
    }

    @Override
    public Collection<AbstractTypeImpl> getAllTypes() {
        return all;
    }

    @Override
    public DictionaryMembership getMembership(Type type) {
        Checks.isNotNull(type, TYPE);

        if (declared.contains(type)) {
            return DictionaryMembership.LOCAL;
        } else if (all.contains(type)) {
            return DictionaryMembership.INHERITED;
        } else {
            return DictionaryMembership.UNRELATED;
        }
    }

    @Override
    public Optional<AbstractTypeImpl> getOptionalType(Name name) {
        Checks.isNotNull(name, NAME);

        final AbstractTypeImpl type = nameToType.get(name);
        return Optional.ofNullable(type);
    }

    @Override

    public Optional<AbstractTypeImpl> getOptionalType(SName name) {
        return getOptionalType(Name.of(name));
    }

    @Override
    public Optional<AbstractTypeImpl> getOptionalType(String name) {
        return getOptionalType(Name.of(name));
    }

    @Override
    public AbstractTypeImpl getType(Name name) {
        return getOptionalType(name).orElseThrow(() -> new NoSuchElementException("No type named '" + name + "' found in "
                + registry.wrapName()));
    }

    @Override
    public AbstractTypeImpl getType(SName name) {
        return getType(Name.of(name));
    }

    @Override
    public AbstractTypeImpl getType(String name) {
        return getType(Name.of(name));
    }

    @Override
    public <T extends Type> Optional<? extends T> getOptionalType(Name name,
                                                                  Class<T> cls) {
        Checks.isNotNull(name, NAME);

        final AbstractTypeImpl type = nameToType.get(name);
        return Optional.ofNullable(cls.cast(type));
    }

    @Override
    public <T extends Type> T getType(Name name,
                                      Class<T> cls) {
        return getOptionalType(name, cls).orElseThrow();
    }

    @Override
    public <T extends Type> T getType(SName name,
                                      Class<T> cls) {
        return getType(Name.of(name), cls);
    }

    @Override
    public <T extends Type> T getType(String name,
                                      Class<T> cls) {
        return getType(Name.of(name), cls);
    }

    @Override
    public Optional<S1000DType> getOptionalTypeWithS1000DId(String s1000dId) {
        Checks.isNotNull(s1000dId, S1000D_ID);

        final AbstractTypeImpl type = s1000DIdToType.get(s1000dId);
        return Optional.ofNullable(type);
    }

    @Override
    public S1000DType getTypeWithS1000DId(String s1000dId) {
        return getOptionalTypeWithS1000DId(s1000dId).orElseThrow(() -> new NoSuchElementException("No type with S1000DId '"
                + s1000dId + "' found in " + registry.wrapName()));
    }

    @Override
    public boolean canCreateType(SName name) {
        if (name != null) {
            final Name n = Name.of(registry.getPrefix(), name);
            // Check locally
            if (nameToType.containsKey(n) || nameToType.containsKey(n.removePrefix())) {
                return false;
            }
            // Check descendants
            for (final AbstractDictionaryImpl descendant : registry.structure.getSortedDescendants(false)) {
                // Types can only be created on registries
                if (descendant instanceof final RegistryImpl r && r.types.nameToType.containsKey(n)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Builder booleanType() {
        return BooleanTypeImpl.builder(registry);
    }

    @Override
    public IntegerTypeImpl.Builder integerType() {
        return IntegerTypeImpl.builder(registry);
    }

    @Override
    public RealTypeImpl.Builder realType() {
        return RealTypeImpl.builder(registry);
    }

    @Override
    public PatternTypeImpl.Builder patternType() {
        return PatternTypeImpl.builder(registry);
    }

    @Override
    public EnumeratedTypeImpl.Builder enumeratedType() {
        return EnumeratedTypeImpl.builder(registry);
    }

    @Override
    public void printTypes(PrintStream out,
                           int level,
                           Verbosity verbosity) {
        Printables.indent(out, level);
        out.println("Types (" + getAllTypes().size() + ")");

        if (!getAllTypes().isEmpty() && verbosity != Verbosity.ESSENTIAL) {
            for (final AbstractTypeImpl type : getAllTypes()) {
                Printables.indent(out, level + 1);
                out.print(getMembership(type) + " ");
                out.print(type.getName() + " = [" + type.getDefinition() + "]");
                out.print(" " + type.getS1000DId());
                if (type.getS1000DPropertyType() != S1000DPropertyType.UNDEFINED) {
                    out.print(" " + type.getS1000DPropertyType());
                }
                if (type.getS1000DProductIdentifier() != S1000DProductIdentifier.NONE
                        && type.getS1000DProductIdentifier() != S1000DProductIdentifier.NOT_APPLICABLE) {
                    out.print(" " + type.getS1000DProductIdentifier());
                }
                out.println();

                type.getNames().print(out, level + 2);

                if (type instanceof final EnumeratedTypeImpl etype) {
                    for (final EnumeratedValueImpl value : etype.getValues()) {
                        value.getLiterals().print(out, level + 2);
                    }

                    final List<StringValue> literals1 = new ArrayList<>(etype.getDomain().getItems());
                    Collections.sort(literals1);
                    for (final StringValue literal1 : literals1) {
                        final List<StringValue> literals2 = new ArrayList<>(etype.getDefinedLessThan(literal1));
                        Collections.sort(literals2);
                        for (final StringValue literal2 : literals2) {
                            Printables.indent(out, level + 2);
                            out.println(literal1 + " < " + literal2);
                        }
                    }
                }
            }
            Printables.indent(out, level + 1);
            out.println("Map (" + nameToType.size() + ")");
            for (final Name name : IterableUtils.toSortedList(nameToType.keySet())) {
                Printables.indent(out, level + 2);
                out.println(name + " -> " + nameToType.get(name));
            }
        }
    }
}