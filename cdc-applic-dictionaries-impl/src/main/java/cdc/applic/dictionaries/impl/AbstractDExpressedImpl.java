package cdc.applic.dictionaries.impl;

import cdc.applic.dictionaries.items.DExpressed;
import cdc.applic.dictionaries.visitors.AddMissingPrefixes;
import cdc.applic.expressions.Expression;
import cdc.util.lang.Checks;

public abstract class AbstractDExpressedImpl implements DExpressed {
    protected final AbstractDictionaryImpl dictionary;
    private Expression expression;
    private Expression qualifiedExpression;

    protected AbstractDExpressedImpl(Builder<?> builder) {
        this.dictionary = builder.dictionary;
        setExpressionInt(builder.expression);
    }

    protected AbstractDExpressedImpl(AbstractDictionaryImpl dictionary,
                                     Expression expression) {
        this.dictionary = dictionary;
        setExpressionInt(expression);
    }

    @Override
    public final AbstractDictionaryImpl getOwner() {
        return dictionary;
    }

    @Override
    public final Expression getExpression() {
        return expression;
    }

    @Override
    public final Expression getQualifiedExpression() {
        return qualifiedExpression;
    }

    protected final void setExpressionInt(Expression expression) {
        this.expression = Checks.isNotNull(expression, "expression");
        this.qualifiedExpression = AddMissingPrefixes.execute(expression, dictionary);
    }

    public abstract static class Builder<B extends Builder<B>> {
        protected final AbstractDictionaryImpl dictionary;
        private Expression expression;

        protected abstract B self();

        protected Builder(AbstractDictionaryImpl dictionary) {
            this.dictionary = dictionary;
        }

        public B expression(Expression expression) {
            this.expression = expression;
            return self();
        }

        public B expression(String expression) {
            return expression(Expression.fromString(expression));
        }
    }
}