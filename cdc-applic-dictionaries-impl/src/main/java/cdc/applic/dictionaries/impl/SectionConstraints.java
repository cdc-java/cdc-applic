package cdc.applic.dictionaries.impl;

import java.io.PrintStream;

import cdc.applic.dictionaries.Constraint;
import cdc.applic.dictionaries.DictionaryConstraints;
import cdc.util.debug.Verbosity;

interface SectionConstraints extends DictionaryConstraints {
    public <C extends Constraint> C addConstraint(C constraint);

    public void removeConstraint(Constraint constraint);

    public void printConstraints(PrintStream out,
                                 int level,
                                 Verbosity verbosity);
}