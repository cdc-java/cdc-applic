package cdc.applic.dictionaries.impl;

import cdc.applic.dictionaries.NamingConvention;
import cdc.util.meta.MetaData;

/**
 * Interface used to add synonyms.
 *
 * @author Damien Carbonne
 *
 * @param <T> The synonym type.
 * @param <R> The reflexive type.
 */
public interface SynonymSetter<T, R extends SynonymSetter<T, R>> {
    /**
     * @return This object.
     */
    public R self();

    /**
     * Adds a synonym for a convention.
     *
     * @param convention The naming convention.
     * @param synonym The synonym.
     * @return This object.
     */
    public R synonym(NamingConvention convention,
                     T synonym);

    /**
     * Adds a synonym for a convention.
     *
     * @param convention The naming convention.
     * @param synonym The synonym.
     * @return This object.
     */
    public R synonym(String convention,
                     String synonym);

    /**
     * Adds synonyms from MetaData.
     *
     * @param meta The MetaData where keys are convention names and values synonyms.
     * @return This object.
     */
    public default R synonyms(MetaData meta) {
        for (final String convention : meta.getKeys()) {
            synonym(convention, meta.get(convention));
        }
        return self();
    }
}