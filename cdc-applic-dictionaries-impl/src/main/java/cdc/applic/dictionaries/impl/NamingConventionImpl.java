package cdc.applic.dictionaries.impl;

import java.util.Locale;
import java.util.Objects;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.util.lang.Checks;

public class NamingConventionImpl implements NamingConvention {
    private final RegistryImpl registry;
    private final Name name;
    private final DescriptionImpl description;

    protected NamingConventionImpl(Builder builder) {
        this.registry = builder.registry;
        this.name = Name.of(registry.getPrefix(), Checks.isNotNull(builder.name, "name"));
        this.description = builder.description.build();
    }

    @Override
    public RegistryImpl getOwner() {
        return registry;
    }

    @Override
    public Name getName() {
        return name;
    }

    @Override
    public final DescriptionImpl getDescription() {
        return description;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(),
                            getDescription());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof NamingConventionImpl)) {
            return false;
        }
        final NamingConventionImpl other = (NamingConventionImpl) object;
        return Objects.equals(getName(), other.getName())
                && Objects.equals(getDescription(), other.getDescription());
    }

    @Override
    public String toString() {
        return "[" + getName() + " " + getDescription() + "]";
    }

    static Builder builder(RegistryImpl registry) {
        return new Builder(registry);
    }

    public static class Builder implements DescriptionSetter<Builder> {
        private final RegistryImpl registry;
        private SName name;
        private final DescriptionImpl.Builder description = DescriptionImpl.builder();

        protected Builder(RegistryImpl registry) {
            this.registry = registry;
        }

        public Builder name(SName name) {
            this.name = name;
            return this;
        }

        public Builder name(String name) {
            return name(SName.of(name));
        }

        @Override
        public Builder description(Locale locale,
                                   String content) {
            Checks.isNotNull(locale, "locale");
            description.description(locale, content);
            return this;
        }

        public NamingConventionImpl build() {
            return registry.conventions.addConvention(new NamingConventionImpl(this));
        }

        public RegistryImpl back() {
            build();
            return registry;
        }
    }
}