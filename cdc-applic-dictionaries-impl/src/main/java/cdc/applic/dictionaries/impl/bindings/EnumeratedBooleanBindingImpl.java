package cdc.applic.dictionaries.impl.bindings;

import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;

/**
 * EnumeratedType/BooleanType binding implementation.
 *
 * @author Damien Carbonne
 */
public class EnumeratedBooleanBindingImpl
        extends AbstractExtensionTypesBinding<EnumeratedType, StringValue, StringSet, BooleanType, BooleanValue, BooleanSet> {

    protected EnumeratedBooleanBindingImpl(Builder builder) {
        super(builder,
              StringValue.class,
              BooleanValue.class,
              StringSet::of,
              BooleanSet::of);
    }

    static Builder builder(DictionariesBindingImpl owner) {
        return new Builder(owner);
    }

    public static class Builder extends
            AbstractExtensionTypesBinding.Builder<Builder, EnumeratedType, StringValue, BooleanType, BooleanValue> {

        protected Builder(DictionariesBindingImpl owner) {
            super(owner,
                  EnumeratedType.class,
                  BooleanType.class);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public Builder map(String source,
                           String target) {
            return map(StringValue.of(source), BooleanValue.of(target));
        }

        public Builder map(String source,
                           boolean target) {
            return map(StringValue.of(source), BooleanValue.of(target));
        }

        @Override
        public EnumeratedBooleanBindingImpl build() {
            return owner.addTypesBinding(new EnumeratedBooleanBindingImpl(this));
        }
    }
}