package cdc.applic.dictionaries.impl;

import java.util.Locale;
import java.util.Objects;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.types.EnumeratedValue;
import cdc.applic.expressions.content.StringValue;
import cdc.util.lang.Checks;

public class EnumeratedValueImpl implements EnumeratedValue {
    private final EnumeratedTypeImpl.Builder owner;
    private final RefSynImpl<StringValue> literals;
    private final StringValue shortLiteral;
    private final int ordinal;
    private final DescriptionImpl description;

    EnumeratedValueImpl(Builder builder) {
        this.owner = builder.owner;
        this.literals = Checks.isNotNull(builder.literals, "literals");
        this.shortLiteral = Checks.isNotNull(builder.shortLiteral, "shortLiteral");
        this.ordinal = builder.ordinal;
        this.description = builder.description.build();
    }

    @Override
    public int getOrdinal() {
        return ordinal;
    }

    @Override
    public RefSynImpl<StringValue> getLiterals() {
        return literals;
    }

    public final void addSynonym(NamingConvention convention,
                                 StringValue synonym) {
        this.literals.addSynonym(synonym, convention);
        // TODO update items
        owner.registry.newCachesSerial(true); // ???
    }

    public final void addSynonym(String convention,
                                 String synonym) {
        addSynonym(owner.registry.getNamingConvention(convention),
                   StringValue.of(synonym));
    }

    @Override
    public StringValue getShortLiteral() {
        return shortLiteral;
    }

    @Override
    public final DescriptionImpl getDescription() {
        return description;
    }

    @Override
    public int hashCode() {
        return Objects.hash(literals,
                            shortLiteral,
                            ordinal);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof EnumeratedValueImpl)) {
            return false;
        }
        final EnumeratedValueImpl other = (EnumeratedValueImpl) object;
        return literals.equals(other.literals)
                && shortLiteral.equals(other.shortLiteral)
                && ordinal == other.ordinal;
    }

    @Override
    public String toString() {
        return "[" + literals + " " + shortLiteral + " " + ordinal + "]";
    }

    static Builder builder(EnumeratedTypeImpl.Builder owner) {
        return new Builder(owner);
    }

    public static Builder builder() {
        return new Builder(null);
    }

    public static class Builder implements DescriptionSetter<Builder>, SynonymSetter<StringValue, Builder> {
        private final EnumeratedTypeImpl.Builder owner;
        private RefSynImpl<StringValue> literals;
        private StringValue shortLiteral;
        private int ordinal = 0;
        private final DescriptionImpl.Builder description = DescriptionImpl.builder();

        protected Builder(EnumeratedTypeImpl.Builder owner) {
            this.owner = owner;
        }

        @Override
        public Builder self() {
            return this;
        }

        public Builder literal(StringValue literal) {
            this.literals = new RefSynImpl<>(literal);
            if (shortLiteral == null) {
                this.shortLiteral = literal;
            }
            return this;
        }

        public Builder literal(String literal) {
            return literal(StringValue.of(literal));
        }

        @Override
        public Builder synonym(NamingConvention convention,
                               StringValue synonym) {
            literals.addSynonym(synonym, convention);
            return this;
        }

        @Override
        public Builder synonym(String convention,
                               String synonym) {
            Checks.assertTrue(owner != null, "Can not set string synonym when owner is not set.");
            return synonym(owner.registry.getNamingConvention(convention),
                           StringValue.of(synonym));
        }

        public Builder shortLiteral(StringValue shortLiteral) {
            this.shortLiteral = shortLiteral;
            return this;
        }

        public Builder shortLiteral(String shortLiteral) {
            return shortLiteral(StringValue.of(shortLiteral));
        }

        public Builder ordinal(int ordinal) {
            this.ordinal = ordinal;
            return this;
        }

        @Override
        public Builder description(Locale locale,
                                   String content) {
            Checks.isNotNull(locale, "locale");
            description.description(locale, content);
            return this;
        }

        public EnumeratedValueImpl build() {
            return new EnumeratedValueImpl(this);
        }

        public EnumeratedTypeImpl.Builder back() {
            owner.add(build());
            return owner;
        }
    }
}