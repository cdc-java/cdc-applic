package cdc.applic.dictionaries.impl.bindings;

import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;

/**
 * IntegerType/BooleanType binding implementation.
 *
 * @author Damien Carbonne
 */
public class IntegerBooleanBindingImpl
        extends AbstractExtensionTypesBinding<IntegerType, IntegerValue, IntegerSet, BooleanType, BooleanValue, BooleanSet> {

    protected IntegerBooleanBindingImpl(Builder builder) {
        super(builder,
              IntegerValue.class,
              BooleanValue.class,
              IntegerSet::of,
              BooleanSet::of);
    }

    static Builder builder(DictionariesBindingImpl owner) {
        return new Builder(owner);
    }

    public static class Builder extends
            AbstractExtensionTypesBinding.Builder<Builder, IntegerType, IntegerValue, BooleanType, BooleanValue> {

        protected Builder(DictionariesBindingImpl owner) {
            super(owner,
                  IntegerType.class,
                  BooleanType.class);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public Builder map(String source,
                           String target) {
            return map(IntegerValue.of(source), BooleanValue.of(target));
        }

        public Builder map(int source,
                           boolean target) {
            return map(IntegerValue.of(source), BooleanValue.of(target));
        }

        @Override
        public IntegerBooleanBindingImpl build() {
            return owner.addTypesBinding(new IntegerBooleanBindingImpl(this));
        }
    }
}