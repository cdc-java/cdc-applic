package cdc.applic.dictionaries.impl.bindings;

import java.io.PrintStream;

import cdc.applic.dictionaries.bindings.BindingRole;
import cdc.applic.dictionaries.bindings.DItemsBinding;
import cdc.applic.dictionaries.items.DItem;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.util.debug.Printable;
import cdc.util.lang.Checks;

/**
 * Base abstract class use to bind 2 {@link DItem}s.
 * <p>
 * Both DItems must be aliases or properties.<br>
 * Mixing a property and an alias is not supported.
 *
 * @author Damien Carbonne
 *
 * @param <N> The DItem type.
 */
public abstract class AbstractDItemsBinding<N extends NamedDItem> implements DItemsBinding, Printable {
    protected final N source;
    protected final N target;

    protected AbstractDItemsBinding(Builder<?, N> builder) {
        this.source = Checks.isNotNull(builder.source, "source");
        this.target = Checks.isNotNull(builder.target, "target");
    }

    @Override
    public NamedDItem getDItem(BindingRole role) {
        Checks.isNotNull(role, "role");
        return role == BindingRole.SOURCE ? source : target;
    }

    @Override
    public String toString() {
        return "[" + source + " >>> " + target + "]";
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        out.println(getDItem(BindingRole.TARGET).getName() + " <<< " + getDItem(BindingRole.SOURCE).getName());
    }

    protected abstract static class Builder<B extends Builder<B, N>, N extends NamedDItem> {
        protected final DictionariesBindingImpl owner;
        protected N source;
        protected N target;

        protected Builder(DictionariesBindingImpl owner) {
            this.owner = owner;
        }

        protected abstract B self();

        protected abstract N getItem(Name name,
                                     BindingRole role);

        public B source(N source) {
            Checks.isNotNull(source, "source");
            this.source = source;
            return self();
        }

        public final B source(Name name) {
            return source(getItem(name, BindingRole.SOURCE));
        }

        public final B source(SName name) {
            return source(getItem(Name.of(owner.getDictionary(BindingRole.SOURCE).getPrefix(), name),
                                  BindingRole.SOURCE));
        }

        public final B source(String name) {
            return source(getItem(Name.of(name), BindingRole.SOURCE));
        }

        public B target(N target) {
            Checks.isNotNull(target, "target");
            this.target = target;
            return self();
        }

        public final B target(Name name) {
            return target(getItem(name, BindingRole.TARGET));
        }

        public final B target(SName name) {
            return target(getItem(Name.of(owner.getDictionary(BindingRole.TARGET).getPrefix(), name),
                                  BindingRole.TARGET));
        }

        public final B target(String name) {
            return target(getItem(Name.of(name), BindingRole.TARGET));
        }

        public abstract AbstractDItemsBinding<N> build();

        public DictionariesBindingImpl back() {
            build();
            return owner;
        }
    }
}