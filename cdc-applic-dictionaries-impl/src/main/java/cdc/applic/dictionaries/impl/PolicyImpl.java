package cdc.applic.dictionaries.impl;

import java.io.PrintStream;

import cdc.applic.dictionaries.Policy;
import cdc.util.debug.Verbosity;

public class PolicyImpl extends AbstractDictionaryImpl implements Policy {
    protected PolicyImpl(Builder builder) {
        super(builder);
        this.items.build();
        this.assertions.build();
    }

    @Override
    public AbstractDictionaryImpl getOwner() {
        return structure.getParent();
    }

    @Override
    public void print(PrintStream out,
                      int level,
                      Verbosity verbosity) {
        printLocalStructure(out, level, verbosity);
        printAvailableItems(out, level + 1, verbosity);
        printConstraints(out, level + 1, verbosity);
        printAssertions(out, level + 1, verbosity);
        printWritingRules(out, level + 1, verbosity);
        printChildrenPolicies(out, level + 1, verbosity);
    }

    protected static Builder builder(AbstractDictionaryImpl parent) {
        return new Builder(parent);
    }

    public static class Builder extends AbstractDictionaryImpl.Builder<Builder> {
        protected Builder(AbstractDictionaryImpl parent) {
            super(parent);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public PolicyImpl build() {
            return addDictionary(new PolicyImpl(this));
        }
    }
}