package cdc.applic.dictionaries.impl;

import java.util.Locale;

/**
 * Interfaces used to set descriptions.
 *
 * @author Damien Carbonne
 *
 * @param <R> The reflexive type.
 */
public interface DescriptionSetter<R extends DescriptionSetter<R>> {
    /**
     * Sets the description associated to a locale.
     *
     * @param locale The locale.
     * @param content The description.
     * @return This object.
     */
    public R description(Locale locale,
                         String content);

    /**
     * Sets the description associated to a locale defined by its language tag.
     *
     * @param locale The locale language tag.
     * @param content The description.
     * @return This object.
     */
    public default R description(String locale,
                                 String content) {
        return description(Locale.forLanguageTag(locale), content);
    }
}