package cdc.applic.dictionaries.impl.bindings;

import java.util.Set;

import cdc.applic.dictionaries.bindings.BindingRole;
import cdc.applic.dictionaries.bindings.PropertyPropertyBinding;
import cdc.applic.dictionaries.bindings.TypesBinding;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

/**
 * Property/Property binding implementation.
 *
 * @author Damien Carbonne
 */
public class PropertyPropertyBindingImpl extends AbstractDItemsBinding<Property> implements PropertyPropertyBinding {
    private final TypesBinding typeBinding;

    protected PropertyPropertyBindingImpl(Builder builder) {
        super(builder);

        if (builder.typeBinding == null) {
            final Set<TypesBinding> set = builder.owner.getTypesBindings(source.getType(), target.getType());
            if (set.size() == 1) {
                this.typeBinding = set.iterator().next();
            } else if (set.isEmpty()) {
                throw new IllegalArgumentException("No type binding found for " + source.getName() + " and " + target.getName());
            } else {
                throw new IllegalArgumentException("Too many type bindings (" + set.size() + ") found for properties "
                        + source.getName() + " and " + target.getName());
            }
        } else {
            Checks.isTrue(builder.owner.typeBindings.contains(builder.typeBinding), "Undeclared type binding.");
            this.typeBinding = builder.typeBinding;
        }
        Checks.isTrue(source.getType() == typeBinding.getSourceType(),
                      "Type of " + source.getName() + " does not match source type of type binding");
        Checks.isTrue(target.getType() == typeBinding.getTargetType(),
                      "Type of " + target.getName() + " does not match target type of type binding");
    }

    @Override
    public Property getDItem(BindingRole role) {
        return getProperty(role);
    }

    @Override
    public Property getProperty(BindingRole role) {
        return (Property) super.getDItem(role);
    }

    @Override
    public final TypesBinding getTypesBinding() {
        return typeBinding;
    }

    static Builder builder(DictionariesBindingImpl owner) {
        return new Builder(owner);
    }

    public static class Builder extends AbstractDItemsBinding.Builder<Builder, Property> {
        private TypesBinding typeBinding;

        protected Builder(DictionariesBindingImpl owner) {
            super(owner);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        protected Property getItem(Name name,
                                   BindingRole role) {
            return owner.getDictionary(role).getProperty(name);
        }

        /**
         * Checks the compliance of source and target property with type binding.
         *
         * @param source The source property.
         * @param target The target property.
         * @param typeBinding The type binding.
         * @throws IllegalArgumentException When a non-compliance is found.
         */
        protected void checkCompliance(Property source,
                                       Property target,
                                       TypesBinding typeBinding) {
            if (typeBinding != null && typeBinding.getType(BindingRole.SOURCE) != null) {
                if (source != null
                        && source.getType() != typeBinding.getType(BindingRole.SOURCE)) {
                    throw new IllegalArgumentException("Source property / type mismatch");

                }
                if (target != null
                        && target.getType() != typeBinding.getType(BindingRole.TARGET)) {
                    throw new IllegalArgumentException("Target property / type mismatch");
                }
            }
        }

        @Override
        public Builder source(Property source) {
            checkCompliance(source, this.target, this.typeBinding);
            return super.source(source);
        }

        @Override
        public Builder target(Property target) {
            checkCompliance(this.source, target, this.typeBinding);
            return super.target(target);
        }

        public Builder typeBinding(TypesBinding typeBinding) {
            Checks.isNotNull(typeBinding, "typeBinding");
            checkCompliance(this.source, this.target, typeBinding);
            this.typeBinding = typeBinding;
            return this;
        }

        @Override
        public PropertyPropertyBindingImpl build() {
            return owner.addDItemsBinding(new PropertyPropertyBindingImpl(this));
        }
    }
}