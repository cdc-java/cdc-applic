package cdc.applic.dictionaries.impl;

import java.io.PrintStream;
import java.util.List;

import cdc.applic.dictionaries.DictionaryStructure;
import cdc.util.debug.Verbosity;

public interface SectionStructure extends DictionaryStructure {
    public RepositoryImpl getRepository();

    public PolicyImpl.Builder policy();

    @Override
    public List<AbstractDictionaryImpl> getParents();

    @Override
    public List<AbstractDictionaryImpl> getSortedAncestors(boolean self);

    @Override
    public List<AbstractDictionaryImpl> getSortedDescendants(boolean self);

    public void printLocalStructure(PrintStream out,
                                    int level,
                                    Verbosity verbosity);

    public void printChildrenPolicies(PrintStream out,
                                      int level,
                                      Verbosity verbosity);
}