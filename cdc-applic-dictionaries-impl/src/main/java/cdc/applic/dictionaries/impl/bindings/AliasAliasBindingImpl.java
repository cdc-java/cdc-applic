package cdc.applic.dictionaries.impl.bindings;

import cdc.applic.dictionaries.bindings.AliasAliasBinding;
import cdc.applic.dictionaries.bindings.BindingRole;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.expressions.literals.Name;

/**
 * Alias/Alias binding implementation.
 *
 * @author Damien Carbonne
 */
public class AliasAliasBindingImpl extends AbstractDItemsBinding<Alias> implements AliasAliasBinding {
    protected AliasAliasBindingImpl(Builder builder) {
        super(builder);
    }

    @Override
    public Alias getDItem(BindingRole role) {
        return getAlias(role);
    }

    @Override
    public Alias getAlias(BindingRole role) {
        return (Alias) super.getDItem(role);
    }

    static Builder builder(DictionariesBindingImpl owner) {
        return new Builder(owner);
    }

    public static class Builder extends AbstractDItemsBinding.Builder<Builder, Alias> {
        protected Builder(DictionariesBindingImpl owner) {
            super(owner);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        protected Alias getItem(Name name,
                                BindingRole role) {
            return owner.getDictionary(role).getAlias(name);
        }

        @Override
        public AliasAliasBindingImpl build() {
            return owner.addDItemsBinding(new AliasAliasBindingImpl(this));
        }
    }
}