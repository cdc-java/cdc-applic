package cdc.applic.dictionaries.impl.bindings;

import cdc.applic.dictionaries.bindings.BindingException;
import cdc.applic.dictionaries.bindings.TypesBinding;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.expressions.content.IntegerSItem;
import cdc.applic.expressions.content.IntegerSet;

/**
 * IntegerType/IntegerType binding implementation.
 * <p>
 * <b>Note:</b> at the moment, only identical definitions are supported.
 *
 * @author Damien Carbonne
 */
public class IntegerIntegerBindingImpl extends AbstractTypesBinding<IntegerType, IntegerSItem, IntegerType, IntegerSItem> {
    protected IntegerIntegerBindingImpl(Builder builder) {
        super(builder,
              IntegerSItem.class,
              IntegerSItem.class);
        // TODO partial mapping
        if (!builder.source.getDomain().contains(builder.target.getDomain())) {
            throw new BindingException("Integer type binding " + TypesBinding.toString(this)
                    + ", target domain " + builder.target.getDefinition()
                    + " not included in source domain " + builder.source.getDefinition());
        }
    }

    @Override
    public IntegerSet getSourceDomain() {
        // TODO partial mapping
        return getSourceType().getDomain();
    }

    @Override
    public IntegerSet getTargetDomain() {
        // TODO partial mapping
        return getTargetType().getDomain();
    }

    @Override
    protected IntegerSItem doForward(IntegerSItem sourceItem) {
        return sourceItem;
    }

    @Override
    protected IntegerSItem doBackward(IntegerSItem targetItem) {
        return targetItem;
    }

    static Builder builder(DictionariesBindingImpl owner) {
        return new Builder(owner);
    }

    public static class Builder extends AbstractTypesBinding.Builder<Builder, IntegerType, IntegerType> {
        protected Builder(DictionariesBindingImpl owner) {
            super(owner,
                  IntegerType.class,
                  IntegerType.class);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public IntegerIntegerBindingImpl build() {
            return owner.addTypesBinding(new IntegerIntegerBindingImpl(this));
        }
    }
}