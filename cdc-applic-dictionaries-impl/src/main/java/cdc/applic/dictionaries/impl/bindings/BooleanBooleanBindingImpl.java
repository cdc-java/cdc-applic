package cdc.applic.dictionaries.impl.bindings;

import java.io.PrintStream;

import cdc.applic.dictionaries.bindings.BooleanBooleanBinding;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;

/**
 * BooleanType/BooleanType binding implementation.
 * <p>
 * It is possible to map {@code true <-> true} and {@code false <-> false}, or revert the mapping.
 *
 * @author Damien Carbonne
 */
public class BooleanBooleanBindingImpl extends AbstractTypesBinding<BooleanType, BooleanValue, BooleanType, BooleanValue>
        implements BooleanBooleanBinding {
    final boolean revert;

    protected BooleanBooleanBindingImpl(Builder builder) {
        super(builder,
              BooleanValue.class,
              BooleanValue.class);
        this.revert = builder.revert;
        // Domains are valid
    }

    @Override
    public BooleanSet getSourceDomain() {
        return BooleanSet.FALSE_TRUE;
    }

    @Override
    public BooleanSet getTargetDomain() {
        return BooleanSet.FALSE_TRUE;
    }

    @Override
    protected BooleanValue doForward(BooleanValue sourceItem) {
        return revert ? sourceItem.negate() : sourceItem;
    }

    @Override
    protected BooleanValue doBackward(BooleanValue targetItem) {
        return revert ? targetItem.negate() : targetItem;
    }

    @Override
    public boolean revert() {
        return revert;
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        super.print(out, level);
        if (revert()) {
            indent(out, level + 1);
            out.println("revert");
        }
    }

    static Builder builder(DictionariesBindingImpl owner) {
        return new Builder(owner);
    }

    public static class Builder extends AbstractTypesBinding.Builder<Builder, BooleanType, BooleanType> {
        private boolean revert = false;

        protected Builder(DictionariesBindingImpl owner) {
            super(owner,
                  BooleanType.class,
                  BooleanType.class);
        }

        @Override
        protected Builder self() {
            return this;
        }

        public Builder revert(boolean revert) {
            this.revert = revert;
            return this;
        }

        @Override
        public BooleanBooleanBindingImpl build() {
            return owner.addTypesBinding(new BooleanBooleanBindingImpl(this));
        }
    }
}