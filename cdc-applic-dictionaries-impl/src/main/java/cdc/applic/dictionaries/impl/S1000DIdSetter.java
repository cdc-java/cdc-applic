package cdc.applic.dictionaries.impl;

public interface S1000DIdSetter<R extends S1000DIdSetter<R>> {
    /**
     * Sets the identifier.
     *
     * @param s1000DId The S1000D id.
     * @return This object.
     */
    public R s1000DId(String s1000DId);
}