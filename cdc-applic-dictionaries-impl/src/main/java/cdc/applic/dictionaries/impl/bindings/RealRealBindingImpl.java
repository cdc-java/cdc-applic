package cdc.applic.dictionaries.impl.bindings;

import cdc.applic.dictionaries.bindings.BindingException;
import cdc.applic.dictionaries.bindings.TypesBinding;
import cdc.applic.dictionaries.types.RealType;
import cdc.applic.expressions.content.RealSItem;
import cdc.applic.expressions.content.RealSet;

/**
 * RealType/RealType binding implementation.
 * <p>
 * <b>Note:</b> at the moment, only identical definitions are supported.
 *
 * @author Damien Carbonne
 */
public class RealRealBindingImpl extends AbstractTypesBinding<RealType, RealSItem, RealType, RealSItem> {
    protected RealRealBindingImpl(Builder builder) {
        super(builder,
              RealSItem.class,
              RealSItem.class);
        if (!builder.source.getDomain().contains(builder.target.getDomain())) {
            throw new BindingException("Real type binding " + TypesBinding.toString(this)
                    + ", target domain " + builder.target.getDefinition()
                    + " not included in source domain " + builder.source.getDefinition());
        }
    }

    @Override
    public RealSet getSourceDomain() {
        // TODO partial mapping
        return getSourceType().getDomain();
    }

    @Override
    public RealSet getTargetDomain() {
        // TODO partial mapping
        return getTargetType().getDomain();
    }

    @Override
    protected RealSItem doForward(RealSItem sourceItem) {
        return sourceItem;
    }

    @Override
    protected RealSItem doBackward(RealSItem targetItem) {
        return targetItem;
    }

    static Builder builder(DictionariesBindingImpl owner) {
        return new Builder(owner);
    }

    public static class Builder extends AbstractTypesBinding.Builder<Builder, RealType, RealType> {
        protected Builder(DictionariesBindingImpl owner) {
            super(owner,
                  RealType.class,
                  RealType.class);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public RealRealBindingImpl build() {
            return owner.addTypesBinding(new RealRealBindingImpl(this));
        }
    }
}