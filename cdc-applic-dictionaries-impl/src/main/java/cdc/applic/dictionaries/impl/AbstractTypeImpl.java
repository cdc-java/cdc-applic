package cdc.applic.dictionaries.impl;

import java.util.Locale;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.s1000d.S1000DProductIdentifier;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.applic.dictionaries.s1000d.S1000DType;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.util.lang.Checks;

public abstract class AbstractTypeImpl implements S1000DType {
    private final RegistryImpl registry;
    private final RefSynImpl<Name> names;
    private final DescriptionImpl description;
    private final String s1000DId;
    private S1000DPropertyType s1000DPropertyType = S1000DPropertyType.UNDEFINED;
    private S1000DProductIdentifier s1000DProductIdentifier = S1000DProductIdentifier.NOT_APPLICABLE;

    protected AbstractTypeImpl(Builder<?> builder) {
        this.registry = builder.registry;
        this.names = Checks.isNotNull(builder.names, "names");
        this.s1000DId = Checks.isNotNull(builder.s1000DId, "s1000DId");
        this.description = builder.description.build();
        this.s1000DPropertyType = builder.s1000DPropertyType;
        this.s1000DProductIdentifier = builder.s1000DProductIdentifier;
    }

    RegistryImpl getDictionary() {
        return registry;
    }

    @Override
    public final String getS1000DId() {
        return s1000DId;
    }

    @Override
    public final RefSynImpl<Name> getNames() {
        return names;
    }

    @Override
    public final DescriptionImpl getDescription() {
        return description;
    }

    @Override
    public S1000DPropertyType getS1000DPropertyType() {
        return s1000DPropertyType;
    }

    public AbstractTypeImpl setS1000DPropertyType(S1000DPropertyType type) {
        this.s1000DPropertyType = type;
        if (!this.s1000DProductIdentifier.isCompliantWith(type)) {
            this.s1000DProductIdentifier = type.getDefaultIdentifier();
        }
        return this;
    }

    @Override
    public S1000DProductIdentifier getS1000DProductIdentifier() {
        return s1000DProductIdentifier;
    }

    public AbstractTypeImpl setS1000DProductIdentifier(S1000DProductIdentifier identifier) {
        Checks.isTrue(identifier.isCompliantWith(getS1000DPropertyType()),
                      getName() + ": " + identifier + " identifier non compliant with property type " + getS1000DPropertyType());
        this.s1000DProductIdentifier = identifier;
        return this;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " " + getName();
    }

    /**
     * Base Builder class.
     *
     * @author Damien Carbonne
     *
     * @param <B> The Builder type.
     */
    public abstract static class Builder<B extends Builder<B>>
            implements S1000DIdSetter<B>, NameSetter<B>, SynonymSetter<SName, B>, DescriptionSetter<B> {
        protected final RegistryImpl registry;
        protected RefSynImpl<Name> names;
        protected final DescriptionImpl.Builder description = DescriptionImpl.builder();
        protected String s1000DId;
        protected S1000DPropertyType s1000DPropertyType = S1000DPropertyType.UNDEFINED;
        protected S1000DProductIdentifier s1000DProductIdentifier = S1000DProductIdentifier.NOT_APPLICABLE;

        protected Builder(RegistryImpl registry) {
            this.registry = registry;
        }

        @Override
        public final B s1000DId(String s1000DId) {
            this.s1000DId = s1000DId;
            return self();
        }

        @Override
        public final B name(SName name) {
            this.names = new RefSynImpl<>(Name.of(registry.getPrefix(), name));
            if (this.s1000DId == null) {
                this.s1000DId = S1000DIdUtils.toAutoS1000DId(names.getReferenceValue());
            }
            return self();
        }

        @Override
        public final B synonym(NamingConvention convention,
                               SName synonym) {
            this.names.addSynonym(Name.of(registry.getPrefix(), synonym), convention);
            return self();
        }

        @Override
        public final B synonym(String convention,
                               String synonym) {
            return synonym(registry.getNamingConvention(convention), SName.of(synonym));
        }

        @Override
        public final B description(Locale locale,
                                   String content) {
            Checks.isNotNull(locale, "locale");
            description.description(locale, content);
            return self();
        }

        public final B s1000DPropertyType(S1000DPropertyType type) {
            this.s1000DPropertyType = type;
            if (!this.s1000DProductIdentifier.isCompliantWith(type)) {
                this.s1000DProductIdentifier = type.getDefaultIdentifier();
            }
            return self();
        }

        public final B s1000DProductIdentifier(S1000DProductIdentifier identifier) {
            Checks.isTrue(identifier.isCompliantWith(s1000DPropertyType),
                          names + ": " + identifier + " identifier non compliant with property type "
                                  + s1000DPropertyType);
            this.s1000DProductIdentifier = identifier;
            return self();
        }

        /**
         * @return A new type instance described by this Builder.
         */
        public abstract AbstractTypeImpl build();

        /**
         * Creates a new type instance described by this Builder. and returns the owning registry.
         *
         * @return the owning registry.
         */
        public final RegistryImpl back() {
            build();
            return registry;
        }
    }
}