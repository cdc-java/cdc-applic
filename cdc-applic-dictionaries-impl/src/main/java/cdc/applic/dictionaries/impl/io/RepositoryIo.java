package cdc.applic.dictionaries.impl.io;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.io.RepositoryXml.StAXLoader;
import cdc.impex.ImpExFactoryFeatures;
import cdc.io.xml.XmlWriter;
import cdc.issues.Issue;
import cdc.issues.IssueSeverity;
import cdc.issues.IssuesCollector;
import cdc.util.events.ProgressController;
import cdc.util.files.Files;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.InvalidDataException;
import cdc.util.lang.UnexpectedValueException;

/**
 * Utility used to save or load a Repository whatever its format is.
 * <p>
 * The supported formats are defined by {@link Format}
 *
 * @author Damien Carbonne
 */
public final class RepositoryIo {
    private static final Logger LOGGER = LogManager.getLogger(RepositoryIo.class);

    private RepositoryIo() {
    }

    /**
     * Enumeration of formats supported by {@link RepositoryIo}.
     *
     * @author Damien Carbonne
     */
    public enum Format {
        XML,
        CSV,
        ODS,
        XLS,
        XLSM,
        XLSX;

        public static Format from(File file) {
            final String ext = Files.getExtension(file);
            for (final Format format : values()) {
                if (format.name().equalsIgnoreCase(ext)) {
                    return format;
                }
            }
            return null;
        }
    }

    /**
     * Loads a repository from a file.
     *
     * @param file The file.
     * @param reaction The reaction to adopt when the file contains errors.
     * @return The repository described in {@code file}.
     * @throws IOException When an IO error occurs.
     * @throws IllegalArgumentException When {@code file} extension is not
     *             recognized.
     * @see Format
     */
    public static RepositoryImpl load(File file,
                                      FailureReaction reaction) throws IOException {
        final Format format = Format.from(file);
        if (format == null) {
            throw new IllegalArgumentException("Can not load " + file);
        } else {
            return switch (format) {
            case XML -> {
                final RepositoryXml.StAXLoader loader = new StAXLoader(reaction);
                yield loader.load(file);
            }

            case CSV, ODS, XLS, XLSM, XLSX -> {
                final IssuesCollector<Issue> collector = new IssuesCollector<>();
                final RepositoryImpl respository = RepositoryOffice.Loader.load(file,
                                                                                collector,
                                                                                ProgressController.VOID);
                if (reaction != FailureReaction.DEFAULT) {
                    boolean failure = false;
                    for (final Issue issue : collector.getIssues()) {
                        if (issue.getSeverity().isAtLeast(IssueSeverity.CRITICAL)) {
                            LOGGER.warn(issue);
                            failure = true;
                        }
                    }
                    if (failure && reaction == FailureReaction.FAIL) {
                        throw new InvalidDataException(file + " contains errors.");
                    }
                }
                yield respository;
            }

            default -> throw new UnexpectedValueException(format);
            };
        }
    }

    /**
     * Saves a repository to a file.
     *
     * @param repository The repository.
     * @param file The file.
     * @param features The features.
     * @throws IOException When an IO error occurs.
     * @throws IllegalArgumentException When {@code file} extension is not
     *             recognized.
     * @see Format
     */
    public static void save(RepositoryImpl repository,
                            File file,
                            RepositoryIoFeatures features) throws IOException {
        final Format format = Format.from(file);
        if (format == null) {
            throw new IllegalArgumentException("Can not save to " + file);
        } else {
            switch (format) {
            case XML -> {
                try (XmlWriter writer = new XmlWriter(file)) {
                    writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
                    RepositoryXml.Printer.write(writer,
                                                repository,
                                                features.isEnabled(RepositoryIoFeatures.Hint.DEBUG));
                }
            }

            case CSV, ODS, XLS, XLSM, XLSX -> {
                final ImpExFactoryFeatures ieff;
                if (features.isEnabled(RepositoryIoFeatures.Hint.BEST)) {
                    ieff = ImpExFactoryFeatures.BEST;
                } else {
                    ieff = ImpExFactoryFeatures.FASTEST;
                }
                RepositoryOffice.Writer.write(repository,
                                              file,
                                              ieff);
            }

            default -> throw new UnexpectedValueException(format);
            }
        }
    }

    /**
     * Saves a repository to a file using default features.
     *
     * @param repository The repository.
     * @param file The file.
     * @throws IOException When an IO error occurs.
     * @throws IllegalArgumentException When {@code file} extension is not
     *             recognized.
     * @see Format
     */
    public static void save(RepositoryImpl repository,
                            File file) throws IOException {
        save(repository, file, RepositoryIoFeatures.DEFAULT);
    }
}