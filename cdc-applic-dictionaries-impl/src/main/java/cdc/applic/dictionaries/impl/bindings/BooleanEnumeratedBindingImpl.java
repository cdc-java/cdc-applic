package cdc.applic.dictionaries.impl.bindings;

import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;

/**
 * BooleanType/EnumeratedType binding implementation.
 *
 * @author Damien Carbonne
 */
public class BooleanEnumeratedBindingImpl
        extends AbstractExtensionTypesBinding<BooleanType, BooleanValue, BooleanSet, EnumeratedType, StringValue, StringSet> {

    protected BooleanEnumeratedBindingImpl(Builder builder) {
        super(builder,
              BooleanValue.class,
              StringValue.class,
              BooleanSet::of,
              StringSet::of);
    }

    static Builder builder(DictionariesBindingImpl owner) {
        return new Builder(owner);
    }

    public static class Builder extends
            AbstractExtensionTypesBinding.Builder<Builder, BooleanType, BooleanValue, EnumeratedType, StringValue> {

        protected Builder(DictionariesBindingImpl owner) {
            super(owner,
                  BooleanType.class,
                  EnumeratedType.class);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public Builder map(String source,
                           String target) {
            return map(BooleanValue.of(source), StringValue.of(target));
        }

        public Builder map(boolean source,
                           String target) {
            return map(BooleanValue.of(source), StringValue.of(target));
        }

        @Override
        public BooleanEnumeratedBindingImpl build() {
            return owner.addTypesBinding(new BooleanEnumeratedBindingImpl(this));
        }
    }
}