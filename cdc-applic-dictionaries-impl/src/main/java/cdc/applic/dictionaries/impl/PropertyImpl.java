package cdc.applic.dictionaries.impl;

import java.util.Locale;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.dictionaries.s1000d.S1000DType;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.util.lang.Checks;

public class PropertyImpl implements S1000DProperty {
    private final RegistryImpl registry;
    private final String s1000DId;
    private final RefSynImpl<Name> names;
    private final S1000DType type;
    private final int ordinal;
    private final DescriptionImpl description;

    protected PropertyImpl(Builder builder) {
        this.registry = builder.registry;
        this.s1000DId = Checks.isNotNull(builder.s1000DId, "s1000DId");
        this.names = Checks.isNotNull(builder.names, "names");
        this.type = Checks.isNotNull(builder.type, "type");
        this.ordinal = builder.ordinal;
        this.description = builder.description.build();
    }

    @Override
    public RegistryImpl getOwner() {
        return registry;
    }

    @Override
    public String getS1000DId() {
        return s1000DId;
    }

    @Override
    public String getDesignation() {
        return "Property " + getName().getProtectedLiteral();
    }

    @Override
    public RefSynImpl<Name> getNames() {
        return names;
    }

    @Override
    public S1000DType getType() {
        return type;
    }

    @Override
    public int getOrdinal() {
        return ordinal;
    }

    @Override
    public DescriptionImpl getDescription() {
        return description;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("Property ")
               .append(getName())
               .append(':')
               .append(getType().getName());
        return builder.toString();
    }

    protected static Builder builder(RegistryImpl registry) {
        return new Builder(registry);
    }

    public static class Builder
            implements S1000DIdSetter<Builder>, NameSetter<Builder>, SynonymSetter<SName, Builder>, DescriptionSetter<Builder> {
        protected final RegistryImpl registry;
        private String s1000DId;
        private RefSynImpl<Name> names;
        private S1000DType type;
        private int ordinal;
        private final DescriptionImpl.Builder description = DescriptionImpl.builder();

        protected Builder(RegistryImpl registry) {
            this.registry = registry;
        }

        @Override
        public Builder self() {
            return this;
        }

        @Override
        public Builder s1000DId(String s1000DId) {
            this.s1000DId = s1000DId;
            return self();
        }

        @Override
        public Builder name(SName name) {
            this.names = new RefSynImpl<>(Name.of(registry.getPrefix(), name));
            if (this.s1000DId == null) {
                this.s1000DId = S1000DIdUtils.toAutoS1000DId(names.getReferenceValue());
            }
            return this;
        }

        @Override
        public Builder synonym(NamingConvention convention,
                               SName synonym) {
            names.addSynonym(Name.of(registry.getPrefix(), synonym), convention);
            return this;
        }

        @Override
        public Builder synonym(String convention,
                               String synonym) {
            return synonym(registry.getRegistry().getNamingConvention(convention),
                           SName.of(synonym));
        }

        @Override
        public Builder description(Locale locale,
                                   String content) {
            Checks.isNotNull(locale, "locale");
            description.description(locale, content);
            return this;
        }

        public Builder ordinal(int ordinal) {
            this.ordinal = ordinal;
            return this;
        }

        public Builder type(AbstractTypeImpl type) {
            this.type = type;
            return this;
        }

        public Builder type(Name typeName) {
            this.type = registry.getRegistry().getType(typeName);
            return this;
        }

        public Builder type(SName typeName) {
            this.type = registry.getRegistry().getType(typeName);
            return this;
        }

        public Builder type(String typeName) {
            this.type = registry.getRegistry().getType(typeName);
            return this;
        }

        public PropertyImpl build() {
            return registry.items.addItemInt(new PropertyImpl(this));
        }

        public RegistryImpl back() {
            build();
            return registry;
        }
    }
}