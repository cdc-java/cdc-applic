package cdc.applic.dictionaries.impl.bindings;

import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;

/**
 * BooleanType/IntegerType binding implementation.
 *
 * @author Damien Carbonne
 */
public class BooleanIntegerBindingImpl
        extends AbstractExtensionTypesBinding<BooleanType, BooleanValue, BooleanSet, IntegerType, IntegerValue, IntegerSet> {

    protected BooleanIntegerBindingImpl(Builder builder) {
        super(builder,
              BooleanValue.class,
              IntegerValue.class,
              BooleanSet::of,
              IntegerSet::of);
    }

    static Builder builder(DictionariesBindingImpl owner) {
        return new Builder(owner);
    }

    public static class Builder extends
            AbstractExtensionTypesBinding.Builder<Builder, BooleanType, BooleanValue, IntegerType, IntegerValue> {

        protected Builder(DictionariesBindingImpl owner) {
            super(owner,
                  BooleanType.class,
                  IntegerType.class);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public Builder map(String source,
                           String target) {
            return map(BooleanValue.of(source), IntegerValue.of(target));
        }

        public Builder map(boolean source,
                           int target) {
            return map(BooleanValue.of(source), IntegerValue.of(target));
        }

        @Override
        public BooleanIntegerBindingImpl build() {
            return owner.addTypesBinding(new BooleanIntegerBindingImpl(this));
        }
    }
}