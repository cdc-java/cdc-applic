package cdc.applic.dictionaries.impl.io;

import java.util.EnumSet;
import java.util.Set;

import cdc.impex.ImpExFactoryFeatures;

public final class RepositoryIoFeatures {
    public static final RepositoryIoFeatures BEST = builder().hint(Hint.BEST)
                                                             .build();
    public static final RepositoryIoFeatures BEST_DEBUG = builder().hint(Hint.BEST)
                                                                   .hint(Hint.DEBUG)
                                                                   .build();
    public static final RepositoryIoFeatures FASTEST = builder().hint(Hint.FASTEST)
                                                                .build();
    public static final RepositoryIoFeatures FASTEST_DEBUG = builder().hint(Hint.FASTEST)
                                                                      .hint(Hint.DEBUG)
                                                                      .build();
    public static final RepositoryIoFeatures DEFAULT = FASTEST;

    private final Set<Hint> hints = EnumSet.noneOf(Hint.class);

    public enum Hint {
        /**
         * If enabled, generates files that have the best rendering.
         * <p>
         * With office, uses {@link ImpExFactoryFeatures#BEST}.
         */
        BEST,

        /**
         * If enabled, generates files as fast as possible.
         * <p>
         * With office, uses {@link ImpExFactoryFeatures#FASTEST}.
         */
        FASTEST,

        /**
         * If enabled, generate debug data.
         * <p>
         * With XML, produces generated assertions.
         */
        DEBUG
    }

    private RepositoryIoFeatures(Set<Hint> hints) {
        this.hints.addAll(hints);
    }

    public boolean isEnabled(Hint hint) {
        return hints.contains(hint);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private final Set<Hint> hints = EnumSet.noneOf(Hint.class);

        private Builder() {
        }

        public Builder hint(Hint hint) {
            hints.add(hint);
            return this;
        }

        public RepositoryIoFeatures build() {
            return new RepositoryIoFeatures(hints);
        }
    }
}