package cdc.applic.dictionaries.impl.bindings;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import cdc.applic.dictionaries.ConversionException;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.Value;
import cdc.util.lang.Checks;

/**
 * Specialization of AbstractTypesBinding usable when binding is done with individual values.
 *
 * @author Damien Carbonne
 *
 * @param <ST> The source Type.
 * @param <SV> The source Value type.
 * @param <SIS> The source SItemSet type.
 * @param <TT> The target Type.
 * @param <TV> The target Value type.
 * @param <TIS> The target SItemSet type.
 */
public class AbstractExtensionTypesBinding<ST extends Type, SV extends Value, SIS extends SItemSet, TT extends Type,
                                           TV extends Value, TIS extends SItemSet>
        extends AbstractTypesBinding<ST, SV, TT, TV> {
    protected final Map<SV, TV> forward;
    protected final Map<TV, SV> backward;
    protected final SIS sourceDomain;
    protected final TIS targetDomain;

    protected AbstractExtensionTypesBinding(Builder<?, ST, SV, TT, TV> builder,
                                            Class<SV> sourceValueClass,
                                            Class<TV> targetValueClass,
                                            Function<Collection<SV>, SIS> createSourceSet,
                                            Function<Collection<TV>, TIS> createTargetSet) {
        super(builder,
              sourceValueClass,
              targetValueClass);

        this.forward = Collections.unmodifiableMap(new HashMap<>(builder.forward));
        this.backward = Collections.unmodifiableMap(new HashMap<>(builder.backward));
        this.sourceDomain = createSourceSet.apply(this.forward.keySet());
        this.targetDomain = createTargetSet.apply(this.backward.keySet());
    }

    public final Set<SV> getSourceValues() {
        return forward.keySet();
    }

    public final Set<TV> getTargetValues() {
        return backward.keySet();
    }

    @Override
    public final SIS getSourceDomain() {
        return sourceDomain;
    }

    @Override
    public final TIS getTargetDomain() {
        return targetDomain;
    }

    @Override
    protected final TV doForward(SV sourceValue) {
        final TV result = forward.get(sourceValue);
        if (result == null) {
            throw new ConversionException("Can not convert " + sourceValue);
        } else {
            return result;
        }
    }

    @Override
    protected final SV doBackward(TV targetValue) {
        final SV result = backward.get(targetValue);
        if (result == null) {
            throw new ConversionException("Can not convert " + targetValue);
        } else {
            return result;
        }
    }

    public abstract static class Builder<B extends Builder<B, ST, SV, TT, TV>,
                                         ST extends Type,
                                         SV extends Value,
                                         TT extends Type,
                                         TV extends Value>
            extends AbstractTypesBinding.Builder<B, ST, TT> {
        protected final Map<SV, TV> forward = new HashMap<>();
        protected final Map<TV, SV> backward = new HashMap<>();

        protected Builder(DictionariesBindingImpl owner,
                          Class<ST> sourceClass,
                          Class<TT> targetClass) {
            super(owner,
                  sourceClass,
                  targetClass);
        }

        public final B map(SV source,
                           TV target) {
            Checks.isNotNull(source, "source");
            Checks.isNotNull(target, "target");
            Checks.isTrue(!forward.containsKey(source), "Duplicate source {}", source);
            Checks.isTrue(!backward.containsKey(target), "Duplicate target {}", target);

            forward.put(source, target);
            backward.put(target, source);
            return self();
        }

        public abstract B map(String source,
                              String target);
    }
}