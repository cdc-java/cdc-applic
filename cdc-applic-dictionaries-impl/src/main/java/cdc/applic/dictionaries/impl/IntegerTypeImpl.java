package cdc.applic.dictionaries.impl;

import java.util.Objects;
import java.util.Optional;

import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;
import cdc.util.lang.Checks;

public class IntegerTypeImpl extends AbstractTypeImpl implements IntegerType {
    private final boolean frozen;
    private final IntegerSet domain;
    private final Optional<IntegerValue> defaultValue;

    protected IntegerTypeImpl(Builder builder) {
        super(builder);
        this.domain = builder.domain;
        this.frozen = builder.frozen;
        this.defaultValue = Optional.ofNullable(builder.defaultValue);

        // Check compliance of default value
        if (defaultValue.isPresent()) {
            Checks.isTrue(isCompliant(defaultValue.get()),
                          "Default value {} is not compliant with {}",
                          this.defaultValue.get(),
                          getDefinition());
        }
    }

    @Override
    public boolean isFrozen() {
        return frozen;
    }

    @Override
    public IntegerSet getDomain() {
        return domain;
    }

    @Override
    public Optional<IntegerValue> getDefaultValue() {
        return defaultValue;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(),
                            getDescription(),
                            getS1000DProductIdentifier(),
                            getS1000DPropertyType(),
                            isFrozen(),
                            getDomain());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof IntegerTypeImpl)) {
            return false;
        }
        final IntegerTypeImpl other = (IntegerTypeImpl) object;
        return Objects.equals(getName(), other.getName())
                && Objects.equals(getDescription(), other.getDescription())
                && getS1000DProductIdentifier() == other.getS1000DProductIdentifier()
                && getS1000DPropertyType() == other.getS1000DPropertyType()
                && isFrozen() == other.isFrozen()
                && Objects.equals(getDomain(), other.getDomain());
    }

    static Builder builder(RegistryImpl registry) {
        return new Builder(registry);
    }

    public static class Builder extends AbstractTypeImpl.Builder<Builder> {
        protected boolean frozen = false;
        protected IntegerSet domain;
        protected IntegerValue defaultValue = null;

        protected Builder(RegistryImpl registry) {
            super(registry);
        }

        @Override
        public Builder self() {
            return this;
        }

        public Builder frozen(boolean frozen) {
            this.frozen = frozen;
            return self();
        }

        public Builder domain(IntegerSet domain) {
            this.domain = domain;
            return self();
        }

        public Builder domain(String content) {
            return domain(IntegerSet.of(content));
        }

        public Builder defaultValue(IntegerValue defaultValue) {
            this.defaultValue = defaultValue;
            return self();
        }

        public Builder defaultValue(int defaultValue) {
            return defaultValue(IntegerValue.of(defaultValue));
        }

        @Override
        public IntegerTypeImpl build() {
            return registry.types.addType(new IntegerTypeImpl(this));
        }
    }
}