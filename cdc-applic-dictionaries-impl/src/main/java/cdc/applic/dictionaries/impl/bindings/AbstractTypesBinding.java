package cdc.applic.dictionaries.impl.bindings;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import cdc.applic.dictionaries.bindings.BindingDirection;
import cdc.applic.dictionaries.bindings.BindingRole;
import cdc.applic.dictionaries.bindings.TypesBinding;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.util.debug.Printable;
import cdc.util.lang.Checks;

/**
 * Base implementation of {@link TypesBinding}.
 *
 * @author Damien Carbonne
 *
 * @param <ST> The source Type.
 * @param <SI> The source SItem type.
 * @param <TT> The target Type.
 * @param <TI> The target SItem type.
 */
public abstract class AbstractTypesBinding<ST extends Type, SI extends SItem, TT extends Type, TI extends SItem>
        implements TypesBinding, Printable {
    private final Class<SI> sourceItemClass;
    private final Class<TI> targetItemClass;
    private final ST source;
    private final TT target;

    protected AbstractTypesBinding(Builder<?, ST, TT> builder,
                                   Class<SI> sourceItemClass,
                                   Class<TI> targetItemClass) {
        this.sourceItemClass = sourceItemClass;
        this.targetItemClass = targetItemClass;
        this.source = Checks.isNotNull(builder.source, "source");
        this.target = Checks.isNotNull(builder.target, "target");
    }

    protected abstract TI doForward(SI sourceItem);

    protected abstract SI doBackward(TI targetItem);

    @Override
    public final ST getSourceType() {
        return source;
    }

    @Override
    public final TT getTargetType() {
        return target;
    }

    @Override
    public final Type getType(BindingRole role) {
        Checks.isNotNull(role, "role");
        return role == BindingRole.SOURCE
                ? getSourceType()
                : getTargetType();
    }

    @Override
    public final SItemSet getDomain(BindingRole role) {
        Checks.isNotNull(role, "role");
        return role == BindingRole.SOURCE
                ? getSourceDomain()
                : getTargetDomain();
    }

    @Override
    public final SItem forward(SItem sourceItem) {
        Checks.isNotNull(sourceItem, "sourceItem");
        Checks.isTrue(getSourceType().isCompliant(sourceItem),
                      "forward(" + sourceItem + ") Non compliant source item");
        return doForward(sourceItemClass.cast(sourceItem));
    }

    @Override
    public final SItem backward(SItem targetItem) {
        Checks.isNotNull(targetItem, "targetItem");
        Checks.isTrue(getTargetType().isCompliant(targetItem),
                      "backward(" + targetItem + ") Non compliant target item");
        return doBackward(targetItemClass.cast(targetItem));
    }

    @Override
    public final SItem convert(SItem item,
                               BindingDirection direction) {
        Checks.isNotNull(direction, "direction");
        if (direction == BindingDirection.FORWARD) {
            return forward(item);
        } else {
            return backward(item);
        }
    }

    @Override
    public final SItemSet forward(SItemSet sourceSet) {
        Checks.isNotNull(sourceSet, "sourceSet");

        final List<SItem> items = new ArrayList<>();
        for (final SItem sourceItem : sourceSet.getItems()) {
            items.add(forward(sourceItem));
        }
        return SItemSetUtils.createBest(items);
    }

    @Override
    public final SItemSet backward(SItemSet targetSet) {
        Checks.isNotNull(targetSet, "targetSet");

        final List<SItem> items = new ArrayList<>();
        for (final SItem targetItem : targetSet.getItems()) {
            items.add(backward(targetItem));
        }
        return SItemSetUtils.createBest(items);
    }

    @Override
    public final SItemSet convert(SItemSet set,
                                  BindingDirection direction) {
        Checks.isNotNull(direction, "direction");
        if (direction == BindingDirection.FORWARD) {
            return forward(set);
        } else {
            return backward(set);
        }
    }

    @Override
    public final String toString() {
        return "[" + source + " >>> " + target + "]";
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        out.println(getType(BindingRole.TARGET).getName() + " <<< " + getType(BindingRole.SOURCE).getName());
    }

    protected abstract static class Builder<B extends Builder<B, ST, TT>, ST extends Type, TT extends Type> {
        protected final DictionariesBindingImpl owner;
        protected final Class<ST> sourceClass;
        protected final Class<TT> targetClass;
        protected ST source;
        protected TT target;

        protected Builder(DictionariesBindingImpl owner,
                          Class<ST> sourceClass,
                          Class<TT> targetClass) {
            this.owner = owner;
            this.sourceClass = sourceClass;
            this.targetClass = targetClass;
        }

        protected abstract B self();

        protected final ST getSourceType(Name name) {
            return owner.getDictionary(BindingRole.SOURCE).getRegistry().getType(name, sourceClass);
        }

        protected final TT getTargetType(Name name) {
            return owner.getDictionary(BindingRole.TARGET).getRegistry().getType(name, targetClass);
        }

        public B source(ST source) {
            this.source = source;
            return self();
        }

        public B source(Name sourceName) {
            return source(getSourceType(sourceName));
        }

        public B source(SName sourceName) {
            return source(Name.of(owner.getDictionary(BindingRole.SOURCE).getPrefix(), sourceName));
        }

        public B source(String sourceName) {
            return source(Name.of(sourceName));
        }

        public B target(TT target) {
            this.target = target;
            return self();
        }

        public B target(Name targetName) {
            return target(getTargetType(targetName));
        }

        public B target(SName targetName) {
            return target(Name.of(owner.getDictionary(BindingRole.TARGET).getPrefix(), targetName));
        }

        public B target(String targetName) {
            return target(Name.of(targetName));
        }

        public abstract TypesBinding build();

        public DictionariesBindingImpl back() {
            build();
            return owner;
        }
    }
}