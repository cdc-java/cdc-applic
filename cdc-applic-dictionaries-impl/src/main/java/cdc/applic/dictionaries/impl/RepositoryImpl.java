package cdc.applic.dictionaries.impl;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.Repository;
import cdc.applic.dictionaries.bindings.BindingRole;
import cdc.applic.dictionaries.bindings.DictionariesBinding;
import cdc.applic.dictionaries.impl.bindings.DictionariesBindingImpl;
import cdc.util.debug.ControlledPrintable;
import cdc.util.debug.Verbosity;
import cdc.util.lang.Checks;
import cdc.util.lang.NotFoundException;
import cdc.util.paths.Path;

/**
 * Repository implementation.
 * <p>
 * Two dictionaries can have the same name if they are not siblings.
 *
 * @author Damien Carbonne
 */
public class RepositoryImpl implements Repository, ControlledPrintable {
    /** Description of this Repository. */
    private final DescriptionImpl description = new DescriptionImpl();
    /** All registries declared in this Repository. */
    private final List<RegistryImpl> registries = new ArrayList<>();
    /** All dictionaries declared in this Repository, indexed by their path. */
    private final Map<Path, AbstractDictionaryImpl> dictionaries = new HashMap<>();
    /** All bindings declared in this Repository. */
    private final List<DictionariesBindingImpl> bindings = new ArrayList<>();

    /**
     * Adds a dictionary to the different internal structures.
     *
     * @param <D> The dictionary type.
     * @param dictionary The dictionary.
     * @return {@code dictionary}.
     * @throws IllegalArgumentException When {@code dictionary} is {@code null},
     *             or another dictionary with the same path exists.
     */
    <D extends AbstractDictionaryImpl> D addDictionary(D dictionary) {
        Checks.isNotNull(dictionary, "dictionary");
        Checks.doesNotContainKey(dictionaries, dictionary.getPath(), "dictionaries");

        dictionaries.put(dictionary.getPath(), dictionary);
        if (dictionary instanceof RegistryImpl) {
            registries.add((RegistryImpl) dictionary);
        }
        return dictionary;
    }

    // Do not use
    public DictionariesBindingImpl addBinding(DictionariesBindingImpl binding) {
        Checks.isNotNull(binding, "binding");

        // Check that no other binding exists for [source, target]
        for (final DictionariesBinding b : bindings) {
            if (b.getDictionary(BindingRole.SOURCE) == binding.getDictionary(BindingRole.SOURCE)
                    && b.getDictionary(BindingRole.TARGET) == binding.getDictionary(BindingRole.TARGET)) {
                throw new IllegalArgumentException("Duplicate binding between "
                        + binding.getDictionary(BindingRole.SOURCE).getName()
                        + " and " + binding.getDictionary(BindingRole.TARGET).getName());
            }
        }

        // TODO check there is no dependency loop
        // TODO check there is no recursive binding.

        this.bindings.add(binding);
        return binding;
    }

    @Override
    public DescriptionImpl getDescription() {
        return description;
    }

    @Override
    public <D extends Dictionary> D getDictionary(Path path,
                                                  Class<D> cls) {
        Checks.isNotNull(path, "path");
        Checks.isNotNull(cls, "cls");
        final D result = cls.cast(dictionaries.get(path.toAbsolute()));
        return NotFoundException.onResult(result,
                                          "No " + cls.getSimpleName() + " matching '" + path + "' found");
    }

    @Override
    public <D extends Dictionary> D getDictionary(String path,
                                                  Class<D> cls) {
        Checks.isNotNull(path, "path");
        return getDictionary(Path.of(path), cls);
    }

    @Override
    public AbstractDictionaryImpl getDictionary(Path path) {
        return getDictionary(path, AbstractDictionaryImpl.class);
    }

    @Override
    public AbstractDictionaryImpl getDictionary(String path) {
        Checks.isNotNull(path, "path");
        return getDictionary(Path.of(path));
    }

    @Override
    public RegistryImpl getRegistry(Path path) {
        return getDictionary(path, RegistryImpl.class);
    }

    @Override
    public RegistryImpl getRegistry(String path) {
        return getRegistry(Path.of(path));
    }

    @Override
    public PolicyImpl getPolicy(Path path) {
        return getDictionary(path, PolicyImpl.class);
    }

    @Override
    public PolicyImpl getPolicy(String path) {
        return getPolicy(Path.of(path));
    }

    @Override
    public List<RegistryImpl> getRegistries() {
        return registries;
    }

    @Override
    public Collection<AbstractDictionaryImpl> getDictionaries() {
        return dictionaries.values();
    }

    @Override
    public List<AbstractDictionaryImpl> getSortedDictionaries() {
        final List<AbstractDictionaryImpl> list = new ArrayList<>();
        for (final RegistryImpl registry : getRegistries()) {
            list.addAll(registry.getSortedDescendants(true));
        }
        return list;
    }

    @Override
    public Collection<PolicyImpl> getPolicies() {
        final List<PolicyImpl> list = new ArrayList<>();
        for (final AbstractDictionaryImpl dictionary : dictionaries.values()) {
            if (dictionary instanceof PolicyImpl) {
                list.add((PolicyImpl) dictionary);
            }
        }
        return list;
    }

    @Override
    public List<DictionariesBindingImpl> getBindings() {
        return bindings;
    }

    public RegistryImpl.Builder registry() {
        return RegistryImpl.builder(this);
    }

    public DictionariesBindingImpl.Builder binding() {
        return DictionariesBindingImpl.builder(this);
    }

    @Override
    public void print(PrintStream out,
                      int level,
                      Verbosity verbosity) {
        indent(out, level);
        out.println("================================================================================");
        indent(out, level);
        out.println("Repository");

        // Registries
        for (final RegistryImpl registry : getRegistries()) {
            registry.print(out, level + 1, verbosity);
        }

        // Bindings
        for (final DictionariesBindingImpl binding : getBindings()) {
            binding.print(out, level + 1, verbosity);
        }
    }
}