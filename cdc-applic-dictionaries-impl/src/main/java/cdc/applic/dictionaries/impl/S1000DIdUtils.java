package cdc.applic.dictionaries.impl;

import java.util.regex.Pattern;

import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;

public final class S1000DIdUtils {
    private static final Pattern P = Pattern.compile("[^\\w\\-]");

    private S1000DIdUtils() {
    }

    public static String toS1000DId(String s) {
        return s == null ? null : P.matcher(s).replaceAll("_");
    }

    public static String toS1000DId(SName name) {
        return name == null ? null : toS1000DId(name.getNonEscapedLiteral());
    }

    public static String toS1000DId(Name name) {
        if (name == null) {
            return null;
        } else if (name.hasPrefix()) {
            return toS1000DId(name.getPrefix()) + "-" + toS1000DId(name.getLocal());
        } else {
            return toS1000DId(name.getLocal());
        }
    }

    public static String toAutoS1000DId(Name name) {
        // Do like that to avoid Sonar (invalid) warning
        return name == null ? null : ("auto-" + toS1000DId(name)).toLowerCase();
    }
}