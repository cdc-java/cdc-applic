package cdc.applic.dictionaries.impl;

import cdc.applic.dictionaries.items.AssertionKind;
import cdc.applic.dictionaries.items.DerivedStandardAssertion;
import cdc.applic.dictionaries.items.StandardAssertion;
import cdc.applic.expressions.Expression;
import cdc.util.lang.Checks;
import cdc.util.lang.ObjectUtils;

/**
 * Implementation of <em>derived</em> assertion.
 *
 * @author Damien Carbonne
 */
public final class DerivedStandardAssertionImpl extends AbstractDExpressedImpl implements DerivedStandardAssertion {
    private final StandardAssertion sourceAssertion;
    private final AbstractDictionaryImpl sourceDictionary;

    protected DerivedStandardAssertionImpl(AbstractDictionaryImpl dictionary,
                                           StandardAssertion sourceAssertion,
                                           AbstractDictionaryImpl sourceDictionary,
                                           Expression expression) {
        super(dictionary, expression);
        this.sourceAssertion = Checks.isNotNull(sourceAssertion, "sourceAssertion");
        this.sourceDictionary = Checks.isNotNull(sourceDictionary, "sourceDictionary");
    }

    @Override
    public AssertionKind getKind() {
        return AssertionKind.DERIVED_STANDARD;
    }

    @Override
    public StandardAssertion getSourceAssertion() {
        return sourceAssertion;
    }

    @Override
    public AbstractDictionaryImpl getSourceDictionary() {
        return sourceDictionary;
    }

    @Override
    public String toString() {
        return "DerivedStandardAssertion@" + ObjectUtils.identityHashString(this)
                + "[" + getExpression()
                + ", derived from " + sourceAssertion.getClass().getSimpleName() + "@"
                + ObjectUtils.identityHashString(sourceAssertion) + " in '" + sourceDictionary.getName() + "']";
    }
}