package cdc.applic.dictionaries.impl;

import cdc.applic.dictionaries.items.AssertionKind;
import cdc.applic.dictionaries.items.UserDefinedAssertion;
import cdc.applic.expressions.Expression;
import cdc.util.lang.ObjectUtils;

/**
 * Implementation of <em>user-defined</em> assertion.
 *
 * @author Damien Carbonne
 */
public final class UserDefinedAssertionImpl extends AbstractDExpressedImpl implements UserDefinedAssertion {
    private final DescriptionImpl description = new DescriptionImpl();

    protected UserDefinedAssertionImpl(AbstractDictionaryImpl dictionary,
                                       Expression expression) {
        super(dictionary, expression);
    }

    // TODO need to update derivations
    // public void setExpression(Expression expression) {
    // setExpressionInt(expression);
    // getDictionary().newCachesSerial(true);
    // }

    @Override
    public final DescriptionImpl getDescription() {
        return description;
    }

    @Override
    public AssertionKind getKind() {
        return AssertionKind.USER_DEFINED;
    }

    @Override
    public String toString() {
        return "UserDefinedAssertion@" + ObjectUtils.identityHashString(this)
                + "[" + getExpression() + "]";
    }
}