package cdc.applic.dictionaries.impl.bindings;

import java.io.PrintStream;
import java.util.function.Consumer;

import cdc.applic.dictionaries.bindings.EnumeratedEnumeratedBinding;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.util.lang.CollectionUtils;

/**
 * EnumeratedType/EnumeratedType binding implementation.
 *
 * @author Damien Carbonne
 */
public class EnumeratedEnumeratedBindingImpl
        extends AbstractExtensionTypesBinding<EnumeratedType, StringValue, StringSet, EnumeratedType, StringValue, StringSet>
        implements EnumeratedEnumeratedBinding {

    protected EnumeratedEnumeratedBindingImpl(Builder builder) {
        super(builder,
              StringValue.class,
              StringValue.class,
              StringSet::of,
              StringSet::of);
    }

    @Override
    public StringValue forward(StringValue sourceValue) {
        return doForward(sourceValue);
    }

    @Override
    public StringValue backward(StringValue targetValue) {
        return doBackward(targetValue);
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        super.print(out, level);
        for (final StringValue targetValue : CollectionUtils.toSortedList(getTargetDomain().getItems())) {
            indent(out, level + 1);
            out.println(targetValue + " <<< " + backward.get(targetValue));
        }
    }

    static Builder builder(DictionariesBindingImpl owner) {
        return new Builder(owner);
    }

    public static class Builder extends
            AbstractExtensionTypesBinding.Builder<Builder, EnumeratedType, StringValue, EnumeratedType, StringValue> {

        protected Builder(DictionariesBindingImpl owner) {
            super(owner,
                  EnumeratedType.class,
                  EnumeratedType.class);
        }

        @Override
        protected Builder self() {
            return this;
        }

        @Override
        public Builder map(String source,
                           String target) {
            return map(StringValue.of(source), StringValue.of(target));
        }

        public Builder accept(Consumer<Builder> consumer) {
            consumer.accept(this);
            return this;
        }

        @Override
        public EnumeratedEnumeratedBindingImpl build() {
            return owner.addTypesBinding(new EnumeratedEnumeratedBindingImpl(this));
        }
    }
}