package cdc.applic.dictionaries.impl;

import java.io.PrintStream;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import cdc.applic.dictionaries.Constraint;
import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.DictionaryMembership;
import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.WritingRuleNameSupplier;
import cdc.applic.dictionaries.items.Assertion;
import cdc.applic.dictionaries.items.ConstraintAssertion;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.StandardAssertion;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.util.debug.ControlledPrintable;
import cdc.util.debug.Verbosity;
import cdc.util.lang.Checks;
import cdc.util.paths.Path;

public abstract class AbstractDictionaryImpl
        implements Dictionary, SectionStructure, SectionAssertions, SectionConstraints, ControlledPrintable {
    private long serial = 0;
    final SectionStructureImpl structure;
    final SectionItemsImpl items;
    final SectionAssertionsImpl assertions;
    final SectionConstraintsImpl constraints;

    protected AbstractDictionaryImpl(Builder<?> builder) {
        this.structure = new SectionStructureImpl(builder.repository,
                                                  this,
                                                  builder.name,
                                                  builder.prefix,
                                                  builder.parent,
                                                  builder.parents);
        this.structure.build();
        this.structure.getDescription().set(builder.description.build());
        this.items = new SectionItemsImpl(this);
        this.assertions = new SectionAssertionsImpl(this, builder.context);
        this.constraints = new SectionConstraintsImpl(this);
    }

    void newCachesSerial(boolean recursive) {
        serial++;
        if (recursive) {
            for (final AbstractDictionaryImpl descendant : structure.getSortedDescendants(false)) {
                descendant.newCachesSerial(false);
            }
        }
    }

    @Override
    public long getCachesSerial() {
        return serial;
    }

    // *****************
    // *** STRUCTURE ***
    // *****************

    @Override
    public String getDesignation() {
        return structure.getDesignation();
    }

    @Override
    public final String getName() {
        return structure.getName();
    }

    @Override
    public final Optional<SName> getPrefix() {
        return structure.getPrefix();
    }

    @Override
    public boolean isDeclaredPrefix(SName prefix) {
        return structure.isDeclaredPrefix(prefix);
    }

    @Override
    public final RepositoryImpl getRepository() {
        return structure.getRepository();
    }

    @Override
    public final List<AbstractDictionaryImpl> getSortedAncestors(boolean self) {
        return structure.getSortedAncestors(self);
    }

    @Override
    public final List<AbstractDictionaryImpl> getSortedDescendants(boolean self) {
        return structure.getSortedDescendants(self);
    }

    @Override
    public final RegistryImpl getRegistry() {
        return structure.getRegistry();
    }

    @Override
    public final <D extends Dictionary> D getDictionary(Path path,
                                                        Class<D> cls) {
        return structure.getDictionary(path, cls);
    }

    @Override
    public final AbstractDictionaryImpl getDictionary(Path path) {
        return structure.getDictionary(path);
    }

    @Override
    public final AbstractDictionaryImpl getDictionary(String path) {
        return structure.getDictionary(path);
    }

    @Override
    public final RegistryImpl getRegistry(Path path) {
        return structure.getRegistry(path);
    }

    @Override
    public final RegistryImpl getRegistry(String path) {
        return structure.getRegistry(path);
    }

    @Override
    public final PolicyImpl getPolicy(Path path) {
        return structure.getPolicy(path);
    }

    @Override
    public final PolicyImpl getPolicy(String path) {
        return structure.getPolicy(path);
    }

    @Override
    public final PolicyImpl.Builder policy() {
        return structure.policy();
    }

    @Override
    public final List<AbstractDictionaryImpl> getParents() {
        return structure.getParents();
    }

    @Override
    public final <T extends Dictionary> List<T> getParents(Class<T> cls) {
        return structure.getParents(cls);
    }

    @Override
    public final List<AbstractDictionaryImpl> getChildren() {
        return structure.getChildren();
    }

    @Override
    public final <T extends Dictionary> List<T> getChildren(Class<T> cls) {
        return structure.getChildren(cls);
    }

    String wrapName() {
        return "'" + getName() + "'";
    }

    @Override
    public final Path getPath() {
        return structure.getPath();
    }

    @Override
    public final DescriptionImpl getDescription() {
        return structure.getDescription();
    }

    @Override
    public void printLocalStructure(PrintStream out,
                                    int level,
                                    Verbosity verbosity) {
        structure.printLocalStructure(out, level, verbosity);
    }

    @Override
    public void printChildrenPolicies(PrintStream out,
                                      int level,
                                      Verbosity verbosity) {
        structure.printChildrenPolicies(out, level, verbosity);
    }

    // ******************
    // *** ASSERTIONS ***
    // ******************

    @Override
    public final void setContextExpression(Expression context) {
        assertions.setContextExpression(context);
    }

    @Override
    public final Expression getContextExpression() {
        return assertions.getContextExpression();
    }

    @Override
    public final <T extends Assertion> Iterable<? extends T> getAssertions(Class<T> cls) {
        return assertions.getAssertions(cls);
    }

    @Override
    public final Set<Assertion> getAllAssertions() {
        return assertions.getAllAssertions();
    }

    @Override
    public final DictionaryMembership getMembership(Assertion assertion) {
        return assertions.getMembership(assertion);
    }

    @Override
    public final UserDefinedAssertionImpl createAssertion(Expression expression) {
        return assertions.createAssertion(expression);
    }

    @Override
    public final UserDefinedAssertionImpl createAssertion(String expression) {
        return assertions.createAssertion(expression);
    }

    @Override
    public final void removeAssertion(StandardAssertion assertion) {
        assertions.removeAssertion(assertion);
    }

    @Override
    public final void removeRelatedAndDerivedAssertions(Constraint constraint) {
        assertions.removeRelatedAndDerivedAssertions(constraint);
    }

    @Override
    public final ConstraintAssertion getRelatedAssertion(Constraint constraint,
                                                         String params) {
        return assertions.getRelatedAssertion(constraint, params);
    }

    @Override
    public final Iterable<ConstraintAssertion> getRelatedAssertions(Constraint constraint) {
        return assertions.getRelatedAssertions(constraint);
    }

    @Override
    public final ConstraintAssertion createAssertion(Constraint constraint,
                                                     String params,
                                                     Expression expression) {
        return assertions.createAssertion(constraint, params, expression);
    }

    @Override
    public final void printAssertions(PrintStream out,
                                      int level,
                                      Verbosity verbosity) {
        assertions.printAssertions(out, level, verbosity);
    }

    // *******************
    // *** CONSTRAINTS ***
    // *******************

    @Override
    public final <C extends Constraint> C addConstraint(C constraint) {
        return constraints.addConstraint(constraint);
    }

    @Override
    public final void removeConstraint(Constraint constraint) {
        constraints.removeConstraint(constraint);
    }

    @Override
    public final Iterable<Constraint> getConstraints() {
        return constraints.getConstraints();
    }

    @Override
    public void printConstraints(PrintStream out,
                                 int level,
                                 Verbosity verbosity) {
        constraints.printConstraints(out, level, verbosity);
    }

    // **********************
    // *** DECLARED ITEMS ***
    // **********************

    @Override
    public final Collection<NamedDItem> getDeclaredItems() {
        return items.getDeclaredItems();
    }

    @Override
    public final Iterable<PropertyImpl> getDeclaredProperties() {
        return items.getDeclaredProperties();
    }

    @Override
    public final Iterable<AliasImpl> getDeclaredAliases() {
        return items.getDeclaredAliases();
    }

    @Override
    public final Optional<NamedDItem> getOptionalDeclaredItem(Name name) {
        return items.getOptionalDeclaredItem(name);
    }

    @Override
    public final Optional<PropertyImpl> getOptionalDeclaredProperty(Name name) {
        return items.getOptionalDeclaredProperty(name);
    }

    @Override
    public final Optional<PropertyImpl> getOptionalDeclaredProperty(SName name) {
        return items.getOptionalDeclaredProperty(name);
    }

    @Override
    public final Optional<PropertyImpl> getOptionalDeclaredProperty(String name) {
        return items.getOptionalDeclaredProperty(name);
    }

    @Override
    public final Optional<AliasImpl> getOptionalDeclaredAlias(Name name) {
        return items.getOptionalDeclaredAlias(name);
    }

    @Override
    public final Optional<AliasImpl> getOptionalDeclaredAlias(SName name) {
        return items.getOptionalDeclaredAlias(name);
    }

    @Override
    public final Optional<AliasImpl> getOptionalDeclaredAlias(String name) {
        return items.getOptionalDeclaredAlias(name);
    }

    @Override
    public final NamedDItem getDeclaredItem(Name name) {
        return items.getDeclaredItem(name);
    }

    @Override
    public final PropertyImpl getDeclaredProperty(Name name) {
        return items.getDeclaredProperty(name);
    }

    @Override
    public final PropertyImpl getDeclaredProperty(SName name) {
        return items.getDeclaredProperty(name);
    }

    @Override
    public final PropertyImpl getDeclaredProperty(String name) {
        return items.getDeclaredProperty(name);
    }

    @Override
    public final AliasImpl getDeclaredAlias(Name name) {
        return items.getDeclaredAlias(name);
    }

    @Override
    public final AliasImpl getDeclaredAlias(SName name) {
        return items.getDeclaredAlias(name);
    }

    @Override
    public final AliasImpl getDeclaredAlias(String name) {
        return items.getDeclaredAlias(name);
    }

    @Override
    public final boolean isDeclared(NamedDItem item) {
        return items.isDeclared(item);
    }

    @Override
    public final boolean hasDeclaredItem(Name name) {
        return items.hasDeclaredItem(name);
    }

    @Override
    public final boolean hasDeclaredProperty(Name name) {
        return items.hasDeclaredProperty(name);
    }

    @Override
    public final boolean hasDeclaredAlias(Name name) {
        return items.hasDeclaredAlias(name);
    }

    // **************
    // *** ITEMS ***
    // **************

    @Override
    public final Collection<NamedDItem> getAllItems() {
        return items.getAllItems();
    }

    @Override
    public final Iterable<PropertyImpl> getAllProperties() {
        return items.getAllProperties();
    }

    @Override
    public final Iterable<AliasImpl> getAllAliases() {
        return items.getAllAliases();
    }

    @Override
    public final Optional<NamedDItem> getOptionalItem(Name name) {
        return items.getOptionalItem(name);
    }

    @Override
    public final Optional<PropertyImpl> getOptionalProperty(Name name) {
        return items.getOptionalProperty(name);
    }

    @Override
    public final Optional<PropertyImpl> getOptionalProperty(SName name) {
        return items.getOptionalProperty(name);
    }

    @Override
    public final Optional<PropertyImpl> getOptionalProperty(String name) {
        return items.getOptionalProperty(name);
    }

    @Override
    public final Optional<AliasImpl> getOptionalAlias(Name name) {
        return items.getOptionalAlias(name);
    }

    @Override
    public final Optional<AliasImpl> getOptionalAlias(SName name) {
        return items.getOptionalAlias(name);
    }

    @Override
    public final Optional<AliasImpl> getOptionalAlias(String name) {
        return items.getOptionalAlias(name);
    }

    @Override
    public final NamedDItem getItem(Name name) {
        return items.getItem(name);
    }

    @Override
    public final Set<NamingConvention> getItemNameNamingConventions(Name name) {
        return items.getItemNameNamingConventions(name);
    }

    @Override
    public final PropertyImpl getProperty(Name name) {
        return items.getProperty(name);
    }

    @Override
    public final PropertyImpl getProperty(SName name) {
        return items.getProperty(name);
    }

    @Override
    public final PropertyImpl getProperty(String name) {
        return items.getProperty(name);
    }

    @Override
    public final AliasImpl getAlias(Name name) {
        return items.getAlias(name);
    }

    @Override
    public final AliasImpl getAlias(SName name) {
        return items.getAlias(name);
    }

    @Override
    public final AliasImpl getAlias(String name) {
        return items.getAlias(name);
    }

    @Override
    public final boolean isAvailable(NamedDItem item) {
        return items.isAvailable(item);
    }

    @Override
    public final DictionaryMembership getMembership(NamedDItem item) {
        return items.getMembership(item);
    }

    @Override
    public final boolean hasItem(Name name) {
        return items.hasItem(name);
    }

    @Override
    public final boolean hasProperty(Name name) {
        return items.hasProperty(name);
    }

    @Override
    public final boolean hasAlias(Name name) {
        return items.hasAlias(name);
    }

    public final void printDeclaredItems(PrintStream out,
                                         int level,
                                         Verbosity verbosity) {
        items.printDeclaredItems(out, level, verbosity);
    }

    public final void printAvailableItems(PrintStream out,
                                          int level,
                                          Verbosity verbosity) {
        items.printAvailableItems(out, level, verbosity);
    }

    // *******************
    // *** PERMISSIONS ***
    // *******************

    @Override
    public final boolean hasTypeUsages() {
        return items.hasTypeUsages();
    }

    @Override
    public final boolean hasItemUsages() {
        return items.hasItemUsages();
    }

    @Override
    public final boolean isAllowed(NamedDItem item) {
        return items.isAllowed(item);
    }

    @Override
    public final boolean hasAllowedItem(Name name) {
        return items.hasAllowedItem(name);
    }

    @Override
    public final Iterable<NamedDItem> getAllowedItems(DItemUsage usage) {
        return items.getAllowedItems(usage);
    }

    @Override
    public final Iterable<PropertyImpl> getAllowedProperties(DItemUsage usage) {
        return items.getAllowedProperties(usage);
    }

    @Override
    public final Iterable<? extends AliasImpl> getAllowedAliases(DItemUsage usage) {
        return items.getAllowedAliases(usage);
    }

    @Override
    public final Optional<NamedDItem> getOptionalAllowedItem(Name name) {
        return items.getOptionalAllowedItem(name);
    }

    @Override
    public final Optional<PropertyImpl> getOptionalAllowedProperty(Name name) {
        return items.getOptionalAllowedProperty(name);
    }

    @Override
    public final Optional<PropertyImpl> getOptionalAllowedProperty(SName name) {
        return items.getOptionalAllowedProperty(name);
    }

    @Override
    public final Optional<PropertyImpl> getOptionalAllowedProperty(String name) {
        return items.getOptionalAllowedProperty(name);
    }

    @Override
    public final Optional<AliasImpl> getOptionalAllowedAlias(Name name) {
        return items.getOptionalAllowedAlias(name);
    }

    @Override
    public final Optional<AliasImpl> getOptionalAllowedAlias(SName name) {
        return items.getOptionalAllowedAlias(name);
    }

    @Override
    public final Optional<AliasImpl> getOptionalAllowedAlias(String name) {
        return items.getOptionalAllowedAlias(name);
    }

    @Override
    public final NamedDItem getAllowedItem(Name name) {
        return items.getAllowedItem(name);
    }

    @Override
    public final PropertyImpl getAllowedProperty(Name name) {
        return items.getAllowedProperty(name);
    }

    @Override
    public final PropertyImpl getAllowedProperty(SName name) {
        return items.getAllowedProperty(name);
    }

    @Override
    public final PropertyImpl getAllowedProperty(String name) {
        return items.getAllowedProperty(name);
    }

    @Override
    public final AliasImpl getAllowedAlias(Name name) {
        return items.getAllowedAlias(name);
    }

    @Override
    public final AliasImpl getAllowedAlias(SName name) {
        return items.getAllowedAlias(name);
    }

    @Override
    public final AliasImpl getAllowedAlias(String name) {
        return items.getAllowedAlias(name);
    }

    @Override
    public final Iterable<NamedDItem> getAllowedItems() {
        return items.getAllowedItems();
    }

    @Override
    public final Iterable<PropertyImpl> getAllowedProperties() {
        return items.getAllowedProperties();
    }

    @Override
    public final Iterable<AliasImpl> getAllowedAliases() {
        return items.getAllowedAliases();
    }

    @Override
    public final DItemUsage getDefaultUsage() {
        return items.getDefaultUsage();
    }

    @Override
    public final DItemUsage getTypeUsage(Type type) {
        return items.getTypeUsage(type);
    }

    @Override
    public final DItemUsage getTypeUsage(Name name) {
        return items.getTypeUsage(name);
    }

    public final AbstractDictionaryImpl setTypeUsage(AbstractTypeImpl type,
                                                     DItemUsage usage) {
        return items.setTypeUsage(type, usage);
    }

    public final AbstractDictionaryImpl setTypeUsage(Name name,
                                                     DItemUsage usage) {
        return items.setTypeUsage(name, usage);
    }

    public final AbstractDictionaryImpl setTypeUsage(SName name,
                                                     DItemUsage usage) {
        return items.setTypeUsage(name, usage);
    }

    public final AbstractDictionaryImpl setTypeUsage(String name,
                                                     DItemUsage usage) {
        return items.setTypeUsage(name, usage);
    }

    public final boolean canSetItemUsage(NamedDItem item,
                                         DItemUsage usage) {
        return items.canSetItemUsage(item, usage);
    }

    public final boolean canSetItemUsage(Name name,
                                         DItemUsage usage) {
        return items.canSetItemUsage(name, usage);
    }

    public final boolean canSetItemUsage(SName name,
                                         DItemUsage usage) {
        return items.canSetItemUsage(name, usage);
    }

    @Override
    public final DItemUsage getItemUsage(NamedDItem item) {
        return items.getItemUsage(item);
    }

    @Override
    public final DItemUsage getItemUsage(Name name) {
        return items.getItemUsage(name);
    }

    @Override
    public final DItemUsage getEffectiveItemUsage(NamedDItem item) {
        return items.getEffectiveItemUsage(item);
    }

    @Override
    public final DItemUsage getEffectiveItemUsage(Name name) {
        return items.getEffectiveItemUsage(name);
    }

    public final AbstractDictionaryImpl setItemUsage(NamedDItem item,
                                                     DItemUsage usage) {
        return items.setItemUsage(item, usage);
    }

    public final AbstractDictionaryImpl setItemUsage(Name name,
                                                     DItemUsage usage) {
        return items.setItemUsage(name, usage);
    }

    public final AbstractDictionaryImpl setItemUsage(String name,
                                                     DItemUsage usage) {
        return items.setItemUsage(name, usage);
    }

    @Override
    public final Set<String> getWritingRuleNames() {
        return items.getWritingRuleNames();
    }

    public final AbstractDictionaryImpl setWritingRuleEnabled(String writingRuleName,
                                                              boolean enabled) {
        return items.setWritingRuleEnabled(writingRuleName, enabled);
    }

    public final AbstractDictionaryImpl setWritingRuleEnabled(WritingRuleNameSupplier supplier,
                                                              boolean enabled) {
        return items.setWritingRuleEnabled(supplier, enabled);
    }

    public final void printWritingRules(PrintStream out,
                                        int level,
                                        Verbosity verbosity) {
        items.printWritingRules(out, level, verbosity);
    }

    @Override
    public String toString() {
        return getPath().toString();
    }

    public abstract static class Builder<B extends Builder<B>> implements DescriptionSetter<B> {
        protected final RepositoryImpl repository;
        private String name;
        private SName prefix;
        protected final DescriptionImpl.Builder description = DescriptionImpl.builder();
        private final AbstractDictionaryImpl parent;
        private List<AbstractDictionaryImpl> parents = Collections.emptyList();
        private Expression context = Expression.TRUE;

        protected Builder(RepositoryImpl repository) {
            this.repository = repository;
            this.parent = null;
        }

        protected Builder(AbstractDictionaryImpl parent) {
            this.parent = Checks.isNotNull(parent, "parent");
            this.repository = parent.getRepository();
            this.parents = List.of(parent);
            this.prefix = parent.getPrefix().orElse(null);
        }

        protected abstract B self();

        public B name(String name) {
            this.name = Checks.isNotNull(name, "name");
            return self();
        }

        protected B prefix(SName prefix) {
            // may be null
            this.prefix = prefix;
            return self();
        }

        @Override
        public final B description(Locale locale,
                                   String content) {
            Checks.isNotNull(locale, "locale");
            description.description(locale, content);
            return self();
        }

        protected B parents(List<AbstractDictionaryImpl> parents) {
            this.parents = Checks.isNotNull(parents, "parents");
            for (final AbstractDictionaryImpl p : parents) {
                Checks.isTrue(p.getRepository() == repository, "Repository mismatch.");
            }
            return self();
        }

        public B context(Expression context) {
            this.context = Checks.isNotNull(context, "context");
            return self();
        }

        public B context(String context) {
            return context(Expression.fromString(context));
        }

        protected <D extends AbstractDictionaryImpl> D addDictionary(D dictionary) {
            return repository.addDictionary(dictionary);
        }

        public abstract AbstractDictionaryImpl build();

        public final RepositoryImpl back() {
            build();
            return repository;
        }
    }
}