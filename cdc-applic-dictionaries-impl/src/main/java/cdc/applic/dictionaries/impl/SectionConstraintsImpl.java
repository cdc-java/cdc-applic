package cdc.applic.dictionaries.impl;

import java.io.PrintStream;
import java.util.LinkedHashSet;
import java.util.Set;

import cdc.applic.dictionaries.Constraint;
import cdc.util.debug.Printables;
import cdc.util.debug.Verbosity;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;
import cdc.util.lang.ObjectUtils;

final class SectionConstraintsImpl implements SectionConstraints {
    private static final String CONSTRAINT = "constraint";

    private final AbstractDictionaryImpl dictionary;

    /** Locally declared constraints. */
    private final Set<Constraint> local = new LinkedHashSet<>();

    SectionConstraintsImpl(AbstractDictionaryImpl dictionary) {
        this.dictionary = dictionary;
    }

    private void addConstraintInt(Constraint constraint) {
        Checks.isNotNull(constraint, CONSTRAINT);

        dictionary.constraints.local.add(constraint);

        dictionary.newCachesSerial(false);
    }

    private void removeConstraintInt(Constraint constraint) {
        Checks.isNotNull(constraint, CONSTRAINT);

        final boolean removed = dictionary.constraints.local.remove(constraint);

        Checks.assertTrue(removed, "Failed to remove {}", constraint);

        dictionary.newCachesSerial(false);
    }

    @Override
    public Iterable<Constraint> getConstraints() {
        return local;
    }

    @Override
    public <C extends Constraint> C addConstraint(C constraint) {
        Checks.isNotNull(constraint, CONSTRAINT);
        Checks.isTrue(constraint.getOwner() == dictionary, "Dictionary mismatch");

        addConstraintInt(constraint);
        return constraint;
    }

    @Override
    public void removeConstraint(Constraint constraint) {
        Checks.isNotNull(constraint, CONSTRAINT);

        removeConstraintInt(constraint);
        dictionary.assertions.removeRelatedAndDerivedAssertions(constraint);
    }

    @Override
    public void printConstraints(PrintStream out,
                                 int level,
                                 Verbosity verbosity) {
        Printables.indent(out, level);
        out.println("Constraints (" + IterableUtils.size(getConstraints()) + ")");
        if (verbosity != Verbosity.ESSENTIAL) {
            for (final Constraint constraint : IterableUtils.toSortedList(getConstraints(),
                                                                          Constraint.TYPE_PARAMS_COMPARATOR)) {
                Printables.indent(out, level + 1);
                out.println(constraint.getClass().getSimpleName() + "@" + ObjectUtils.identityHashString(constraint)
                        + " " + constraint.getTypeName() + " " + ": " + constraint.getParams());
            }
        }
    }
}