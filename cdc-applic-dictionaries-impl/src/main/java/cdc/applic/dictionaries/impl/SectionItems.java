package cdc.applic.dictionaries.impl;

import java.io.PrintStream;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.DictionaryDeclaredItems;
import cdc.applic.dictionaries.DictionaryItems;
import cdc.applic.dictionaries.DictionaryPermissions;
import cdc.applic.dictionaries.WritingRuleNameSupplier;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.util.debug.Verbosity;

interface SectionItems extends DictionaryDeclaredItems, DictionaryItems, DictionaryPermissions {
    /**
     * Returns {@code true} if a name can be used to create a new item.
     * <p>
     * This is the case when:
     * <ul>
     * <li>{@code name} is not {@code null},
     * <li>and there is no other type item the same <em>local</em> name,
     * <li>and there is no other type item the same <em>qualified</em> name,
     * <li>and there is no descendant containing an item with the same <em>qualified</em> name.
     * </ul>
     *
     * @param name The <em>local</em> name.
     * @return {@code true} if {@code name} can be used to create a new item.
     */
    public boolean canCreateItem(SName name);

    /**
     * Returns {@code true} if a name can be used to create a new item.
     * <p>
     * This is the case when:
     * <ul>
     * <li>{@code name} is not {@code null},
     * <li>and there is no other type item the same <em>local</em> name,
     * <li>and there is no other type item the same <em>qualified</em> name,
     * <li>and there is no descendant containing an item with the same <em>qualified</em> name.
     * </ul>
     *
     * @param name The <em>local</em> name.
     * @return {@code true} if {@code name} can be used to create a new item.
     */
    public default boolean canCreateItem(String name) {
        return canCreateItem(SName.of(name));
    }

    public PropertyImpl.Builder property();

    public AliasImpl.Builder alias();

    /**
     * Sets the default usage of properties of a type.
     *
     * @param type The type.
     * @param usage The usage to set.
     * @return This dictionary.
     * @throws IllegalArgumentException When {@code type} or {@code usage} is {@code null}.
     */
    public AbstractDictionaryImpl setTypeUsage(AbstractTypeImpl type,
                                           DItemUsage usage);

    /**
     * Sets the default usage of properties of a type.
     *
     * @param name The type name.
     * @param usage The usage to set.
     * @return This dictionary.
     * @throws IllegalArgumentException When {@code name} or {@code usage} is {@code null}.
     */
    public AbstractDictionaryImpl setTypeUsage(Name name,
                                           DItemUsage usage);

    /**
     * Sets the default usage of properties of a type.
     *
     * @param name The type name.
     * @param usage The usage to set.
     * @return This dictionary.
     * @throws IllegalArgumentException When {@code name} or {@code usage} is {@code null}.
     */
    public default AbstractDictionaryImpl setTypeUsage(SName name,
                                                   DItemUsage usage) {
        return setTypeUsage(Name.of(name), usage);
    }

    /**
     * Sets the default usage of properties of a type.
     *
     * @param name The type name.
     * @param usage The usage to set.
     * @return This dictionary.
     * @throws IllegalArgumentException When {@code name} or {@code usage} is {@code null}.
     */
    public default AbstractDictionaryImpl setTypeUsage(String name,
                                                   DItemUsage usage) {
        return setTypeUsage(Name.of(name), usage);
    }

    /**
     * Returns {@code true} if it is possible to set the usage of an item with a particular value.
     *
     * @param item The item.
     * @param usage The usage to set.
     * @return {@code true} if it is possible to set the usage of {@code item} with {@code usage}.
     * @throws IllegalArgumentException When {@code item} or {@code usage} is {@code null},
     *             or when {@code item} is not available in this Dictionary.
     */
    public boolean canSetItemUsage(NamedDItem item,
                                   DItemUsage usage);

    /**
     * Returns {@code true} if it is possible to set the usage of an item with a particular value.
     *
     * @param name The item name.
     * @param usage The usage to set.
     * @return {@code true} if it is possible to set the usage of item named {@code name} with {@code usage}.
     * @throws IllegalArgumentException When {@code name} or {@code usage} is {@code null},
     *             or when {@code name} is not the name of an available item in this Dictionary.
     */
    public boolean canSetItemUsage(Name name,
                                   DItemUsage usage);

    /**
     * Returns {@code true} if it is possible to set the usage of an item with a particular value.
     *
     * @param name The item name.
     * @param usage The usage to set.
     * @return {@code true} if it is possible to set the usage of item named {@code name} with {@code usage}.
     * @throws IllegalArgumentException When {@code name} or {@code usage} is {@code null},
     *             or when {@code name} is not the name of an available item in this Dictionary.
     */
    public default boolean canSetItemUsage(SName name,
                                           DItemUsage usage) {
        return canSetItemUsage(Name.of(name), usage);
    }

    /**
     * @param item The item.
     * @return The default usage of {@code item} inherited from {@link #getDefaultUsage()}
     *         and {@link #getTypeUsage(Type)}.
     */
    public DItemUsage getEffectiveItemUsage(NamedDItem item);

    /**
     * @param name The item name.
     * @return The default usage of item named {@code name} inherited from {@link #getDefaultUsage()}
     *         and {@link #getTypeUsage(Type)}.
     */
    public DItemUsage getEffectiveItemUsage(Name name);

    /**
     * @param name The item name.
     * @return The default usage of item named {@code name} inherited from {@link #getDefaultUsage()}
     *         and {@link #getTypeUsage(Type)}.
     */
    public default DItemUsage getDefaultItemUsage(SName name) {
        return getEffectiveItemUsage(Name.of(name));
    }

    /**
     * @param name The item name.
     * @return The default usage of item named {@code name} inherited from {@link #getDefaultUsage()}
     *         and {@link #getTypeUsage(Type)}.
     */
    public default DItemUsage getDefaultItemUsage(String name) {
        return getEffectiveItemUsage(Name.of(name));
    }

    /**
     * Set the usage of an item.
     *
     * @param item The item.
     * @param usage The usage.
     * @return This dictionary.
     * @throws IllegalArgumentException When {@code item} or {@code usage} is {@code null}.
     */
    public AbstractDictionaryImpl setItemUsage(NamedDItem item,
                                           DItemUsage usage);

    /**
     * Sets the usage of an item.
     *
     * @param name The <em>local</em> or <em>prefixed</em> item name.
     * @param usage The usage.
     * @return This dictionary.
     * @throws IllegalArgumentException When {@code name} or {@code usage} is {@code null},
     *             or when no item named {@code name} is found.
     */
    public AbstractDictionaryImpl setItemUsage(Name name,
                                           DItemUsage usage);

    /**
     * Sets the usage of an item.
     *
     * @param name The <em>local</em> item name.
     * @param usage The usage.
     * @return This dictionary.
     * @throws IllegalArgumentException When {@code name} or {@code usage} is {@code null},
     *             or when no item named {@code name} is found.
     */
    public default AbstractDictionaryImpl setItemUsage(SName name,
                                                   DItemUsage usage) {
        return setItemUsage(Name.of(name), usage);
    }

    /**
     * Sets the usage of an item.
     *
     * @param name The <em>local</em> or <em>prefixed</em> item name.
     * @param usage The usage.
     * @return This dictionary.
     * @throws IllegalArgumentException When {@code name} or {@code usage} is {@code null},
     *             or when no item named {@code name} is found.
     */
    public default AbstractDictionaryImpl setItemUsage(String name,
                                                   DItemUsage usage) {
        return setItemUsage(Name.of(name), usage);
    }

    public AbstractDictionaryImpl setWritingRuleEnabled(String writingRuleName,
                                                    boolean enabled);

    public AbstractDictionaryImpl setWritingRuleEnabled(WritingRuleNameSupplier supplier,
                                                    boolean enabled);

    public void printAvailableItems(PrintStream out,
                                    int level,
                                    Verbosity verbosity);

    public void printDeclaredItems(PrintStream out,
                                   int level,
                                   Verbosity verbosity);

    public void printWritingRules(PrintStream out,
                                  int level,
                                  Verbosity verbosity);
}