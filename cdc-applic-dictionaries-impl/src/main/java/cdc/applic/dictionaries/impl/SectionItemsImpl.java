package cdc.applic.dictionaries.impl;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.DictionaryMembership;
import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.Policy;
import cdc.applic.dictionaries.SemanticException;
import cdc.applic.dictionaries.WritingRuleNameSupplier;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.Named;
import cdc.applic.expressions.literals.SName;
import cdc.util.debug.Printables;
import cdc.util.debug.Verbosity;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;
import cdc.util.lang.CollectionUtils;

final class SectionItemsImpl implements SectionItems {
    private static final String CALIAS = "Alias";
    private static final String CITEM = "Item";
    private static final String CPROPERTY = "Property";
    private static final String ITEM = "item";
    private static final String NAME = "name";
    private static final String TYPE = "type";
    private static final String USAGE = "usage";

    private final AbstractDictionaryImpl dictionary;

    /** Locally declared items. */
    private final Set<NamedDItem> declared = new HashSet<>();
    /** All (locally declared + inherited) items. */
    private final Set<NamedDItem> all = new HashSet<>();
    /**
     * Map from name (local without prefix, local with prefix, inherited with prefix) to item.
     * Reference name and synonyms are used as keys.
     */
    private final Map<Name, NamedDItem> map = new HashMap<>();

    /**
     * The global default usage of items that are absent from {@link #itemToUsage} map.
     * <p>
     * For a Policy it is {@link DItemUsage#FORBIDDEN FORBIDDEN}.<br>
     * For a Registry it is {@link DItemUsage#OPTIONAL OPTIONAL}.
     */
    private final DItemUsage defaultUsage;

    /**
     * The type specific default usage of properties that are absent from {@link #itemToUsage} map.
     */
    private final Map<Type, DItemUsage> typeToUsage = new HashMap<>();

    /**
     * All (locally declared + inherited) items to their usage.
     * <p>
     * If an item is absent, its usage is the default one.
     */
    private final Map<NamedDItem, DItemUsage> itemToUsage = new HashMap<>();

    private final Set<String> writingRuleNames = new HashSet<>();

    SectionItemsImpl(AbstractDictionaryImpl dictionary) {
        this.dictionary = dictionary;
        this.defaultUsage = dictionary instanceof Policy
                ? DItemUsage.FORBIDDEN
                : DItemUsage.OPTIONAL;
    }

    private static void checkUsage(DItemUsage usage) {
        Checks.isNotNull(usage, USAGE);
        Checks.isTrue(usage != DItemUsage.FORBIDDEN, "Unexpected usage " + usage);
    }

    private static String named(String kind,
                                Object name) {
        return kind + " named " + name;
    }

    private SemanticException availableMismatch(Name name,
                                                String kind) {
        return new SemanticException(named("Item", name) + " available in " + dictionary.wrapName() + " is not a " + kind);
    }

    private SemanticException declaredMismatch(Object name,
                                               String kind) {
        return new SemanticException(named("Item", name) + " declared in " + dictionary.wrapName() + " is not a " + kind);
    }

    private SemanticException noAvailable(Name name,
                                          String kind) {
        throw new SemanticException("No " + named(kind, name) + " is available in " + dictionary.wrapName());
    }

    private SemanticException notAllowed(Name name,
                                         String kind) {
        throw new SemanticException(named(kind, name) + " is not allowed in " + dictionary.wrapName());
    }

    private SemanticException noDeclared(Object name,
                                         String kind) {
        throw new SemanticException("No " + named(kind, name) + " is declared in " + dictionary.wrapName());
    }

    private void checkIsAvailable(NamedDItem item) {
        Checks.isTrue(isAvailable(item), "Item is not avaliable in " + dictionary.wrapName());
    }

    private NamedDItem getItemIfAvailable(Name name) {
        final NamedDItem item = getOptionalItem(name).orElse(null);
        if (item == null) {
            throw noAvailable(name, CITEM);
        } else {
            return item;
        }
    }

    private void addLocally(NamedDItem item) {
        // No declared item with same local or qualified name
        Checks.doesNotContainKey(map, item.getName().removePrefix(), dictionary.wrapName() + ".map no prefix");
        Checks.doesNotContainKey(map, item.getName(), dictionary.wrapName() + ".map with prefix");

        for (final Name synonym : item.getNames().getSynonyms()) {
            Checks.doesNotContainKey(map, synonym.removePrefix(), dictionary.wrapName() + ".map without prefix");
            Checks.doesNotContainKey(map, synonym, dictionary.wrapName() + ".map with prefix");
        }

        map.put(item.getName().removePrefix(), item);
        map.put(item.getName(), item);
        for (final Name synonym : item.getNames().getSynonyms()) {
            map.put(synonym.removePrefix(), item);
            map.put(synonym, item);
        }

        declared.add(item);
        all.add(item);

        dictionary.newCachesSerial(false);
    }

    private void addInherited(NamedDItem item,
                              Dictionary sourceDictionary) {
        if (!all.contains(item)) {
            Checks.doesNotContainKey(map, item.getName(), dictionary.wrapName() + ".map with prefix");

            for (final Name synonym : item.getNames().getSynonyms()) {
                Checks.doesNotContainKey(map, synonym, dictionary.wrapName() + ".map with prefix");
            }

            // We can inherit an item from several parents in case of diamond inheritance
            // So we don't check duplicates

            // Register qualified name
            map.put(item.getName(), item);
            for (final Name synonym : item.getNames().getSynonyms()) {
                map.put(synonym, item);
            }

            // Register local name when reference registry is the same.
            // This is the case with policies which have the same namespace as their registry
            // BUT, this must be done for items that are locally declared, not the inherited ones
            // Note that currently sourceDictionary.getRegistry() == sourceDictionary
            if (sourceDictionary.getRegistry() == dictionary.getRegistry()
                    && dictionary.getRegistry().isDeclared(item)) {
                map.put(item.getName().removePrefix(), item);
                for (final Name synonym : item.getNames().getSynonyms()) {
                    map.put(synonym.removePrefix(), item);
                }
            }

            all.add(item);

            // DO NOT propagate usage to descendants

            dictionary.newCachesSerial(false);
        }
    }

    <T extends NamedDItem> T addItemInt(T item) {
        Checks.isNotNull(item, ITEM);

        // Local processing
        addLocally(item);

        // Inheritance
        for (final AbstractDictionaryImpl descendant : dictionary.structure.getSortedDescendants(false)) {
            descendant.items.addInherited(item, dictionary);
        }
        return item;
    }

    /**
     * Collects and inherits all items from parent dictionaries.
     * <p>
     * This <em>MUST</em> be called when a dictionary is created.
     */
    void build() {
        // There is no descendant at the time of call
        // This does not matter
        addInheritedItems();
    }

    void refreshItems() {
        addInheritedItems();
    }

    private void addInheritedItems() {
        // Iterate on all descendants, excluding itself
        for (final AbstractDictionaryImpl descendant : dictionary.structure.getSortedDescendants(true)) {
            // Iterate on all ancestors of descendant.
            for (final AbstractDictionaryImpl ancestor : descendant.structure.getSortedAncestors(false)) {
                // We currently pass all items, but passing declared ones should suffice
                for (final NamedDItem item : ancestor.getAllItems()) {
                    descendant.items.addInherited(item, ancestor);
                }
            }
        }
    }

    // **********************
    // *** DECLARED ITEMS **
    // **********************

    @Override
    public DItemUsage getDefaultUsage() {
        return defaultUsage;
    }

    @Override
    public DItemUsage getTypeUsage(Type type) {
        Checks.isNotNull(type, TYPE);

        return typeToUsage.get(type);
    }

    @Override
    public DItemUsage getTypeUsage(Name name) {
        Checks.isNotNull(name, NAME);

        final AbstractTypeImpl type = dictionary.getRegistry().getType(name);

        return getTypeUsage(type);
    }

    @Override
    public AbstractDictionaryImpl setTypeUsage(AbstractTypeImpl type,
                                               DItemUsage usage) {
        Checks.isNotNull(type, TYPE);
        Checks.isTrue(dictionary.getRegistry().getMembership(type).isRelated(), "Unrelated type");

        if (usage == null) {
            typeToUsage.remove(type);
        } else {
            typeToUsage.put(type, usage);
        }
        return dictionary;
    }

    @Override
    public AbstractDictionaryImpl setTypeUsage(Name name,
                                               DItemUsage usage) {
        Checks.isNotNull(name, NAME);

        final AbstractTypeImpl type = dictionary.getRegistry().getType(name);

        return setTypeUsage(type, usage);
    }

    @Override
    public boolean canCreateItem(SName name) {
        if (name != null) {
            final Name n = Name.of(dictionary.getPrefix(), name);
            // Check locally
            if (map.containsKey(n) || map.containsKey(n.removePrefix())) {
                return false;
            }
            // Check descendants
            for (final AbstractDictionaryImpl descendant : dictionary.structure.getSortedDescendants(false)) {
                // Item are available on all dictionaries, but checking registries is sufficient
                if (descendant instanceof RegistryImpl && descendant.items.map.containsKey(n)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public PropertyImpl.Builder property() {
        return PropertyImpl.builder((RegistryImpl) dictionary);
    }

    @Override
    public AliasImpl.Builder alias() {
        return AliasImpl.builder((RegistryImpl) dictionary);
    }

    @Override
    public Collection<NamedDItem> getDeclaredItems() {
        return declared;
    }

    @Override
    public Iterable<PropertyImpl> getDeclaredProperties() {
        return IterableUtils.filterAndConvert(PropertyImpl.class, getDeclaredItems());
    }

    @Override
    public Iterable<AliasImpl> getDeclaredAliases() {
        return IterableUtils.filterAndConvert(AliasImpl.class, getDeclaredItems());
    }

    @Override
    public Optional<NamedDItem> getOptionalDeclaredItem(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        if (declared.contains(item)) {
            return Optional.of(item);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<PropertyImpl> getOptionalDeclaredProperty(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        if (declared.contains(item)) {
            if (item instanceof final PropertyImpl property) {
                return Optional.of(property);
            } else {
                throw declaredMismatch(name, CPROPERTY);
            }
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<PropertyImpl> getOptionalDeclaredProperty(SName name) {
        return getOptionalDeclaredProperty(Name.of(name));
    }

    @Override
    public Optional<PropertyImpl> getOptionalDeclaredProperty(String name) {
        return getOptionalDeclaredProperty(Name.of(name));
    }

    @Override
    public Optional<AliasImpl> getOptionalDeclaredAlias(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        if (declared.contains(item)) {
            if (item instanceof final AliasImpl alias) {
                return Optional.of(alias);
            } else {
                throw declaredMismatch(name, CALIAS);
            }
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<AliasImpl> getOptionalDeclaredAlias(SName name) {
        return getOptionalDeclaredAlias(Name.of(name));
    }

    @Override
    public Optional<AliasImpl> getOptionalDeclaredAlias(String name) {
        return getOptionalDeclaredAlias(Name.of(name));
    }

    @Override
    public NamedDItem getDeclaredItem(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        if (declared.contains(item)) {
            return item;
        } else {
            throw noDeclared(name, CITEM);
        }
    }

    @Override
    public PropertyImpl getDeclaredProperty(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        if (!declared.contains(item)) {
            throw noDeclared(name, CPROPERTY);
        } else if (item instanceof final PropertyImpl property) {
            // item is local and is a property
            return property;
        } else {
            // item is local but is not a property
            throw declaredMismatch(name, CPROPERTY);
        }
    }

    @Override
    public PropertyImpl getDeclaredProperty(SName name) {
        return getDeclaredProperty(Name.of(name));
    }

    @Override
    public PropertyImpl getDeclaredProperty(String name) {
        return getDeclaredProperty(Name.of(name));
    }

    @Override
    public AliasImpl getDeclaredAlias(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        if (!declared.contains(item)) {
            throw noDeclared(name, CALIAS);
        } else if (item instanceof final AliasImpl alias) {
            // item is local and is an alias
            return alias;
        } else {
            // item is local but is not an alias
            throw declaredMismatch(name, CALIAS);
        }
    }

    @Override
    public AliasImpl getDeclaredAlias(SName name) {
        return getDeclaredAlias(Name.of(name));
    }

    @Override
    public AliasImpl getDeclaredAlias(String name) {
        return getDeclaredAlias(Name.of(name));
    }

    @Override
    public boolean isDeclared(NamedDItem item) {
        Checks.isNotNull(item, ITEM);

        return declared.contains(item);
    }

    @Override
    public boolean hasDeclaredItem(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        return declared.contains(item);
    }

    @Override
    public boolean hasDeclaredProperty(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        return declared.contains(item) && item instanceof PropertyImpl;
    }

    @Override
    public boolean hasDeclaredAlias(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        return declared.contains(item) && item instanceof AliasImpl;
    }

    @Override
    public void printDeclaredItems(PrintStream out,
                                   int level,
                                   Verbosity verbosity) {
        // Declared Properties
        Printables.indent(out, level);
        out.println("Declared Properties (" + IterableUtils.size(getDeclaredProperties()) + ")");
        if (verbosity != Verbosity.ESSENTIAL) {
            for (final PropertyImpl property : IterableUtils.toSortedList(getDeclaredProperties(), Named.NAME_COMPARATOR)) {
                Printables.indent(out, level + 1);
                out.println(property.getName().removePrefix() + " " + ": " + property.getType().getName());
                out.println(property.getS1000DId());
                property.getNames().print(out, level + 2);
            }
        }

        // Declared Aliases
        Printables.indent(out, level);
        out.println("Declared Aliases (" + IterableUtils.size(getDeclaredAliases()) + ")");
        if (verbosity != Verbosity.ESSENTIAL) {
            for (final AliasImpl alias : IterableUtils.toSortedList(getDeclaredAliases(), Named.NAME_COMPARATOR)) {
                Printables.indent(out, level + 1);
                out.println(alias.getName().removePrefix() + " = [" + alias.getExpression() + "]");
                alias.getNames().print(out, level + 2);
            }
        }
    }

    // ***********************
    // *** AVAILABLE ITEMS ***
    // ***********************

    @Override
    public Collection<NamedDItem> getAllItems() {
        return all;
    }

    @Override
    public Iterable<PropertyImpl> getAllProperties() {
        return IterableUtils.filterAndConvert(PropertyImpl.class, getAllItems());
    }

    @Override
    public Iterable<AliasImpl> getAllAliases() {
        return IterableUtils.filterAndConvert(AliasImpl.class, getAllItems());
    }

    @Override
    public DictionaryMembership getMembership(NamedDItem item) {
        Checks.isNotNull(item, ITEM);

        if (declared.contains(item)) {
            return DictionaryMembership.LOCAL;
        } else if (all.contains(item)) {
            return DictionaryMembership.INHERITED;
        } else {
            return DictionaryMembership.UNRELATED;
        }
    }

    @Override
    public Optional<NamedDItem> getOptionalItem(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        return Optional.ofNullable(item);
    }

    @Override
    public Optional<PropertyImpl> getOptionalProperty(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        if (item == null || item instanceof PropertyImpl) {
            return Optional.ofNullable((PropertyImpl) item);
        } else {
            throw declaredMismatch(name, CPROPERTY);
        }
    }

    @Override
    public Optional<PropertyImpl> getOptionalProperty(SName name) {
        return getOptionalProperty(Name.of(name));
    }

    @Override
    public Optional<PropertyImpl> getOptionalProperty(String name) {
        return getOptionalProperty(Name.of(name));
    }

    @Override
    public Optional<AliasImpl> getOptionalAlias(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        if (item == null || item instanceof AliasImpl) {
            return Optional.ofNullable((AliasImpl) item);
        } else {
            throw declaredMismatch(name, CALIAS);
        }
    }

    @Override
    public Optional<AliasImpl> getOptionalAlias(SName name) {
        return getOptionalAlias(Name.of(name));
    }

    @Override
    public Optional<AliasImpl> getOptionalAlias(String name) {
        return getOptionalAlias(Name.of(name));
    }

    @Override
    public NamedDItem getItem(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        if (item == null) {
            throw noAvailable(name, CITEM);
        } else {
            return item;
        }
    }

    @Override
    public Set<NamingConvention> getItemNameNamingConventions(Name name) {
        // item will be found whether name has a prefix or not
        final Optional<NamedDItem> item = getOptionalItem(name);
        if (item.isPresent()) {
            // we must add prefix as names only contains prefixed values
            return item.get().getNames().getNamingConventions(name.setPrefixIfMissing(dictionary.getPrefix().orElse(null)));
        } else {
            return Collections.emptySet();
        }
    }

    @Override
    public PropertyImpl getProperty(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        if (item == null) {
            throw noAvailable(name, CPROPERTY);
        } else if (item instanceof final PropertyImpl property) {
            return property;
        } else {
            throw availableMismatch(name, CPROPERTY);
        }
    }

    @Override
    public PropertyImpl getProperty(SName name) {
        return getProperty(Name.of(name));
    }

    @Override
    public PropertyImpl getProperty(String name) {
        return getProperty(Name.of(name));
    }

    @Override
    public AliasImpl getAlias(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        if (item == null) {
            throw noAvailable(name, CALIAS);
        } else if (item instanceof final AliasImpl alias) {
            return alias;
        } else {
            throw availableMismatch(name, CALIAS);
        }
    }

    @Override
    public AliasImpl getAlias(SName name) {
        return getAlias(Name.of(name));
    }

    @Override
    public AliasImpl getAlias(String name) {
        return getAlias(Name.of(name));
    }

    @Override
    public boolean isAvailable(NamedDItem item) {
        Checks.isNotNull(item, ITEM);

        final NamedDItem value = map.get(item.getName());

        return value == item;
    }

    @Override
    public boolean hasItem(Name name) {
        Checks.isNotNull(name, NAME);

        return map.containsKey(name);
    }

    @Override
    public boolean hasProperty(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        return item instanceof PropertyImpl;
    }

    @Override
    public boolean hasAlias(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);
        return item instanceof AliasImpl;
    }

    @Override
    public void printAvailableItems(PrintStream out,
                                    int level,
                                    Verbosity verbosity) {
        // Default usage
        Printables.indent(out, level);
        out.println("Default usage: " + getDefaultUsage());

        // Default type usage
        Printables.indent(out, level);
        out.println("Type usages");
        for (final Type type : dictionary.getRegistry().getAllTypes()) {
            if (getTypeUsage(type) != null) {
                Printables.indent(out, level + 1);
                out.println(type.getName() + ": " + getTypeUsage(type));
            }
        }

        // Available Items
        Printables.indent(out, level);
        out.println("Available Items (" + IterableUtils.size(getAllItems()) + ")");

        // Available Properties
        Printables.indent(out, level + 1);
        out.println("Available Properties (" + IterableUtils.size(getAllProperties()) + ")");
        if (verbosity != Verbosity.ESSENTIAL) {
            for (final PropertyImpl property : IterableUtils.toSortedList(getAllProperties(), Named.NAME_COMPARATOR)) {
                Printables.indent(out, level + 2);
                out.println(getMembership(property) + " " + property.getName().removePrefix(dictionary.getPrefix()) + " : "
                        + property.getType().getName() + " "
                        + getEffectiveItemUsage(property));
                property.getNames().print(out, level + 3);
            }
        }

        // Available Aliases
        Printables.indent(out, level + 1);
        out.println("Available Aliases (" + IterableUtils.size(getAllAliases()) + ")");
        if (verbosity != Verbosity.ESSENTIAL) {
            for (final AliasImpl alias : IterableUtils.toSortedList(getAllAliases(), Named.NAME_COMPARATOR)) {
                Printables.indent(out, level + 2);
                out.println(getMembership(alias) + " " + alias.getName().removePrefix(dictionary.getPrefix()) + " = ["
                        + alias.getExpression() + "] " + getEffectiveItemUsage(alias));
                alias.getNames().print(out, level + 3);
            }
        }

        Printables.indent(out, level + 1);
        out.println("Map (" + map.size() + ")");
        if (verbosity != Verbosity.ESSENTIAL) {
            for (final Name name : IterableUtils.toSortedList(map.keySet())) {
                Printables.indent(out, level + 2);
                out.println(name + " -> " + map.get(name));
            }
        }
    }

    // *******************
    // *** PERMISSIONS ***
    // *******************

    private void checkUsageCompliance(NamedDItem item,
                                      DItemUsage usage) {
        if (!canSetItemUsage(item, usage)) {
            throw new IllegalArgumentException("Set usage of " + item.getDesignation()
                    + " to " + usage + " in " + dictionary.getDesignation() + " is not possible.");
        }
    }

    /**
     * @param dictionary The dictionary.
     * @param item The item.
     * @return {@code true} if all parents of {@code dictionary} where {@code item} is available
     *         forbid its usage.
     */
    private static boolean isForbiddenByAllParents(AbstractDictionaryImpl dictionary,
                                                   NamedDItem item) {
        int count = 0;
        for (final AbstractDictionaryImpl parent : dictionary.getParents()) {
            if (parent.isAvailable(item)) {
                count++;
                if (parent.getEffectiveItemUsage(item) != DItemUsage.FORBIDDEN) {
                    return false;
                }
            }
        }
        return count > 0;
    }

    @Override
    public boolean hasTypeUsages() {
        return !typeToUsage.isEmpty();
    }

    @Override
    public boolean hasItemUsages() {
        return !itemToUsage.isEmpty();
    }

    @Override
    public boolean canSetItemUsage(NamedDItem item,
                                   DItemUsage usage) {
        Checks.isNotNull(item, ITEM);
        Checks.isNotNull(usage, USAGE);
        Checks.isTrue(isAvailable(item), "Item is not availaible in this dictionary");

        // Item is available in this dictionary

        // Only take into account parents where item is available
        // If item is locally declared, this makes a difference
        final List<DItemUsage> parentsUsages = new ArrayList<>();
        for (final AbstractDictionaryImpl parent : dictionary.getParents()) {
            if (parent.isAvailable(item)) {
                parentsUsages.add(parent.getEffectiveItemUsage(item));
            }
        }
        return DItemUsage.isCompliant(usage, parentsUsages);
    }

    @Override
    public boolean canSetItemUsage(Name name,
                                   DItemUsage usage) {
        Checks.isNotNull(name, NAME);
        Checks.isNotNull(usage, USAGE);

        final NamedDItem item = getOptionalItem(name).orElse(null);
        if (item == null) {
            throw new IllegalArgumentException("No item named " + name + " in " + dictionary.getName());
        } else {
            return canSetItemUsage(item, usage);
        }
    }

    @Override
    public DItemUsage getItemUsage(NamedDItem item) {
        Checks.isNotNull(item, ITEM);
        checkIsAvailable(item);

        return itemToUsage.get(item);
    }

    @Override
    public DItemUsage getItemUsage(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = getItemIfAvailable(name);
        return getItemUsage(item);
    }

    /**
     * @param item The item.
     * @return The default usage to be used for {@code item} if no specific usage is set for it.
     */
    private DItemUsage getDefaultItemUsage(NamedDItem item) {
        Checks.isNotNull(item, ITEM);

        if (isAvailable(item)) {
            if (item instanceof final Property property) {
                final Type type = property.getType();
                return typeToUsage.getOrDefault(type, defaultUsage);
            } else {
                return defaultUsage;
            }
        } else {
            return DItemUsage.FORBIDDEN;
        }
    }

    @Override
    public DItemUsage getEffectiveItemUsage(NamedDItem item) {
        Checks.isNotNull(item, ITEM);

        // If all parents forbid that item, then it can not be allowed
        if (isForbiddenByAllParents(dictionary, item)) {
            return DItemUsage.FORBIDDEN;
        } else {
            return itemToUsage.getOrDefault(item, getDefaultItemUsage(item));
        }
    }

    @Override
    public DItemUsage getEffectiveItemUsage(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = getOptionalItem(name).orElse(null);

        if (item == null) {
            return DItemUsage.FORBIDDEN;
        } else {
            return getEffectiveItemUsage(item);
        }
    }

    @Override
    public AbstractDictionaryImpl setItemUsage(NamedDItem item,
                                               DItemUsage usage) {
        Checks.isNotNull(item, ITEM);
        Checks.isNotNull(usage, USAGE);

        checkUsageCompliance(item, usage);

        // We can not be too clever as default type usage can change at any time
        // Therefore, we store all passed value, whether they are currently useful of not.

        if (usage == null) {
            itemToUsage.remove(item);
        } else {
            itemToUsage.put(item, usage);
        }

        // DO NOT propagate to descendants

        return dictionary;
    }

    @Override
    public AbstractDictionaryImpl setItemUsage(Name name,
                                               DItemUsage usage) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = getOptionalItem(name).orElse(null);
        if (item == null) {
            throw new IllegalArgumentException("No item named " + name + " in " + dictionary.getName());
        } else {
            return setItemUsage(item, usage);
        }
    }

    @Override
    public AbstractDictionaryImpl setWritingRuleEnabled(String writingRuleName,
                                                        boolean enabled) {
        if (enabled) {
            if (!writingRuleNames.contains(writingRuleName)) {
                writingRuleNames.add(writingRuleName);
                // Recurse on children policies
                for (final PolicyImpl policy : dictionary.getChildren(PolicyImpl.class)) {
                    policy.setWritingRuleEnabled(writingRuleName, true);
                }
            }
        } else {
            writingRuleNames.remove(writingRuleName);
            // Leave the rule on children
        }
        return dictionary;
    }

    @Override
    public AbstractDictionaryImpl setWritingRuleEnabled(WritingRuleNameSupplier supplier,
                                                        boolean enabled) {
        return setWritingRuleEnabled(supplier.getRuleName(), enabled);
    }

    @Override
    public boolean isAllowed(NamedDItem item) {
        return getEffectiveItemUsage(item) != DItemUsage.FORBIDDEN;
    }

    @Override
    public boolean hasAllowedItem(Name name) {
        return getEffectiveItemUsage(name) != DItemUsage.FORBIDDEN;
    }

    @Override
    public Optional<NamedDItem> getOptionalAllowedItem(Name name) {
        Checks.isNotNull(name, NAME);

        // The available item or null
        final NamedDItem item = dictionary.getOptionalItem(name).orElse(null);
        // The item usage
        final DItemUsage usage = item == null ? DItemUsage.FORBIDDEN : getEffectiveItemUsage(item);

        if (usage == DItemUsage.FORBIDDEN) {
            // Item is either null or available and FORBIDDEN
            return Optional.empty();
        } else {
            // Item is available and not FORBIDDEN
            return Optional.of(item);
        }
    }

    @Override
    public Optional<PropertyImpl> getOptionalAllowedProperty(Name name) {
        Checks.isNotNull(name, NAME);

        // The available item or null
        final NamedDItem item = dictionary.getOptionalItem(name).orElse(null);
        if (item instanceof PropertyImpl || item == null) {
            // item is an available property or is null

            // The item usage
            final DItemUsage usage = item == null ? DItemUsage.FORBIDDEN : getEffectiveItemUsage(item);

            if (usage == DItemUsage.FORBIDDEN) {
                // Item is either null, or available and FORBIDDEN
                return Optional.empty();
            } else {
                // Item is available and not FORBIDDEN
                return Optional.of((PropertyImpl) item);
            }
        } else {
            // item is an available alias
            throw availableMismatch(name, CPROPERTY);
        }
    }

    @Override
    public Optional<PropertyImpl> getOptionalAllowedProperty(SName name) {
        return getOptionalAllowedProperty(Name.of(name));
    }

    @Override
    public Optional<PropertyImpl> getOptionalAllowedProperty(String name) {
        return getOptionalAllowedProperty(Name.of(name));
    }

    @Override
    public Optional<AliasImpl> getOptionalAllowedAlias(Name name) {
        Checks.isNotNull(name, NAME);

        // The available item or null
        final NamedDItem item = dictionary.getOptionalItem(name).orElse(null);
        if (item instanceof AliasImpl || item == null) {
            // item is an available alias or is null

            // The item usage
            final DItemUsage usage = item == null ? DItemUsage.FORBIDDEN : getEffectiveItemUsage(item);

            if (usage == DItemUsage.FORBIDDEN) {
                // Item is either null, or available and FORBIDDEN
                return Optional.empty();
            } else {
                // Item is available and not FORBIDDEN
                return Optional.of((AliasImpl) item);
            }
        } else {
            // item is an available alias
            throw availableMismatch(name, CALIAS);
        }
    }

    @Override
    public Optional<AliasImpl> getOptionalAllowedAlias(SName name) {
        return getOptionalAllowedAlias(Name.of(name));
    }

    @Override
    public Optional<AliasImpl> getOptionalAllowedAlias(String name) {
        return getOptionalAllowedAlias(Name.of(name));
    }

    @Override
    public Iterable<NamedDItem> getAllowedItems(DItemUsage usage) {
        checkUsage(usage);

        return IterableUtils.filter(all,
                                    x -> itemToUsage.getOrDefault(x, getDefaultUsage()) == usage);
    }

    @Override
    public Iterable<PropertyImpl> getAllowedProperties(DItemUsage usage) {
        checkUsage(usage);

        return IterableUtils.filterAndConvert(PropertyImpl.class,
                                              all,
                                              x -> itemToUsage.getOrDefault(x, getDefaultUsage()) == usage);
    }

    @Override
    public Iterable<AliasImpl> getAllowedAliases(DItemUsage usage) {
        checkUsage(usage);

        return IterableUtils.filterAndConvert(AliasImpl.class,
                                              all,
                                              x -> itemToUsage.getOrDefault(x, getDefaultUsage()) == usage);
    }

    @Override
    public Iterable<NamedDItem> getAllowedItems() {
        return IterableUtils.filter(all,
                                    this::isAllowed);
    }

    @Override
    public Iterable<PropertyImpl> getAllowedProperties() {
        return IterableUtils.filterAndConvert(PropertyImpl.class,
                                              all,
                                              this::isAllowed);
    }

    @Override
    public Iterable<AliasImpl> getAllowedAliases() {
        return IterableUtils.filterAndConvert(AliasImpl.class,
                                              all,
                                              this::isAllowed);
    }

    @Override
    public NamedDItem getAllowedItem(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);

        if (item == null) {
            throw noDeclared(name, CITEM);
        } else {
            final DItemUsage usage = getEffectiveItemUsage(item);
            if (usage == DItemUsage.FORBIDDEN) {
                throw notAllowed(name, CITEM);
            } else {
                return item;
            }
        }
    }

    @Override
    public PropertyImpl getAllowedProperty(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);

        if (item == null) {
            throw noDeclared(name, CITEM);
        } else {
            final DItemUsage usage = getEffectiveItemUsage(item);

            if (usage == DItemUsage.FORBIDDEN) {
                throw notAllowed(name, CITEM);
            } else {
                if (item instanceof final PropertyImpl property) {
                    return property;
                } else {
                    throw availableMismatch(name, CPROPERTY);
                }
            }
        }
    }

    @Override
    public PropertyImpl getAllowedProperty(SName name) {
        return getAllowedProperty(Name.of(name));
    }

    @Override
    public PropertyImpl getAllowedProperty(String name) {
        return getAllowedProperty(Name.of(name));
    }

    @Override
    public AliasImpl getAllowedAlias(Name name) {
        Checks.isNotNull(name, NAME);

        final NamedDItem item = map.get(name);

        if (item == null) {
            throw noDeclared(name, CITEM);
        } else {
            final DItemUsage usage = getEffectiveItemUsage(item);

            if (usage == DItemUsage.FORBIDDEN) {
                throw notAllowed(name, CITEM);
            } else {
                if (item instanceof final AliasImpl alias) {
                    return alias;
                } else {
                    throw availableMismatch(name, CALIAS);
                }
            }
        }
    }

    @Override

    public AliasImpl getAllowedAlias(SName name) {
        return getAlias(Name.of(name));
    }

    @Override
    public AliasImpl getAllowedAlias(String name) {
        return getAlias(Name.of(name));
    }

    @Override
    public Set<String> getWritingRuleNames() {
        return writingRuleNames;
    }

    @Override
    public void printWritingRules(PrintStream out,
                                  int level,
                                  Verbosity verbosity) {
        // Writing Rules
        Printables.indent(out, level);
        out.println("Writing rules (" + getWritingRuleNames().size() + ")");
        if (verbosity != Verbosity.ESSENTIAL) {
            for (final String ruleName : CollectionUtils.toSortedList(getWritingRuleNames())) {
                Printables.indent(out, level + 1);
                out.println("'" + ruleName + "'");
            }
        }
    }
}