package cdc.applic.dictionaries.impl;

import cdc.applic.expressions.literals.SName;

/**
 * Interface used to set Name.
 *
 * @author Damien Carbonne
 *
 * @param <R> The reflexive type.
 */
public interface NameSetter<R extends NameSetter<R>> {
    /**
     * Sets the name.
     *
     * @param name The name.
     * @return This object.
     */
    public R name(SName name);

    /**
     * Sets the name.
     *
     * @param name The name.
     * @return This object.
     */
    public default R name(String name) {
        return name(SName.of(name));
    }
}