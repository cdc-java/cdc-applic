package cdc.applic.dictionaries.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.EnumeratedValue;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.graphs.EdgeDirection;
import cdc.graphs.PartialOrderPosition;
import cdc.graphs.core.GraphPartialOrder;
import cdc.graphs.impl.BasicGraphEdge;
import cdc.graphs.impl.BasicSuperLightGraph;
import cdc.tuples.Tuple2;
import cdc.util.lang.Checks;

public class EnumeratedTypeImpl extends AbstractTypeImpl implements EnumeratedType {
    private final boolean frozen;
    /** List of declared values. */
    private final List<EnumeratedValueImpl> values = new ArrayList<>();
    /** (literal, value) map. Includes synonyms. */
    private final Map<StringValue, EnumeratedValueImpl> map = new HashMap<>();
    /**
     * Set of used short literals.
     * Used to check there is no duplicate.
     */
    private final Set<StringValue> shortLiterals = new HashSet<>();
    /** Convention: {@code v1 -> v2 iff v1 > v2}. */
    private final BasicSuperLightGraph<StringValue> graph = new BasicSuperLightGraph<>();
    private final GraphPartialOrder<StringValue, BasicGraphEdge<StringValue>> graphPO = new GraphPartialOrder<>(graph);
    private StringSet domain = StringSet.EMPTY;
    private final Optional<StringValue> defaultValue;

    protected EnumeratedTypeImpl(Builder builder) {
        super(builder);
        this.frozen = builder.frozen;
        addValues(builder.values);
        for (final Tuple2<StringValue, StringValue> tuple : builder.lessThan) {
            addLessThan(tuple.value0(), tuple.value1());
        }
        this.defaultValue = Optional.ofNullable(builder.defaultValue);

        // Check compliance of default value
        if (builder.defaultValueCheck && defaultValue.isPresent()) {
            Checks.isTrue(isCompliant(defaultValue.get()),
                          "Default value {} is not compliant with {}",
                          this.defaultValue.get(),
                          getDefinition());
        }
    }

    private void addValues(List<EnumeratedValueImpl> values) {
        // The added literals
        // Used to update domain
        final List<StringValue> addedLiterals = new ArrayList<>();
        for (final EnumeratedValueImpl value : values) {
            if (map.containsKey(value.getLiteral())) {
                throw new IllegalArgumentException("Duplicate literal " + value);
            }
            for (final StringValue synonym : value.getLiterals().getSynonyms()) {
                if (map.containsKey(synonym)) {
                    throw new IllegalArgumentException("Duplicate literal synonym " + synonym);
                }
            }
            if (shortLiterals.contains(value.getShortLiteral())) {
                throw new IllegalArgumentException("Duplicate short literal");
            }
            addedLiterals.add(value.getLiteral());
            this.values.add(value);
            map.put(value.getLiteral(), value);
            for (final StringValue synonym : value.getLiterals().getSynonyms()) {
                map.put(synonym, value);
            }
            shortLiterals.add(value.getShortLiteral());
            graph.addNode(value.getLiteral());
        }
        domain = domain.union(StringSet.of(addedLiterals));

        getDictionary().newCachesSerial(true);
    }

    public EnumeratedTypeImpl addValue(EnumeratedValueImpl value) {
        Checks.isNotNull(value, "value");
        addValues(List.of(value));
        return this;
    }

    public EnumeratedTypeImpl addValue(StringValue literal,
                                       StringValue shortLiteral,
                                       int ordinal) {
        return addValue(EnumeratedValueImpl.builder()
                                           .literal(literal)
                                           .shortLiteral(shortLiteral)
                                           .ordinal(ordinal)
                                           .build());
    }

    public EnumeratedTypeImpl addValue(String literal,
                                       String shortLiteral,
                                       int ordinal) {
        return addValue(StringValue.of(literal),
                        StringValue.of(shortLiteral),
                        ordinal);
    }

    public EnumeratedTypeImpl addValue(String literal,
                                       int ordinal) {
        return addValue(literal, literal, ordinal);
    }

    public void addLessThan(StringValue literal1,
                            StringValue literal2) {
        Checks.isNotNull(literal1, "literal1");
        Checks.isNotNull(literal2, "literal2");
        Checks.isFalse(literal1.equals(literal2), "Can not link {} to itself.", literal1);

        final StringValue ref1 = map.get(literal1).getLiteral();
        final StringValue ref2 = map.get(literal2).getLiteral();

        // This checks that tips are valid
        // x -> y mean x > y, so we invert literal1 and literal2
        final BasicGraphEdge<StringValue> edge = graph.addEdge(ref2, ref1);
        if (!graphPO.isPartialOrTotalOrder()) {
            graph.removeEdge(edge);
            throw new IllegalArgumentException("Can not link " + literal1 + " to " + literal2 + ", this would create a cycle.");
        }
        // This should not be necessary at the moment, but it may one day
        getDictionary().newCachesSerial(true);
    }

    public void addLessThan(String literal1,
                            String literal2) {
        addLessThan(StringValue.of(literal1),
                    StringValue.of(literal2));
    }

    /**
     * @return A Set of ordered pairs. For each pair {@code (a, b), a < b}.
     */
    public Set<Tuple2<EnumeratedValue, EnumeratedValue>> getOrderConstraints() {
        final Set<Tuple2<EnumeratedValue, EnumeratedValue>> set = new HashSet<>();
        for (final BasicGraphEdge<StringValue> edge : graph.getEdges()) {
            // See comment above for inversion of values
            set.add(Tuple2.of(getValue(edge.getTarget()), getValue(edge.getSource())));
        }
        return set;
    }

    @Override
    public boolean isFrozen() {
        return frozen;
    }

    @Override
    public StringSet getDomain() {
        return domain;
    }

    @Override
    public Optional<StringValue> getDefaultValue() {
        return defaultValue;
    }

    @Override
    public List<EnumeratedValueImpl> getValues() {
        return values;
    }

    @Override
    public EnumeratedValueImpl getValue(StringValue name) {
        Checks.isNotNull(name, "name");
        return map.get(name);
    }

    @Override
    public EnumeratedValueImpl getValue(String name) {
        return getValue(StringValue.of(name));
    }

    @Override
    public Set<StringValue> getDefinedLessThan(StringValue literal) {
        return graph.getConnectedNodes(literal, EdgeDirection.INGOING);
    }

    @Override
    public PartialOrderPosition partialCompare(StringValue left,
                                               StringValue right) {
        return graphPO.compare(left, right);
    }

    @Override
    public StringSet toSet(StringValue value,
                           PartialOrderPosition position) {
        return StringSet.of(graphPO.getAllNodes(value, position));
    }

    @Override
    public boolean isRelevantPartialOrder() {
        return graph.hasEdges();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(),
                            getDescription(),
                            getS1000DProductIdentifier(),
                            getS1000DPropertyType(),
                            isFrozen(),
                            getValues());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof EnumeratedTypeImpl)) {
            return false;
        }
        final EnumeratedTypeImpl other = (EnumeratedTypeImpl) object;
        return Objects.equals(getName(), other.getName())
                && Objects.equals(getDescription(), other.getDescription())
                && getS1000DProductIdentifier() == other.getS1000DProductIdentifier()
                && getS1000DPropertyType() == other.getS1000DPropertyType()
                && isFrozen() == other.isFrozen()
                && Objects.equals(getValues(), other.getValues());
    }

    static Builder builder(RegistryImpl registry) {
        return new Builder(registry);
    }

    public static class Builder extends AbstractTypeImpl.Builder<Builder> {
        protected boolean frozen = false;
        protected final List<EnumeratedValueImpl> values = new ArrayList<>();
        protected final List<Tuple2<StringValue, StringValue>> lessThan = new ArrayList<>();
        protected StringValue defaultValue = null;
        // This is a temporary hack
        protected boolean defaultValueCheck = true;

        protected Builder(RegistryImpl registry) {
            super(registry);
        }

        protected EnumeratedValueImpl add(EnumeratedValueImpl value) {
            this.values.add(value);
            return value;
        }

        @Override
        public Builder self() {
            return this;
        }

        public Builder frozen(boolean frozen) {
            this.frozen = frozen;
            return self();
        }

        public EnumeratedValueImpl.Builder value() {
            return EnumeratedValueImpl.builder(this);
        }

        public Builder literals(String... literals) {
            int ordinal = 0;
            for (final String litreral : literals) {
                value().literal(litreral).ordinal(ordinal).back();
                ordinal++;
            }
            return this;
        }

        public Builder lessThan(StringValue literal1,
                                StringValue literal2) {
            this.lessThan.add(Tuple2.of(literal1, literal2));
            return this;
        }

        public Builder lessThan(String literal1,
                                String literal2) {
            return lessThan(StringValue.of(literal1),
                            StringValue.of(literal2));
        }

        public Builder defaultValue(StringValue defaultValue) {
            this.defaultValue = defaultValue;
            return self();
        }

        public Builder defaultValue(String defaultValue) {
            return defaultValue(defaultValue == null ? null : StringValue.of(defaultValue));
        }

        /**
         * Temporary hack for RepositoryOffice used to disable check of default value.
         *
         * @return This builder.
         */
        public Builder hackDisableDefaultValueCheck() {
            this.defaultValueCheck = false;
            return self();
        }

        @Override
        public EnumeratedTypeImpl build() {
            return registry.types.addType(new EnumeratedTypeImpl(this));
        }
    }
}