package cdc.applic.renaming;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.LexicalException;
import cdc.applic.expressions.SyntacticException;
import cdc.applic.expressions.ast.Node;

/**
 * Interface implemented by classes that can rename parts of an expression or an AST.
 *
 * @author Damien Carbonne
 */
public interface Renamer {
    /**
     * @return The renaming profile.
     */
    public RenamingProfile getProfile();

    /**
     * Renames a node tree.
     *
     * @param node The root node.
     * @return The renaming of {@code node}.
     */
    public Node rename(Node node);

    /**
     * Renames an expression.
     * <p>
     * Spaces and operators should be preserved.
     *
     * @param expression The expression.
     * @return The renaming of {@code expression}.
     * @throws LexicalException When {@code expression} is lexically incorrect.
     * @throws SyntacticException When {@code expression} is syntactically incorrect.
     */
    public Expression rename(Expression expression);
}