package cdc.applic.renaming;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;

/**
 * List of renaming steps.
 *
 * @author Damien Carbonne
 */
public class RenamingProfile {
    private final List<RenamingStep> steps;
    private final Set<Object> from = new HashSet<>();

    protected RenamingProfile(List<RenamingStep> steps) {
        this.steps = steps;
        for (final RenamingStep step : steps) {
            from.add(step.getFrom());
        }
    }

    /**
     * @return The renaming steps.
     */
    public final List<RenamingStep> getSteps() {
        return steps;
    }

    /**
     * @param name The name.
     * @return {@code true} if {@code name} is a candidate for renaming.
     */
    public boolean matches(Name name) {
        return from.contains(name) || from.contains(name.getPrefix());
    }

    /**
     * Returns the renaming of a name.
     * <p>
     * Steps are applied in sequence.
     *
     * @param name The name.
     * @return The renaming of {@code name}.
     */
    public Name rename(Name name) {
        Name n = name;
        if (matches(name)) {
            for (final RenamingStep step : steps) {
                n = step.rename(n);
            }
        }
        return n;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private final List<RenamingStep> steps = new ArrayList<>();

        protected Builder() {
        }

        /**
         * Adds a step to rename a prefix.
         *
         * @param from The old prefix.
         * @param to The new prefix, possibly {@code null}.
         * @return This Builder.
         */
        public Builder prefix(SName from,
                              SName to) {
            steps.add(new PrefixRenaming(from, to));
            return this;
        }

        /**
         * Adds a step to rename a prefix.
         *
         * @param from The old prefix.
         * @param to The new prefix, possibly {@code null}.
         * @return This Builder.
         */
        public Builder prefix(String from,
                              String to) {
            return prefix(SName.of(from), SName.of(to));
        }

        /**
         * Adds a step to rename a name.
         *
         * @param from The old name (with or without prefix).
         * @param to The new name (with or without prefix).
         * @return This Builder.
         */
        public Builder name(Name from,
                            Name to) {
            steps.add(new NameRenaming(from, to));
            return this;
        }

        /**
         * Adds a step to rename a name.
         *
         * @param from The old name (with or without prefix).
         * @param to The new name (with or without prefix).
         * @return This Builder.
         */
        public Builder name(String from,
                            String to) {
            return name(Name.of(from), Name.of(to));
        }

        public RenamingProfile build() {
            return new RenamingProfile(Collections.unmodifiableList(new ArrayList<>(steps)));
        }
    }
}