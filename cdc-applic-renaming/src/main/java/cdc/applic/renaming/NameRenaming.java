package cdc.applic.renaming;

import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

/**
 * Definition of a NamedDItem renaming.
 *
 * @author Damien Carbonne
 */
public final class NameRenaming implements RenamingStep {
    private final Name from;
    private final Name to;

    public NameRenaming(Name from,
                        Name to) {
        this.from = Checks.isNotNull(from, "from");
        this.to = Checks.isNotNull(to, "to");
    }

    @Override
    public Name getFrom() {
        return from;
    }

    @Override
    public Name getTo() {
        return to;
    }

    @Override
    public Name rename(Name name) {
        if (name.equals(from)) {
            return to;
        } else {
            return name;
        }
    }

    @Override
    public String toString() {
        return "[Name " + from + " -> " + to + "]";
    }
}