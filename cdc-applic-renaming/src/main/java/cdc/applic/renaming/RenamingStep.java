package cdc.applic.renaming;

import cdc.applic.expressions.literals.Name;

/**
 * Marking interface of renaming steps.
 *
 * @author Damien Carbonne
 */
public interface RenamingStep {
    /**
     * @return The input value that must be renamed.
     */
    public Object getFrom();

    /**
     * @return The renamed output value.
     */
    public Object getTo();

    /**
     * @param name The name.
     * @return The renaming of {@code name}.
     */
    public Name rename(Name name);
}