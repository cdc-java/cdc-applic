package cdc.applic.renaming;

import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.util.lang.Checks;

/**
 * Definition of a prefix renaming.
 *
 * @author Damien Carbonne
 */
public final class PrefixRenaming implements RenamingStep {
    private final SName from;
    private final SName to;

    public PrefixRenaming(SName from,
                          SName to) {
        this.from = Checks.isNotNull(from, "from");
        this.to = to;
    }

    @Override
    public SName getFrom() {
        return from;
    }

    @Override
    public SName getTo() {
        return to;
    }

    @Override
    public Name rename(Name name) {
        if (from.equals(name.getPrefix())) {
            if (to == null) {
                return name.removePrefix();
            } else {
                return Name.of(to, name.getLocal());
            }
        } else {
            return name;
        }
    }

    @Override
    public String toString() {
        return "[Prefix " + from + " -> " + (to == null ? "" : to) + "]";
    }
}