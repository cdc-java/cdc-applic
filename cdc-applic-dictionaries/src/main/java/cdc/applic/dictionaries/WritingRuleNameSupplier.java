package cdc.applic.dictionaries;

/**
 * Interface implemented by classes that can supply a writing rule name.
 *
 * @author Damien Carbonne
 */
@FunctionalInterface
public interface WritingRuleNameSupplier {
    public String getRuleName();
}