package cdc.applic.dictionaries;

/**
 * Interface implemented by classed of objects that have a designation.
 * <p>
 * The designation is intended to designate the object in messages, instead of toString().
 * Designation should be clear and sort, and only contains
 * the relevant information useful to recognize the designated object.
 *
 * @author Damien Carbonne
 */
public interface Designated {
    /**
     * @return The designation.
     */
    public String getDesignation();
}