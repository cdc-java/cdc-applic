package cdc.applic.dictionaries;

public interface DictionaryConstraints {
    /**
     * @return An {@link Iterable} of {@link Constraint Constraints} <em>declared</em> in this {@link Dictionary}.
     */
    public Iterable<? extends Constraint> getConstraints();
}