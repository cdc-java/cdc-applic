package cdc.applic.dictionaries;

/**
 * A Registry is a root dictionary.
 * <p>
 * It has an absolute name.<br>
 * It can extend zero or more dictionaries.
 *
 * @author Damien Carbonne
 */
public interface Registry extends Dictionary, DictionaryNamingConventions, DictionaryTypes {
    @Override
    Repository getOwner();
}