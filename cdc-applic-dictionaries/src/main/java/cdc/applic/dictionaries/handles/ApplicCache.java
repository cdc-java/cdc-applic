package cdc.applic.dictionaries.handles;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.Registry;
import cdc.util.debug.ControlledPrintable;
import cdc.util.lang.Checks;

/**
 * Base Cache class.
 * <p>
 * A Cache can be invalidated.<br>
 * Validating a Cache can be done just after invalidation, or lazily.
 *
 * @author Damien Carbonne
 */
public abstract class ApplicCache implements ControlledPrintable {
    private long serial = -1L;
    private final DictionaryHandle handle;
    private final ApplicCacheId id;

    /**
     * Creates a Cache.
     *
     * @param handle The handle to which it belongs.
     * @param tag The optional tag.
     *            It is used to differentiate caches that have the same class but different parameters.
     */
    protected ApplicCache(DictionaryHandle handle,
                          Object tag) {
        Checks.isNotNull(handle, "handle");
        this.handle = handle;
        this.id = new ApplicCacheId(getClass(), tag);
    }

    /**
     * If serial of associated dictionary does not match the serial of this cache,
     * then this cache is invalidated.<br>
     * The serial of this cache is also set to the serial of associated dictionary.
     * <p>
     * <b>WARNING:</b> This should be called before any use of cached data.
     */
    protected void checkSerial() {
        final long dictionarySerial = handle.getDictionary().getCachesSerial();
        if (this.serial != dictionarySerial) {
            invalidate();
            this.serial = dictionarySerial;
        }
    }

    /**
     * @return The current serial.
     *         If is does not match the dictionary serial, it means this cache is invalid.
     */
    public final long getSerial() {
        return serial;
    }

    /**
     * @return The Handle of this Cache.
     */
    public final DictionaryHandle getDictionaryHandle() {
        return handle;
    }

    /**
     * @return The identifier of this Cache.
     */
    public ApplicCacheId getId() {
        return id;
    }

    /**
     * @return The Registry of this Cache.
     */
    public final Registry getRegistry() {
        return handle.getRegistry();
    }

    /**
     * @return The Dictionary of this Cache.
     */
    public final Dictionary getDictionary() {
        return handle.getDictionary();
    }

    /**
     * Called to invalidate (and generally immediately clear, but not necessarily) this cache.
     * <p>
     * If clearing is not immediate, cache values are invalid and can not be used.<br>
     * They should be cleared and recomputed before next use.<br>
     * Practically, clearing should be done immediately, but new computations should be done lazily.
     */
    public abstract void invalidate();

    /**
     * Clears the cache.
     */
    public abstract void clear();
}