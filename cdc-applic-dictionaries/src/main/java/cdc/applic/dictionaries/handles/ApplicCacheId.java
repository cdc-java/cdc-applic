package cdc.applic.dictionaries.handles;

import java.util.Objects;

import cdc.util.lang.Checks;

/**
 * Cache identifier.
 * <p>
 * It is a {@code (cache class, tag)} pair, where {@code tag} is optional.
 *
 * @author Damien Carbonne
 */
public final class ApplicCacheId implements Comparable<ApplicCacheId> {
    private final Class<? extends ApplicCache> cls;
    private final Object tag;

    public ApplicCacheId(Class<? extends ApplicCache> cls,
                         Object tag) {
        Checks.isNotNull(cls, "cls");
        this.cls = cls;
        this.tag = tag;
    }

    public ApplicCacheId(Class<? extends ApplicCache> cls) {
        this(cls, null);
    }

    /**
     * @return The class of the identified cache.
     */
    public Class<? extends ApplicCache> getCacheClass() {
        return cls;
    }

    /**
     * @return The tag (possibly {@code null}).
     */
    public Object getTag() {
        return tag;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cls, tag);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof ApplicCacheId)) {
            return false;
        }
        final ApplicCacheId other = (ApplicCacheId) object;
        return Objects.equals(cls, other.cls)
                && Objects.equals(tag, other.tag);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(getCacheClass().getSimpleName());
        if (getTag() != null) {
            builder.append(" " + getTag());
        }
        return builder.toString();
    }

    @Override
    public int compareTo(ApplicCacheId o) {
        return toString().compareTo(o.toString());
    }
}