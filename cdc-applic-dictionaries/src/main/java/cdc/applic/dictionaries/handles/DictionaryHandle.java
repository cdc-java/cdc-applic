package cdc.applic.dictionaries.handles;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.Registry;
import cdc.util.debug.ControlledPrintable;
import cdc.util.debug.Verbosity;
import cdc.util.lang.Checks;
import cdc.util.lang.CollectionUtils;

/**
 * A DictionaryHandle is used to associate cached data to a Dictionary.
 *
 * @author Damien Carbonne
 */
public final class DictionaryHandle implements ControlledPrintable {
    /** The associated Dictionary. */
    private final Dictionary dictionary;
    /** The associated caches. */
    private final Map<ApplicCacheId, ApplicCache> map = new HashMap<>();

    /**
     * Creates a DictionaryHandle associated to a dictionary.
     *
     * @param dictionary The dictionary.
     */
    public DictionaryHandle(Dictionary dictionary) {
        Checks.isNotNull(dictionary, "dictionary");
        this.dictionary = dictionary;
    }

    /**
     * @return The Registry of this DictionaryHandle.
     */
    public Registry getRegistry() {
        return dictionary.getRegistry();
    }

    /**
     * @return The Dictionary of this DictionaryHandle.
     */
    public Dictionary getDictionary() {
        return dictionary;
    }

    public <C extends ApplicCache> C computeIfAbsent(Class<C> cls,
                                                     ApplicCacheId id,
                                                     BiFunction<DictionaryHandle, Object, C> ctor) {
        Checks.isNotNull(cls, "cls");
        Checks.isNotNull(id, "id");
        Checks.isTrue(cls.equals(id.getCacheClass()), "cls/id mismatch");
        return cls.cast(map.computeIfAbsent(id,
                                            c -> {
                                                final C cache = ctor.apply(this, id.getTag());
                                                Checks.isNotNull(cache, "cache");
                                                return cache;
                                            }));
    }

    public <C extends ApplicCache> C computeIfAbsent(Class<C> cls,
                                                     Function<DictionaryHandle, C> ctor) {
        Checks.isNotNull(cls, "cls");
        return cls.cast(map.computeIfAbsent(new ApplicCacheId(cls),
                                            c -> {
                                                final C cache = ctor.apply(this);
                                                Checks.isNotNull(cache, "cache");
                                                return cache;
                                            }));
    }

    public Set<ApplicCacheId> getIds() {
        return map.keySet();
    }

    public ApplicCache getCache(ApplicCacheId id) {
        return map.get(id);
    }

    /**
     * Invalidate all associated caches.
     */
    public void invalidate() {
        for (final ApplicCache cache : map.values()) {
            cache.invalidate();
        }
    }

    /**
     * Clears all computation caches.
     */
    public void clearComputationsCaches() {
        for (final ApplicCache cache : map.values()) {
            if (cache instanceof ComputationCache) {
                cache.clear();
            }
        }
    }

    /**
     * Removes (and clears) all computation caches.
     */
    public void removeComputationsCaches() {
        // Ids of cache to remove
        final List<ApplicCacheId> ids = new ArrayList<>();
        for (final ApplicCache cache : map.values()) {
            if (cache instanceof ComputationCache) {
                cache.clear();
                ids.add(cache.getId());
            }
        }
        for (final ApplicCacheId id : ids) {
            map.remove(id);
        }
    }

    public int getNumberOfComputationCaches() {
        int count = 0;
        for (final ApplicCache cache : map.values()) {
            if (cache instanceof ComputationCache) {
                count++;
            }
        }
        return count;
    }

    public int getNumberOfComputationCacheEntries() {
        int count = 0;
        for (final ApplicCache cache : map.values()) {
            if (cache instanceof ComputationCache) {
                count += ((ComputationCache<?>) cache).size();
            }
        }
        return count;
    }

    @Override
    public void print(PrintStream out,
                      int level,
                      Verbosity verbosity) {
        indent(out, level);
        out.println("Dictionary Handle '" + dictionary.getName() + "' " + map.size() + " Cache(s)");
        if (verbosity == Verbosity.ESSENTIAL) {
            for (final ApplicCacheId id : CollectionUtils.toSortedList(map.keySet())) {
                final ApplicCache cache = map.get(id);
                if (!(cache instanceof ComputationCache)) {
                    indent(out, level + 1);
                    out.println(id);
                    cache.print(out, level + 2, verbosity);
                }
            }
            indent(out, level + 1);
            out.println(getNumberOfComputationCaches() + " Computation Cache(s), containing "
                    + getNumberOfComputationCacheEntries() + " Entry(ies)");

        } else {
            for (final ApplicCacheId id : CollectionUtils.toSortedList(map.keySet())) {
                indent(out, level + 1);
                out.println(id);
                final ApplicCache cache = map.get(id);
                cache.print(out, level + 2, verbosity);
            }
        }
    }
}