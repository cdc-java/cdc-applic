package cdc.applic.dictionaries.handles;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.Node;
import cdc.util.debug.Verbosity;
import cdc.util.lang.CollectionUtils;

/**
 * A cache of computed results associated to a {@link Node}.
 *
 * @author Damien Carbonne
 *
 * @param <R> The computed type.
 */
public class ComputationCache<R> extends ApplicCache {
    private final Map<String, R> map = new HashMap<>();

    public ComputationCache(DictionaryHandle handle,
                            Object tag) {
        super(handle, tag);
    }

    /**
     * @param node The node.
     * @return The String key corresponding to {@code node}.
     */
    private static String toKey(Node node) {
        return node.toInfix(Formatting.SHORT_NARROW);
    }

    @Override
    public void invalidate() {
        clear();
    }

    @Override
    public void clear() {
        map.clear();
    }

    /**
     * @return The number of entries of this cache.<br>
     *         <b>WARNING:</b>This does not modify or invalidate cache.
     */
    public int size() {
        return map.size();
    }

    /**
     * @param node The node.
     * @return {@code true} if a value is associated to {@code node} in this cache. <br>
     *         <b>WARNING:</b>This does not modify or invalidate cache.
     */
    public final boolean containsKey(Node node) {
        return map.containsKey(toKey(node));
    }

    /**
     * @param node The node.
     * @return The value associated to {@code node} or {@code null}.<br>
     *         <b>WARNING:</b>This does not modify or invalidate cache.
     */
    public final R get(Node node) {
        return map.get(toKey(node));
    }

    /**
     * Sets the value associated to a node.
     * <p>
     * <b>WARNING:</b>This does not modify or invalidate cache.
     *
     * @param node The node.
     * @param value The value
     * @return The passed {@code value}.
     */
    public final R put(Node node,
                       R value) {
        return map.put(toKey(node), value);
    }

    /**
     * Starts by calling {@link #checkSerial()}.<br>
     * Then, if {@code node} is not already associated to a value (or is mapped to {@code null}),
     * attempts to compute its value using the given mapping function and enters it into this map unless {@code null}.
     *
     * @param node The node.
     * @param mappingFunction The mapping function to compute a value.
     * @return The current (existing or computed) value associated with the {@code node},
     *         possibly {@code null}.
     */
    public final R computeIfAbsent(Node node,
                                   Function<Node, R> mappingFunction) {
        checkSerial();
        final String key = toKey(node);
        return map.computeIfAbsent(key, k -> mappingFunction.apply(node));
    }

    @Override
    public void print(PrintStream out,
                      int level,
                      Verbosity verbosity) {
        indent(out, level);
        out.println("Entries (" + map.size() + ")");
        if (verbosity != Verbosity.ESSENTIAL) {
            for (final String key : CollectionUtils.toSortedList(map.keySet())) {
                indent(out, level);
                out.println(key + ":" + map.get(key));
            }
        }
    }
}