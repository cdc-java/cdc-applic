package cdc.applic.dictionaries;

/**
 * A Policy is a child dictionary that has one and only one parent dictionary.
 * <p>
 * It has a local name.
 *
 * @author Damien Carbonne
 */
public interface Policy extends Dictionary {
    @Override
    Dictionary getOwner();
}