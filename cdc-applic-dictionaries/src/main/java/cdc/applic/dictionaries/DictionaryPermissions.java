package cdc.applic.dictionaries;

import java.util.Optional;
import java.util.Set;

import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;

public interface DictionaryPermissions {
    /**
     * @return The default usage to be used with this dictionary.
     */
    public DItemUsage getDefaultUsage();

    /**
     * @return {@code true} if this dictionary has specific type usages.
     */
    public boolean hasTypeUsages();

    /**
     * Returns the usage of a type in this Dictionary.
     *
     * @param type The type.
     * @return The default usage to be used with {@code type}, possibly {@code null}.
     */
    public DItemUsage getTypeUsage(Type type);

    /**
     * Returns the usage of a type in this Dictionary.
     *
     * @param name The type name.
     * @return The default usage to be used with type named {@code name}, possibly {@code null}.
     */
    public DItemUsage getTypeUsage(Name name);

    /**
     * Returns the usage of a type in this Dictionary.
     *
     * @param name The type name.
     * @return The default usage to be used with type named {@code name}, possibly {@code null}.
     */
    public default DItemUsage getTypeUsage(SName name) {
        return getTypeUsage(Name.of(name));
    }

    /**
     * Returns the usage of a type in this Dictionary.
     *
     * @param name The type name.
     * @return The default usage to be used with type named {@code name}, possibly {@code null}.
     */
    public default DItemUsage getTypeUsage(String name) {
        return getTypeUsage(Name.of(name));
    }

    /**
     * @return {@code true} if this dictionary has specific item usages.
     */
    public boolean hasItemUsages();

    /**
     * Returns the usage of an item in this Dictionary.
     *
     * @param item The item.
     * @return The usage of {@code item}, possibly {@code null}.
     */
    public DItemUsage getItemUsage(NamedDItem item);

    /**
     * Returns the usage of an item in this Dictionary.
     *
     * @param name The item name.
     * @return The usage of item named  {@code name}, possibly {@code null}.
     */
    public DItemUsage getItemUsage(Name name);

    /**
     * Returns the usage of an item in this Dictionary.
     *
     * @param name The item name.
     * @return The usage of item named  {@code name}, possibly {@code null}.
     */
    public default DItemUsage getItemUsage(SName name) {
        return getItemUsage(Name.of(name));
    }

    /**
     * Returns the usage of an item in this Dictionary.
     *
     * @param name The item name.
     * @return The usage of item named  {@code name}, possibly {@code null}.
     */
    public default DItemUsage getItemUsage(String name) {
        return getItemUsage(Name.of(name));
    }

    /**
     * Returns the effective usage of an item in this Dictionary.
     * <p>
     * If a usage is explicitly set for {@code item}, then this value is returned.
     * Otherwise, if the item is a property, and a usage is explicitly set for that type, the this value is returned.
     * Otherwise, the default usage for the dictionary is returned.
     *
     * @param item The item.
     * @return The usage of {@code item} in this Dictionary.<br>
     *         If {@code item} is unrelated, returns {@link DItemUsage#FORBIDDEN}.
     * @throws IllegalArgumentException When {@code item} is {@code null}.
     */
    public DItemUsage getEffectiveItemUsage(NamedDItem item);

    /**
     * Returns the effective usage of an item in this Dictionary.
     *
     * @param name The item name.
     * @return The usage of the item named {@code name} in this Dictionary.<br>
     *         If no item named {@code name} is known, returns {@link DItemUsage#FORBIDDEN}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     */
    public DItemUsage getEffectiveItemUsage(Name name);

    /**
     * Returns the effective usage of an item in this Dictionary.
     *
     * @param name The item name.
     * @return The usage of the item named {@code name} in this Dictionary.<br>
     *         If no item named {@code name} is known, returns {@link DItemUsage#FORBIDDEN}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     */
    public default DItemUsage getEffectiveItemUsage(SName name) {
        return getEffectiveItemUsage(Name.of(name));
    }

    /**
     * Returns the effective usage of an item in this Dictionary.
     *
     * @param name The item name.
     * @return The usage of the item named {@code name} in this Dictionary.<br>
     *         If no item named {@code name} is known, returns {@link DItemUsage#FORBIDDEN}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     */
    public default DItemUsage getEffectiveItemUsage(String name) {
        return getEffectiveItemUsage(Name.of(name));
    }

    /**
     * Returns {@code true} if an item is allowed by this Dictionary.
     *
     * @param item The item.
     * @return {@code true} if a {@code item} is allowed by this Dictionary.
     * @throws IllegalArgumentException When {@code item} is {@code null}.
     */
    public boolean isAllowed(NamedDItem item);

    /**
     * Returns {@code true} if an item with a given name is allowed by this Dictionary.
     *
     * @param name The name.
     * @return {@code true} if an item named {@code name} is allowed by this Dictionary.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     */
    public boolean hasAllowedItem(Name name);

    /**
     * Returns an Optional of the allowed item that has a given name.
     *
     * @param name The item name.
     * @return An Optional of the available item named {@code name} if it is allowed.<br>
     *         If {@code name} designates an available item which is FORBIDDEN, the returned Optional is empty.<br>
     *         If {@code name} designates an unrelated item, the returned Optional is empty.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     */
    public Optional<NamedDItem> getOptionalAllowedItem(Name name);

    /**
     * Returns an Optional of the allowed item that has a given name.
     *
     * @param name The item name.
     * @return An Optional of the available item named {@code name} if it is allowed.<br>
     *         If {@code name} designates an available item which is FORBIDDEN, the returned Optional is empty.<br>
     *         If {@code name} designates an unrelated item, the returned Optional is empty.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     */
    public default Optional<NamedDItem> getOptionalAllowedItem(SName name) {
        return getOptionalAllowedItem(Name.of(name));
    }

    /**
     * Returns an Optional of the allowed item that has a given name.
     *
     * @param name The item name.
     * @return An Optional of the available item named {@code name} if it is allowed.<br>
     *         If {@code name} designates an available item which is FORBIDDEN, the returned Optional is empty.<br>
     *         If {@code name} designates an unrelated item, the returned Optional is empty.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     */
    public default Optional<NamedDItem> getOptionalAllowedItem(String name) {
        return getOptionalAllowedItem(Name.of(name));
    }

    /**
     * Returns an Optional of the allowed property that has a given name.
     *
     * @param name The property name.
     * @return An Optional of the property named {@code name} if it is allowed.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws ApplicException When {@code name} designates an available item that is not a property.
     */
    public Optional<? extends Property> getOptionalAllowedProperty(Name name);

    /**
     * Returns an Optional of the allowed property that has a given name.
     *
     * @param name The property name.
     * @return An Optional of the property named {@code name} if it is allowed.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws ApplicException When {@code name} designates an available item that is not a property.
     */
    public Optional<? extends Property> getOptionalAllowedProperty(SName name);

    /**
     * Returns an Optional of the allowed property that has a given name.
     *
     * @param name The property name.
     * @return An Optional of the property named {@code name} if it is allowed.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws ApplicException When {@code name} designates an available item that is not a property.
     */
    public Optional<? extends Property> getOptionalAllowedProperty(String name);

    /**
     * Returns an Optional of the allowed alias that has a given name.
     *
     * @param name The alias name.
     * @return An Optional of the alias named {@code name} if it is allowed.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws ApplicException When {@code name} designates an available item that is not an alias.
     */
    public Optional<? extends Alias> getOptionalAllowedAlias(Name name);

    /**
     * Returns an Optional of the allowed alias that has a given name.
     *
     * @param name The alias name.
     * @return An Optional of the alias named {@code name} if it is allowed.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws ApplicException When {@code name} designates an available item that is not an alias.
     */
    public Optional<? extends Alias> getOptionalAllowedAlias(SName name);

    /**
     * Returns an Optional of the allowed alias that has a given name.
     *
     * @param name The alias name.
     * @return An Optional of the alias named {@code name} if it is allowed.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws ApplicException When {@code name} designates an available item that is not an alias.
     */
    public Optional<? extends Alias> getOptionalAllowedAlias(String name);

    public NamedDItem getAllowedItem(Name name);

    public Property getAllowedProperty(Name name);

    public Property getAllowedProperty(SName name);

    public Property getAllowedProperty(String name);

    public Alias getAllowedAlias(Name name);

    public Alias getAllowedAlias(SName name);

    public Alias getAllowedAlias(String name);

    /**
     * Returns an Iterable of all items that are allowed by this Dictionary for a given a usage.
     *
     * @param usage The usage.
     * @return An Iterable of all items that are allowed by this Dictionary for {@code usage}.
     * @throws IllegalArgumentException When {@code usage} is {@code null} or {@link DItemUsage#FORBIDDEN}.
     */
    public Iterable<NamedDItem> getAllowedItems(DItemUsage usage);

    /**
     * Returns an Iterable of all Properties that are allowed by this Dictionary for a given a usage.
     *
     * @param usage The usage.
     * @return An Iterable of all Properties that are allowed by this Dictionary for {@code usage}.
     */
    public Iterable<? extends Property> getAllowedProperties(DItemUsage usage);

    /**
     * Returns an Iterable of all Aliases that are allowed by this Dictionary for a given a usage.
     *
     * @param usage The usage.
     * @return An Iterable of all Aliases that are allowed by this Dictionary for {@code usage}.
     */
    public Iterable<? extends Alias> getAllowedAliases(DItemUsage usage);

    /**
     * @return An Iterable of all items that are allowed by this Dictionary.<br>
     *         Their usage is either {@link DItemUsage#OPTIONAL},
     *         {@link DItemUsage#RECOMMENDED} or {@link DItemUsage#MANDATORY}.
     */
    public Iterable<NamedDItem> getAllowedItems();

    /**
     * @return An Iterable of all Properties that are allowed by this Dictionary.
     */
    public Iterable<? extends Property> getAllowedProperties();

    /**
     * @return An Iterable of all Aliases that are allowed by this Dictionary.
     */
    public Iterable<? extends Alias> getAllowedAliases();

    /**
     * @return All named of writing rules that are enabled for this Dictionary.
     */
    public Set<String> getWritingRuleNames();
}