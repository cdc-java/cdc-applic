package cdc.applic.dictionaries;

import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.IllegalOperationException;
import cdc.applic.expressions.checks.CheckedData;

/**
 * Interface giving read access to an expression associated to
 * an optional context and optional dictionary handle.
 *
 * @author Guillaume Forget
 * @author Damien Carbonne
 */
public interface ConstrainedExpression {
    /**
     * @return {@code true} if the associated expression can not be set.
     */
    public boolean isReadOnly();

    /**
     * @return The expression.
     */
    public Expression getExpression();

    /**
     * Sets the expression.
     * <p>
     * Conformity of {@code expression} with associated dictionary is <b>NOT</b> checked.
     *
     * @param expression The new expression.
     * @throws IllegalArgumentException When {@code expression} is {@code null}.
     * @throws IllegalOperationException When this object is read-only.
     */
    public void setExpressionUnchecked(Expression expression);

    /**
     * Sets the expression.
     * <p>
     * Conformity of {@code expression} with associated dictionary is
     * checked (if there is an associated dictionary).
     *
     * @param expression The new expression.
     * @throws IllegalArgumentException When {@code expression} is {@code null}.
     * @throws ApplicException When a conformity issue is detected.
     * @throws IllegalOperationException When this object is read-only.
     */
    public default void setExpression(Expression expression) {
        final DictionaryHandle handle = getDictionaryHandleOrNull();
        if (handle != null) {
            WritingRuleChecker.checkCompliance(handle, new CheckedData(null, expression));
        }
        setExpressionUnchecked(expression);
    }

    /**
     * @return The associated DictionaryHandle or {@code null}.
     */
    public DictionaryHandle getDictionaryHandleOrNull();

    /**
     * @return The associated DictionaryHandle.
     * @throws IllegalOperationException When there is no associated DictionaryHandle.
     */
    public default DictionaryHandle getDictionaryHandle() {
        final DictionaryHandle result = getDictionaryHandleOrNull();
        if (result == null) {
            throw new IllegalOperationException("No associated dictionary handle");
        }
        return result;
    }

    /**
     * @return {@code true} if there is an associated dictionary handle.
     */
    public default boolean hasDictionaryHandle() {
        return getDictionaryHandleOrNull() != null;
    }

    /**
     * @return The associated dictionary or {@code null}.
     */
    public default Dictionary getDictionaryOrNull() {
        final DictionaryHandle handle = getDictionaryHandleOrNull();
        return handle == null ? null : handle.getDictionary();
    }

    /**
     * @return The associated dictionary.
     * @throws IllegalOperationException When there is no associated dictionary.
     */
    public default Dictionary getDictionary() {
        final Dictionary result = getDictionaryOrNull();
        if (result == null) {
            throw new IllegalOperationException("No associated dictionary");
        }
        return result;
    }

    /**
     * @return {@code true} if there is an associated dictionary.
     */
    public default boolean hasDictionary() {
        return getDictionaryOrNull() != null;
    }

    /**
     * @return The associated context or {@code null}.
     */
    public ConstrainedExpression getContextOrNull();

    /**
     * @return The associated context.
     * @throws IllegalOperationException When there is no associated context.
     */
    public default ConstrainedExpression getContext() {
        final ConstrainedExpression result = getContextOrNull();
        if (result == null) {
            throw new IllegalOperationException("No associated context");
        }
        return result;
    }

    /**
     * @return {@code true} if there is an associated context.
     */
    public default boolean hasContext() {
        return getContextOrNull() != null;
    }
}