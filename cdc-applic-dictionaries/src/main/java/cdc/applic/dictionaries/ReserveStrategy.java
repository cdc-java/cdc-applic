package cdc.applic.dictionaries;

import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.ModifiableType;
import cdc.applic.dictionaries.types.Type;

/**
 * Enumeration of possible reserve strategies for types definitions.
 * <p>
 * Technically, the definition of a type can:
 * <ol>
 * <li>change over time: {@link ModifiableType}
 * <li>indefinitely be frozen: {@link BooleanType}
 * </ol>
 * In the first case, users can tell (through {@link ModifiableType#isFrozen()})
 * that, even if technically possible, definition of a type will never change.
 * <p>
 * Considering that a type may evolve has implication on <b>simplifications</b>.<br>
 * Considering current type definition, a full set can be replaced by {@code true}.<br>
 * But with a reserve, such replacement is not possible.
 * <p>
 * It may also impact <b>projection matching</b>.<br>
 * Something that is <em>ALWAYS</em> or <em>NEVER</em> true with current type definition,
 * can become <em>SOMETIMES</em> true with reserves.
 *
 * @author Damien Carbonne
 */
public enum ReserveStrategy {
    /**
     * Consider that the definition of a type is as defined, without possible extension.
     */
    NO_RESERVES,

    /**
     * Consider that the definition of a type includes reserves if user declared it.
     */
    USER_DEFINED_RESERVES,

    /**
     * Consider that the definition of a type includes reserves if it is
     * technically possible, whatever the user asked.
     */
    ALL_POSSIBLE_RESERVES;

    /**
     * Returns {@code true} if a type has reserves, using this reserve strategy.
     * <ul>
     * <li>if this == {@link #NO_RESERVES}, returns {@code false}.
     * <li>if this == {@link #USER_DEFINED_RESERVES} and {@code type} is
     * modifiable, returns !isFrozen();
     * <li>if this == {@link #USER_DEFINED_RESERVES} and {@code type} is
     * not modifiable, returns {@code false};
     * <li>if this == {@link #ALL_POSSIBLE_RESERVES} returns {@code true}
     * if {@code type} is modifiable.
     * </ul>
     *
     * @param type The type.
     * @return {@code true} if {@code type} has reserves, using this reserve strategy.
     */
    public boolean hasReserve(Type type) {
        if (this == NO_RESERVES) {
            return false;
        } else {
            final boolean modifiable = type instanceof ModifiableType;
            if (this == USER_DEFINED_RESERVES) {
                if (modifiable) {
                    return !((ModifiableType) type).isFrozen();
                } else {
                    return false;
                }
            } else {
                return modifiable;
            }
        }
    }
}