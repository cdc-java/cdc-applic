package cdc.applic.dictionaries;

import cdc.applic.expressions.ApplicException;

public class ConversionException extends ApplicException {
    private static final long serialVersionUID = 1L;

    public ConversionException(String message) {
        super(message, null);
    }
}