package cdc.applic.dictionaries;

import java.util.Comparator;

/**
 * Base interface of constraints, which are assertion generators.
 * <p>
 * A Constraint is defined in a dictionary, has a type, and parameters.<br>
 * Parameters of a constraint can change.<br>
 * It is the constraint responsibility to ensure that generated assertions are in line with its parameters.
 *
 * @author Damien Carbonne
 */
public interface Constraint extends DOwnedElement, Described {
    public static Comparator<Constraint> TYPE_PARAMS_COMPARATOR =
            Comparator.comparing(Constraint::getTypeName)
                      .thenComparing(Constraint::getParams);

    @Override
    public Dictionary getOwner();

    /**
     * @return The type name of this constraint.
     */
    public String getTypeName();

    /**
     * @return The parameters used to configure this constraint, encoded as a String.<br>
     *         The encoding scheme is free.
     */
    public String getParams();
}