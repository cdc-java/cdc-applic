package cdc.applic.dictionaries;

/**
 * Enumeration of possible memberships.
 *
 * @author Damien Carbonne
 */
public enum DictionaryMembership {
    /**
     * The member is locally declared in the {@link Dictionary}.
     */
    LOCAL,

    CONTEXT,

    /**
     * The member is inherited or derived from another {@link Dictionary}.
     */
    INHERITED,

    DERIVED,

    /**
     * The member is unrelated with the {@link Dictionary}.
     */
    UNRELATED;

    public boolean isRelated() {
        return this != UNRELATED;
    }

    public boolean isLocal() {
        return this == LOCAL;
    }

    public boolean isInherited() {
        return this == INHERITED;
    }
}