package cdc.applic.dictionaries;

/**
 * Interface implemented by objects that can have a description.
 *
 * @author Damien Carbonne
 */
public interface Described {
    /**
     * @return The description of this object.
     */
    public default Description getDescription() {
        return Description.EMPTY;
    }
}