package cdc.applic.dictionaries;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;
import cdc.util.lang.Introspection;

/**
 * Constraints factory.
 *
 * @author Damien Carbonne
 */
public final class Constraints {
    private static final Logger LOGGER = LogManager.getLogger(Constraints.class);

    private Constraints() {
    }

    private static final Map<String, BiFunction<Dictionary, String, ? extends Constraint>> CONSTRAINT_FACTORIES = new HashMap<>();

    /**
     * Registers a constraint factory.
     *
     * @param <C> The constraint type.
     * @param typeName The constraint type name.
     * @param factory The constraint factory. It is a function that takes
     *            a dictionary and parameters as input and returns a constraint.
     * @throws IllegalArgumentException When {@code typeName} or {@code factory} is {@code null},
     *             or when another factory associated to the same {@code name} is already registered.
     */
    public static <C extends Constraint> void register(String typeName,
                                                       BiFunction<Dictionary, String, C> factory) {
        Checks.isNotNull(typeName, "typeName");
        Checks.isNotNull(factory, "factory");
        Checks.doesNotContainKey(CONSTRAINT_FACTORIES, typeName, "factories");

        CONSTRAINT_FACTORIES.put(typeName, factory);
    }

    /**
     * @return A set of registered constraint type names.
     */
    public static Set<String> getTypeNames() {
        return CONSTRAINT_FACTORIES.keySet();
    }

    /**
     * Returns the factory associated to a constraint type name.
     *
     * @param typeName The constraint type name.
     * @return The factory associated to {@code typeName}.
     * @throws IllegalArgumentException When no factory is registered for {@code typeName}.
     */
    public static BiFunction<Dictionary, String, ? extends Constraint> getFactory(String typeName) {
        Checks.assertTrue(CONSTRAINT_FACTORIES.containsKey(typeName), "No factory registered for '" + typeName + "'");
        return CONSTRAINT_FACTORIES.get(typeName);
    }

    /**
     * Creates a constraint.
     *
     * @param typeName The constraint type name.
     * @param dictionary The dictionary.
     * @param params The constraint parameters.
     * @return A new constraint.
     * @throws IllegalArgumentException When no factory is registered for {@code typeName}.
     */
    public static Constraint create(String typeName,
                                    Dictionary dictionary,
                                    String params) {
        final BiFunction<Dictionary, String, ? extends Constraint> factory = getFactory(typeName);
        return factory.apply(dictionary, params);
    }

    /**
     * Ensures that all standard constraints factories are registered.
     * <p>
     * This invokes StandardConstraints.elaborate() using introspection.
     */
    public static void elaborateStandardConstraints() {
        final String name = "cdc.applic.dictionaries.core.constraints.StandardConstraints";
        final Class<?> cls =
                Introspection.getClass(name);
        if (cls == null) {
            LOGGER.warn("Could not find class {}", name);
        } else {
            final Method m = Introspection.getMethod(cls, "elaborate");
            if (m == null) {
                LOGGER.warn("Could not find method elaborate() in class {}", name);
            } else {
                try {
                    m.invoke(null);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    LOGGER.error("Failed to invoke elaborate()", e);
                }
            }
        }
    }
}