package cdc.applic.dictionaries.s1000d;

/**
 * Enumeration of possible S1000D property types.
 *
 * @author Damien Carbonne
 */
public enum S1000DPropertyType {
    /** The property is a product attribute. */
    PRODUCT_ATTRIBUTE,
    /** The property is a product condition. It can be declared in CCT and PCT. */
    PRODUCT_CONDITION,
    /** The property is an external condition. It can be declared in CCT, not in PCT */
    EXTERNAL_CONDITION,
    /** The property is hidden to S1000D. */
    UNDEFINED;

    /**
     * @return {@code true} if this type corresponds to a property
     *         that can be included in an ACT: {@link #PRODUCT_ATTRIBUTE}.
     */
    public boolean isActCandidate() {
        return this == PRODUCT_ATTRIBUTE;
    }

    /**
     * @return {@code true} if this type corresponds to a property
     *         that can be included in a CCT: {@link #EXTERNAL_CONDITION} or {@link #PRODUCT_CONDITION}.
     */
    public boolean isCctCandidate() {
        return this == EXTERNAL_CONDITION || this == PRODUCT_CONDITION;
    }

    /**
     * @return {@code true} if this type corresponds to a property
     *         that can be included in a PCT: {@link #PRODUCT_ATTRIBUTE} or {@link #PRODUCT_CONDITION}.
     */
    public boolean isPctCandidate() {
        return this == PRODUCT_ATTRIBUTE || this == PRODUCT_CONDITION;
    }

    public S1000DProductIdentifier getDefaultIdentifier() {
        if (this == PRODUCT_ATTRIBUTE) {
            return S1000DProductIdentifier.NONE;
        } else {
            return S1000DProductIdentifier.NOT_APPLICABLE;
        }
    }

    /**
     * @param type The type.
     * @return {@code true} if {@code type} is compliant with S1000D.
     */
    public static boolean isS1000DCompliant(S1000DPropertyType type) {
        return type != null && type != UNDEFINED;
    }
}