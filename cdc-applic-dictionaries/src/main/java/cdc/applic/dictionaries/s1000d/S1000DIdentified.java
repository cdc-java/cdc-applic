package cdc.applic.dictionaries.s1000d;

import cdc.applic.dictionaries.NamingConvention;

/**
 * Interface implemented by objects that have an S1000D identifier.
 *
 * @author Damien Carbonne
 */
public interface S1000DIdentified {
    /**
     * @return The S1000D identifier.
     */
    public String getS1000DId();

    /**
     * @param convention The target naming convention.
     * @return The S1000D identifier.
     * @deprecated Use {@link #getS1000DId()}.
     */
    @Deprecated(since = "2024-05-12", forRemoval = true)
    public default String getId(NamingConvention convention) {
        return getS1000DId();
    }
}