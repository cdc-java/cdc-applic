package cdc.applic.dictionaries.s1000d;

import cdc.util.lang.UnexpectedValueException;

/**
 * Enumeration of possible S1000D product product identifier categories.
 *
 * @author Damien Carbonne
 */
public enum S1000DProductIdentifier {
    /** The product attribute is a primary key. */
    PRIMARY,
    /** The product attribute is a secondary key. */
    SECONDARY,
    /** The product attribute is neither primary nor secondary. */
    NONE,
    /** The property is not a product attribute. */
    NOT_APPLICABLE;

    /**
     * Returns the compliance of this key kind with a {@link S1000DPropertyType}.
     *
     * @param propertyKind The S1000D property kind.
     * @return The compliance of this key kind with {@code propertyKind}.
     */
    public boolean isCompliantWith(S1000DPropertyType propertyKind) {
        if (propertyKind == S1000DPropertyType.PRODUCT_ATTRIBUTE) {
            return this != NOT_APPLICABLE;
        } else {
            return this == NOT_APPLICABLE;
        }
    }

    public String getXmlValue() {
        if (this == PRIMARY) {
            return "primary";
        } else if (this == SECONDARY) {
            return "secondary";
        } else if (this == NONE) {
            return "no";
        } else {
            throw new UnexpectedValueException(this);
        }
    }
}