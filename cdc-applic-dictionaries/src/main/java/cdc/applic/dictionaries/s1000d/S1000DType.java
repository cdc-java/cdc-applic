package cdc.applic.dictionaries.s1000d;

import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.RealType;
import cdc.applic.dictionaries.types.Type;
import cdc.util.lang.UnexpectedValueException;

/**
 * Specialization of {@link Type} dedicated to S1000D.
 *
 * @author Damien Carbonne
 */
public interface S1000DType extends Type, S1000DIdentified {
    /**
     * @return The S1000D property kind of this type.
     */
    public S1000DPropertyType getS1000DPropertyType();

    /**
     * @return The S1000D key kind of this type.<br>
     *         Shall return {@link S1000DProductIdentifier#NONE NONE} when
     *         property kind is <em>NOT</em> {@link S1000DPropertyType#PRODUCT_ATTRIBUTE PRODUCT_ATTRIBUTE}.
     */
    public S1000DProductIdentifier getS1000DProductIdentifier();

    public default String getXmlValue() {
        if (this instanceof BooleanType) {
            return "boolean";
        } else if (this instanceof EnumeratedType || this instanceof PatternType) {
            return "string";
        } else if (this instanceof IntegerType) {
            return "integer";
        } else if (this instanceof RealType) {
            return "real";
        } else {
            throw new UnexpectedValueException("Non supported type");
        }
    }
}