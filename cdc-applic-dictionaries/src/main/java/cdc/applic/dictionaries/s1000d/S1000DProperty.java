package cdc.applic.dictionaries.s1000d;

import java.util.Comparator;

import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;

/**
 * Definition of an S1000D property.
 * <p>
 * It adds S1000D specific data, which are not necessary for most computations.
 *
 * @author Damien Carbonne
 */
public interface S1000DProperty extends Property, S1000DIdentified {
    /**
     * A comparator that first compares S1000D types then names.
     */
    public static final Comparator<S1000DProperty> COMPARATOR =
            Comparator.comparing(S1000DProperty::getS1000DPropertyType)
                      .thenComparing(NamedDItem::getName);

    @Override
    public S1000DType getType();

    public default S1000DPropertyType getS1000DPropertyType() {
        return getType().getS1000DPropertyType();
    }

    public default boolean isS1000DCompliant() {
        return S1000DPropertyType.isS1000DCompliant(getS1000DPropertyType());
    }

    public default S1000DProductIdentifier getS1000DProductIdentifier() {
        return getType().getS1000DProductIdentifier();
    }
}