package cdc.applic.dictionaries.s1000d;

import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.util.lang.Checks;

public final class S1000DUtils {
    private S1000DUtils() {
    }

    public static String toXmlValue(SItemSet set) {
        Checks.isNotNull(set, "set");

        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        for (final SItem item : set.getItems()) {
            if (first) {
                first = false;
            } else {
                builder.append('|');
            }
            builder.append(item.getNonEscapedLiteral());
        }
        return builder.toString();
    }
}