package cdc.applic.dictionaries;

import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.Named;

/**
 * Definition of a naming convention.
 *
 * @author Damien Carbonne
 */
public interface NamingConvention extends DOwnedElement, Named, Described {
    /**
     * Name of the default naming convention.
     */
    public static final Name DEFAULT_NAME = Name.of("Default");

    @Override
    Registry getOwner();

    /**
     * @return {@code true} if this convention is the default one.
     */
    public default boolean isDefault() {
        return this == DEFAULT;
    }

    /**
     * Default naming convention.
     */
    public static final NamingConvention DEFAULT = new NamingConvention() {
        @Override
        public Registry getOwner() {
            return null;
        }

        @Override
        public Name getName() {
            return DEFAULT_NAME;
        }

        @Override
        public Description getDescription() {
            return Description.EMPTY;
        }

        @Override
        public String toString() {
            return getName().getNonEscapedLiteral();
        }
    };
}