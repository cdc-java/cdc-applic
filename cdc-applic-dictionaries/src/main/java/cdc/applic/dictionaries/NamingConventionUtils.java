package cdc.applic.dictionaries;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.content.Value;
import cdc.util.lang.Checks;

public final class NamingConventionUtils {
    private static final String CONVENTION = "convention";
    private static final String ITEM = "item";
    private static final String SET = "set";
    private static final String TYPE = "type";
    private static final String VALUE = "value";

    private NamingConventionUtils() {
    }

    /**
     * Converts a {@link StringValue} of an {@link EnumeratedType} to a target {@link NamingConvention}.
     *
     * @param value The {@link StringValue} to convert.
     * @param type The {@link EnumeratedType} of {@code value}.
     * @param convention The target {@link NamingConvention}.
     * @return The synonym of {@code value} in {@code convention}.
     */
    public static StringValue convert(StringValue value,
                                      EnumeratedType type,
                                      NamingConvention convention) {
        Checks.isNotNull(value, VALUE);
        Checks.isNotNull(type, TYPE);
        Checks.isNotNull(convention, CONVENTION);

        return type.convertLiteral(value, convention);
    }

    /**
     * Converts a {@link Value} to a target {@link NamingConvention}.
     * <p>
     * If {@code value} is a {@link StringValue} and {@code type} an {@link EnumeratedType},
     * then a conversion is done. Otherwise, {@code value} is returned unchanged.
     *
     * @param value The {@link Value} to convert.
     * @param type The {@link Type} of {@code value}.
     * @param convention The target {@link NamingConvention}.
     * @return The synonym of {@code value} in {@code convention}.
     */
    public static Value convert(Value value,
                                Type type,
                                NamingConvention convention) {
        Checks.isNotNull(value, VALUE);
        Checks.isNotNull(type, TYPE);
        Checks.isNotNull(convention, CONVENTION);

        if (value instanceof final StringValue v && type instanceof final EnumeratedType t) {
            return convert(v, t, convention);
        } else {
            return value;
        }
    }

    /**
     * Converts an {@link SItem} to a target {@link NamingConvention}.
     * <p>
     * If {@code item} is a {@link StringValue} and {@code type} an {@link EnumeratedType},
     * then a conversion is done. Otherwise, {@code item} is returned unchanged.
     *
     * @param <T> The item type.
     * @param item The {@link SItem} to convert.
     * @param itemClass The class of {@code item}.
     * @param type The {@link Type} of {@code item}.
     * @param convention The target {@link NamingConvention}.
     * @return The synonym of {@code item} in {@code convention}.
     * @throws ClassCastException When {@code itemClass} is not compliant with conversion result.
     */
    public static <T extends SItem> T convert(T item,
                                              Class<T> itemClass,
                                              Type type,
                                              NamingConvention convention) {
        Checks.isNotNull(item, ITEM);
        Checks.isNotNull(type, TYPE);
        Checks.isNotNull(convention, CONVENTION);

        if (item instanceof final StringValue v && type instanceof final EnumeratedType t) {
            return itemClass.cast(convert(v, t, convention));
        } else {
            return item;
        }
    }

    /**
     * Converts a {@link StringSet} whose type of values is an {@link EnumeratedType}
     * to a target {@link NamingConvention}.
     *
     * @param set The {@link StringSet} to convert.
     * @param type The {@link EnumeratedType} of values of {@code set}.
     * @param convention The target {@link NamingConvention}.
     * @return A {@link StringSet} equivalent to {@code set}, and conform to {@code convention}.
     */
    public static StringSet convert(StringSet set,
                                    EnumeratedType type,
                                    NamingConvention convention) {
        Checks.isNotNull(set, SET);
        Checks.isNotNull(type, TYPE);
        Checks.isNotNull(convention, CONVENTION);

        if (set.isEmpty()) {
            return set;
        } else {
            final List<StringValue> newValues = new ArrayList<>();
            for (final StringValue value : set.getItems()) {
                final StringValue newValue = type.convertLiteral(value, convention);
                newValues.add(newValue);
            }
            return StringSet.of(newValues);
        }
    }

    /**
     * Converts a {@link SItemSet} to a target {@link NamingConvention}.
     * <p>
     * If {@code set} is a {@link StringSet} and {@code type} an {@link EnumeratedType},
     * then a conversion is done. Otherwise, {@code set} is returned unchanged.
     *
     * @param set The {@link SItemSet} to convert.
     * @param type The {@link Type} of values of {@code set}.
     * @param convention The target {@link NamingConvention}.
     * @return A {@link SItemSet} equivalent to {@code set}, and conform to {@code convention}.
     */
    public static SItemSet convert(SItemSet set,
                                   Type type,
                                   NamingConvention convention) {
        Checks.isNotNull(set, SET);
        Checks.isNotNull(type, TYPE);
        Checks.isNotNull(convention, CONVENTION);

        if (set.isEmpty()) {
            return set;
        } else if (type instanceof final EnumeratedType t) {
            final SItemSet best = set.getBest();
            if (best instanceof final StringSet s) {
                return convert(s, t, convention);
            } else {
                return set;
            }
        } else {
            return set;
        }
    }
}