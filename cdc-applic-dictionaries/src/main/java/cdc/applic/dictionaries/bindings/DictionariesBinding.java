package cdc.applic.dictionaries.bindings;

import java.util.Comparator;
import java.util.Set;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.Registry;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.NotFoundException;

/**
 * Interface describing a binding between 2 Dictionaries.
 *
 * @author Damien Carbonne
 */
public interface DictionariesBinding {
    public static final Comparator<DictionariesBinding> TARGET_COMPARATOR =
            Comparator.comparing(b -> b.getDictionary(BindingRole.TARGET).getPath());

    public static final Comparator<DictionariesBinding> SOURCE_COMPARATOR =
            Comparator.comparing(b -> b.getDictionary(BindingRole.SOURCE).getPath());

    public static final Comparator<DictionariesBinding> TARGET_SOURCE_COMPARATOR =
            TARGET_COMPARATOR.thenComparing(SOURCE_COMPARATOR);

    public static final Comparator<DictionariesBinding> SOURCE_TARGET_COMPARATOR =
            SOURCE_COMPARATOR.thenComparing(TARGET_COMPARATOR);

    /**
     * Returns the {@link Registry} that plays a role in the binding.
     *
     * @param role The role.
     * @return The Registry that plays {@code role} in the binding.
     */
    public Dictionary getDictionary(BindingRole role);

    /**
     * @return A set of all {@link TypesBinding}s associated to this binding.
     */
    public Set<TypesBinding> getTypesBindings();

    /**
     * Returns the {@link TypesBinding}s between 2 types.
     *
     * @param sourceType The source Type.
     * @param targetType The target Type.
     * @return The TypeBindings between {@code sourceType} and {@code targetType}.
     * @throws IllegalArgumentException When {@code sourceType} or {@code targetType} is {@code null}.
     */
    public Set<TypesBinding> getTypesBindings(Type sourceType,
                                              Type targetType);

    /**
     * Returns the {@link TypesBinding} between 2 types.
     * <p>
     * If there is not one answer (0 or more than 1), returns {@code null}.
     *
     * @param sourceType The source Type.
     * @param targetType The target Type.
     * @return The TypeBinding between {@code sourceType} and {@code targetType} or {@code null}.
     */
    public default TypesBinding getTypesBinding(Type sourceType,
                                                Type targetType) {
        final Set<TypesBinding> set = getTypesBindings(sourceType, targetType);
        if (set.size() == 1) {
            return set.iterator().next();
        } else {
            return null;
        }
    }

    /**
     * @return All {@link DItemsBinding}s associated to this binding.
     */
    public Set<DItemsBinding> getDItemBindings();

    /**
     * Returns the {@link DItemsBinding}s associated to an item name playing a role.
     *
     * @param name The item name.
     * @param role The role.
     * @return The DItemBindings associated to item named {@code name} and playing {@code role}.
     * @throws IllegalArgumentException When {@code name} or {@code role} is {@code null}.
     */
    public Set<DItemsBinding> getDItemBindings(Name name,
                                               BindingRole role);

    /**
     * Returns the {@link AliasAliasBinding} associated to an Alias name playing a role.
     *
     * @param name The Alias name.
     * @param role The role.
     * @return The AliasAliasBinding associated to the Alias named {@code name} and playing {@code role}.
     * @throws IllegalArgumentException When {@code name} or {@code role} is {@code null}.
     * @throws NotFoundException When no such AliasBinding exists.
     */
    public AliasAliasBinding getAliasAliasBinding(Name name,
                                                  BindingRole role);

    /**
     * Returns the {@link PropertyPropertyBinding}s associated to a Property name playing a role.
     *
     * @param name The Property name.
     * @param role The role.
     * @return The PropertyPropertyBinding associated to the Property named {@code name} and playing {@code role}.
     * @throws IllegalArgumentException When {@code name} or {@code role} is {@code null}.
     */
    public Set<PropertyPropertyBinding> getPropertyPropertyBindings(Name name,
                                                                    BindingRole role);
}