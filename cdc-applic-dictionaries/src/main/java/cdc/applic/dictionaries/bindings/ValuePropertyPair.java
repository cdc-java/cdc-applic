package cdc.applic.dictionaries.bindings;

import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.content.Value;

public record ValuePropertyPair(Value value,
                                Property property) {
}