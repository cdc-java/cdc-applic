package cdc.applic.dictionaries.bindings;

import cdc.applic.dictionaries.items.Alias;

/**
 * Interface for Aliases bindings.
 *
 * @author Damien Carbonne
 */
public interface AliasAliasBinding extends DItemsBinding {
    @Override
    public Alias getDItem(BindingRole role);

    public Alias getAlias(BindingRole role);
}