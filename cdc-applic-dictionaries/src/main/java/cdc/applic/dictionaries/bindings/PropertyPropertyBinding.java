package cdc.applic.dictionaries.bindings;

import cdc.applic.dictionaries.items.Property;

/**
 * Interface for properties bindings.
 * <p>
 * A Property can be associated to several PropertyBindings, as long as their domains are disjoint.
 *
 * @author Damien Carbonne
 */
public interface PropertyPropertyBinding extends DItemsBinding {
    @Override
    public Property getDItem(BindingRole role);

    public Property getProperty(BindingRole role);

    /**
     * @return The associated type binding.
     */
    public TypesBinding getTypesBinding();
}