package cdc.applic.dictionaries.bindings;

import java.util.HashSet;
import java.util.Set;

import cdc.applic.dictionaries.ConversionException;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.Value;
import cdc.util.lang.Checks;

/**
 * Interface implemented by objects that can convert expressions between 2 dictionaries.
 * <p>
 * This is possible when a binding exists between the 2 registries.<br>
 * Normally, all expressions that are compliant with target registry can be converted to expressions in source registry.
 *
 * @author Damien Carbonne
 */
public interface DictionaryConverter {
    /**
     * Returns the registry that plays a role.
     *
     * @param role The role.
     * @return The registry that plays this {@code role}.
     * @throws IllegalArgumentException When {@code role} is {@code null}.
     */
    public Dictionary getDictionary(BindingRole role);

    /**
     * Converts a property following a direction.
     *
     * @param property The property.
     * @param direction The direction.
     * @return The conversion of {@code property} following {@code direction}.
     * @throws IllegalArgumentException When {@code property} or {@code direction} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public default Set<Property> convert(Property property,
                                         BindingDirection direction) {
        Checks.isNotNull(property, "property");
        Checks.isNotNull(direction, "direction");
        return direction == BindingDirection.FORWARD
                ? forward(property)
                : backward(property);
    }

    /**
     * Converts an alias following a direction.
     *
     * @param alias The alias.
     * @param direction The direction.
     * @return The conversion of {@code alias} following {@code direction}.
     * @throws IllegalArgumentException When {@code alias} or {@code direction} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public default Alias convert(Alias alias,
                                 BindingDirection direction) {
        Checks.isNotNull(alias, "alias");
        Checks.isNotNull(direction, "direction");
        return direction == BindingDirection.FORWARD
                ? forward(alias)
                : backward(alias);
    }

    /**
     * Converts a value following a direction.
     *
     * @param value The value.
     * @param property The property.
     * @param direction The direction.
     * @return The conversion of {@code property} following {@code direction}.
     * @throws IllegalArgumentException When {@code property} or {@code direction} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public default ValuePropertyPair convert(Value value,
                                             Property property,
                                             BindingDirection direction) {
        Checks.isNotNull(value, "value");
        Checks.isNotNull(property, "property");
        Checks.isNotNull(direction, "direction");
        return direction == BindingDirection.FORWARD
                ? forward(value, property)
                : backward(value, property);
    }

    /**
     * Converts a set following a direction.
     *
     * @param set The set.
     * @param property The property.
     * @param direction The direction.
     * @return The conversion of {@code set} following {@code direction}.
     * @throws IllegalArgumentException When {@code set} or {@code direction} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public default Set<SItemSetPropertyPair> convert(SItemSet set,
                                                     Property property,
                                                     BindingDirection direction) {
        Checks.isNotNull(set, "set");
        Checks.isNotNull(property, "property");
        Checks.isNotNull(direction, "direction");
        return direction == BindingDirection.FORWARD
                ? forward(set, property)
                : backward(set, property);
    }

    /**
     * Converts a Node following a direction.
     *
     * @param node The node.
     * @param direction The direction.
     * @return The conversion of {@code node} following {@code direction}.
     * @throws IllegalArgumentException When {@code node} or {@code direction} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public default Node convert(Node node,
                                BindingDirection direction) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(direction, "direction");
        return direction == BindingDirection.FORWARD
                ? forward(node)
                : backward(node);
    }

    /**
     * Converts an Expression following a direction.
     *
     * @param expression The expression.
     * @param direction The direction.
     * @return The conversion of {@code expression} following {@code direction}.
     * @throws IllegalArgumentException When {@code expression} or {@code direction} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public default Expression convert(Expression expression,
                                      BindingDirection direction) {
        Checks.isNotNull(expression, "expression");
        Checks.isNotNull(direction, "direction");
        return direction == BindingDirection.FORWARD
                ? forward(expression)
                : backward(expression);
    }

    /**
     * Converts a property from source registry to target registry.
     *
     * @param property The property.
     * @return The conversion of {@code property}.
     * @throws IllegalArgumentException When {@code property} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public Set<Property> forward(Property property);

    /**
     * Converts an alias from source registry to target registry.
     *
     * @param alias The alias.
     * @return The conversion of {@code alias}.
     * @throws IllegalArgumentException When {@code alias} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public Alias forward(Alias alias);

    /**
     * Converts a value from source registry to target registry.
     *
     * @param value The value.
     * @param property The property.
     * @return The conversion of {@code value}.
     * @throws IllegalArgumentException When {@code value} or {@code property} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public ValuePropertyPair forward(Value value,
                                     Property property);

    /**
     * Converts a set from source registry to target registry.
     *
     * @param set The set.
     * @param property The property.
     * @return The conversion of {@code set}.
     * @throws IllegalArgumentException When {@code set} or {@code property} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public Set<SItemSetPropertyPair> forward(SItemSet set,
                                             Property property);

    /**
     * Converts a node from source registry to target registry.
     *
     * @param node The node.
     * @return The conversion of node.
     * @throws IllegalArgumentException When {@code node} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public Node forward(Node node);

    /**
     * Converts an Expression from source registry to target registry.
     *
     * @param expression The expression.
     * @return The conversion of expression.
     * @throws IllegalArgumentException When {@code expression} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public default Expression forward(Expression expression) {
        Checks.isNotNull(expression, "expression");
        return forward(expression.getRootNode()).toExpression();
    }

    /**
     * Converts a property from target registry to source registry.
     *
     * @param property The property.
     * @return The conversion of {@code property}.
     * @throws IllegalArgumentException When {@code property} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public Set<Property> backward(Property property);

    /**
     * Converts an alias from target registry to source registry.
     *
     * @param alias The alias.
     * @return The conversion of {@code alias}.
     * @throws IllegalArgumentException When {@code alias} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public Alias backward(Alias alias);

    /**
     * Converts a value from target registry to source registry.
     *
     * @param value The value.
     * @param property The property.
     * @return The conversion of {@code value}.
     * @throws IllegalArgumentException When {@code value} or {@code property} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public ValuePropertyPair backward(Value value,
                                      Property property);

    /**
     * Converts a set from target registry to source registry.
     *
     * @param set The set.
     * @param property The property.
     * @return The conversion of {@code set}.
     * @throws IllegalArgumentException When {@code set} or {@code property} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public Set<SItemSetPropertyPair> backward(SItemSet set,
                                              Property property);

    /**
     * Converts a node from target registry to source registry.
     *
     * @param node The node.
     * @return The conversion of node.
     * @throws IllegalArgumentException When {@code node} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public Node backward(Node node);

    /**
     * Converts an Expression from target registry to source registry.
     *
     * @param expression The expression.
     * @return The conversion of expression.
     * @throws IllegalArgumentException When {@code expression} is {@code null}.
     * @throws ConversionException When conversion fails.
     */
    public default Expression backward(Expression expression) {
        Checks.isNotNull(expression, "expression");
        return backward(expression.getRootNode()).toExpression();
    }

    /**
     * @return A converter with inverted source and target.
     */
    public default DictionaryConverter revert() {
        return new DictionaryConverter() {
            @Override
            public Dictionary getDictionary(BindingRole role) {
                return DictionaryConverter.this.getDictionary(role.opposite());
            }

            @Override
            public Set<Property> forward(Property property) {
                return DictionaryConverter.this.backward(property);
            }

            @Override
            public Alias forward(Alias alias) {
                return DictionaryConverter.this.backward(alias);
            }

            @Override
            public ValuePropertyPair forward(Value value,
                                             Property property) {
                return DictionaryConverter.this.backward(value, property);
            }

            @Override
            public Set<SItemSetPropertyPair> forward(SItemSet set,
                                                     Property property) {
                return DictionaryConverter.this.backward(set, property);
            }

            @Override
            public Set<Property> backward(Property property) {
                return DictionaryConverter.this.forward(property);
            }

            @Override
            public Alias backward(Alias alias) {
                return DictionaryConverter.this.forward(alias);
            }

            @Override
            public Node forward(Node node) {
                return DictionaryConverter.this.backward(node);
            }

            @Override
            public ValuePropertyPair backward(Value value,
                                              Property property) {
                return DictionaryConverter.this.forward(value, property);
            }

            @Override
            public Set<SItemSetPropertyPair> backward(SItemSet set,
                                                      Property property) {
                return DictionaryConverter.this.forward(set, property);
            }

            @Override
            public Node backward(Node node) {
                return DictionaryConverter.this.forward(node);
            }
        };
    }

    /**
     * Returns a composed registry converter that, in forward direction, first applies {@code before}
     * to its input, and then applies this converter to the result.
     *
     * @param before The converter to apply before this converter is applied, in forward direction.
     *
     * @return A composed registry converter that, in forward direction, first applies {@code before}
     *         to its input, and then applies this converter to the result.
     * @throws IllegalArgumentException When {@code before} is {@code null},
     *             or when this converter is not compliant with {@code before} converter.
     */
    public default DictionaryConverter compose(DictionaryConverter before) {
        Checks.isNotNull(before, "before");
        Checks.isTrue(before.getDictionary(BindingRole.TARGET) == this.getDictionary(BindingRole.SOURCE),
                      "Non composable bindings: " + toString(before) + " and " + toString(this));

        return new DictionaryConverter() {
            @Override
            public Dictionary getDictionary(BindingRole role) {
                Checks.isNotNull(role, "role");

                if (role == BindingRole.SOURCE) {
                    return before.getDictionary(BindingRole.SOURCE);
                } else {
                    return DictionaryConverter.this.getDictionary(BindingRole.TARGET);
                }
            }

            @Override
            public Set<Property> forward(Property property) {
                final Set<Property> set1 = before.forward(property);
                final Set<Property> set2 = new HashSet<>();
                for (final Property p1 : set1) {
                    set2.addAll(DictionaryConverter.this.forward(p1));
                }
                return set2;
            }

            @Override
            public Alias forward(Alias alias) {
                return DictionaryConverter.this.forward(before.forward(alias));
            }

            @Override
            public ValuePropertyPair forward(Value value,
                                             Property property) {
                final ValuePropertyPair pair1 = before.forward(value, property);
                return DictionaryConverter.this.forward(pair1.value(),
                                                        pair1.property());
            }

            @Override
            public Set<SItemSetPropertyPair> forward(SItemSet set,
                                                     Property property) {
                final Set<SItemSetPropertyPair> set1 = before.forward(set, property);
                final Set<SItemSetPropertyPair> set2 = new HashSet<>();
                for (final SItemSetPropertyPair pair1 : set1) {
                    set2.addAll(DictionaryConverter.this.forward(pair1.set(),
                                                                 pair1.property()));
                }
                return set2;
            }

            @Override
            public Node forward(Node node) {
                return DictionaryConverter.this.forward(before.forward(node));
            }

            @Override
            public Set<Property> backward(Property property) {
                final Set<Property> set1 = DictionaryConverter.this.backward(property);
                final Set<Property> set2 = new HashSet<>();
                for (final Property p1 : set1) {
                    set2.addAll(before.backward(p1));
                }
                return set2;
            }

            @Override
            public Alias backward(Alias alias) {
                return before.backward(DictionaryConverter.this.backward(alias));
            }

            @Override
            public ValuePropertyPair backward(Value value,
                                              Property property) {
                final ValuePropertyPair pair1 = DictionaryConverter.this.backward(value, property);
                return before.backward(pair1.value(),
                                       pair1.property());
            }

            @Override
            public Set<SItemSetPropertyPair> backward(SItemSet set,
                                                      Property property) {
                final Set<SItemSetPropertyPair> set1 = DictionaryConverter.this.backward(set, property);
                final Set<SItemSetPropertyPair> set2 = new HashSet<>();
                for (final SItemSetPropertyPair pair1 : set1) {
                    set2.addAll(before.backward(pair1.set(),
                                                pair1.property()));
                }
                return set2;
            }

            @Override
            public Node backward(Node node) {
                return before.backward(DictionaryConverter.this.backward(node));
            }
        };
    }

    /**
     * Returns a composed registry converter that, in forward direction, first applies this converter to
     * its input, and then applies {@code after} to the result.
     *
     * @param after The converter to apply after this converter is applied, in forward direction.
     *
     * @return A composed registry converter that, in forward direction, first applies this converter to
     *         to its input, and then applies {@code after} to the result.
     * @throws IllegalArgumentException When {@code after} is {@code null},
     *             or when this converter is not compliant with {@code after} converter.
     *
     */
    public default DictionaryConverter andThen(DictionaryConverter after) {
        Checks.isNotNull(after, "after");
        Checks.isTrue(this.getDictionary(BindingRole.TARGET) == after.getDictionary(BindingRole.SOURCE),
                      "Non composable bindings: " + toString(this) + " and " + toString(after));

        return new DictionaryConverter() {
            @Override
            public Dictionary getDictionary(BindingRole role) {
                Checks.isNotNull(role, "role");

                if (role == BindingRole.SOURCE) {
                    return DictionaryConverter.this.getDictionary(BindingRole.SOURCE);
                } else {
                    return after.getDictionary(BindingRole.TARGET);
                }
            }

            @Override
            public Set<Property> forward(Property property) {
                final Set<Property> set1 = DictionaryConverter.this.forward(property);
                final Set<Property> set2 = new HashSet<>();
                for (final Property p1 : set1) {
                    set2.addAll(after.forward(p1));
                }
                return set2;
            }

            @Override
            public Alias forward(Alias alias) {
                return after.forward(DictionaryConverter.this.forward(alias));
            }

            @Override
            public ValuePropertyPair forward(Value value,
                                             Property property) {
                final ValuePropertyPair pair1 = DictionaryConverter.this.forward(value, property);
                return after.forward(pair1.value(),
                                     pair1.property());
            }

            @Override
            public Set<SItemSetPropertyPair> forward(SItemSet set,
                                                     Property property) {
                final Set<SItemSetPropertyPair> set1 = DictionaryConverter.this.forward(set, property);
                final Set<SItemSetPropertyPair> set2 = new HashSet<>();
                for (final SItemSetPropertyPair pair1 : set1) {
                    set2.addAll(after.forward(pair1.set(),
                                              pair1.property()));
                }
                return set2;
            }

            @Override
            public Node forward(Node node) {
                return after.forward(DictionaryConverter.this.forward(node));
            }

            @Override
            public Set<Property> backward(Property property) {
                final Set<Property> set1 = after.backward(property);
                final Set<Property> set2 = new HashSet<>();
                for (final Property p1 : set1) {
                    set2.addAll(DictionaryConverter.this.backward(p1));
                }
                return set2;
            }

            @Override
            public Alias backward(Alias alias) {
                return DictionaryConverter.this.backward(after.backward(alias));
            }

            @Override
            public ValuePropertyPair backward(Value value,
                                              Property property) {
                final ValuePropertyPair pair1 = after.backward(value, property);
                return DictionaryConverter.this.backward(pair1.value(),
                                                         pair1.property());
            }

            @Override
            public Set<SItemSetPropertyPair> backward(SItemSet set,
                                                      Property property) {
                final Set<SItemSetPropertyPair> set1 = after.backward(set, property);
                final Set<SItemSetPropertyPair> set2 = new HashSet<>();
                for (final SItemSetPropertyPair pair1 : set1) {
                    set2.addAll(DictionaryConverter.this.backward(pair1.set(),
                                                                  pair1.property()));
                }
                return set2;
            }

            @Override
            public Node backward(Node node) {
                return DictionaryConverter.this.backward(after.backward(node));
            }
        };
    }

    public static String toString(DictionaryConverter converter) {
        return "(" + converter.getDictionary(BindingRole.SOURCE).getName()
                + " -> "
                + converter.getDictionary(BindingRole.TARGET).getName() + ")";
    }
}