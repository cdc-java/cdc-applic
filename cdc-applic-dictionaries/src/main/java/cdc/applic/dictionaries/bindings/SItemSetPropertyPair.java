package cdc.applic.dictionaries.bindings;

import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.content.SItemSet;

public record SItemSetPropertyPair(SItemSet set,
                                   Property property) {
}