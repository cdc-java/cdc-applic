package cdc.applic.dictionaries.bindings;

public enum BindingRole {
    SOURCE,
    TARGET;

    public BindingRole opposite() {
        return this == SOURCE ? TARGET : SOURCE;
    }
}