package cdc.applic.dictionaries.bindings;

public enum BindingDirection {
    FORWARD,
    BACKWARD;

    public BindingRole getSourceRole() {
        return this == FORWARD ? BindingRole.SOURCE : BindingRole.TARGET;
    }

    public BindingRole getTargetRole() {
        return this == FORWARD ? BindingRole.TARGET : BindingRole.SOURCE;
    }
}