package cdc.applic.dictionaries.bindings;

import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;

/**
 * Description of BooleanType/BooleanType binding.
 * <p>
 * There are two possibilities:
 * <ul>
 * <li>Map {@link BooleanValue#FALSE} on {@link BooleanValue#FALSE}, and {@link BooleanValue#TRUE} on {@link BooleanValue#TRUE}.
 * <li>Map {@link BooleanValue#FALSE} on {@link BooleanValue#TRUE}, and {@link BooleanValue#TRUE} on {@link BooleanValue#FALSE}.
 * </ul>
 */
public interface BooleanBooleanBinding extends TypesBinding {
    @Override
    public BooleanType getSourceType();

    @Override
    public BooleanType getTargetType();

    @Override
    public BooleanSet getSourceDomain();

    @Override
    public BooleanSet getTargetDomain();

    /**
     * @return {@code true} if mapping of values is reverted.
     */
    public boolean revert();
}