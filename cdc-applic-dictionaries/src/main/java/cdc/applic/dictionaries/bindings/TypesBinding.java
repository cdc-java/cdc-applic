package cdc.applic.dictionaries.bindings;

import java.util.Comparator;

import cdc.applic.dictionaries.ConversionException;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;

/**
 * Interface describing a binding between 2 types.
 * <p>
 * <b>Note:</b> a TypeBinding binding can be partial.
 *
 * @author Damien Carbonne
 */
public interface TypesBinding {
    public static final Comparator<TypesBinding> TARGET_COMPARATOR =
            Comparator.comparing(b -> b.getType(BindingRole.TARGET).getName());

    public static final Comparator<TypesBinding> SOURCE_COMPARATOR =
            Comparator.comparing(b -> b.getType(BindingRole.SOURCE).getName());

    public static final Comparator<TypesBinding> TARGET_SOURCE_COMPARATOR =
            TARGET_COMPARATOR.thenComparing(SOURCE_COMPARATOR);

    public static final Comparator<TypesBinding> SOURCE_TARGET_COMPARATOR =
            SOURCE_COMPARATOR.thenComparing(TARGET_COMPARATOR);

    /**
     * @return The source type.
     */
    public Type getSourceType();

    /**
     * @return The target type.
     */
    public Type getTargetType();

    /**
     * Returns the type that plays a role.
     *
     * @param role The role.
     * @return The type that plays {@code role}.
     * @throws IllegalArgumentException When {@code role} is {@code null}.
     */
    public Type getType(BindingRole role);

    /**
     * @return The source domain.
     */
    public SItemSet getSourceDomain();

    /**
     * @return The target domain.
     */
    public SItemSet getTargetDomain();

    /**
     * Returns the domain that is mapped.
     *
     * @param role The role.
     * @return The domain that is mapped when playing {@code role}.
     */
    public SItemSet getDomain(BindingRole role);

    /**
     * Converts a source item to a target one.
     *
     * @param sourceItem The source item.
     * @return The conversion of {@code sourceItem}.
     * @throws ConversionException When conversion failed.
     */
    public SItem forward(SItem sourceItem);

    /**
     * Converts a target item to a source one.
     *
     * @param targetItem The target item.
     * @return The conversion of {@code targetItem}.
     * @throws ConversionException When conversion failed.
     */
    public SItem backward(SItem targetItem);

    /**
     * Converts an SItem following a direction.
     *
     * @param item The item.
     * @param direction The direction.
     * @return The converted item.
     * @throws IllegalArgumentException When {@code item} or {@code direction} is {@code null},
     *             or when {@code item} is not compliant with source role of {@code direction},
     *             or when {@code item} is not included in supported domain.
     * @throws ConversionException When conversion failed.
     */
    public SItem convert(SItem item,
                         BindingDirection direction);

    /**
     * Converts a source item set to a target one.
     *
     * @param sourceSet The source item set.
     * @return The conversion of {@code sourceSet}.
     * @throws ConversionException When conversion failed.
     */
    public SItemSet forward(SItemSet sourceSet);

    /**
     * Converts a target item set to a source one.
     *
     * @param targetSet The target item set.
     * @return The conversion of {@code targetItem}.
     * @throws ConversionException When conversion failed.
     */
    public SItemSet backward(SItemSet targetSet);

    /**
     * Converts an SItemSet following a direction.
     *
     * @param set The set.
     * @param direction The direction.
     * @return The converted set.
     * @throws IllegalArgumentException When {@code set} or {@code direction} is {@code null},
     *             or when {@code set} is not compliant with source role of {@code direction},
     *             or when {@code item} is not included in supported domain.
     * @throws ConversionException When conversion failed.
     */
    public SItemSet convert(SItemSet set,
                            BindingDirection direction);

    public static String toString(TypesBinding binding) {
        return "[" + binding.getType(BindingRole.TARGET).getName()
                + " <<< "
                + binding.getType(BindingRole.SOURCE).getName() + "]";
    }
}