package cdc.applic.dictionaries.bindings;

import cdc.applic.expressions.ApplicException;

public class BindingException extends ApplicException {
    private static final long serialVersionUID = 1L;

    public BindingException(String message) {
        super(message, null);
    }
}