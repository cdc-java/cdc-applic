package cdc.applic.dictionaries.bindings;

import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;

/**
 * Description of a binding between 2 {@link EnumeratedType}s.
 *
 * @author Damien Carbonne
 */
public interface EnumeratedEnumeratedBinding extends TypesBinding {
    @Override
    public EnumeratedType getSourceType();

    @Override
    public EnumeratedType getTargetType();

    @Override
    public StringSet getSourceDomain();

    @Override
    public StringSet getTargetDomain();

    public StringValue forward(StringValue sourceValue);

    public StringValue backward(StringValue targetValue);
}