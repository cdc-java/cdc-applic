package cdc.applic.dictionaries.bindings;

import java.util.Comparator;

import cdc.applic.dictionaries.items.NamedDItem;

/**
 * Base interface of NamedDItems bindings. *
 *
 * @author Damien Carbonne
 */
public interface DItemsBinding {
    public static final Comparator<DItemsBinding> TARGET_COMPARATOR =
            Comparator.comparing(b -> b.getDItem(BindingRole.TARGET).getName());

    public static final Comparator<DItemsBinding> SOURCE_COMPARATOR =
            Comparator.comparing(b -> b.getDItem(BindingRole.SOURCE).getName());

    public static final Comparator<DItemsBinding> TARGET_SOURCE_COMPARATOR =
            TARGET_COMPARATOR.thenComparing(SOURCE_COMPARATOR);

    public static final Comparator<DItemsBinding> SOURCE_TARGET_COMPARATOR =
            SOURCE_COMPARATOR.thenComparing(TARGET_COMPARATOR);

    /**
     * Returns the DItem that plays a role.
     *
     * @param role The role.
     * @return The NamedDItem that play {@code role}.
     * @throws IllegalArgumentException When {@code role} is {@code null}.
     */
    public NamedDItem getDItem(BindingRole role);
}