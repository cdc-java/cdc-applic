package cdc.applic.dictionaries;

import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.issues.Diagnosis;
import cdc.issues.HasDiagnosis;

public class SemanticException extends ApplicException implements HasDiagnosis<ApplicIssue> {
    private static final long serialVersionUID = 1L;
    private final Diagnosis<ApplicIssue> diagnosis;

    public SemanticException(String message,
                             Diagnosis<ApplicIssue> diagnosis,
                             Throwable cause) {
        super(message, cause);
        this.diagnosis = diagnosis;
    }

    public SemanticException(String message,
                             Diagnosis<ApplicIssue> diagnosis) {
        this(message, diagnosis, null);
    }

    public SemanticException(String message) {
        this(message, null);
    }

    @Override
    public Diagnosis<ApplicIssue> getDiagnosis() {
        return diagnosis;
    }
}