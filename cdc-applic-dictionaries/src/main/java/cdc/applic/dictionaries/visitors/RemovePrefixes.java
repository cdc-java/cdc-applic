package cdc.applic.dictionaries.visitors;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.RenamableNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.util.lang.Checks;

/**
 * Converter that removes prefixes from nodes whose item is declared in the registry of a dictionary.
 * <p>
 * <b>WARNING:</b> use of this converter may yield semantically invalid expressions.
 *
 * @author Damien Carbonne
 */
public final class RemovePrefixes extends AbstractConverter {
    private final Dictionary dictionary;

    private RemovePrefixes(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    private static Node executeInt(Node node,
                                   Dictionary dictionary) {
        final RemovePrefixes visitor = new RemovePrefixes(dictionary);
        return node.accept(visitor);
    }

    public static Node execute(Node node,
                               Dictionary dictionary) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(dictionary, "dictionary");
        if (dictionary.getRegistry().getPrefix().isPresent()) {
            return executeInt(node, dictionary);
        } else {
            return node;
        }
    }

    public static Expression execute(Expression expression,
                                     Dictionary dictionary) {
        Checks.isNotNull(expression, "expression");
        Checks.isNotNull(dictionary, "dictionary");
        if (dictionary.getRegistry().getPrefix().isPresent()) {
            return executeInt(expression.getRootNode(), dictionary).toExpression();
        } else {
            return expression;
        }
    }

    private RenamableNode removePrefix(RenamableNode node) {
        if (!node.getName().hasPrefix()) {
            // node has no prefix
            return node;
        } else if (dictionary.getRegistry().hasDeclaredItem(node.getName().getLocal())) {
            // node has a prefix and its name is declared in the reference registry
            return node.removePrefix();
        } else {
            // node has a prefix but is not known in registry of dictionary
            // It is defined in another ancestor dictionary, and we can not do anything for it
            // This may be normal, when this ancestor has no prefix
            return node;
        }
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (node instanceof final RenamableNode n) {
            return removePrefix(n);
        } else {
            return node;
        }
    }
}