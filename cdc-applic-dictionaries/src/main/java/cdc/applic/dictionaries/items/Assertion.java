package cdc.applic.dictionaries.items;

import java.util.Comparator;

import cdc.applic.dictionaries.Constraint;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.expressions.Expression;

/**
 * An assertion is an {@link Expression} that must always evaluate to true.
 * <p>
 * Assertions are used to link several {@link Property Properties} or {@link Alias Aliases}: they make them dependent.
 * <p>
 * There are several kinds of assertions:
 * <ul>
 * <li>{@link LocalAssertion Local Assertions}.
 * They are locally defined or computed. It can be:
 * <ul>
 * <li>{@link ContextAssertion Context} which are locally derived from the context expression.
 * <li>{@link StandardAssertion Standard}, which are either:
 * <ul>
 * <li>{@link UserDefinedAssertion User-defined},
 * <li>or {@link ConstraintAssertion Constraint}. They are automatically and locally generated
 * by {@link Constraint Constraints}.
 * </ul>
 * </ul>
 * <li>{@link DerivedAssertion Derived}.
 * They ae automatically computed from ancestor {@link Dictionary Dictionaries}. The are either:
 * <ul>
 * <li>{@link DerivedContextAssertion Derived Context}.
 * They are automatically inherited (and transformed) from context assertions of ancestor {@link Dictionary Dictionaries}.
 * <li>{@link DerivedStandardAssertion Derived Standard}.
 * They are automatically inherited (and transformed) from standard assertions of ancestor {@link Dictionary Dictionaries}.
 * </ul>
 * </ul>
 *
 * @author Damien Carbonne
 */
public interface Assertion extends DExpressed {
    /**
     * A Comparator of Assertions based on kind and expression.
     */
    public static final Comparator<Assertion> KIND_EXPRESSION_COMPARATOR =
            Comparator.comparing(Assertion::getKind)
                      .thenComparing(Assertion::getQualifiedExpression);

    /**
     * @return The kind of this assertion.
     */
    public AssertionKind getKind();
}