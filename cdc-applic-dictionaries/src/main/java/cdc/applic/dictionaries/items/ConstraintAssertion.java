package cdc.applic.dictionaries.items;

import java.util.Comparator;

import cdc.applic.dictionaries.Constraint;
import cdc.applic.expressions.Expression;

/**
 * Marking interface of <em>constraint</em> assertions.
 * <p>
 * They are <em>locally generated</em> from {@link Constraint Constraints}.
 *
 * @author Damien Carbonne
 */
public interface ConstraintAssertion extends StandardAssertion {
    /**
     * A GeneratedAssertion Comparator that compares constraints, then parameters, and then expressions.
     */
    public static final Comparator<ConstraintAssertion> CONSTRAINT_PARAMS_EXPRESSION_COMPARATOR =
            Comparator.comparing(ConstraintAssertion::getConstraint, Constraint.TYPE_PARAMS_COMPARATOR)
                      .thenComparing(ConstraintAssertion::getQualifiedExpression)
                      .thenComparing(ConstraintAssertion::getParams);

    /**
     * @return The constraint that generated this assertion.
     */
    public Constraint getConstraint();

    /**
     * @return The parameters associated to this assertion by its constraint, as a String.<br>
     *         The encoding scheme is free.
     */
    public String getParams();

    /**
     * Sets the expression associated to this assertion.
     *
     * @param expression The expression.
     */
    public void setExpression(Expression expression);
}