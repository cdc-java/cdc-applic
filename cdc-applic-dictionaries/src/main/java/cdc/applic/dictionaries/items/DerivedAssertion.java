package cdc.applic.dictionaries.items;

/**
 * Marking interface of <em>derived</em> assertions.
 * <p>
 * They are derived from ancestors dictionaries.
 * A dictionary that has no parents should not have any derived assertion.
 *
 * @author Damien Carbonne
 */
public interface DerivedAssertion extends Assertion {
    // Ignore
}