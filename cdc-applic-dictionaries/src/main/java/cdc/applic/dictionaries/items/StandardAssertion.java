package cdc.applic.dictionaries.items;

/**
 * Marking interface of <em>standard</em> interfaces: <em>user defined</em> and <em>constraint</em>.
 *
 * @author Damien Carbonne
 */
public interface StandardAssertion extends LocalAssertion {
    // Ignore
}
