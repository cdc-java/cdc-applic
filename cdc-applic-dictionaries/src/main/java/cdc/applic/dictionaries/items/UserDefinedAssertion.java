package cdc.applic.dictionaries.items;

/**
 * Base interface of <em>user defined</em> assertions.
 *
 * @author Damien Carbonne
 */
public interface UserDefinedAssertion extends StandardAssertion {
    // Ignore
}