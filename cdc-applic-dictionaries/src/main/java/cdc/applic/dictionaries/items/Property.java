package cdc.applic.dictionaries.items;

import java.util.Comparator;

import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.RealType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.StringSet;

/**
 * A property is a characteristic of a product.
 * <p>
 * It has a type that defines the set of values that can be taken.
 *
 * @author Damien Carbonne
 */
public interface Property extends NamedDItem {
    /**
     * @return The type of this property.
     */
    public Type getType();

    public static int compare(Property property,
                              SItemSet set1,
                              SItemSet set2) {
        final Type type = property.getType();
        if (type instanceof final EnumeratedType t) {
            final Comparator<StringSet> cmp = EnumeratedType.lexicographicOrdinalLiteralComparator(t);
            return cmp.compare(set1.toStringSet(), set2.toStringSet());
        } else if (type instanceof PatternType) {
            return StringSet.LEXICOGRAPHIC_COMPARATOR.compare(set1.toStringSet(), set2.toStringSet());
        } else if (type instanceof IntegerType) {
            return IntegerSet.LEXICOGRAPHIC_COMPARATOR.compare(set1.toIntegerSet(), set2.toIntegerSet());
        } else if (type instanceof RealType) {
            return RealSet.LEXICOGRAPHIC_COMPARATOR.compare(set1.toRealSet(), set2.toRealSet());
        } else {
            return BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(set1.toBooleanSet(), set2.toBooleanSet());
        }
    }
}