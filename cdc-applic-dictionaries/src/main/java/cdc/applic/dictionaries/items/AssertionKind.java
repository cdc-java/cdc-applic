package cdc.applic.dictionaries.items;

import cdc.applic.dictionaries.Dictionary;

/**
 * Enumeration of possible assertion kinds.
 *
 * @author Damien Carbonne
 */
public enum AssertionKind {
    /**
     * The assertion is locally <em>user-defined</em>. It is a <em>standard</em> assertion.
     */
    USER_DEFINED,

    /**
     * The assertion is locally generated from a <em>constraint</em>. It is a <em>standard</em> assertion.
     */
    CONSTRAINT,

    /**
     * The assertion is locally generated from the <em>context</em> expression.
     */
    CONTEXT,

    /**
     * The assertion is <em>derived</em> from a <em>standard</em> assertion
     * belonging to an ancestor {@link Dictionary}.
     */
    DERIVED_STANDARD,

    /**
     * The assertion is <em>derived</em> from <em>context</em> assertions
     * belonging to ancestor {@link Dictionary Dictionaries}.
     */
    DERIVED_CONTEXT;
}