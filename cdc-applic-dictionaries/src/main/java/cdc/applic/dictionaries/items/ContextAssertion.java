package cdc.applic.dictionaries.items;

/**
 * Marking interface of <em>context</em> assertions.
 * <p>
 * They are <em>locally generated</em> from the context expression.
 *
 * @author Damien Carbonne
 */
public interface ContextAssertion extends LocalAssertion {
    // Ignore
}