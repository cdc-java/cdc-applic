package cdc.applic.dictionaries.items;

import cdc.applic.dictionaries.Dictionary;

/**
 * Marking interface of assertions that are derived from ancestors <em>standard</em> assertions.
 *
 * @author Damien Carbonne
 */
public interface DerivedStandardAssertion extends DerivedAssertion {
    /**
     * @return The {@link LocalAssertion} from which this one is derived.
     */
    public StandardAssertion getSourceAssertion();

    /**
     * @return The {@link Dictionary} containing the source assertion.
     */
    public Dictionary getSourceDictionary();
}