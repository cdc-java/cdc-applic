package cdc.applic.dictionaries.items;

/**
 * Marking interface of <em>locally</em> declared {@link Assertion Assertions}.
 * <p>
 * It can be <em>user defined</em>, <em>constraint</em> or <em>context</em>.
 *
 * @author Damien Carbonne
 */
public interface LocalAssertion extends Assertion {
    // Ignore
}