package cdc.applic.dictionaries.items;

import java.util.Comparator;

import cdc.applic.expressions.Expression;

/**
 * Base interface of {@link DItem items} that are associated to an {@link Expression}:
 * {@link Alias Aliases} and {@link Assertion Assertions}.
 *
 * @author Damien Carbonne
 */
public interface DExpressed extends DItem {
    /**
     * A Comparator of DExpresseds based on associated expression.
     */
    public static final Comparator<DExpressed> EXPRESSION_COMPARATOR =
            Comparator.comparing(DExpressed::getQualifiedExpression);

    /**
     * @return The associated expression.
     */
    public Expression getExpression();

    /**
     * @return The associated expression, with all prefixes.
     */
    public Expression getQualifiedExpression();
}