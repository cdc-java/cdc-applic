package cdc.applic.dictionaries.items;

import java.util.Comparator;

import cdc.applic.dictionaries.Designated;
import cdc.applic.dictionaries.MultiNamed;
import cdc.applic.dictionaries.types.Ordered;
import cdc.applic.expressions.literals.Name;

/**
 * Base interface of items that have a {@link Name}: {@link Property Properties} and {@link Alias Aliases}.
 *
 * @author Damien Carbonne
 */
public interface NamedDItem extends DItem, Designated, MultiNamed, Ordered {
    public static final Comparator<NamedDItem> ORDINAL_NAME_COMPARATOR =
            Comparator.comparing(NamedDItem::getOrdinal)
                      .thenComparing(NamedDItem::getName);
}