package cdc.applic.dictionaries.items;

import java.util.Comparator;

/**
 * Base interface of aliases.
 * <p>
 * An Alias is an named expression. This name can be used in place of the expression.
 *
 * @author Damien Carbonne
 */
public interface Alias extends DExpressed, NamedDItem {
    public static final Comparator<Alias> NAME_EXPRESSION_COMPARATOR =
            Comparator.comparing(Alias::getName)
                      .thenComparing(Alias::getQualifiedExpression);
}