package cdc.applic.dictionaries.items;

import java.util.Comparator;

import cdc.applic.dictionaries.DOwnedElement;
import cdc.applic.dictionaries.Described;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.expressions.literals.Named;

/**
 * Marking interface of dictionary items: {@link Property Properties}, {@link Alias Aliases} and {@link Assertion Assertions}.
 *
 * @author Damien Carbonne
 */
public interface DItem extends DOwnedElement, Described {
    public static final Comparator<DItem> CLASS_COMPARATOR =
            Comparator.comparing(DItem::getClassRank);

    @Override
    public Dictionary getOwner();

    /**
     * A DItem comparator that first compares classes.
     * <p>
     * {@link Property} {@code <} {@link Alias} {@code <} {@link Assertion} {@code <} {@link ConstraintAssertion}
     */
    public static final Comparator<DItem> COMPARATOR = (DItem o1,
                                                        DItem o2) -> {
        final int classCmp = CLASS_COMPARATOR.compare(o1, o2);
        if (classCmp == 0) {
            if (o1 instanceof final ConstraintAssertion ca1) {
                // Test GeneratedAssertion before Assertion
                return ConstraintAssertion.CONSTRAINT_PARAMS_EXPRESSION_COMPARATOR.compare(ca1,
                                                                                           (ConstraintAssertion) o2);
            } else if (o1 instanceof final Assertion a1) {
                return Assertion.KIND_EXPRESSION_COMPARATOR.compare(a1, (Assertion) o2);
            } else if (o1 instanceof final Alias a1) {
                return Alias.NAME_EXPRESSION_COMPARATOR.compare(a1, (Alias) o2);
            } else {
                return Named.NAME_COMPARATOR.compare((Property) o1, (Property) o2);
            }
        } else {
            return classCmp;
        }
    };

    public static int getClassRank(DItem item) {
        if (item instanceof Property) {
            return 1;
        } else if (item instanceof Alias) {
            return 2;
        } else if (item instanceof ConstraintAssertion) {
            return 4;
        } else if (item instanceof Assertion) {
            return 3;
        } else {
            return -1;
        }
    }
}