package cdc.applic.dictionaries.items;

/**
 * Marking interface of assertions that are <em>derived</em> from <em>contexts</em> of ancestors.
 *
 * @author Damien Carbonne
 */
public interface DerivedContextAssertion extends DerivedAssertion {
    // Ignore
}
