package cdc.applic.dictionaries.types;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cdc.applic.expressions.content.StringSItem;
import cdc.applic.expressions.content.StringValue;
import cdc.util.lang.Checks;

/**
 * A PatternType is a {@link StringType} whose allowed values are defined by a pattern.
 * <p>
 * PatternType should only be used when {@link EnumeratedType} can not be used.
 *
 * @author Damien Carbonne
 */
public interface PatternType extends StringType {
    /**
     * @return The pattern that compliant values shall follow.
     */
    public Pattern getPattern();

    public static boolean isCompliant(PatternType type,
                                      StringSItem item) {
        Checks.isNotNull(item, "item");

        if (StringValue.PATTERN_UNKNOWN.equals(item)) {
            return true;
        } else {
            // StringValue are the only supported item at the moment
            final Matcher m = type.getPattern().matcher(((StringValue) item).getNonEscapedLiteral());
            return m.matches();
        }
    }
}