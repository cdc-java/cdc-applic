package cdc.applic.dictionaries.types;

import java.util.Comparator;

/**
 * Interface implemented by object that have an ordinal.
 * <p>
 * This ordinal can be used to sort objects.
 *
 * @author Damien Carbonne
 */
@FunctionalInterface
public interface Ordered {
    /**
     * A Comparator of Ordered objects that uses ordinal.
     */
    public static final Comparator<Ordered> ORDINAL_COMPARATOR =
            Comparator.comparing(Ordered::getOrdinal);

    /**
     * @return The ordinal of this object.
     */
    public int getOrdinal();
}