package cdc.applic.dictionaries.types;

import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.Value;
import cdc.graphs.PartialOrderPosition;

/**
 * Interface implemented by types associated to a partial order.
 *
 * @author Damien Carbonne
 *
 * @param <V> The value type.
 * @param <S> The SItemSet type.
 */
public interface PartiallyOrderedType<V extends Value, S extends SItemSet> extends Type {
    /**
     * Returns the relative position of 2 values.
     *
     * @param left The left value.
     * @param right The right value.
     * @return The position of {@code left} relatively to {@code right}..
     */
    public PartialOrderPosition partialCompare(V left,
                                               V right);

    /**
     * Returns a set defined by a value and a partial order position.
     *
     * @param value The value.
     * @param position The position of other values relatively to {@code value}.
     * @return The set of values whose position matches {@code position}.
     */
    public S toSet(V value,
                   PartialOrderPosition position);

    /**
     * @return {@code true} if the partial order of this type is relevant.<br>
     *         &exist; (x, y), x &ne; y, partialCompare(x, y) != UNRELATED.
     */
    public boolean isRelevantPartialOrder();
}