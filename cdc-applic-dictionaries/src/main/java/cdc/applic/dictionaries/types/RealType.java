package cdc.applic.dictionaries.types;

import java.util.Optional;

import cdc.applic.expressions.content.RealSItem;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.SItem;
import cdc.graphs.PartialOrderPosition;
import cdc.util.lang.Checks;

/**
 * Interface describing a real type.
 *
 * @author Damien Carbonne
 */
public interface RealType extends ModifiableType, DomainedType, PartiallyOrderedType<RealValue, RealSet> {
    /**
     * @return The {@link RealSItem}s that define this type.
     */
    @Override
    public RealSet getDomain();

    /**
     * Returns {@code true} when a {@link RealSItem} is compliant with this type.
     *
     * @param item The item.
     * @return {@code true} when {@code item} is compliant with this type.
     */
    public default boolean isCompliant(RealSItem item) {
        Checks.isNotNull(item, "item");

        return getDomain().contains(item);
    }

    @Override
    default PartialOrderPosition partialCompare(RealValue left,
                                                RealValue right) {
        Checks.isNotNull(left, "left");

        return left.partialCompareTo(right);
    }

    @Override
    default RealSet toSet(RealValue value,
                          PartialOrderPosition position) {
        return RealSet.of(value, position).intersection(getDomain());
    }

    @Override
    default boolean isRelevantPartialOrder() {
        return true;
    }

    @Override
    public default Optional<RealValue> getDefaultValue() {
        return Optional.empty();
    }

    public static boolean isCompliant(RealType type,
                                      SItem item) {
        Checks.isNotNull(item, "item");

        if (item instanceof final RealSItem i) {
            return type.isCompliant(i);
        } else {
            return false;
        }
    }
}