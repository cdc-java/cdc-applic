package cdc.applic.dictionaries.types;

import java.util.Optional;

import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.StringSItem;
import cdc.applic.expressions.content.StringValue;
import cdc.util.lang.Checks;

/**
 * Base interface of string based types: {@link EnumeratedType} and {@link PatternType}.
 *
 * @author Damien Carbonne
 */
public interface StringType extends ModifiableType {
    /**
     * Returns {@code true} when a {@link StringSItem} is compliant with this type.
     *
     * @param item The item.
     * @return {@code true} when {@code item} is compliant with this type.
     */
    public default boolean isCompliant(StringSItem item) {
        return isCompliant(this, item);
    }

    public static boolean isCompliant(StringType type,
                                      SItem item) {
        Checks.isNotNull(item, "item");

        if (item instanceof final StringSItem i) {
            return type.isCompliant(i);
        } else {
            return false;
        }
    }

    @Override
    public default Optional<StringValue> getDefaultValue() {
        return Optional.empty();
    }

    public static boolean isCompliant(StringType type,
                                      StringSItem item) {
        if (type instanceof final EnumeratedType t) {
            return EnumeratedType.isCompliant(t, item);
        } else {
            return PatternType.isCompliant((PatternType) type, item);
        }
    }
}