package cdc.applic.dictionaries.types;

import cdc.applic.expressions.content.SItemSet;

/**
 * Interface of types that have a Domain.
 */
public interface DomainedType extends Type {
    /**
     * @return The {@link SItemSet} that defines this type.
     */
    public SItemSet getDomain();
}