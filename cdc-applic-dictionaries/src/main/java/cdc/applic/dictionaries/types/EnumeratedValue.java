package cdc.applic.dictionaries.types;

import java.util.Comparator;

import cdc.applic.dictionaries.Described;
import cdc.applic.dictionaries.RefSyn;
import cdc.applic.expressions.content.StringValue;

/**
 * Enumerated value.
 *
 * @author Damien Carbonne
 */
public interface EnumeratedValue extends Described, Ordered {
    /**
     * A Comparator of EnumeratedValues that uses ordinal then literal.
     */
    public static final Comparator<EnumeratedValue> ORDINAL_LITERAL_COMPARATOR =
            Comparator.comparing(EnumeratedValue::getOrdinal)
                      .thenComparing(EnumeratedValue::getLiteral);

    public RefSyn<StringValue> getLiterals();

    /**
     * @return The literal of this enumerated value.
     */
    public default StringValue getLiteral() {
        return getLiterals().getReferenceValue();
    }

    /**
     * @return The short literal of this enumerated value.
     */
    public StringValue getShortLiteral();
}