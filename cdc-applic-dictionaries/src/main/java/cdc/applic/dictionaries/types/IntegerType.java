package cdc.applic.dictionaries.types;

import java.util.Optional;

import cdc.applic.expressions.content.IntegerSItem;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.SItem;
import cdc.graphs.PartialOrderPosition;
import cdc.util.lang.Checks;

/**
 * Interface describing an integer type.
 *
 * @author Damien Carbonne
 */
public interface IntegerType extends ModifiableType, DomainedType, CountableType, PartiallyOrderedType<IntegerValue, IntegerSet> {

    /**
     * @return The {@link IntegerSItem}s that define this type.
     */
    @Override
    public IntegerSet getDomain();

    @Override
    public default Iterable<IntegerValue> getCountableValues() {
        return getDomain().getValues();
    }

    @Override
    public default long getExtent() {
        return getDomain().getExtent();
    }

    /**
     * Returns {@code true} when an {@link IntegerSItem} is compliant with this type.
     *
     * @param item The item.
     * @return {@code true} when {@code item} is compliant with this type.
     */
    public default boolean isCompliant(IntegerSItem item) {
        Checks.isNotNull(item, "item");

        return getDomain().contains(item);
    }

    @Override
    default PartialOrderPosition partialCompare(IntegerValue left,
                                                IntegerValue right) {
        Checks.isNotNull(left, "left");

        return left.partialCompareTo(right);
    }

    @Override
    default IntegerSet toSet(IntegerValue value,
                             PartialOrderPosition position) {
        return IntegerSet.of(value, position).intersection(getDomain());
    }

    @Override
    default boolean isRelevantPartialOrder() {
        return true;
    }

    @Override
    public default Optional<IntegerValue> getDefaultValue() {
        return Optional.empty();
    }

    public static boolean isCompliant(IntegerType type,
                                      SItem item) {
        Checks.isNotNull(item, "item");

        if (item instanceof final IntegerSItem i) {
            return type.isCompliant(i);
        } else {
            return false;
        }
    }
}