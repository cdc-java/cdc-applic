package cdc.applic.dictionaries.types;

import java.util.Comparator;
import java.util.Optional;

import cdc.applic.dictionaries.Described;
import cdc.applic.dictionaries.MultiNamed;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.CheckedSet;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.content.Value;
import cdc.util.lang.Checks;

/**
 * Base interface of types.
 *
 * @author Damien Carbonne
 */
public interface Type extends Described, MultiNamed {
    public static Comparator<Type> NAME_COMPARATOR =
            Comparator.comparing(Type::getName);

    /**
     * Returns {@code true} if an {@link SItem} is compliant with this type.
     *
     * @param item The item.
     * @return {@code true} if {@code item} is compliant with this type.
     */
    public default boolean isCompliant(SItem item) {
        return isCompliant(this, item);
    }

    /**
     * Returns {@code true} if an {@link SItemSet} is compliant with this type.
     *
     * @param set The set.
     * @return {@code true} if {@code set} is compliant with this type.
     */
    public default boolean isCompliant(SItemSet set) {
        for (final SItem item : set.getItems()) {
            if (!isCompliant(item)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return A string describing this type.
     */
    public default String getDefinition() {
        return getDefinition(this);
    }

    /**
     * @return An empty set compliant with this type.
     */
    public default SItemSet getEmptySet() {
        return getEmptySet(this);
    }

    /**
     * @return The optional default value associated to this type.
     */
    public Optional<? extends Value> getDefaultValue();

    public default boolean hasDefaultValue() {
        return getDefaultValue().isPresent();
    }

    public static Class<? extends CheckedSet<?, ?>> getCheckedSetClass(Type type) {
        Checks.isNotNull(type, "type");

        if (type instanceof StringType) {
            return StringSet.class;
        } else if (type instanceof IntegerType) {
            return IntegerSet.class;
        } else if (type instanceof RealType) {
            return RealSet.class;
        } else {
            return BooleanSet.class;
        }
    }

    public static TypeKind getTypeKind(Type type) {
        Checks.isNotNull(type, "type");

        if (type instanceof StringType) {
            return TypeKind.STRING;
        } else if (type instanceof IntegerType) {
            return TypeKind.INTEGER;
        } else if (type instanceof RealType) {
            return TypeKind.REAL;
        } else {
            return TypeKind.BOOLEAN;
        }
    }

    /**
     * Returns {@code true} if a type has an empty definition.
     * <p>
     * In that case, the type is invalid.
     * We consider that all pattern types are correctly defined.
     *
     * @param type The type.
     * @return {@code true} if {@code type} has an empty definition.
     */
    public static boolean isEmpty(Type type) {
        if (type instanceof final DomainedType t) {
            return t.getDomain().isEmpty();
        } else {
            return false;
        }
    }

    /**
     * Default implementation of Type definition.
     *
     * @param type The type.
     * @return A string describing {@code type}.
     */
    public static String getDefinition(Type type) {
        Checks.isNotNull(type, "type");

        if (type instanceof BooleanType) {
            return "false, true";
        } else if (type instanceof final EnumeratedType t) {
            final StringBuilder builder = new StringBuilder();
            boolean first = true;
            for (final StringValue value : t.getDomain().getItems()) {
                if (first) {
                    first = false;
                } else {
                    builder.append(", ");
                }
                builder.append(value);
            }
            return builder.toString();
        } else if (type instanceof final PatternType t) {
            return t.getPattern().pattern();
        } else if (type instanceof final IntegerType t) {
            return t.getDomain().getContent();
        } else {
            // RealTye
            final RealType t = (RealType) type;
            return t.getDomain().getContent();
        }
    }

    public static boolean isCompliant(Type type,
                                      SItem item) {
        if (type instanceof final StringType t) {
            return StringType.isCompliant(t, item);
        } else if (type instanceof final IntegerType t) {
            return IntegerType.isCompliant(t, item);
        } else if (type instanceof final RealType t) {
            return RealType.isCompliant(t, item);
        } else {
            return BooleanType.isCompliant((BooleanType) type, item);
        }
    }

    public static SItemSet getEmptySet(Type type) {
        if (type instanceof StringType) {
            return StringSet.EMPTY;
        } else if (type instanceof IntegerType) {
            return IntegerSet.EMPTY;
        } else if (type instanceof RealType) {
            return RealSet.EMPTY;
        } else {
            return BooleanSet.EMPTY;
        }
    }
}