package cdc.applic.dictionaries.types;

import java.util.List;
import java.util.Optional;

import cdc.applic.expressions.content.BooleanSItem;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.SItem;
import cdc.graphs.PartialOrderPosition;
import cdc.util.lang.Checks;

/**
 * Interface of boolean types.
 *
 * @author Damien Carbonne
 */
public interface BooleanType extends Type, DomainedType, CountableType, PartiallyOrderedType<BooleanValue, BooleanSet> {

    /**
     * @return The {@link BooleanSItem}s that define this type.
     */
    @Override
    public BooleanSet getDomain();

    @Override
    public default List<BooleanValue> getCountableValues() {
        return BooleanSet.FALSE_TRUE.getItems();
    }

    @Override
    public default long getExtent() {
        return getDomain().getExtent();
    }

    /**
     * Returns {@code true} when an {@link BooleanSItem} is compliant with this type.
     *
     * @param item The item.
     * @return {@code true} when {@code item} is compliant with this type.
     */
    public default boolean isCompliant(BooleanSItem item) {
        Checks.isNotNull(item, "item");

        return true;
    }

    @Override
    public default PartialOrderPosition partialCompare(BooleanValue left,
                                                       BooleanValue right) {
        Checks.isNotNull(left, "left");

        return left.partialCompareTo(right);
    }

    @Override
    default BooleanSet toSet(BooleanValue value,
                             PartialOrderPosition position) {
        return BooleanSet.toSet(value, position);
    }

    @Override
    default boolean isRelevantPartialOrder() {
        return true;
    }

    @Override
    public default Optional<BooleanValue> getDefaultValue() {
        return Optional.empty();
    }

    public static boolean isCompliant(BooleanType type,
                                      SItem item) {
        Checks.isNotNull(item, "item");

        if (item instanceof final BooleanSItem i) {
            return type.isCompliant(i);
        } else {
            return false;
        }
    }
}