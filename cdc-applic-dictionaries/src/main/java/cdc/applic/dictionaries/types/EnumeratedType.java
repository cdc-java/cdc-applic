package cdc.applic.dictionaries.types;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.NamingConventionUtils;
import cdc.applic.expressions.content.StringSItem;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.graphs.PartialOrderPosition;
import cdc.util.lang.Checks;

/**
 * An enumerated type is a string type that is defined in extension.
 * <p>
 * The enumerated values literals shall be different.<br>
 * The enumerated values short literals shall also be different.
 *
 * @author Damien Carbonne
 */
public interface EnumeratedType extends StringType, DomainedType, CountableType, PartiallyOrderedType<StringValue, StringSet> {
    /**
     * @return The {@link StringSItem}s that defines this type.
     */
    @Override
    public StringSet getDomain();

    @Override
    public default List<StringValue> getCountableValues() {
        return getDomain().getItems();
    }

    @Override
    public default long getExtent() {
        return getDomain().getExtent();
    }

    /**
     * @return The enumerated values that defines this type.
     */
    public List<? extends EnumeratedValue> getValues();

    /**
     * Returns the enumerated value that has a given name.
     *
     * @param name The name.
     * @return The enumerated value that is named {@code name}, or {@code null}.
     */
    public EnumeratedValue getValue(StringValue name);

    public default EnumeratedValue getValue(String name) {
        return getValue(StringValue.of(name));
    }

    public default Set<NamingConvention> getNamingConventions(StringValue name) {
        final EnumeratedValue value = getValue(name);
        return value.getLiterals().getNamingConventions(name);
    }

    public default Set<NamingConvention> getNamingConventions(String name) {
        return getNamingConventions(StringValue.of(name));
    }

    public default StringValue convertLiteral(StringValue literal,
                                              NamingConvention convention) {
        final EnumeratedValue value = getValue(literal);
        return value == null ? literal : value.getLiterals().getValue(convention);
    }

    public default StringValue convertLiteral(String literal,
                                              NamingConvention convention) {
        return convertLiteral(StringValue.of(literal), convention);
    }

    /**
     * Returns a set of all literals for which a less than relationship has been defined.
     *
     * @param literal The reference literal.
     * @return A set of all literals {@code l} such as {@code literal < l} by definition.
     */
    public Set<StringValue> getDefinedLessThan(StringValue literal);

    @Override
    default PartialOrderPosition partialCompare(StringValue left,
                                                StringValue right) {
        Checks.isNotNull(left, "left");
        Checks.isNotNull(right, "right");

        return PartialOrderPosition.UNRELATED;
    }

    /**
     * Creates a StringSet Comparator based on ordinal and literal comparison of StringValues.
     *
     * @param type The type.
     * @return A new StringSet Comparator.
     */
    public static Comparator<StringSet> lexicographicOrdinalLiteralComparator(EnumeratedType type) {
        final Comparator<StringValue> comparator = (sv1,
                                                    sv2) -> {
            final EnumeratedValue ev1 = type.getValue(sv1);
            final EnumeratedValue ev2 = type.getValue(sv2);
            return EnumeratedValue.ORDINAL_LITERAL_COMPARATOR.compare(ev1, ev2);
        };

        return StringSet.lexicographicComparator(comparator);
    }

    public static boolean isCompliant(EnumeratedType type,
                                      StringSItem item) {
        Checks.isNotNull(type, "type");
        Checks.isNotNull(item, "item");

        return type.getDomain()
                   .contains(NamingConventionUtils.convert(item,
                                                           StringSItem.class,
                                                           type,
                                                           NamingConvention.DEFAULT));
    }
}