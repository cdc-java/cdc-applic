package cdc.applic.dictionaries.types;

import cdc.applic.expressions.content.Value;

/**
 * Interface of countable types, for which we can explicitly list all values.
 *
 * @author Damien Carbonne
 */
public interface CountableType extends Type {
    /**
     * @return An Iterable of Values defined by this Type.
     */
    public Iterable<? extends Value> getCountableValues();

    /**
     * @return The number of Values defined by this type (ignoring reserves).
     */
    public long getExtent();
}