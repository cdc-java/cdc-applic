package cdc.applic.dictionaries.types;

/**
 * Enumeration of basic (technical) types.
 *
 * @author Damien Carbonne
 */
public enum TypeKind {
    BOOLEAN,
    INTEGER,
    REAL,
    STRING;

    public String getS1000DLiteral() {
        return name().toLowerCase();
    }
}