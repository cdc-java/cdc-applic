package cdc.applic.dictionaries;

import java.util.List;

import cdc.util.lang.Checks;

/**
 * Enumeration of possible usages of an item (Property or Alias) in a Dictionary.
 *
 * @author Damien Carbonne
 */
public enum DItemUsage {
    /**
     * Use of the item is forbidden.
     */
    FORBIDDEN,

    /**
     * Use of the item is possible.
     */
    OPTIONAL,

    /**
     * Use of the item is recommended.
     */
    RECOMMENDED,

    /**
     * Use of the item is mandatory.
     */
    MANDATORY;

    /**
     * Returns {@code true} when a child usage is compliant with parents usages.
     * <ul>
     * <li>If there are no parents, then any child usage is possible.
     * <li>If there are parents, and all parents usages are {@link DItemUsage#FORBIDDEN FORBIDDEN},
     * then child usage can only be {@link DItemUsage#FORBIDDEN FORBIDDEN}.
     * <li>Otherwise, if any parent usage is different from {@link DItemUsage#FORBIDDEN FORBIDDEN},
     * then any child usage is possible.
     * </ul>
     * <b>WARNING:</b> this should be only used if associated item is available in child dictionary.
     * For parents, only dictionaries where the associated item is available should be used.
     *
     * @param child The child usage.
     * @param parents The parents usages.
     * @return {@code true} if {@code child} is compliant with {@code parents}.
     */
    public static boolean isCompliant(DItemUsage child,
                                      List<DItemUsage> parents) {
        Checks.isNotNull(child, "childUsage");
        Checks.isNotNull(parents, "parentsUsages");

        for (final DItemUsage usage : parents) {
            if (usage != DItemUsage.FORBIDDEN) {
                return true;
            }
        }
        // Here, all parents (if any) are FORBIDEN
        return parents.isEmpty() || child == DItemUsage.FORBIDDEN;
    }
}
