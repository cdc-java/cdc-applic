package cdc.applic.dictionaries;

import java.util.Comparator;

public interface Dictionary
        extends DOwnedElement, DictionaryStructure, DictionaryItems, DictionaryDeclaredItems, DictionaryAssertions,
        DictionaryConstraints,
        DictionaryPermissions {
    /** A comparator of dictionaries based on names. */
    public static Comparator<Dictionary> NAME_COMPARATOR =
            Comparator.comparing(Dictionary::getName);

    /** A comparator of dictionaries based on paths. */
    public static Comparator<Dictionary> PATH_COMPARATOR =
            Comparator.comparing(Dictionary::getPath);

    /**
     * @return The serial number of this dictionary associated to computation of caches.<br>
     *         This number is supposed to change each time the dictionary content changed in a way impacting caches.
     */
    public long getCachesSerial();
}