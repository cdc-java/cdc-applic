package cdc.applic.dictionaries;

import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.Named;

public interface MultiNamed extends Named {
    public RefSyn<Name> getNames();

    @Override
    public default Name getName() {
        return getNames().getReferenceValue();
    }
}