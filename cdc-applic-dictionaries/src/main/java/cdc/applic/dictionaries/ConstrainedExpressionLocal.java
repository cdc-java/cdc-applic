package cdc.applic.dictionaries;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.IllegalOperationException;
import cdc.util.lang.Checks;

/**
 * In-memory implementation of ConstrainedExpression
 *
 * @author Guillaume Forget
 * @author Damien Carbonne
 */
public class ConstrainedExpressionLocal implements ConstrainedExpression {
    protected final DictionaryHandle handle;
    protected Expression expression;
    protected final ConstrainedExpression context;

    protected ConstrainedExpressionLocal(DictionaryHandle handle,
                                         Expression expression,
                                         ConstrainedExpression context) {
        Checks.isNotNull(expression, "expression");

        this.handle = handle; // May be null
        this.expression = expression;
        this.context = context; // May be null
    }

    @Override
    public boolean isReadOnly() {
        return false;
    }

    @Override
    public final Expression getExpression() {
        return expression;
    }

    @Override
    public void setExpressionUnchecked(Expression expression) {
        Checks.isNotNull(expression, "expression");
        if (isReadOnly()) {
            throw new IllegalOperationException("Read-only ConstrainedExpression");
        }

        this.expression = expression;
    }

    @Override
    public final DictionaryHandle getDictionaryHandleOrNull() {
        return handle;
    }

    @Override
    public final ConstrainedExpression getContextOrNull() {
        return context;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private DictionaryHandle handle;
        private Expression expression = Expression.TRUE;
        private ConstrainedExpression context;

        protected Builder() {
            super();
        }

        public Builder handle(DictionaryHandle handle) {
            this.handle = handle;
            return this;
        }

        public Builder context(ConstrainedExpression context) {
            this.context = context;
            return this;
        }

        public Builder expression(Expression expression) {
            this.expression = expression;
            return this;
        }

        public ConstrainedExpressionLocal build() {
            return new ConstrainedExpressionLocal(handle,
                                                  expression,
                                                  context);
        }
    }
}
