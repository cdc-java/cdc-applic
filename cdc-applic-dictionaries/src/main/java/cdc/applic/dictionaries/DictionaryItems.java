package cdc.applic.dictionaries;

import java.util.Optional;
import java.util.Set;

import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;

/**
 * Interface dedicated to <em>available</em> {@link NamedDItem Items} in a {@link Dictionary}.
 * <p>
 * These <em>available</em> items can either be locally declared or inherited from a parent dictionary.<br>
 * The use of available items can be controlled.
 *
 * @author Damien Carbonne
 */
public interface DictionaryItems {
    /**
     * @return an {@link Iterable} of all <em>available</em> {@link NamedDItem Items}
     *         in the {@link Dictionary}.
     */
    public Iterable<NamedDItem> getAllItems();

    /**
     * @return an {@link Iterable} of all <em>available</em> {@link Property Properties}
     *         in the {@link Dictionary}.
     */
    public Iterable<? extends Property> getAllProperties();

    /**
     * @return an {@link Iterable} of all <em>available</em> {@link Alias Aliases}
     *         in the {@link Dictionary}.
     */
    public Iterable<? extends Alias> getAllAliases();

    /**
     * Returns an {@link Optional} referencing the <em>available</em> {@link NamedDItem Item}
     * that has a given {@link Name} and is known in the {@link Dictionary}.
     *
     * @param name The {@link Name}.
     * @return An {@link Optional} referencing the <em>available</em> {@link NamedDItem Item} named {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     */
    public Optional<NamedDItem> getOptionalItem(Name name);

    public default Optional<NamedDItem> getOptionalItem(SName name) {
        return getOptionalItem(Name.of(name));
    }

    public default Optional<NamedDItem> getOptionalItem(String name) {
        return getOptionalItem(Name.of(name));
    }

    /**
     * Returns an {@link Optional} referencing the <em>available</em> {@link Property} that has a given {@link Name}.
     *
     * @param name The {@link Name}.
     * @return An {@link Optional} referencing the <em>available</em> {@link Property} named {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When an {@link NamedDItem Item} named {@code name}
     *             is <em>available</em> but is not a {@link Property}.
     */
    public Optional<? extends Property> getOptionalProperty(Name name);

    public Optional<? extends Property> getOptionalProperty(SName name);

    public Optional<? extends Property> getOptionalProperty(String name);

    /**
     * Returns an {@link Optional} referencing the <em>available</em> {@link Alias} that has a given {@link Name}.
     *
     * @param name The {@link Name}.
     * @return An {@link Optional} referencing the <em>available</em> {@link Alias} named {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When an {@link NamedDItem Item} named {@code name}
     *             is <em>available</em> but is not an {@link Alias}.
     */
    public Optional<? extends Alias> getOptionalAlias(Name name);

    public Optional<? extends Alias> getOptionalAlias(SName name);

    public Optional<? extends Alias> getOptionalAlias(String name);

    /**
     * Returns The {@link NamedDItem Item} that has a given {@link Name}.
     *
     * @param name The {@link Name}.
     * @return The {@link NamedDItem Item} named {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When no {@link NamedDItem Item} named {@code name} is <em>available</em>.
     */
    public NamedDItem getItem(Name name);

    public default NamedDItem getItem(SName name) {
        return getItem(Name.of(name));
    }

    public default NamedDItem getItem(String name) {
        return getItem(Name.of(name));
    }

    /**
     * Returns all naming conventions associated to an item name.
     * <p>
     * <b>WARNING:</b> this is different from all naming conventions associated to an item.
     *
     * @param name The {@link Name}.
     * @return A set of all naming conventions associated to the item {@code name}.
     */
    public Set<NamingConvention> getItemNameNamingConventions(Name name);

    public default Set<NamingConvention> getItemNameNamingConventions(SName name) {
        return getItemNameNamingConventions(Name.of(name));
    }

    public default Set<NamingConvention> getItemNameNamingConventions(String name) {
        return getItemNameNamingConventions(Name.of(name));
    }

    public default Name convertItemName(Name name,
                                        NamingConvention convention) {
        final NamedDItem item = getItem(name);
        return item.getNames().getValue(convention);
    }

    public default Name convertItemName(SName name,
                                        NamingConvention convention) {
        return convertItemName(Name.of(name), convention);
    }

    public default Name convertItemName(String name,
                                        NamingConvention convention) {
        return convertItemName(Name.of(name), convention);
    }

    /**
     * Returns The {@link Property} that has a given {@link Name}.
     *
     * @param name The {@link Name}.
     * @return The {@link Property} named {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When no {@link Property} named {@code name} is <em>available</em>.
     *             This is particularly the case when an {@link Alias} named {@code name} is <em>available</em>.
     */
    public Property getProperty(Name name);

    public Property getProperty(SName name);

    public Property getProperty(String name);

    /**
     * Returns The {@link Alias} that has a given {@link Name}.
     *
     * @param name The {@link Name}.
     * @return The {@link Alias} named {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When no {@link Alias} named {@code name} is <em>available</em>.
     *             This is particularly the case when an {@link Property} named {@code name} is <em>available</em>.
     */
    public Alias getAlias(Name name);

    public Alias getAlias(SName name);

    public Alias getAlias(String name);

    /**
     * Returns {@code true} if an {@link NamedDItem Item} is <em>available</em> in this {@link Dictionary}.
     *
     * @param item The {@link NamedDItem Item}.
     * @return {@code true} if {@code item} is <em>available</em> in this {@link Dictionary}.
     */
    public boolean isAvailable(NamedDItem item);

    /**
     * Returns the {@link DictionaryMembership Membership} of an {@link NamedDItem Item}.
     *
     * @param item The {@link NamedDItem Item}.
     * @return the {@link DictionaryMembership Membership} of {@code item}.
     */
    public DictionaryMembership getMembership(NamedDItem item);

    /**
     * Returns {@code true} if an {@link NamedDItem Item} with a particular name is <em>available</em> in this {@link Dictionary}.
     *
     * @param name The {@link Name}.
     * @return {@code true} if an {@link NamedDItem Item} named {@code name} is <em>available</em> in this {@link Dictionary}.
     */
    public boolean hasItem(Name name);

    public default boolean hasItem(SName name) {
        return hasItem(Name.of(name));
    }

    public default boolean hasItem(String name) {
        return hasItem(Name.of(name));
    }

    /**
     * Returns {@code true} if a {@link Property} with a particular name is <em>available</em> in this {@link Dictionary}.
     *
     * @param name The {@link Name}.
     * @return {@code true} if a {@link Property} named {@code name} is <em>available</em> in this {@link Dictionary}.
     */
    public boolean hasProperty(Name name);

    public default boolean hasProperty(SName name) {
        return hasProperty(Name.of(name));
    }

    public default boolean hasProperty(String name) {
        return hasProperty(Name.of(name));
    }

    /**
     * Returns {@code true} if an {@link Alias} with a particular name is <em>available</em> in this {@link Dictionary}.
     *
     * @param name The {@link Name}.
     * @return {@code true} if an {@link Alias} named {@code name} is <em>available</em> in this {@link Dictionary}.
     */
    public boolean hasAlias(Name name);

    public default boolean hasAlias(SName name) {
        return hasAlias(Name.of(name));
    }

    public default boolean hasAlias(String name) {
        return hasAlias(Name.of(name));
    }
}