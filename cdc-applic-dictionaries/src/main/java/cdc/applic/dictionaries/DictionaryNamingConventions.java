package cdc.applic.dictionaries;

import java.util.Optional;
import java.util.Set;

import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;

/**
 * Interface dedicated to naming conventions.
 *
 * @author Damien Carbonne
 */
public interface DictionaryNamingConventions {
    /**
     * @return A {@link Set} of <em>declared</em> naming conventions.<br>
     *         The {@link NamingConvention#DEFAULT default} one is excluded.
     */
    public Set<? extends NamingConvention> getDeclaredNamingConventions();

    /**
     * @return A {@link Set} of <em>all</em> naming conventions.<br>
     *         The {@link NamingConvention#DEFAULT default} one is included.
     */
    public Set<? extends NamingConvention> getAllNamingConventions();

    public DictionaryMembership getMembership(NamingConvention convention);

    public Optional<NamingConvention> getOptionalNamingConvention(Name name);

    public default Optional<NamingConvention> getOptionalNamingConvention(SName name) {
        return getOptionalNamingConvention(Name.of(name));
    }

    public default Optional<NamingConvention> getOptionalNamingConvention(String name) {
        return getOptionalNamingConvention(Name.of(name));
    }

    public NamingConvention getNamingConvention(Name name);

    public default NamingConvention getNamingConvention(SName name) {
        return getNamingConvention(Name.of(name));
    }

    public default NamingConvention getNamingConvention(String name) {
        return getNamingConvention(Name.of(name));
    }
}