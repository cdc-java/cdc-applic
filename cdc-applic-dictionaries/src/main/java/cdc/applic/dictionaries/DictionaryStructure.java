package cdc.applic.dictionaries;

import java.util.List;
import java.util.Optional;

import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.expressions.literals.SName;
import cdc.util.lang.Checks;
import cdc.util.lang.NotFoundException;
import cdc.util.paths.Path;

/**
 * Interface dedicated to structure (relationships) of {@link Dictionary Dictionaries}.
 * <p>
 * Currently, there are 2 kinds of dictionaries:
 * <ul>
 * <li>{@link Registry Registries}.
 * A Registry can have any number (0, 1 or more) of Registries as parents.<br>
 * A Registry can have any number (0, 1 or more) of children Dictionaries (Registries or Policies).<br>
 * <li>{@link Policy Policies}.
 * A Policy must have exactly one (1) parent Dictionary (Registry or Policy).<br>
 * A Policy can have any number (0, 1 or more) of children Policies.
 * </ul>
 * A Registry is intended to define a new vocabulary, and optionally inherit other vocabularies and constrain them.<br>
 * A Policy is intended to inherit one vocabulary and constrain it.
 * <p>
 * Registries make a lattice and Policies make a forest rooted in registries.
 * <p>
 * Use of Registries is mandatory, whereas use of Policies is optional.<br>
 * Therefore, one could only use Registries.
 * One fundamental and practical difference is that Registries have a unique name in a repository,
 * whilst policies can have identical names as long as they are not siblings.
 *
 * @author Damien Carbonne
 */
public interface DictionaryStructure extends Designated, Described {
    /**
     * @return The name of this {@link Dictionary}.
     */
    public String getName();

    /**
     * @return The prefix to use to explicitly designate an {@link NamedDItem item} of this {@link Dictionary}.
     */
    public Optional<SName> getPrefix();

    public boolean isDeclaredPrefix(SName prefix);

    /**
     * @return The {@link Registry} of this {@link Dictionary}, possibly itself.
     */
    public Registry getRegistry();

    /**
     * @return A list of children {@link Dictionary Dictionaries} of this {@link Dictionary}.
     */
    public List<? extends Dictionary> getChildren();

    /**
     * Returns a list of children {@link Dictionary Dictionaries} matching a class.
     *
     * @param <T> The children type.
     * @param cls The children class.
     * @return A list of children which are instance of {@code cls}.
     */
    public <T extends Dictionary> List<T> getChildren(Class<T> cls);

    /**
     * @return A list of parent dictionaries.
     */
    public List<? extends Dictionary> getParents();

    public default boolean hasParents() {
        return !getParents().isEmpty();
    }

    /**
     * Returns a list of parents {@link Dictionary Dictionaries} matching a class.
     *
     * @param <T> The parents type.
     * @param cls The parents class.
     * @return A list of parents which are instance of {@code cls}.
     */
    public <T extends Dictionary> List<T> getParents(Class<T> cls);

    /**
     * Return a topologically sorted list of ancestors dictionaries, optionally including itself.
     *
     * @param self If {@code true}, this dictionary is included as first element of the returned list.
     * @return A list topologically sorted list of ancestors dictionaries.
     */
    public List<? extends Dictionary> getSortedAncestors(boolean self);

    /**
     * Return a topologically sorted list of descendants dictionaries, optionally including itself.
     *
     * @param self If {@code true}, this dictionary is included as first element of the returned list.
     * @return A list topologically sorted list of descendants dictionaries.
     */
    public List<? extends Dictionary> getSortedDescendants(boolean self);

    /**
     * @return The {@link Path} of this {@link Dictionary}.
     */
    public Path getPath();

    /**
     * Returns the dictionary at a path.
     * <p>
     * If {@code path} is absolute, the dictionary is searched from the repository.<br>
     * If {@code path} is relative, the dictionary is searched relatively this this dictionary.
     *
     * @param <D> The dictionary type.
     * @param path The dictionary path.
     * @param cls The dictionary class.
     * @return The dictionary corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} or {@code cls} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     * @throws ClassCastException When a dictionary is found, but with a non compliant class.
     */
    public <D extends Dictionary> D getDictionary(Path path,
                                                  Class<D> cls);

    /**
     * Returns the dictionary at a path.
     * <p>
     * If {@code path} is absolute, the dictionary is searched from the repository.<br>
     * If {@code path} is relative, the dictionary is searched relatively this this dictionary.
     *
     * @param <D> The dictionary type.
     * @param path The dictionary path.
     * @param cls The dictionary class.
     * @return The dictionary corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} or {@code cls} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     * @throws ClassCastException When a dictionary is found, but with a non compliant class.
     */
    public default <D extends Dictionary> D getDictionary(String path,
                                                          Class<D> cls) {
        Checks.isNotNull(path, "path");
        return getDictionary(new Path(path), cls);
    }

    /**
     * Returns the dictionary at a path.
     * <p>
     * If {@code path} is absolute, the dictionary is searched from the repository.<br>
     * If {@code path} is relative, the dictionary is searched relatively this this dictionary.
     *
     * @param path The path.
     * @return The dictionary corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     */
    public default Dictionary getDictionary(Path path) {
        return getDictionary(path, Dictionary.class);
    }

    /**
     * Returns the dictionary at a path.
     * <p>
     * If {@code path} is absolute, the dictionary is searched from the repository.<br>
     * If {@code path} is relative, the dictionary is searched relatively this this dictionary.
     *
     * @param path The path.
     * @return The dictionary corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     */
    public default Dictionary getDictionary(String path) {
        Checks.isNotNull(path, "path");
        return getDictionary(new Path(path));
    }

    /**
     * Returns the registry at a path.
     * <p>
     * If {@code path} is absolute, the registry is searched from the repository.<br>
     * If {@code path} is relative, the registry is searched relatively this this dictionary.
     *
     * @param path The path.
     * @return The registry corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     * @throws ClassCastException When a dictionary is found, but is not a registry.
     */
    public default Registry getRegistry(Path path) {
        return getDictionary(path, Registry.class);
    }

    /**
     * Returns the registry at a path.
     * <p>
     * If {@code path} is absolute, the registry is searched from the repository.<br>
     * If {@code path} is relative, the registry is searched relatively this this dictionary.
     *
     * @param path The path.
     * @return The registry corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     * @throws ClassCastException When a dictionary is found, but is not a registry.
     */
    public default Registry getRegistry(String path) {
        Checks.isNotNull(path, "path");
        return getRegistry(new Path(path));
    }

    /**
     * Returns the policy at a path.
     * <p>
     * If {@code path} is absolute, the policy is searched from the repository.<br>
     * If {@code path} is relative, the policy is searched relatively this this dictionary.
     *
     * @param path The path.
     * @return The policy corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     * @throws ClassCastException When a dictionary is found, but is not a policy.
     */
    public default Policy getPolicy(Path path) {
        return getDictionary(path, Policy.class);
    }

    /**
     * Returns the policy at a path.
     * <p>
     * If {@code path} is absolute, the policy is searched from the repository.<br>
     * If {@code path} is relative, the policy is searched relatively this this dictionary.
     *
     * @param path The path.
     * @return The policy corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     * @throws ClassCastException When a dictionary is found, but is not a policy.
     */
    public default Policy getPolicy(String path) {
        return getPolicy(new Path(path));
    }
}