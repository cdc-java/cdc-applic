package cdc.applic.dictionaries;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

/**
 * Interface describing a reference value and a set of synonyms.
 *
 * @author Damien Carbonne
 *
 * @param <T> The value type.
 */
public interface RefSyn<T extends Comparable<? super T>> {
    /**
     * A set of {@link NamingConvention NamingConventions} that only contains
     * the {@link NamingConvention#DEFAULT default} one.
     */
    public static final Set<NamingConvention> DEFAULT_NAMING_CONVENTIONS =
            Set.of(NamingConvention.DEFAULT);

    /**
     * @return The reference value.
     */
    public T getReferenceValue();

    /**
     * @return The set of synonyms of the reference value.<br>
     *         The reference value is NOT considered as a synonym.
     */
    public default Set<T> getSynonyms() {
        return Collections.emptySet();
    }

    /**
     * @return The set of supported {@link NamingConvention NamingConventions},
     *         necessarily including the {@link NamingConvention#DEFAULT default} one.
     */
    public default Set<NamingConvention> getNamingConventions() {
        return DEFAULT_NAMING_CONVENTIONS;
    }

    /**
     * @return The set of supported {@link NamingConvention NamingConventions},
     *         excluding the {@link NamingConvention#DEFAULT default} one.
     */
    public default Set<NamingConvention> getNonDefaultNamingConventions() {
        return Collections.emptySet();
    }

    /**
     * @return {@code true} if some synonyms are defined.
     */
    public default boolean hasSynonyms() {
        return !getSynonyms().isEmpty();
    }

    /**
     * Returns the set of {@link NamingConvention NamingConventions} associated to a value.
     * <p>
     * If the value is neither the reference value nor a synonym, an empty set is returned.
     *
     * @param value The value, that should either be the reference value or a synonym.
     * @return The set of {@link NamingConvention NamingConventions} associated to {@code value}.
     */
    public default Set<NamingConvention> getNamingConventions(T value) {
        if (Objects.equals(value, getReferenceValue())) {
            return DEFAULT_NAMING_CONVENTIONS;
        } else {
            return Collections.emptySet();
        }
    }

    /**
     * Returns the value (reference value or synonym) associated to a {@link NamingConvention}.
     *
     * @param convention The naming convention.
     * @return The value associated to {@code convention}.<br>
     *         If {@code convention} is unknown or is the
     *         {@link NamingConvention#DEFAULT default} convention,
     *         returns the reference value.
     */
    public default T getValue(NamingConvention convention) {
        return getReferenceValue();
    }
}