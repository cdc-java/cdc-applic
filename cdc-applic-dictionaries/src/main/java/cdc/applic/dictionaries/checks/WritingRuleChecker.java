package cdc.applic.dictionaries.checks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.WritingRuleException;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.IllegalOperationException;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.Checker;
import cdc.applic.expressions.checks.SyntaxChecker;
import cdc.issues.Diagnosis;
import cdc.util.lang.Checks;

/**
 * Base class of writing rule checkers.
 * <p>
 * <b>Note:</b> checking a writing rule is possible if the expression is semantically valid.
 *
 * @author Damien Carbonne
 */
public abstract class WritingRuleChecker implements Checker {
    private static final String DATA = "data";
    private static final String EXPRESSION = "expression";
    private static final String HANDLE = "handle";
    private static final String ISSUES = "issues";

    static final Map<String, Function<DictionaryHandle, WritingRuleChecker>> MAP = new HashMap<>();

    /**
     * Method that can be implemented to do the checks.
     * <p>
     * <b>WARNING:</b> if this method is overridden, then {@link #isCompliant(CheckedData)}
     * and {@link #check(CheckedData, List)} should not be overridden.
     * <ul>
     * <li>If {@code issues} is {@code null}, it returns {@code false}
     * when the first issue is found, {@code true} if no issue is found.
     * <li>If {@code issues} is <em>NOT</em> {@code null}, it can return anything.
     * </ul>
     *
     * @param data The {@link CheckedData}.
     * @param issues The optional list of issues.
     * @return {@code false} if {@code issues} is {@code null} and an issue is found,
     *         {@code true} otherwise, whatever the number of issues is.
     * @throws IllegalOperationException When called and not overridden.
     */
    protected boolean doCheck(CheckedData data,
                              List<ApplicIssue> issues) {
        throw new IllegalOperationException("Not overridden");
    }

    /**
     * {@inheritDoc}
     * <p>
     * <b>WARNING:</b> If this method is overridden, then {@link #check(CheckedData, List)}
     * should be overridden and {@link #doCheck(CheckedData, List)} should not.
     */
    @Override
    public boolean isCompliant(CheckedData data) {
        Checks.isNotNull(data, DATA);

        return doCheck(data, null);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <b>WARNING:</b> If this method is overridden, then {@link #isCompliant(CheckedData)}
     * should be overridden and {@link #doCheck(CheckedData, List)} should not.
     */
    @Override
    public void check(CheckedData data,
                      List<ApplicIssue> issues) {
        Checks.isNotNull(data, DATA);
        Checks.isNotNull(issues, ISSUES);

        doCheck(data, issues);
    }

    @Override
    public void checkCompliance(CheckedData data) {
        final Diagnosis<ApplicIssue> diagnosis = check(data);
        if (!diagnosis.isOk()) {
            throw new WritingRuleException("Non compliant expression", diagnosis);
        }
    }

    /**
     * @return The purpose of the rule checked by this checker.
     */
    public abstract WritingRulePurpose getPurpose();

    /**
     * Registers a WritingRuleChecker factory to be used for a rule name.
     *
     * @param writingRuleName The rule name.
     * @param creator The WritingRuleChecker factory.<br>
     *            It takes a DictionaryHandle as input and returns a WritingRuleChecker.
     */
    protected static void register(String writingRuleName,
                                   Function<DictionaryHandle, WritingRuleChecker> creator) {
        Checks.isNotNull(writingRuleName, "writingRuleName");
        Checks.isNotNull(creator, "creator");

        if (MAP.containsKey(writingRuleName)) {
            throw new IllegalArgumentException("Duplicate rule name " + writingRuleName);
        }

        MAP.put(writingRuleName, creator);
    }

    public static WritingRuleChecker create(String writingRuleName,
                                            DictionaryHandle handle) {
        final Function<DictionaryHandle, WritingRuleChecker> creator = MAP.get(writingRuleName);
        if (creator == null) {
            throw new IllegalArgumentException("Unkown writing rule name '" + writingRuleName + "'");
        }

        return creator.apply(handle);
    }

    /**
     * Checks the compliance of a {@link CheckedData} the writing rules associated to a {@link Dictionary},
     * and adds detected issues to a list.
     *
     * @param handle The {@link DictionaryHandle}.
     * @param data The {@link CheckedData}.
     * @param issues The list of issues that must be filled.
     * @throws IllegalArgumentException When {@code handle}, {@code data} or {@code issues} is {@code null}.
     */
    public static void check(DictionaryHandle handle,
                             CheckedData data,
                             List<ApplicIssue> issues) {
        Checks.isNotNull(handle, HANDLE);
        Checks.isNotNull(data, DATA);
        Checks.isNotNull(issues, ISSUES);

        // Check syntax
        SyntaxChecker.INSTANCE.check(data, issues);
        if (issues.isEmpty()) {
            // Check semantic
            final SemanticChecker schecker = new SemanticChecker(handle.getDictionary());
            schecker.check(data, issues);
            if (issues.isEmpty()) {
                // Check writing rules
                for (final String writingRuleName : handle.getDictionary().getWritingRuleNames()) {
                    final WritingRuleChecker checker = create(writingRuleName, handle);
                    checker.check(data, issues);
                }
            }
        }
    }

    /**
     * Checks the compliance of an {@link Expression} the writing rules associated to a {@link Dictionary},
     * and adds detected issues to a list.
     *
     * @param handle The {@link DictionaryHandle}.
     * @param expression The {@link Expression}.
     * @param issues The list of issues that must be filled.
     * @throws IllegalArgumentException When {@code handle}, {@code expression} or {@code issues} is {@code null}.
     */
    public static void check(DictionaryHandle handle,
                             Expression expression,
                             List<ApplicIssue> issues) {
        Checks.isNotNull(handle, HANDLE);
        Checks.isNotNull(expression, EXPRESSION);
        Checks.isNotNull(issues, ISSUES);

        check(handle, new CheckedData(expression), issues);
    }

    /**
     * Checks the compliance of a {@link CheckedData} the writing rules associated to a {@link Dictionary},
     * and throws an exception if it is not the case.
     *
     * @param handle The {@link DictionaryHandle}.
     * @param data The {@link CheckedData}.
     * @throws IllegalArgumentException When {@code handle} or {@code data} is {@code null}.
     * @throws ApplicException When a compliance issue is detected.
     */
    public static void checkCompliance(DictionaryHandle handle,
                                       CheckedData data) {
        Checks.isNotNull(handle, HANDLE);
        Checks.isNotNull(data, DATA);

        // Check syntax
        SyntaxChecker.INSTANCE.checkCompliance(data);

        // Check semantic
        final SemanticChecker schecker = new SemanticChecker(handle.getDictionary());
        schecker.checkCompliance(data);

        // Check writing rules
        for (final String writingRuleName : handle.getDictionary().getWritingRuleNames()) {
            final WritingRuleChecker checker = create(writingRuleName, handle);
            checker.checkCompliance(data);
        }
    }

    /**
     * Checks the compliance of an {@link Expression} the writing rules associated to a {@link Dictionary},
     * and throws an exception if it is not the case.
     *
     * @param handle The {@link DictionaryHandle}.
     * @param expression The {@link Expression}.
     * @throws IllegalArgumentException When {@code handle} or {@code expression} is {@code null}.
     * @throws ApplicException When a compliance issue is detected.
     */
    public static void checkCompliance(DictionaryHandle handle,
                                       Expression expression) {
        Checks.isNotNull(handle, HANDLE);
        Checks.isNotNull(expression, EXPRESSION);

        checkCompliance(handle, new CheckedData(expression));
    }

    /**
     * Checks the compliance of a {@link CheckedData} the writing rules associated to a {@link Dictionary},
     * and builds a {@link Diagnosis} with detected issues.
     *
     * @param handle The {@link DictionaryHandle}.
     * @param data The {@link CheckedData}.
     * @return A {@link Diagnosis} with issues detected by this checker in {@code data}.
     * @throws IllegalArgumentException When {@code handle} or {@code data} is {@code null}.
     */
    public static Diagnosis<ApplicIssue> check(DictionaryHandle handle,
                                               CheckedData data) {
        Checks.isNotNull(handle, HANDLE);
        Checks.isNotNull(data, DATA);

        final List<ApplicIssue> issues = new ArrayList<>();
        check(handle, data, issues);
        return Diagnosis.of(issues);
    }

    /**
     * Checks compliance of an {@link Expression} with the writing rules associated to a {@link Dictionary},
     * and builds a {@link Diagnosis} with detected issues.
     *
     * @param handle The {@link DictionaryHandle}.
     * @param expression The {@link Expression}.
     * @return A {@link Diagnosis} with issues detected by this checker in {@code data}.
     * @throws IllegalArgumentException When {@code handle} or {@code expression} is {@code null}.
     */
    public static Diagnosis<ApplicIssue> check(DictionaryHandle handle,
                                               Expression expression) {
        Checks.isNotNull(handle, HANDLE);
        Checks.isNotNull(expression, EXPRESSION);

        return check(handle, new CheckedData(expression));
    }

    /**
     * Returns {@code true} if a {@link CheckedData} is compliant with the writing rules associated to a {@link Dictionary}.
     *
     * @param handle The {@link DictionaryHandle}.
     * @param data The {@link CheckedData}.
     * @return {@code true} if {@code data} is compliant with the writing rules
     *         associated to the dictionary of {@code handle}.
     * @throws IllegalArgumentException When {@code handle} or {@code data} is {@code null}.
     */
    public static boolean areCompliant(DictionaryHandle handle,
                                       CheckedData data) {
        Checks.isNotNull(handle, HANDLE);
        Checks.isNotNull(data, DATA);

        return check(handle, data).isOk();
    }

    /**
     * Returns {@code true} if an {@link Expression} is compliant with the writing rules associated to a {@link Dictionary}.
     *
     * @param handle The {@link DictionaryHandle}.
     * @param expression The {@link Expression}.
     * @return {@code true} if {@code expression} is compliant with the writing rules
     *         associated to the dictionary of {@code handle}.
     * @throws IllegalArgumentException When {@code handle} or {@code expression} is {@code null}.
     */
    public static boolean areCompliant(DictionaryHandle handle,
                                       Expression expression) {
        Checks.isNotNull(handle, HANDLE);
        Checks.isNotNull(expression, EXPRESSION);

        return areCompliant(handle, new CheckedData(expression));
    }
}