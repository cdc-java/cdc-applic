package cdc.applic.dictionaries.checks;

public enum WritingRulePurpose {
    /**
     * The intent is to reduce complexity.
     */
    COMPLEXITY,

    /**
     * The intent is to improve homogeneity.
     * <p>
     * The way of writing an expression should not depend on the person who wrote it.
     */
    HOMOGENEITY,

    /**
     * The intent is to avoid presence of expressions that define nothing or an non-acceptable thing.
     */
    SATISFIABILITY
}