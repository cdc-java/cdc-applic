package cdc.applic.dictionaries.checks;

import java.util.Objects;

import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.checks.ExpressionLocation;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;

/**
 * Issue dedicated to property values and sets.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-21", forRemoval = true)
public final class ContentIssue extends SemanticIssue {
    private final SItem item;
    private final SItemSet set;
    private final Type type;

    private ContentIssue(Builder builder) {
        super(builder);
        this.item = builder.item;
        this.set = builder.set;
        this.type = builder.type;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public ContentIssue(String context,
                        Node rootNode,
                        Node node,
                        String description,
                        SItem item,
                        Type type) {
        this(builder().location(new ExpressionLocation(context, rootNode))
                      .rootNode(rootNode)
                      .node(node)
                      .description(description)
                      .item(item)
                      .type(type));
        // super(context, rootNode, node, description);
        // this.item = item;
        // this.set = null;
        // this.type = type;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public ContentIssue(String context,
                        Node rootNode,
                        Node node,
                        String description,
                        SItemSet set,
                        Type type) {
        this(builder().location(new ExpressionLocation(context, rootNode))
                      .rootNode(rootNode)
                      .node(node)
                      .description(description)
                      .set(set)
                      .type(type));
        // super(context, node, root, description);
        // this.item = null;
        // this.set = set;
        // this.type = type;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public final SItem getItem() {
        return item;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public final SItemSet getSet() {
        return set;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public final Type getType() {
        return type;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode()
                + Objects.hash(item,
                               set,
                               type);
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        final ContentIssue other = (ContentIssue) object;
        return Objects.equals(item, other.item)
                && Objects.equals(set, other.set)
                && Objects.equals(type, other.type);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends SemanticIssue.Builder<Builder> {
        private SItem item;
        private SItemSet set;
        private Type type;

        protected Builder() {
        }

        public Builder item(SItem item) {
            this.item = item;
            return this;
        }

        public Builder set(SItemSet set) {
            this.set = set;
            return this;
        }

        public Builder type(Type type) {
            this.type = type;
            return this;
        }

        @Override
        public ContentIssue build() {
            return new ContentIssue(this);
        }
    }
}