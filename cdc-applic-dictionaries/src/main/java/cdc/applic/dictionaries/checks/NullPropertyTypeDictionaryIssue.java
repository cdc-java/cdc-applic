package cdc.applic.dictionaries.checks;

import java.util.Objects;
import java.util.Set;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.Property;
import cdc.util.lang.Checks;
import cdc.util.lang.CollectionUtils;

@Deprecated(since = "2024-12-21", forRemoval = true)
public final class NullPropertyTypeDictionaryIssue extends DictionaryIssue {
    private final Property property;

    private NullPropertyTypeDictionaryIssue(Builder builder) {
        super(builder.impliedItems(CollectionUtils.toSet(builder.property)));
        this.property = Checks.isNotNull(builder.property, "property");
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public NullPropertyTypeDictionaryIssue(Dictionary dictionary,
                                           String description,
                                           Property property) {
        this(builder().dictionary(dictionary)
                      .location(new DictionaryLocation(dictionary))
                      .impliedItems(Set.of(property))
                      .description(description)
                      .property(property));
        // super(dictionary,
        // CollectionUtils.toList(property),
        // description);
        // this.property = property;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public Property getProperty() {
        return property;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode()
                + Objects.hash(property);
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        final NullPropertyTypeDictionaryIssue other = (NullPropertyTypeDictionaryIssue) object;
        return Objects.equals(property, other.property);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends DictionaryIssue.Builder<Builder> {
        private Property property;

        protected Builder() {
        }

        public final Builder property(Property property) {
            this.property = property;
            return self();
        }

        @Override
        public NullPropertyTypeDictionaryIssue build() {
            return new NullPropertyTypeDictionaryIssue(this);
        }
    }
}