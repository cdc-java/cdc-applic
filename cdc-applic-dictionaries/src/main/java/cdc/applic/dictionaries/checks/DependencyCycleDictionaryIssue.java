package cdc.applic.dictionaries.checks;

import java.util.Objects;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.DItem;
import cdc.graphs.impl.BasicGraphEdge;
import cdc.graphs.impl.ExplicitSubGraph;
import cdc.util.lang.Checks;

/**
 * Used when a dependency cycle is detected in a dictionary.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-21", forRemoval = true)
public final class DependencyCycleDictionaryIssue extends DictionaryIssue {
    private final ExplicitSubGraph<DItem, BasicGraphEdge<DItem>> cycles;

    private DependencyCycleDictionaryIssue(Builder builder) {
        super(builder);
        this.cycles = Checks.isNotNull(builder.cycles, "cycles");
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public DependencyCycleDictionaryIssue(Dictionary dictionary,
                                          String description,
                                          ExplicitSubGraph<DItem, BasicGraphEdge<DItem>> cycles) {
        this(builder().dictionary(dictionary)
                      .location(new DictionaryLocation(dictionary))
                      .description(description)
                      .cycles(cycles)
                      .impliedItems(cycles.getNodes()));
        // super(dictionary,
        // cycles.getNodes(),
        // description);
        // this.cycles = cycles;
    }

    /**
     * @return The cycles.
     */
    @Deprecated(since = "2024-12-21", forRemoval = true)
    public ExplicitSubGraph<DItem, BasicGraphEdge<DItem>> getCycles() {
        return cycles;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode()
                + Objects.hash(cycles);
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        final DependencyCycleDictionaryIssue other = (DependencyCycleDictionaryIssue) object;
        return Objects.equals(cycles, other.cycles);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends DictionaryIssue.Builder<Builder> {
        private ExplicitSubGraph<DItem, BasicGraphEdge<DItem>> cycles;

        protected Builder() {
        }

        public final Builder cycles(ExplicitSubGraph<DItem, BasicGraphEdge<DItem>> cycles) {
            this.cycles = cycles;
            return self();
        }

        @Override
        public DependencyCycleDictionaryIssue build() {
            return new DependencyCycleDictionaryIssue(this);
        }
    }
}