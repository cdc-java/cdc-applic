package cdc.applic.dictionaries.checks;

import cdc.applic.dictionaries.Dictionary;
import cdc.issues.locations.AbstractLocation;
import cdc.util.lang.Checks;

/**
 * Location targeting a {@link Dictionary} or its content.
 *
 * @author Damien Carbonne
 */
public class DictionaryLocation extends AbstractLocation {
    private final Dictionary dictionary;

    public static final String TAG = "DictionaryLocation";

    public DictionaryLocation(Dictionary dictionary) {
        this.dictionary = Checks.isNotNull(dictionary, "dictionary");
    }

    public final Dictionary getDictionary() {
        return dictionary;
    }

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    public String getPath() {
        return dictionary.getName();
    }

    @Override
    public String getAnchor() {
        return null;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return super.equals(object);
    }
}