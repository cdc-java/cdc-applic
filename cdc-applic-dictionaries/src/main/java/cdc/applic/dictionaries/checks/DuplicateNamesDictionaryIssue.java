package cdc.applic.dictionaries.checks;

import java.util.Set;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.NamedDItem;

@Deprecated(since = "2024-12-21", forRemoval = true)
public final class DuplicateNamesDictionaryIssue extends DictionaryIssue {
    private DuplicateNamesDictionaryIssue(Builder builder) {
        super(builder);
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public DuplicateNamesDictionaryIssue(Dictionary dictionary,
                                         String description,
                                         Set<NamedDItem> items) {
        this(builder().dictionary(dictionary)
                      .location(new DictionaryLocation(dictionary))
                      .description(description)
                      .impliedItems(items));
        // super(dictionary, items, description);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends DictionaryIssue.Builder<Builder> {
        protected Builder() {
        }

        @Override
        public DuplicateNamesDictionaryIssue build() {
            return new DuplicateNamesDictionaryIssue(this);
        }
    }
}