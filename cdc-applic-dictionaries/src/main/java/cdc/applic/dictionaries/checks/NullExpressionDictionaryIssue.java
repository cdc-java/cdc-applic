package cdc.applic.dictionaries.checks;

import java.util.Objects;
import java.util.Set;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.DExpressed;
import cdc.util.lang.Checks;

@Deprecated(since = "2024-12-21", forRemoval = true)
public final class NullExpressionDictionaryIssue extends DictionaryIssue {
    private final DExpressed expressed;

    private NullExpressionDictionaryIssue(Builder builder) {
        super(builder);
        this.expressed = Checks.isNotNull(builder.expressed, "expressed");
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public NullExpressionDictionaryIssue(Dictionary dictionary,
                                         String description,
                                         DExpressed expressed) {
        this(builder().dictionary(dictionary)
                      .location(new DictionaryLocation(dictionary))
                      .impliedItems(Set.of(expressed))
                      .description(description)
                      .expressed(expressed));
        // super(dictionary,
        // CollectionUtils.toList(expressed),
        // description);
        // this.expressed = expressed;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public DExpressed getPExpressed() {
        return expressed;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode()
                + Objects.hash(expressed);
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        final NullExpressionDictionaryIssue other = (NullExpressionDictionaryIssue) object;
        return Objects.equals(expressed, other.expressed);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends DictionaryIssue.Builder<Builder> {
        private DExpressed expressed;

        protected Builder() {
        }

        public final Builder expressed(DExpressed expressed) {
            this.expressed = expressed;
            return self();
        }

        @Override
        public NullExpressionDictionaryIssue build() {
            return new NullExpressionDictionaryIssue(this);
        }
    }
}