package cdc.applic.dictionaries.checks;

import java.util.Objects;

import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.checks.ExpressionLocation;
import cdc.util.lang.Checks;

/**
 * Issue dedicated to improper use of inequalities with types that don't support them.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-21", forRemoval = true)
public final class InequalityIssue extends SemanticIssue {
    private final Type type;

    private InequalityIssue(Builder builder) {
        super(builder);
        this.type = Checks.isNotNull(builder.type, "type");
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public InequalityIssue(String context,
                           Node rootNode,
                           Node node,
                           String description,
                           Type type) {
        this(builder().location(new ExpressionLocation(context, rootNode))
                      .rootNode(rootNode)
                      .node(node)
                      .description(description)
                      .type(type));
        // super(context,
        // rootNode,
        // node,
        // description);
        // this.type = type;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public final Type getType() {
        return type;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode()
                + Objects.hash(type);
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        final InequalityIssue other = (InequalityIssue) object;
        return Objects.equals(type, other.type);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends SemanticIssue.Builder<Builder> {
        private Type type;

        protected Builder() {
        }

        public Builder type(Type type) {
            this.type = type;
            return self();
        }

        @Override
        public InequalityIssue build() {
            return new InequalityIssue(this);
        }
    }
}