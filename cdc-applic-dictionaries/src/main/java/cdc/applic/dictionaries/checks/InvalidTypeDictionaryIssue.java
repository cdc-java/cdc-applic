package cdc.applic.dictionaries.checks;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.Type;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;

@Deprecated(since = "2024-12-21", forRemoval = true)
public final class InvalidTypeDictionaryIssue extends DictionaryIssue {
    private final Type type;

    private InvalidTypeDictionaryIssue(Builder builder) {
        super(builder.impliedItems(IterableUtils.toSet(getImpliedItems(builder.dictionary, builder.type))));
        this.type = Checks.isNotNull(builder.type, "type");
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public InvalidTypeDictionaryIssue(Dictionary dictionary,
                                      String description,
                                      Type type) {
        this(builder().dictionary(dictionary)
                      .location(new DictionaryLocation(dictionary))
                      .description(description)
                      .impliedItems(getImpliedItems(dictionary, type))
                      .type(type));
        // super(dictionary,
        // getImpliedItems(dictionary, type),
        // description);
        // this.type = type;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public Type getType() {
        return type;
    }

    private static Set<Property> getImpliedItems(Dictionary dictionary,
                                                 Type type) {
        final Set<Property> set = new HashSet<>();
        for (final Property property : dictionary.getRegistry().getDeclaredProperties()) {
            if (property.getType() == type) {
                set.add(property);
            }
        }
        return set;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode()
                + Objects.hash(type);
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        final InvalidTypeDictionaryIssue other = (InvalidTypeDictionaryIssue) object;
        return Objects.equals(type, other.type);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends DictionaryIssue.Builder<Builder> {
        private Type type;

        protected Builder() {
        }

        public final Builder type(Type type) {
            this.type = type;
            return self();
        }

        @Override
        public InvalidTypeDictionaryIssue build() {
            return new InvalidTypeDictionaryIssue(this);
        }
    }
}