package cdc.applic.dictionaries.checks;

import java.util.List;
import java.util.Objects;

import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.ApplicIssueType;
import cdc.issues.locations.Location;
import cdc.util.lang.Checks;

@Deprecated(since = "2024-12-21", forRemoval = true)
public class WritingRuleIssue extends ApplicIssue {
    private final String ruleName;

    protected WritingRuleIssue(Builder<?> builder) {
        super(builder.name(ApplicIssueType.WRITING_ISSUE));
        this.ruleName = Checks.isNotNull(builder.ruleName, "ruleName");
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public WritingRuleIssue(String ruleName,
                            List<? extends Location> locations,
                            String description) {
        this(builder().ruleName(ruleName)
                      .locations(List.copyOf(locations)) // FIXME remove copyOf when cdc-issue is fixed
                      .description(description));
        // super(ApplicIssueType.WRITING_ISSUE,
        // null,
        // locations,
        // description);
        // this.ruleName = ruleName;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public WritingRuleIssue(String ruleName,
                            Location location,
                            String description) {
        this(builder().ruleName(ruleName)
                      .location(location)
                      .description(description));
        // super(ApplicIssueType.WRITING_ISSUE,
        // null,
        // location,
        // description);
        // this.ruleName = ruleName;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public final String getRuleName() {
        return ruleName;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode()
                + Objects.hash(ruleName);
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        final WritingRuleIssue other = (WritingRuleIssue) object;
        return Objects.equals(ruleName, other.ruleName);
    }

    public static Builder<?> builder() {
        return new Builder<>();
    }

    public static class Builder<B extends Builder<B>> extends ApplicIssue.Builder<B> {
        private String ruleName;

        protected Builder() {
        }

        public final B ruleName(String ruleName) {
            this.ruleName = ruleName;
            return self();
        }

        @Override
        public WritingRuleIssue build() {
            return new WritingRuleIssue(this);
        }
    }
}