package cdc.applic.dictionaries.checks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.DItem;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.ApplicIssueType;
import cdc.issues.locations.Location;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;

/**
 * Base class of Dictionary issues.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-21", forRemoval = true)
public abstract class DictionaryIssue extends ApplicIssue {
    private final Dictionary dictionary;
    private final Set<DItem> impliedItems;

    private static Builder<?> fix(Builder<?> builder) {
        if (!builder.impliedItems.isEmpty()) {
            builder.locations(toIssueLocations(builder.dictionary, builder.impliedItems));
        }
        return builder;
    }

    protected DictionaryIssue(Builder<?> builder) {
        super(fix(builder.name(ApplicIssueType.DICTIONARY_ISSUE)));
        this.dictionary = Checks.isNotNull(builder.dictionary, "dictionary");
        this.impliedItems = Checks.isNotNull(builder.impliedItems, "implieditems");
    }

    // protected DictionaryIssue(Dictionary dictionary,
    // Iterable<? extends DItem> impliedItems,
    // String description) {
    // super(ApplicIssueType.DICTIONARY_ISSUE,
    // null,
    // toIssueLocations(dictionary, impliedItems),
    // description);
    //
    // this.dictionary = dictionary;
    // this.impliedItems = IterableUtils.toSet(impliedItems);
    // }
    //
    // protected DictionaryIssue(Dictionary dictionary,
    // Location location,
    // String description) {
    // super(ApplicIssueType.DICTIONARY_ISSUE,
    // null,
    // location,
    // description);
    // this.dictionary = dictionary;
    // this.impliedItems = Collections.emptySet();
    // }

    private static List<Location> toIssueLocations(Dictionary dictionary,
                                                   Iterable<? extends DItem> items) {
        Checks.isNotNull(dictionary, "dictionary");
        Checks.isNotNull(items, "items");
        final List<Location> list = new ArrayList<>();
        for (final DItem item : items) {
            list.add(new DItemLocation(dictionary, item));
        }

        return list;
    }

    /**
     * @return The dictionary the issue is related to.
     */
    @Deprecated(since = "2024-12-21", forRemoval = true)
    public final Dictionary getDictionary() {
        return dictionary;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public Iterable<? extends DItem> getImpliedItems() {
        return impliedItems;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode()
                + Objects.hash(dictionary,
                               impliedItems);
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        final DictionaryIssue other = (DictionaryIssue) object;
        return Objects.equals(dictionary, other.dictionary)
                && Objects.equals(impliedItems, other.impliedItems);
    }

    public static class Builder<B extends Builder<B>> extends ApplicIssue.Builder<B> {
        protected Dictionary dictionary;
        private Set<DItem> impliedItems = Collections.emptySet();

        protected Builder() {
        }

        public final B dictionary(Dictionary dictionary) {
            this.dictionary = dictionary;
            return self();
        }

        public final B impliedItems(Iterable<? extends DItem> impliedItems) {
            this.impliedItems = IterableUtils.toSet(impliedItems);
            return self();
        }
    }
}