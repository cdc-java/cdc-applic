package cdc.applic.dictionaries.checks;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.types.Type;
import cdc.util.lang.Checks;

/**
 * Location targeting a {@link Type} inside a {@link Dictionary}.
 *
 * @author Damien Carbonne
 */
public class TypeLocation extends DictionaryLocation {
    private final Type type;

    public TypeLocation(Dictionary dictionary,
                        Type type) {
        super(dictionary);
        this.type = Checks.isNotNull(type, "type");
    }

    public Type getType() {
        return type;
    }

    @Override
    public String getAnchor() {
        return "Type:" + type.getName();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return super.equals(object);
    }
}