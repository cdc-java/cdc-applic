package cdc.applic.dictionaries.checks;

import java.util.Objects;

import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

@Deprecated(since = "2024-12-21", forRemoval = true)
public abstract class IdentifierIssue extends SemanticIssue {
    private final Name identifierName;

    protected IdentifierIssue(Builder<?> builder) {
        super(builder);
        this.identifierName = Checks.isNotNull(builder.identifierName, "identifierName");
    }

    // protected IdentifierIssue(String context,
    // Node rootNode,
    // Node node,
    // String description,
    // Name name) {
    // super(context,
    // rootNode,
    // node,
    // description);
    // this.name = name;
    // }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public final Name getIdentifierName() {
        return identifierName;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode()
                + Objects.hash(identifierName);
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        final IdentifierIssue other = (IdentifierIssue) object;
        return Objects.equals(identifierName, other.identifierName);
    }

    public static class Builder<B extends Builder<B>> extends SemanticIssue.Builder<B> {
        private Name identifierName;

        protected Builder() {
        }

        public final B identifierName(Name identifierName) {
            this.identifierName = identifierName;
            return self();
        }

        // @Override
        // public IdentifierIssue build() {
        // return new IdentifierIssue(this);
        // }
    }
}