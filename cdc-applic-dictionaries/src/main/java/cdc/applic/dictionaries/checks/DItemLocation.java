package cdc.applic.dictionaries.checks;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.Assertion;
import cdc.applic.dictionaries.items.DItem;
import cdc.applic.dictionaries.items.Property;
import cdc.util.lang.Checks;

/**
 * Location targeting a {@link DItem} inside a {@link Dictionary}.
 *
 * @author Damien Carbonne
 */
public class DItemLocation extends DictionaryLocation {
    private final DItem item;

    public DItemLocation(Dictionary dictionary,
                         DItem item) {
        super(dictionary);
        this.item = Checks.isNotNull(item, "item");
    }

    public DItem getItem() {
        return item;
    }

    @Override
    public String getAnchor() {
        if (item instanceof final Alias i) {
            return "Alias:" + i.getName();
        } else if (item instanceof final Property i) {
            return "Property:" + i.getName();
        } else {
            return "Assertion:" + ((Assertion) item).getExpression();
        }
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return super.equals(object);
    }
}