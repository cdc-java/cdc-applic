package cdc.applic.dictionaries.checks;

import java.util.Objects;

import cdc.applic.dictionaries.SemanticException;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.ApplicIssueType;
import cdc.applic.expressions.checks.ExpressionLocation;
import cdc.util.lang.Checks;

/**
 * Specialization of Issue dedicated to Semantic problems.
 * <p>
 * They are associated to a {@link SemanticException}.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-21", forRemoval = true)
public class SemanticIssue extends ApplicIssue {
    private final Node rootNode;
    private final Node node;

    protected SemanticIssue(Builder<?> builder) {
        super(builder.name(ApplicIssueType.SEMANTIC_ISSUE));
        this.rootNode = Checks.isNotNull(builder.rootNode, "rootNode");
        this.node = Checks.isNotNull(builder.node, "node");
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public SemanticIssue(String context,
                         Node rootNode,
                         Node node,
                         String description) {
        this(builder().location(new ExpressionLocation(context, rootNode))
                      .rootNode(rootNode)
                      .node(node)
                      .description(description));
        // super(ApplicIssueType.SEMANTIC_ISSUE,
        // null,
        // new ExpressionLocation(context, rootNode),
        // description);
        // this.rootNode = rootNode;
        // this.node = node;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public final Node getRootNode() {
        return rootNode;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public final Node getNode() {
        return node;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode()
                + Objects.hash(rootNode,
                               node);
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        final SemanticIssue other = (SemanticIssue) object;
        return Objects.equals(rootNode, other.rootNode)
                && Objects.equals(node, other.node);
    }

    public static Builder<?> builder() {
        return new Builder<>();
    }

    public static class Builder<B extends Builder<B>> extends ApplicIssue.Builder<B> {
        private Node rootNode;
        private Node node;

        protected Builder() {
        }

        public final B rootNode(Node rootNode) {
            this.rootNode = rootNode;
            return self();
        }

        public final B node(Node node) {
            this.node = node;
            return self();
        }

        @Override
        public SemanticIssue build() {
            return new SemanticIssue(this);
        }
    }
}