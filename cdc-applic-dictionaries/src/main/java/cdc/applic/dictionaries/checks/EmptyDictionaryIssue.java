package cdc.applic.dictionaries.checks;

import java.util.Collections;
import java.util.Set;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.DItem;

@Deprecated(since = "2024-12-21", forRemoval = true)
public final class EmptyDictionaryIssue extends DictionaryIssue {
    private EmptyDictionaryIssue(Builder builder) {
        super(builder);
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public EmptyDictionaryIssue(Dictionary dictionary,
                                String description) {
        this(builder().dictionary(dictionary)
                      .location(new DictionaryLocation(dictionary))
                      .description(description));
        // super(dictionary,
        // new DictionaryLocation(dictionary),
        // description);
    }

    @Override
    @Deprecated(since = "2024-12-21", forRemoval = true)
    public Set<DItem> getImpliedItems() {
        return Collections.emptySet();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends DictionaryIssue.Builder<Builder> {
        protected Builder() {
        }

        @Override
        public EmptyDictionaryIssue build() {
            return new EmptyDictionaryIssue(this);
        }
    }
}