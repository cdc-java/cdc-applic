package cdc.applic.dictionaries.checks;

import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.checks.ExpressionLocation;
import cdc.applic.expressions.literals.Name;

@Deprecated(since = "2024-12-21", forRemoval = true)
public final class UnknownIdentifierIssue extends IdentifierIssue {
    private UnknownIdentifierIssue(Builder builder) {
        super(builder);
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public UnknownIdentifierIssue(String context,
                                  Node rootNode,
                                  Node node,
                                  String description,
                                  Name identifierName) {
        this(builder().location(new ExpressionLocation(context, rootNode))
                      .rootNode(rootNode)
                      .node(node)
                      .description(description)
                      .identifierName(identifierName));
        // super(context,
        // rootNode,
        // node,
        // description,
        // name);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends IdentifierIssue.Builder<Builder> {
        protected Builder() {
        }

        @Override
        public UnknownIdentifierIssue build() {
            return new UnknownIdentifierIssue(this);
        }
    }
}