package cdc.applic.dictionaries.checks;

import java.util.Objects;
import java.util.Set;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.DExpressed;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.issues.Diagnosis;
import cdc.issues.HasDiagnosis;
import cdc.util.lang.Checks;

@Deprecated(since = "2024-12-21", forRemoval = true)
public final class InvalidExpressionDictionaryIssue extends DictionaryIssue implements HasDiagnosis<ApplicIssue> {
    private final DExpressed expressed;
    private final Diagnosis<ApplicIssue> diagnosis;

    private InvalidExpressionDictionaryIssue(Builder builder) {
        super(builder);
        this.expressed = Checks.isNotNull(builder.expressed, "expressed");
        this.diagnosis = Checks.isNotNull(builder.diagnosis, "diagnosis");
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public InvalidExpressionDictionaryIssue(Dictionary dictionary,
                                            String description,
                                            DExpressed expressed,
                                            Diagnosis<ApplicIssue> diagnosis) {
        this(builder().dictionary(dictionary)
                      .location(new DictionaryLocation(dictionary))
                      .impliedItems(Set.of(expressed))
                      .description(description)
                      .expressed(expressed)
                      .diagnosis(diagnosis));
        // super(dictionary,
        // CollectionUtils.toList(expressed),
        // description);
        // this.expressed = expressed;
        // this.diagnosis = diagnosis;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public DExpressed getPExpressed() {
        return expressed;
    }

    @Override
    @Deprecated(since = "2024-12-21", forRemoval = true)
    public Diagnosis<ApplicIssue> getDiagnosis() {
        return diagnosis;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode()
                + Objects.hash(expressed,
                               diagnosis);
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        final InvalidExpressionDictionaryIssue other = (InvalidExpressionDictionaryIssue) object;
        return Objects.equals(expressed, other.expressed)
                && Objects.equals(diagnosis, other.diagnosis);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends DictionaryIssue.Builder<Builder> {
        private DExpressed expressed;
        private Diagnosis<ApplicIssue> diagnosis;

        protected Builder() {
        }

        public final Builder expressed(DExpressed expressed) {
            this.expressed = expressed;
            return self();
        }

        public final Builder diagnosis(Diagnosis<ApplicIssue> diagnosis) {
            this.diagnosis = diagnosis;
            return self();
        }

        @Override
        public InvalidExpressionDictionaryIssue build() {
            return new InvalidExpressionDictionaryIssue(this);
        }
    }
}