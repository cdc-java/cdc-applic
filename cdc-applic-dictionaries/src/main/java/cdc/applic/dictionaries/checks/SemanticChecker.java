package cdc.applic.dictionaries.checks;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.DictionaryUtils;
import cdc.applic.dictionaries.SemanticException;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.PartiallyOrderedType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.SyntacticException;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractPropertyNode;
import cdc.applic.expressions.ast.AbstractSetNode;
import cdc.applic.expressions.ast.AbstractValueNode;
import cdc.applic.expressions.ast.InequalityNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.visitors.AbstractCollector;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.Checker;
import cdc.applic.expressions.checks.SyntaxChecker;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.issues.Diagnosis;
import cdc.util.lang.Checks;

/**
 * A {@link Checker} that checks semantic of an {@link Expression}.
 * <p>
 * The expression must be syntactically valid to pass this check.
 *
 * @author Damien Carbonne
 */
public class SemanticChecker implements Checker {
    public static final String RULE_NAME = "Semantic correctness";
    private final Dictionary dictionary;

    public SemanticChecker(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }

    @Override
    public boolean isCompliant(CheckedData data) {
        Checks.isNotNull(data, "data");

        final List<ApplicIssue> issues = new ArrayList<>();
        Collector.collect(data.getContext(),
                          data.getNode(),
                          dictionary,
                          issues);
        return issues.isEmpty();
    }

    @Override
    public void check(CheckedData data,
                      List<ApplicIssue> issues) {
        Checks.isNotNull(data, "data");
        Checks.isNotNull(issues, "issues");

        Collector.collect(data.getContext(),
                          data.getNode(),
                          dictionary,
                          issues);
    }

    /**
     * Checks the semantic compliance of an expression and throws an exception in case or non compliance.
     *
     * @param data The data to check.
     * @throws SyntacticException When data syntax is not compliant.
     * @throws SemanticException When data semantic is not compliant.
     */
    @Override
    public void checkCompliance(CheckedData data) {
        final List<ApplicIssue> issues = new ArrayList<>();
        Collector.collect(data.getContext(),
                          data.getNode(),
                          dictionary,
                          issues);
        if (!issues.isEmpty()) {
            final Diagnosis<ApplicIssue> diagnosis = Diagnosis.of(issues);
            throw new SemanticException("Semantic issue: " + diagnosis.getIssues(), diagnosis);
        }
    }

    private static final class Collector extends AbstractCollector<ApplicIssue> {
        private final String context;
        private final Node root;
        private final Dictionary dictionary;

        private Collector(String context,
                          Node root,
                          Dictionary dictionary,
                          List<ApplicIssue> issues) {
            super(issues);
            this.context = context;
            this.root = root;
            this.dictionary = dictionary;
        }

        public static void collect(String context,
                                   Node node,
                                   Dictionary dictionary,
                                   List<ApplicIssue> issues) {
            final Collector collector = new Collector(context,
                                                      node,
                                                      dictionary,
                                                      issues);
            node.accept(collector);
        }

        /**
         * Checks semantic of a Property.
         * <uL>
         * <li>Name exists
         * <li>Value is compliant with property type
         * <li>Set is compliant with property type
         * <li>Inequalities are used with compliant types
         * </ul>
         *
         * @param node The AbstractPropertyNode.
         */
        private void checkProperty(AbstractPropertyNode node) {
            // Check name of the property
            final Name name = node.getName();
            if (!dictionary.getRegistry().hasProperty(name)) {
                add(new UnknownIdentifierIssue(context,
                                               root,
                                               node,
                                               "Unknown property " + Checker.wrap(name), name));
            } else {
                // This property is declared
                // Check its content
                if (node instanceof final AbstractValueNode n) {
                    checkValue(n);
                    if (node instanceof final InequalityNode in) {
                        checkInequality(in);
                    }
                } else {
                    checkSet((AbstractSetNode) node);
                }
                // Check its usage
                checkDItemUsage(node, name);
            }
        }

        /**
         * Checks semantic of a Ref.
         *
         * @param node The RefNode.
         */
        private void checkRef(RefNode node) {
            // Check name of the ref
            final Name name = node.getName();
            if (dictionary.getRegistry().hasAlias(name)) {
                // This is a declared Alias
                // Check its usage
                checkDItemUsage(node, name);
            } else if (dictionary.getRegistry().hasProperty(name)) {
                // This is a declared property
                if (!DictionaryUtils.isAvailableBooleanProperty(name, dictionary)) {
                    add(new NonUsableIdentifierIssue(context,
                                                     root,
                                                     node,
                                                     "Non boolean property " + Checker.wrap(name), name));
                } else {
                    // This is a boolean property
                    // Check its usage
                    checkDItemUsage(node, name);
                }
            } else {
                // This is an unknown item (Alias or Property)
                add(new UnknownIdentifierIssue(context,
                                               root,
                                               node,
                                               "Unknown item " + Checker.wrap(name), name));
            }
        }

        /**
         * Checks the compliance of a value with the node type.
         *
         * @param node The node.
         */
        private void checkValue(AbstractValueNode node) {
            final Property property = dictionary.getRegistry().getProperty(node.getName());
            final Type type = property.getType();
            final Value value = node.getValue();

            if (!type.isCompliant(value)) {
                add(new ContentIssue(context,
                                     root,
                                     node,
                                     "Value " + Checker.wrap(value) + " not compliant with type "
                                             + Checker.wrap(type.getName()) + " (" + type.getDefinition() + ")",
                                     value,
                                     type));
            }
        }

        /**
         * Checks that use of inequality is possible with the property type.
         *
         * @param node The node.
         */
        private void checkInequality(InequalityNode node) {
            final Property property = dictionary.getRegistry().getProperty(node.getName());
            final Type type = property.getType();
            if (!(type instanceof PartiallyOrderedType)) {
                add(new InequalityIssue(context,
                                        root,
                                        node,
                                        "Inequality cannot be used with this type "
                                                + Checker.wrap(type.getName()) + " (" + type.getDefinition() + ")",
                                        type));
            }
        }

        /**
         * Checks the compliance of a set with the node type.
         *
         * @param node The node.
         */
        private void checkSet(AbstractSetNode node) {
            final Property property = dictionary.getRegistry().getProperty(node.getName());
            final Type type = property.getType();
            final SItemSet set = node.getSet();

            if (!type.isCompliant(set)) {
                add(new ContentIssue(context,
                                     root,
                                     node,
                                     "Set " + Checker.wrap(set) + " not compliant with type "
                                             + Checker.wrap(type.getName()) + " (" + type.getDefinition() + ")",
                                     set,
                                     type));
            }
        }

        /**
         * Check that a DItem usage is possible.
         *
         * @param node The node.
         * @param name The name.
         */
        private void checkDItemUsage(Node node,
                                     Name name) {
            final DItemUsage usage = dictionary.getEffectiveItemUsage(name);
            if (usage == DItemUsage.FORBIDDEN) {
                final boolean isAlias = dictionary.getRegistry().hasAlias(name);
                add(new DItemUsageIssue(context,
                                        root,
                                        node,
                                        (isAlias ? "Alias " : "Property ") + Checker.wrap(name) + " is not allowed by "
                                                + Checker.wrap(dictionary.getName()),
                                        usage));
            }
        }

        @Override
        public Void visitLeaf(AbstractLeafNode node) {
            if (node instanceof final AbstractPropertyNode n) {
                checkProperty(n);
            } else if (node instanceof final RefNode n) {
                checkRef(n);
            }
            return null;
        }
    }

    public static void check(Dictionary dictionary,
                             CheckedData data,
                             List<ApplicIssue> issues) {
        Checks.isNotNull(dictionary, "dictionary");

        // Check syntax
        SyntaxChecker.INSTANCE.check(data, issues);

        if (issues.isEmpty()) {
            // Check semantic
            final SemanticChecker schecker = new SemanticChecker(dictionary);
            schecker.check(data, issues);
        }
    }

    /**
     * Checks the syntactic and semantic compliance of a data with a dictionary.
     *
     * @param dictionary The dictionary.
     * @param data The data.
     * @throws IllegalArgumentException When {@code dictionary} or {@code data} is {@code null}.
     * @throws SyntacticException When data syntax is not compliant.
     * @throws SemanticException When data semantic is not compliant.
     */
    public static void checkCompliance(Dictionary dictionary,
                                       CheckedData data) {
        Checks.isNotNull(dictionary, "dictionary");
        Checks.isNotNull(data, "data");

        // Check syntax
        SyntaxChecker.INSTANCE.checkCompliance(data);
        // Check semantic
        final SemanticChecker schecker = new SemanticChecker(dictionary);
        schecker.checkCompliance(data);
    }

    /**
     * Checks the syntactic and semantic compliance of an expression with a dictionary.
     *
     * @param dictionary The dictionary.
     * @param expression The expression.
     * @throws IllegalArgumentException When {@code dictionary} or {@code expression} is {@code null}.
     * @throws SyntacticException When expression syntax is not compliant.
     * @throws SemanticException When expression semantic is not compliant.
     */
    public static void checkCompliance(Dictionary dictionary,
                                       Expression expression) {
        checkCompliance(dictionary, new CheckedData(expression));
    }

    /**
     * Checks the semantic compliance of a {@link Node} with a {@link Dictionary}.
     *
     * @param dictionary The dictionary.
     * @param node The node.
     * @throws IllegalArgumentException When {@code dictionary} or {@code node} is {@code null}.
     * @throws SemanticException When node semantic is not compliant.
     */
    public static void checkCompliance(Dictionary dictionary,
                                       Node node) {
        checkCompliance(dictionary, new CheckedData(node));
    }

    public static Diagnosis<ApplicIssue> check(Dictionary dictionary,
                                               CheckedData data) {
        final List<ApplicIssue> issues = new ArrayList<>();
        check(dictionary, data, issues);
        return Diagnosis.of(issues);
    }

    public static void check(Dictionary dictionary,
                             Expression expression,
                             List<ApplicIssue> issues) {
        check(dictionary, new CheckedData(expression), issues);
    }

    public static Diagnosis<ApplicIssue> check(Dictionary dictionary,
                                               Expression expression) {
        return check(dictionary, new CheckedData(expression));
    }

    public static Diagnosis<ApplicIssue> check(Dictionary dictionary,
                                               Node node) {
        return check(dictionary, new CheckedData(node));
    }

    public static boolean areCompliant(Dictionary dictionary,
                                       CheckedData data) {
        return check(dictionary, data).isOk();
    }

    public static boolean areCompliant(Dictionary dictionary,
                                       Expression expression) {
        return areCompliant(dictionary, new CheckedData(expression));
    }
}