package cdc.applic.dictionaries.checks;

import java.util.Objects;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.checks.ExpressionLocation;
import cdc.util.lang.Checks;

@Deprecated(since = "2024-12-21", forRemoval = true)
public final class DItemUsageIssue extends SemanticIssue {
    private final DItemUsage usage;

    private DItemUsageIssue(Builder builder) {
        super(builder);
        this.usage = Checks.isNotNull(builder.usage, "usage");
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public DItemUsageIssue(String context,
                           Node rootNode,
                           Node node,
                           String description,
                           DItemUsage usage) {
        this(builder().location(new ExpressionLocation(context, rootNode))
                      .rootNode(rootNode)
                      .node(node)
                      .description(description)
                      .usage(usage));
        // super(context,
        // rootNode,
        // node,
        // description);
        // this.usage = usage;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public DItemUsage getUsage() {
        return usage;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode()
                + Objects.hash(usage);
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        final DItemUsageIssue other = (DItemUsageIssue) object;
        return Objects.equals(usage, other.usage);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends SemanticIssue.Builder<Builder> {
        private DItemUsage usage;

        private Builder() {
        }

        public Builder usage(DItemUsage usage) {
            this.usage = usage;
            return this;
        }

        @Override
        public DItemUsageIssue build() {
            return new DItemUsageIssue(this);
        }
    }
}