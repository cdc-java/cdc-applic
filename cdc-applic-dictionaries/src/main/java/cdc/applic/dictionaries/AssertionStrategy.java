package cdc.applic.dictionaries;

/**
 * Enumeration of possible strategies in handling of assertions.
 *
 * @author Damien Carbonne
 */
public enum AssertionStrategy {
    /**
     * Assertions are ignored.
     */
    EXCLUDE_ASSERTIONS,

    /**
     * Assertions are taken into account.
     */
    INCLUDE_ASSERTIONS
}