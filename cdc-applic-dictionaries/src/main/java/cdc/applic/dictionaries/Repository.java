package cdc.applic.dictionaries;

import java.util.Collection;
import java.util.List;

import cdc.applic.dictionaries.bindings.DictionariesBinding;
import cdc.util.lang.NotFoundException;
import cdc.util.paths.Path;

/**
 * Base interface of a repository containing dictionaries, bindings and related items.
 *
 * @author Damien Carbonne
 */
public interface Repository extends DElement, Described {

    /**
     * Returns the {@link Dictionary} at a {@link Path}.
     *
     * @param <D> The dictionary type.
     * @param path The dictionary path.
     * @param cls The dictionary class.
     * @return The dictionary corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} or {@code cls} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     * @throws ClassCastException When a dictionary is found, but with a non compliant class.
     */
    public <D extends Dictionary> D getDictionary(Path path,
                                                  Class<D> cls);

    /**
     * Returns the {@link Dictionary} at a {@link Path}.
     *
     * @param <D> The dictionary type.
     * @param path The dictionary path.
     * @param cls The dictionary class.
     * @return The dictionary corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} or {@code cls} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     * @throws ClassCastException When a dictionary is found, but with a non compliant class.
     */
    public <D extends Dictionary> D getDictionary(String path,
                                                  Class<D> cls);

    /**
     * Returns the {@link Dictionary} at a {@link Path}.
     *
     * @param path The dictionary path.
     * @return The dictionary corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     */
    public Dictionary getDictionary(Path path);

    /**
     * Returns the {@link Dictionary} at a {@link Path}.
     *
     * @param path The dictionary path.
     * @return The dictionary corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     */
    public Dictionary getDictionary(String path);

    /**
     * Returns the {@link Registry} at a {@link Path}.
     *
     * @param path The path.
     * @return The registry corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     * @throws ClassCastException When a dictionary is found, but is not a registry.
     */
    public Registry getRegistry(Path path);

    /**
     * Returns the {@link Registry} at a {@link Path}.
     *
     * @param path The path.
     * @return The registry corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     * @throws ClassCastException When a dictionary is found, but is not a registry.
     */
    public Registry getRegistry(String path);

    /**
     * Returns the {@link Policy} at a {@link Path}.
     *
     * @param path The path.
     * @return The policy corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     * @throws ClassCastException When a dictionary is found, but is not a registry.
     */
    public Policy getPolicy(Path path);

    /**
     * Returns the {@link Policy} at a {@link Path}.
     *
     * @param path The path.
     * @return The policy corresponding to {@code path}.
     * @throws IllegalArgumentException When {@code path} is {@code null}.
     * @throws NotFoundException When no matching dictionary is found.
     * @throws ClassCastException When a dictionary is found, but is not a registry.
     */
    public Policy getPolicy(String path);

    /**
     * @return A collection of all declared dictionaries.
     */
    public Collection<? extends Dictionary> getDictionaries();

    /**
     * @return A sorted list of all declared dictionaries.<br>
     */
    public List<? extends Dictionary> getSortedDictionaries();

    /**
     * @return A list of all declared registries (in order of declaration).
     */
    public List<? extends Registry> getRegistries();

    /**
     * @return A collection of all declared policies.
     */
    public Collection<? extends Policy> getPolicies();

    /**
     * @return A list of all declared dictionaries bindings.
     */
    public List<? extends DictionariesBinding> getBindings();
}