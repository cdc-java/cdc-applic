package cdc.applic.dictionaries;

import java.util.Optional;

import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;

/**
 * Interface dedicated to <em>locally declared</em> dictionary {@link NamedDItem Items}.
 *
 * @author Damien Carbonne
 */
public interface DictionaryDeclaredItems {
    /**
     * @return An {@link Iterable} of all {@link NamedDItem Items} that are
     *         <em>locally declared</em> in this {@link Dictionary}.
     */
    public Iterable<NamedDItem> getDeclaredItems();

    /**
     * @return An {@link Iterable} of all {@link Property Properties} that are
     *         <em>locally declared</em> in this {@link Dictionary}.
     */
    public Iterable<? extends Property> getDeclaredProperties();

    /**
     * @return An {@link Iterable} of all {@link Alias Aliases} that are
     *         <em>locally declared</em> in this {@link Dictionary}.
     */
    public Iterable<? extends Alias> getDeclaredAliases();

    /**
     * @param name The <em>prefixed</em> or <em>local</em> name.
     * @return An {@link Optional} of the <em>locally declared</em> {@link NamedDItem Item}
     *         whose <em>prefixed</em> or <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     */
    public Optional<NamedDItem> getOptionalDeclaredItem(Name name);

    /**
     * @param name The <em>local</em> name.
     * @return An {@link Optional} of the <em>locally declared</em> {@link NamedDItem Item}
     *         whose <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     */
    public default Optional<NamedDItem> getOptionalDeclaredItem(SName name) {
        return getOptionalDeclaredItem(Name.of(name));
    }

    /**
     * @param name The <em>prefixed</em> or <em>local</em> name.
     * @return An {@link Optional} of the <em>locally declared</em> {@link NamedDItem Item}
     *         whose <em>prefixed</em> or <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     */
    public default Optional<NamedDItem> getOptionalDeclaredItem(String name) {
        return getOptionalDeclaredItem(Name.of(name));
    }

    /**
     * @param name The <em>prefixed</em> or <em>local</em> name.
     * @return An {@link Optional} of the <em>locally declared</em> {@link Property}
     *         whose <em>prefixed</em> or <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When an {@link NamedDItem} named {@code name}
     *             is <em>locally declared</em> but is not a {@link Property}.
     */
    public Optional<? extends Property> getOptionalDeclaredProperty(Name name);

    /**
     * @param name The <em>local</em> name.
     * @return An {@link Optional} of the <em>locally declared</em> {@link Property}
     *         whose <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When an {@link NamedDItem} named {@code name}
     *             is <em>locally declared</em> but is not a {@link Property}.
     */
    public Optional<? extends Property> getOptionalDeclaredProperty(SName name);

    /**
     * @param name The <em>prefixed</em> or <em>local</em> name.
     * @return An {@link Optional} of the <em>locally declared</em> {@link Property}
     *         whose <em>prefixed</em> or <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When an {@link NamedDItem} named {@code name}
     *             is <em>locally declared</em> but is not a {@link Property}.
     */
    public Optional<? extends Property> getOptionalDeclaredProperty(String name);

    /**
     * @param name The <em>prefixed</em> or <em>local</em> name.
     * @return An {@link Optional} of the <em>locally declared</em> {@link Alias}
     *         whose <em>prefixed</em> or <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When an {@link NamedDItem} named {@code name}
     *             is <em>locally declared</em> but is not an {@link Alias}.
     */
    public Optional<? extends Alias> getOptionalDeclaredAlias(Name name);

    /**
     * @param name The <em>local</em> name.
     * @return An {@link Optional} of the <em>locally declared</em> {@link Alias}
     *         whose <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When an {@link NamedDItem} named {@code name}
     *             is <em>locally declared</em> but is not an {@link Alias}.
     */
    public Optional<? extends Alias> getOptionalDeclaredAlias(SName name);

    /**
     * @param name The <em>prefixed</em> or <em>local</em> name.
     * @return An {@link Optional} of the <em>locally declared</em> {@link Alias}
     *         whose <em>prefixed</em> or <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When an {@link NamedDItem} named {@code name}
     *             is <em>locally declared</em> but is not an {@link Alias}.
     */
    public Optional<? extends Alias> getOptionalDeclaredAlias(String name);

    /**
     * @param name The <em>prefixed</em> or <em>local</em> name.
     * @return The <em>locally declared</em> {@link NamedDItem Item}
     *         whose <em>prefixed</em> or <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When no {@link NamedDItem} named {@code name}
     *             is <em>locally declared</em> in this {@link Dictionary}.
     */
    public NamedDItem getDeclaredItem(Name name);

    /**
     * @param name The <em>local</em> name.
     * @return The <em>locally declared</em> {@link NamedDItem Item}
     *         whose <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When no {@link NamedDItem} named {@code name}
     *             is <em>locally declared</em> in this {@link Dictionary}.
     */
    public default NamedDItem getDeclaredItem(SName name) {
        return getDeclaredItem(Name.of(name));
    }

    /**
     * @param name The <em>prefixed</em> or <em>local</em> name.
     * @return The <em>locally declared</em> {@link NamedDItem Item}
     *         whose <em>prefixed</em> or <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When no {@link NamedDItem} named {@code name}
     *             is <em>locally declared</em> in this {@link Dictionary}.
     */
    public default NamedDItem getDeclaredItem(String name) {
        return getDeclaredItem(Name.of(name));
    }

    /**
     * @param name The <em>prefixed</em> or <em>local</em> name.
     * @return The <em>locally declared</em> {@link Property}
     *         whose <em>prefixed</em> or <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When no {@link Property} named {@code name} is locally declared.
     */
    public Property getDeclaredProperty(Name name);

    /**
     * @param name The <em>local</em> name.
     * @return The <em>locally declared</em> {@link Property}
     *         whose <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When no {@link Property} named {@code name} is locally declared.
     */
    public Property getDeclaredProperty(SName name);

    /**
     * @param name The <em>prefixed</em> or <em>local</em> name.
     * @return The <em>locally declared</em> {@link Property}
     *         whose <em>prefixed</em> or <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When no {@link Property} named {@code name} is locally declared.
     */
    public Property getDeclaredProperty(String name);

    /**
     * @param name The <em>prefixed</em> or <em>local</em> name.
     * @return The <em>locally declared</em> {@link Alias}
     *         whose <em>prefixed</em> or <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When no {@link Alias} named {@code name} is locally declared.
     */
    public Alias getDeclaredAlias(Name name);

    /**
     * @param name The <em>local</em> name.
     * @return The <em>locally declared</em> {@link Alias}
     *         whose <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When no {@link Alias} named {@code name} is locally declared.
     */
    public Alias getDeclaredAlias(SName name);

    /**
     * @param name The <em>prefixed</em> or <em>local</em> name.
     * @return The <em>locally declared</em> {@link Alias}
     *         whose <em>prefixed</em> or <em>local</em> name is {@code name}.
     * @throws IllegalArgumentException When {@code name} is {@code null}.
     * @throws SemanticException When no {@link Alias} named {@code name} is locally declared.
     */
    public Alias getDeclaredAlias(String name);

    /**
     * @param item The item.
     * @return {@code true} when a {@code item} is <em>locally declared</em> in this {@link Dictionary}.
     */
    public boolean isDeclared(NamedDItem item);

    /**
     * @param name The <em>prefixed</em> or <em>local</em> name.
     * @return {@code true} when an {@link NamedDItem Item} named {@code name} is <em>locally declared</em>
     *         in this {@link Dictionary}.
     */
    public boolean hasDeclaredItem(Name name);

    public default boolean hasDeclaredItem(SName name) {
        return hasDeclaredItem(Name.of(name));
    }

    public default boolean hasDeclaredItem(String name) {
        return hasDeclaredItem(Name.of(name));
    }

    /**
     * @param name The <em>prefixed</em> or <em>local</em> name.
     * @return {@code true} when a {@link Property} named {@code name} is <em>locally declared</em>
     *         in this {@link Dictionary}.
     */
    public boolean hasDeclaredProperty(Name name);

    public default boolean hasDeclaredProperty(SName name) {
        return hasDeclaredAlias(Name.of(name));
    }

    public default boolean hasDeclaredProperty(String name) {
        return hasDeclaredAlias(Name.of(name));
    }

    /**
     * @param name The <em>prefixed</em> or <em>local</em> name.
     * @return {@code true} when a {@link Alias} named {@code name} is <em>locally declared</em>
     *         in this {@link Dictionary}.
     */
    public boolean hasDeclaredAlias(Name name);

    public default boolean hasDeclaredAlias(SName name) {
        return hasDeclaredAlias(Name.of(name));
    }

    public default boolean hasDeclaredAlias(String name) {
        return hasDeclaredAlias(Name.of(name));
    }
}