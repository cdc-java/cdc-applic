package cdc.applic.dictionaries;

import cdc.applic.dictionaries.items.Assertion;
import cdc.applic.dictionaries.items.ConstraintAssertion;
import cdc.applic.dictionaries.items.StandardAssertion;
import cdc.applic.expressions.Expression;

public interface DictionaryAssertions {
    /**
     * @return The Expression that restricts usage of parent dictionaries.
     */
    public Expression getContextExpression();

    /**
     * Returns an Iterable of assertions matching a certain type.
     *
     * @param <T> The assertion type.
     * @param cls The assertion class.
     * @return An Iterable of assertions matching {@code cls}.
     */
    public <T extends Assertion> Iterable<? extends T> getAssertions(Class<T> cls);

    /**
     * @return All assertions associated to this Dictionary.
     */
    public Iterable<? extends Assertion> getAllAssertions();

    /**
     * @param assertion The assertion.
     * @return The membership of {@code assertion}.
     */
    public DictionaryMembership getMembership(Assertion assertion);

    /**
     * Removes a <em>standard</em> assertion.
     * <p>
     * <b>WARNING:</b> this should only be used for <em>user defined</em> assertions.<br>
     * Removing a <em>constraint</em> assertion must be done by constraint implementation.
     *
     * @param assertion The assertion.
     */
    public void removeAssertion(StandardAssertion assertion);

    /**
     * Removes all <em>generated</em> and <em>derived</em> assertions that are associated to a constraint.
     *
     * @param constraint The constraint.
     */
    public void removeRelatedAndDerivedAssertions(Constraint constraint);

    /**
     * Returns an {@link Iterable} of all <em>generated</em> assertions associated to a {@link Constraint}.
     *
     * @param constraint The constraint.
     * @return An {@link Iterable} of all <em>generated</em> assertions associated to {@code constraint}.
     */
    public Iterable<? extends ConstraintAssertion> getRelatedAssertions(Constraint constraint);

    /**
     * Returns the <em>generated</em> assertion associated to a {@link Constraint} and parameters.
     *
     * @param constraint The constraint.
     * @param params The parameters.
     * @return The {@link ConstraintAssertion} associated to {@code constraint} and {@code params}, or {@code null}.
     */
    public ConstraintAssertion getRelatedAssertion(Constraint constraint,
                                                   String params);

    public ConstraintAssertion createAssertion(Constraint constraint,
                                               String params,
                                               Expression expression);
}