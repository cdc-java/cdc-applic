package cdc.applic.dictionaries;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.RealType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Expressions;
import cdc.applic.expressions.ast.AbstractNamedNode;
import cdc.applic.expressions.ast.AbstractPropertyNode;
import cdc.applic.expressions.ast.InformalNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.SortableNode;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.content.UncheckedSet;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

/**
 *
 * @author Damien Carbonne
 * @author Guillaume Forget
 */
public final class DictionaryUtils {
    private DictionaryUtils() {
    }

    public static boolean isAvailableBooleanProperty(Name name,
                                                     Dictionary dictionary) {
        Checks.isNotNull(dictionary, "dictionary");

        final NamedDItem item = dictionary.getRegistry().getOptionalItem(name).orElse(null);
        if (item instanceof final Property p) {
            final Type type = p.getType();
            return type instanceof BooleanType;
        } else {
            return false;
        }
    }

    public static Property getAvailableBooleanProperty(Name name,
                                                       Dictionary dictionary) {
        final Property property = dictionary.getRegistry().getProperty(name);
        final Type type = property.getType();
        if (type instanceof BooleanType) {
            return property;
        } else {
            throw new SemanticException("Property named " + name + " is not a boolean property", null);
        }
    }

    /**
     * Fills a collection with NamedDItems from their names.
     *
     * @param names The collection of {@link Name}s.
     * @param items The collection of {@link NamedDItem}s to fill.
     * @param dictionary The dictionary used to retrieve a NamedDItem from its Name.
     */
    public static void fillNamedItems(Collection<Name> names,
                                      Collection<NamedDItem> items,
                                      Dictionary dictionary) {
        final Registry registry = dictionary.getRegistry();
        for (final Name name : names) {
            items.add(registry.getItem(name));
        }
    }

    public static int getOrdinal(SortableNode node,
                                 Dictionary dictionary) {
        if (node instanceof AbstractPropertyNode || node instanceof RefNode) {
            final Name name = ((AbstractNamedNode) node).getName();
            final NamedDItem item = dictionary.getRegistry().getItem(name);
            return item.getOrdinal();
        } else if (node instanceof InformalNode) {
            return Integer.MIN_VALUE + 1;
        } else {
            // FalseNode or TrueNode
            return Integer.MIN_VALUE;
        }
    }

    public static Comparator<SortableNode> createOrdinaNameComparator(Dictionary dictionary) {
        final Comparator<SortableNode> ordinalCmp = Comparator.comparing(n -> getOrdinal(n, dictionary));
        return ordinalCmp.thenComparing(SortableNode::getSortingText);
    }

    /**
     * Returns an expression that contains all mandatory properties.
     *
     * @param dictionary The dictionary.
     * @param initTrue If {@code dictionary} is {@code null} or there are no MANDATORY items,
     *            returns {@link Expression#TRUE} if {@code true},
     *            or {@link Expression#FALSE} if {@code false}.
     * @return An expression that contains all mandatory properties.
     */
    public static Expression createCompliantExpression(Dictionary dictionary,
                                                       boolean initTrue) {
        if (dictionary == null) {
            return initTrue ? Expression.TRUE : Expression.FALSE;
        } else {
            final List<Expression> expressions = new ArrayList<>();
            for (final Property property : dictionary.getAllowedProperties(DItemUsage.MANDATORY)) {
                final SItemSet set;
                if (initTrue) {
                    final Type type = property.getType();
                    final List<SItem> items = new ArrayList<>();
                    // TODO use ElementsSetsOperations
                    if (type instanceof final EnumeratedType t) {
                        t.getValues().forEach(v -> items.add(v.getLiteral()));
                    } else if (type instanceof final IntegerType t) {
                        t.getDomain().getRanges().forEach(items::add);
                    } else if (type instanceof final RealType t) {
                        t.getDomain().getRanges().forEach(items::add);
                    } else if (type instanceof BooleanType) {
                        items.add(StringValue.of("true"));
                        items.add(StringValue.of("false"));
                    }

                    set = UncheckedSet.of(items);
                } else {
                    set = UncheckedSet.EMPTY;
                }
                expressions.add(Expressions.SHORT_NARROW_NO_SIMPLIFY.in(property.getName(), set));
            }
            if (expressions.isEmpty()) {
                return initTrue ? Expression.TRUE : Expression.FALSE;
            } else {
                return Expressions.SHORT_NARROW_SIMPLIFY.and(expressions.toArray(new Expression[expressions.size()]));
            }
        }
    }
}