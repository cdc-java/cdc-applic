package cdc.applic.dictionaries;

import java.util.function.Consumer;
import java.util.function.Supplier;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;

/**
 * Functional implementation of ConstrainedExpression
 *
 * @author Guillaume Forget
 * @author Damien Carbonne
 */
public class ConstrainedExpressionFunctional implements ConstrainedExpression {
    protected final Supplier<DictionaryHandle> handle;
    protected final Supplier<Expression> expressionSupplier;
    protected final Consumer<Expression> expressionConsumer;
    protected final Supplier<ConstrainedExpression> context;

    protected ConstrainedExpressionFunctional(Supplier<DictionaryHandle> handle,
                                              Supplier<Expression> expressionSupplier,
                                              Consumer<Expression> expressionConsumer,
                                              Supplier<ConstrainedExpression> context) {
        this.handle = handle;
        this.expressionSupplier = expressionSupplier;
        this.expressionConsumer = expressionConsumer;
        this.context = context;
    }

    @Override
    public boolean isReadOnly() {
        return expressionConsumer == null;
    }

    @Override
    public Expression getExpression() {
        return expressionSupplier.get();
    }

    @Override
    public void setExpressionUnchecked(Expression expression) {
        expressionConsumer.accept(expression);
    }

    @Override
    public DictionaryHandle getDictionaryHandleOrNull() {
        return handle == null ? null : handle.get();
    }

    @Override
    public ConstrainedExpression getContextOrNull() {
        return context == null ? null : context.get();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Supplier<DictionaryHandle> handle;
        private Supplier<Expression> expressionSupplier;
        private Consumer<Expression> expressionConsumer;
        private Supplier<ConstrainedExpression> context;

        protected Builder() {
            super();
        }

        public Builder handle(Supplier<DictionaryHandle> handle) {
            this.handle = handle;
            return this;
        }

        public Builder context(Supplier<ConstrainedExpression> context) {
            this.context = context;
            return this;
        }

        public Builder expressionSupplier(Supplier<Expression> supplier) {
            this.expressionSupplier = supplier;
            return this;
        }

        public Builder expressionConsumer(Consumer<Expression> consumer) {
            this.expressionConsumer = consumer;
            return this;
        }

        public ConstrainedExpressionFunctional build() {
            return new ConstrainedExpressionFunctional(handle,
                                                       expressionSupplier,
                                                       expressionConsumer,
                                                       context);
        }
    }
}
