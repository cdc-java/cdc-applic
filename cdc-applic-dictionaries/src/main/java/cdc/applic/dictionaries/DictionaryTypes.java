package cdc.applic.dictionaries;

import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Optional;

import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;

/**
 * Interface dedicated to dictionary types.
 *
 * @author Damien Carbonne
 */
public interface DictionaryTypes {
    /**
     * @return A {@link Collection} of <em>locally declared</em> {@link Type Types}.
     */
    public Collection<? extends Type> getDeclaredTypes();

    /**
     * @return A {@link Collection} of <em>all</em> {@link Type Types}.
     */
    public Collection<? extends Type> getAllTypes();

    public DictionaryMembership getMembership(Type type);

    /**
     * Returns the optional {@link Type} that has a <em>local</em> or <em>prefixed</em> name.
     *
     * @param name The <em>local</em> or <em>prefixed</em> name.
     * @return The optional type named {@code name}.
     */
    public Optional<? extends Type> getOptionalType(Name name);

    /**
     * Returns the optional {@link Type} that has a <em>local</em> name.
     *
     * @param name The <em>local</em> name.
     * @return The optional type named {@code name}.
     */
    public Optional<? extends Type> getOptionalType(SName name);

    /**
     * Returns the optional {@link Type} that has a <em>local</em> or <em>prefixed</em> name.
     *
     * @param name The <em>local</em> or <em>prefixed</em> name.
     * @return The optional type named {@code name}.
     */
    public Optional<? extends Type> getOptionalType(String name);

    /**
     * Returns the {@link Type} that has a <em>local</em> or <em>prefixed</em> name.
     *
     * @param name The <em>local</em> or <em>prefixed</em> name.
     * @return The type named {@code name}.
     * @throws NoSuchElementException When no type named {@code name} is found.
     */
    public Type getType(Name name);

    /**
     * Returns the {@link Type} that has a <em>local</em> name.
     *
     * @param name The <em>local</em> name.
     * @return The type named {@code name}.
     * @throws NoSuchElementException When no type named {@code name} is found.
     */
    public Type getType(SName name);

    /**
     * Returns the {@link Type} that has a <em>local</em> or <em>prefixed</em> name.
     *
     * @param name The <em>local</em> or <em>prefixed</em> name.
     * @return The type named {@code name}.
     * @throws NoSuchElementException When no type named {@code name} is found.
     */
    public Type getType(String name);

    public <T extends Type> Optional<? extends T> getOptionalType(Name name,
                                                                  Class<T> cls);

    public <T extends Type> T getType(Name name,
                                      Class<T> cls);

    public <T extends Type> T getType(SName name,
                                      Class<T> cls);

    public <T extends Type> T getType(String name,
                                      Class<T> cls);

}