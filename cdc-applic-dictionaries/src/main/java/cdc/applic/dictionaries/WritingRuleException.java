package cdc.applic.dictionaries;

import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.issues.Diagnosis;
import cdc.issues.HasDiagnosis;

public class WritingRuleException extends ApplicException implements HasDiagnosis<ApplicIssue> {
    private static final long serialVersionUID = 1L;
    private final Diagnosis<ApplicIssue> diagnosis;

    public WritingRuleException(String message,
                                Diagnosis<ApplicIssue> diagnosis) {
        super(message, null);
        this.diagnosis = diagnosis;
    }

    @Override
    public Diagnosis<ApplicIssue> getDiagnosis() {
        return diagnosis;
    }
}