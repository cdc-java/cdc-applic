package cdc.applic.dictionaries;

import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;
import java.util.Set;

/**
 * Multilingual description.
 *
 * @author Damien Carbonne
 */
public interface Description {
    public static final Comparator<Locale> LOCALE_LANGUAGE_COMPARATOR =
            Comparator.comparing(Locale::getLanguage);

    /**
     * Default empty Description.
     */
    public static final Description EMPTY = new Description() {
        @Override
        public Set<Locale> getLocales() {
            return Collections.emptySet();
        }

        @Override
        public String getContent(Locale locale) {
            return null;
        }
    };

    /**
     * @return A {@link Set} of {@link Locale Locales} for which a descriptive text is provided.
     */
    public Set<Locale> getLocales();

    /**
     * Returns the descriptive text associated to a {@link Locale}.
     *
     * @param locale The Locale.
     * @return The descriptive text associated to {@code locale}.
     */
    public String getContent(Locale locale);
}