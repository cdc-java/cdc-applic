package cdc.applic.dictionaries;

public interface DOwnedElement extends DElement {
    public DElement getOwner();
}