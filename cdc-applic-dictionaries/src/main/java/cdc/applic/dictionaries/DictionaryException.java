package cdc.applic.dictionaries;

import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.issues.Diagnosis;
import cdc.issues.HasDiagnosis;

public class DictionaryException extends ApplicException implements HasDiagnosis<ApplicIssue> {
    private static final long serialVersionUID = 1L;
    private final Diagnosis<ApplicIssue> diagnosis;

    public DictionaryException(String message,
                               Diagnosis<ApplicIssue> diagnosis,
                               Throwable cause) {
        super(message, cause);
        this.diagnosis = diagnosis;
    }

    public DictionaryException(String message,
                               Diagnosis<ApplicIssue> diagnosis) {
        this(message, diagnosis, null);
    }

    @Override
    public Diagnosis<ApplicIssue> getDiagnosis() {
        return diagnosis;
    }
}