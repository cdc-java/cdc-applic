package cdc.applic.dictionaries;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.checks.NonUsableIdentifierIssue;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.literals.Name;

class NonUsableIdentifierIssueTest {
    @Test
    void test() {
        final NonUsableIdentifierIssue issue = new NonUsableIdentifierIssue(null,
                                                                            TrueNode.INSTANCE,
                                                                            FalseNode.INSTANCE,
                                                                            "description",
                                                                            Name.of("Name"));
        assertEquals(TrueNode.INSTANCE, issue.getRootNode());
        assertEquals(FalseNode.INSTANCE, issue.getNode());
        assertEquals("description", issue.getDescription());
        assertEquals(Name.of("Name"), issue.getIdentifierName());
    }
}