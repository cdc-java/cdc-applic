package cdc.applic.dictionaries;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.checks.DItemUsageIssue;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.TrueNode;

class DItemUsageIssueTest {
    @Test
    void test() {
        final DItemUsageIssue issue = new DItemUsageIssue(null,
                                                          TrueNode.INSTANCE,
                                                          FalseNode.INSTANCE,
                                                          "description",
                                                          DItemUsage.FORBIDDEN);
        assertEquals(TrueNode.INSTANCE, issue.getRootNode());
        assertEquals(FalseNode.INSTANCE, issue.getNode());
        assertEquals("description", issue.getDescription());
        assertEquals(DItemUsage.FORBIDDEN, issue.getUsage());
        // Coverage
        issue.toString();
    }
}