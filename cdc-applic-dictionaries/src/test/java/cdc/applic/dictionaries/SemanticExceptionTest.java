package cdc.applic.dictionaries;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.issues.Diagnosis;

class SemanticExceptionTest {
    @Test
    void test() {
        final SemanticException x = new SemanticException("Hello", Diagnosis.okDiagnosis());
        assertEquals("Hello", x.getMessage());
        assertEquals(Diagnosis.OK_DIAGNOSIS, x.getDiagnosis());
    }
}