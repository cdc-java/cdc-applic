package cdc.applic.dictionaries;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

class DItemUsageTest {

    static boolean isCompliant(DItemUsage child,
                               DItemUsage... parents) {
        return DItemUsage.isCompliant(child, List.of(parents));
    }

    @Test
    void testIsCompliant() {
        assertTrue(isCompliant(DItemUsage.FORBIDDEN));
        assertTrue(isCompliant(DItemUsage.OPTIONAL));
        assertTrue(isCompliant(DItemUsage.RECOMMENDED));
        assertTrue(isCompliant(DItemUsage.MANDATORY));

        assertTrue(isCompliant(DItemUsage.FORBIDDEN, DItemUsage.OPTIONAL));
        assertTrue(isCompliant(DItemUsage.OPTIONAL, DItemUsage.OPTIONAL));
        assertTrue(isCompliant(DItemUsage.RECOMMENDED, DItemUsage.OPTIONAL));
        assertTrue(isCompliant(DItemUsage.MANDATORY, DItemUsage.OPTIONAL));

        assertTrue(isCompliant(DItemUsage.FORBIDDEN, DItemUsage.FORBIDDEN));
        assertFalse(isCompliant(DItemUsage.OPTIONAL, DItemUsage.FORBIDDEN));
        assertFalse(isCompliant(DItemUsage.RECOMMENDED, DItemUsage.FORBIDDEN));
        assertFalse(isCompliant(DItemUsage.MANDATORY, DItemUsage.FORBIDDEN));

        assertTrue(isCompliant(DItemUsage.FORBIDDEN, DItemUsage.RECOMMENDED));
        assertTrue(isCompliant(DItemUsage.OPTIONAL, DItemUsage.RECOMMENDED));
        assertTrue(isCompliant(DItemUsage.RECOMMENDED, DItemUsage.RECOMMENDED));
        assertTrue(isCompliant(DItemUsage.MANDATORY, DItemUsage.RECOMMENDED));

        assertTrue(isCompliant(DItemUsage.FORBIDDEN, DItemUsage.MANDATORY));
        assertTrue(isCompliant(DItemUsage.OPTIONAL, DItemUsage.MANDATORY));
        assertTrue(isCompliant(DItemUsage.RECOMMENDED, DItemUsage.MANDATORY));
        assertTrue(isCompliant(DItemUsage.MANDATORY, DItemUsage.MANDATORY));

    }
}