package cdc.applic.dictionaries;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.checks.ContentIssue;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.content.BooleanSItem;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.literals.Name;

class ContentIssueTest {
    @Test
    void test() {
        final Type type = new BooleanType() {
            @Override
            public Name getName() {
                return null;
            }

            @Override
            public boolean isCompliant(SItem item) {
                return false;
            }

            @Override
            public BooleanSet getDomain() {
                return null;
            }

            @Override
            public boolean isCompliant(BooleanSItem item) {
                return false;
            }

            @Override
            public RefSyn<Name> getNames() {
                return null;
            }
        };
        final ContentIssue issue1 = new ContentIssue(null,
                                                     TrueNode.INSTANCE,
                                                     FalseNode.INSTANCE,
                                                     "description",
                                                     BooleanValue.FALSE,
                                                     type);
        final ContentIssue issue2 = new ContentIssue(null,
                                                     TrueNode.INSTANCE,
                                                     FalseNode.INSTANCE,
                                                     "description",
                                                     BooleanSet.FALSE,
                                                     type);

        assertEquals(TrueNode.INSTANCE, issue1.getRootNode());
        assertEquals(FalseNode.INSTANCE, issue1.getNode());
        assertEquals("description", issue1.getDescription());
        assertEquals(type, issue1.getType());
        assertEquals(BooleanValue.FALSE, issue1.getItem());
        assertEquals(null, issue1.getSet());
        assertEquals(null, issue2.getItem());
        assertEquals(BooleanSet.FALSE, issue2.getSet());
    }
}