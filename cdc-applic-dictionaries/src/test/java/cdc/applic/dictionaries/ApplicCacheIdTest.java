package cdc.applic.dictionaries;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.handles.ApplicCache;
import cdc.applic.dictionaries.handles.ApplicCacheId;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.util.debug.Verbosity;

class ApplicCacheIdTest {
    private static class TestCache1 extends ApplicCache {
        protected TestCache1(DictionaryHandle handle,
                             Object tag) {
            super(handle, tag);
        }

        @Override
        public void print(PrintStream out,
                          int level,
                          Verbosity verbosity) {
            // Ignore
        }

        @Override
        public void invalidate() {
            // Ignore
        }

        @Override
        public void clear() {
            // Ignore
        }
    }

    private static class TestCache2 extends ApplicCache {
        protected TestCache2(DictionaryHandle handle,
                             Object tag) {
            super(handle, tag);
        }

        @Override
        public void print(PrintStream out,
                          int level,
                          Verbosity verbosity) {
            // Ignore
        }

        @Override
        public void invalidate() {
            // Ignore
        }

        @Override
        public void clear() {
            // Ignore
        }
    }

    @Test
    void test() {
        final ApplicCacheId id1 = new ApplicCacheId(TestCache1.class);
        final ApplicCacheId id2 = new ApplicCacheId(TestCache1.class, null);
        final ApplicCacheId id3 = new ApplicCacheId(TestCache1.class, "Hello");
        final ApplicCacheId id4 = new ApplicCacheId(TestCache1.class, "World");
        final ApplicCacheId id5 = new ApplicCacheId(TestCache2.class, "Hello");

        assertEquals(TestCache1.class, id1.getCacheClass());
        assertEquals(null, id1.getTag());

        assertEquals(id1, id1);
        assertEquals(id1, id2);
        assertNotEquals(id1, null);
        assertNotEquals(id1, "Hello");
        assertNotEquals(id1, id3);
        assertNotEquals(id3, id4);
        assertNotEquals(id3, id5);
        assertEquals(id1.hashCode(), id2.hashCode());

        assertTrue(id1.compareTo(id2) == 0);
        assertTrue(id3.compareTo(id4) < 0);
    }
}