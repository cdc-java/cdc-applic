package cdc.applic.dictionaries;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.issues.locations.DefaultLocation;

class WritingRuleIssueTest {
    @Test
    void test() {
        final WritingRuleIssue issue = new WritingRuleIssue("rule", new DefaultLocation("targetId", "path"), "description");
        assertEquals("rule", issue.getRuleName());
        assertEquals("description", issue.getDescription());
    }
}