package cdc.applic.dictionaries;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.types.Ordered;

class OrderedTest {
    private static class X implements Ordered {
        private final int ordinal;

        public X(int ordinal) {
            this.ordinal = ordinal;
        }

        @Override
        public int getOrdinal() {
            return ordinal;
        }
    }

    @Test
    void testComparator() {
        final X x1 = new X(1);
        final X x2 = new X(2);
        assertTrue(Ordered.ORDINAL_COMPARATOR.compare(x1, x1) == 0);
        assertTrue(Ordered.ORDINAL_COMPARATOR.compare(x1, x2) < 0);
        assertTrue(Ordered.ORDINAL_COMPARATOR.compare(x2, x1) > 0);
    }
}