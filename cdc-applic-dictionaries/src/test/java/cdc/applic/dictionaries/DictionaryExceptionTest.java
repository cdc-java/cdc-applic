package cdc.applic.dictionaries;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.issues.Diagnosis;

class DictionaryExceptionTest {
    @Test
    void test() {
        final DictionaryException x = new DictionaryException("Message", Diagnosis.okDiagnosis());
        assertEquals(Diagnosis.OK_DIAGNOSIS, x.getDiagnosis());
    }
}