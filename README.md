# CDC-Applic
This project contains code related to handling of S-Series-like
**applicability statements**, with the intent to support:
- their formal verification
    - are they valid?
    - are they meaningful?
    - are they defined in a consistent manner?
    - are they compliant?
    - are they equivalent?
    - ...  
- their transformations
    - simplification
    - conversion
    - ...
- and their visualization.

**Note:** in this library, **expression** or **applicability expression** is the name given to applicability statement.

## S-Series applicability statements
According to S1000D, *applicability information allows the technical author to specify which data is appropriate in what situations*.
And according to SX002D, an applicability statement is the *situation or situations under which related items are valid*.  

Typically, the situations can be related to:
- The state or configuration of the supported product;
- The state or configuration of tools used to support the product;
- The events the supported product met during it use;
- The environmental conditions the product was operated in;
- The environmental conditions where the product is currently supported;
- The skills of the personnel in charge of supporting the product;
- ...

The produced applicability expressions are mainly intended for filtering.  
Building meaningful applicability expressions is not in the primary scope of S-Series,
it is clearly in the scope of this library.

S1000D was first (in S-Series) to adopt a powerful formalization of expressions.  
Other (S-Series) standards (S3000L, S5000F, ...) adopted the S1000D approach with some differences.


### S1000D point of view
With S1000D, an applicability can be:
- an informal **display text**
- an informal **display text** and a **formula**
- a **formula**

![Image](doc-files/S1000D-4.2-applic.png)

A formula can take 3 forms:
- **assert** which is a terminal formula built 
    - either with a **property** (`applicPropertyIdent` and `applicPropertyType` attributes)
      and a set of **values** (`applicPropertyValues` attribute), possibly a singleton,
    - or with an informal **text** (element content).
- **evaluate** which is a logical `or` or `and` combination of **assert** or **evaluate**.
- **expression** which can be interpreted by a logical engine (used by process data modules).


**Note:** the **expression** approach is not supported by this library.  
It can use a rich set of functions (addition, subtraction, division, trigonometric functions, ...), which seems overkill for our needs.

![Image](doc-files/S1000D-4.2-evaluate.png)

Two kinds of properties are defined:
- **product attributes** (in ACT): they have an anonymous data type
- **conditions** (in CCT): they have an explicit data type

**ACT**  
![Image](doc-files/S1000D-4.2-act-content.png)
![Image](doc-files/S1000D-4.2-act.png)  
![Image](doc-files/S1000D-4.2-product-attribute.png)

**CCT**  
![Image](doc-files/S1000D-4.2-cct-content.png)
![Image](doc-files/S1000D-4.2-cct.png)  
![Image](doc-files/S1000D-4.2-condition-type.png)
![Image](doc-files/S1000D-4.2-condition.png)

**Note:** in this library, there is no such distinction,
there are only properties, and all data types are explicit.

S1000D data types for product attributes (§4.14.1-2.1.2.1) and
conditions (§4.14.2-2.2.1) are:
- boolean
- string
- integer
- real

![Image](doc-files/S1000D-properties-and-types.svg)

It is the `valueDataType` attribute of `productAttribute` or `condType` elements that defines the data type.
 
In addition, enumeration(s) and pattern (only for strings, see §3.9.5.3.1-2.3.1 and
§3.9.5.3.2-2.3.1) can be associated to those data types to constrain the set of allowed values.

Enumerations can be defined with single values or ranges.

To define an integer or real domain, one can use values and ranges.  
Examples:
- 0\~100|200|300|400\~999
- -10.0\~10.0

Using ranges with string may have surprising effects.  
Defining an integer type as 1\~3, means that only 1, 2 and 3 are valid values.
One could also write 01, 02, etc, but semantic is unchanged.

Defining a string type as 1\~3 means something different.
All strings that are lexicographically between 1 and 3 are valid.
So, without additional constraints (e.g., using pattern), this string type defines an
infinite set of values:
- 1, 11, 12, ...,1A, 1B, ..., 111, 112, ...,11A, 11B, ..., 2, 21, 22, ...,2A, 2B, ...,
221, 222, ..., 22A, 22B, ... 3

**Note:** this interpretation of string ranges is possibly wrong and must be confirmed.  
All S1000D examples use lower and upper strings that have the same length, such as "F/A-18A~F/A-18D".  
S1000D (§3.9.5.3-2.2.4) warns on use of ranges:
*It is recommended that projects or organizations consider*
*the use of ranges on values that are not numerical sequences.*  
*Ranges on some alphanumeric values, if used in the human-readable display text,*
*can be difficult to understand and can be misinterpreted by users.*


Assigning values to a property can be done by using single values or ranges.  
Examples:
- 1|10~20|30
- A|BB~ZZ|FF
- 1.0~2.0|3.1 

Some additional attributes are associated to product attributes and conditions,
but they have no influence on formal verification and are therefore ignored.  

S1000D provides means to control the way an applicability is displayed to end user.

### SX002D point of view
The `common data model` (SX002D) describes the `Applicability Statement UoF` (Unit of Functionality) as:

![Image](doc-files/SX002D-2.1-UoF Applicability Statement.png)

In addition to `and` and `or`, `xor` and `not` are allowed.  
Unlike S1000D, set operations don't seem possible,
except simple ranges for `EvaluationByAssertionOfSerializedItems` and conditions using `ValueRangePropertyType`.  
Similarly, display of expressions does not seem to be in the scope of this standard.

**Note:** `xor` is defined as binary whilst `and` and `or` are nary.

It is possible to reuse an existing expression
(`EvaluationByNestedApplicabilityStatement`) to define an expression. 

Property types and values are:
- either implicitly defined from objects of the data model
  (`ClassInstanceAssertion` and `SerializedAssertion`),
- or explicitly defined (`ConditionType` and `ConditionTypeAssertMember`).

Properties are:
- either explicitly defined as `ConditionType` or `ConditionInstance`,
- or implicitly defined from objects of the data model.  
  `ClassInstanceAssertion` can be considered as independent boolean properties.

### CDC choices
S-Series approach in its generality is quite challenging for formal verification.  
This is why we adopted a pragmatic solution by only allowing things that can be
verified formally.
This now (as of 2024-05-11) includes informal texts.

An expression being a combination of operators, properties and values, the main
challenge is with negation (even if negation is not allowed by S1000D).  
Negation is necessary to write implications or equivalences (which are also
out of S-Series scope).  
Using negation necessitates computation of the complement of a set of values.
Negation on ranges necessitates the computation of the successor or predecessor
of a value in the set of possible values.
 
At the moment, the following types are supported:
 - boolean
 - integer
 - real
 - enumerated string
 - pattern string

![Image](cdc-applic-dictionaries/src/main/javadoc/cdc/applic/dictionaries/doc-files/types.svg)

Many operators, including negation, are supported (however `xor` is not yet supported).  
Means are provided to eliminate them in order to obtain a compliant expression.  
Several syntaxes are available to define operators.


### Comparison of approaches and features
S1000D is XML-based and defines expressions as XML nodes.  
Other S-Series adopted a data model / UML-based approach from which corresponding XML schemas are derived.  
In both cases, the structure and semantic of applicability expressions is made explicit, which obviously
facilitates the use of expressions by tools.

This library takes a compiler-based approach: expressions are strings that must be analyzed to extract their structure and semantic.

#### Misc features

<table>
   <thead>
      <tr><th>Feature</th><th>S1000D</th><th>SX002D</th><th>CDC Applic</th><th>Notes</th></tr>
   </thead>
   <tbody>
      <tr><td>Approach</td><td>XSD</td><td>UML</td><td>Text</td><td></td></tr>
      <tr><td>Informal expressions</td><td>Yes</td><td>No</td><td>Yes</td><td></td></tr>
      <tr><td>Formal expressions</td><td>Yes</td><td>Yes</td><td>Yes</td><td></td></tr>
      <tr><td>Filtering</td><td>Yes</td><td>Yes</td><td>Yes</td><td></td></tr>
      <tr><td>Proofs</td><td>No</td><td>No</td><td>Yes</td><td></td></tr>
      <tr><td>Transformations</td><td>No</td><td>No</td><td>Yes</td><td></td></tr>
      <tr><td>Simplification</td><td>No</td><td>No</td><td>Yes <sup>(1)</sup></td><td><sup>(1)</sup> This topic needs some improvements.</td></tr>
      <tr><td>Projection</td><td>No</td><td>No</td><td>Yes</td><td></td></tr>
      <tr><td>Check of writing rules</td><td>No</td><td>No</td><td>Yes</td><td></td></tr>
      <tr><td>Display text</td><td>Yes <sup>(1)</sup></td><td>No</td><td>Yes</td><td><sup>(1)</sup> It should be configured as it may depend on the client.</td></tr>
      <tr><td>Reuse of expressions</td><td>Partial</td><td>Yes</td><td>Yes <sup>(1)</sup></td><td><sup>(1)</sup> See Aliases in CDC Applic.</td></tr>
      <tr><td>Multiple dictionaries</td><td>Yes <sup>(1)</sup></td><td>No</td><td>Yes</td><td><sup>(1)</sup> See ACT catalogs.<br>However, this topic may lack maturity.</td></tr>
      <tr><td>Interdependencies of properties</td><td>Partial <sup>(1)</sup></td><td>No</td><td>Yes <sup>(2)</sup></td><td><sup>(1)</sup> S1000d has a limited solution for conditions.<br><sup>(2)</sup> See Assertions and Constraints in CDC Applic.</td></tr>
      <tr><td>Property name synonyms</td><td>No</td><td>Yes</td><td>No</td><td></td></tr>
      <tr><td>Boolean type</td><td>Yes</td><td>No</td><td>Yes</td><td></td></tr>
      <tr><td>Boolean ranges</td><td>Yes</td><td>No</td><td>No</td><td></td></tr>
      <tr><td>Enumerated type</td><td>Yes</td><td>Yes</td><td>Yes</td><td></td></tr>
      <tr><td>Pattern type</td><td>Yes</td><td>?</td><td>Yes <sup>(1)</sup></td><td><sup>(1)</sup> Limitations on transformations and proofs with CDC Applic.</td></tr>
      <tr><td>String type</td><td>Yes</td><td>Yes</td><td>Yes <sup>(1)</sup></td><td><sup>(1)</sup> Use '.*' pattern with CDC Applic.</td></tr>
      <tr><td>String ranges</td><td>Yes</td><td>No</td><td>No <sup>(1)</sup></td><td><sup>(1)</sup> They seem useless with enumerated types.<br>No solution has been found with pattern types.</td></tr>
      <tr><td>Integer type</td><td>Yes</td><td>No</td><td>Yes</td><td></td></tr>
      <tr><td>Integer ranges</td><td>Yes</td><td>No</td><td>Yes</td><td></td></tr>
      <tr><td>Real type</td><td>Yes</td><td>Yes</td><td>Yes <sup>(1)</sup></td><td><sup>(1)</sup> The use seems difficult at the moment with CDC Applic when transformations are applied.<br>Introducing precision might be necessary.</td></tr>
      <tr><td>Real ranges</td><td>Yes</td><td>Yes</td><td>Yes</td><td></td></tr>
      <tr><td>Value synonyms</td><td>No</td><td>Yes</td><td>No</td><td></td></tr>
      <tr><td>true, false as expressions</td><td>No <sup>(1)</sup></td><td>No <sup>(2)</sup></td><td>Yes</td><td><sup>(1)(2)</sup> The absence of applicability statement can be interpreted as true.</td></tr>
      <tr><td>Units of measures</td><td>No</td><td>Yes</td><td>No</td><td></td></tr>
   </tbody>
</table>

#### Operators

<table>
   <thead>
      <tr><th>Feature</th><th>S1000D</th><th>SX002D</th><th>CDC Applic</th><th>Notes</th></tr>
   </thead>
   <tbody>
      <tr><td>And</td><td>Yes</td><td>Yes</td><td>Yes</td><td></td></tr>
      <tr><td>Or</td><td>Yes</td><td>Yes</td><td>Yes</td><td></td></tr>
      <tr><td>Xor</td><td>No</td><td>Yes</td><td>Yes</td><td></td></tr>
      <tr><td>Not</td><td>No</td><td>Yes</td><td>Yes</td><td></td></tr>
      <tr><td>Implication</td><td>No</td><td>No</td><td>Yes</td><td></td></tr>
      <tr><td>Equivalence</td><td>No</td><td>No</td><td>Yes</td><td></td></tr>
      <tr><td>Equal</td><td>Yes <sup>(1)</sup></td><td>Yes <sup>(2)</sup></td><td>Yes</td><td><sup>(1)</sup> x <em>equal</em> y must be modeled as x &isin; {y} with S1000D.<br><sup>(2)</sup> See <em>conditionTypeAssertMemberAssertValueComparisonOperator EQ</em>.</td></tr>
      <tr><td>Not Equal</td><td>No</td><td>Yes <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> See <em>conditionTypeAssertMemberAssertValueComparisonOperator NE</em>.</td></tr>
      <tr><td>Is In</td><td>Yes</td><td>Yes <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> See <em>conditionTypeAssertMemberAssertValueComparisonOperator IN</em>.</td></tr>
      <tr><td>Is Not In</td><td>No</td><td>Yes <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> See <em>conditionTypeAssertMemberAssertValueComparisonOperator OUT</em>.</td></tr>
      <tr><td>Less Than</td><td>No</td><td>Yes <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> See <em>conditionTypeAssertMemberAssertValueComparisonOperator LT</em>.</td></tr>
      <tr><td>Less Or Equal</td><td>No</td><td>Yes <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> See <em>conditionTypeAssertMemberAssertValueComparisonOperator LE</em>.</td></tr>
      <tr><td>Greater Than</td><td>No</td><td>Yes <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> See <em>conditionTypeAssertMemberAssertValueComparisonOperator GT</em>.</td></tr>
      <tr><td>Greater Or Equal</td><td>No</td><td>Yes <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> See <em>conditionTypeAssertMemberAssertValueComparisonOperator GE</em>.</td></tr>
      <tr><td>Not Less Than</td><td>No</td><td>No</td><td>Yes</td><td></td></tr>
      <tr><td>Neither Less Nor Equal</td><td>No</td><td>No</td><td>Yes</td><td></td></tr>
      <tr><td>Not Greater Than</td><td>No</td><td>No</td><td>Yes</td><td></td></tr>
      <tr><td>Neither Greater Nor Equal</td><td>No</td><td>No</td><td>Yes</td><td></td></tr>
   </tbody>
</table>

**Note:** `conditionTypeAssertMemberAssertValueComparisonOperator`  allows
usage of some operator with SX002D conditions.

### Compared examples
In the following examples:
- `Rank` is an integer property,
- `Length` is a real property,
- `CH0001` is a boolean property,
- `Version` is an enumerated property with 3 values, V1, V2 and V3.


<table>
   <thead>
      <tr><th>Expression</th><th>S1000D</th><th>SX002D</th><th>CDC Applic</th><th>Notes</th></tr>
   </thead>
   <tbody>
      <tr><td>Rank = 10</td><td>Not strictly <sup>(1)</sup></td><td>Not strictly <sup>(2)</sup></td><td>Yes</td><td><sup>(1)</sup> As Rank &isin; {10}.<br><sup>(2)</sup> Using EQ condition, SingleValuePropertyType and considering integers as reals.</td></tr>
      <tr><td>Rank &isin; {10~100}</td><td>Yes</td><td>Not strictly <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> Using IN condition, ValueRangePropertyType and considering integers as reals.</td></tr>
      <tr><td>Rank &isin; {10,11}</td><td>Yes</td><td>Not strictly <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> Combining EQ conditions, SingleValuePropertyType and considering integers as reals: Rank = 10 or Rank = 11.</td></tr>
      <tr><td>Rank &isin; &empty;</td><td>Yes? <sup>(1)</sup></td><td>Yes? <sup>(2)</sup></td><td>Yes</td><td><sup>(1)</sup> Using an empty set of values. Is this allowed?<br><sup>(2)</sup> Using IN condition and no value?</td></tr>
      <tr><td>Rank &ne; 10</td><td>No</td><td>Not strictly <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> Using NE condition and considering integers as reals.</td></tr>
      <tr><td>Rank &notin; {10~100}</td><td>No</td><td>Not strictly <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> Using OUT condition, ValueRangePropertyType and considering integers as reals.</td></tr>
      <tr><td>CH0001 = true</td><td>Not strictly <sup>(1)</sup></td><td>Not strictly <sup>(2)</sup></td><td>Yes</td><td><sup>(1)</sup> As CH0001 &isin; {true}.<br><sup>(2)</sup>Using EQ condition and considering booleans as reals, or using TextPropertytType.</td></tr>
      <tr><td>CH0001</td><td>Not strictly <sup>(1)</sup></td><td>Not strictly <sup>(2)</sup></td><td>Yes</td><td><sup>(1)</sup> As CH0001 &isin; {true}.<br><sup>(2)</sup>Using EQ condition and considering booleans as reals, or using TextPropertytType.</td></tr>
      <tr><td>Version &isin; {V1, V2}</td><td>Yes</sup></td><td>Not strictly <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> Combining EQ conditions: Version = V1 or Version = V2 with TextPropertyType.</td></tr>
      <tr><td>Version = V1</td><td>Not strictly <sup>(1)</sup></td><td>Yes</td><td>Yes</td><td><sup>(1)</sup> As Version &isin; {V1}.</td></tr>
      <tr><td>Length = 10.0</td><td>Not strictly <sup>(1)</sup></td><td>Yes <sup>(2)</sup></td><td>Yes</td><td><sup>(1)</sup> As Length &isin; {10.0}.<br><sup>(2)</sup> Using EQ condition and SingleValuePropertyType.</td></tr>
      <tr><td>Length &ne; 10.0</td><td>No</td><td>Yes <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> Using NE condition and SingleValuePropertyType.</td></tr>
      <tr><td>Length &isin; {10.0~20.0}</td><td>Yes</td><td>Yes <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> Using IN condition and ValueRangePropertyType.</td></tr>
      <tr><td>Length &notin; {10.0~20.0}</td><td>No</td><td>Yes <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> Using OUT condition and ValueRangePropertyType.</td></tr>
      <tr><td>Length &ge; 10.0</td><td>No</td><td>Yes <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> Using GE condition and SingleValuePropertyType.</td></tr>
      <tr><td>Length &gt; 10.0</td><td>No</td><td>Yes <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> Using GT condition and SingleValuePropertyType.</td></tr>
      <tr><td>Length &le; 10.0</td><td>No</td><td>Yes <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> Using LE condition and SingleValuePropertyType.</td></tr>
      <tr><td>Length &lt; 10.0</td><td>No</td><td>Yes <sup>(1)</sup></td><td>Yes</td><td><sup>(1)</sup> Using LT condition and SingleValuePropertyType.</td></tr>
      <tr><td>$it is hot$</td><td>Yes</td><td>No</td><td>Yes</td><td></td></tr>
   </tbody>
</table>


## Principles
- A **system** (product, equipment, tool, environment, ...) is characterized by a set of **properties**.
- Each **property** has a **type** which is a **set of values**.
- The number of **properties** characterizing a **system** can change (usually increase) with time.
- The **values** of a **type** can change (usually increase) with time.
- There is no time relationship between **properties**.  
  Doing something before another thing cannot be expressed directly.  
- Each **property** of a **system instance** has one and only one **value** at a given time.  
  But this **value** is not necessarily know from everyone.    
- Each **property** of a **system instance** can change with time. This is typical of conditions.

### Interpretation
A **Dictionary** defines **Properties** that can take **Values**.  
Properties are generally linked: certain combinations of values are allowed, others are disallowed.

Mathematically, each property P<sub>i</sub> (i &isin; [1..N]) of a dictionary D defines a set of values
S(P<sub>i</sub>) = {V<sub>i,1</sub>, V<sub>i,2</sub>, ..., V<sub>i,N<sub>i</sub></sub>}
and  D defines the Cartesian product of those sets:  
S(D) = S(P<sub>1</sub>) &times; S(P<sub>2</sub>) &times; ... &times; S(P<sub>N</sub>) 
= {V<sub>1,1</sub>, V<sub>1,2</sub>, ..., V<sub>1,N<sub>1</sub></sub>}
&times; {V<sub>2,1</sub>, V<sub>2,2</sub>, ..., V<sub>2,N<sub>2</sub></sub>}
&times; ...
&times; {V<sub>N,1</sub>, V<sub>N,2</sub>, ..., V<sub>N,N<sub>N</sub></sub>}  
An **Expression** E defines a subset, S(E), of S(D), and **Assertions** A<sub>i</sub> (i &isin; [1..M])
define subsets, S(&not;A<sub>i</sub>), of S(D) that are invalid.

**Note:** we will often use X instead of S(X) to designate the set associated to X, where X can be a dictionary,
a property, an expression, or an assertion.

![Image](doc-files/set-interpretation.svg)

**Note:** the dimension of S(D), N, the number of properties, may typically be greater than 1000.

## Modules

There are 4 types of modules:
- **Foundations** that provide core data types and algorithms.
  They are related to expressions, dictionaries and proofs.
- **Utilities** that provide high level functionalities. 
  They contain API, data types and sometimes implementations.
- **Defaults** containing default implementations of data models or basic functions.
- **Implementations** containing reference implementations of utilities.
  They should only be used to construct objects and should be considered as runtime dependencies.

![Image](doc-files/modules.svg)

### Foundations modules

#### Expressions
##### Grammar
An approximate grammar of expressions is:   
<pre><code>
Expr ::= '(' Expr ')'
       | BinaryExpr
       | UnaryExpr
       | BaseExpr
       | InformalExpr
       | BooleanValue
BinaryExpr ::= Expr (AndSymbol | OrSymbol | ImplicationSymbol | EquivalenceSymbol | XorSymbol) Expr
UnaryExpr ::= NotSymbol Expr
BaseExpr ::= ValueExpr
           | SetExpr
           | RefExpr
InformalExpr ::= '$' ([. - '$']* | '$$')* '$'
ValueExpr ::= PropertyName (EqualSymbol | NotEqualSymbol
                            | LessSymbol | NotLessSymbol
                            | GreaterSymbol | NotGreaterSymbol
                            | LessOrEqualSymbol | NeitherLessNorEqualSymbol
                            | GreaterOrEqualSymbol | NeitherGreaterNorEqualSymbol) Value
SetExpr ::= PropertyName (InSymbol | NotInSymbol) Set
Set ::= EmptySet
      | NormalSet
EmptySet ::= '{}' 
           | '&empty;'
NormalSet ::= '{' SetItem  (',' SetItem)* '}'
SetItem ::= Value
          | Range
Value ::= StringValue
        | IntegerValue
        | RealValue
        | BooleanValue
Range ::= IntegerRange
        | RealRange
IntegerRange ::= IntegerValue ToSymbol IntegerValue
RealRange ::= RealValue ToSymbol RealValue
RefExpr ::= PropertyName
          | AliasName
PropertyName ::= Name
AliasName ::= Name
Name ::= (PrefixName PathSeparatorSymbol)? LocalName
PrefixName ::= StringValue
LocalName ::= StringValue
StringValue ::= StrictString
              | EscapedString
StrictString ::= [. - .' {}()-!<>&|,"']+
EscapedString ::= '"' ([. - '"']* | '""')* '"'
IntegerValue ::= ('+'|'-')?[0-9]+
RealValue ::= ('+'|'-')?[0-9]+'.'[0-9]+(([eE]('+'|'-')?[0-9]+)?
BooleanValue ::= TrueValue
               | FalseValue
PathSeparatorSymbol ::= '.'
ToSymbol ::= '~'
           | 'to'
TrueValue ::= [Tt][Rr][Uu][Ee]
            | '&top;'
FalseValue ::= [Ff][Aa][Ll][Ss][Ee]
             | '&perp;'
NotSymbol ::= [Nn][Oo][Tt]
            | '!'
            | '&not;'
AndSymbol ::= [Aa][Nn][Dd]
            | '&amp;'
            | '&and;'
OrSymbol ::= [Oo][Rr]
           | '|'
           | '&or;'
ImplicationSymbol ::= [Ii][Mm][Pp]
                    | '->'
                    | '&rarr;'
EquivalenceSymbol ::= [Ii][Ff][Ff]
                    | '&lt;->'
                    | '&harr;'
XorSymbol ::= [Xx][Oo][Rr]
            | '>-&lt;'
            | '&nharr;'
EqualSymbol ::= '='
NotEqualSymbol ::= '!='
                 | '&ne;'
LessSymbol ::= '&lt;'
NotLessSymbol ::= '!&lt;'
                | '&nlt;'
GreaterSymbol ::= '>'
NotGreaterSymbol ::= '!>'
                   | '&ngt;'
LessOrEqualSymbol ::= '&lt;='
                    | '&le;'
NeitherLessNorEqualSymbol ::= '!&lt;='
                            | '&nle;'
GreaterOrEqualSymbol ::= '>='
                       | '&ge;'
NeitherGreaterNorEqualSymbol ::= '!>='
                               | '&nge;'
InSymbol ::= [Ii][NN]
           | '&lt;:'
           | '&isin;'
NotInSymbol ::= [Nn][Oo][Tt]' '+[Ii][Nn]
              | '!&lt;:'
              | '&notin;'
</code></pre>

With current implementation, the type family of a value is recognized during
parsing.   
So, 1 is recognized as an integer, 1.0 as a real, true as a boolean, 1a or 1.0a
as a string.   
Using 1, 1.0 or true as strings is possible with escaping. 

##### Name, local name and prefix
When defined, names of properties and aliases have an optional prefix and a mandatory local name.  
Prefix comes from the dictionary into which they are defined.  
The dot ('.') character is the separator between prefix and local name. That separator can be used in prefix definition or local name, but escaping is necessary.


The next table gives examples of valid names.

<table>
   <thead>
      <tr><th>Input</th><th>Prefix</th><th>Local name</th></tr>
   </thead>
   <tbody>
      <tr><td>ABC</td><td></td><td>ABC</td></tr>
      <tr><td>A.BC</td><td>A</td><td>BC</td></tr>
      <tr><td>"A.BC"</td><td></td><td>A.BC</td></tr>
      <tr><td>"A.B".C</td><td>A.B</td><td>C</td></tr>
      <tr><td>"A.B"."C.D"</td><td>A.B</td><td>C.D</td></tr>
   </tbody>
</table>


##### Operators
There are 3 forms of operators:
- **short**: '&', '|', '!', '=', '!=', 
  '<', '<=', '!<', '!<=',
  '>', '>=', '!>', '!>=',
  '<:', '!<:',
  '->', '<->' and '>-<'.   
  They can be used without spaces around them. They are the easiest to type.
- **long**: 'and', 'or', 'not', '=', '!=',
  '<', '<=', '!<', '!<=',
   '>', '>=', '!>', '!>=',
  'in', 'not in',
  'imp', 'iff' and 'xor'   
  They are case insensitive and generally need spaces around them.
- **mathematical**: '&and;', '&or;', '&not;', '=', '&ne;',
  '<', '&le;', '&nlt;', '&nle;',
  '>', '&ge;', '&ngt;', '&nge;',
  '&isin;','&notin;',
  &rarr;, &harr; and &nharr;.   
  They can be used without spaces around them.

**Note:** S1000D does not support negative and inequality operators, so expressions that use
'!', '!=', '!<:', '<', '<=', '!<', '<!=', '>', '>=', '!>', '!>=', '->', '<->' or '>-<'
must be transformed to be S1000D compliant.   
In addition, use of '->' and '<->' is mainly intended for writing of assertions
 

**Warning:** the NotIn operator is a single operator, that must be written using
one convention.  
For example, it is not possible to write '! in' or 'not <:'

It is advised to use one convention in expressions. 


##### True and False
They are case insensitive. It is possible to use their mathematical symbols:
&top; and &perp; 

##### Empty set
Empty set can be represented as '{}' or as &empty;.  

**Note:** One can also define a set equivalent to an empty set using empty
ranges such as '{10~9}', but that should be avoided.

##### String values
Strings that do not contain spaces, are not operators or boolean values, can be
typed directly.   
Otherwise they must be escaped. Use of escaped strings is highly discouraged.  

So writing:
- "true" = "true" means:
the string property named 'true' equals the string value 'true'
- "true" = true means:
the boolean property named 'true' equals the boolean value true
- "true" means:
the boolean property named 'true' is true
- "A A" = "Value 1" means:
the string property named 'A A' equals the string value 'Value 1'.

##### Forbidden characters
'|' and '~' are reserved characters (S1000D) that cannot be used in string
values.   

##### Informal expressions
An informal expression must be delimited with $. If $ must be used inside an informal expression, it must be defined twice.

So, writing:
- \$it is hot\$ is the informal expression *it is hot*
- \$it costs more than 2\$\$\$ is the informal expression *it costs more than 2\$*


##### Examples
In the following examples, P is a property name 

###### String (enumerated or pattern) type property
- P = AAA
- P != AAA   
  P &ne; AAA   
  !(P = AAA)   
  &not;(P = AAA)
  
###### Boolean type property
They cans be used with (not) equal and (not) in operators, but it is much
simpler to use them directly. 

- P    
  P = true   
  P = &top;   
  P != false   
  P != &perp;   
  P &ne; false   
  P &ne; &perp;   
- !P   
  &not;P   
  P = false   
  P = &perp;
  
###### Integer type property
- P = 10
- P != 10   
  P &ne; 10
- P in {1, 3, 5\~10}   
  P <: {1, 3, 5\~10}   
  P &isin; {1, 3, 5\~10}
- P not in {2\~10}   
  P !<: {2\~10}   
  P &notin; {2\~10}   
  not (P in {2\~10})   
  not (P &isin; {2\~10})   
  &not; (P in {2\~10})   
  &not; (P &isin; {2\~10})

###### Real type property
- P = 10.0
- P != 10.0   

###### Complete examples
 - P=10 & Q<:{1\~10,20} or Q!=15
 - P=P1 -> (Q=Q1 -> R=R1)  

##### Formatting of expressions
Basic infix formatting of expressions is supported.  
The produced text can be parsed as an expression that is equivalent to the initial expression.

**Note:** For advanced formatting, see [Publication](#publication) module.

![Image](cdc-applic-expressions/src/main/javadoc/cdc/applic/expressions/doc-files/formatting.svg)

The infix formatting depends on 2 parameters: **spacing** and **symbol type**.

**Spacing** can be:
- **NARROW** to use as few spaces as possible.
- **WIDE** to use spaces even when they are not necessary.

**SymbolType** can be:
- **SHORT** to use short operators and symbols
- **LONG** to use English dictionary operators and symbols when possible
- **MATH** to use mathematical operators and symbols

See [Operators](#operators).

<table>
   <thead>
      <tr><th>Symbol</th><th>SHORT</th><th>LONG</th><th>MATH</th><th>Notes</th></tr>
   </thead>
   <tbody>
      <tr><td>Logical And</td><td>&amp;</td><td>and</td><td>&and; (U+2227)</td><td></td></tr>
      <tr><td>Logical Or</td><td>|</td><td>or</td><td>&or; (U+2228)</td><td></td></tr>
      <tr><td>Logical Negation</td><td>!</td><td>not</td><td>&not; (U+00AC)</td><td></td></tr>
      <tr><td>Equal</td><td>=</td><td>=</td><td>=</td><td></td></tr>
      <tr><td>Not Equal</td><td>!=</td><td>!=</td><td>&ne; (U+2260)</td><td>Use ne for LONG?</td></tr>
      <tr><td>Less Than</td><td>&lt;</td><td>&lt;</td><td>&lt;</td><td>Use lt for LONG?</td></tr>
      <tr><td>Less Or Equal</td><td>&lt;=</td><td>&lt;=</td><td>&le; (U+2264)</td><td>Use le for LONG?</td></tr>
      <tr><td>Greater Than</td><td>&gt;</td><td>&gt;</td><td>&gt;</td><td>Use gt for LONG?</td></tr>
      <tr><td>Greater Or Equal</td><td>&gt;=</td><td>&gt;=</td><td>&ge; (U+2265)</td><td>Use ge for LONG?</td></tr>
      <tr><td>Not Less Than</td><td>!&lt;</td><td>!&lt;</td><td>&#8814; (U+226E)</td><td>Use nlt for LONG?</td></tr>
      <tr><td>Neither Less Nor Equal</td><td>!&lt;=</td><td>!&lt;=</td><td>&#8816; (U+2270)</td><td>Use nle for LONG?</td></tr>
      <tr><td>Not Greater Than </td><td>!&gt;</td><td>!&gt;</td><td>&#8815; (U+226F)</td><td>Use ngt for LONG?</td></tr>
      <tr><td>Neither Greater Nor Equal</td><td>!&gt;=</td><td>!&gt;=</td><td>&#8817; (U+2271)</td><td>Use nge for LONG?</td></tr>
      <tr><td>Is In</td><td>&lt;:</td><td>in</td><td>&isin; (U+2208)</td><td></td></tr>
      <tr><td>Is Not In</td><td>!&lt;:</td><td>not in</td><td>&notin; (U+2209)</td><td></td></tr>
      <tr><td>Implies</td><td>-></td><td>imp</td><td>&rarr; (U+2192)</td><td></td></tr>
      <tr><td>Equivalent To</td><td>&lt;-></td><td>iff</td><td>&harr; (U+2194)</td><td></td></tr>
      <tr><td>Exclusive Or</td><td>&gt;-&lt;</td><td>xor</td><td>&nharr; (U+21AE)</td><td></td></tr>
      <tr><td>Logical True (Tautology)</td><td>true</td><td>true</td><td>&#8868; (U+22A4)</td><td>Only used for leaf nodes, not for property values</td></tr>
      <tr><td>Logical False (Contradiction)</td><td>false</td><td>false</td><td>&#8869; (U+22A5)</td><td>Only used for leaf nodes, not for property values</td></tr>
      <tr><td>Empty Set</td><td>{}</td><td>{}</td><td>&empty; (U+2205)</td><td></td></tr>
   </tbody>
</table>

#### Values, ranges and sets
Expressions are based on **Values**, **Ranges** and **Sets**.

Their technical type can be one of: **boolean**, **integer**, **real**, or **string**.  
The technical type of a value is recognized during parsing, without needing additional information:
- 1234, -1234, +1234 are recognized as integers
- 1234.56, -1234.45, 123.4e23, -123.4e-23 are recognized as reals
- true, false are recognized as booleans
- hello, "hello world", "123", "123.45", "false" are recognized as strings

**Note:** by using the parsing context, some recognition rules may be relaxed.

##### Set items
Values and Ranges are **SItems** (set items). They can be one of:
- Boolean value
- String value
- Integer value
- Integer range
- Real value
- Real range

**Note:** boolean ranges seem useless, and string ranges are problematic with computations.

**Note:** as currently implemented, use of negation with reals can raise issues. One may probably need to introduce fixed  resolution to make things easier and natural.

![Image](cdc-applic-expressions/src/main/javadoc/cdc/applic/expressions/doc-files/content-sitems.svg)

###### Order
Booleans, integers and reals are totally ordered, this order is natural and we don't see any need to define another order.  
That would create ambiguities and would be dangerous.
They are therefore partially ordered, and one can use inequality operators without risk or ambiguity.

Writing &pi; > 10, or &pi; <= 15.0, is perfectly clear for anyone.  
Writing &pi; &gt; &top; (which is equivalent to &bot; !) is probably less clear and useless. One needs to know that the boolean values are ordered as &bot; < &top;, which is probably less natural.

Strings are also associated to the lexicographic order, the one used for ranges.  
This could be semantically meaningful with patterns, but a technical solution must be found to allow some computations.

Enumerations are different. Even if they are defined with strings, they are not strings.  
The lexicographic order is usually meaningless for them.  
However it is possible to explicitly define a partial order with enumerations. See [Types](#types).

##### Domains
The concept of **Domain** has been introduced to share code for integers and reals. A Domain is associated to a totally ordered set of values.

![Image](cdc-applic-expressions/src/main/javadoc/cdc/applic/expressions/doc-files/content-domains.svg)

There are 2 concrete Domains: **IntegerDomain** and **RealDomain**.

**Note:** we could (should?) have defined a BooleanDomain.

##### Sets
All sets implement the **SItemSet** interface.

Classical set operations are provided, such as <a href="https://en.wikipedia.org/wiki/Union_(set_theory)">union</a>,
<a href="https://en.wikipedia.org/wiki/Intersection_(set_theory)">intersection</a>, ...

A **CountableSet** is a set of [countable](https://en.wikipedia.org/wiki/Countable_set) (discrete) values.  
Technically, reals (as implemented) are countable, semantically (mathematical reals) they are not.

A **CheckedSet** is a set whose elements all have the same technical type.

An **UncheckedSet** is a set whose elements may have different technical types.  
An **UncheckedSet** can be converted to a **CheckedSet** if all its elements have the same technical type. There are 2 cases:
- if it is empty, it can be converted to any technical type
- otherwise it can only be converted to the technical type of its elements.

![Image](cdc-applic-expressions/src/main/javadoc/cdc/applic/expressions/doc-files/content-sitemsets.svg)

#### Abstract Syntax Tree
Internally, an expression is represented as an AST.  
Parsing of expressions can only produce certain nodes, others are obtained
with transformations.

- value equality nodes have the form &pi; = &nu;, &pi; &ne; &nu;
- value inequality nodes have the form &pi; &lt; &nu;, &pi; &nlt; &nu;, &pi; &le; &nu;, &pi; &nle; &nu;,
 &pi; &gt; &nu;, &pi; &ngt; &nu;, &pi; &ge; &nu;, &pi; &nge; &nu;
- set nodes have the form &pi; &isin; &Omega;, &pi; &notin; &Omega;
- atom nodes can be directly transformed to boolean variables for SAT.
- ref nodes correspond to a boolean property (one can write &pi; instead of &pi; = &top;) or an alias.

![Image](cdc-applic-expressions/src/main/javadoc/cdc/applic/expressions/doc-files/nodes.svg)

##### Parsing nodes
Parsing nodes are the only nodes that parsing of an expression can produce.  
Other nodes are produced by transforming an AST.

![Image](cdc-applic-expressions/src/main/javadoc/cdc/applic/expressions/doc-files/nodes-parsing.svg)

**Warning:** currently, nary nodes are not produced by parser, but they could.  
An expression like A<sub>1</sub> or A<sub>2</sub> or ... or A<sub>n</sub>,
will produce a deep AST of binary nodes:
or(A<sub>1</sub>,or(A<sub>2</sub>,or(...,or(A<sub>n-1</sub>, A<sub>n</sub>)...).  
This may generate deep call stacks when visiting the AST with a recursive function.

##### Negative nodes
Negative nodes have a  direct dependency on negation and have a positive counterpart node.  
An expression that contains negation is not S1000D compliant.

**Note:** implication and equivalence are not considered as negative even if their definition
depends on negation. Is this expected?

**Note:** writing &pi; = &perp; is not considered as using negation, whilst &not;&pi; makes use of negation.

![Image](cdc-applic-expressions/src/main/javadoc/cdc/applic/expressions/doc-files/nodes-negative.svg)

##### Named nodes
Named nodes are associated to a name.

![Image](cdc-applic-expressions/src/main/javadoc/cdc/applic/expressions/doc-files/nodes-named.svg)


##### Visitors
Visitors are used to traverse AST and possibly convert them.  
There are 3 basic abstract visitors from which most implementations derive:
- `AbstractAnalyzer`: it is the base class of analyzers. They don't modify AST. Visiting a node returns void.
- `AbstractCollector`: it is a specialization of analyzer that can collect data.
- `AbstractConverter`: it is the base converter. Visiting a node returns another node.

![Image](cdc-applic-expressions/src/main/javadoc/cdc/applic/expressions/doc-files/visitors.svg)

Most visitors don't need to be used directly.
They are typically indirectly used through higher level API such as `simplification` or `publication`.  
However, a few visitors may be useful for developpers.
- `PredicateCounter` is an analyzer that counts nodes  that match a predicate.
- `PredicateMatcher` is an analyzer that checks if some nodes  match a predicate.
- `PredicateCollector` is a collector that collects nodes that match a predicate.
- `CollectAliasNames` is a collector that collects names of aliases that are
  directly used.
- `CollectPropertyNames` is a collector that collects names of properties that are
  directly used.
- `CollectNamedDItemsNames` is a collector that collects names of properties and aliases
  that are directly used.

**Note:** collecting all aliases or properties that are directly or *indirectly* used by an expression
is possible using `DictionaryDependencyCache`.

One can of course write any necessary visitor.


#### Dictionaries

A **Dictionary** defines:
- a **vocabulary**, with properties and aliases,
- and **rules**, with assertions, constraints and writing rules.

There are 2 kinds of dictionaries:
- **Registries**, which are root dictionaries used to define a vocabulary and types,
- **Policies**, which are used to restrict the vocabulary of another dictionary.

![Image](cdc-applic-dictionaries/src/main/javadoc/cdc/applic/dictionaries/doc-files/dictionaries.svg)

Dictionaries are characterized by:
- a **name**.
  Sibling dictionaries cannot have the same name.
- a **path**
- a **prefix** which identifies a namespace.  
  A namespace can not contain 2 `Types` with the same name, or 2 `NamedDItems` with the same name.
- a **context** expression used to restrict the vocabulary scope of parent dictionaries.


Several interfaces are used to characterize dictionary items:
- **DItem** is a marking interface of dictionary items that play a role in proofs.
- **DExpressed** is a specialization of DItems that are associated to an expression.
- **NamedDItem** is a specialization of DItems that have a name and an ordinal.

![Image](cdc-applic-dictionaries/src/main/javadoc/cdc/applic/dictionaries/doc-files/dictionary-items.svg)

##### Dictionaries relationships
The following diagram is a typical example of dictionaries and their relationships.

![Image](cdc-applic-dictionaries/src/main/javadoc/cdc/applic/dictionaries/doc-files/dictionary-relationships.svg)

It shows that:
- Registries have 0, 1 or more parents.
    - "/Global" registry has no parents.
    - "/Program 1" registry has 1 parent.
    - "/All Programs" registry has 3 parents.
- Policies have 1 parent.
    - "Program 1/Foo" policy has 1 parent, "/Program 1" registry.
    - "Program 1/Foo/Bar" policy has 1 parent, "/Program 1/Foo" policy.
- Registries have an optional prefix.
    - "/Global" registry has no prefix.
    - Prefix of "/Program 1" registry is "P1".
- Policies inherit their prefix from their parent.
    - Prefix of "/Program 1/Foo" policy is "P1".
    - Prefix of "/Program 1/Foo/Bar" policy is "P1".

In addition:
- The graph of dictionaries is a DAG.
- The graph of policies is a forest (set of trees) rooted in registries.
- The context of a dictionary that has no parents (necessarily a registry) cannot be set and equals "true".
- The context of a dictionary that has parents must be based on the vocabulary of its ancestors.


##### Registries
A **Registry** is a root dictionary. It is a set of **Types**, **Properties**, **Aliases**,
**Constraints** and **Assertions**.  
All Properties and Aliases are defined in Registries.

**Note:** inside a registry, the name of a property or alias must be unique.

The `path` of a registry is absolute and correspond to its name.  
The path of a registry named 'Foo' is '/Foo' (or '|Foo' or '\Foo')

It is possible to define a **prefix** for a registry.  
Use of prefix is optional when referencing a Property, Alias or Type defined locally. It is mandatory in other cases.

A registry can have any number of parent dictionaries.

##### Types
The `cdc-applic-dictionaries` module describes types by interfaces. An implementation is provided by `cdc-applic-dictionaries-impl`.

**Type** is the base interface of all types. There are 5 leaf types:
- **BooleanType**
- **IntegerType**
- **RealType**
- **EnumeratedType**
- **PatternType**

![Image](cdc-applic-dictionaries/src/main/javadoc/cdc/applic/dictionaries/doc-files/types-details.svg)

A **CountableType** is associated to a countable set that can be explicitly iterated.

BooleanType, IntegerType and EnumeratedType are countable types.  
RealType is not a countable type, as reals are not countable.  
PatternType is not a countable type, even if it should. At the moment we don't know any solution to iterate over all values of a pattern type or compute the number of those values.

A **PartiallyOrderedType** is a type that is associated to a partial order.

BooleanType, IntegerType and RealType are (naturally) partially ordered types.  
One can define a partial order for an EnumeratedType. The provided implementation of EnumeratedType supports this feature.  
PatternType does not support partial order, even if lexicographic order would be meaningful.


A **DomainedType** is a type whose domain can be explicitly defined by a set.  
All **CountableType**s are **DomainedType**s.

BooleanType, IntegerType, RealType and EnumeratedType are domained types.  
PatternType is not a domained type for the same reasons it is not a countable type.

A **ModifiableType** is a type whose definition may possibly change over time.

All types are modifiable types, except BooleanType.  
Provided implementation allows the user to indicate whether a modifiable type is really modifiable (isFrozen() returns false) or not (isFrozen() returns true).  
When a type is not frozen, we consider that its domain may be changed (mainly extended) in the future.
It is as if a reserve value was implicitly declared.  
This characteristic of types is used to avoid certain simplifications.

Let's take an example.  
Given
- &tau;<sub>frozen</sub> = {A, B, C}
- &pi;<sub>1</sub> : &tau;<sub>frozen</sub>
- &tau;<sub>not frozen</sub> = {A, B, C}, which is implicitly equivalent to &tau;' = {A, B, C, RESERVE}
- &pi;<sub>2</sub> : &tau;<sub>not frozen</sub>

One can say that:
- &pi;<sub>1</sub> &isin; {A, B, C} &equiv; &top;: &pi;<sub>1</sub> &isin; {A, B, C} **can be** replaced by &top;
- &pi;<sub>2</sub> &isin; {A, B, C} &nequiv; &top;: &pi;<sub>2</sub> &isin; {A, B, C} **cannot be** replaced by &top;
  because of the implicit RESERVE value.

**Note:** synonyms are not directly supported for enumerated values. One could use Aliases to create synonyms, but this is not necessarily easy.  
There is no way to create a type &pi; = {A, B, C, ...} and say, e.g., that A is a synonym of B.  
An enumerated type is associated to an `exactly one` constraint which is not compliant with synonyms.

##### Policies
A **Policy** is a restriction of a Registry or of another Policy.  
The set of Properties or Aliases that can be used in Policy is a subset of the
ones allowed in its parent dictionary.  
Some additional Assertions can be defined. Parent assertions are inherited.  
Finally, a Policy defines **Writing Rules**. The are used to harmonize the
way expressions are written.

The prefix of a policy is inherited form its parent.

The path of a policy is built from its name and names of its ancestors up to the first registry.

##### Properties
A **Property** corresponds to an S1000D Product Attribute or Condition.  
It has a name, a type, and an ordinal (used by some writing rules).

##### Aliases
An **Alias** is a named expression. The name can be used in place
of the expression.  
It has an ordinal used by some writing rules.

**Note:** Aliases don't exist in S1000D. If used, they must be replaced by
an equivalent expression to be S1000D compliant.

##### Assertions
An **Assertion** is a Predicate that must always evaluate to true.  
Assertions are used to link properties and their values. An assertion reduces
the set of valid expressions.

**Note:** in their generality, assertions don't exist in S1000D.

There are several types of assertions:
- `local` assertions
    - `context` assertions which are locally generated from context expression
    - `standard` assertions
        - `user defined` assertions, which are locally defined by users
        - `generated` assertions, which are locally generated by constraints
- `derived` assertions, which are automatically derived from ancestor dictionaries.
    - `context derived` assertions, which are automatically derived from contexts of ancestor dictionaries.
    - `standard derived` assertions, which are automatically derived from standard assertions of ancestor dictionaries.

![Image](cdc-applic-dictionaries/src/main/javadoc/cdc/applic/dictionaries/doc-files/assertions.svg)


##### Constraints
Creating consistent assertions may be difficult and error-prone.  
A **Constraint** is an abstract concept designed to create user-friendly means
(GUI) that can be used to define assertions.  
Ideally, all assertions should derive from constraints.

Additional constraints can be created if the provided ones don't fit needs.

A constraint has a type and parameters from which one or more assertions
are generated. When parameters of a constraint are modified, associated 
generated assertions are automatically created, updated or deleted.

The following constraints are currently provided:
- **Property Restriction** intended to limit the set of usable values of a property.
- **At Most One** to create an *at most one* constraint among N expressions.
- **At Least One** to create an *at least one* constraint among N expressions.
- **Exactly One** to create an *exactly one* constraint among N expressions.
- **Matrix** (*aka* **Tabular**) which is the most advanced constraint, used to link 2 disjoint
  sets of properties.

##### Writing Rules
They are used to harmonize the way expressions are written.  
Some default rules are provided, but additional ones can be added.  
Enabling or disabling a rule in a Policy is a user choice.  
Enabling a rule at a level normally implies this rule must also be enabled in children Policies.

A Registry disables all writing rules.  
Assertions don't have to comply with the writing rules of the policy they are defined in.  
However, they must be semantically valid.

Currently, the following default writing rules can check that:
- An expression **complies with prescribed usage of properties and aliases**.  
  The usage of a property or alias can be mandated, recommended, optional or forbidden.  

- An expression **does not contain removable nodes**.  
  For example, an expression such as 'A|B|A' should be replace by 'A|B'.

- An expression **does not make use of deep true**.  
  This can be used to forbid writing an expression such as 'A&true'.

- An expression **does not contain informal parts**.

- An expression **does not use implication or equivalence**.  
  Usage of those operators should generally be limited to writing assertions.

- An expression **does not use negation**.  
  This forbids use of implication, equivalence, not, operators.  
  That way, one can ensure the expression is directly S1000D compliant.
  
- An expression **does not use operators**.  
  This implies that only simple expressions can be used.
  
- An expression **is in Disjunctive Normal Form**.  

- An expression **is satisfiable**.  
  If this is not the case, the expression designate something that does not exist.

- **Names (of properties and aliases) are used in the prescribed order** in an expression.  
  This is useful to harmonize writing of expressions.  
  The ordinal attribute is used for that purpose.
  
- **Values in sets** of an expression **are normalized**.  
  This is useful to harmonize writing of expressions.

- An expression is such that **all properties of the policy have one and only one valid value**.


##### S1000D extensions
Certain types are dedicated to S1000D support:
- **S1000DIdentified** is an interface implemented by classes that contain an S1000D identifier.
- **S1000DProductIdentifier** is an enumeration with those values:
    - **PRIMARY** to declare a `primary` `product attribute`.
    - **SECONDARY** to declare a `secondary` `product attribute`.
    - **NONE** to declare a `product attribute` that is neither `primary` nor `secondary`.
    - **NOT_APPLICABLE** for properties that are not product attributes.
- **S1000DProperty** is a specialization of `Property` giving access to S1000D specific attributes.
- **S1000DPropertyType** is an enumeration with those values:
    - **PRODUCT_ATTRIBUTE** when the `property` is a `product attribute`.
    - **PRODUCT_CONDITION** when the `property` is a `condition` characterizing a `product`.
    - **EXTERNAL_CONDITION** when the `property` is a `condition` characterizing the `product environment`.
    - **UNDEFINED** when the `property` is meaningless for S1000D.
- **SS1000DType** is a specialization of `Type` giving access to S1000D specific attributes.

![Image](cdc-applic-dictionaries/src/main/javadoc/cdc/applic/dictionaries/doc-files/dictionaries-s1000d.svg)

**Note:** the distinction between `product condition` and `external condition` is used to support PCT generation.

#### Conversions
This module contains code to convert expressions between 2 registries.

This can be used to ensure compatibility with legacy data.

Conversion is parameterized using 3 layers:
- [bindings of Types](#types-bindings),
- [bindings of Items](#items-bindings),
- and [bindings of Registries](#registries-bindings).

##### Types bindings
The first level of conversions is based on bindings of types.

A first general concept is that of `BindingRole` which can have 2 values:
- **SOURCE**,
- and **TARGET**.

A second related concept is `BindingDirection` with 2 values:
- **FORWARD**, from `SOURCE` to `TARGET`,
- and **BACKWARD**, from `TARGET` to `SOURCE`.

![Image](cdc-applic-dictionaries/src/main/javadoc/cdc/applic/dictionaries/doc-files/bindings-types.svg)

A `TypesBinding` is used to convert values and sets of a type to values and sets of another type.

**Note:** a `TypesBinding` may map a subset of the values of the `SOURCE` or `TARGET` type.

The following table gives an overview of available type bindings.

<table style="text-align:center;">
   <tr><th colspan="2" rowspan="2"></th><th colspan="5">Target type</tr>
   <tr><th>B</th><th>I</th><th>R</th><th>E</th><th>P</th></tr>
   <tr><th rowspan="5" style="writing-mode:vertical-lr;text-orientation:mixed;">Source type</th><th>B</th><td>&#10003;</td><td>&#10003;</td><td></td><td>&#10003;</td><td></td></tr>
   <tr><th>I</th><td>&#10003;</td><td>&#10003;</td><td></td><td>&#10003;</td><td></td></tr>
   <tr><th>R</th><td></td><td></td><td>&#10003;</td><td></td><td></td></tr>
   <tr><th>E</th><td>&#10003;</td><td>&#10003;</td><td></td><td>&#10003;</td><td></td></tr>
   <tr><th>P</th><td></td><td></td><td></td><td></td><td>&#10003;</td></tr>
</table>

##### Items bindings
The second level of conversions is based on bindings of `properties` and `aliases`.

![Image](cdc-applic-dictionaries/src/main/javadoc/cdc/applic/dictionaries/doc-files/bindings-items.svg)

<table style="text-align:center;">
   <tr><th colspan="2" rowspan="2"></th><th colspan="2">Target item</tr>
   <tr><th>P</th><th>A</th></tr>
   <tr><th rowspan="2" style="writing-mode:vertical-lr;text-orientation:mixed;">Source item</th><th>P</th><td>&#10003;</td><td></td></tr>
   <tr><th>A</th><td></td><td>&#10003;</td></tr>
</table>

**Note:** the `types binding` associated to a `properties binding` must be compliant with types of properties.

**Note:** a `property` may be associated to several `properties bindings`.

##### Registries bindings
The third level of conversions is based on bindings of registries.  
A `RegistriesBinding` binds the properties and aliases of 2 registries.

![Image](cdc-applic-dictionaries/src/main/javadoc/cdc/applic/dictionaries/doc-files/bindings-registries.svg)

##### Registry Converter
A `RegistryConverter` can convert expressions using a `RegistriesBinding`.  
It is possible to chain conversions and to revert roles and directions.

![Image](cdc-applic-dictionaries/src/main/javadoc/cdc/applic/dictionaries/doc-files/registry-converter.svg)

#### Clauses
- A boolean property is naturally converted to an atom.
- An integer or real property is converted to atoms that represent used ranges.
  An 'exactly one' clause is also created.  
  This conversion depends on values used in current resolution context.
- Each value of an enumerated property is converted to an atom.
  An 'exactly one' clause is also created.
- Each used value of a pattern type is converted to an atom.
  An 'exactly one' clause is also created.  
  This conversion depends on values used in current resolution context.


#### Proofs
This module contains code related to proofs.
Proofs are done using a SAT solver. For that, expressions are transformed to Clauses.  
Doing a proof can be done including or ignoring assertions.  
Assertions that should be used are automatically determined.

![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/prover.svg)

##### Conventions
- D is the set of points in the space of properties.  
- A point of D  is valid when all assertions are true at that point: &and;A<sub>k</sub>  
- A point of D is invalid when at least one assertion is false at that point: &not;&and;A<sub>k</sub>  
- E is the subset of D for which E is true  
- &not;E is the subset D for which E is false.  
- A = A<sub>1</sub> &and; A<sub>2</sub> &and; ..., the conjunction of all assertions  

![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/conventions.svg)


##### is sometimes (maybe always) true(E)
The set of valid points of E is not empty  
This is the fundamental proof that corresponds to SAT(E &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/is-sometimes-true.svg)

##### is never true(E)
The set of valid points of E is empty  
This is equivalent to &not;SAT(E &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/is-never-true.svg)

##### is always true(E)
This is equivalent to SAT(E&and;A) &and; &not;SAT(&not;E&and;A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/is-always-true.svg)

##### is sometimes (maybe always) false(E)  
This is equivalent to SAT(&not;E &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/is-sometimes-false.svg)

##### is never false(E)
This is equivalent to &not;SAT(&not;E &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/is-never-false.svg)

##### is always false(E)
This is equivalent to SAT(&not;E &and; A) &and; &not;SAT(E &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/is-always-false.svg)

##### are always equivalent(E1, E2)
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/are-always-equivalent.svg)

##### intersects(E1, E2)
This is equivalent to SAT(E1 &and; E2 &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/intersects.svg)

##### contains(E1, E2)
This is equivalent to &not;SAT(&not;E1 &and; E2 &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/contains.svg)

##### contains non empty(E1, E2)
This is equivalent to &not;SAT(&not;E1 &and; E2 &and; A) &and; SAT(E1 &and; E2 &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/contains-non-empty.svg)

##### matching(E)
- NEVER  
This is equivalent to &not;SAT(E &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/matching-never.svg)

- SOMETIMES (but not always)  
This is equivalent to SAT(E &and; A) &and; SAT(&not;E &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/matching-sometimes.svg)

- ALWAYS  
This is equivalent to SAT(E &and; A) &and; &not;SAT(&not;E &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/matching-always.svg)

##### projected matching(E1, E2)
- NEVER  
This is equivalent to &not;SAT(E1 &and; E2 &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/projected-matching-never.svg)
    
- SOMETIMES (but not always)  
This is equivalent to SAT(E1 &and; E2 &and; A) &and; SAT(E1 &and; &not;E2 &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/projected-matching-sometimes.svg)
    
- ALWAYS  
This is equivalent to SAT(E1 &and; E2 &and; A) &and; &not;SAT(E1 &and; &not;E2 &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/projected-matching-always.svg)

##### Constraints

<table>
  <tr><th>Case</th><th>&gt;=1</th><th>&lt;=1</th><th>=1</th></tr>
  <tr><td><img src="cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/always-x-one-000.svg" width="50" height="50"/></td><td>No</td><td>Yes</td><td>No</td></tr>
  <tr><td><img src="cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/always-x-one-001.svg" width="50" height="50"/></td><td>Yes</td><td>Yes</td><td>Yes</td></tr>
  <tr><td><img src="cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/always-x-one-010.svg" width="50" height="50"/></td><td>Yes</td><td>No</td><td>No</td></tr>
  <tr><td><img src="cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/always-x-one-011.svg" width="50" height="50"/></td><td>Yes</td><td>No</td><td>No</td></tr>
  <tr><td><img src="cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/always-x-one-100.svg" width="50" height="50"/></td><td>Yes</td><td>Yes</td><td>Yes</td></tr>
  <tr><td><img src="cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/always-x-one-101.svg" width="50" height="50"/></td><td>Yes</td><td>No</td><td>No</td></tr>
  <tr><td><img src="cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/always-x-one-110.svg" width="50" height="50"/></td><td>Yes</td><td>No</td><td>No</td></tr>
  <tr><td><img src="cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/always-x-one-111.svg" width="50" height="50"/></td><td>Yes</td><td>No</td><td>No</td></tr>
</table>

##### always at least one(E1,...)
This is equivalent to SAT((E1 &or; ...) &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/always-at-least-one.svg)

##### always at most one(E1,...)
This is equivalent to SAT(<font size="+3">&and;</font><sub>i&lt;j</sub> &not;(Ei &and; Ej) &and; A)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/always-at-most-one.svg)

##### always exactly one(E1,...)  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/always-exactly-one.svg)

##### always at least one in context(C, E1,...)
All valid points of context are valid points of the union of expressions.  
![Image](cdc-applic-proofs/src/main/javadoc/cdc/applic/proofs/doc-files/always-at-least-one-in-context.svg)

##### always at most one in context(C, E1,...)
TODO

##### always exactly one in context(C, E1,...)
TODO

### Utilities modules

#### Charts
These modules contain code to build a graphical representation of a policy:
- `charts` which contains the API;
- and `charts-core` which contains implementation of algorithms.

This can help find issues when a policy contains contradictions.  

![Image](cdc-applic-charts/screenshots/policy-with-types.png)

##### DictionaryToGv
The command-line tool **DictionaryToGv** has the following options:

````
USAGE
DictionaryToGv [--all-policies] [--args-file <arg>] [--args-file-charset <arg>] [--dictionary <arg>] [--directed | --undirected]
                     [--engine-circo | --engine-dot | --engine-fdp | --engine-neato | --engine-sfdp | --engine-twopi]      [--font
                     <arg>] [--format-bmp] [--format-canon] [--format-cmapx] [--format-dot] [--format-dot_json] [--format-eps]
                     [--format-exr] [--format-fig] [--format-gif] [--format-gv] [--format-ico] [--format-imap] [--format-jp2]
                     [--format-jpg] [--format-json] [--format-json0] [--format-pdf] [--format-pic] [--format-plain]
                     [--format-plain-ext] [--format-png] [--format-ps] [--format-ps2] [--format-psd] [--format-svg] [--format-svgz]
                     [--format-tga] [--format-tiff] [--format-xdot] [--format-xdot_json] [--group] [-h] [--help-width <arg>]
                     [--horizontal] [--long-symbols | --math-symbols | --short-symbols] [--math-font <arg>]  [--narrow-spacing |
                     --wide-spacing] --output-dir <arg> [--path <arg>] --repository <arg>  [--show-descritions] [--show-issues]
                     [--show-types]

OPTIONS
    --all-policies              If enabled, generate graphs for all policies.
    --args-file <arg>           Optional name of the file from which options can be read.
                                A line is either ignored or interpreted as a single argument (option or value).
                                A line is ignored when it is empty or starts by any number of white spaces followed by '#'.
                                A line that only contains white spaces is an argument.
                                A comment starts by a '#' not following a '\'. The "\#" sequence is read as '#'.
    --args-file-charset <arg>   Optional name of the args file charset.
                                It may be used if args file encoding is not the OS default file encoding.
    --dictionary <arg>          Path of the optional dictionary to use. If none is passed, uses the first registry.
                                It has the form '/registryName[/policyName]*'.
    --directed                  Create a directed graph.
    --engine-circo              Use circo layout engine.
    --engine-dot                Use dot layout engine.
    --engine-fdp                Use fdp layout engine.
    --engine-neato              Use neato layout engine.
    --engine-sfdp               Use sfdp layout engine.
    --engine-twopi              Use twopi layout engine.
    --font <arg>                Optional name of the font to use for standard text. (default: Arial).
    --format-bmp                Generate bmp file(s).
    --format-canon              Generate canon file(s).
    --format-cmapx              Generate cmapx file(s).
    --format-dot                Generate dot file(s).
    --format-dot_json           Generate dot_json file(s).
    --format-eps                Generate eps file(s).
    --format-exr                Generate exr file(s).
    --format-fig                Generate fig file(s).
    --format-gif                Generate gif file(s).
    --format-gv                 Generate gv file(s).
    --format-ico                Generate ico file(s).
    --format-imap               Generate imap file(s).
    --format-jp2                Generate jp2 file(s).
    --format-jpg                Generate jpg file(s).
    --format-json               Generate json file(s).
    --format-json0              Generate json0 file(s).
    --format-pdf                Generate pdf file(s).
    --format-pic                Generate pic file(s).
    --format-plain              Generate plain file(s).
    --format-plain-ext          Generate plain-ext file(s).
    --format-png                Generate png file(s).
    --format-ps                 Generate ps file(s).
    --format-ps2                Generate ps2 file(s).
    --format-psd                Generate psd file(s).
    --format-svg                Generate svg file(s).
    --format-svgz               Generate svgz file(s).
    --format-tga                Generate tga file(s).
    --format-tiff               Generate tiff file(s).
    --format-xdot               Generate xdot file(s).
    --format-xdot_json          Generate xdot_json file(s).
    --group                     If enabled and dot engine is used, nodes are grouped by categories (types, properties, aliases and
                                assertions).
 -h,--help                      Prints this help and exits.
    --help-width <arg>          Optional help width (default: 74).
    --horizontal                If enabled and dot engine is used, horizontal layout is used.
    --long-symbols              Use long symbols for expressions.
    --math-font <arg>           Optional name of the font to use for expressions. (default: Arial).
    --math-symbols              Use math symbols for expressions. (default)
    --narrow-spacing            Use narrow spacing for expressions. (default)
    --output-dir <arg>          Name of mandatory output directory where files are generated.
    --path <arg>                Optional directory(ies) where the Graphviz binaries can be found.
    --repository <arg>          Name of the mandatory input XML repository file.
    --short-symbols             Use short symbols for expressions.
    --show-descritions          If enabled, descriptions are displayed.
    --show-issues               If enabled, issues are displayed.
    --show-types                If enabled, types are displayed.
    --undirected                Create an undirected graph. (default)
    --wide-spacing              Use wide spacing for expressions.
````

#### Consistency
These modules contain code to describe a `configured problem` and analyze it:
- `consistency` which contains the API;
- `consistency-impl` (**Deprecated**) which provides some data implementations and IO utilities;
- and `consistency-core` (**Deprecated**) which contains implementation of algorithms.

**Note:** with deprecation of `consistency-impl` and `consistency-core`, all code is now in `consistency`.

##### Configured problem
**TODO** update
 
A configured problem is described by the **ConsistencyData** interface.  
It is viewed as a composition DAG (Directed Acyclic Graph) of **Node**s, enriched with navigation links.

![Image](cdc-applic-consistency/src/main/javadoc/cdc/applic/consistency/doc-files/consistency-data-model.svg)


**Node**s have 2 concrete forms:
- **Block**s that can have any number of parent Blocks and any number of children Nodes.
- **Reference**s that have one parent Block and reference (target) another Block.  
  The parent link is part of the DAG, the target (navigation) link is not.

Blocks that have no parent are `roots`.

**Node**s are abstract and  characterized by:
- an `identifier` used internally to designate and retrieve the node;
- a `display label` intended for end users;
- a `local applicability`. All applicabilities must be based on the same Registry;
- a `dictionary` the applicability must comply with.

In addition, a **Block** is characterized by:
- a `composition` that described how sub-blocks should be considered from an applicability point of view;
- its `parents` Blocks; 
- its `children` Nodes.

And a **Reference** is also characterized by:
- its `target` Block;
- and its `parent` Block.

**Note:** at the API level (`consistency` module), the DAG is just a concept that can be explored,
but there is no imposed interface or class, except `ConsitencyData` interface and `Composition` enumeration.  
The `consistency-impl` module contains a possible implementation, but any other implementation can be provided.  
It is possible implement `ConsistencyData` to give access to existing code without any change to it.

A concrete problem could look like this:

![Image](cdc-applic-consistency/src/main/javadoc/cdc/applic/consistency/doc-files/consistency-data-model-example.svg)

The DAG has 8 nodes, `b1` to `b6`, `r1` and `r2`, 2 roots, and 9 edges.
- `b1` is root and has 3 children, `b2`, `b3` and `r1`;
- `b2` has 2 parents, `b1` and `b4`, and 2 children, `b3` and `b5`;
- `b3` has 2 parents, `b1` and `b2`, and 1 child, `b6`;
- `b4` is root and has 2 children, `b2` and `r2`;
- `b5` has 1 parent, `b2`, and 1 child, `b6`;
- `b6` has 2 parents, `b3` and `b5`, and no children;
- `r1` belongs to `b1` and targets `b4`;
- `r2` belongs to `b4` and targets `b1`.

##### Local vs. effective applicability

In the description of a configured problem, the applicability of a node is a `local applicability`.  
The `effective applicability` of a node is computed as follows:
- if the node has no parents (it is a root block), it is its `local applicability`.
- otherwise, it is the conjunction of its `local applicability` and of the `effective applicabilities` of its parents.

##### Composition modes
There are 4 composition modes:
- **ANY** used to indicate that the composition can be ignored.
- **AT_LEAST_ONE** used to indicate that, in the context of the parent block effective applicability,
  there is always at least one child block applicability that is valid.  
  see [Always at least one in context](#always-at-least-one-in-contextc-e1).
- **AT_MOST_ONE** used to indicate that, in the context of the parent block effective applicability,
  there is always at most one child block applicability that is valid.  
  see [Always at most one in context](#always-at-most-one-in-contextc-e1).
- **EXACTLY_ONE** used to indicate that, in the context of the parent block effective applicability,
  there is always exactly one child block applicability that is valid.  
  see [Always exactly one in context](#always-exactly-one-in-contextc-e1).

##### Checked rules
The following rules are checked:
- Existence of used dictionaries.
- Use of a common registry by all dictionaries.
- Existence of all targets.
- Compliance of the local applicability of a node with the effective applicability of its parents blocks.
- Compliance of the effective applicability of a reference with the effective applicability of its target blocks.
- Compliance of the composition rule of a parent block with the applicability of its children blocks.  
  Reference children are ignored by that check. If necessary, one just needs to wrap them in a block.

##### Examples
This section contains examples of possible mappings of real-life configured problems to the proposed abstraction.

###### Mapping of S1000D Data Modules
A Data Module (DM) having an XML structure, it can be viewed (from the applicability perspective)
as a tree of blocks, each having an optional applicability.  
An applicability is a restriction, so the absence of applicability is equivalent to defining a `true` applicability.

In addition text can contain links.

In that case, mapping comes naturally:
- Each XML element that is not an `applic` become a block.
- Each XML `applic` element becomes the applicability of the appropriate block.
- All links become references.
- All blocks, except root blocks, have one parent block.

Of course, only blocks that contain a meaningful applicability, or that are referenced, are useful.  
The block that corresponds to the DM should have an applicability.
If the DM does not have it, then it is implicit, and it should be made explicit.

![Image](cdc-applic-consistency/src/main/javadoc/cdc/applic/consistency/doc-files/consistency-examples-s1000d.svg)

![Image](cdc-applic-consistency/src/main/javadoc/cdc/applic/consistency/doc-files/consistency-examples-s1000d-nodes.svg)

###### Mapping of S3000L MTA Tasks
TODO


#### Factorization
These modules contain code that can be used to help handling of envelope (150%) data sets:
- `factorization` which contains the API;
- and `factorization-core` which contains implementation of algorithms.

The following algorithms are provided:
- [Cutter](#cutter)
- [Extender](#extender)
- [Merger](#merger)
- [Partitioner](#partitioner)
- [Reducer](#reducer)
- [Splitter](#splitter)

Those algorithms are essentially based on the [adapter](https://en.wikipedia.org/wiki/Adapter_pattern) design pattern:
they can be used on any existing code without modifying it.

##### Basic concepts
Those algorithms are related to objects that have an `applicability`.  
In some cases, the object content (what is called its `definition`) must be taken into account.
The `definition` of an object depends on the context. Typically, it shall exclude its identification.

2 adaptation interfaces are defined to extract or manipulate `applicability` and `definition`:
- `ApplicabilityExtractor` is used to extract the `applicability` of an object.
- `DefinitionAnalyzer` is used to compare the `definition`s of 2 objects.  
  In addition, to optimize certain algorithms, the `definition hash` of an object can be extracted.  
  Common hypotheses on hash hold:
    - 2 objects that have the same definition must have the same definition hash.
    - 2 objects that have different definitions can have the same definition hash.

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-basic-concepts.svg)

Results may be sent back to caller through `handlers`.
In that case, the algorithm behaves like an [active/internal iterator](https://en.wikipedia.org/wiki/Iterator).

Specific handlers are defined when necessary. They derive from `Handler` base interface that declares common methods. 

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-handler.svg)

`Factorizationfeatures` is used to configure behavior of algorithms.

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-features.svg)

Possible hints are:
- **SIMPLIFY** to simplify produced expressions.
- **CHECK** to enable some heavy checks.

**Note:** in all cases, prover, simplifier and dictionaries are passed to constructors of implementations.

The `formatting` data is used to format produced expressions.

##### Cutter
Given 
- a set of N input objects O<sub>1</sub> to O<sub>N</sub>, each having an applicability E(O<sub>i</sub>),
- and a target applicability E<sub>target</sub>,

generate, for each object O<sub>i</sub>, one of 4 possible events:
- **EXCLUDE_OBJECT_APPLICABILITY**:
  object's applicability is excluded from target applicability.  
  E(O<sub>i</sub>) &ne; &empty; &and; E(O<sub>i</sub>) &cap; E<sub>target</sub> = &empty;  
- **INCLUDE_OBJECT_APPLICABILITY**:
  object's applicability is included in target applicability.  
  E(O<sub>i</sub>) &ne; &empty; &and; E(O<sub>i</sub>) &sube; E<sub>target</sub>  
- **CUT_OBJECT_APPLICABILITY**:
  object's applicability overlaps target applicability.  
  E(O<sub>i</sub>) &ne; &empty; &and; E(O<sub>i</sub>) &cap; E<sub>target</sub> &ne; &empty;
  &and; E(O<sub>i</sub>) &nsube; E<sub>target</sub>  
  In that case, the `excluded` and `included` applicabilities are computed.
- **DEGENERATE_OBJECT_APPLICABILITY**:
  object's applicability is degenerate.  
  E(O<sub>i</sub>) = &empty;

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-cutter.svg)

The following diagrams illustrate the 4 possible situations.

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-cutter-example.svg)

##### Extender
Given 
- a set of N input objects O<sub>1</sub> to O<sub>N</sub>, each having an applicability E(O<sub>i</sub>),
- and an extension applicability E<sub>extension</sub>,

compute, for each object O<sub>i</sub> the applicability E(O<sub>i</sub>) &cup; E<sub>extension</sub>

This generates the following events:
- **CHANGE_OBJECT_APPLICABILITY**: the applicability of the input object is extended.  
  This is the case when E<sub>extension</sub> &nsub; E(O<sub>i</sub>)
- **IGNORE_OBJECT**: the applicability of input object is unchanged.  
  This is the case when E<sub>extension</sub> &sube; E(O<sub>i</sub>)

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-extender.svg)

The following diagrams illustrate the possible scenarios.

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-extender-example-1.svg)

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-extender-example-2.svg)

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-extender-example-3.svg)
##### Merger
Given 
- a set of N input objects O<sub>in,1</sub> to O<sub>in,N</sub>, each having an applicability E(O<sub>in,i</sub>) and a definition D(O<sub>in,i</sub>)

produce the minimal set of P output objects O<sub>out,1</sub> to O<sub>out,P</sub>, each having an applicability E(O<sub>out,i</sub>) and a definition D(O<sub>out,i</sub>), so that:
- &forall; i &isin; [1..P], &forall; j &isin; [1..P], i &ne; j, D(O<sub>out,i</sub>) &ne; D(O<sub>out,j</sub>)
- &forall; i &isin; [1..N], &exist;! j &isin; [1..P], D(O<sub>in,i</sub>) = D(O<sub>out,j</sub>) &and; E(O<sub>in,i</sub>) &sube; E(O<sub>out,j</sub>)
- &forall; i &isin; [1..P], &forall; x &isin; E(O<sub>out,i</sub>), &exist; j &isin; [1..N], D(O<sub>out,i</sub>) = D(O<sub>in,j</sub>) &and; x &isin; E(O<sub>in,j</sub>)

This generates the following events:
- **CHANGE_OBJECT_APPLICABILITY**: the applicability of the input object is extended.  
  Its definition is the same as the definition of another object.
- **REMOVE_OBJECT**: the input object is useless and should be removed.  
  Its definition is the same as the definition of another object.

So, for each set of objects sharing the same definition, one object is kept, its applicability
  is extended to the union of applicabilities, and other objects are removed.

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-merger.svg)

The following diagram illustrates a simple scenario.

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-merger-example.svg)

##### Partitioner
Given 
- a set of N input expressions E<sub>in,1</sub> to E<sub>in,N</sub>,

produce the minimal set of expressions E<sub>out,1</sub> to E<sub>out,P</sub>,
so that:
- &xcup;<sub>i</sub> E<sub>in,i</sub> = &xcup;<sub>j</sub> E<sub>out,j</sub>
- &forall; i &isin; [1..P], E<sub>out,i</sub> &ne; &empty;
- &forall; i &isin; [1..P], &forall; j &isin; [1..P], i &ne; j, E<sub>out,i</sub> &cap; E<sub>out,j</sub> = &empty;
- &forall; i &isin; [1..P], &exist; j &isin; [1..N], E<sub>out,i</sub> &sube; E<sub>in,j</sub>

**Note:** with N input expressions, P &le; 2<sup>N</sup>-1. The algorithm has an exponential complexity.  
To help it, some knowledge on input expressions can be passed to it.

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-partitioner.svg)

For example, with 2 input expressions E<sub>in,1</sub> and E<sub>in,2</sub>,
we will generally obtain 3 output expressions E<sub>out,1</sub>, E<sub>out,2</sub> and E<sub>out,3</sub>.

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-partitioner-example.svg)

The inclusion relationship between output expressions and input ones is also generated.

##### Reducer
Given 
- a set of N input objects O<sub>1</sub> to O<sub>N</sub>, each having an applicability E(O<sub>i</sub>),
- and a reduction applicability E<sub>reduction</sub>,

compute, for each object O<sub>i</sub> the applicability E(O<sub>i</sub>) &cap; E<sub>reduction</sub>

This generates the following events:
- **CHANGE_OBJECT_APPLICABILITY**: the applicability of the input object is reduced.  
  This is the case when  E(O<sub>i</sub>) &cap; E<sub>reduction</sub> &ne; &empty; &and; E(O<sub>i</sub>) &nsube; E<sub>reduction</sub>
- **REMOVE_OBJECT**: the applicability of input object is empty.  
  This is the case when  E(O<sub>i</sub>) &cap; E<sub>reduction</sub> = &empty;
- **IGNORE_OBJECT**: the applicability of input object is unchanged.  
  This is the case when  E(O<sub>i</sub>) &sube; E<sub>reduction</sub>

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-reducer.svg)

The following diagrams illustrate the 3 possible scenarios.

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-reducer-example-1.svg)

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-reducer-example-2.svg)

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-reducer-example-3.svg)


##### Splitter
Given 
- a set of N input objects O<sub>in,1</sub> to O<sub>in,N</sub>, each having an applicability E(O<sub>in,i</sub>),
  those applicabilities being disjoint,
- and a target applicability E<sub>target</sub>,

produce the minimal set of P output objects O<sub>out,1</sub> to O<sub>out,P</sub> so that:
- &exist;! j<sub>target</sub> &isin; [1..P] / E(O<sub>out,j<sub>target</sub></sub>) = E<sub>target</sub>
- &forall; j &isin; [1..P], j &ne; j<sub>target</sub>, &exist; i &isin; [1..N], E(O<sub>out,j</sub>) &sube; E(O<sub>in;i</sub>) &and; E(O<sub>out,j</sub>) &cap; E<sub>target</sub> = &empty;
- E<sub>target</sub> &cup; &xcup;<sub>i</sub> E(O<sub>out,i</sub>) = E<sub>target</sub> &cup; &xcup;<sub>j</sub> E(O<sub>in,j</sub>)
- reuse as much as possible input objects

This generates the following events:
- **KEEP_OBJECT**: the input object is kept unchanged.  
  Its applicability has an empty intersection with target applicability.
- **REDUCE_OBJECT_APPLICABILITY**: the input object is kept, but is applicability is reduced.  
  Its applicability has a non-empty intersection with target applicability and is not included in target applicability.
- **CREATE_OBJECT**: a new object must be created. There are not enough input objects.  
  This can happen at most once.
- **REUSE_OBJECT**: the input object is reused and its new applicability is the target applicability.  
  Its initial applicability was included in target applicability.  
  This can be a merge of a remove/create pair of events, and can happen at most once.
- **REMOVE_OBJECT**: the input object is useless and should be removed.  
  Its initial applicability was included in target applicability.  

**Note:** CREATE_OBJECT and REUSE_OBJECT are exclusive.

So:
- all input objects whose applicability does not intersect target applicability are kept unchanged.
- all input objects whose applicability intersect target applicability without being included in it are reduced.
- If there are input objects whose applicability is included in target applicability:
    - one of them is reused
    - the others are removed
- otherwise, an object is created.

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-splitter.svg)

The following diagrams illustrate simple scenarios.

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-splitter-example-1.svg)

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-splitter-example-2.svg)

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-splitter-example-3.svg)

![Image](cdc-applic-factorization/src/main/javadoc/cdc/applic/factorization/doc-files/factorization-splitter-example-4.svg)

#### Mountability
These modules contain code to transform interchangeability into mountability:
- `mountability` which contains the API;
- `mountability-impl` which provides some data implementations and IO utilities;
- and `mountability-core` which contains implementation of algorithms.

##### Mountability data model
Mountability is based on those concepts:
- **UsePoint** wich describes a location of a `system of interest` where different parts can be installed.
- **Variant** which describes a part that can be installed at a `UsePoint`.  
  It is also associated to the state of the `system of interest`.
- **Interchangeability** which describes the possibility to replace a `Variant` by another one at a given `UsePoint`.

In the general case, at a given `UsePoint`, one should define interchangeability for all pairs of `Variants`.  
One considers that a Variant can always replace itself.  
So, with N `Variants`, that means N<sup>2</sup> - N pairs.

<table>
<tr><th></th><th>V<sub>1</sub></th><th>V<sub>2</sub></th><th>...</th><th>V<sub>i</sub></th><th>...</th><th>V<sub>N</sub></th></tr>
<tr><th>V<sub>1</sub></th><td>&#10003;</td><td>?</td><td></td><td>?</td><td></td><td>?</td></tr>
<tr><th>V<sub>2</sub></th><td>?</td><td>&#10003;</td><td></td><td>?</td><td></td><td>?</td></tr>
<tr><th>...</th><td></td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><th>V<sub>i</sub></th><td>?</td><td>?</td><td></td><td>&#10003;</td><td></td><td>?</td></tr>
<tr><th>...</th><td></td><td></td><td></td><td></td><td></td><td></td></tr>
<tr><th>V<sub>N</sub></th><td>?</td><td>?</td><td></td><td>?</td><td></td><td>&#10003;</td></tr>
</table>

Practically, a common simplification consists in considering that `Variants` are ordered (from oldest to newest), and that N-1 pairs are sufficient.

The `Interchangeability` enumeration contains 3 values, the fourth possibility being currently ignored.

<table>
<tr><th>New<br>can replace<br>Old</th><th>Old<br>can replace<br>New</th><th colspan="2">Interchangeability</th></tr>
<tr><td>&#10007;</td><td>&#10007;</td><td>NOT_INTERCHANGEABLE</td><td><img height="60" src="cdc-applic-mountability/src/main/javadoc/cdc/applic/mountability/doc-files/interchangeability-ff.svg"></td></tr>
<tr><td>&#10007;</td><td>&#10003;</td><td></td><td><img height="60" src="cdc-applic-mountability/src/main/javadoc/cdc/applic/mountability/doc-files/interchangeability-ft.svg"></td></tr>
<tr><td>&#10003;</td><td>&#10007;</td><td>ONE_WAY</td><td><img height="60" src="cdc-applic-mountability/src/main/javadoc/cdc/applic/mountability/doc-files/interchangeability-tf.svg"></td></tr>
<tr><td>&#10003;</td><td>&#10003;</td><td>TWO_WAYS</td><td><img height="60" src="cdc-applic-mountability/src/main/javadoc/cdc/applic/mountability/doc-files/interchangeability-tt.svg"></td></tr>
</table>


![Image](cdc-applic-mountability/src/main/javadoc/cdc/applic/mountability/doc-files/mountability-data-model.svg)

##### Mountability computation

![Image](cdc-applic-mountability/src/main/javadoc/cdc/applic/mountability/doc-files/mountability.svg)

##### ComputeMountability
The command-line tool **ComputeMountability** has the following options:

````
USAGE
ComputeMountability [--all-possible-reserves | --no-reserves | --user-defined-reserves] [--args-file <arg>] [--args-file-charset
                          <arg>] [--dictionary <arg>] [--exclude-assertions | --include-assertions] [--formatters-catalog <arg>] [-h
                          | -v] [--help-width <arg>]  --input <arg> [--issues <arg>] [--keep-going | --no-keep-going]
                          [--long-symbols | --math-symbols | --short-symbols]  [--naming-convention <arg>]  [--no-pretty | --pretty]
                          [--no-simplify | --simplify] --output <arg>  --repository <arg>

OPTIONS
    --all-possible-reserves      Takes into account all possible reserves in simplification of expressions.
    --args-file <arg>            Optional name of the file from which options can be read.
                                 A line is either ignored or interpreted as a single argument (option or value).
                                 A line is ignored when it is empty or starts by any number of white spaces followed by '#'.
                                 A line that only contains white spaces is an argument.
                                 A comment starts by a '#' not following a '\'. The "\#" sequence is read as '#'.
    --args-file-charset <arg>    Optional name of the args file charset.
                                 It may be used if args file encoding is not the OS default file encoding.
    --dictionary <arg>           Key of the optional dictionary to use. If none is passed, the first registry is used.
                                 It has the form 'name(/name)*'.
    --exclude-assertions         Excludes assertions in simplification of expressions (default).
    --formatters-catalog <arg>   Name of the optional input XML formatters catalog file.
 -h,--help                       Prints this help and exits.
    --help-width <arg>           Optional help width (default: 74).
    --include-assertions         Includes assertions in simplification of expressions.
    --input <arg>                Name of the mandatory input XML mountability file.
    --issues <arg>               Name of the optional output issues file.
    --keep-going                 Keep going even if an error is detected.
    --long-symbols               Generates mountability expressions using long symbols (and, or, in, ...) (default).
    --math-symbols               Generates mountability expressions using math symbols (∧, ∨, ∈, ...).
    --naming-convention <arg>    Optional naming convention (Defaults to default naming convention).
    --no-keep-going              Interrupt processing when an error is detected (default).
    --no-pretty                  Does not pretty print generated file.
    --no-reserves                Ignores reserves in simplification of expressions (default).
    --no-simplify                Does not simplify mountability expressions.
    --output <arg>               Name of the mandatory output XML mountability file.
    --pretty                     Pretty prints generated file (default).
    --repository <arg>           Name of the mandatory input XML, XLS or XLSX repository file.
    --short-symbols              Generates mountability using short symbols (&, |, <:, ...).
    --simplify                   Simplifies mountability expressions (default).
    --user-defined-reserves      Takes into account user defined reserves in simplifications of expressions.
 -v,--version                    Prints version and exits.
````

#### Projections
These modules contain code to project an expression on a subspace:
- `projections` which contains the API;
- and `projections-core` which contains implementation of algorithms.

This allows a simplified, yet correct, view of an expression.

##### Why projections?
Applicability is pervasive and plays a major role in support domain.  
There is a need to display many expressions simultaneously, interpret and compare them at a glance.

There are 2 common ways (with variants) to display an expression:
- as a linear text,
- or as a tree.

In all cases, display may take room, there is no fast and direct interpretation, and it is not easy to compare several expressions.

In some situations, there are digital and analog solutions to display a piece of information.
Above solutions are rather digital-like even if the tree approach can be half-way.

Following [Interpretation](#interpretation), a dictionary D can be associated to a set of points S(D) and an expression E to a subset S(E) of that set.  
As indicated, the dimension of S(D), N, can be very large, obviously much greater than 3.
Displaying S(E) as a subset of S(D) is generally impossible. Projection is a solution. It does not contain all information, but dimension of the projection space can be chosen.

![Image](cdc-applic-projections/src/main/javadoc/cdc/applic/projections/doc-files/projections-analogy.svg)

##### How is it working?
D' is a sub-dictionary of D defined by selecting K properties among the N defined in D.  
S(D') = S(P<sub>1</sub>) &times; S(P<sub>2</sub>) &times; ... &times; S(P<sub>K</sub>)  
**Note:** 1 is probably the most useful value for K as it allows the densest graphical representations.

To each element e'<sub>i</sub> of D', we can associate the subset D<sub>i</sub> of matching elements of D.

![Image](cdc-applic-projections/src/main/javadoc/cdc/applic/projections/doc-files/projections-principles-1.svg)

For  a given expression E, each D<sub>i</sub> can be split in 4 subsets by using 2 criteria:
- elements that match E and those that don't.
- elements that match assertions and those that do'nt.

![Image](cdc-applic-projections/src/main/javadoc/cdc/applic/projections/doc-files/projections-principles-2.svg)

Finally, there are 4 possible and interesting situations based on the cardinal of
D<sub>i</sub> &cap; S(E) &cap; S(A) and of D<sub>i</sub> &cap; S(&not;E) &cap; S(A):
- **SOMETIMES** (but not always): assuming e'<sub>i</sub>, there are valid solutions in D<sub>i</sub>, part of them satisfying E, the others not satisfying E.
- **ALWAYS**: assuming e'<sub>i</sub>, there are valid solutions in D<sub>i</sub>, all of them satisfying E.
- **NEVER**: assuming e'<sub>i</sub>, there are valid solutions in D<sub>i</sub>, none of them satisfying E.
- **DEGENERATE**:  assuming e'<sub>i</sub>, there are no valid solutions in D<sub>i</sub>.

![Image](cdc-applic-projections/src/main/javadoc/cdc/applic/projections/doc-files/projections-principles-3.svg)

##### Possible displays
A basic idea to visualize a projection is:
- adopt a color convention for each of the 4 possible situations,
- for an expression E, display each element of D' using the appropriate color.

![Image](cdc-applic-projections/src/main/javadoc/cdc/applic/projections/doc-files/projections-possible-display.svg)

The main difficulty with that idea is related to the cardinal of D'.
- if it is small enough, things are easy.
- if it is too large, visualization may be difficult, confusing, error-prone, ...

Another difficulty is with the ability to enumerate elements of D'.

There are several possibilities when card(D') is large:
- adopt a mechanism to show that visualization is impossible without ambiguity,
- provide a mechanism to focus (e.g., zoom or filtering) on part of D'.

When K=1, create 1-axis visualizations:
- `enumerated` or `boolean` property: use one box for each value.  
  Boxes can be ordered using the `ordinal` attribute.  
  ![Image](cdc-applic-projections/src/main/javadoc/cdc/applic/projections/doc-files/projections-1d-enum.svg)
- `integer` and `real` properties: use natural order and color intervals.  
  If an interval is too small to be displayed, indicate it.  
  Possibly, use logarithmic scales.  
  ![Image](cdc-applic-projections/src/main/javadoc/cdc/applic/projections/doc-files/projections-1d-interval.svg)
- `pattern` properties: use one box for each known value and one box for unknown values.

When K=2, create 2-axis visualizations based on above ones.

##### Projections classes
The projections API contains the following classes and interfaces:
- **Axis**, representing an 1D Axis corresponding to an `Alias`, a `Property` or an `Expression`;
- **AxisPiece**, which is a subset of the set values of an Axis.
- **Shadow**, which is the result of a projection, and is composed of `ShadowPieces`.
- **ShadowPiece** composed of 3 (`NEVER`, `SOMETIMES`, `ALWAYS`) Cartesian products of `AxisPieces`.
- **ExpressionProjector** that is used to compute the projection of an expression, or of a node, on selected axes.

It contains 2 implementations of `ExpressionProjector`:
- **GeneralExpressionProjector** that can do any projection;
- **SingleAxisExpressionProjector** that is dedicated to `single axis` projections.

![Image](cdc-applic-projections/src/main/javadoc/cdc/applic/projections/doc-files/projections-api.svg)

#### Publication
These modules contain code that can be used to transform an expression to different forms:
- `publication` which contains the API;
- `publication-core` which contains implementation of algorithms;
- and `publication-html` which contains code dedicated to HTML.

The obtained form can be user-friendly, but may loose formalism, so that it cannot be parsed.

Formatting an expression is based on:
- The `ExpressionFormatter` interface implemented by classes that handle the main structure of the generated elements.  
  `InfixFormatter` implementation is provided, but other (postfix, prefix, ...) can be added.
- The `FormattingHandler` interface implemented by classes that consume events generated by an `ExpressionFormatter`.  
  Two implementations are provided, `StringFormattingHandler` and `HtmlFormattingHandler`.  
  Other implementations can be added.
- Formatting of fine grained elements is handled by `PieceFormatter` whose implementations are grouped and organized in `FormatersCatalog`s.

![Image](cdc-applic-publication/src/main/javadoc/cdc/applic/publication/doc-files/publication-formatting-principles.svg)

The hierarchy of `PieceFormatter`s contains the following interfaces:
- `SpaceFormatter` to handle spaces;
- `SymbolFormatter` to handle symbols;
- `SNameFormatter` to handle simple names (local names and prefixes);
- `InformalTextFormatter` to handle informal text;
- and `ValueFormatter` to handle values.  
  An implementation of `ValueFormatter` may only support formatting of certain kinds of values.

![Image](cdc-applic-publication/src/main/javadoc/cdc/applic/publication/doc-files/publication-formatting-pieces.svg)

The symbols that correspond to operators are:
- **AND**, **OR**, **XOR**, **NOT**
- **IMPLICATION**, **EQUIVALENCE**
- **EQUAL**, **NOT_EQUAL**
- **IN**, **NOT_IN**
- **GREATER**, **NOT_GREATER**
- **GREATER_OR_EQUAL**, **NEITHER_GREATER_NOR_EQUAL**
- **LESS**, **NOT_LESS**
- **LESS_OR_EQUAL**, **NEITHER_LESS_NOR_EQUAL**

Those that are used for boolean values are:
- **FALSE** and **TRUE**

Those that are used for sets are:
- **EMPTY_SET**
- **OPEN_SET**, **CLOSE_SET**
- **TO**
- **ITEMS_SEPARATOR**, **LAST_ITEMS_SEPARATOR**

Finally 2 symbols are dedicated to parentheses:
- **OPEN_PAREN** and **CLOSE_PAREN**

##### Piece formatters
There are implementations of `PieceFormatter` dedicated to:
- [formatting of spaces](#formatting-of-spaces).
- [formatting of symbols](#formatting-of-symbols).
- [formatting of simple names](#formatting-of-simple-names).
- [formatting of informal text](#formatting-of-informal-text).
- [formatting of boolean values](#formatting-of-boolean-values).
- [formatting of integer values](#formatting-of-integer-values).
- [formatting of real values](#formatting-of-real-values).
- [formatting of string values](#formatting-of-string-values).


###### Formatting of spaces
`SpaceFormatterImpl` is an implementation of `SpaceFormatter`.

![Image](cdc-applic-publication/src/main/javadoc/cdc/applic/publication/doc-files/publication-formatting-pieces-spaces.svg)

Formatting of spaces is based on:
- `text`, the string to use to represent the space;
- and `spacing`, specifying when spaces should be used:
depending on the context, a space may be necessary or optional:
    - when it is necessary, `text` is emitted.
    - when it is optional, `text` is emitted if `spacing` asks it.

###### Formatting of symbols
`SymbolFormatterImpl` is an implementation of `SymbolFormatter`.

![Image](cdc-applic-publication/src/main/javadoc/cdc/applic/publication/doc-files/publication-formatting-pieces-symbols.svg)

Formatting of symbols has a `default symbol type` attribute.
It was used to initialize the formatter in order to make sure all symbols are supported.  
Each symbol is associated to:
- a `literal` attribute, which is its string representation;
- a `space` attribute, which is a boolean used to tell whether a space is needed between the text representation of the symbol and a text literal.
- a `default` attribute, which tells whether the symbol formatting is the one inherited from `default symbol type` or was overridden.  
  This is purely informative and is not used by formatting.

###### Formatting of simple names
`IdentitySNameFormatter` and `MapSNameFormatter` are implementations of `SNameFormatter`.

![Image](cdc-applic-publication/src/main/javadoc/cdc/applic/publication/doc-files/publication-formatting-pieces-simple-names.svg)

The `identity formatting` of simple names depends on an `escaping mode` that has 3 values:
- **NON_ESCAPED**: the raw name is emitted.
- **ESCAPED**: the escaped name is emitted, event if not necessary.
- **PROTECTED**: the escaped name is emitted if necessary, otherwise the raw name is emitted.

The `map formatting` of simple names depends on a `map`.  
If a simple name is not defined in the map, the protected name is emitted.

###### Formatting of informal text
`IdentityInformalTextFormatter` and `MapInformalTextFormatter` are implementations of `InformalTextFormatter`.

![Image](cdc-applic-publication/src/main/javadoc/cdc/applic/publication/doc-files/publication-formatting-pieces-informal-text.svg)


###### Formatting of boolean values
`BooleanValueFormatter` is an implementation of `ValueFormatter` dedicated to formatting of boolean values.

![Image](cdc-applic-publication/src/main/javadoc/cdc/applic/publication/doc-files/publication-formatting-pieces-booleans.svg)

Formatting of boolean values is based on 2 string `literals`, one for `false`, one for `true`.

**Note:** the boolean symbols are formatted as symbols, not boolean values.

###### Formatting of integer values
`IntegerValueFormatter` is an implementation of `ValueFormatter` dedicated to formatting of integer values.

![Image](cdc-applic-publication/src/main/javadoc/cdc/applic/publication/doc-files/publication-formatting-pieces-integers.svg)

Formatting of integers is based on:
- an optional `locale`;
- and a `format` string.

**Note:** it is the user responsibility to define a valid format string.

###### Formatting of real values
`RealValueFormatter` is an implementation of `ValueFormatter` dedicated to formatting of real values.

![Image](cdc-applic-publication/src/main/javadoc/cdc/applic/publication/doc-files/publication-formatting-pieces-reals.svg)

Formatting of reals is based on:
- an optional `locale`;
- and a `format` string.

**Note:** it is the user responsibility to define a valid format string.

###### Formatting of string values
`IdentityStringValueFormatter`, `MapStringValueFormatter` and `ConverterStringValueFormatter`
are an implementations of `ValueFormatter` dedicated to formatting of string values.

![Image](cdc-applic-publication/src/main/javadoc/cdc/applic/publication/doc-files/publication-formatting-pieces-strings.svg)

The `identity formatting` of string values depends on an `escaping mode` that has 3 values:
- **NON_ESCAPED**: the raw string value is emitted.
- **ESCAPED**: the escaped string value is emitted, event if not necessary.
- **PROTECTED**: the escaped string value is emitted if necessary, otherwise the raw string value is emitted.

The `map formatting` of string values depends on a `map`.  
If a string value is not defined in the map, the protected string value is emitted.

The `converter formatting` of string values is based on a `converter` which is a `UnaryOperator<String>`.

##### Formatters catalog
`FormattersCatalog` is organized with inheritance rules in mind.  
- There are 9 mandatory default `PieceFormatter`s for spaces, symbols, local names, prefixes, informal text, booleans, integers, reals and strings.
- One can associate to each `type` an optional `PieceFormatter`s for spaces, symbols and values.
- One can associate to each `Property` or `Alias` an optional `PieceFormatter`s for spaces, symbols and values.

When a `PieceFormatter` is searched for a `Property` or `Alias`, it is searched in the reverse order:
- If the corresponding `PieceFormatter` was defined for `Property` or `Alias`, return it.
- Otherwise, for a `Property`, if the corresponding `PieceFormatter` was defined for its type, return it.
- Otherwise, return the appropriate default `PieceFormatter`.

![Image](cdc-applic-publication/src/main/javadoc/cdc/applic/publication/doc-files/publication-formatting-catalog.svg)

**Note:** no implementations of `ExpressionbPublisher` exist at the moment.

#### Renaming
These modules contain code that can be used to renames named items of an expression:
- `renaming` which contains the API;
- `renaming-core` which contains implementation of algorithms.

TODO

![Image](cdc-applic-renaming/src/main/javadoc/cdc/applic/renaming/doc-files/renaming.svg)


#### S1000D
These modules contains utilities to help compute ACT, CCT and PCT:
- `s1000d` which contains the API;
- and `s1000d-core` which contains implementation of algorithms.

It contains data classes such as:
- [S1000DProfile](#s1000d-profile) used to configure ACT, CCT and CCT generation;
- [S1000DIssue](#s1000d-issue) to describe S1000D specific issues;
- [S1000DAct](#s1000d-act) to represent an ACT;
- [S1000DCct](#s1000d-cct) to represent a CCT;
- [S1000DPct](#s1000d-pct) to represent a PCT;
- [S1000DActCctPctCollector](#s1000d-act-cct-and-pct-collector) to collect generated data and store them in an `S1000DAct`, `S1000DCct` and `S1000DPct`.

It also contains interfaces used to check data and compute ACT, CCT or PCT:
- [S1000DGlobalChecker](#s1000d-global-checks) to check `Dictionary` data for S1000D compliance.
- [S1000DActGenerator](#s1000d-act-generation) to generate an ACT;
- [S1000DCctGenerator](#s1000d-cct-generation) to generate a CCT;
- [S1000DPctGenerator](#s1000d-pct-generation)  to generate a PCT;

All these algorithms work on a `dictionary` associated to an `additional assertion`.

Conversion algorithms are also provided:
- [ExpressionToS1000DConverter](#expression-to-s1000d-converter) to convert an `Expression` to the corresponding S1000D XML `<applic>` element.
- [S1000DToExpressionConverter](#s1000d-to-expression-converter) to convert an S1000D XML `<applic>` element to an `Expression `.

##### S1000D Profile
An **S1000DProfile** is used to enable or disable certain **Feature**s used during ACT, CCT or PCT generation.

![Image](cdc-applic-s1000d/src/main/javadoc/cdc/applic/s1000d/doc-files/s1000d-profile.svg)

Available `Features` are:
- **INTEGER_TYPES**: if enabled, integer types are supported by the target application.  
  If disabled and an integer type is used, an issue will be generated.
- **INTEGER_RANGES**: if enabled, integer ranges are supported by the target application.  
  If disabled and an integer range is used, an issue will be generated.
- **REAL_TYPES**: if enabled, real types are supported by the target application.  
  If disabled and a real type is used, an issue will be generated.
- **REAL_RANGES**: if enabled, real ranges are supported by the target application.  
  If disabled and a real range is used, an issue will be generated.
- **BOOLEAN_TYPES**: if enabled, boolean types are supported by the target application.  
  If disabled and a boolean type is used, an issue will be generated.
- **BOOLEAN_RANGES**: if enabled, boolean ranges are supported by the target application.  
  If disabled and a boolean range is used, an issue will be generated.
- **STRING_TYPES_ENUMS**: if enabled, enumerated string types are supported by the target application.  
  If disabled and an enumerated string type is used, and issue will be generated.
- **STRING_TYPES_PATTERNS**: if enabled, pattern string types are supported by the target application.  
  If disabled and a pattern string type is used, and issue will be generated.
- **STRING_RANGES**: if enabled, string ranges are supported by the target application.  
  If disabled and a string range is used, and issue will be generated.
- **CONDITION_DEPENDENCIES**: if enabled, condition dependencies are supported by the target application and are generated.  
  If disabled, condition dependencies are not generated.

**Note:** `BOOLEAN_RANGES` and `STRING_RANGES` are provisions.

##### S1000D Issue

**S1000DIssue** is a specialization of `Issue` dedicated to S1000D.

![Image](cdc-applic-s1000d/src/main/javadoc/cdc/applic/s1000d/doc-files/s1000d-issue.svg)

**Note:** some **S1000DIssueTypes** are used to inform on processing steps, others to signal a problem.

The values of **S1000DIssueTypes** are:
- **CHECK_DICTIONARY**: a global check has started.
- **CHECKED_DICTIONARY**: a global check has ended.
- **GENERATE_ACT**: an ACT generation has started.
- **GENERATED_ACT**: an ACT generation has ended.
- **GENERATE_CCT**: a CCT generation has started.
- **GENERATED_CCT**: a CCT generation has ended.
- **GENERATE_PCT**: a PCT generation has started.
- **GENERATED_PCT**: a PCT generation has ended.
- **NON_S1000D_PROPERTY**: a dictionary property is not an S1000DProperty.
- **NON_COMPLIANT_PROPERTY**: the S1000DPropertyType is not set appropriately.
- **CANNOT_SET_PROPERTY**: cannot set any value for a property.
- **CANNOT_SET_ANY_PROPERTY**: cannot set any value for any property.
- **CANNOT_SET_MASTER_PROPERTY**: The master property cannot be set.
- **EXCLUDED_VALUES**: some valid values are excluded for a property.
- **EXCLUDED_PRODUCTS**: some valid values of master property are excluded.
- **NON_RESTRICTED_PROPERTIES**: all values can be set for a property.
- **TOO_MANY_VALUES**: too many values can be set for a property.
- **NON_SUPPORTED_FEATURE**: a non supported feature is used.

##### S1000D ACT
An **S1000DAct** is a list of **ProductAttributes**.

A **ProductAttribute** is characterized by:
- a `property` that must be an `S1000DProperty` that is `PRODUCT_ATTRIBUTE`;
- an optional `pattern` if the property type is a `PatternType`, and which is the `property type pattern`;
- an optional `domain` if the property type is a `DomainedType`, and which is a subset of the `property type domain`.

**Note:** exactly one of `pattern` and `domain` must be defined.

![Image](cdc-applic-s1000d/src/main/javadoc/cdc/applic/s1000d/doc-files/s1000d-act.svg)

##### S1000D CCT
An **S1000DCct** is a list of **ConditionTypes** and a list of **Conditions**.

A **ConditionType** is characterized by:
- a `type` that must be an `S1000DType` that is `PRODUCT_CONDITION` or `EXTERNAL_CONDITION`;
- an optional `pattern` if the type is a `PatternType`, and which is the `type pattern`;
- an optional `domain` if the type is a `DomainedType`, and which is a subset of the `type domain`.

**Note:** exactly  one of `pattern` and `domain` must be defined.

A **Condition** is characterized by:
- a `property` that must be an `S1000DProperty` that is either `PRODUCT_CONDITION` or `EXTERNAL_CONDITION`;
- an optional `domain` if the `type` is a `DomainedType`, and which is a subset of the `condition type domain`.
- a list of `dependencies`.

A **ConditionDependency** is characterized by:
- the `values` of its condition for which it is defined;
- the `dependency` which is the property it is depending on;
- the `dependency values` which is the set of values of `dependency` for which the dependency is defined.

![Image](cdc-applic-s1000d/src/main/javadoc/cdc/applic/s1000d/doc-files/s1000d-cct.svg)

**Note:** this way of defining a condition dependency is a simplification of S1000D approach.  
&pi;<sub>cond</sub> &isin; &Omega;<sub>cond</sub> &rarr; &pi;<sub>x</sub> &isin; &Omega;<sub>x</sub>  
S1000D defines a more general way to create dependencies: some values of a condition imply an expression:  
&pi;<sub>cond</sub> &isin; &Omega;<sub>cond</sub> &rarr; E  
In addition, there is a possible ambiguity: one one hand it is said that a condition can only depend on other conditions,
and, on the other hand, that any expression can be referenced.  
A general solution must be found to transform assertions into condition dependencies.  
See #99.

##### S1000D PCT
An **S1000DPct** is a list of **Products** which are lists of **ProductProperties**.

A **ProductProperty** is characterized by:
- a `property` that must be an `S1000DProperty` that is either a `PRODUCT_ATTRIBUTE` or `PRODUCT_CONDITION`;
- some `values` (`S1000DValues`).

**S1000DValues** is used to describe a set of values and how this set should be analyzed. It is characterized by:
- `cut values`, the set of possible values of the `product property` (attribute or condition), taking into account assertions and `additional assertion`.
- `values`, the set of values of the `product property` that should be used to generate PCT.
- `filling`, the filling of `values` relatively to `cut values`.
- `interpretation`, a hint on interpretation of `filling`.

![Image](cdc-applic-s1000d/src/main/javadoc/cdc/applic/s1000d/doc-files/s1000d-pct.svg)

In an `S1000DValues`, `values` is the only attribute that is used in the PCT,
other attributes are used to check validity of `values`.

According to our understanding of S1000D, for each product of a PCT:
- each `product attribute` should have **one and only one value**.
- each `product condition` should have **at least one value**.

##### S1000D global checks
The **S1000DGlobalChecker** interface and its implementation, **S1000DGlobalCheckerImpl**
are used to make some global checks on a dictionary from an S1000D perspective.  
It is an `active/internal iterator` that triggers events to the passed **S1000DGlobalCheckerHandler**.

![Image](cdc-applic-s1000d/src/main/javadoc/cdc/applic/s1000d/doc-files/s1000d-global-checks.svg)

A necessary condition for a dictionary to be used for S1000D is that all
its properties are `S1000DProperties`, each having a valid `S1000DPropertyType`.

##### S1000D ACT generation
The **S1000DActGenerator** interface and its implementation, **S1000DActGeneratorImpl**
are used to generate an ACT.  
It is an `active/internal iterator` that triggers events to the passed **S1000DActHandler**.

![Image](cdc-applic-s1000d/src/main/javadoc/cdc/applic/s1000d/doc-files/s1000d-act-generation.svg)

One `ActProductAttribute` event is generated for each `PRODUCT_ATTRIBUTE` property.

##### S1000D CCT generation
The **S1000DCctGenerator** interface and its implementation, **S1000DCctGeneratorImpl**
are used to generate a CCT.  
It is an `active/internal iterator` that triggers events to the passed **S1000DCctHandler**.

![Image](cdc-applic-s1000d/src/main/javadoc/cdc/applic/s1000d/doc-files/s1000d-cct-generation.svg)

One `CctCondition` event is generated for each `PRODUCT_CONDITION` or `EXTERNAL_CONDITION` property.

##### S1000D PCT generation
The **S1000DPctGenerator** interface and its implementation, **S1000DPctGeneratorImpl**
are used to generate a PCT.  
It is an `active/internal iterator` that triggers events to the passed **S1000DPctHandler**.

When generating a PCT, a global checks, ACT and PCT generations are also run.

![Image](cdc-applic-s1000d/src/main/javadoc/cdc/applic/s1000d/doc-files/s1000d-pct-generation.svg)

One `PctProduct` event is generated for each possible `master value`, taking into account `assertions` and `addition assertion`.  
Each time, one `PctProductProperty` is generated for each `PRODUCT_ATTRIBUTE` or `PRODUCT_CONDITION`:
`EXTERNAL_CONDITION` properties are ignored in PCT generation (they are included in CCT generation).

##### S1000D ACT, CCT and PCT Collector
**S1000DActCCtPctCollector** class implements `S1000DActHandler`, `S1000DCctHandler`, `S1000DPctHandler` and `S100D0GlobalCheckerHandler`.  
It collects the generated events into an `S1000DAct`, an `S1000DCct`, an `S1000Dct` and an `IssuesCollector`.

![Image](cdc-applic-s1000d/src/main/javadoc/cdc/applic/s1000d/doc-files/s1000d-act-cct-pct-collector.svg)

It is intended to be used with any implementation of `S100DActGenerator`,
`S1000DCctGenerator`, `S1000DPctGenerator` or `S1000DGlobalChecker`.

##### Expression to S1000D Converter
**ExpressionToS1000DConverter** and its implementation **ExpressionToS1000DConverterImpl** can be
used to convert an Expression to S1000D XML.

**Note:** the conversion of an expressions to S1000D can fail for several reasons:
- The expression is invalid (syntax or semantic)
- The expression contains features that are not supported by S1000D, such as negation, special operators, ...  
  Those limitations can be circumvented by transforming the expression to an equivalent one, but that does
  not use those features.

The **XmlHander** is a minimalistic interface used to abstract XML generation.  
Several implementations are provided:
- **DataXmlHandler** that creates an `cdc.io.data` tree.
- **XmlWriterXmlHandler** based on `cdc.io.xml.XmlWriter`.
- **StringXmlHandler** based on `ByteArrayOutputStream` and that can be used to generate a String.

![Image](cdc-applic-s1000d/src/main/javadoc/cdc/applic/s1000d/doc-files/expression-to-s1000d.svg)


##### S1000D to Expression Converter
**S1000DToExpressionConverter** and its implementation **S1000DToExpressionConverterImpl** can be
used to convert an S1000D XML applibility to an Expression.

**Note:** the conversion of an S1000D XML `<applic>` to an expression can fail for several reasons:
- It is malmformed
- It uses non supported features, such as informal expressions or logic engine expressions.
- The passed `Dictionary` is not compliant with teh S1000D applic.

The **XmlSource** is a minimalistic interface used to abstract reading of XML tree.  
Several implementations are provided:
- **DataXmlSource** that is based on `cdc.io.data`.
- **StringXmlSource** that is based on `cdc.io.data` and uses a String as input.

![Image](cdc-applic-s1000d/src/main/javadoc/cdc/applic/s1000d/doc-files/s1000d-to-expression.svg)


#### Simplification
These modules contain code that can transform expressions to simplify them:
- `simplification` which contains the API;
- and `simplification-core` which contains implementation of algorithms.

Taking into account or ignoring assertions impacts simplification result.  
Simplification supports removal of negation. This can be necessary to generate S1000D compliant expressions.

Simplification is configured with `SimplifierFeatures` that is characterized by:
- a set of enabled `hints`;
- a `maximum time` to spent in simplification;
- a `maximum number of iterations` that can be done to simplify;
- and `prover features`

Available hints are:
- **REMOVE_NEGATION**  
  This should be enabled to obtain a S1000D compliant expression.
- **FAIL_ON_NON_REMOVABLE_NEGATION**: if negation cannot be eliminated, the simplification fails.
- **REMOVE_ALWAYS_TRUE_OR_FALSE**: removes sections of the expression that always evaluate to true or false.
  This may be very expensive.
- **REMOVE_REDUNDANT_SIBLINGS**: removes sections of the expression that are redundant.
  This may be very expensive.
- **REMOVE_INEQUALITIES**  
  This should be enabled to obtain a S1000D compliant expression.
- **REMOVE_USELESS_PREFIXES**
- **NORMALIZE_BOOLEAN_PROPERTIES**
- **CONVERT_TO_DNF**  
  This may be very expensive.
- **AUTO_PROJECT**  
  This can be used to take assertions into account to remove values that can not be used.  
  For example, with integer property I: {1\~999} and assertion I != 1, expression I in {1\~999} can be simplified to I in {2\~999}.

**Note:** a hint that was enabled by code can be disabled by adding `-Dapplic.simplifier.disable.hint` on the command line,
  where `hint` is the hint name (case does not matter).

Result of simplification is `QualifiedValue` which (value, quality) pair.  
`Quality` is used to characterized the obtained result:
- **SUCCESS** used when simplification succeeded without any interruption.
- **PARTIAL** used when simplification was interrupted because of limits (max time or max iterations).  
  In that case, obtained result is however equivalent to input expression.
- **FAILURE** used to indicate that simplification failed. Prover failed or there is a bug in simplification.


  ![Image](cdc-applic-simplification/src/main/javadoc/cdc/applic/simplification/doc-files/simplification.svg)

There are known limitations to simplification:
- Simplification does not work in some cases where complements
  cannot be computed, even if a valid result could be produced. See #86.
- Some transformations are very expensive or don't produce expected result.
  Some more clever algorithms should be used in some cases. See #62.

#### Substitutions
A substitution is the replacement of a piece of expression by another one.  
For example, one can replace use of standard in an expression by use of ranks.  
These modules contain code that supports substitutions:
- `substitution` which contains the API;
- and `substitution-core` which contains implementation of algorithms.

TODO

#### Tools

##### ApplicConsistencyChecker
**ApplicConsistencyChecker** analyzes consistency of a configured data set and generates issues.

See [Consistency](#consistency) module.

This command-line tool has the following options:

````
USAGE
ApplicConsistencyChecker [--args-file <arg>] [--args-file-charset <arg>] --basename <arg> [--best | --fastest]  [-h | -v]
                               [--help-width <arg>] --model <arg> --output-dir <arg> [--profile-config <arg>] --repository <arg>
                               [--verbose]

Utility that can analyze an applicability model and check its consistency.

OPTIONS
    --args-file <arg>           Optional name of the file from which options can be read.
                                A line is either ignored or interpreted as a single argument (option or value).
                                A line is ignored when it is empty or starts by any number of white spaces followed by '#'.
                                A line that only contains white spaces is an argument.
                                A comment starts by a '#' not following a '\'. The "\#" sequence is read as '#'.
    --args-file-charset <arg>   Optional name of the args file charset.
                                It may be used if args file encoding is not the OS default file encoding.
    --basename <arg>            Mandatory base name of generated files.
    --best                      Use options that generate best output.
    --fastest                   Use options that are fast to generate output (default).
 -h,--help                      Prints this help and exits.
    --help-width <arg>          Optional help width (default: 74).
    --model <arg>               Mandatory name of the Consistency Model file to load.
    --output-dir <arg>          Mandatory name of the output directory. If this directory does not exist, it is created.
    --profile-config <arg>      Optional name of the Consistency Profile Config file to load.
    --repository <arg>          Mandatory name of the Applic Repository file to load.
 -v,--version                   Prints version and exits.
    --verbose                   Print messages.
````

##### ExtractS1000DDmApplic
**ExtractS1000DDmApplic** extracts applicabilities from a set of S1000D Data Modules, analyzes them
and produces a report.

This command-line tool has the following options:

````
USAGE
ExtractS1000DDmApplic --act <arg> | --repository <arg> [--args-file <arg>] [--args-file-charset <arg>] [--dictionary-name <arg>]
                            --dictionary-path <arg> [-h | -v] [--help-width <arg>] --input-dir <arg> [--locale <arg>] --output <arg>
                            [--transform-types]  [--verbose] [--white-list <arg>]

Extract applicability from S1000D Data Modules (Issues 3(?), 4 and 5) and convert them to Expressions.
An Office (XLS, XLSX, ...) file is generated.
A repository must be passed or can be generated from an ACT/CCT/PCT.

OPTIONS
    --act <arg>                 Optional name of the input ACT file used to generate the repository.
                                Must be used when a preexisting repository is not passed.
    --args-file <arg>           Optional name of the file from which options can be read.
                                A line is either ignored or interpreted as a single argument (option or value).
                                A line is ignored when it is empty or starts by any number of white spaces followed by '#'.
                                A line that only contains white spaces is an argument.
                                A comment starts by a '#' not following a '\'. The "\#" sequence is read as '#'.
    --args-file-charset <arg>   Optional name of the args file charset.
                                It may be used if args file encoding is not the OS default file encoding.
    --dictionary-name <arg>     Optional name of the generated dictionary (default: Dictionary).
                                Used when ACT is loaded to generate the repository.
    --dictionary-path <arg>     Mandatory name of the applic dictionary path in the repository.
 -h,--help                      Prints this help and exits.
    --help-width <arg>          Optional help width (default: 74).
    --input-dir <arg>           Mandatory name of the intput directory that contains S1000D Data Modules.
    --locale <arg>              Optional locale of descriptions in ACT/CCT/PCT (default: en).
                                Used when ACT is loaded to generate the repository.
    --output <arg>              Mandatory name of the output file.
                                Its extension must be a recognized one: xls, xlsx, ods, ...
                                Generating a csv is possible, but not advised because several sheets are produced.
    --repository <arg>          Optional name of the applic repository to load.
                                Must be defined if the dictionary is not generated from ACT.
    --transform-types           Tries to recognize some type declarations (patterns, ...) and replaces them by better, more specific
                                ones:
                                - false|true enumeration (case and order insensitive) is transformed to a boolean
                                - \d pattern is transformed to an integer in range 0~9
                                - [0-9] pattern is transformed to an integer in range 0~9
                                - \d{min,max} pattern is transformed to an integer in range 0~10**max-1
                                - [0-9]{min,max} pattern is transformed to an integer in range 0~10**max-1
                                Type substitutions are safe and have no impact on their usage.
 -v,--version                   Prints version and exits.
    --verbose                   Prints messages.
    --white-list <arg>          Optional name of a white-list file containing the names of DM to analyze.
````

##### RegistryEncoder
**RegistryEncoder** generates a `RegistryBinding` and uses it to convert a source Registry to a target one.  
This can be used to anomymize a Registry.

This command-line tool has the following options:

````
USAGE
RegistryEncoder [--args-file <arg>] [--args-file-charset <arg>] [--encode-aliases] [--encode-properties] [--encode-types]
                      [--encode-values] [-h | -v] [--help-width <arg>] --input-repository <arg> --output-repository <arg>
                      [--preserve-item <arg>] [--preserve-type <arg>] [--preserve-value <arg>] --source-registry <arg>
                      --target-registry <arg> [--target-registry-prefix <arg>]

OPTIONS
    --args-file <arg>                Optional name of the file from which options can be read.
                                     A line is either ignored or interpreted as a single argument (option or value).
                                     A line is ignored when it is empty or starts by any number of white spaces followed by '#'.
                                     A line that only contains white spaces is an argument.
                                     A comment starts by a '#' not following a '\'. The "\#" sequence is read as '#'.
    --args-file-charset <arg>        Optional name of the args file charset.
                                     It may be used if args file encoding is not the OS default file encoding.
    --encode-aliases                 If enabled, all aliases names are encoded, except the preserved ones.
    --encode-properties              If enabled, all properties names are encoded, except the preserved ones.
    --encode-types                   If enabled, all types names are encoded, except the preserved ones.
    --encode-values                  If enabled, all types values are encoded, except the preserved ones.
 -h,--help                           Prints this help and exits.
    --help-width <arg>               Optional help width (default: 74).
    --input-repository <arg>         Name of the file containing the repository to load.
    --output-repository <arg>        Name of the file containing the resulting repository.
    --preserve-item <arg>            Name(s) of item(s) that must be preserved.
    --preserve-type <arg>            Name(s) of type(s) that must be preserved.
    --preserve-value <arg>           Name(s) of type(s) whose values must be preserved.
    --source-registry <arg>          Name of the source registry to encode.
    --target-registry <arg>          Name of the target encoded registry.
    --target-registry-prefix <arg>   Prefix of the target encoded registry.
 -v,--version                        Prints version and exits.
````

##### S1000DApplicToRepository

**S1000DApplicToRepository** reads an ACT and its companion files, and converts them to a `Repository`.

This command-line tool has the following options:

````
USAGE
S1000DApplicToRepository [--args-file <arg>] [--args-file-charset <arg>] [--dictionary-name <arg>] [-h | -v] [--help-width <arg>]
                               --input <arg> [--locale <arg>] --output <arg> [--transform-types]  [--verbose]

S1000DApplicToRepository reads an ACT and its companion files (CCT, ...), and generates an applic repository.

OPTIONS
    --args-file <arg>           Optional name of the file from which options can be read.
                                A line is either ignored or interpreted as a single argument (option or value).
                                A line is ignored when it is empty or starts by any number of white spaces followed by '#'.
                                A line that only contains white spaces is an argument.
                                A comment starts by a '#' not following a '\'. The "\#" sequence is read as '#'.
    --args-file-charset <arg>   Optional name of the args file charset.
                                It may be used if args file encoding is not the OS default file encoding.
    --dictionary-name <arg>     Optional name of the generated dictionary (default: Dictionary).
 -h,--help                      Prints this help and exits.
    --help-width <arg>          Optional help width (default: 74).
    --input <arg>               Mandatory name of the input ACT file.
    --locale <arg>              Optional locale of descriptions (default: en).
    --output <arg>              Mandatory name of the output repository file.
    --transform-types           Tries to recognize some type declarations (patterns, ...) and replaces them by better, more specific
                                ones:
                                - false|true enumeration (case and order insensitive) is transformed to a boolean
                                - \d pattern is transformed to an integer in range 0~9
                                - [0-9] pattern is transformed to an integer in range 0~9
                                - \d{min,max} pattern is transformed to an integer in range 0~10**max-1
                                - [0-9]{min,max} pattern is transformed to an integer in range 0~10**max-1
                                Type substitutions are safe and have no impact on their usage.
 -v,--version                   Prints version and exits.
    --verbose                   Prints messages.

LIMITATIONS"
PCT and condition dependencies are not analyzed.
````


##### VKSD
TODO

This command-line tool **VariantedKeyedSheetDiff** has the following options:

````
USAGE
VariantedKeyedSheetDiff [--added-mark <arg>] [--added-or-removed-marks | --no-added-or-removed-marks] --applic-column <arg>
                              --applic-dictionary <arg> --applic-repository <arg> [--args-file <arg>] [--args-file-charset <arg>]
                              [--attribute <arg>] [--auto-size-columns] [--auto-size-rows] [--cell-diff-columns |
                              --no-cell-diff-columns] [--changed-mark <arg>] [--charset <arg>] [--colors | --no-colors] [--diff-mark
                              <arg>] [--dump-partitions] --file1 <arg> [--file1-mark <arg>] --file2 <arg> [--file2-mark <arg>] [-h |
                              -v] [--help-width <arg>] --key <arg> [--line-diff-column | --no-line-diff-column] [--map1 <arg>]
                              [--map2 <arg>] [--marks | --no-marks]      [--no-unchanged-lines | --unchanged-lines]
                              [--no-vulnerability-protections | --vulnerability-protections] --output <arg> [--removed-mark <arg>]
                              [--save-synthesis] [--separator <arg>] [--sheet <arg>] [--sheet1 <arg>] [--sheet2 <arg>]
                              [--show-change-details] [--show-original-names] [--sort-lines] [--split-comparisons] [--synthesis]
                              [--unchanged-mark <arg>]  [--verbose]

VariantedKeyedSheetDiff is used to compare two sheets (csv, xls, xlsx or ods).
Lines in sheets are matched by a set of key columns and an applic column.
Input and output files can use different formats.
Differences are indicated with textual marks or colors (if output format supports it).
Several lines with the same keys can exist as long as their applicabilities are disjoint. If this is not the case, this tool will
not work.

OPTIONS
    --added-mark <arg>               Optional mark for added cells (default: "<A>").
    --added-or-removed-marks         Output added or removed marks (default).
    --applic-column <arg>            Mandatory name of the applic column.
    --applic-dictionary <arg>        Mandatory path of the applic dictionary to use.
    --applic-repository <arg>        Mandatory path to the applic repository.
    --args-file <arg>                Optional name of the file from which options can be read.
                                     A line is either ignored or interpreted as a single argument (option or value).
                                     A line is ignored when it is empty or starts by any number of white spaces followed by '#'.
                                     A line that only contains white spaces is an argument.
                                     A comment starts by a '#' not following a '\'. The "\#" sequence is read as '#'.
    --args-file-charset <arg>        Optional name of the args file charset.
                                     It may be used if args file encoding is not the OS default file encoding.
    --attribute <arg>                Optional name(s) of attribute column(s) to compare.
                                     If omitted, all attribute columns are compared.
                                     If mapping is used, use new name(s).
    --auto-size-columns              Auto size columns. This may take longer time.
    --auto-size-rows                 Auto size rows. This may take longer time.
    --cell-diff-columns              Add a column for each key or data column describing differences of cells.
    --changed-mark <arg>             Optional mark for changed cells (default: "<C>").
    --charset <arg>                  Optional name of the charset for csv files (default: platform default charset).
    --colors                         Use colors (default with output formats that support colors).
    --diff-mark <arg>                Optional mark for diff columns (default: "Diff").
    --dump-partitions                Dump generated partitions that are then compared.
    --file1 <arg>                    Mandatory name of the first input file.
    --file1-mark <arg>               Optional mark for file1 columns (default: "1").
                                     Related to show-original-namesoption.
    --file2 <arg>                    Mandatory name of the second input file.
    --file2-mark <arg>               Optional mark for file2 columns (default: "2").
                                     Related to show-original-namesoption.
 -h,--help                           Prints this help and exits.
    --help-width <arg>               Optional help width (default: 74).
    --key <arg>                      Mandatory name(s) of key column(s).
                                     If mapping is used, use new name(s).
    --line-diff-column               Add a column describing differences of lines (default).
    --map1 <arg>                     Optional mapping of column names (keys and attributes) of first file.
                                     Each mapping has the form <old name>::<new name>.
    --map2 <arg>                     Optional mapping of column names (keys and attributes) of second file.
                                     Each mapping has the form <old name>::<new name>.
    --marks                          Use marks (default with output formats that don't support colors).
    --no-added-or-removed-marks      Do not output added or removed marks.
    --no-cell-diff-columns           Do not add a column for each key or data column describing differences of cells (default).
    --no-colors                      Do not use colors (default with output formats that don't support colors).
    --no-line-diff-column            Do not add a column describing differences of lines.
    --no-marks                       Do not use marks (default with output formats that support colors).
    --no-unchanged-lines             Do not output unchanged lines.
    --no-vulnerability-protections   Disable vulnerability protections such as detection of Zip bombs.
                                     This should be used with trusted sources.
    --output <arg>                   Mandatory name of the output file.
    --removed-mark <arg>             Optional mark for removed cells (default: "<R>").
    --save-synthesis                 Save synthesis in output file, in a dedicated sheet.
    --separator <arg>                Optional char separator for csv files (default: ';').
    --sheet <arg>                    Optional name of the delta sheet in the output file. (default: "Delta").
    --sheet1 <arg>                   Optional name of the sheet in the first input file. If omitted, the first sheet is loaded.
    --sheet2 <arg>                   Optional name of the sheet in the second input file. If omitted, the first sheet is loaded.
    --show-change-details            If enabled, show value 1 (with removed mark or color) and value 2 (with added mark or color).
                                     Otherwise, show value 2 (with changed mark or color).
    --show-original-names            If enabled, adds names of original columns in output columns.
                                     This is useful when name mapping is done.
    --sort-lines                     Sort lines using keys and applic columns. Order of key columns declaration matters.
    --split-comparisons              If enabled, 2 columns are created for each pair of compared input columns, one for each input
                                     file.
    --synthesis                      Print a synthesis of differences on terminal.
    --unchanged-lines                Output unchanged lines (default).
    --unchanged-mark <arg>           Optional mark for unchanged cells (default: "").
 -v,--version                        Prints version and exits.
    --verbose                        Print progress messages.
    --vulnerability-protections      Enable vulnerability protections such as detection of Zip bombs (default).

KNOWN LIMITATIONS
All csv files (input and output) must use the same charset and separator.
When mixing input file formats with CSV, if a key column contains numbers, comparison will fail.
Ods handling is experimental. Ods output does not support coloring.
````

#### UI Swing
This module is mainly intended to experiment and illustrate concepts and features.  
It is currently very poor and has a low priority.


![Image](cdc-applic-demos/screenshots/expression-label-demo.png)


## TODO
- Fixed point? Computations with reals is not practical.
- Implement real S1000D condition dependencies
- ...