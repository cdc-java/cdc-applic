package cdc.applic.renaming.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;
import cdc.applic.renaming.RenamingProfile;

class RenamerImplTest {
    private static void checkExpression(String expectedExpression,
                                        String expression,
                                        RenamingProfile profile) {
        final RenamerImpl renamer = new RenamerImpl(profile);
        final Expression expected = new Expression(expectedExpression);
        final Expression actual = renamer.rename(new Expression(expression));
        assertEquals(expected, actual);
    }

    private static void checkNode(String expectedExpression,
                                  String expression,
                                  RenamingProfile profile) {
        final RenamerImpl renamer = new RenamerImpl(profile);
        final Node expected = new Expression(expectedExpression).getRootNode();
        final Node actual = renamer.rename(new Expression(expression).getRootNode());
        assertEquals(expected, actual);
    }

    @Test
    void testExpressionOnePrefix() {
        final RenamingProfile profile = RenamingProfile.builder()
                                                       .prefix("P1", "p1")
                                                       .build();

        checkExpression("p1.X", "P1.X", profile);
        checkExpression("not p1.X", "not P1.X", profile);
        checkExpression(" p1.X ", " P1.X ", profile);
        checkExpression(" p1.X = 10 ", " P1.X = 10 ", profile);
        checkExpression("P1", "P1", profile); // This is not a prefix !
        checkExpression("P1 or p1.P1", "P1 or P1.P1", profile);
        checkExpression("P1 and p1.P1", "P1 and P1.P1", profile);
        checkExpression("P1 xor p1.P1", "P1 xor P1.P1", profile);

        assertThrows(ApplicException.class, () -> {
            checkExpression("not", "not", profile);
        });
    }

    @Test
    void testExpressionRemovePrefix() {
        final RenamingProfile profile = RenamingProfile.builder()
                                                       .prefix("P1", null)
                                                       .build();
        checkExpression("X", "P1.X", profile);
        checkExpression("P1", "P1", profile);
    }

    @Test
    void testExpressionName() {
        final RenamingProfile profile = RenamingProfile.builder()
                                                       .name("P1.A", "a") // First renaming
                                                       .name("a", "p2.b") // Second renaming
                                                       .build();
        checkExpression("p2.b", "P1.A", profile);
    }

    @Test
    void testNodeOnePrefix() {
        final RenamingProfile profile = RenamingProfile.builder()
                                                       .prefix("P1", "p1")
                                                       .build();
        checkNode("p1.X", "P1.X", profile);
        checkNode("!p1.X", "   not    P1.X   ", profile);
    }
}