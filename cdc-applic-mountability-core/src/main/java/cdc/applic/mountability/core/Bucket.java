package cdc.applic.mountability.core;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Expressions;
import cdc.applic.mountability.Interchangeability;
import cdc.util.lang.Checks;

/**
 * Internal class used to wrap a variant.
 *
 * @param <V> The variant type.
 */
public class Bucket<V> {
    /** The variant. */
    final V variant;
    /** The interchangeability of variant with its predecessor. */
    final Interchangeability interchangeability;
    /** The variant applicability. */
    final Expression applicability;

    /**
     * All elementary expressions associated to possible replacement.
     * <p>
     * This is filled by {@link Computer}.
     */
    final List<Expression> p = new ArrayList<>();

    /**
     * All elementary expressions associated to impossible replacement.
     * <p>
     * This is filled by {@link Computer}.
     */
    final List<Expression> n = new ArrayList<>();

    public Bucket(V variant,
                  Interchangeability interchangeability,
                  Expression applicability) {
        this.variant = variant;
        this.interchangeability = interchangeability;
        this.applicability = Checks.isNotNull(applicability, "applicabilty");
    }

    public boolean isValid() {
        return applicability.isValid();
    }

    /**
     * @return The synthesis of possible replacement.
     */
    public Expression getPositive() {
        return Expressions.SHORT_NARROW_NO_SIMPLIFY.or(p);
    }

    /**
     * @return The synthesis of impossible replacement.
     */
    public Expression getNegative() {
        return n.isEmpty()
                ? Expression.FALSE
                : Expressions.SHORT_NARROW_NO_SIMPLIFY.or(n);
    }

    /**
     * @return The mountability. Must be called once {@link Computer} has finished its job.
     */
    public Expression getMountability() {
        return Expressions.SHORT_NARROW_NO_SIMPLIFY.and(getPositive(),
                                                        Expressions.SHORT_NARROW_NO_SIMPLIFY.not(getNegative()));
    }

    @Override
    public String toString() {
        return variant + " " + interchangeability + " " + applicability;
    }
}