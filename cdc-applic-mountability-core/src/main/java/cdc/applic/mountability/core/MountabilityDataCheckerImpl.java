package cdc.applic.mountability.core;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.dictionaries.checks.SemanticChecker;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.SyntaxChecker;
import cdc.applic.mountability.MountabilityData;
import cdc.applic.mountability.MountabilityDataChecker;
import cdc.issues.IssuesHandler;

/**
 * Implementation of {@link MountabilityDataChecker}.
 *
 * @param <U> The Use Point type.
 * @param <V> The Variant type.
 */
public class MountabilityDataCheckerImpl<U, V> implements MountabilityDataChecker<U, V> {
    private final DictionaryHandle handle;

    public MountabilityDataCheckerImpl(DictionaryHandle handle) {
        this.handle = handle;
    }

    @Override
    public void check(MountabilityData<U, V> data,
                      IssuesHandler<ApplicIssue> handler) {
        final SyntaxChecker syntyax = new SyntaxChecker();
        final SemanticChecker semantic = new SemanticChecker(handle.getDictionary());

        // Buffer of issues
        final List<ApplicIssue> issues = new ArrayList<>();

        for (final U usePoint : data.getUsePoints()) {
            for (final V variant : data.getVariants(usePoint)) {
                final Expression applicability = data.getVariantApplicability(usePoint, variant);
                issues.clear();
                syntyax.check(applicability, issues);
                if (issues.isEmpty()) {
                    semantic.check(applicability, issues);
                }
                handler.issues(issues);
            }
        }
    }
}