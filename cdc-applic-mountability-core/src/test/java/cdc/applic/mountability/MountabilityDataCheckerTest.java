package cdc.applic.mountability;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.mountability.core.MountabilityDataCheckerImpl;
import cdc.applic.mountability.impl.Data;
import cdc.applic.mountability.impl.UsePoint;
import cdc.applic.mountability.impl.Variant;
import cdc.issues.IssuesCollector;

class MountabilityDataCheckerTest {
    private static final Logger LOGGER = LogManager.getLogger(MountabilityDataCheckerTest.class);

    @Test
    void test() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final DictionaryHandle registryHandle = new DictionaryHandle(registry);

        registry.integerType().name("Rank").frozen(true).domain("1~999").build();
        registry.property().name("Rank").type("Rank").build();

        final Data data = new Data();
        final UsePoint up1 = new UsePoint("up1");
        data.addUsePoint(up1);
        up1.addVariant("v1", null, Expression.of("R=10", false)); // R does not exist
        up1.addVariant("v2", null, Expression.of("Rank=", false)); // Expression is incomplete
        up1.addVariant("v3", null, Expression.of("Rank=1000", false)); // 1000 is out of range

        final MountabilityDataCheckerImpl<UsePoint, Variant> checker = new MountabilityDataCheckerImpl<>(registryHandle);
        final IssuesCollector<ApplicIssue> collector = new IssuesCollector<>();

        checker.check(data, collector);
        LOGGER.info("Issues: {}", collector.getIssues().size());
        for (final ApplicIssue issue : collector.getIssues()) {
            LOGGER.info("   {}", issue);
        }

        assertSame(3, collector.getIssues().size());
    }
}