package cdc.applic.mountability;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import cdc.applic.mountability.core.ComputeMountability;
import cdc.util.cli.MainResult;

class Issue134Test {
    @Test
    void test() {
        assertSame(MainResult.SUCCESS,
                   ComputeMountability.exec("--args-file", "src/test/resources/mountability-134-args.txt"));
    }
}