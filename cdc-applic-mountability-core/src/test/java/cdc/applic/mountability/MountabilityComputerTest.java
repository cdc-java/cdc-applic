package cdc.applic.mountability;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.Expression;
import cdc.applic.mountability.core.MountabilityComputerImpl;
import cdc.applic.mountability.events.MountabilityComputationEvent;
import cdc.applic.mountability.events.MountabilityEvent;
import cdc.applic.mountability.impl.TestMountabilityData;
import cdc.applic.mountability.impl.TestUsePoint;
import cdc.applic.mountability.impl.TestVariant;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.simplification.SimplifierFeatures;
import cdc.applic.simplification.SimplifierFeatures.Hint;
import cdc.util.events.ProgressController;

class MountabilityComputerTest {
    private static final Logger LOGGER = LogManager.getLogger(MountabilityComputerTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    private final RepositoryImpl repository = new RepositoryImpl();
    private final RegistryImpl registry = repository.registry().name("Registry").build();
    private final DictionaryHandle registryHandle = new DictionaryHandle(registry);

    MountabilityComputerTest() {
        registry.integerType().name("Rank").frozen(true).domain("1~999").build();
        registry.booleanType().name("Boolean").build();
        registry.property().name("Rank").type("Rank").ordinal(0).build();
        registry.property().name("SB1").type("Boolean").ordinal(1).build();
        registry.property().name("SB2").type("Boolean").ordinal(2).build();

        registry.createAssertion("SB2 -> SB1");

        // repository.print(OUT, 0);
    }

    private static class Result {
        final int usePointId;
        final int variantId;
        final Expression mountability;

        public Result(int usePointId,
                      int variantId,
                      Expression mountability) {
            this.usePointId = usePointId;
            this.variantId = variantId;
            this.mountability = mountability;
        }
    }

    static Result r(int usePointId,
                    int variantId,
                    String mountability) {
        return new Result(usePointId, variantId, new Expression(mountability));
    }

    private void check(TestMountabilityData data,
                       Result... results) {
        LOGGER.debug("===============================");
        LOGGER.debug("check(...)");
        data.print(OUT);
        final MountabilityComputer<TestUsePoint, TestVariant> computer =
                new MountabilityComputerImpl<>(registryHandle,
                                               SimplifierFeatures.builder()
                                                                 .proverFeatures(ProverFeatures.EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)
                                                                 .hints(Hint.CONVERT_TO_DNF,
                                                                        Hint.NORMALIZE_BOOLEAN_PROPERTIES,
                                                                        Hint.REMOVE_ALWAYS_TRUE_OR_FALSE,
                                                                        Hint.REMOVE_NEGATION,
                                                                        Hint.REMOVE_REDUNDANT_SIBLINGS)
                                                                 .noLimits()
                                                                 .build());
        final List<MountabilityEvent> events = computer.compute(data,
                                                                MountabilityComputerFeatures.builder()
                                                                                            .hint(MountabilityComputerFeatures.Hint.SIMPLIFY)
                                                                                            .build(),
                                                                ProgressController.VOID);

        int index = 0;
        LOGGER.debug("Events:");
        for (final MountabilityEvent event : events) {
            LOGGER.debug("   event: " + event);
            if (event instanceof MountabilityComputationEvent) {
                @SuppressWarnings("unchecked")
                final MountabilityComputationEvent<TestUsePoint, TestVariant> e =
                        (MountabilityComputationEvent<TestUsePoint, TestVariant>) event;
                if (e.getType() == MountabilityComputationEvent.Type.VARIANT_MOUNTABILITY) {
                    assertTrue(index < results.length, "Too many effective events");
                    final Result expected = results[index];
                    assertEquals(expected.usePointId, e.getUsePoint().getId(), "Unexpected use point id");
                    assertEquals(expected.variantId, e.getVariant().getId(), "Unexpected variant id");
                    assertEquals(expected.mountability.compress(),
                                 e.getMountability().compress(),
                                 "Unexpected mountability on " + expected.usePointId + "/" + expected.variantId);
                    index++;
                }
            }
        }
        assertEquals(results.length, index, "Expected " + results.length + " mountability events, found: " + index);
    }

    @Test
    void testEmpty() {
        final TestMountabilityData data = new TestMountabilityData();
        check(data);
    }

    @Test
    void test1UP0() {
        final TestMountabilityData data = new TestMountabilityData();
        data.addUsePoint(new TestUsePoint(1));
        check(data);
    }

    @Test
    void test1UP1() {
        final TestMountabilityData data = new TestMountabilityData();
        data.addUsePoint(new TestUsePoint(1).addVariant(null, "Rank<:{1~999}"));
        check(data,
              r(1, 1, "Rank<:{1~999}"));
    }

    @Test
    void test1UP2NotInterchangeable() {
        final TestMountabilityData data = new TestMountabilityData();
        data.addUsePoint(new TestUsePoint(1).addVariant(null, "Rank<:{1~10}")
                                            .addVariant(Interchangeability.NOT_INTERCHANGEABLE, "Rank<:{11~999}"));
        check(data,
              r(1, 1, "Rank<:{1~10}"),
              r(1, 2, "Rank<:{11~999}"));
    }

    @Test
    void test1UP2TwoWays() {
        final TestMountabilityData data = new TestMountabilityData();
        data.addUsePoint(new TestUsePoint(1).addVariant(null, "Rank<:{1~10}")
                                            .addVariant(Interchangeability.TWO_WAYS, "Rank<:{11~999}"));
        check(data,
              r(1, 1, "Rank<:{1~999}"),
              r(1, 2, "Rank<:{1~999}"));
    }

    @Test
    void test1UP2OneWay() {
        final TestMountabilityData data = new TestMountabilityData();
        data.addUsePoint(new TestUsePoint(1).addVariant(null, "Rank<:{1~10}")
                                            .addVariant(Interchangeability.ONE_WAY, "Rank<:{11~999}"));
        check(data,
              r(1, 1, "Rank<:{1~10}"),
              r(1, 2, "Rank<:{1~999}"));
    }

    @Test
    void test1UP4A() {
        final TestMountabilityData data = new TestMountabilityData();
        data.addUsePoint(new TestUsePoint(1).addVariant(null, "Rank<:{1~10}")
                                            .addVariant(Interchangeability.ONE_WAY, "Rank<:{11~20}")
                                            .addVariant(Interchangeability.NOT_INTERCHANGEABLE,
                                                        "Rank<:{21~30} | Rank<:{1~10}&SB1 | Rank<:{11~20}&SB2")
                                            .addVariant(Interchangeability.TWO_WAYS, "Rank<:{31~999}"));
        check(data,
              r(1, 1, "Rank<:{1~10}&!SB1"),
              r(1, 2, "Rank<:{1~10}&!SB1|Rank<:{11~20}&!SB2"),
              r(1, 3, "Rank<:{1~10}&SB1|Rank<:{11~20}&SB2|Rank<:{21~999}"),
              r(1, 4, "Rank<:{1~10}&SB1|Rank<:{11~20}&SB2|Rank<:{21~999}"));
    }

    @Test
    void test1UP4B() {
        final TestMountabilityData data = new TestMountabilityData();
        data.addUsePoint(new TestUsePoint(1).addVariant(null, "Rank<:{1~10}")
                                            .addVariant(Interchangeability.ONE_WAY, "Rank<:{11~20}")
                                            .addVariant(Interchangeability.NOT_INTERCHANGEABLE, "Rank<:{21~30}")
                                            .addVariant(Interchangeability.TWO_WAYS, "Rank<:{31~999}"));
        check(data,
              r(1, 1, "Rank<:{1~10}"),
              r(1, 2, "Rank<:{1~20}"),
              r(1, 3, "Rank<:{21~999}"),
              r(1, 4, "Rank<:{21~999}"));
    }

    @Test
    void test1UP4C() {
        final TestMountabilityData data = new TestMountabilityData();
        data.addUsePoint(new TestUsePoint(1).addVariant(null, "Rank<:{1~10}")
                                            .addVariant(Interchangeability.TWO_WAYS, "Rank<:{11~20}")
                                            .addVariant(Interchangeability.ONE_WAY, "Rank<:{21~30}")
                                            .addVariant(Interchangeability.TWO_WAYS, "Rank<:{31~999}"));
        check(data,
              r(1, 1, "Rank<:{1~20}"),
              r(1, 2, "Rank<:{1~20}"),
              r(1, 3, "Rank<:{1~999}"),
              r(1, 4, "Rank<:{1~999}"));
    }
}