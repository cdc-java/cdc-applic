package cdc.applic.consistency.handlers;

import cdc.issues.Issue;
import cdc.issues.IssuesHandler;

@Deprecated(since = "2024-12-31", forRemoval = true)
public interface ConsistencyHandler extends IssuesHandler<Issue> {
    public void beginAnalysis();

    public void endAnalysis();
}