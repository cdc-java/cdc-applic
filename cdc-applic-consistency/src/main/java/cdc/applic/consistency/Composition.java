package cdc.applic.consistency;

/**
 * Enumeration of possible composition relationships between items.
 *
 * @author Damien Carbonne
 * @deprecated Use the new consistency code.
 */
@Deprecated(since = "2024-12-31", forRemoval = true)
public enum Composition {
    AT_MOST_ONE,

    EXACTLY_ONE,

    AT_LEAST_ONE,

    ANY
}