package cdc.applic.consistency.model;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.Repository;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.clauses.ProverImpl;
import cdc.util.lang.Checks;

public final class CyModel extends CyAbstractElement implements CyParent {
    private final Repository repository;
    private final List<CyBlock> children = new ArrayList<>();
    private final Map<String, CyElement> idToElement = new HashMap<>();
    /** The Includes to fix. When they were created, the included Block was unknown. */
    private final Map<String, Set<CyInclude>> toFix = new HashMap<>();

    private final Map<Dictionary, DictionaryHandle> handles = new HashMap<>();
    private final Map<Dictionary, Prover> provers = new HashMap<>();

    final void addElement(CyElement element) {
        // The added element is a Model, Reference, Include or Block
        Checks.assertFalse(idToElement.containsKey(element.getId()), "Duplicate element id {}", element.getId());
        idToElement.put(element.getId(), element);

        if (element instanceof final CyInclude inc) {
            // The added element is an Include
            if (idToElement.containsKey(inc.getTargetId())) {
                // The referenced Block is known: fix the added Include
                inc.fix();
            } else {
                // The included block is unknown: the added Include is registered for later fix
                final Set<CyInclude> set = toFix.computeIfAbsent(element.getId(), k -> new HashSet<>());
                set.add(inc);
            }
        } else if (element instanceof CyBlock && toFix.containsKey(element.getId())) {
            // The added element is a Block and some Include that reference it need to be fixed.
            for (final CyInclude include : toFix.get(element.getId())) {
                include.fix();
            }
            toFix.remove(element.getId());
        }
    }

    final Optional<DictionaryHandle> getDictionaryHandle(Optional<Dictionary> dictionary) {
        if (dictionary.isPresent()) {
            return Optional.ofNullable(handles.computeIfAbsent(dictionary.orElseThrow(), DictionaryHandle::new));
        } else {
            return Optional.empty();
        }
    }

    final Optional<Prover> getProver(Optional<Dictionary> dictionary) {
        final Optional<DictionaryHandle> handle = getDictionaryHandle(dictionary);
        if (handle.isPresent()) {
            return Optional.of(provers.computeIfAbsent(dictionary.orElseThrow(),
                                                       p -> new ProverImpl(handle.orElseThrow(),
                                                                           ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Adds a Node to the list of children if it is a BlokByDef and it is owned by this model.
     *
     * @param candidate The possible child.
     */
    final void addPossibleChild(CyAbstractNode candidate) {
        Checks.assertTrue(candidate.getOwner() == this, "Candidate is not owned by this model.");
        if (candidate instanceof final CyBlock bbd) {
            children.add(bbd);
        }
    }

    private CyModel(Builder builder) {
        super(builder);
        this.repository = Checks.isNotNull(builder.repository, "repository");
        postProcess();
    }

    /**
     * @return The repository.
     */
    @Override
    public Repository getRepository() {
        return repository;
    }

    @Override
    public List<CyBlock> getChildren() {
        return children;
    }

    @Override
    public CyModel getModel() {
        return this;
    }

    /**
     * @return The set of registered element identifiers.
     */
    public Set<String> getElementIds() {
        return idToElement.keySet();
    }

    /**
     * @param id The identifier.
     * @return {@code true} if this model contains an element identified by {@code id}.
     */
    public boolean hasElementWithId(String id) {
        return idToElement.containsKey(id);
    }

    /**
     * @param id The identifier.
     * @return An Optional of the element identified by {@code id}.
     */
    public Optional<CyElement> getElementWithId(String id) {
        return Optional.ofNullable(idToElement.get(id));
    }

    /**
     * @param <X> the element type.
     * @param id The identifier.
     * @param elementClass The element class.
     * @return An Optional of the element identified by {@code id} as a {@code X}.
     * @throws ClassCastException When an element identified by {@code id} exists but can not be cast to {@code elementClass}.
     */
    public <X extends CyElement> Optional<X> getElementWithId(String id,
                                                              Class<X> elementClass) {
        final CyElement element = idToElement.get(id);
        return Optional.ofNullable(elementClass.cast(element));
    }

    public CyBlock.Builder block() {
        return CyBlock.builder(this);
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        printIdTitle(out);
        out.println();
        indent(out, level + 1);
        out.println(getHierarchy());
        indent(out, level + 1);
        printDictionary(out);
        out.println();

        for (final CyBlock root : getChildren()) {
            root.print(out, level + 1);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends CyAbstractElement.Builder<Builder> {
        private Repository repository;

        private Builder() {
            super(null);
        }

        @Override
        protected Repository getRepository() {
            return repository;
        }

        public Builder repository(Repository repository) {
            this.repository = repository;
            return self();
        }

        @Override
        public CyModel build() {
            return new CyModel(this);
        }
    }
}