package cdc.applic.consistency.model;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.Repository;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.proofs.Prover;
import cdc.issues.locations.Location;
import cdc.util.debug.Printable;
import cdc.util.lang.Checks;

/**
 * Base implementation of {@link CyElement}.
 */
public abstract class CyAbstractElement implements CyElement, Printable {
    /** Optional owner (may be null). */
    private final CyAbstractElement owner;
    /** Mandatory id. */
    private final String id;
    /** Optional title. */
    private final String name;
    private final Optional<Dictionary> dictionary;

    protected CyAbstractElement(Builder<?> builder) {
        this.owner = (CyAbstractElement) builder.owner;
        this.id = Checks.isNotNull(builder.id, "id");
        this.name = builder.name;
        this.dictionary = Optional.ofNullable(builder.dictionary);
    }

    /**
     * <ul>
     * <li>Register this element in the model.
     * <li>Possibly add this element to the children list of its owner.
     * </ul>
     */
    protected final void postProcess() {
        getModel().addElement(this);

        if (this instanceof final CyAbstractNode node) {
            if (node.getOwner() instanceof final CyModel m) {
                m.addPossibleChild(node);
            } else if (node.getOwner() instanceof final CyBlock bbd) {
                bbd.addChild(node);
            }
        }
    }

    /**
     * @return The element's owner.
     */
    protected CyElement getOwner() {
        return owner;
    }

    @Override
    public final String getId() {
        return id;
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public final Optional<Dictionary> getDictionary() {
        return dictionary;
    }

    @Override
    public final Optional<Dictionary> getEffectiveDictionary() {
        if (dictionary.isPresent()) {
            // There is a local dictionary
            return dictionary;
        } else if (owner != null) {
            // Retrieve the effective dictionary of the owner
            return owner.getEffectiveDictionary();
        } else {
            // There is no owner and no local dictionary
            return Optional.empty();
        }
    }

    /**
     * @return The {@link DictionaryHandle} associated to this element.
     */
    public final Optional<DictionaryHandle> getDictionaryHandle() {
        return getModel().getDictionaryHandle(getEffectiveDictionary());
    }

    /**
     * @return The {@link Prover} associated to this element.
     */
    public final Optional<Prover> getProver() {
        return getModel().getProver(getEffectiveDictionary());
    }

    @Override
    public final List<CyElement> getHierarchy() {
        final List<CyElement> list = new ArrayList<>();
        CyAbstractElement iter = this;
        while (iter != null) {
            list.add(iter);
            iter = iter.owner;
        }
        Collections.reverse(list);
        return list;
    }

    @Override
    public final Location getLocation() {
        return CyLocation.of(this);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "@" + getId();
    }

    protected void printIdTitle(PrintStream out) {
        out.print(getClass().getSimpleName());
        out.print("@");
        out.print(getId());
        out.print(" '");
        out.print(getName());
        out.print("' ");
    }

    protected void printDictionary(PrintStream out) {
        if (getDictionary().isPresent()) {
            out.print(getDictionary().orElseThrow().getPath());
        } else {
            out.print("???");
        }
    }

    public abstract static class Builder<B extends Builder<B>> {
        protected final CyElement owner;
        private String id;
        protected String name;
        private Dictionary dictionary;

        protected Builder(CyElement owner) {
            this.owner = owner;
        }

        @SuppressWarnings("unchecked")
        protected final B self() {
            return (B) this;
        }

        protected abstract Repository getRepository();

        /**
         * Sets the element's identifier.
         *
         * @param id The identifier.
         * @return This Builder.
         */
        public final B id(String id) {
            this.id = id;
            return self();
        }

        /**
         * Sets the element's name.
         *
         * @param name The name.
         * @return This Builder.
         */
        public final B name(String name) {
            this.name = name;
            return self();
        }

        public final B dictionary(Dictionary dictionary) {
            this.dictionary = dictionary;
            return self();
        }

        public final B dictionary(String path) {
            return dictionary(path == null ? null : getRepository().getDictionary(path));
        }

        /**
         * @return A new element instance.
         */
        public abstract CyElement build();
    }
}