package cdc.applic.consistency.model;

import java.util.Objects;
import java.util.regex.Pattern;

import cdc.util.lang.Checks;

/**
 * Local part of an element location. It is a triplet:
 * <ul>
 * <li>mandatory code, corresponding to the element class.
 * <li>optional element title.
 * <li>optional element id.
 * </ul>
 * The string representation is {@code code@title[id]}, where {@code title} and {@code id} are optional.
 */
public final class CyLocationPart {
    /** The code pattern: only upper case letters. */
    public static final Pattern CODE_PATTERN = Pattern.compile("[A-Z]+");
    /** The id pattern */
    public static final Pattern ID_PATTERN = Pattern.compile("[^\\[\\]]*");
    public static final char SEPARATOR = '@';
    public static final char OPEN = '[';
    public static final char CLOSE = ']';
    public static final String NULL = "";

    /** The code associated to the element type. */
    private final String code;
    /** The local name of the element. */
    private final String title;
    /** The element id. */
    private final String id;

    private CyLocationPart(String code,
                           String title,
                           String id) {
        this.code = Checks.isNotNull(code, "code");
        this.title = title == null ? NULL : title;
        this.id = id == null ? NULL : id;
        Checks.isTrue(CODE_PATTERN.matcher(this.code).matches(), "Invalid code {}", this.code);
        Checks.isTrue(ID_PATTERN.matcher(this.id).matches(), "Invalid id {}", this.id);
    }

    /**
     * @param element The element.
     * @return A new instance of CyLocationPart corresponding to {@code element}.
     */
    public static CyLocationPart of(CyElement element) {
        Checks.isNotNull(element, "element");

        final String code = element.getCode();
        return new CyLocationPart(code,
                                  element.getName(),
                                  element.getId());
    }

    /**
     * @param s The string representation of the part.
     * @return A new instance of MfLocationPart corresponding to {@code s}.
     * @throws IllegalArgumentException When {@code s} can not be correctly interpreted.
     */
    public static CyLocationPart of(String s) {
        final int sepLoc = s.indexOf(SEPARATOR);
        if (sepLoc > 0) {
            final String code = s.substring(0, sepLoc);
            final String tail = s.substring(sepLoc + 1);
            final int openLoc = tail.lastIndexOf(OPEN);
            if (openLoc >= 0) {
                final String name = tail.substring(0, openLoc);
                final String id = tail.substring(openLoc + 1, tail.length() - 1);
                return new CyLocationPart(code, name, id);
            }
        }
        throw new IllegalArgumentException("Invalid text: " + s);
    }

    /**
     * @return The code.
     */
    public String getCode() {
        return code;
    }

    /**
     * @return The element class, derived from code.
     */
    public Class<? extends CyElement> getElementClass() {
        return CyUtils.codeToClass(code);
    }

    public boolean hasValidName() {
        return !title.isEmpty();
    }

    /**
     * @return The element name. May be empty.
     */
    public String getTitle() {
        return title;
    }

    public boolean hasValidId() {
        return !id.isEmpty();
    }

    /**
     * @return The element id. May be empty.
     */
    public String getId() {
        return id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code,
                            title,
                            id);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof CyLocationPart)) {
            return false;
        }
        final CyLocationPart other = (CyLocationPart) object;
        return Objects.equals(code, other.code)
                && Objects.equals(title, other.title)
                && Objects.equals(id, other.id);
    }

    @Override
    public String toString() {
        return code + SEPARATOR + title + OPEN + id + CLOSE;
    }
}