package cdc.applic.consistency.model;

import java.io.PrintStream;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Expressions;

/**
 * Base implementation of nodes that have a target:
 * <ul>
 * <li>{@link CyInclude}
 * <li>{@link CyReference}
 * </ul>
 */
public abstract class CyAbstractNodeWithTarget extends CyAbstractNode {
    private final String targetId;

    protected CyAbstractNodeWithTarget(Builder<?> builder) {
        super(builder);
        this.targetId = builder.targetId;
    }

    @Override
    public final CyBlock getOwner() {
        return (CyBlock) super.getOwner();
    }

    @Override
    public final Set<CyBlock> getParents() {
        return Set.of(getOwner());
    }

    @Override
    public final List<? extends CyAbstractNode> getChildren() {
        return Collections.emptyList();
    }

    /**
     * @return The identifier of the block targeted by this node.
     */
    public final String getTargetId() {
        return targetId;
    }

    /**
     * @return The block targeted by this node.
     */
    public final Optional<CyBlock> getTarget() {
        return getModel().getElementWithId(getTargetId(), CyBlock.class);
    }

    @Override
    public final Expression getActualApplicability() {
        final Expression parentActual = getOwner().getActualApplicability();
        final Expression local = getApplicability();
        final Expression effective = Expressions.SHORT_NARROW_NO_SIMPLIFY.and(parentActual, local);
        return simplify(effective);
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        printIdTitle(out);
        out.print(" -> @");
        out.print(getTargetId());
        out.println();

        indent(out, level + 1);
        out.println(getIndices());

        indent(out, level + 1);
        out.println(getHierarchy());

        indent(out, level + 1);
        printDictionaryApplicability(out);
        out.println();
    }

    public abstract static class Builder<B extends Builder<B>> extends CyAbstractNode.Builder<B> {
        private String targetId;

        protected Builder(CyBlock owner) {
            super(owner);
        }

        public B targetId(String targetId) {
            this.targetId = targetId;
            return self();
        }
    }
}