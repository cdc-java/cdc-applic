package cdc.applic.consistency.model;

/**
 * Inclusion of a {@link CyBlock}.
 */
public final class CyInclude extends CyAbstractNodeWithTarget {
    private CyBlock target = null;

    private CyInclude(Builder builder) {
        super(builder);
        postProcess();
    }

    void fix() {
        if (target == null) {
            target = getModel().getElementWithId(getTargetId(), CyBlock.class).orElse(null);
            if (target != null) {
                target.addPossibleParent(getOwner());
            }
        }
    }

    static Builder builder(CyBlock owner) {
        return new Builder(owner);
    }

    public static final class Builder extends CyAbstractNodeWithTarget.Builder<Builder> {
        protected Builder(CyBlock owner) {
            super(owner);
        }

        @Override
        public CyInclude build() {
            return new CyInclude(this);
        }
    }
}