package cdc.applic.consistency.model.io;

import java.io.File;
import java.io.IOException;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import cdc.applic.consistency.model.CyAbstractNode;
import cdc.applic.consistency.model.CyBlock;
import cdc.applic.consistency.model.CyComposition;
import cdc.applic.consistency.model.CyElement;
import cdc.applic.consistency.model.CyInclude;
import cdc.applic.consistency.model.CyModel;
import cdc.applic.consistency.model.CyReference;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.Expression;
import cdc.io.xml.AbstractStAXLoader;
import cdc.io.xml.AbstractStAXParser;
import cdc.io.xml.XmlWriter;
import cdc.util.lang.FailureReaction;

public final class CyModelXmlIo {
    private static final String APPLICABILITY = "applicability";
    private static final String BLOCK = "block";
    private static final String COMPOSITION = "composition";
    private static final String DICTIONARY = "dictionary";
    private static final String ID = "id";
    private static final String INCLUDE = "include";
    private static final String MODEL = "model";
    private static final String NAME = "name";
    private static final String REFERENCE = "reference";
    private static final String TARGET_ID = "target-id";

    private CyModelXmlIo() {
    }

    public static void save(CyModel model,
                            File file) throws IOException {
        try (final XmlWriter writer = new XmlWriter(file)) {
            writer.setIndentString("  ");
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
            Printer.write(writer, model);
        }
    }

    public static CyModel load(File file,
                               RepositoryImpl repository) throws IOException {
        final StAXLoader loader = new StAXLoader(FailureReaction.FAIL, repository);
        return loader.load(file);
    }

    public static final class Printer {
        private Printer() {
        }

        public static void write(XmlWriter writer,
                                 CyModel model) throws IOException {
            final String namespace = "https://www.gitlab.com/cdc-java";
            final String schema = "https://www.gitlab.com/cdc-java/applic-consistency-model.xsd";
            writer.beginDocument();
            writer.beginElement(MODEL);
            writer.addDefaultNamespace(namespace);
            writer.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            writer.addAttribute("xsi:schemaLocation",
                                namespace + " " + schema);

            writeElementAtts(writer, model);

            for (final CyBlock root : model.getChildren()) {
                writeNode(writer, root);
            }

            writer.endElement();
            writer.endDocument();
        }

        private static void writeNode(XmlWriter writer,
                                      CyAbstractNode node) throws IOException {
            if (node instanceof final CyBlock block) {
                writeBlock(writer, block);
            } else if (node instanceof final CyInclude include) {
                writeInclude(writer, include);
            } else {
                writeReference(writer, (CyReference) node);
            }
        }

        private static void writeElementAtts(XmlWriter writer,
                                             CyElement element) throws IOException {
            writer.addAttribute(ID, element.getId());
            if (element.getName() != null) {
                writer.addAttribute(NAME, element.getName());
            }
            if (element.getDictionary().isPresent()) {
                writer.addAttribute(DICTIONARY, element.getDictionary().orElseThrow().getPath());
            }
        }

        private static void writeNodeAtts(XmlWriter writer,
                                          CyAbstractNode node) throws IOException {
            writeElementAtts(writer, node);
            if (!node.getApplicability().equals(Expression.TRUE)) {
                writer.addAttribute(APPLICABILITY, node.getApplicability().getContent());
            }
        }

        private static void writeBlock(XmlWriter writer,
                                       CyBlock block) throws IOException {
            writer.beginElement(BLOCK);

            writeNodeAtts(writer, block);

            if (block.getComposition() != CyComposition.ANY) {
                writer.addAttribute(COMPOSITION, block.getComposition());
            }

            for (final CyAbstractNode child : block.getChildren()) {
                writeNode(writer, child);
            }

            writer.endElement();
        }

        private static void writeInclude(XmlWriter writer,
                                         CyInclude include) throws IOException {
            writer.beginElement(INCLUDE);
            writeNodeAtts(writer, include);
            writer.addAttribute(TARGET_ID, include.getTargetId());
            writer.endElement();
        }

        private static void writeReference(XmlWriter writer,
                                           CyReference reference) throws IOException {
            writer.beginElement(REFERENCE);
            writeNodeAtts(writer, reference);
            writer.addAttribute(TARGET_ID, reference.getTargetId());
            writer.endElement();
        }
    }

    public static class StAXLoader extends AbstractStAXLoader<CyModel> {
        public StAXLoader(FailureReaction reaction,
                          RepositoryImpl repository) {
            super((reader,
                   systemId) -> new Parser(reader, systemId, reaction, repository));
        }

        private static class Parser extends AbstractStAXParser<CyModel> {
            private final RepositoryImpl repository;

            protected Parser(XMLStreamReader reader,
                             String systemId,
                             FailureReaction reaction,
                             RepositoryImpl repository) {
                super(reader, systemId, reaction);
                this.repository = repository;
            }

            @Override
            protected CyModel parse() throws XMLStreamException {
                trace("parse()");
                // Move to root start tag
                nextTag();

                if (isStartElement(MODEL)) {
                    final CyModel result = parseModel();
                    next();
                    return result;
                } else {
                    throw unexpectedEvent();
                }
            }

            /**
             * Must be called when cursor is on {@link #MODEL} start tag.
             *
             * @return The parsed consistency data.
             * @throws XMLStreamException When an exception occurs.
             */
            private CyModel parseModel() throws XMLStreamException {
                trace("parseModel()");
                expectStartElement("parseModel()", MODEL);

                final String id = getAttributeValue(ID);
                final String name = getAttributeValue(NAME, null);
                final String dictionaryPath = getAttributeValue(DICTIONARY, null);

                final CyModel model = CyModel.builder()
                                             .id(id)
                                             .name(name)
                                             .repository(repository)
                                             .dictionary(dictionaryPath)
                                             .build();

                // Move to next start tag or to end tag of repository
                nextTag();

                while (isStartElement(BLOCK)) {
                    parseBlock(model.block());
                    nextTag();
                }

                return model;
            }

            private void parseBlock(CyBlock.Builder builder) throws XMLStreamException {
                final String ctx = "parseBlock()";
                trace(ctx);
                expectStartElement(ctx, BLOCK);

                final String id = getAttributeValue(ID, null);
                final String name = getAttributeValue(NAME, null);
                final String dictionaryPath = getAttributeValue(DICTIONARY, null);
                final String applicability = getAttributeValue(APPLICABILITY, "true");
                final CyComposition composition = getAttributeAsEnum(COMPOSITION, CyComposition.class, CyComposition.ANY);

                final CyBlock block = builder.id(id)
                                             .name(name)
                                             .dictionary(dictionaryPath)
                                             .composition(composition)
                                             .applicability(applicability)
                                             .build();

                nextTag();
                parseBlockChildren(block);
                expectEndElement(ctx, BLOCK);
            }

            private void parseBlockChildren(CyBlock owner) throws XMLStreamException {
                final String ctx = "parseBlockChildren()";
                trace(ctx);

                while (reader.isStartElement()) {
                    if (isStartElement(BLOCK)) {
                        parseBlock(owner.block());
                    } else if (isStartElement(INCLUDE)) {
                        parseInclude(owner);
                    } else if (isStartElement(REFERENCE)) {
                        parseReference(owner);
                    } else {
                        throw unexpectedEvent();
                    }
                    nextTag();
                }
            }

            private void parseInclude(CyBlock owner) throws XMLStreamException {
                final String ctx = "parseInclude()";
                trace(ctx);
                expectStartElement(ctx, INCLUDE);

                final String id = getAttributeValue(ID, null);
                final String name = getAttributeValue(NAME, null);
                final String dictionaryPath = getAttributeValue(DICTIONARY, null);
                final String applicability = getAttributeValue(APPLICABILITY, "true");
                final String targetId = getAttributeValue(TARGET_ID, null);
                owner.include()
                     .id(id)
                     .name(name)
                     .dictionary(dictionaryPath)
                     .applicability(applicability)
                     .targetId(targetId)
                     .build();

                nextTag();
                expectEndElement(ctx, INCLUDE);
            }

            private void parseReference(CyBlock owner) throws XMLStreamException {
                final String ctx = "parseReference()";
                trace(ctx);
                expectStartElement(ctx, REFERENCE);

                final String id = getAttributeValue(ID, null);
                final String name = getAttributeValue(NAME, null);
                final String dictionaryPath = getAttributeValue(DICTIONARY, null);
                final String applicability = getAttributeValue(APPLICABILITY, "true");
                final String targetId = getAttributeValue(TARGET_ID, null);

                owner.reference()
                     .id(id)
                     .name(name)
                     .dictionary(dictionaryPath)
                     .applicability(applicability)
                     .targetId(targetId)
                     .build();

                nextTag();
                expectEndElement(ctx, REFERENCE);
            }
        }
    }
}