package cdc.applic.consistency.model;

/**
 * Enumeration of possible composition relationships between a block and its children.
 *
 * @author Damien Carbonne
 */
public enum CyComposition {
    AT_MOST_ONE,

    EXACTLY_ONE,

    AT_LEAST_ONE,

    ANY
}