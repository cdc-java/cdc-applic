package cdc.applic.consistency.model;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import cdc.applic.dictionaries.Repository;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.simplification.Simplifier;
import cdc.applic.simplification.SimplifierFeatures;
import cdc.applic.simplification.core.SimplifierImpl;

/**
 * Base implementation of nodes.
 */
public abstract class CyAbstractNode extends CyAbstractElement {
    private final Expression applicability;

    protected CyAbstractNode(Builder<?> builder) {
        super(builder);
        this.applicability = builder.applicability;
    }

    @Override
    public final CyModel getModel() {
        return getOwner().getModel();
    }

    @Override
    public final Repository getRepository() {
        return getModel().getRepository();
    }

    /**
     * @return The node's owner.
     */
    @Override
    public CyParent getOwner() {
        return (CyParent) super.getOwner();
    }

    /**
     * @return A set of all parents of this block.
     *         A Block may have zero, one or more parents.
     *         A Reference or Include have one parent.
     */
    public abstract Set<CyBlock> getParents();

    /**
     * @return The index of this element among the children of its owner.
     */
    public final int getIndex() {
        return getOwner().getChildren().indexOf(this);
    }

    public final List<Integer> getIndices() {
        final List<Integer> list = new ArrayList<>();
        CyAbstractNode iter = this;
        while (iter != null) {
            list.add(iter.getIndex());
            iter = iter.getOwner() instanceof final CyAbstractNode next ? next : null;
        }
        Collections.reverse(list);
        return list;
    }

    /**
     * Returns the applicability restriction of this node.
     * <p>
     * The restriction is relative to its parents.<br>
     * True shall be used to indicate no restriction.
     *
     * @return The applicability restriction of this node.
     */
    public final Expression getApplicability() {
        return applicability;
    }

    public abstract Expression getActualApplicability();

    protected final Expression simplify(Expression expression) {
        final Optional<DictionaryHandle> handle = getDictionaryHandle();
        if (handle.isPresent()) {
            try {
                final Simplifier simplifier = new SimplifierImpl(handle.orElseThrow());
                final SimplifierFeatures features =
                        SimplifierFeatures.builder()
                                          .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)
                                          .allHints()
                                          .build();
                return simplifier.simplify(expression, features).getValue();
            } catch (final RuntimeException e) {
                return expression;
            }
        } else {
            return expression;
        }
    }

    protected void printDictionaryApplicability(PrintStream out) {
        printDictionary(out);
        out.print(" ");
        out.print(getApplicability());
    }

    public abstract static class Builder<B extends Builder<B>> extends CyAbstractElement.Builder<B> {
        private Expression applicability = Expression.TRUE;

        protected Builder(CyParent owner) {
            super(owner);
        }

        @Override
        protected Repository getRepository() {
            return owner.getRepository();
        }

        public final B applicability(Expression applicability) {
            this.applicability = applicability;
            return self();
        }

        public final B applicability(String applicability) {
            return applicability(Expression.of(applicability, false));
        }
    }
}