package cdc.applic.consistency.model;

/**
 * Reference to a {@link CyBlock}.
 */
public final class CyReference extends CyAbstractNodeWithTarget {
    private CyReference(Builder builder) {
        super(builder);
        postProcess();
    }

    static Builder builder(CyBlock owner) {
        return new Builder(owner);
    }

    public static final class Builder extends CyAbstractNodeWithTarget.Builder<Builder> {
        private Builder(CyBlock owner) {
            super(owner);
        }

        @Override
        public CyReference build() {
            return new CyReference(this);
        }
    }
}