package cdc.applic.consistency.model;

/**
 * Interface implemented by elements that can effectively have children:
 * <ul>
 * <li>{@link CyModel}
 * <li>{@link CyBlock}
 * </ul>
 * <b>Note:</b>
 */
public interface CyParent extends CyElement {
    // Ignore
}