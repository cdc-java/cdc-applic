package cdc.applic.consistency.model;

import java.util.Optional;

import cdc.issues.locations.AbstractLocation;
import cdc.issues.locations.Location;
import cdc.issues.locations.Locations;
import cdc.util.lang.Checks;

/**
 * Implementation of {@link Location} dedicated to Consistency Model elements.
 */
public final class CyLocation extends AbstractLocation {
    public static final String TAG = "CyLocation";

    static {
        Locations.register(TAG, CyLocation::of);
    }

    private final String path;
    private final String anchor;

    private CyLocation(String path,
                       String anchor) {
        this.path = Checks.isNotNullOrEmpty(path, "path");
        this.anchor = anchor; // May be null
    }

    private CyLocation(CyElement element,
                       String anchor) {
        this.path = CyLocationPart.of(element).toString();
        this.anchor = anchor;
    }

    public static void elaborate() {
        // Ignore
    }

    /**
     * @param element The element.
     * @param anchor The anchor
     * @return A new instance of CyLocation.
     */
    public static CyLocation of(CyElement element,
                                String anchor) {
        return new CyLocation(element, anchor);
    }

    /**
     * @param element The element.
     * @return A new instance of CyLocation.
     */
    public static CyLocation of(CyElement element) {
        return new CyLocation(element, null);
    }

    /**
     * @param path The path.
     * @param anchor The anchor.
     * @return A new instance of MfLocation.
     */
    public static CyLocation of(String path,
                                String anchor) {
        return new CyLocation(path, anchor);
    }

    /**
     * @param path The path.
     * @return A new instance of MfLocation.
     */
    public static CyLocation of(String path) {
        return of(path, null);
    }

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public String getAnchor() {
        return anchor;
    }

    /**
     * Returns the element corresponding to this location in a model.
     * <p>
     * It is the caller responsibility to make sure that the location and model are consistent.
     *
     * @param model The model.
     * @return The element corresponding to this location in {@code model}.
     */
    public Optional<CyElement> resolve(CyModel model) {
        final CyLocationPart part = CyLocationPart.of(path);
        final Optional<CyElement> element = model.getElementWithId(part.getId());
        if (element.isPresent() && part.getCode().equals(element.orElseThrow().getCode())) {
            return element;
        } else {
            return Optional.empty();
        }
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return super.equals(object);
    }
}