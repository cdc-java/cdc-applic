package cdc.applic.consistency.model.io;

import java.io.File;
import java.io.IOException;

import cdc.applic.consistency.model.CyModel;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.io.xml.XmlWriter;
import cdc.util.files.Files;

public final class CyModelIo {
    private CyModelIo() {
    }

    /**
     * Enumeration of formats supported by {@link CyModelIo}.
     *
     * @author Damien Carbonne
     */
    public enum Format {
        XML;

        public static Format from(File file) {
            final String ext = Files.getExtension(file);
            for (final Format format : values()) {
                if (format.name().equalsIgnoreCase(ext)) {
                    return format;
                }
            }
            return null;
        }
    }

    public static void save(CyModel model,
                            File file) throws IOException {
        final Format format = Format.from(file);
        if (format == Format.XML) {
            try (XmlWriter writer = new XmlWriter(file)) {
                writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT, true);
                CyModelXmlIo.Printer.write(writer, model);
            }
        } else {
            throw new IllegalArgumentException("Can not save model to " + file);
        }
    }

    public static CyModel load(File file,
                                   RepositoryImpl repository) throws IOException {
        final Format format = Format.from(file);
        if (format == Format.XML) {
            return CyModelXmlIo.load(file, repository);
        } else {
            throw new IllegalArgumentException("Can not load model from " + file);
        }
    }
}