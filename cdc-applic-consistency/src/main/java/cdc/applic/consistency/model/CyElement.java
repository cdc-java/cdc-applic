package cdc.applic.consistency.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.Repository;
import cdc.issues.locations.LocatedItem;

/**
 * Base interface of all model elements:
 * <ul>
 * <li>{@link CyModel}
 * <li>{@link CyBlock}
 * <li>{@link CyInclude}
 * <li>{@link CyReference}
 * </ul>
 */
public interface CyElement extends LocatedItem {
    public static final Comparator<CyElement> ID_COMPARATOR =
            Comparator.comparing(CyElement::getId);

    public default String getKind() {
        return CyUtils.getKind(this);
    }

    public default String getCode() {
        return CyUtils.getCode(this);
    }

    /**
     * @return The element's identifier.
     */
    public String getId();

    /**
     * @return The element's name.
     */
    public String getName();

    /**
     * @return The element's model, possibly itself.
     */
    public CyModel getModel();

    /**
     * @return The model's repository.
     */
    public Repository getRepository();

    /**
     * @return The Dictionary that should be used for applicability of this element or its children (recursively).
     */
    public Optional<Dictionary> getDictionary();

    /**
     * @return The dictionary that should effectively be used for this element.
     *         If this element has an associated dictionary, then it is this one.
     *         Otherwise, it is the effective dictionary of its owner.
     */
    public Optional<Dictionary> getEffectiveDictionary();

    /**
     * @return The children of this element.
     *         Note that only classes that implement {@link CyParent} can have children.
     */
    public List<? extends CyAbstractNode> getChildren();

    /**
     * @return A List of elements, from model to this element.
     */
    public List<? extends CyElement> getHierarchy();

    /**
     * Traverses all elements and invokes a consumer on elements that match a class and predicate.
     *
     * @param <E> The element type.
     * @param elementClass The element class.
     * @param consumer The element consumer.
     * @param predicate The element predicate.
     */
    public default <E extends CyElement> void traverse(Class<E> elementClass,
                                                       Consumer<? super E> consumer,
                                                       Predicate<? super E> predicate) {
        if (elementClass.isInstance(this)) {
            final E x = elementClass.cast(this);
            if (predicate.test(x)) {
                consumer.accept(x);
            }
        }
        for (final CyElement child : getChildren()) {
            child.traverse(elementClass, consumer, predicate);
        }
    }

    /**
     * Traverses all elements and invokes a consumer on elements that match a class.
     *
     * @param <E> The element type.
     * @param elementClass The element class.
     * @param consumer The element consumer.
     */
    public default <E extends CyElement> void traverse(Class<E> elementClass,
                                                       Consumer<? super E> consumer) {
        traverse(elementClass, consumer, x -> true);
    }

    /**
     * @param <E> The element type.
     * @param elementClass The element class.
     * @param predicate The element predicate.
     * @return A List of all elements that are instances of {@code elementClass} and match {@code predicate}.
     */
    public default <E extends CyElement> List<E> collect(Class<E> elementClass,
                                                         Predicate<? super E> predicate) {
        final List<E> list = new ArrayList<>();
        traverse(elementClass, list::add, predicate);
        return list;
    }

    /**
     * @param <E> The element type.
     * @param elementClass The element class.
     * @return A List of all elements that are instances of {@code elementClass}.
     */
    public default <E extends CyElement> List<E> collect(Class<E> elementClass) {
        return collect(elementClass, x -> true);
    }

    // TODO Meta data
}