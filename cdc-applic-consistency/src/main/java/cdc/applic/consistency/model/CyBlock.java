package cdc.applic.consistency.model;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Expressions;
import cdc.util.lang.Checks;

/**
 * Block definition.
 */
public final class CyBlock extends CyAbstractNode implements CyParent {
    private final CyComposition composition;
    private final List<CyAbstractNode> children = new ArrayList<>();
    private final Set<CyBlock> parents = new HashSet<>();

    void addChild(CyAbstractNode child) {
        children.add(child);
    }

    void addPossibleParent(CyElement parent) {
        if (parent instanceof final CyBlock bbd) {
            parents.add(bbd);
        }
    }

    private CyBlock(Builder builder) {
        super(builder);
        this.composition = Checks.isNotNull(builder.composition, "composition");
        postProcess();
        addPossibleParent(getOwner());
    }

    /**
     * @return The composition rule that this block should follow.
     */
    public CyComposition getComposition() {
        return composition;
    }

    @Override
    public List<CyAbstractNode> getChildren() {
        return children;
    }

    @Override
    public Set<CyBlock> getParents() {
        return parents;
    }

    @Override
    public Expression getActualApplicability() {
        final List<Expression> effectives = new ArrayList<>();
        for (final CyBlock parent : getParents()) {
            effectives.add(parent.getActualApplicability());
        }
        effectives.add(getApplicability());
        return simplify(Expressions.SHORT_NARROW_NO_SIMPLIFY.and(effectives));
    }

    public CyBlock.Builder block() {
        return CyBlock.builder(this);
    }

    public CyInclude.Builder include() {
        return CyInclude.builder(this);
    }

    public CyReference.Builder reference() {
        return CyReference.builder(this);
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        printIdTitle(out);
        out.print(" ");
        out.print(getComposition());
        out.println();

        indent(out, level + 1);
        out.println(getIndices());

        indent(out, level + 1);
        out.println(getHierarchy());

        indent(out, level + 1);
        printDictionaryApplicability(out);
        out.println();

        for (final CyAbstractNode child : getChildren()) {
            child.print(out, level + 1);
        }
    }

    static Builder builder(CyParent owner) {
        return new Builder(owner);
    }

    public static final class Builder extends CyAbstractNode.Builder<Builder> {
        private CyComposition composition = CyComposition.ANY;

        protected Builder(CyParent owner) {
            super(owner);
        }

        public Builder composition(CyComposition composition) {
            this.composition = composition;
            return self();
        }

        @Override
        public CyBlock build() {
            return new CyBlock(this);
        }
    }
}