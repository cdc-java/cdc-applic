package cdc.applic.consistency.model;

import cdc.issues.locations.Location;
import cdc.util.encoding.ExtensionEncoder;

public final class CyUtils {
    @SuppressWarnings("rawtypes")
    private static final ExtensionEncoder<Class, String> KINDS =
            new ExtensionEncoder<>(Class.class, String.class);
    @SuppressWarnings("rawtypes")
    private static final ExtensionEncoder<Class, String> CODES =
            new ExtensionEncoder<>(Class.class, String.class);

    static {
        put(CyModel.class,
            "model");
        put(CyInclude.class,
            "include");
        put(CyBlock.class,
            "block");
        put(CyReference.class,
            "reference");
        KINDS.lock();
        CODES.lock();
    }

    private static <B extends CyAbstractElement.Builder<B>>
            void put(Class<? extends CyAbstractElement> elementClass,
                     String kind) {
        KINDS.put(elementClass, kind);
        CODES.put(elementClass, kind.toUpperCase());
    }

    private CyUtils() {
    }

    public static String validate(String s) {
        return s == null ? "" : s;
    }

    public static String getKind(Class<? extends CyElement> cls) {
        return KINDS.encode(cls);
    }

    public static String getCode(Class<? extends CyElement> cls) {
        return CODES.encode(cls);
    }

    /**
     * @param item The item.
     * @return A string representing the item kind.
     */
    public static String getKind(CyElement item) {
        return KINDS.encode(item.getClass());
    }

    /**
     * @param item The item.
     * @return A string representing the item code.
     */
    public static String getCode(CyElement item) {
        return CODES.encode(item.getClass());
    }

    /**
     * @param kind The kind.
     * @return The class corresponding to {@code kind}.
     */
    public static Class<? extends CyElement> kindToClass(String kind) {
        @SuppressWarnings("unchecked")
        final Class<? extends CyElement> tmp = KINDS.decode(kind);
        return tmp;
    }

    /**
     * @param code The code.
     * @return The class corresponding to {@code code}.
     */
    public static Class<? extends CyElement> codeToClass(String code) {
        @SuppressWarnings("unchecked")
        final Class<? extends CyElement> tmp = CODES.decode(code);
        return tmp;
    }

    public static String identify(CyElement item) {
        return Location.toString(item.getLocation(), false);
    }
}