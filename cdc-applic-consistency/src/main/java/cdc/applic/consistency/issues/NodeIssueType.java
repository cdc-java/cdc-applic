package cdc.applic.consistency.issues;

import cdc.issues.IssueSeverity;
import cdc.issues.IssueSeverityItem;

@Deprecated(since = "2024-12-31", forRemoval = true)
public enum NodeIssueType implements IssueSeverityItem {
    /**
     * A Node has a null dictionary.
     * <p>
     * This can prevent further checks.
     */
    NULL_DICTIONARY(IssueSeverity.BLOCKER),

    /**
     * The target of a Reference is null.
     */
    NULL_TARGET(IssueSeverity.CRITICAL),

    ACTUAL_CHILD_PARENT_COMPLIANCE(IssueSeverity.CRITICAL),

    /**
     * The actual applicability of a Reference if not compliant with the actual applicability of the target Block.
     */
    ACTUAL_REF_TARGET_COMPLIANCE(IssueSeverity.CRITICAL),

    AT_LEAST_ONE_COMPLIANCE(IssueSeverity.CRITICAL),
    EXACTLY_ONE_COMPLIANCE(IssueSeverity.CRITICAL),
    AT_MOST_ONE_COMPLIANCE(IssueSeverity.CRITICAL);

    private final IssueSeverity severity;

    private NodeIssueType(IssueSeverity severity) {
        this.severity = severity;
    }

    @Override
    public IssueSeverity getSeverity() {
        return severity;
    }
}