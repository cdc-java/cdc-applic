package cdc.applic.consistency.issues;

import cdc.issues.IssueSeverity;
import cdc.issues.IssueSeverityItem;

@Deprecated(since = "2024-12-31", forRemoval = true)
public enum GlobalIssueType implements IssueSeverityItem {
    /**
     * All policies are not based on the same registry.
     * <p>
     * This can prevent further checks.
     */
    REGISTRY_CONSISTENCY(IssueSeverity.BLOCKER);

    private final IssueSeverity severity;

    private GlobalIssueType(IssueSeverity severity) {
        this.severity = severity;
    }

    @Override
    public IssueSeverity getSeverity() {
        return severity;
    }
}