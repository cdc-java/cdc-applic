package cdc.applic.consistency.issues;

import cdc.applic.consistency.ConsistencyCatalog;
import cdc.issues.Issue;

@Deprecated(since = "2024-12-31", forRemoval = true)
public final class ConsistencyIssues {
    private ConsistencyIssues() {
    }

    public static Issue.Builder<?> builder() {
        return Issue.builder().domain(ConsistencyCatalog.DOMAIN);
    }
}