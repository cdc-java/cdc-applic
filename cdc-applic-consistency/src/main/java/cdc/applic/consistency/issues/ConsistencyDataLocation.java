package cdc.applic.consistency.issues;

import cdc.applic.consistency.ConsistencyData;
import cdc.issues.locations.DefaultLocation;

@Deprecated(since = "2024-12-31", forRemoval = true)
public class ConsistencyDataLocation extends DefaultLocation {
    private final String nodeId;

    public ConsistencyDataLocation(ConsistencyData<?, ?, ?, ?> data) {
        super(data.getSystemId());
        this.nodeId = null;
    }

    public <N> ConsistencyDataLocation(ConsistencyData<N, ?, ?, ?> data,
                                       N node) {
        super(data.getSystemId(),
              ConsistencyData.getPath(data, node, true));
        this.nodeId = data.getNodeId(node).toString();
    }

    public String getNodeId() {
        return nodeId;
    }
}