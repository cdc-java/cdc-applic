package cdc.applic.consistency.checks.models;

import cdc.applic.consistency.checks.CyProfile;
import cdc.applic.consistency.checks.elements.ElementNameIsRecommended;
import cdc.applic.consistency.checks.nodes.NodesChecker;
import cdc.applic.consistency.model.CyModel;
import cdc.issues.Issue;
import cdc.issues.IssuesCollector;
import cdc.issues.checks.RootChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;

public class CyModelChecker extends RootChecker<CyModel> {
    public CyModelChecker(String projectName,
                          CyModel model,
                          IssuesCollector<Issue> issuesCollector) {
        super(SnapshotManager.builder()
                             .projectName(projectName)
                             .profile(CyProfile.PROFILE)
                             .issuesCollector(issuesCollector)
                             .stats(true)
                             .build(),
              CyModel.class,
              LocatedObject.of(model));

        add(new ElementNameIsRecommended(getManager()));
        add(new ModelDictionariesMustBeCompliant(getManager()));
        add(new NodesChecker(getManager()));
    }

    public static SnapshotManager check(String projectName,
                                        CyModel model,
                                        IssuesCollector<Issue> issuesCollector) {
        final CyModelChecker checker =
                new CyModelChecker(projectName,
                                   model,
                                   issuesCollector);

        checker.check();
        return checker.getManager();
    }
}