package cdc.applic.consistency.checks.nodes;

import java.util.Optional;

import cdc.applic.consistency.checks.CyAbstractRuleChecker;
import cdc.applic.consistency.checks.CyIssueDescription;
import cdc.applic.consistency.checks.CyRuleUtils;
import cdc.applic.consistency.model.CyBlock;
import cdc.applic.consistency.model.CyReference;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.Prover;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class ReferenceActualApplicabilityMustBeCompliantWithTargetActualApplicability
        extends CyAbstractRuleChecker<CyReference> {
    public static final String NAME = "CYRA1";
    public static final String TITLE = "Reference actual applicability must be compliant the actual applicability of its target";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;

    public static final Rule RULE =
            CyRuleUtils.rule(NAME,
                             TITLE,
                             description -> description.define("The {%wrap} of a {%wrap} must be compliant with (included into) the {%wrap} of its {%wrap}.",
                                                               N_ACTUAL + WS + N_APPLICABILITY,
                                                               N_REFERENCE,
                                                               N_ACTUAL + WS + N_APPLICABILITY,
                                                               N_TARGET)
                                                       .appliesTo("References"),
                             SEVERITY)
                       .build();

    public ReferenceActualApplicabilityMustBeCompliantWithTargetActualApplicability(SnapshotManager manager) {
        super(manager,
              CyReference.class,
              RULE);
    }

    @Override
    protected String getHeader(CyReference object) {
        return getTheActualApplicabilityHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             CyReference object,
                             Location location) {
        // The reference actual applicability
        final Expression actualApplic = object.getActualApplicability();
        // The prover
        final Optional<Prover> prover = object.getProver();
        // The target (referenced block)
        final Optional<CyBlock> target = object.getTarget();

        // The target actual applicability
        if (prover.isPresent() && target.isPresent()) {
            final Expression targetActualApplic = target.orElseThrow().getActualApplicability();
            final boolean compliant = prover.orElseThrow().contains(targetActualApplic, actualApplic);

            if (!compliant) {
                final CyIssueDescription.Builder description = CyIssueDescription.builder();

                description.text(getHeader(object))
                           .text(": '" + actualApplic.getContent() + "'")
                           .text(" is not included into the actual applicability of its target:")
                           .uItems(target.orElseThrow().getLocation().toString(false)
                                   + ": '" + targetActualApplic.getContent() + "'");

                add(issue().description(description)
                           .location(object)
                           .build());
                return CheckResult.FAILURE;
            } else {
                return CheckResult.SUCCESS;
            }
        } else {
            return CheckResult.SKIPPED;
        }
    }
}