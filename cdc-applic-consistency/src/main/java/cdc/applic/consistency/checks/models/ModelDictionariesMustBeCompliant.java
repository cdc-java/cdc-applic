package cdc.applic.consistency.checks.models;

import java.util.HashSet;
import java.util.Set;

import cdc.applic.consistency.checks.CyAbstractRuleChecker;
import cdc.applic.consistency.checks.CyIssueDescription;
import cdc.applic.consistency.checks.CyRuleUtils;
import cdc.applic.consistency.model.CyElement;
import cdc.applic.consistency.model.CyModel;
import cdc.applic.dictionaries.Registry;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class ModelDictionariesMustBeCompliant extends CyAbstractRuleChecker<CyModel> {
    public static final String NAME = "CYMD1";
    public static final String TITLE = "Model dictionaries must be compliant";
    public static final IssueSeverity SEVERITY = IssueSeverity.BLOCKER;

    public static final Rule RULE =
            CyRuleUtils.rule(NAME,
                             TITLE,
                             description -> description.define("All {%wrap} usd in a {%wrap} must belong to the same {%wrap}.",
                                                               N_DICTIONARIES,
                                                               N_MODEL,
                                                               N_REGISTRY)
                                                       .appliesTo("Models"),
                             SEVERITY)
                       .build();

    public ModelDictionariesMustBeCompliant(SnapshotManager manager) {
        super(manager,
              CyModel.class,
              RULE);
    }

    @Override
    protected String getHeader(CyModel object) {
        return getHeader("The dictionaries of ", object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             CyModel object,
                             Location location) {
        final Set<Registry> registries = new HashSet<>();
        object.traverse(CyElement.class,
                        e -> {
                            if (e.getDictionary().isPresent()) {
                                registries.add(e.getDictionary().orElseThrow().getRegistry());
                            }
                        });
        if (registries.size() > 1) {
            final CyIssueDescription.Builder description = CyIssueDescription.builder();

            description.text(getHeader(object))
                       .text(" belong to differents registries:")
                       .uItems(registries.stream()
                                         .map(Registry::getName)
                                         .sorted()
                                         .toList());

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}