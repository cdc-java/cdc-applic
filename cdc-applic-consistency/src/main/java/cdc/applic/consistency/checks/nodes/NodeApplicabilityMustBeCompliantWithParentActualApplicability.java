package cdc.applic.consistency.checks.nodes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import cdc.applic.consistency.checks.CyAbstractRuleChecker;
import cdc.applic.consistency.checks.CyIssueDescription;
import cdc.applic.consistency.checks.CyRuleUtils;
import cdc.applic.consistency.model.CyAbstractNode;
import cdc.applic.consistency.model.CyBlock;
import cdc.applic.consistency.model.CyElement;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.Prover;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class NodeApplicabilityMustBeCompliantWithParentActualApplicability extends CyAbstractRuleChecker<CyAbstractNode> {
    public static final String NAME = "CYNA2";
    public static final String TITLE = "Node applicability must be compliant the actual applicability of its parents";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;

    public static final Rule RULE =
            CyRuleUtils.rule(NAME,
                             TITLE,
                             description -> description.define("The {%wrap} of a {%wrap} must be compliant with the {%wrap} of its {%wrap}.",
                                                               N_APPLICABILITY,
                                                               N_NODE,
                                                               N_ACTUAL + WS + N_APPLICABILITY,
                                                               N_PARENT + S)
                                                       .appliesTo("Blocks",
                                                                  "Includes",
                                                                  "References"),
                             SEVERITY)
                       .build();

    public NodeApplicabilityMustBeCompliantWithParentActualApplicability(SnapshotManager manager) {
        super(manager,
              CyAbstractNode.class,
              RULE);
    }

    @Override
    protected String getHeader(CyAbstractNode object) {
        return getTheApplicabilityHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             CyAbstractNode object,
                             Location location) {
        // The node applicability
        final Expression applic = object.getApplicability();
        // The prover
        final Optional<Prover> prover = object.getProver();
        // The parents for which there is a compliance issue
        final List<CyBlock> problematicParents = new ArrayList<>();

        if (prover.isPresent()) {
            // The node parents
            final Set<CyBlock> parents = object.getParents();

            for (final CyBlock parent : parents) {
                final Expression parentApplic = parent.getActualApplicability();
                final boolean success = prover.orElseThrow().intersects(parentApplic, applic);
                if (!success) {
                    problematicParents.add(parent);
                }
            }
            problematicParents.sort(CyElement.ID_COMPARATOR);
        }

        if (!problematicParents.isEmpty()) {
            final CyIssueDescription.Builder description = CyIssueDescription.builder();

            description.text(getHeader(object))
                       .text(": '" + applic.getContent() + "'")
                       .text(" is not compliant with actual applicability of its parent(s):")
                       .uItems(problematicParents.stream()
                                                 .map(p -> p.getLocation().toString(false)
                                                         + ": '" + p.getActualApplicability() + "'")
                                                 .toList());

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}