package cdc.applic.consistency.checks.nodes;

import cdc.applic.consistency.checks.elements.ElementNameIsRecommended;
import cdc.applic.consistency.model.CyAbstractNode;
import cdc.applic.consistency.model.CyAbstractNodeWithTarget;
import cdc.applic.consistency.model.CyBlock;
import cdc.applic.consistency.model.CyInclude;
import cdc.applic.consistency.model.CyReference;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.LazyChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.checks.SpecializationChecker;

public class NodeChecker extends CompositeChecker<CyAbstractNode> {
    public NodeChecker(SnapshotManager manager) {
        super(manager,
              CyAbstractNode.class,
              // All Nodes
              new ElementNameIsRecommended(manager),
              new NodeEffectiveDictionaryIsMandatory(manager),
              new NodeApplicabilityMustBeSyntacticallyCorrect(manager),
              new NodeApplicabilityMustBeSemanticallyValid(manager),
              new NodeApplicabilityMustBeCompliantWithWritingRules(manager),
              new NodeApplicabilityMustBeCompliantWithParentActualApplicability(manager),
              // Nodes with target
              new SpecializationChecker<>(manager,
                                          CyAbstractNode.class,
                                          new CompositeChecker<>(manager,
                                                                 CyAbstractNodeWithTarget.class,
                                                                 new NodeTargetIsMandatory(manager),
                                                                 new NodeTargetMustBeValid(manager))),
              // Includes
              new SpecializationChecker<>(manager,
                                          CyAbstractNode.class,
                                          new CompositeChecker<>(manager,
                                                                 CyInclude.class,
                                                                 new IncludeActualApplicabilityMustBeCompliantWithTargetActualApplicability(manager))),
              // References
              new SpecializationChecker<>(manager,
                                          CyAbstractNode.class,
                                          new CompositeChecker<>(manager,
                                                                 CyReference.class,
                                                                 new ReferenceActualApplicabilityMustBeCompliantWithTargetActualApplicability(manager))),
              // Blocks
              new SpecializationChecker<>(manager,
                                          CyAbstractNode.class,
                                          new CompositeChecker<>(manager,
                                                                 CyBlock.class,
                                                                 new BlockCompositionMustBeCompliantWithChildrenActualApplicability(manager),
                                                                 new LazyChecker<CyBlock>(manager,
                                                                                          CyBlock.class,
                                                                                          NodesChecker.KEY))));
    }
}