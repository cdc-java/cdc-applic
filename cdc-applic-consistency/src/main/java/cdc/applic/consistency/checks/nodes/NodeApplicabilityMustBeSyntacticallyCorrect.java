package cdc.applic.consistency.checks.nodes;

import cdc.applic.consistency.checks.CyAbstractRuleChecker;
import cdc.applic.consistency.checks.CyIssueDescription;
import cdc.applic.consistency.checks.CyRuleUtils;
import cdc.applic.consistency.model.CyAbstractNode;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.checks.SyntaxChecker;
import cdc.issues.Diagnosis;
import cdc.issues.Issue;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class NodeApplicabilityMustBeSyntacticallyCorrect extends CyAbstractRuleChecker<CyAbstractNode> {
    public static final String NAME = "CYNA4";
    public static final String TITLE = "Node applicability must be syntactically correct";
    public static final IssueSeverity SEVERITY = IssueSeverity.BLOCKER;

    public static final Rule RULE =
            CyRuleUtils.rule(NAME,
                             TITLE,
                             description -> description.define("The {%wrap} of a {%wrap} must be syntactically correct.",
                                                               N_APPLICABILITY,
                                                               N_NODE)
                                                       .appliesTo("Blocks",
                                                                  "Includes",
                                                                  "References"),
                             SEVERITY)
                       .build();

    public NodeApplicabilityMustBeSyntacticallyCorrect(SnapshotManager manager) {
        super(manager,
              CyAbstractNode.class,
              RULE);
    }

    @Override
    protected String getHeader(CyAbstractNode object) {
        return getTheApplicabilityHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             CyAbstractNode object,
                             Location location) {
        final Expression applic = object.getApplicability();

        final Diagnosis<?> diagnosis =
                SyntaxChecker.INSTANCE.check(applic);

        if (!diagnosis.isOk()) {
            final CyIssueDescription.Builder description = CyIssueDescription.builder();

            description.text(getHeader(object))
                       .text(": '" + applic.getContent() + "'")
                       .text(" is syntactically incorrect:")
                       .uItems(diagnosis.getIssues()
                                        .stream()
                                        .map(Issue::getDescription)
                                        .toList());

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}