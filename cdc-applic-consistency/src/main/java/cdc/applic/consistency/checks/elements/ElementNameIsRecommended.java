package cdc.applic.consistency.checks.elements;

import cdc.applic.consistency.checks.CyAbstractRuleChecker;
import cdc.applic.consistency.checks.CyIssueDescription;
import cdc.applic.consistency.checks.CyRuleUtils;
import cdc.applic.consistency.model.CyElement;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.util.strings.StringUtils;

public class ElementNameIsRecommended extends CyAbstractRuleChecker<CyElement> {
    public static final String NAME = "CYEN1";
    public static final String TITLE = "Element name is recommended";
    public static final IssueSeverity SEVERITY = IssueSeverity.MINOR;

    public static final Rule RULE =
            CyRuleUtils.rule(NAME,
                             TITLE,
                             description -> description.define("All {%wrap} should have a {%wrap}.",
                                                               N_ELEMENT + S,
                                                               N_NAME)
                                                       .appliesTo("Models",
                                                                  "Blocks",
                                                                  "Includes",
                                                                  "References"),
                             SEVERITY)
                       .build();

    public ElementNameIsRecommended(SnapshotManager manager) {
        super(manager,
              CyElement.class,
              RULE);
    }

    @Override
    protected String getHeader(CyElement object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             CyElement object,
                             Location location) {
        if (StringUtils.isNullOrEmpty(object.getName())) {
            final CyIssueDescription.Builder description = CyIssueDescription.builder();

            description.text(getTheNameHeader(object))
                       .text(" is undefined.");

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}