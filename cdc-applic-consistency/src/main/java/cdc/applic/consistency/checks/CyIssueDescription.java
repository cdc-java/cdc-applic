package cdc.applic.consistency.checks;

import java.util.Collection;

import cdc.applic.consistency.model.CyElement;
import cdc.applic.consistency.model.CyUtils;
import cdc.issues.StructuredDescription;

public class CyIssueDescription extends StructuredDescription {
    protected CyIssueDescription(Builder builder) {
        super(builder);
    }

    public static CyIssueDescription.Builder builder() {
        return new CyIssueDescription.Builder();
    }

    public static class Builder extends StructuredDescription.Builder<Builder> {
        protected Builder() {
            super();
        }

        /**
         * Appends an element as a list item.
         *
         * @param element The element.
         * @return This Builder.
         */
        public Builder element(CyElement element) {
            return uItem(CyUtils.identify(element));
        }

        /**
         * Appends an element as a list item at a given indentation level.
         *
         * @param level The list indentation level.
         * @param element The element.
         * @return This Builder.
         */
        public Builder element(int level,
                               CyElement element) {
            return uItem(level, CyUtils.identify(element));
        }

        /**
         * Appends a collection of elements as list items.
         *
         * @param elements The elements.
         * @return This Builder.
         */
        public Builder elements(Collection<? extends CyElement> elements) {
            for (final CyElement element : elements) {
                element(element);
            }
            return self();
        }

        /**
         * Appends a collection of elements as list items at a given indentation level.
         *
         * @param level The list indentation level.
         * @param elements The elements.
         * @return This Builder.
         */
        public Builder elements(int level,
                                Collection<? extends CyElement> elements) {
            for (final CyElement element : elements) {
                element(level, element);
            }
            return self();
        }

        @Override
        public CyIssueDescription build() {
            return new CyIssueDescription(this);
        }
    }
}