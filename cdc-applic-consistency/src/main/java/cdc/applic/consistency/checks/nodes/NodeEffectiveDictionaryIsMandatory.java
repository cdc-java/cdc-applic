package cdc.applic.consistency.checks.nodes;

import cdc.applic.consistency.checks.CyAbstractRuleChecker;
import cdc.applic.consistency.checks.CyIssueDescription;
import cdc.applic.consistency.checks.CyRuleUtils;
import cdc.applic.consistency.model.CyAbstractNode;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class NodeEffectiveDictionaryIsMandatory extends CyAbstractRuleChecker<CyAbstractNode> {
    public static final String NAME = "CYND1";
    public static final String TITLE = "Node effective dictionary is mandatory";
    public static final IssueSeverity SEVERITY = IssueSeverity.BLOCKER;

    public static final Rule RULE =
            CyRuleUtils.rule(NAME,
                             TITLE,
                             description -> description.define("""
                                                               All {%wrap} must have an {%wrap}.
                                                               It should be locally defined or inherited.""",
                                                               N_NODE + S,
                                                               N_EFFECTIVE + WS + N_DICTIONARY)
                                                       .appliesTo("Blocks",
                                                                  "Includes",
                                                                  "References"),
                             SEVERITY)
                       .build();

    public NodeEffectiveDictionaryIsMandatory(SnapshotManager manager) {
        super(manager,
              CyAbstractNode.class,
              RULE);
    }

    @Override
    protected String getHeader(CyAbstractNode object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             CyAbstractNode object,
                             Location location) {

        if (object.getEffectiveDictionary().isEmpty()) {
            final CyIssueDescription.Builder description = CyIssueDescription.builder();

            description.text(getHeader(object))
                       .text(" has no effective dictionary.");

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}