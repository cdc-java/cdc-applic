package cdc.applic.consistency.checks;

import cdc.applic.consistency.checks.elements.ElementNameIsRecommended;
import cdc.applic.consistency.checks.models.ModelDictionariesMustBeCompliant;
import cdc.applic.consistency.checks.nodes.BlockCompositionMustBeCompliantWithChildrenActualApplicability;
import cdc.applic.consistency.checks.nodes.IncludeActualApplicabilityMustBeCompliantWithTargetActualApplicability;
import cdc.applic.consistency.checks.nodes.NodeApplicabilityMustBeCompliantWithParentActualApplicability;
import cdc.applic.consistency.checks.nodes.NodeApplicabilityMustBeCompliantWithWritingRules;
import cdc.applic.consistency.checks.nodes.NodeApplicabilityMustBeSemanticallyValid;
import cdc.applic.consistency.checks.nodes.NodeApplicabilityMustBeSyntacticallyCorrect;
import cdc.applic.consistency.checks.nodes.NodeEffectiveDictionaryIsMandatory;
import cdc.applic.consistency.checks.nodes.NodeTargetIsMandatory;
import cdc.applic.consistency.checks.nodes.NodeTargetMustBeValid;
import cdc.applic.consistency.checks.nodes.ReferenceActualApplicabilityMustBeCompliantWithTargetActualApplicability;
import cdc.issues.impl.ProfileImpl;
import cdc.issues.rules.Rule;

/**
 * Repository of checked rules.
 *
 * @author Damien Carbonne
 */
public final class CyProfile {
    private CyProfile() {
    }

    public static final String DOMAIN = "Applic";

    private static void add(Rule rule) {
        PROFILE.add(rule);
    }

    public static final ProfileImpl PROFILE =
            new ProfileImpl("Applic Consistency Profile").setDescription("Rules used to check an Applic Consistency Model.");
    static {
        // Elements
        add(ElementNameIsRecommended.RULE);

        // Models
        add(ModelDictionariesMustBeCompliant.RULE);

        // Nodes
        add(NodeApplicabilityMustBeCompliantWithParentActualApplicability.RULE);
        add(NodeApplicabilityMustBeCompliantWithWritingRules.RULE);
        add(NodeApplicabilityMustBeSemanticallyValid.RULE);
        add(NodeApplicabilityMustBeSyntacticallyCorrect.RULE);
        add(NodeEffectiveDictionaryIsMandatory.RULE);

        // Blocks
        add(BlockCompositionMustBeCompliantWithChildrenActualApplicability.RULE);

        // Nodes with target
        add(NodeTargetIsMandatory.RULE);
        add(NodeTargetMustBeValid.RULE);

        // Includes
        add(IncludeActualApplicabilityMustBeCompliantWithTargetActualApplicability.RULE);

        // References
        add(ReferenceActualApplicabilityMustBeCompliantWithTargetActualApplicability.RULE);
    }
}