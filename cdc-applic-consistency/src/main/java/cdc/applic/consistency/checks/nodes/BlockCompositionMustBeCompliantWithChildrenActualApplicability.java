package cdc.applic.consistency.checks.nodes;

import java.util.List;
import java.util.Optional;

import cdc.applic.consistency.checks.CyAbstractRuleChecker;
import cdc.applic.consistency.checks.CyIssueDescription;
import cdc.applic.consistency.checks.CyRuleUtils;
import cdc.applic.consistency.model.CyAbstractNode;
import cdc.applic.consistency.model.CyBlock;
import cdc.applic.consistency.model.CyComposition;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.Prover;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class BlockCompositionMustBeCompliantWithChildrenActualApplicability
        extends CyAbstractRuleChecker<CyBlock> {
    public static final String NAME = "CYBC1";
    public static final String TITLE =
            "Block composition must be compliant the actual applicability of its children";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;

    public static final Rule RULE =
            CyRuleUtils.rule(NAME,
                             TITLE,
                             description -> description.define("The {%wrap} and {ẅrap} of a {%wrap} must be compliant with the {%wrap} of its {%wrap}.",
                                                               N_ACTUAL + WS + N_APPLICABILITY,
                                                               N_COMPOSITION,
                                                               N_BLOCK,
                                                               N_ACTUAL + WS + N_APPLICABILITY,
                                                               N_CHILDREN)
                                                       .appliesTo("Blocks"),
                             SEVERITY)
                       .build();

    public BlockCompositionMustBeCompliantWithChildrenActualApplicability(SnapshotManager manager) {
        super(manager,
              CyBlock.class,
              RULE);
    }

    @Override
    protected String getHeader(CyBlock object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             CyBlock object,
                             Location location) {
        // The block composition
        final CyComposition composition = object.getComposition();
        // The prover
        final Optional<Prover> prover = object.getProver();

        if (composition == CyComposition.ANY || prover.isEmpty()) {
            return CheckResult.SKIPPED;
        } else {
            // The children
            final List<CyAbstractNode> children = object.getChildren();
            if (children.isEmpty()
                    && (composition == CyComposition.AT_LEAST_ONE || composition == CyComposition.EXACTLY_ONE)) {
                final CyIssueDescription.Builder description = CyIssueDescription.builder();

                description.text(getHeader(object))
                           .text(" has no children.");

                add(issue().description(description)
                           .location(object)
                           .build());
                return CheckResult.FAILURE;
            } else {
                // The block actual applicability
                final Expression actualApplic = object.getActualApplicability();
                final Expression[] childrenActualApplics =
                        children.stream()
                                .map(CyAbstractNode::getActualApplicability)
                                .toArray(Expression[]::new);
                if (composition == CyComposition.AT_LEAST_ONE) {
                    final boolean success = prover.orElseThrow().alwaysAtLeastOneInContext(actualApplic, childrenActualApplics);
                    if (!success) {
                        final CyIssueDescription.Builder description = CyIssueDescription.builder();

                        description.text(getHeader(object))
                                   .text(" does not have at least one applicable child.");

                        add(issue().description(description)
                                   .location(object)
                                   .build());
                        return CheckResult.FAILURE;
                    }
                } else if (composition == CyComposition.EXACTLY_ONE) {
                    final boolean success = prover.orElseThrow().alwaysExactlyOneInContext(actualApplic, childrenActualApplics);
                    if (!success) {
                        final CyIssueDescription.Builder description = CyIssueDescription.builder();

                        description.text(getHeader(object))
                                   .text(" does not have exactly one applicable child.");

                        add(issue().description(description)
                                   .location(object)
                                   .build());
                        return CheckResult.FAILURE;
                    }
                } else {
                    final boolean success = prover.orElseThrow().alwaysAtMostOneInContext(actualApplic, childrenActualApplics);
                    if (!success) {
                        final CyIssueDescription.Builder description = CyIssueDescription.builder();

                        description.text(getHeader(object))
                                   .text(" does not have at most one applicable child.");

                        add(issue().description(description)
                                   .location(object)
                                   .build());
                        return CheckResult.FAILURE;
                    }
                }
                return CheckResult.SUCCESS;
            }
        }
    }
}