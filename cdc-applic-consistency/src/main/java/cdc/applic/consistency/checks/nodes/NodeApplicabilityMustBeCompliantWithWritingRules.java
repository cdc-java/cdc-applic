package cdc.applic.consistency.checks.nodes;

import java.util.Optional;

import cdc.applic.consistency.checks.CyAbstractRuleChecker;
import cdc.applic.consistency.checks.CyIssueDescription;
import cdc.applic.consistency.checks.CyRuleUtils;
import cdc.applic.consistency.model.CyAbstractNode;
import cdc.applic.dictionaries.checks.SemanticChecker;
import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.checks.CheckedData;
import cdc.issues.Diagnosis;
import cdc.issues.Issue;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class NodeApplicabilityMustBeCompliantWithWritingRules extends CyAbstractRuleChecker<CyAbstractNode> {
    public static final String NAME = "CYNA1";
    public static final String TITLE = "Node applicability must be compliant with writing rules";
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;

    public static final Rule RULE =
            CyRuleUtils.rule(NAME,
                             TITLE,
                             description -> description.define("The {%wrap} of a {%wrap} must be compliant with writing rules of its {%wrap}.",
                                                               N_APPLICABILITY,
                                                               N_NODE,
                                                               N_ACTUAL + WS + N_DICTIONARY)
                                                       .appliesTo("Blocks",
                                                                  "Includes",
                                                                  "References"),
                             SEVERITY)
                       .build();

    public NodeApplicabilityMustBeCompliantWithWritingRules(SnapshotManager manager) {
        super(manager,
              CyAbstractNode.class,
              RULE);
    }

    @Override
    protected String getHeader(CyAbstractNode object) {
        return getTheApplicabilityHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             CyAbstractNode object,
                             Location location) {
        final Expression applic = object.getApplicability();
        final Optional<DictionaryHandle> handle = object.getDictionaryHandle();

        if (handle.isPresent()
                && SemanticChecker.areCompliant(handle.orElseThrow().getDictionary(), applic)) {
            final Diagnosis<?> diagnosis =
                    WritingRuleChecker.check(handle.orElseThrow(),
                                             new CheckedData("", applic));

            if (!diagnosis.isOk()) {
                final CyIssueDescription.Builder description = CyIssueDescription.builder();

                description.text(getHeader(object))
                           .text(": '" + applic.getContent() + "'")
                           .text(" is not compliant with writing rules:")
                           .uItems(diagnosis.getIssues()
                                            .stream()
                                            .map(Issue::getDescription)
                                            .toList());

                add(issue().description(description)
                           .location(object)
                           .build());
                return CheckResult.FAILURE;
            } else {
                return CheckResult.SUCCESS;
            }
        } else {
            return CheckResult.SKIPPED;
        }
    }
}