package cdc.applic.consistency.checks.nodes;

import java.util.List;

import cdc.applic.consistency.model.CyAbstractNode;
import cdc.applic.consistency.model.CyElement;
import cdc.applic.consistency.model.CyParent;
import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;

/**
 * Check the node parts (children) of a parent element.
 */
public class NodesChecker extends AbstractPartsChecker<CyParent, CyAbstractNode, NodesChecker> {
    public static final String KEY = NodesChecker.class.getSimpleName();

    public NodesChecker(SnapshotManager manager) {
        super(manager,
              CyParent.class,
              CyAbstractNode.class,
              new NodeChecker(manager));
        manager.register(KEY, this);
    }

    @Override
    protected List<LocatedObject<? extends CyAbstractNode>> getParts(CyParent object) {
        final List<CyAbstractNode> delegates = object.getChildren()
                                                     .stream()
                                                     .map(CyAbstractNode.class::cast)
                                                     .sorted(CyElement.ID_COMPARATOR)
                                                     .toList();
        return LocatedObject.locate(delegates);
    }
}