package cdc.applic.consistency.checks;

import cdc.applic.consistency.model.CyElement;
import cdc.applic.consistency.model.CyUtils;
import cdc.issues.checks.AbstractRuleChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;

public abstract class CyAbstractRuleChecker<O> extends AbstractRuleChecker<O> {
    protected static final String WS = " ";
    protected static final String S = "s";
    protected static final String ES = "es";

    protected static final String THE = RuleDescription.THE;

    protected static final String N_ACTUAL = "actual";
    protected static final String N_APPLICABILITIES = "applicabilities";
    protected static final String N_APPLICABILITY = "applicability";
    protected static final String N_BLOCK = "block";
    protected static final String N_CHILDREN = "children";
    protected static final String N_COMPOSITION = "composition";
    protected static final String N_DICTIONARY = "dictionary";
    protected static final String N_DICTIONARIES = "dictionaries";
    protected static final String N_EFFECTIVE = "effective";
    protected static final String N_ELEMENT = "element";
    protected static final String N_INCLUDE = "include";
    protected static final String N_MODEL = "model";
    protected static final String N_NAME = "name";
    protected static final String N_NODE = "node";
    protected static final String N_PARENT = "parent";
    protected static final String N_REFERENCE = "reference";
    protected static final String N_REGISTRY = "registry";
    protected static final String N_TARGET = "target";
    protected static final String N_VALID = "valid";

    protected CyAbstractRuleChecker(SnapshotManager manager,
                                    Class<O> objectClass,
                                    Rule rule) {
        super(manager,
              objectClass,
              rule);
    }

    protected static String getHeader(String prefix,
                                      CyElement object) {
        return prefix == null ? CyUtils.identify(object) : prefix + CyUtils.identify(object);
    }

    protected static String getTheItemHeader(CyElement item) {
        return getHeader("The ", item);
    }

    protected static String getTheNameHeader(CyElement item) {
        return getHeader("The name of ", item);
    }

    protected static String getTheTargetHeader(CyElement item) {
        return getHeader("The target of ", item);
    }

    protected static String getTheApplicabilityHeader(CyElement item) {
        return getHeader("The applicability of ", item);
    }

    protected static String getTheActualApplicabilityHeader(CyElement item) {
        return getHeader("The actual applicability of ", item);
    }
}