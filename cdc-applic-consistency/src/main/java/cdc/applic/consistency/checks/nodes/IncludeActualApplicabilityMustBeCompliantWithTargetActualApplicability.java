package cdc.applic.consistency.checks.nodes;

import java.util.Optional;

import cdc.applic.consistency.checks.CyAbstractRuleChecker;
import cdc.applic.consistency.checks.CyIssueDescription;
import cdc.applic.consistency.checks.CyRuleUtils;
import cdc.applic.consistency.model.CyBlock;
import cdc.applic.consistency.model.CyInclude;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.Prover;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class IncludeActualApplicabilityMustBeCompliantWithTargetActualApplicability
        extends CyAbstractRuleChecker<CyInclude> {
    public static final String NAME = "CYIA1";
    public static final String TITLE = "Include actual applicability must be compliant the actual applicability of its target";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;

    public static final Rule RULE =
            CyRuleUtils.rule(NAME,
                             TITLE,
                             description -> description.define("The {%wrap} of a {%wrap} must be compliant with (intersect) the {%wrap} of its {%wrap}.",
                                                               N_ACTUAL + WS + N_APPLICABILITY,
                                                               N_INCLUDE,
                                                               N_ACTUAL + WS + N_APPLICABILITY,
                                                               N_TARGET)
                                                       .appliesTo("Includes"),
                             SEVERITY)
                       .build();

    public IncludeActualApplicabilityMustBeCompliantWithTargetActualApplicability(SnapshotManager manager) {
        super(manager,
              CyInclude.class,
              RULE);
    }

    @Override
    protected String getHeader(CyInclude object) {
        return getTheActualApplicabilityHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             CyInclude object,
                             Location location) {
        // The include actual applicability
        final Expression actualApplic = object.getActualApplicability();
        // The prover
        final Optional<Prover> prover = object.getProver();
        // The target (included block)
        final Optional<CyBlock> target = object.getTarget();

        // The target actual applicability
        if (prover.isPresent() && target.isPresent()) {
            final Expression targetActualApplic = target.orElseThrow().getActualApplicability();
            final boolean compliant = prover.orElseThrow().intersects(targetActualApplic, actualApplic);

            if (!compliant) {
                final CyIssueDescription.Builder description = CyIssueDescription.builder();

                description.text(getHeader(object))
                           .text(": '" + actualApplic.getContent() + "'")
                           .text(" does not intersect the actual applicability of its target:")
                           .uItems(target.orElseThrow().getLocation().toString(false)
                                   + ": '" + targetActualApplic.getContent() + "'");

                add(issue().description(description)
                           .location(object)
                           .build());
                return CheckResult.FAILURE;
            } else {
                return CheckResult.SUCCESS;
            }
        } else {
            return CheckResult.SKIPPED;
        }
    }
}