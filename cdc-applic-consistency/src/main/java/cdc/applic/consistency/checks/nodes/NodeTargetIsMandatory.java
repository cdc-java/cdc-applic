package cdc.applic.consistency.checks.nodes;

import cdc.applic.consistency.checks.CyAbstractRuleChecker;
import cdc.applic.consistency.checks.CyIssueDescription;
import cdc.applic.consistency.checks.CyRuleUtils;
import cdc.applic.consistency.model.CyAbstractNodeWithTarget;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.util.strings.StringUtils;

public class NodeTargetIsMandatory extends CyAbstractRuleChecker<CyAbstractNodeWithTarget> {
    public static final String NAME = "CYNT1";
    public static final String TITLE = "Node target is mandatory";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;

    public static final Rule RULE =
            CyRuleUtils.rule(NAME,
                             TITLE,
                             description -> description.define("All {%wrap} and {%wrap} must have a {%wrap}.",
                                                               N_INCLUDE + S,
                                                               N_REFERENCE + S,
                                                               N_TARGET)
                                                       .appliesTo("Includes",
                                                                  "References"),
                             SEVERITY)
                       .build();

    public NodeTargetIsMandatory(SnapshotManager manager) {
        super(manager,
              CyAbstractNodeWithTarget.class,
              RULE);
    }

    @Override
    protected String getHeader(CyAbstractNodeWithTarget object) {
        return getTheTargetHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             CyAbstractNodeWithTarget object,
                             Location location) {

        if (StringUtils.isNullOrEmpty(object.getTargetId())) {
            final CyIssueDescription.Builder description = CyIssueDescription.builder();

            description.text(getHeader(object))
                       .text(" is undefined.");

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}