package cdc.applic.consistency.checks;

import java.util.function.Consumer;

import cdc.issues.IssueSeverity;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;

public final class CyRuleUtils {
    private CyRuleUtils() {
    }

    public static Rule.Builder rule(String name,
                                    String title,
                                    Consumer<RuleDescription.Builder<?>> descriptionBuilder,
                                    IssueSeverity severity) {
        final RuleDescription.Builder<?> description = RuleDescription.builder();
        descriptionBuilder.accept(description);
        return Rule.builder()
                   .domain(CyProfile.DOMAIN)
                   .name(name)
                   .title(title)
                   .description(description.build().toString())
                   .severity(severity);
    }

    public static Rule define(String name,
                              String title,
                              Consumer<RuleDescription.Builder<?>> descriptionBuilder,
                              IssueSeverity severity) {
        return rule(name, title, descriptionBuilder, severity).build();
    }
}