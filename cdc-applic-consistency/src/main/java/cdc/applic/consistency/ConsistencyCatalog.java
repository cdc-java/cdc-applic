package cdc.applic.consistency;

import cdc.applic.consistency.issues.GlobalIssueType;
import cdc.applic.consistency.issues.NodeIssueType;
import cdc.issues.impl.ProfileImpl;
import cdc.issues.rules.Profile;
import cdc.issues.rules.Rule;

/**
 * Definition of rules that are specific to consistency.
 *
 * @author Damien Carbonne
 * @deprecated Use the new consistency code.
 */
@Deprecated(since = "2024-12-31", forRemoval = true)
public final class ConsistencyCatalog {
    private ConsistencyCatalog() {
    }

    /**
     * Catalog of consistency specific rules.
     */
    public static final Profile PROFILE;

    public static final String DOMAIN = "Consistency";

    public static final Rule REGISTRY_CONSISTENCY =
            Rule.builder()
                .domain(DOMAIN)
                .name(GlobalIssueType.REGISTRY_CONSISTENCY)
                .description("In an applicability consistency dataset, "
                        + "configuration dictionaries are based on different registries.")
                .build();

    public static final Rule NULL_DICTIONARY =
            Rule.builder()
                .domain(DOMAIN)
                .name(NodeIssueType.NULL_DICTIONARY)
                .description("In an applicability consistency dataset, "
                        + "the configuration dictionary of a block or reference can not be found.")
                .build();

    public static final Rule NULL_TARGET =
            Rule.builder()
                .domain(DOMAIN)
                .name(NodeIssueType.NULL_TARGET)
                .description("In an applicability consistency dataset, "
                        + "a referenced block can not be found.")
                .build();

    public static final Rule ACTUAL_CHILD_PARENT_COMPLIANCE =
            Rule.builder()
                .domain(DOMAIN)
                .name(NodeIssueType.ACTUAL_CHILD_PARENT_COMPLIANCE)
                .description("In an applicability consistency dataset, "
                        + "the applicability of a reference or block is not compliant "
                        + "with the applicability of one of its parent block.")
                .build();

    public static final Rule ACTUAL_REF_TARGET_COMPLIANCE =
            Rule.builder()
                .domain(DOMAIN)
                .name(NodeIssueType.ACTUAL_REF_TARGET_COMPLIANCE)
                .description("In an applicability consistency dataset, "
                        + "the actual applicability of a reference is not compliant "
                        + "with the applicability of the referenced block.")
                .build();

    public static final Rule AT_LEAST_ONE_COMPLIANCE =
            Rule.builder()
                .domain(DOMAIN)
                .name(NodeIssueType.AT_LEAST_ONE_COMPLIANCE)
                .description("In an applicability consistency dataset, "
                        + "the applicability of a block is not covered by applicability "
                        + "of at least one child blocks.")
                .build();

    public static final Rule EXACTLY_ONE_COMPLIANCE =
            Rule.builder()
                .domain(DOMAIN)
                .name(NodeIssueType.EXACTLY_ONE_COMPLIANCE)
                .description("In an applicability consistency dataset, "
                        + "the applicability of a block is not covered by applicability "
                        + "of exactly one child blocks.")
                .build();

    public static final Rule AT_MOST_ONE_COMPLIANCE =
            Rule.builder()
                .domain(DOMAIN)
                .name(NodeIssueType.AT_MOST_ONE_COMPLIANCE)
                .description("In an applicability consistency dataset, "
                        + "the applicability of a block is not covered by applicability "
                        + "of at most one child blocks.")
                .build();

    static {
        final ProfileImpl profile = new ProfileImpl("ApplicConsistency");
        PROFILE = profile;
        profile.add(REGISTRY_CONSISTENCY);
        profile.add(NULL_DICTIONARY);
        profile.add(NULL_TARGET);
        profile.add(ACTUAL_CHILD_PARENT_COMPLIANCE);
        profile.add(ACTUAL_REF_TARGET_COMPLIANCE);
        profile.add(AT_LEAST_ONE_COMPLIANCE);
        profile.add(EXACTLY_ONE_COMPLIANCE);
        profile.add(AT_MOST_ONE_COMPLIANCE);
    }
}