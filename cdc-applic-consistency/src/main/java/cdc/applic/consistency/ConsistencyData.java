package cdc.applic.consistency;

import java.util.Objects;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.Registry;
import cdc.applic.expressions.Expression;
import cdc.util.strings.StringUtils;

/**
 * Interface describing a problem whose consistency must be checked.
 * <p>
 * The problem is viewed as a DAG of Nodes that have 2 concrete forms: Blocks and References.<br>
 * Blocks can have any number of parents Blocks and any number of children Nodes.<br>
 * References must have one parent Block and they target another Block.
 *
 * @author Damien Carbonne
 *
 * @param <N> The Node type.
 * @param <NID> The Node identifier type.
 * @param <B> The Block type.
 * @param <R> The Reference type.
 * @deprecated Use the new consistency code.
 */
@Deprecated(since = "2024-12-31", forRemoval = true)
public interface ConsistencyData<N, NID, B extends N, R extends N> {
    public String getSystemId();

    /**
     * @return The registry.
     */
    public Registry getRegistry();

    /**
     * @return An Iterable of all root Blocks.
     */
    public Iterable<? extends B> getRootBlocks();

    /**
     * Returns the Node identifier.
     *
     * @param node the node.
     * @return The identifier of {@code node}.
     */
    public NID getNodeId(N node);

    /**
     * Returns {@code true} if a node is a block.
     *
     * @param node The node.
     * @return {@code true} if {@code node} is a block.
     */
    public boolean isBlock(N node);

    /**
     * Returns {@code true} if a node is a reference.
     *
     * @param node The node.
     * @return {@code true} if {@code node} is a reference.
     */
    public boolean isReference(N node);

    /**
     * Return the label of a Node.
     *
     * @param node The Node.
     * @return The label used for {@code node}.
     */
    public String getNodeDisplayLabel(N node);

    /**
     * Returns the Dictionary that a Node should follow.
     *
     * @param node The Node.
     * @return The Dictionary that {@code node} should follow.
     */
    public Dictionary getNodeDictionary(N node);

    /**
     * Returns the applicability restriction of a Node.
     * <p>
     * The restriction is relative to its parents.<br>
     * True shall be used to indicate no restriction.
     *
     * @param node The Node.
     * @return The applicability restriction of {@code node}.
     */
    public Expression getNodeLocalApplicability(N node);

    /**
     * Returns an Iterable of all children (Block or Reference) of a Block.
     * <p>
     * Children can be Blocks or References.
     *
     * @param parent The parent block.
     * @return An Iterable of children of {@code parent}.
     */
    public Iterable<? extends N> getBlockChildren(B parent);

    /**
     * Returns an Iterable of all parents of a block.
     *
     * @param child The child block.
     * @return An Iterable of all parents of {@code child}.
     */
    public Iterable<? extends B> getBlockParents(B child);

    /**
     * Returns the composition rule that a block should follow.
     *
     * @param block The block.
     * @return The composition rule that {@code block} should follow.
     */
    public Composition getBlockComposition(B block);

    /**
     * Returns the block targeted by a Reference.
     *
     * @param ref The reference.
     * @return The block targeted by {@code ref}.
     */
    public B getReferenceTarget(R ref);

    public static <N> String getPath(ConsistencyData<N, ?, ?, ?> data,
                                     N node,
                                     boolean capital) {
        final StringBuilder builder = new StringBuilder();
        if (data.isBlock(node)) {
            builder.append(capital ? 'B' : 'b');
            builder.append("lock '");
        } else {
            builder.append(capital ? 'R' : 'r');
            builder.append("eference '");
        }
        builder.append(data.getNodeId(node));
        final String label = data.getNodeDisplayLabel(node);
        if (!StringUtils.isNullOrEmpty(label) && !Objects.equals(label, data.getNodeId(node))) {
            builder.append(" (" + label + ")");
        }
        builder.append('\'');
        return builder.toString();
    }
}