package cdc.applic.consistency;

import java.util.List;

import cdc.applic.consistency.handlers.ConsistencyHandler;
import cdc.issues.Issue;

/**
 * Interface implemented by classes that can check consistency of an applicability problem.
 *
 * @author Damien Carbonne
 *
 * @param <N> The Node type.
 * @param <NID> The Node identifier type.
 * @param <B> The Block type.
 * @param <R> The Reference type.
 * @deprecated Use the new consistency code.
 */
@Deprecated(since = "2024-12-31", forRemoval = true)
public interface ConsistencyChecker<N, NID, B extends N, R extends N> {
    /**
     * Check consistency data.
     *
     * @param data The data to check.
     * @param handler The events handler.
     */
    public void check(ConsistencyData<N, NID, B, R> data,
                      ConsistencyHandler handler);

    public List<Issue> check(ConsistencyData<N, NID, B, R> data);
}