package cdc.applic.consistency;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;

import javax.xml.transform.Source;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.Comparison;
import org.xmlunit.diff.ComparisonListener;
import org.xmlunit.diff.ComparisonResult;
import org.xmlunit.diff.DOMDifferenceEngine;
import org.xmlunit.diff.DifferenceEngine;
import org.xmlunit.validation.Languages;
import org.xmlunit.validation.ValidationProblem;
import org.xmlunit.validation.ValidationResult;
import org.xmlunit.validation.Validator;

import cdc.applic.consistency.model.CyBlock;
import cdc.applic.consistency.model.CyComposition;
import cdc.applic.consistency.model.CyInclude;
import cdc.applic.consistency.model.CyModel;
import cdc.applic.consistency.model.CyReference;
import cdc.applic.consistency.model.io.CyModelIo;
import cdc.applic.dictionaries.impl.RepositoryImpl;

class CyModelIoTest {
    private static final Logger LOGGER = LogManager.getLogger(CyModelIoTest.class);

    private static void assertSameModels(File file1,
                                         File file2) {
        final Source source1 = Input.from(file1).build();
        final Source source2 = Input.from(file2).build();
        final DifferenceEngine diff = new DOMDifferenceEngine();
        diff.addDifferenceListener(new ComparisonListener() {
            @Override
            public void comparisonPerformed(Comparison comparison,
                                            ComparisonResult outcome) {
                LOGGER.error("Found a difference: {}", comparison);
                assertFalse(true, "Found a difference: " + comparison);
            }
        });
        diff.compare(source1, source2);

        final Validator validator = Validator.forLanguage(Languages.W3C_XML_SCHEMA_NS_URI);
        validator.setSchemaSource(Input.fromFile(new File("src/main/resources/cdc/applic/applic-consistency-model.xsd")).build());
        final ValidationResult result = validator.validateInstance(Input.fromFile(file1).build());

        int count = 0;
        for (final ValidationProblem problem : result.getProblems()) {
            count++;
            LOGGER.error(" - {}) {}", count, problem);
        }
        assertSame(0, count);
    }

    void check(CyModel model,
               RepositoryImpl repository,
               String basename,
               Consumer<CyModel> verifier) throws IOException {
        final String prefix = "model-xml-io-test-";
        final File file1 = new File("target/", prefix + basename + "-1.xml");
        LOGGER.info("save {}", file1);
        CyModelIo.save(model, file1);
        verifier.accept(model);
        LOGGER.info("load {}", file1);
        final CyModel model2 = CyModelIo.load(file1, repository);
        final File file2 = new File("target/", prefix + basename + "-2.xml");
        LOGGER.info("save {}", file2);
        CyModelIo.save(model2, file2);

        assertSameModels(file1, file2);
    }

    @Test
    void test0() throws IOException {
        final RepositoryImpl repository =
                new RepositoryImpl();
        repository.registry()
                  .name("R1")
                  .build();

        final CyModel model =
                CyModel.builder()
                       .id("M0")
                       .name("M")
                       .repository(repository)
                       .dictionary("R1")
                       .build();
        assertNotNull(model);
        model.block()
             .id("B0")
             .name("B0")
             .build();
        final CyBlock b1 =
                model.block()
                     .id("B1")
                     .name("B1")
                     .composition(CyComposition.AT_MOST_ONE)
                     .build();
        b1.reference()
          .id("R1")
          .name("R1")
          .targetId("B0")
          .build();

        b1.include()
          .id("I0")
          .targetId("B0")
          .build();
        b1.include()
          .id("I1")
          .targetId("B0")
          .build();

        check(model,
              repository,
              "test0",
              m -> {
                  assertTrue(m.getElementWithId("M0").orElseThrow() instanceof CyModel);
                  assertTrue(m.getElementWithId("B0").orElseThrow() instanceof CyBlock);
                  assertTrue(m.getElementWithId("B1").orElseThrow() instanceof CyBlock);
                  assertTrue(m.getElementWithId("R1").orElseThrow() instanceof CyReference);
                  assertTrue(m.getElementWithId("I0").orElseThrow() instanceof CyInclude);
                  assertTrue(m.getElementWithId("I1").orElseThrow() instanceof CyInclude);
              });
    }
}