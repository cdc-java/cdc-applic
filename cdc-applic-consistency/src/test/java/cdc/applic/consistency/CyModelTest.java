package cdc.applic.consistency;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.consistency.model.CyBlock;
import cdc.applic.consistency.model.CyComposition;
import cdc.applic.consistency.model.CyInclude;
import cdc.applic.consistency.model.CyModel;
import cdc.applic.consistency.model.CyReference;
import cdc.applic.dictionaries.impl.RepositoryImpl;

class CyModelTest {
    private static final Logger LOGGER = LogManager.getLogger(CyModelTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.INFO).buildPrintStream();

    @Test
    void testModel() throws IOException {
        assertThrows(IllegalArgumentException.class,
                     () -> CyModel.builder()
                                      .build());
        assertThrows(IllegalArgumentException.class,
                     () -> CyModel.builder()
                                      .id("id")
                                      .build());
        assertThrows(IllegalArgumentException.class,
                     () -> CyModel.builder()
                                      .id("id")
                                      .name("title")
                                      .build());

        final RepositoryImpl repository = new RepositoryImpl();
        repository.registry()
                  .name("R1")
                  .build();

        final CyModel model = CyModel.builder()
                                             .id("M0")
                                             .name("M")
                                             .repository(repository)
                                             .dictionary("R1")
                                             .build();
        assertNotNull(model);
        final CyBlock b0 =
                model.block()
                     .id("B0")
                     .name("B0")
                     .build();
        final CyBlock b1 =
                model.block()
                     .id("B1")
                     .name("B1")
                     .composition(CyComposition.AT_MOST_ONE)
                     .build();
        final CyReference r1 =
                b1.reference()
                  .id("R1")
                  .name("R1")
                  .targetId("B0")
                  .build();

        final CyInclude i0 =
                b1.include()
                  .id("I0")
                  .targetId("B0")
                  .build();
        final CyInclude i1 =
                b1.include()
                  .id("I1")
                  .targetId("B0")
                  .build();

        model.print(OUT);

        // Children
        assertSame(2, model.getChildren().size());
        assertSame(0, b0.getChildren().size());
        assertSame(3, b1.getChildren().size());

        // Owner
        assertSame(model, b0.getOwner());
        assertSame(model, b1.getOwner());
        assertSame(b1, r1.getOwner());
        assertSame(b1, i0.getOwner());
        assertSame(b1, i1.getOwner());

        // Parents
        assertSame(1, b0.getParents().size()); // 3 times the same parent
        assertSame(0, b1.getParents().size());
    }
}