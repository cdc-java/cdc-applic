package cdc.applic.mountability;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

import cdc.util.lang.Checks;

/**
 * Configuration of a {@link MountabilityComputer}.
 */
public class MountabilityComputerFeatures {
    private final Set<Hint> hints;

    public enum Hint {
        /**
         * Keep going, as much as possible, even in case of error.
         * Otherwise, computation may be interrupted early.
         */
        KEEP_GOING,

        /** Simplify expressions. */
        SIMPLIFY
    }

    MountabilityComputerFeatures(Builder builder) {
        this.hints = Collections.unmodifiableSet(builder.hints);
    }

    public Set<Hint> getHints() {
        return hints;
    }

    /**
     * Returns {@code true} if a {@link Hint} is enabled.
     *
     * @param hint The hint.
     * @return {@code true} if {@code hint} is enabled.
     */
    public boolean isEnabled(Hint hint) {
        return hints.contains(hint);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hints);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof MountabilityComputerFeatures)) {
            return false;
        }
        final MountabilityComputerFeatures other = (MountabilityComputerFeatures) object;
        return hints.equals(other.hints);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private final Set<Hint> hints = EnumSet.noneOf(Hint.class);

        public Builder hint(Hint hint) {
            Checks.isNotNull(hint, "hint");

            this.hints.add(hint);
            return this;
        }

        public Builder hints(Hint... hints) {
            for (final Hint hint : hints) {
                this.hints.add(hint);
            }
            return this;
        }

        public MountabilityComputerFeatures build() {
            return new MountabilityComputerFeatures(this);
        }
    }
}