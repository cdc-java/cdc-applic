package cdc.applic.mountability;

public enum Interchangeability {
    NOT_INTERCHANGEABLE,
    ONE_WAY,
    TWO_WAYS
}