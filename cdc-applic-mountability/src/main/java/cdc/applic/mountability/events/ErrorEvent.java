package cdc.applic.mountability.events;

import java.util.Objects;

/**
 * Specialization of {@link MountabilityEvent} intended to describe an exception.
 */
public final class ErrorEvent implements MountabilityEvent {
    private final String exceptionClassName;
    private final String exceptionMessage;

    private ErrorEvent(String exceptionClassName,
                       String exceptionMessage) {
        this.exceptionClassName = exceptionClassName;
        this.exceptionMessage = exceptionMessage;
    }

    public static ErrorEvent newRuntimeExceptionEvent(RuntimeException e) {
        return newErrorEvent(e.getClass().getCanonicalName(), e.getMessage());
    }

    public static ErrorEvent newErrorEvent(String exceptionClassName,
                                           String exceptionMessage) {
        return new ErrorEvent(exceptionClassName, exceptionMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(exceptionClassName,
                            exceptionMessage);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof ErrorEvent)) {
            return false;
        }
        final ErrorEvent other = (ErrorEvent) object;
        return Objects.equals(this.exceptionClassName, other.exceptionClassName)
                && Objects.equals(this.exceptionMessage, other.exceptionMessage);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(exceptionClassName);
        builder.append(": ");
        builder.append(exceptionMessage);
        return builder.toString();
    }
}