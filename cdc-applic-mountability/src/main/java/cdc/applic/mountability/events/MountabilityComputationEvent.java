package cdc.applic.mountability.events;

import java.util.Objects;

import cdc.applic.expressions.Expression;

/**
 * Specialization of {@link MountabilityEvent} intended to description of normal events.
 *
 * @param <U> The Use Point type.
 * @param <V> The Variant type.
 */
public final class MountabilityComputationEvent<U, V> implements MountabilityEvent {
    private final Type type;
    private final U usePoint;
    private final V variant;
    private final Expression mountability;

    public enum Type {
        BEGIN_USE_POINTS,
        END_USE_POINTS,
        BEGIN_USE_POINT,
        END_USE_POINT,
        VARIANT_MOUNTABILITY
    }

    private MountabilityComputationEvent(Type type,
                                         U usePoint,
                                         V variant,
                                         Expression mountability) {
        this.type = type;
        this.usePoint = usePoint;
        this.variant = variant;
        this.mountability = mountability;
    }

    public static <U, V> MountabilityComputationEvent<U, V> newBeginUsePoints() {
        return new MountabilityComputationEvent<>(Type.BEGIN_USE_POINTS, null, null, null);
    }

    public static <U, V> MountabilityComputationEvent<U, V> newEndUsePoints() {
        return new MountabilityComputationEvent<>(Type.END_USE_POINTS, null, null, null);
    }

    public static <U, V> MountabilityComputationEvent<U, V> newBeginUsePoint(U usePoint) {
        return new MountabilityComputationEvent<>(Type.BEGIN_USE_POINT, usePoint, null, null);
    }

    public static <U, V> MountabilityComputationEvent<U, V> newEndUsePoint(U usePoint) {
        return new MountabilityComputationEvent<>(Type.END_USE_POINT, usePoint, null, null);
    }

    public static <U, V> MountabilityComputationEvent<U, V> newVariantMountability(U usePoint,
                                                                                   V variant,
                                                                                   Expression mountability) {
        return new MountabilityComputationEvent<>(Type.VARIANT_MOUNTABILITY, usePoint, variant, mountability);
    }

    public Type getType() {
        return type;
    }

    public U getUsePoint() {
        return usePoint;
    }

    public V getVariant() {
        return variant;
    }

    public Expression getMountability() {
        return mountability;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type,
                            usePoint,
                            variant,
                            mountability);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof MountabilityComputationEvent)) {
            return false;
        }
        final MountabilityComputationEvent<?, ?> other = (MountabilityComputationEvent<?, ?>) object;
        return this.type == other.type
                && Objects.equals(this.usePoint, other.usePoint)
                && Objects.equals(this.variant, other.variant)
                && Objects.equals(this.mountability, other.mountability);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(getType());
        if (getUsePoint() != null) {
            builder.append(" UsePoint: ").append(getUsePoint());
        }
        if (getVariant() != null) {
            builder.append(" Variant: ").append(getVariant());
        }
        if (getMountability() != null) {
            builder.append(" Mountability: ").append(getMountability());
        }
        return builder.toString();
    }
}