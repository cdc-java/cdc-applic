package cdc.applic.mountability.events;

/**
 * Base marking interface of mountability events.
 */
public interface MountabilityEvent {
    // Empty
}