package cdc.applic.mountability;

import java.util.List;

import cdc.applic.expressions.Expression;
import cdc.util.function.IterableUtils;

/**
 * Interface describing a mountability data set.
 * <p>
 * It is a collection of Use Points, each constituted of Variants.
 *
 * @param <U> The Use Point type.
 * @param <V> The Variant type.
 */
public interface MountabilityData<U, V> {
    /**
     * @return An Iterable of Use Points (spare functions).
     */
    public Iterable<U> getUsePoints();

    /**
     * Returns a String identifying a Use Point.
     *
     * @param usePoint The Use Point.
     * @return The identifier of {@code usePoint}.
     */
    public default String getUsePointId(U usePoint) {
        return usePoint.toString();
    }

    /**
     * @return The total number of use points in this data set.
     */
    public default int getUsePointsCount() {
        return IterableUtils.size(getUsePoints());
    }

    /**
     * @return The total number of variants in this data set.
     */
    public default int getVariantsCount() {
        int count = 0;
        for (final U usePoint : getUsePoints()) {
            count += getVariants(usePoint).size();
        }
        return count;
    }

    /**
     * Returns an ordered list of variants located at a use point.
     *
     * @param usePoint The use point (spare function).
     * @return An ordered list of variants located at {@code usePoint}.
     */
    public List<V> getVariants(U usePoint);

    /**
     * Returns a String locally identifying a Variant at a Use Point.
     *
     * @param usePoint The Use Point.
     * @param variant The variant.
     * @return The local identifier of {@code variant} at {@code usePoint}.
     */
    public default String getVariantLocalId(U usePoint,
                                            V variant) {
        return variant.toString();
    }

    /**
     * Returns the interchangeability of a Variant with the preceding one.
     *
     * @param usePoint The use point.
     * @param variant The variant.
     * @return The interchangeability of {@code variant} with the preceding variant.
     */
    public Interchangeability getVariantInterchangeability(U usePoint,
                                                           V variant);

    /**
     * Returns the applicability of a Variant.
     *
     * @param usePoint The use point.
     * @param variant The variant.
     * @return The applicability of {@code variant}.
     */
    public Expression getVariantApplicability(U usePoint,
                                              V variant);
}