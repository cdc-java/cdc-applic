package cdc.applic.mountability;

import java.util.List;

import cdc.applic.mountability.events.MountabilityEvent;
import cdc.applic.mountability.handlers.MountabilityHandler;
import cdc.util.events.ProgressController;

/**
 * Interface used to compute the mountability of variants located at use points (spare functions).
 *
 * @author Damien Carbonne
 *
 * @param <U> The use point type.
 * @param <V> The variant type.
 */
public interface MountabilityComputer<U, V> {
    /**
     * Computes the mountability of all variants in a mountability data set.
     *
     * @param data The mountability input data set.
     * @param handler The mountability handler, notified when necessary.
     * @param features The features.
     * @param controller The progress controller.
     */
    public void compute(MountabilityData<U, V> data,
                        MountabilityHandler<U, V> handler,
                        MountabilityComputerFeatures features,
                        ProgressController controller);

    /**
     * Computes the mountability of all variants in a mountability data set.
     * <p>
     * This simply calls {@link #compute(MountabilityData, MountabilityHandler, MountabilityComputerFeatures, ProgressController)}
     * and wraps handler notifications in events.
     *
     * @param data The mountability input data set.
     * @param features The features.
     * @param controller The progress controller.
     * @return A List of {@link MountabilityEvent}.
     */
    public List<MountabilityEvent> compute(MountabilityData<U, V> data,
                                           MountabilityComputerFeatures features,
                                           ProgressController controller);
}