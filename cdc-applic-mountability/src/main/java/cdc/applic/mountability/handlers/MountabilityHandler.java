package cdc.applic.mountability.handlers;

import cdc.applic.expressions.Expression;

/**
 * Interface invoked by the mountability computation.
 *
 * @param <U> The Use Point type.
 * @param <V> The Variant type.
 */
public interface MountabilityHandler<U, V> {
    /**
     * Invoked once when processing of use points starts.
     */
    public void processBeginUsePoints();

    /**
     * Invoked once when processing of use points is finished.
     */
    public void processEndUsePoints();

    /**
     * Invoked once for each use point, when its processing starts.
     *
     * @param usePoint The use point.
     */
    public void processBeginUsePoint(U usePoint);

    /**
     * Invoked once for each use point, when its processing is finished.
     *
     * @param usePoint The use point.
     */
    public void processEndUsePoint(U usePoint);

    /**
     * Invoked once for each variant of each use point.
     *
     * @param usePoint The use point.
     * @param variant The variant.
     * @param mountability The computed mountability of {@code variant}.
     */
    public void processVariantMountability(U usePoint,
                                           V variant,
                                           Expression mountability);

    public void processError(RuntimeException e);
}