package cdc.applic.mountability;

import cdc.applic.expressions.checks.ApplicIssue;
import cdc.issues.IssuesHandler;

/**
 * Interface allowing check of mountability data.
 *
 * @param <U> The Use Point type.
 * @param <V> The Variant type.
 */
public interface MountabilityDataChecker<U, V> {
    /**
     * Check mountability data.
     * <p>
     * Note that the way data was built impacts issues that can be detected.<br>
     * For example, if expressions were built with lexical and syntax checks, no syntax or lexical issue can be generated.
     *
     * @param data The data to check.
     * @param handler The issues handler used to collect issues.
     */
    public void check(MountabilityData<U, V> data,
                      IssuesHandler<ApplicIssue> handler);
}