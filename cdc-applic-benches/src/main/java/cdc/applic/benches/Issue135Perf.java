package cdc.applic.benches;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.util.time.Chronometer;

public class Issue135Perf {
    private static final Logger LOGGER = LogManager.getLogger(Issue135Perf.class);

    private static void test(int size) {
        LOGGER.info("Test {}", size);
        final Chronometer chrono = new Chronometer();
        chrono.start();
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final String[] values = new String[size];
        for (int index = 0; index < size; index++) {
            values[index] = "E" + index;
        }
        registry.enumeratedType().name("Enum").frozen(false).literals(values).build();
        registry.property().name("E1").type("Enum").ordinal(0).build();
        chrono.suspend();
        LOGGER.info("Done ({})", chrono);
    }

    public static void main(String[] args) {
        test(10);
        test(20);
        test(50);
        test(100);
        test(200);
        test(500);
        test(1000);
        test(2000);
        test(3000);
        test(4000);
        test(5000);
        test(10000);
    }
}