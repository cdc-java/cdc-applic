package cdc.applic.benches;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.PolicyImpl;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.literals.Name;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.SatSolverException;
import cdc.applic.proofs.core.SatSystemSolver;
import cdc.applic.proofs.core.SatSystemSolverFactory;
import cdc.applic.proofs.core.clauses.DictionarySentencesCache;
import cdc.applic.proofs.core.clauses.SatSystem;
import cdc.util.time.RefTime;

public class SatSystemPerfs {
    private static final Logger LOGGER = LogManager.getLogger(SatSystemPerfs.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.INFO).buildPrintStream();
    protected final RepositoryImpl repository = new RepositoryImpl();
    protected final RegistryImpl registry = repository.registry().name("Registry").build();
    protected PolicyImpl policy = registry.policy().name("Policy 1").build();
    protected final DictionaryHandle dictionaryHandle = new DictionaryHandle(policy);
    protected final List<Expression> expressions = new ArrayList<>();
    protected final SatSystemSolver solver = new SatSystemSolverFactory().newSolver();

    protected int numAtoms = 0;
    protected int numClauses = 0;
    protected int numOk = 0;
    protected int numKo = 0;
    protected long chacheTimeNanos = 0;
    protected int count = 1000;

    SatSystemPerfs() {
        LOGGER.info("SatSystemPerfs()");
        registry.enumeratedType()
                .name("Version")
                .frozen(false)
                .value()
                .literal("RAM")
                .shortLiteral("M")
                .ordinal(0)
                .back()
                .value()
                .literal("RAB")
                .shortLiteral("B")
                .ordinal(1)
                .back()
                .value()
                .literal("RAA")
                .shortLiteral("C")
                .ordinal(2)
                .back()
                .value()
                .literal("RDM")
                .shortLiteral("DM")
                .ordinal(3)
                .back()
                .value()
                .literal("REM")
                .shortLiteral("EM")
                .ordinal(4)
                .back()
                .value()
                .literal("REH")
                .shortLiteral("EH")
                .ordinal(5)
                .back()
                .value()
                .literal("RDH")
                .shortLiteral("DH")
                .ordinal(6)
                .back()
                .value()
                .literal("REQ")
                .shortLiteral("EQ")
                .ordinal(7)
                .back()
                .value()
                .literal("RDQ")
                .shortLiteral("DQ")
                .ordinal(8)
                .back()
                .build();
        registry.enumeratedType()
                .name("Standard")
                .frozen(false)
                .value()
                .literal("F1")
                .ordinal(0)
                .back()
                .value()
                .literal("F2")
                .ordinal(1)
                .back()
                .value()
                .literal("F3")
                .ordinal(2)
                .back()
                .value()
                .literal("F4")
                .ordinal(3)
                .back()
                .value()
                .literal("REG1")
                .ordinal(10)
                .back()
                .value()
                .literal("REG2")
                .ordinal(11)
                .back()
                .value()
                .literal("REG3")
                .ordinal(12)
                .back()
                .build();
        registry.integerType().name("Rank").frozen(false).domain("1~999,2000~2999").build();
        registry.enumeratedType()
                .name("Change")
                .frozen(false)
                .value()
                .literal("APPLIED")
                .shortLiteral("Post")
                .ordinal(0)
                .back()
                .value()
                .literal("NOT_APPLIED")
                .shortLiteral("Pre")
                .ordinal(1)
                .back()
                .build();

        registry.property().name("Version").type("Version").ordinal(0).build();
        registry.property().name("Standard").type("Standard").ordinal(1).build();
        registry.property().name("Rank").type("Rank").ordinal(2).build();
        for (int index = 0; index < 1000; index++) {
            registry.property().name(String.format("M%04d", index)).type("Change").ordinal(3 + index).build();
        }

        registry.alias().name("Monoplace").expression("Version in {RAM, RAA}").build();
        registry.alias().name("Biplace").expression("Version = RAB").build();

        registry.createAssertion("Standard = F1 -> Version = RAM");
        registry.createAssertion("Standard in {F1, F2, F3, F4} <-> Version in  {RAM, RAB, RAA}");
        registry.createAssertion("Standard in {REG1, REG2, REG3} <-> Version in  {RDM, REM}");

        policy.setItemUsage(Name.of("Version"), DItemUsage.OPTIONAL);
        policy.setItemUsage(Name.of("Standard"), DItemUsage.OPTIONAL);
        policy.setItemUsage(Name.of("Rank"), DItemUsage.OPTIONAL);
        policy.setTypeUsage("Change", DItemUsage.OPTIONAL);

        expressions.add(new Expression("Version in { RAM, RAB }"));
        expressions.add(new Expression("Rank in { 10, 20 }"));
        expressions.add(new Expression("Version = RAM or Version = RAB or Version = RAA"));
        expressions.add(new Expression("Version = RAM and Standard in { F1, F2, F3 } and Rank in {1~10, 20}"));
        expressions.add(new Expression("(Rank = 10 or Rank = 20) and Rank in {9~40}"));
        expressions.add(new Expression("Version<:{RAM,RAB}&Standard<:{F2,F3}"));
        expressions.add(new Expression("Version=RAM&Standard<:{F1,F2,F3}|Version<:{RAB,RAA}&Standard<:{F2,F3}"));
        expressions.add(new Expression("Version=RAM & Standard = REG1"));
    }

    private void display(long elapsed) {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("{} for {} atom(s), {} clause(s), {} satisfiable, {} non satisfiable",
                        RefTime.nanosToString(elapsed),
                        numAtoms,
                        numClauses,
                        numOk,
                        numKo);
            final long totalNanos = elapsed;
            final double totalSecs = totalNanos * 1.0e-9;
            final double totalCacheSecs = chacheTimeNanos * 1.0e-9;
            final double avgCacheSecs = totalCacheSecs / count;
            LOGGER.info("   Total time in cache refresh: {}s", totalCacheSecs);
            LOGGER.info("   Average time in cache refresh: {}s", avgCacheSecs);
            final long avgTime1 = totalNanos / numAtoms;
            final double ratio1 = numAtoms / totalSecs;
            LOGGER.info("   Average time per atom: {}", RefTime.nanosToString(avgTime1));
            LOGGER.info("   Average atoms per second: {}", (long) ratio1);
            final long avgTime2 = totalNanos / numClauses;
            final double ratio2 = numClauses / totalSecs;
            LOGGER.info("   Average time per clause: {}", RefTime.nanosToString(avgTime2));
            LOGGER.info("   Average clauses per second: {}", (long) ratio2);
            final long avgTime3 = totalNanos / (numOk + numKo);
            final double ratio3 = (numOk + numKo) / totalSecs;
            LOGGER.info("   Average time per expression: {}", RefTime.nanosToString(avgTime3));
            LOGGER.info("   Average expressions per second: {}", (long) ratio3);
        }
    }

    protected void run(int count,
                       boolean refresh) {
        chacheTimeNanos = 0;
        for (int i = 0; i < count; i++) {
            if (refresh) {
                chacheTimeNanos -= System.nanoTime();
                dictionaryHandle.computeIfAbsent(DictionarySentencesCache.class, DictionarySentencesCache::new);
                chacheTimeNanos += System.nanoTime();
            }
            for (final Expression expression : expressions) {
                final SatSystem system = new SatSystem(dictionaryHandle, expression, ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES);
                numAtoms += system.getNumberOfAtoms();
                numClauses += system.getFullSentence().getClauses().size();
                try {
                    final boolean ok = solver.isSatisfiable(system);
                    if (ok) {
                        numOk++;
                    } else {
                        numKo++;
                    }
                } catch (final SatSolverException e) {
                    LOGGER.catching(e);
                }
            }
        }
    }

    private void printInput() {
        for (final Expression expression : expressions) {
            LOGGER.info("-----------------------------");
            final SatSystem system = new SatSystem(dictionaryHandle, expression, ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES);
            system.print(OUT);
        }
    }

    private void cacheBuilding() {
        final long start = System.nanoTime();
        dictionaryHandle.computeIfAbsent(DictionarySentencesCache.class, DictionarySentencesCache::new).validate();
        final long end = System.nanoTime();
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Cache building: {}", RefTime.nanosToString(end - start));
        }
    }

    public void runAll() {
        cacheBuilding();
        dictionaryHandle.computeIfAbsent(DictionarySentencesCache.class, DictionarySentencesCache::new).print(OUT, 0);

        printInput();

        LOGGER.info("==============================");
        LOGGER.info("Start: {}", getClass().getSimpleName());
        LOGGER.info("==============================");

        final boolean[] refreshes = { false, true };
        for (final boolean refresh : refreshes) {
            final int runs = 10;
            for (int index = 0; index < runs; index++) {
                LOGGER.info("------------------------------");
                final long start = System.nanoTime();
                LOGGER.info("Run {}/{} Cache refresh: {}", (index + 1), runs, refresh);
                numAtoms = 0;
                numClauses = 0;
                numOk = 0;
                numKo = 0;
                run(count, refresh);
                final long end = System.nanoTime();
                display(end - start);
            }
        }
        LOGGER.info("------------------------------");
        LOGGER.info("Done");
    }

    public static void main(String[] args) {
        final SatSystemPerfs instance = new SatSystemPerfs();
        instance.runAll();
    }
}