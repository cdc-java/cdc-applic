package cdc.applic.benches;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;

public class RepositorySupport {
    public final RepositoryImpl repository = new RepositoryImpl();
    public final RegistryImpl registry = repository.registry().name("Registry").build();
    public final DictionaryHandle registryHandle = new DictionaryHandle(registry);

    public RepositorySupport() {
        registry.booleanType().name("B").build();
        registry.integerType().name("I").frozen(false).domain("1~999").build();
        registry.realType().name("R").frozen(false).domain("1.0~999.0").build();
        registry.patternType().name("P").frozen(false).pattern("[A-Z]{2}").build();
        registry.enumeratedType().name("E").frozen(false).literals("E1", "E2", "E3").build();

        registry.property().name("B").type("B").ordinal(0).build();
        registry.property().name("I").type("I").ordinal(1).build();
        registry.property().name("R").type("R").ordinal(2).build();
        registry.property().name("P").type("P").ordinal(3).build();
        registry.property().name("E").type("E").ordinal(4).build();
    }
}