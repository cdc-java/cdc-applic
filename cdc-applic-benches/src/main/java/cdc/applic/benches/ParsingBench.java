package cdc.applic.benches;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.applic.expressions.ast.ParsingNode;
import cdc.applic.expressions.parsing.Parser;
import cdc.applic.expressions.parsing.Recognizer;
import cdc.util.bench.BenchUtils;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(value = 1, jvmArgs = { "-Xms1G", "-Xmx8G" })
@Warmup(iterations = 10, time = 100, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 25, time = 100, timeUnit = TimeUnit.MILLISECONDS)
public class ParsingBench {

    @Param({
            "True",
            "Version = V1",
            "Version = V1 or Version = V2",
            "Version in {V1, V2}"
    })
    public String expression;

    final Parser parser = new Parser();
    final Recognizer recognizer = new Recognizer();

    @Benchmark
    public ParsingNode benchParse() {
        return parser.parse(expression);
    }

    @Benchmark
    public boolean benchIsValid() {
        return recognizer.isValid(expression);
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(ParsingBench.class.getSimpleName())
                                    .forks(1)
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", ParsingBench.class, ".csv"))
                                    .build();
        new Runner(opt).run();
    }
}