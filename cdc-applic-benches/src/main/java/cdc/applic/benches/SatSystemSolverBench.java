package cdc.applic.benches;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.applic.expressions.Expression;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.SatSystemSolver;
import cdc.applic.proofs.core.SatSystemSolverFactory;
import cdc.applic.proofs.core.clauses.SatSystem;
import cdc.util.bench.BenchUtils;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(value = 1, jvmArgs = { "-Xms1G", "-Xmx8G" })
@Warmup(iterations = 10, time = 250, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 50, time = 250, timeUnit = TimeUnit.MILLISECONDS)
public class SatSystemSolverBench {
    private final SatSystemSolverFactory factory = new SatSystemSolverFactory();
    private final SatSystemSolver solver = factory.newSolver();
    private final RepositorySupport support = new RepositorySupport();

    @Param({ "true", "false", "E = E1", "E = E1 & I <: {10~20}" })
    public String expression;

    @Benchmark
    public SatSystem benchBuildExcludeAssertionsNoReserves() {
        final SatSystem system = new SatSystem(support.registryHandle,
                                               new Expression(expression),
                                               ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES);
        return system;
    }

    @Benchmark
    public boolean benchExcludeAssertionsNoReserves() {
        final SatSystem system = new SatSystem(support.registryHandle,
                                               new Expression(expression),
                                               ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES);
        return solver.isSatisfiable(system);
    }

    @Benchmark
    public SatSystem benchBuildIncludeAssertionsNoReserves() {
        final SatSystem system = new SatSystem(support.registryHandle,
                                               new Expression(expression),
                                               ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES);
        return system;
    }

    @Benchmark
    public boolean benchIncludeAssertionsNoReserves() {
        final SatSystem system = new SatSystem(support.registryHandle,
                                               new Expression(expression),
                                               ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES);
        return solver.isSatisfiable(system);
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(SatSystemSolverBench.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", SatSystemSolverBench.class, ".csv"))
                                    .forks(1)
                                    .build();
        new Runner(opt).run();
    }
}