package cdc.applic.benches;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Expressions;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.RefNode;
import cdc.util.bench.BenchUtils;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(value = 1,
        jvmArgs = { "-Xms1G", "-Xmx8G" })
@Warmup(iterations = 5,
        time = 100,
        timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 10,
        time = 250,
        timeUnit = TimeUnit.MILLISECONDS)
public class BuildingBench {
    @Benchmark
    public Expression benchExpression1() {
        return new Expression("A");
    }

    @Benchmark
    public Expression benchExpression2() {
        return new Expression("A & B");
    }

    @Benchmark
    public Expression benchExpression4() {
        return new Expression("A & B & C & D");
    }

    @Benchmark
    public Expression benchExpression8() {
        return new Expression("A & B & C & D & E & F & G & H");
    }

    @Benchmark
    public Expression benchExpressionsNoFormattingNoSimplify1() {
        final Expressions x = Expressions.NO_FORMATTING_NO_SIMPLIFY;
        return x.ref("A");
    }

    @Benchmark
    public Expression benchExpressionsNoFormattingNoSimplify2() {
        final Expressions x = Expressions.NO_FORMATTING_NO_SIMPLIFY;
        return x.and(x.ref("A"),
                     x.ref("B"));
    }

    @Benchmark
    public Expression benchExpressionsNoFormattingNoSimplify4() {
        final Expressions x = Expressions.NO_FORMATTING_NO_SIMPLIFY;
        return x.and(x.ref("A"),
                     x.ref("B"),
                     x.ref("C"),
                     x.ref("D"));
    }

    @Benchmark
    public Expression benchExpressionsNoFormattingNoSimplify8() {
        final Expressions x = Expressions.NO_FORMATTING_NO_SIMPLIFY;
        return x.and(x.ref("A"),
                     x.ref("B"),
                     x.ref("C"),
                     x.ref("D"),
                     x.ref("E"),
                     x.ref("F"),
                     x.ref("G"),
                     x.ref("H"));
    }

    @Benchmark
    public Expression benchExpressionsShortNarrowNoSimplify1() {
        final Expressions x = Expressions.SHORT_NARROW_NO_SIMPLIFY;
        return x.ref("A");
    }

    @Benchmark
    public Expression benchExpressionsShortNarrowNoSimplify2() {
        final Expressions x = Expressions.SHORT_NARROW_NO_SIMPLIFY;
        return x.and(x.ref("A"),
                     x.ref("B"));
    }

    @Benchmark
    public Expression benchExpressionsShortNarrowNoSimplify4() {
        final Expressions x = Expressions.SHORT_NARROW_NO_SIMPLIFY;
        return x.and(x.ref("A"),
                     x.ref("B"),
                     x.ref("C"),
                     x.ref("D"));
    }

    @Benchmark
    public Expression benchExpressionsShortNarrowNoSimplify8() {
        final Expressions x = Expressions.SHORT_NARROW_NO_SIMPLIFY;
        return x.and(x.ref("A"),
                     x.ref("B"),
                     x.ref("C"),
                     x.ref("D"),
                     x.ref("E"),
                     x.ref("F"),
                     x.ref("G"),
                     x.ref("H"));
    }

    @Benchmark
    public Expression benchExpressionsShortNarrowSimplify1() {
        final Expressions x = Expressions.SHORT_NARROW_SIMPLIFY;
        return x.ref("A");
    }

    @Benchmark
    public Expression benchExpressionsShortNarrowSimplify2() {
        final Expressions x = Expressions.SHORT_NARROW_SIMPLIFY;
        return x.and(x.ref("A"),
                     x.ref("B"));
    }

    @Benchmark
    public Expression benchExpressionsShortNarrowSimplify4() {
        final Expressions x = Expressions.SHORT_NARROW_SIMPLIFY;
        return x.and(x.ref("A"),
                     x.ref("B"),
                     x.ref("C"),
                     x.ref("D"));
    }

    @Benchmark
    public Expression benchExpressionsShortNarrowSimplify8() {
        final Expressions x = Expressions.SHORT_NARROW_SIMPLIFY;
        return x.and(x.ref("A"),
                     x.ref("B"),
                     x.ref("C"),
                     x.ref("D"),
                     x.ref("E"),
                     x.ref("F"),
                     x.ref("G"),
                     x.ref("H"));
    }

    @Benchmark
    public Node benchNode1() {
        return new RefNode("A");
    }

    @Benchmark
    public Node benchNode2() {
        return new AndNode(new RefNode("A"),
                           new RefNode("B"));
    }

    @Benchmark
    public Node benchNode4() {
        return new AndNode(new RefNode("A"),
                           new AndNode(new RefNode("B"),
                                       new AndNode(new RefNode("C"),
                                                   new RefNode("D"))));
    }

    @Benchmark
    public Node benchNode8() {
        return new AndNode(new RefNode("A"),
                           new AndNode(new RefNode("B"),
                                       new AndNode(new RefNode("C"),
                                                   new AndNode(new RefNode("D"),
                                                               new AndNode(new RefNode("E"),
                                                                           new AndNode(new RefNode("F"),
                                                                                       new AndNode(new RefNode("G"),
                                                                                                   new RefNode("H"))))))));
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(BuildingBench.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", BuildingBench.class, ".csv"))
                                    .forks(1)
                                    .build();
        new Runner(opt).run();
    }
}