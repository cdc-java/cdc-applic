package cdc.applic.benches;

import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.results.format.ResultFormatType;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;
import cdc.util.bench.BenchUtils;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@State(Scope.Benchmark)
@Fork(value = 1,
        jvmArgs = { "-Xms1G", "-Xmx8G" })
@Warmup(iterations = 5,
        time = 100,
        timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 10,
        time = 250,
        timeUnit = TimeUnit.MILLISECONDS)
public class NodeHashBench {

    private static final Expression[] EXPRESSIONS = {
            new Expression("A"),
            new Expression("A & B"),
            new Expression("A & B & C"),
            new Expression("A = X & B in {1, 2} & !(C in {X, Y})"),
            Expression.TRUE,
            Expression.FALSE
    };

    private static final Node[] NODES = {
            EXPRESSIONS[0].getRootNode(),
            EXPRESSIONS[1].getRootNode(),
            EXPRESSIONS[2].getRootNode(),
            EXPRESSIONS[3].getRootNode(),
            EXPRESSIONS[4].getRootNode(),
            EXPRESSIONS[5].getRootNode()
    };

    @Param({ "0", "1", "2", "3", "4", "5" })
    public int index;

    @Benchmark
    public int benchExpressionHash() {
        return EXPRESSIONS[index].hashCode();
    }

    @Benchmark
    public int benchNodeHash() {
        return NODES[index].hashCode();
    }

    public static void main(String[] args) throws RunnerException {
        final Options opt =
                new OptionsBuilder().include(NodeHashBench.class.getSimpleName())
                                    .resultFormat(ResultFormatType.CSV)
                                    .result(BenchUtils.filename("benchmarks", NodeHashBench.class, ".csv"))
                                    .forks(1)
                                    .build();
        new Runner(opt).run();
    }
}