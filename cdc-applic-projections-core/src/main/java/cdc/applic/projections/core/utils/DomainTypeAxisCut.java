package cdc.applic.projections.core.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.core.NodeFocus;
import cdc.applic.dictionaries.core.utils.CutPointBuffer;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.content.AbstractRange;
import cdc.applic.expressions.content.Domain;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.content.Value;
import cdc.applic.projections.Axis;

public abstract class DomainTypeAxisCut<D extends Domain<V, R>, V extends Value & Comparable<? super V>,
                                        R extends AbstractRange<V, R>>
        extends ContextualAxisCut {
    private final List<R> ranges;
    private final List<Node> nodes = new ArrayList<>();

    protected DomainTypeAxisCut(DictionaryHandle handle,
                                AssertionStrategy assertionStrategy,
                                Axis axis,
                                D domain,
                                Node node) {
        super(handle, assertionStrategy);

        final Property property = axis.getPropertyOrNull();
        this.ranges = buildRanges(node, property, domain);

        for (final R range : ranges) {
            final Node on = new InNode(property.getName(), SItemSetUtils.createBest(range));
            nodes.add(on);
        }
    }

    @Override
    public final int size() {
        return ranges.size();
    }

    @Override
    public final R getCutSItem(int index) {
        return ranges.get(index);
    }

    @Override
    public final Node getCutNode(int index) {
        return nodes.get(index);
    }

    private List<R> buildRanges(Node node,
                                Property property,
                                D domain) {
        // Focus on the node.
        final NodeFocus focus = new NodeFocus(handle, node, assertionStrategy);

        // Collect all SItems that are used with this node
        final Set<SItem> sitems = focus.collectRelatedSItems(property);

        // Build CutPointBuffer from collected SItems
        final CutPointBuffer<D, V, R> cpb = new CutPointBuffer<>(domain);
        for (final SItem sitem : sitems) {
            cpb.add(sitem);
        }

        // Extract cut ranges
        return cpb.getCutRanges();
    }
}