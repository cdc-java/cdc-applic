package cdc.applic.projections.core.utils;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.handles.DictionaryHandle;

/**
 * Base class for AcisCuts that depend on a context.
 *
 * @author Damien Carbonne
 */
public abstract class ContextualAxisCut implements AxisCut {
    protected final DictionaryHandle handle;
    protected final AssertionStrategy assertionStrategy;

    protected ContextualAxisCut(DictionaryHandle handle,
                                AssertionStrategy assertionStrategy) {
        this.handle = handle;
        this.assertionStrategy = assertionStrategy;
    }
}