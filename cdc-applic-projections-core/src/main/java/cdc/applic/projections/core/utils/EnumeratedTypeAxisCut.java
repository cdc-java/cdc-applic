package cdc.applic.projections.core.utils;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.projections.Axis;

public class EnumeratedTypeAxisCut implements NonContextualAxisCut {
    private final List<StringValue> values = new ArrayList<>();
    private final List<Node> nodes = new ArrayList<>();

    public EnumeratedTypeAxisCut(Axis axis) {
        final Property property = axis.getPropertyOrNull();
        final EnumeratedType type = (EnumeratedType) property.getType();

        for (final StringValue value : type.getDomain().getItems()) {
            values.add(value);
            nodes.add(new EqualNode(property.getName(), value));
        }
    }

    @Override
    public int size() {
        return values.size();
    }

    @Override
    public StringValue getCutSItem(int index) {
        return values.get(index);
    }

    @Override
    public Node getCutNode(int index) {
        return nodes.get(index);
    }

    @Override
    public StringSet getEmptySet() {
        return StringSet.EMPTY;
    }
}