package cdc.applic.projections.core;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.projections.ExpressionProjector;
import cdc.applic.projections.Shadow;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.clauses.ProverImpl;
import cdc.util.lang.Checks;

public abstract class AbstractExpressionProjector implements ExpressionProjector {
    protected final Prover prover;

    protected AbstractExpressionProjector(DictionaryHandle handle,
                                          ProverFeatures features) {
        Checks.isNotNull(handle, "handle");
        Checks.isNotNull(features, "features");
        this.prover = new ProverImpl(handle, features);
    }

    @Override
    public final DictionaryHandle getDictionaryHandle() {
        return prover.getDictionaryHandle();
    }

    @Override
    public final ProverFeatures getProverFeatures() {
        return prover.getFeatures();
    }

    @Override
    public final Shadow project(Expression expression) {
        return project(expression.getRootNode());
    }
}