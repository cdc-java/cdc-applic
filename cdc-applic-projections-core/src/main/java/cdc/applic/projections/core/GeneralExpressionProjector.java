package cdc.applic.projections.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cdc.applic.dictionaries.handles.ApplicCacheId;
import cdc.applic.dictionaries.handles.ComputationCache;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.projections.Axis;
import cdc.applic.projections.AxisPiece;
import cdc.applic.projections.Shadow;
import cdc.applic.projections.ShadowPiece;
import cdc.applic.projections.core.utils.AxisCut;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.ProverMatching;
import cdc.util.lang.Checks;

public class GeneralExpressionProjector extends AbstractExpressionProjector {
    private final List<Axis> axes;
    private final AxisCut[] cachedAxisCuts;
    private final Tag tag;

    /**
     * Cache Tag.
     *
     * @author Damien Carbonne
     */
    private static class Tag {
        private final ProverFeatures features;
        private final Axis[] axes;

        public Tag(ProverFeatures features,
                   Axis... axes) {
            this.features = features;
            this.axes = axes.clone();
        }

        @Override
        public int hashCode() {
            return features.hashCode()
                    + 31 * Arrays.hashCode(axes);
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof Tag)) {
                return false;
            }
            final Tag other = (Tag) object;
            return features.equals(other.features)
                    && Arrays.equals(axes, other.axes);
        }

        @Override
        public String toString() {
            return "[" + features + ", " + Arrays.toString(axes) + "]";
        }
    }

    /**
     * Cache of shadows.
     *
     * @author Damien Carbonne
     */
    private static final class ProjectionCache extends ComputationCache<Shadow> {
        private ProjectionCache(DictionaryHandle handle,
                                Tag tag) {
            super(handle,
                  tag);
        }

        public static ProjectionCache get(DictionaryHandle handle,
                                          Tag tag) {
            return handle.computeIfAbsent(ProjectionCache.class,
                                          new ApplicCacheId(ProjectionCache.class, tag),
                                          (c,
                                           t) -> new ProjectionCache(c, (Tag) t));
        }
    }

    public GeneralExpressionProjector(DictionaryHandle handle,
                                      ProverFeatures features,
                                      Axis... axes) {
        super(handle,
              features);
        Checks.isNotNullOrEmpty(axes, "axes");

        // Checks axes
        final List<Axis> tmp = new ArrayList<>();
        for (final Axis axis : axes) {
            Checks.isNotNull(axis, "axis");
            if (tmp.contains(axis)) {
                throw new IllegalArgumentException("Duplicate axis " + axis);
            }
            final NamedDItem item = axis.getItemOrNull();
            if (item != null) {
                Checks.isTrue(handle.getDictionary().isAllowed(item), "Axis " + axis + "is not allowed by dictionary.");
            }
            tmp.add(axis);
        }
        this.axes = Collections.unmodifiableList(tmp);

        // Compute and cache non contextual AxisCuts.
        this.cachedAxisCuts = new AxisCut[this.axes.size()];
        for (int index = 0; index < this.axes.size(); index++) {
            final Axis axis = this.axes.get(index);
            if (axis.isContextual()) {
                this.cachedAxisCuts[index] = null;
            } else {
                this.cachedAxisCuts[index] = AxisCut.createNonContextual(axis);
            }
        }
        this.tag = new Tag(features, axes);
    }

    @Override
    public List<Axis> getAxes() {
        return axes;
    }

    @Override
    public Shadow project(Node node) {
        final ProjectionCache cache = ProjectionCache.get(getDictionaryHandle(), tag);
        return cache.computeIfAbsent(node, this::doProject);
    }

    private Shadow doProject(Node node) {
        // Compute AxisCuts
        final AxisCut[] axisCuts = new AxisCut[axes.size()];
        for (int index = 0; index < axes.size(); index++) {
            if (cachedAxisCuts[index] == null) {
                axisCuts[index] = AxisCut.create(prover, axes.get(index), node);
            } else {
                axisCuts[index] = cachedAxisCuts[index];
            }
        }

        final Set<ShadowPiece>[] sets = createSets();
        final Node[] cutNodes = new Node[axisCuts.length];
        final AxisPiece[] axisPieces = new AxisPiece[axisCuts.length];

        recurse(node, sets, axisCuts, cutNodes, axisPieces, 0);

        return toShadow(sets);
    }

    private void recurse(Node node,
                         Set<ShadowPiece>[] sets,
                         AxisCut[] axisCuts,
                         Node[] cutNodes,
                         AxisPiece[] axisPieces,
                         int axisIndex) {

        // Iterate on all cut nodes of current axis
        for (int cutNodeIndex = 0; cutNodeIndex < axisCuts[axisIndex].size(); cutNodeIndex++) {
            final Axis axis = getAxes().get(axisIndex);
            final AxisCut axisCut = axisCuts[axisIndex];
            // Set the cut node
            cutNodes[axisIndex] = axisCut.getCutNode(cutNodeIndex);
            axisPieces[axisIndex] = new AxisPiece(axis, axisCut.getCutSItem(cutNodeIndex));
            if (axisIndex == axisCuts.length - 1) {
                // We reached the maximum depth: all cut nodes have been set
                // Project node on the space defined by all those cut nodes.
                final Node spaceNode = NaryAndNode.createSimplestAnd(cutNodes);
                final ProverMatching matching = prover.getProjectedMatching(node, spaceNode);
                final ShadowPiece shadowPiece = new ShadowPiece(axisPieces);
                sets[matching.ordinal()].add(shadowPiece);
            } else {
                recurse(node, sets, axisCuts, cutNodes, axisPieces, axisIndex + 1);
            }
        }
    }

    private static Set<ShadowPiece>[] createSets() {
        @SuppressWarnings("unchecked")
        final Set<ShadowPiece>[] sets = new Set[3];
        for (int index = 0; index < 3; index++) {
            sets[index] = new HashSet<>();
        }
        return sets;
    }

    private static Shadow toShadow(Set<ShadowPiece>[] sets) {
        return new Shadow(sets[ProverMatching.NEVER.ordinal()],
                          sets[ProverMatching.SOMETIMES.ordinal()],
                          sets[ProverMatching.ALWAYS.ordinal()]);
    }
}