package cdc.applic.projections.core.utils;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.content.IntegerDomain;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.projections.Axis;

public class IntegerTypeAxisCut extends DomainTypeAxisCut<IntegerDomain, IntegerValue, IntegerRange> {
    public IntegerTypeAxisCut(DictionaryHandle handle,
                              AssertionStrategy assertionStrategy,
                              Axis axis,
                              Node node) {
        super(handle,
              assertionStrategy,
              axis,
              IntegerDomain.INSTANCE,
              node);
    }

    @Override
    public IntegerSet getEmptySet() {
        return IntegerSet.EMPTY;
    }
}