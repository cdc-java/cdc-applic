package cdc.applic.projections.core.utils;

import cdc.applic.projections.Axis;

public class ExpressionAxisCut extends BooleanAxisCut {
    public ExpressionAxisCut(Axis axis) {
        super(axis.getExpressionOrNull().getRootNode());
    }
}