package cdc.applic.projections.core;

import java.util.List;
import java.util.Objects;

import cdc.applic.dictionaries.handles.ApplicCacheId;
import cdc.applic.dictionaries.handles.ComputationCache;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.projections.Axis;
import cdc.applic.projections.ExpressionProjector;
import cdc.applic.projections.Shadow;
import cdc.applic.projections.core.utils.AxisCut;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.ProverMatching;
import cdc.util.lang.Checks;

/**
 * Implementation of {@link ExpressionProjector} that supports only one axis.
 * <p>
 * General implementation should support any number of axes.<br>
 * Practically, 1 axis should satisfy most needs.
 *
 * @author Damien Carbonne
 */
public class SingleAxisExpressionProjector extends AbstractExpressionProjector {
    private final Axis axis;
    private final List<Axis> axes;
    /** Cached AxisCut when axis is not contextual. */
    private final AxisCut cachedAxisCut;
    private final Tag tag;

    /**
     * Cache Tag.
     *
     * @author Damien Carbonne
     */
    private static class Tag {
        private final ProverFeatures features;
        private final Axis axis;

        public Tag(ProverFeatures features,
                   Axis axis) {
            this.features = features;
            this.axis = axis;
        }

        @Override
        public int hashCode() {
            return Objects.hash(features,
                                axis);
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof Tag)) {
                return false;
            }
            final Tag other = (Tag) object;
            return features.equals(other.features)
                    && axis.equals(other.axis);
        }

        @Override
        public String toString() {
            return "[" + features + ", " + axis + "]";
        }
    }

    /**
     * Cache of shadows.
     *
     * @author Damien Carbonne
     */
    private static final class ProjectionCache extends ComputationCache<Shadow> {
        private ProjectionCache(DictionaryHandle handle,
                                Tag tag) {
            super(handle,
                  tag);
        }

        public static ProjectionCache get(DictionaryHandle handle,
                                          Tag tag) {
            return handle.computeIfAbsent(ProjectionCache.class,
                                          new ApplicCacheId(ProjectionCache.class, tag),
                                          (c,
                                           t) -> new ProjectionCache(c, (Tag) t));
        }
    }

    public SingleAxisExpressionProjector(DictionaryHandle handle,
                                         ProverFeatures features,
                                         Axis axis) {
        super(handle,
              features);
        Checks.isNotNull(axis, "axis");

        final NamedDItem item = axis.getItemOrNull();
        if (item != null) {
            Checks.isTrue(handle.getDictionary().isAllowed(item), "Axis " + axis + "is not allowed by dictionary.");
        }

        this.axis = axis;
        this.axes = List.of(axis);
        if (axis.isContextual()) {
            this.cachedAxisCut = null;
        } else {
            this.cachedAxisCut = AxisCut.createNonContextual(axis);
        }
        this.tag = new Tag(features, axis);
    }

    public Axis getAxis() {
        return axis;
    }

    @Override
    public List<Axis> getAxes() {
        return axes;
    }

    @Override
    public Shadow project(Node node) {
        final ProjectionCache cache = ProjectionCache.get(getDictionaryHandle(), tag);
        return cache.computeIfAbsent(node, this::doProject);
    }

    private Shadow doProject(Node node) {
        final AxisCut axisCut = cachedAxisCut == null
                ? AxisCut.create(prover, axis, node)
                : cachedAxisCut;

        // Initialize raw result
        final SItemSet[] sets = createSets(axisCut.getEmptySet());

        // Launch prover on each relevant target expression
        for (int index = 0; index < axisCut.size(); index++) {
            final Node spaceNode = axisCut.getCutNode(index);
            final ProverMatching matching = prover.getProjectedMatching(node, spaceNode);
            sets[matching.ordinal()] = sets[matching.ordinal()].union(axisCut.getCutSItem(index));
        }

        // Convert raw result to Shadow
        return toShadow(sets);
    }

    /**
     * Creates an array of 3 empty sets.
     *
     * @param empty The empty set.
     * @return A new array of 3 empty sets.
     */
    private static SItemSet[] createSets(SItemSet empty) {
        return new SItemSet[] { empty, empty, empty };
    }

    /**
     * Converts an array of 3 {@link SItemSet}s to a {@link Shadow}.
     *
     * @param sets The array of 3 sets.
     * @return A new Shadow.
     */
    private Shadow toShadow(SItemSet[] sets) {
        return new Shadow(axis,
                          validate(sets[ProverMatching.NEVER.ordinal()]),
                          validate(sets[ProverMatching.SOMETIMES.ordinal()]),
                          validate(sets[ProverMatching.ALWAYS.ordinal()]));
    }

    private static SItemSet validate(SItemSet set) {
        return set.isEmpty() ? null : set;
    }
}
