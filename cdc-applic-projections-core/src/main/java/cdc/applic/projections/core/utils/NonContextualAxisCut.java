package cdc.applic.projections.core.utils;

/**
 * Marking interface of AxisCuts that don't depend on a context.
 *
 * @author Damien Carbonne
 */
public interface NonContextualAxisCut extends AxisCut {
    // Ignore
}