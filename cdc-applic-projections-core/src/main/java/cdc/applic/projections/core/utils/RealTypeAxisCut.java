package cdc.applic.projections.core.utils;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.content.RealDomain;
import cdc.applic.expressions.content.RealRange;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.projections.Axis;

public class RealTypeAxisCut extends DomainTypeAxisCut<RealDomain, RealValue, RealRange> {
    public RealTypeAxisCut(DictionaryHandle handle,
                           AssertionStrategy assertionStrategy,
                           Axis axis,
                           Node node) {
        super(handle,
              assertionStrategy,
              axis,
              RealDomain.INSTANCE,
              node);
    }

    @Override
    public RealSet getEmptySet() {
        return RealSet.EMPTY;
    }
}