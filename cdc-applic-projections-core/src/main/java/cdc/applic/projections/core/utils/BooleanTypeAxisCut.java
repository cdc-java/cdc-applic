package cdc.applic.projections.core.utils;

import cdc.applic.expressions.ast.RefNode;
import cdc.applic.projections.Axis;

public class BooleanTypeAxisCut extends BooleanAxisCut {
    public BooleanTypeAxisCut(Axis axis) {
        super(new RefNode(axis.getPropertyOrNull().getName()));
    }
}