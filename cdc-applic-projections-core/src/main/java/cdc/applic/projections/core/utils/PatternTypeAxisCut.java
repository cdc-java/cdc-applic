package cdc.applic.projections.core.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.core.NodeFocus;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.projections.Axis;

public class PatternTypeAxisCut extends ContextualAxisCut {
    private final List<StringValue> values;
    private final List<Node> nodes = new ArrayList<>();

    public PatternTypeAxisCut(DictionaryHandle handle,
                              AssertionStrategy assertionStrategy,
                              Axis axis,
                              Node node) {

        super(handle, assertionStrategy);

        final Property property = axis.getPropertyOrNull();
        this.values = buildValues(handle, assertionStrategy, node, property);

        for (final StringValue value : values) {
            nodes.add(new EqualNode(property.getName(), value));
        }
    }

    @Override
    public int size() {
        return values.size();
    }

    @Override
    public StringValue getCutSItem(int index) {
        return values.get(index);
    }

    @Override
    public Node getCutNode(int index) {
        return nodes.get(index);
    }

    @Override
    public StringSet getEmptySet() {
        return StringSet.EMPTY;
    }

    private static List<StringValue> buildValues(DictionaryHandle handle,
                                                 AssertionStrategy assertionStrategy,
                                                 Node node,
                                                 Property property) {
        // Focus on the node.
        final NodeFocus focus = new NodeFocus(handle, node, assertionStrategy);

        // Collect all SItems that are used with node
        final Set<SItem> sitems = focus.collectRelatedSItems(property);

        // Convert SItems to StringValues
        final List<StringValue> values = new ArrayList<>();
        for (final SItem sitem : sitems) {
            values.add((StringValue) sitem);
        }

        return values;
    }
}