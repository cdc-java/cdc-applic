package cdc.applic.projections.core.utils;

import cdc.applic.expressions.ast.RefNode;
import cdc.applic.projections.Axis;

public class AliasAxisCut extends BooleanAxisCut {
    public AliasAxisCut(Axis axis) {
        super(new RefNode(axis.getAliasOrNull().getName()));
    }
}