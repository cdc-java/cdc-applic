package cdc.applic.projections.core.utils;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.projections.Axis;
import cdc.applic.proofs.Prover;

/**
 * An AxisCut is the cut of an {@link Axis} into {@link SItem}s.
 * <p>
 * This cut may depend on, or be independent of, the context (the node onto which projection is done).
 * <p>
 * A {@link Node} is associated to each {@link SItem}: it defines the corresponding formula.
 *
 * @author Damien Carbonne
 */
public interface AxisCut {
    /**
     * @return The number of {@link SItem}s of the AxisCut.
     */
    public int size();

    /**
     * @param index The index.
     * @return The {@link SItem} at {@code index}.
     */
    public SItem getCutSItem(int index);

    /**
     * @param index The index.
     * @return The {@link Node} at {@code index}.
     */
    public Node getCutNode(int index);

    /**
     * @return The empty set to be used for computations. It must be compliant with axis.
     */
    public SItemSet getEmptySet();

    public static AxisCut create(DictionaryHandle handle,
                                 AssertionStrategy assertionStrategy,
                                 Axis axis,
                                 Node node) {
        final Axis.Kind axisKind = axis.getKind();

        if (axisKind == Axis.Kind.PROPERTY) {
            final Property property = axis.getPropertyOrNull();
            final Type type = property.getType();
            if (type instanceof EnumeratedType) {
                return new EnumeratedTypeAxisCut(axis);
            } else if (type instanceof BooleanType) {
                return new BooleanTypeAxisCut(axis);
            } else if (type instanceof PatternType) {
                return new PatternTypeAxisCut(handle, assertionStrategy, axis, node);
            } else if (type instanceof IntegerType) {
                return new IntegerTypeAxisCut(handle, assertionStrategy, axis, node);
            } else {
                return new RealTypeAxisCut(handle, assertionStrategy, axis, node);
            }
        } else if (axisKind == Axis.Kind.ALIAS) {
            return new AliasAxisCut(axis);
        } else {
            return new ExpressionAxisCut(axis);
        }
    }

    public static AxisCut create(Prover prover,
                                 Axis axis,
                                 Node node) {
        return create(prover.getDictionaryHandle(),
                      prover.getFeatures().getAssertionStrategy(),
                      axis,
                      node);
    }

    public static AxisCut createNonContextual(Axis axis) {
        final Axis.Kind axisKind = axis.getKind();

        if (axisKind == Axis.Kind.PROPERTY) {
            final Property property = axis.getPropertyOrNull();
            final Type type = property.getType();
            if (type instanceof EnumeratedType) {
                return new EnumeratedTypeAxisCut(axis);
            } else if (type instanceof BooleanType) {
                return new BooleanTypeAxisCut(axis);
            } else {
                throw new IllegalArgumentException("Contextual axis: " + axis);
            }
        } else if (axisKind == Axis.Kind.ALIAS) {
            return new AliasAxisCut(axis);
        } else {
            return new ExpressionAxisCut(axis);
        }
    }
}