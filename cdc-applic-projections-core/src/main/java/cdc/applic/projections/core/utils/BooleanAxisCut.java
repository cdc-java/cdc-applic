package cdc.applic.projections.core.utils;

import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;

/**
 * Abstract AxisCut for things that are mapped to boolean axis: {@link BooleanType}, {@link Alias} and {@link Expression}.
 *
 * @author Damien Carbonne
 */
public abstract class BooleanAxisCut implements NonContextualAxisCut {
    private final Node on;
    private final Node off;

    protected BooleanAxisCut(Node on) {
        this.on = on;
        this.off = new NotNode(on);
    }

    @Override
    public final int size() {
        return 2;
    }

    @Override
    public final BooleanValue getCutSItem(int index) {
        return BooleanValue.FALSE_TRUE.get(index);
    }

    @Override
    public final Node getCutNode(int index) {
        if (index == 0) {
            return off;
        } else {
            return on;
        }
    }

    @Override
    public final BooleanSet getEmptySet() {
        return BooleanSet.EMPTY;
    }
}