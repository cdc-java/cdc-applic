package cdc.applic.projections.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.RealRange;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.literals.Name;
import cdc.applic.projections.Axis;
import cdc.applic.projections.core.utils.AxisCut;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.clauses.ProverImpl;

class AxisCutTest {
    private final RepositorySupport support = new RepositorySupport();

    @SafeVarargs
    private static <T> T[] toArray(T... values) {
        return values;
    }

    public AxisCutTest() {
        support.registry.alias().name("A").expression("true").ordinal(0).build();
    }

    private void checkAxis(String text,
                           boolean expression,
                           AssertionStrategy assertionStrategy,
                           Node node,
                           int expectedSize,
                           SItem[] expectedCutSItems) {
        final Prover prover = new ProverImpl(support.registryHandle,
                                             ProverFeatures.builder()
                                                           .assertionStrategy(assertionStrategy)
                                                           .build());

        final Axis axis;
        if (expression) {
            final Expression x = new Expression(text);
            axis = new Axis(x);
        } else {
            final NamedDItem item = support.registry.getItem(Name.of(text));
            axis = new Axis(item);
        }

        final AxisCut axisCut0 = AxisCut.create(prover, axis, node);
        final AxisCut axisCut1 = AxisCut.create(support.registryHandle,
                                                assertionStrategy,
                                                axis,
                                                node);
        assertEquals(expectedSize, axisCut0.size());
        assertEquals(expectedSize, axisCut1.size());
        assertEquals(axis.getCheckedSetClass(), axisCut0.getEmptySet().getClass());
        assertEquals(axis.getCheckedSetClass(), axisCut1.getEmptySet().getClass());

        for (int index = 0; index < axisCut1.size(); index++) {
            assertEquals(expectedCutSItems[index], axisCut1.getCutSItem(index));
            axisCut1.getCutNode(index);
        }

        if (axis.isContextual()) {
            assertThrows(IllegalArgumentException.class,
                         () -> {
                             AxisCut.createNonContextual(axis);
                         });
        } else {
            final AxisCut axisCut2 = AxisCut.createNonContextual(axis);
            assertEquals(expectedSize, axisCut2.size());
        }
    }

    private void checkItemAxis(String name,
                               AssertionStrategy assertionStrategy,
                               Node node,
                               int expectedSize,
                               SItem[] expectedCutSItems) {
        checkAxis(name, false, assertionStrategy, node, expectedSize, expectedCutSItems);
    }

    private void checkExpressionAxis(String expression,
                                     AssertionStrategy assertionStrategy,
                                     Node node,
                                     int expectedSize,
                                     SItem[] expectedCutSItems) {
        checkAxis(expression, true, assertionStrategy, node, expectedSize, expectedCutSItems);
    }

    @Test
    void testBooleanType() {
        checkItemAxis("B",
                      AssertionStrategy.EXCLUDE_ASSERTIONS,
                      null,
                      2,
                      toArray(BooleanValue.FALSE, BooleanValue.TRUE));
    }

    @Test
    void testEnumeratedType() {
        checkItemAxis("E",
                      AssertionStrategy.EXCLUDE_ASSERTIONS,
                      null,
                      3,
                      toArray(StringValue.of("E1"), StringValue.of("E2"), StringValue.of("E3")));
    }

    @Test
    void testIntegerType() {
        checkItemAxis("I",
                      AssertionStrategy.EXCLUDE_ASSERTIONS,
                      TrueNode.INSTANCE,
                      1,
                      toArray(IntegerRange.of(1, 999)));
    }

    @Test
    void testRealType() {
        checkItemAxis("R",
                      AssertionStrategy.EXCLUDE_ASSERTIONS,
                      TrueNode.INSTANCE,
                      1,
                      toArray(RealRange.of(1.0, 999.0)));
    }

    @Test
    void testPatternType() {
        checkItemAxis("P",
                      AssertionStrategy.EXCLUDE_ASSERTIONS,
                      TrueNode.INSTANCE,
                      1,
                      toArray(StringValue.PATTERN_UNKNOWN));

        checkItemAxis("P",
                      AssertionStrategy.EXCLUDE_ASSERTIONS,
                      new Expression("P = AA").getRootNode(),
                      2,
                      toArray(StringValue.of("AA"),
                              StringValue.PATTERN_UNKNOWN));
    }

    @Test
    void testAlias() {
        checkItemAxis("A",
                      AssertionStrategy.EXCLUDE_ASSERTIONS,
                      null,
                      2,
                      toArray(BooleanValue.FALSE, BooleanValue.TRUE));
    }

    @Test
    void testExpression() {
        checkExpressionAxis("I=10",
                            AssertionStrategy.EXCLUDE_ASSERTIONS,
                            null,
                            2,
                            toArray(BooleanValue.FALSE, BooleanValue.TRUE));
    }
}