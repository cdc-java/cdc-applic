package cdc.applic.projections.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositoryXYEnumSupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.literals.Name;
import cdc.applic.projections.Axis;
import cdc.applic.projections.ExpressionProjector;
import cdc.applic.projections.Shadow;
import cdc.applic.proofs.ProverFeatures;

class ProjectionOn1AxisTest {
    private static final Logger LOGGER = LogManager.getLogger(ProjectionOn1AxisTest.class);
    private final RepositoryXYEnumSupport support = new RepositoryXYEnumSupport();

    private static SItemSet set(String axisName,
                                int[] values) {
        if (values.length == 0) {
            return null;
        } else {
            return SItemSetUtils.createBest(RepositoryXYEnumSupport.toContent(axisName, values));
        }
    }

    private Shadow s(String axisName,
                     int[] never,
                     int[] sometimes,
                     int[] always) {
        final Axis axis = new Axis(support.registry.getProperty(Name.of(axisName)));
        return new Shadow(axis,
                          set(axisName, never),
                          set(axisName, sometimes),
                          set(axisName, always));
    }

    private static int[] a(int... values) {
        return values;
    }

    private void check(boolean withReserve,
                       String axisName,
                       String expression,
                       Shadow expectedShadow) {
        LOGGER.debug("===============================================================");
        LOGGER.debug("check({}RESERVES, {}, {})", withReserve ? "" : "NO ", expression, axisName);
        final ProverFeatures features = withReserve
                ? ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES
                : ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES;
        final Axis axis = new Axis(support.registry.getProperty(Name.of(axisName)));
        final Expression x = new Expression(expression);

        final ExpressionProjector projector1 = new SingleAxisExpressionProjector(support.registryHandle,
                                                                                 features,
                                                                                 axis);
        assertEquals(1, projector1.getAxes().size());
        assertEquals(axis, projector1.getAxes().get(0));
        assertEquals(support.registryHandle, projector1.getDictionaryHandle());
        assertEquals(features, projector1.getProverFeatures());

        final ExpressionProjector projector2 = new GeneralExpressionProjector(support.registryHandle,
                                                                              features,
                                                                              axis);
        assertEquals(1, projector2.getAxes().size());
        assertEquals(axis, projector2.getAxes().get(0));
        assertEquals(support.registryHandle, projector2.getDictionaryHandle());
        assertEquals(features, projector2.getProverFeatures());

        final Shadow shadow1 = projector1.project(x);
        LOGGER.debug(shadow1);
        assertEquals(expectedShadow, shadow1);

        final Shadow shadow2 = projector2.project(x);
        LOGGER.debug(shadow2);
        // assertEquals(expectedShadow, shadow2);
    }

    @Test
    void testOnX() {
        check(false, "X", "true", s("X", a(1), a(), a(2, 3)));
        check(false, "X", "false", s("X", a(1, 2, 3), a(), a()));
        check(false, "X", "Y=Y1", s("X", a(1, 2), a(3), a()));
    }
}