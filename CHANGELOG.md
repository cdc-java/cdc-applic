# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.14.2] - 2025-02-23
### Changed
- Updated dependencies:
    - cdc-impex-0.55.1
    - cdc-issues-0.66.0
    - cdc-office-0.60.1
    - org.junit-5.12.0


## [0.14.1] - 2025-02-08
### Changed
- Updated dependencies:
    - cdc-io-0.53.2
    - cdc-impex-0.55.0
    - cdc-issues-0.65.0
    - cdc-office-0.60.0


## [0.14.0] - 2025-01-03
### Added
- Created new code for `consistency`, based on `cdc-issues` checkers. #205

### Changed
- Updated dependencies:
    - cdc-graphs-0.71.3
    - cdc-gv-0.100.3
    - cdc-impex-0.54.0
    - cdc-io-0.53.1
    - cdc-issues-0.63.0
    - cdc-office-0.58.1
    - cdc-ui-0.31.3
    - cdc-util-0.54.0
    - commons-text-1.13.0
    - org.junit-5.11.4
    - org.apache.log4j-2.24.3

### Removed
- Removed code that was deprecated in 2023.

### Deprecated
- Deprecated `ApplicIssue` and its extensions.  
  One should not use methods that are defined in those classes, but only those that are defined in `Issue`.  
  Many checkers and rules will be moved to `dictionary edition` (not yet usable).
- Deprecated `Consistency` code and replaced it with new code based on `cdc-issues`.  
  New code is now in `cdc.applic.consistency.model` and `cdc.applic.consistency.checks`.  
  The only useful maven module is now `cdc-applic-consistency`.  
  `cdc-applic-consistency-core` and `cdc-applic-consistency-impl` will be removed.  
  The data model has been enriched: an include can now have an applicability. #205


## [0.13.3] - 2024-09-28
### Changed
- Updated dependencies:
    - cdc-graphs-0.71.2
    - cdc-gv-0.100.2
    - cdc-impex-0.53.2
    - cdc-io-0.52.1
    - cdc-issues-0.61.0
    - cdc-office-0.57.2
    - cdc-ui-0.31.2
    - cdc-util-0.53.0
    - commons-cli-1.9.0
    - commons-lang3-3.17.0
    - org.hamcrest-3.0
    - org.junit-5.11.1


## [0.13.2] - 2024-07-22
### Changed
- Updated dependencies:
    - cdc-impex-0.53.1
    - cdc-issues-0.60.0
    - cdc-office-0.57.1


## [0.13.1] - 2024-07-21
### Changed
- Updated dependencies:
    - cdc-impex-0.53.0
    - cdc-office-0.57.0


## [0.13.0] - 2024-06-29
### Changed
- Updated dependencies:
    - cdc-impex-0.52.0
    - cdc-office-0.56.0
    - commons-cli-1.8.0
- Created `S1000DTypeConverter` and used it in `S1000DApplicToRepositoryImpl` and `S1000DToExpressionConverterImpl`.  
  A new constructor of `S1000DToExpressionConverterImpl` has been added to enable / disable type transformations. #220
- **Breaking** `--transform-patterns` option has been renamed to `--transformed-types` in `S1000DApplicToRepository`.  

### Fixed
- Accept duplicate ACT/CTT identifiers in `S1000DToExpressionConverterImpl` when loading ACT/CCT. #221
- Reject duplicate names in `S1000DToExpressionConverterImpl` when loading ACT/CCT. #222
- Fix recognition of propertyType in`S1000DToExpressionConverterImpl`. #223


## [0.12.0] - 2024-05-20
### Added
- Created `Expliciter` class that can generate complementary expression using default values.  
  This utility can be used in conjunction with existring algorithms. #217

### Changed
- **Breaking:** changed `XmlSource` and `XmlHandler` to support element content. #213
- Added conversion from/to informal expressions in `S1000DToExpressionConverterImpl`
  and `ExpressionToS1000DConverterImpl`. #213
- Added full support of S1000D Id for S1000D properties and S1000D types. #217
    - If this attribute is not set by application, it is automatically set (deduced from name).
    - Added `s1000d-id` optional attribute for types and properties in `applic-repository.xsd`.
    - Added `S1000D Id` optional column for types and properties in `RepositoryOffice`.
- Added optional default values to types. # 219
- Updated dependencies:
    - cdc-graphs-0.71.1
    - cdc-gv-0.100.1
    - cdc-impex-0.51.1
    - cdc-io-0.52.0
    - cdc-issues-0.59.1
    - cdc-office-0.55.0
    - cdc-ui-0.31.1
    - cdc-util-0.52.1

### Deprecated
- Deprecated `getId(NamingConvention)` in `S1000DIdentified`. #217

### Fixed
- Fixed conversion of expressions to/from S1000D. #217


## [0.11.0] - 2024-05-11
### Added
- **Breaking:** added support of informal expressions.  
  An informal expression is delimited by $ that must now be escaped when used as first character of a simple name (prefix and local name).  
  This change is breaking for names that start with $. Otherwise, it should be transparent. #213

### Changed
- Updated dependencies:
    - commons-cli-1.7.0
    - xml-unit-2.10.0
- Updated maven plugins.

### Fixed
- Ignored external entities during loading of S1000D DM.  
  This may lead to incorrect results with some DM.  
  However, at the moment, only applicability related data are leaded, so this rtosj is minimized.  
  A better solution is needed to support handling of externbal entities. #216


## [0.10.0] - 2024-04-17
### Changed
- Updated dependencies:
    - cdc-impex-0.51.2
    - cdc-io-0.51.2
    - cdc-issues-0.59.0
    - cdc-office-0.54.0
    - commons-text-1.12.0
    - org.junit-5.10.2
    - org.apache.log4j-2.23.1
- Changed API of `EnRuleUtils` and `ApplicRuleUtils`.

### Fixed
- Fixed NPE because of improperly built `DictionaryDependencyCache` whith registries that have parents. #215


## [0.9.0] - 2024-01-29
### Added
- Added creation of S1000DToExpressionConverterImpl from ACT/CTT. #209
- Added means to disable `SimplifierFeatures` hints with command line, whatever the code asks.  
  One must pass `-Dapplic.simplifier.disable.xxx` or `-Dapplic.simplifier.disable.xxx=true` to disable `xxx` hint.  
  Passing `-Dapplic.simplifier.disable.xxx=???` where `???` is anything but `true` (case does not matter), does nothing.  
  The case of `xxx` does not matter.  
  For example, to disable `AUTO_PROJECT`, one can pass `-Dapplic.simplifier.disable.AUTO_PROJECT`
  or `-Dapplic.simplifier.disable.auto_project`. #210

### Changed
- Updated dependencies:
    - cdc-impex-0.51.1
    - cdc-issues-0.55.0
    - cdc-office-0.53.0

### Fixed
- Used product attribute name (instead of id) as property name.


## [0.8.0] - 2024-01-01
### Added
- Added a new simplification pass enabled with `SimplifierFeatures.Hint.AUTO_PROJECT`. #208  
  Values that can never be valid are removed from expressions.  
  For example, with a dictionary containing the property *I:{1\~999}* and the assertion *I != 1*,
  the expression *I in {1\~999}* can be simplified to *I in {2\~999}*.

### Changed
- Updated dependencies:
    - cdc-graphs-0.71.0
    - cdc-gv-0.100.0
    - cdc-impex-0.51.0
    - cdc-io-0.51.0
    - cdc-issues-0.52.0
    - cdc-office-0.52.0
    - cdc-ui-0.31.0
    - cdc-util-0.52.0
- Added `exec()` and call `System.exit()` in `ApplicConsistencyChecker`, `CoomputeMountability`,
  `DictionaryToGv`, `ExtractS1000DDmApplic`, `RegistryEncoder`, `S1000DApplicToRepository` and `VKSD`.
- Updated maven plugins.


## [0.7.0] - 2023-11-25
### Added
- Created `S1000DIssues`. #204
- Created `ConsistencyIssues`. #204
- Added a header check to `VKSD`.

### Changed
- Updated maven plugins.
- Added maven enforcer plugin.
- Used `java.version` and `maven.version` properties in pom.
- Replaced use of `S1000DIssue`by `Issue`. #204  
- Replaced use of `GlobalIssue` and `NodeIssue` by `Issue`. #204
- Updated dependencies:
    - cdc-graphs-0.70.0
    - cdc-gv-0.99.0
    - cdc-impex-0.50.0
    - cdc-io-0.50.0
    - cdc-issues-0.50.0
    - cdc-office-0.50.0
    - cdc-tuples-1.4.0
    - cdc-ui-0.30.0
    - cdc-util-0.50.0
    - commons-lang3-3.14.0
    - commons-text-1.11.0
    - org.junit-5.10.1
- Moved to Java 17

### Removed
- Removed code that was deprecated before 2022-09.
- Removed `S1000DIssue`. #204
- Removed `GlobalIssue` and `NodeIssue`. #204


## [0.6.0] - 2023-07-23
### Added
- Added `--white-list` option to `ExtractS1000DDmApplic`.
- Added options to ExtractS1000DDmApplic to load ACT/CCT. #195
- Added type usage to all dictionaries. #196
- Added generic parameters to `CheckedSet`. This allows writing better generic code on checked sets.
- Added code to have better control of mountability data and computation. #197
    - Added `MountabilityDataChecker` and an implementation of it.  
      It can be used to detect issues on mountability data before launching computation.
    - Added `MountabilityComputerFeatures.Hint.KEEP_GOING`.  
      It can be used to continue computations even in presence of errors.  
      If not passed, behaviour is unchanghed and computation is interrupted on first exception.
    - Added an option to `DataXml.StAXLoader` constructor in order to load XML mountability data without checking expressions.  
      The previous constructor remains available with same behaviour as before: expressions are checked at loading,
      so that loading is interrupted when an invalid expression is met.
    - Added a pass to check mountability data in `ComputeMountability`.

### Changed
- Updated dependencies:
    - cdc-graphs-0.65.1
    - cdc-gv-0.96.7
    - cdc-impex-0.25.0
    - cdc-io-0.27.0
    - cdc-issues-0.31.1
    - cdc-office-0.30.1
    - cdc-ui-0.20.13
    - cdc-util-0.33.0
    - org.junit-5.10.0
- Avoid NPE in `S1000DApplicToRepositoryImpl` when retrieving elements. #194
- Changed handling of item usage. Now, there is no propagation to descendant at creation time.  
  At interrogation time (getEffectiveItemUsage), parents are recursively checked.  
  If they all forbid an item, then that item is effectively forbidden.  
  This behaviour is probably correct, even if more expensive.  
  Implementing caches (memoization) may be necessary if performances are too bad when interrogating item usage. #196

### Removed
- Removed old deprecated `Tokenizer.tokenize(String)`.
- Removed code that was deprecated before 2022-07-14.

### Deprecated
- Deprecated `getUsage()`that was replaced by `getEffectiveItemlUsage()`. #196

### Fixed
- Replaced `int` by `double` in several places of `RealSet`. #200
- Fixed construction of `IntegerSet ` and `RealSet` from String. #199 


## [0.5.0] - 2023-04-22
### Added
- Added `--map1`, `--map2`, `--attribute` and `--auto-size-rows` to `vksd`.
- Created `S1000DApplicToRepository` and `S1000DApplicToRepositoryImpl`.
  They can be used to generate a repository from an ACT/CCT.  #189
- Created `ExpressionToS1000DConverter`and `ExpressionToS1000DConverterImpl`.
  They can be used to create an S1000D XML expression tree from an expression. #191
- Created `S1000DToExpressionConverter` and `S1000DToExpressionConverterImpl`.
  They can be used to create an Expression from an S1000D XML expression tree. #190
- Created `ExtractS1000DDmApplic` that can be used to extract applicability from S1000D Data Modules.

### Changed
- Updated dependencies:
    - cdc-graphs-0.64.5
    - cdc-gv-0.96.5
    - cdc-impex-0.24.5
    - cdc-io-0.26.0
    - cdc-issues-0.30.0
    - cdc-office-0.29.4
    - cdc-ui-0.20.12
    - cdc-util-0.31.0
    - org.apache.log4j-2.20.0
    - org.junit-5.9.2
    - xml-unit-2.9.1
- Added new `KSD` options to `VKSD` (`--split-comparisons`,  ...).  
  Some options were renamed.
- `ConversionException` was moved from `cdc.applic.dictionaries.bindings` to `cdc.applic.dictionaries`.

### Fixed
- Fixed invalid writing order in `RepositoryXml.writeRegistry()`. #188
- Fixed `ExpressionToS1000DConverterImpl` that now converts to n-ary operators. #192


## [0.4.0] - 2022-11-12
### Added
- Improved traces in `ComputeMountabilityImpl`. #134
- A registry can now be defined by merging other dictionaries.  
  All dictionaries can now have a prefix, but this can only be defined for registries. #70
- Added functions to check usability of names to create
  types (`RegistryImpl.canCreateType`) or items (`RegistryImpl.canCreateItem`). #143
- Created `RemovedPrefixes`converter. #144
- Added a hint (`SimplifierFeatures.REMOVE_USELESS_PREFIXES`) to remove useless prefixes in simplification. #146
- Added `DoesNotContainUselessPrefixesChecker` to detect use of useless prefixes. #145
- Added `VERBOSE` and `SAVE_SYNTHESIS` options to `VariantedKeyedSheetDiff`.
- Added `Constraints.elaborateStandardConstraints()` and created `StandardConstraints`
  to facilitate registration of standard constraints. #164
- Added `xor` operator. #127
- Created `DoesNotUseXorChecker`. #128
- Bindings can now be defined between dictionaries. #160
- Created `cdc-applic-renaming` and `cdc-applic-renaming-core` modules. #148
- Added a hint to display descriptions in charts. #173
- Added a hint to display issues in charts. #173
- Added display of prefix. #173
- Support of prefixes in publication. #174
- Support of `naming conventions` and `synonyms` for Types, Properties, Aliases and Enumerated Values.  
  This has impacts on schemas and APIs. Most changes are backward compatible, some are not.  
  For example, S1000D API now require a naming convention parameter.  #163
- Created `of` static function to create objects. #178
- Added `isContained`, `isNonEmpyContained` to `Prover`. #181
- Added new methods to `WritingRuleChecker` and improved Javadoc.
- Created the `Repository` interface. #182
- Created `Builders` for `Registries`, `Policies` and `Bindings`. #168
- Added an option to `VariantedKeyedSheetDiff` to disable detection of vulnerabilities.
- Added missing construction of `description` in `AbstractDictionary.Builder`.

### Changed
- Updated maven plugins
- Updated dependencies:
    - cdc-graphs-0.64.1
    - cdc-gv-0.96.1
    - cdc-impex-0.24.0
    - cdc-io-0.23.1
    - cdc-issues-0.24.0
    - cdc-office-0.27.0
    - cdc-tuples-1.2.0
    - cdc-ui-0.20.7
    - cdc-util-0.28.1
    - org.apache.commons.text-1.10.0
    - org.apache.log4j-2.19.0
    - org.junit-5.9.1
    - org.openjdk.jmh-1.35
    - xml-unit-2.9.0
- Upgraded to Java 11
- Implemented support of qualified names for properties and aliases. `Name` is now qualified, with an optional prefix.  
  The path separator is '.'.  
  This may have some impact on existing expressions, if they use the path separator in some property or alias names. #124
- `Config` data is now retrieved from Manifest.
- Deprecated `Tokenizer.tokenize(String)`. It should not be used and was mainly intended for tests.
- Implementing merge of dictionaries required many changes. Some APIs have disappeared. #70
- Used implementation types as return types in `AbstractDictionary`. #140
- Renamed `StringValue.create` to `StringValue.of`.
- Reduced Visibility of `SName` and `Name` constructors. `SName.of` and `Name.of` must now be used.
- Set interchangeability to `NOT_INTERCHAGEABLE` when not set. #142
- Added serial to dictionaries to detect changes that necessitate invalidation of caches.  
  Caches are now automatically invalidated. #147
- `Dictionary` now extends `DictionaryDeclaredItems`. #150
- Propagation of item usage has been relaxed and now takes into account composition of registries. #153
- Changed handling of Item Usage in Office IO and XML IO. `applic-repository.xsd` was modified. #152
- Renamed `RepositoryIO` to `RepositoryIo`. #154
- Improved message when keys are missing in `VariantedKeyedSheetDiff`.
- Added comments to `applic-repository.xsd` and made `assertion@id` optional. #159
- Renamed `GeneratedAssertion` to `ConstraintAssertion`. Also updated `applic-repository.xsd`. #161
- In dictionary composition, do not generate context assertions that forbid overlapping of scopes of inherited dictionaries.  
  It should now be possible to simultaneously directly and indirectly inherit from the same dictionary. #141
- Changed `RegistriesBinding` to `DictionariesBinding`, and other similar changes.  
  Comments in XML schema were updated, but schema is otherwise unchanged. #160
- Changed and simplified Builders of Bindings. #171
- `SpaceFormatterImpl` constructor is now private. Use `Builder`.
- Changed and improved publication code.
- Fixed a warning in `NamingConventionUtils`. An argument was added to the method. #177
- Class `AbstractExpressionPredicate` has been transformed into interface `ExpressionPredicate`.
- Changed `BooleanValue.FALSE_TRUE` definition from an array to a List.
- Improve handling of item usage.

### Removed
- Removed `TypesCatalog` interface that had become useless.
- Removed `MergedDictionary`.
- Removed now useless code in `DictionaryUtils`. #70
- Removed some methods of `DItemUsage`. #153
- Removed `CharMatcher` (and related implementations) that has been moved to `CharPredicate` in `cdc-util`.
- Removed `CharMapper` that have been moved to `CharFunction` in `cdc-util`.

### Fixed
- Fixed regression with parsing of values that contain '.'. #134
- Improved creation time of EnumeratedTypes. #135
- Fixed a bug related to management of order of enumerated values. #139
- Fixed handling of prefixes in building of caches.
- Fixed `SectionItemsImpl`. The exception that was randomly thrown by HierarchyTest should have disappeared. #149
- Implemented IO of allowed items and writing rules in registries.
  XSD has been updated in accordance. #151
- Fixed a NPE related to Aliases and Prefixes. #158
- Fixed generation of false context assertions. #157
- Fixed `DoesNotUseImplicationOrEquivalenceChecker`rule name (removed extra space).
- Fixed bounds (beginIndex, endIndex) of `EPSILON` token.
- Fixed display of types in charts when definition is empty. #173
- Fixed charts when there is a prefix. #173
- Fixed handling of prefixes in conversions. #175
- Fixed the `--no-added-or-removed-marks` option in `VKSD`.
- Added missing `FORBIDDEN` enumeration value to `DItemUsage` in `applic-repository.xsd`.
- Fixed export of registries parents. Use path instead of name.

### Deprecated
- Deprecated `create` static function to create objects. `of` must be used instead. #178  
    - `BooleanValue`, `BooleanSet`
    - `IntegerRange`, `IntegerValue`, `IntegerSet`
    - `RealRange`, `RealValue`, `RealSet`
    - `StringSet`
    - `UncheckedSet`
    - `PClause`, `PAndClause`, `POrClause`
    - `PSentence`, `PAndSentence`, `POrSentence`
- Deprecated `SItemKind`, `SItemSetKind`. #176
- Deprecated creation methods for `Registries `, `Policies ` and `Bindings`. #168


## [0.3.0] - 2022-01-15 
### Added
- Added `RepositoryIO.Format` enumeration. #120
- Created `RegistryEncoder` that can create an anonymized version of a registry. #119
- Added new conversion methods to `RegistryConverter`. #121
- Created `ApplicConsistencyChecker` tool. #75

### Changed
- Renamed `addTypeBinding` to `addTypesBinding` in `RegistriesBindingImpl`. #119
- Renamed `starget()` to `target()` in `AbstractDItemsBinding.Builder`. #119
- Improved performance of computation of CCT types. #115

### Security
- Updated dependencies:
    - cdc-graphs-0.52.2
    - cdc-gv-0.92.2
    - cdc-impex-0.13.2
    - cdc-issues-0.14.2
    - cdc-office-0.14.2
    - cdc-ui-0.13.2
    - cdc-util-0.14.2
    - org.apache.log4j-2.17.1. #116
    - org.openjdk.jmh-1.34


## [0.2.0] - 2021-12-18
### Added
- Created `cdc-applic-benches` module and moved bench code to it.
- Created `cdc-applic-demos` module and moved demo code to it.
- Improved README. #72
- Implemented `HasDiagnosis` on Applic exceptions. #78
- Created `Partitioner`. #83
- Created `cdc-applic-tools` module and more particularly
 `VariantedKeyedSheetDiff`. #84
- Created `RepositoryIO`. This will allow transparent use of future formats.
- Better error detection and messages in `VariantedKeyedSheetDiff`. #87
- Added `AUTO_SIZE_COLUMNS`and `DUMP_PARTITIONS` options to `VKSD`. #92
- Added `SHOW_CHANGE_DETAILS` to `VariantedKeyedSheetDiff`. #91
- Added `Expressions.getPool()`. #93
- Added `noLimits()` to `SimplifierFeatures.Builder` and used it in some places. #94
- Added `--args-file` option to `CompureMountability`. #100
- Created `cdc-applic-mountability-impl` module and moved some code to it. #101
- Added Office IO for `RepositoryImpl`. #105
- Created `Constraints` factory class. #105
- Added `RepositoryIO.save()`.
- Added methods to clear and remove caches. #113
- Added `S1000DProfile.Hint`. It is now possible to disable generation of CCT condition dependencies. #107

### Changed
- Used latest version of `cdc-issues`, `cdc-gv`, ...
- Renamed test packages.
- Some reserved words (AND, IFF, IMP, IN, NOT, OR, TO) can now be used without
  escaping when they represent a value.  
  For example it is now possible to write P = IN, P in {IN}. Before, one needed
  to write P = "IN", P in {"IN"}. #61
- Added a `maxMillis` (timeout) parameter to `EliminateRedundantSiblings` and
  `EliminateAlwaysTrueOrFalse` converters. #73
- `Simplifier` now returns a `QualifiedValue<Node>` instead of a Node.  
  One must call `simplifier.simplify(...).getValue()` in place of
  `simplifier.simplify(...)` to make code compile and behave as previously. #74
- `EliminateAlwaysTrueOrFalse` and `EliminateRedundantSiblings` return
   a `QualifiedValue<Node>` instead of a Node. #74
- Deprecated types on `Repository`. #79  
- Removed `Diagnosis` and used the cdc-issues one. #76
- Improved implementation of `PredicateMatcher`. Traversal is now interrupted
  as soon as possible. #80
- Now, two dictionaries can have the same name, as long as they are defined
  in different registries. #85  
  Several incompatible changes were made:
   - In `RepositoryImpl`, `getDictionary()` (resp. `getRegistry()` and
     `getPolicy()`) throws an exception if no dictionary (resp. registry
     and policy) is found.
     Use `getDictionaryOrNull()` (resp. `getRegistryOrNull()` and
     `getPolicyOrNull()`) to obtain previous behavior.
   - `applic-consistency-data.xsd` schema was changed.  
     The `dictionary` attribute was renamed to `policy`.
- Added `CHECK` Hint `FactorizationFeatures` and renamed
  `SIMPLIFY_SHORT_NARROW` to `SIMPLIFY_CHECK_SHORT_NARROW`.
- More nodes implement `ParsingNode` now.
  This allowed improvement of  `Expressions` with nary operators. #93
- Moved some S1000D code to `dictionaries`. #97
- Renamed `degenerated` to `degenerate` in `factorization cutter`.
- Renamed `setContentFormatter` to `setValueFormatter` and `getContentFormater` to `getValueFormatter` in `FormattersCatalog`.
- Added `S1000DProfile.Feature.BOOLEAN_RANGES` provision.
- Renamed `SymbolFormatterImpl.getSymbolType()` to `SymbolFormatterImpl.getDefaultSymbolType()`.
- Changed visibility of `ApplicCache.invalidate()`to public. #111
- Used `ControlledPrintable` instead of `Printable` to provide a better control of traces. #108
- Changed dictionary separator to `/`. #114
- Added dictionary path. It is now possible to have 2 dictionaries with the same name as long as they are not siblings.  
  Accessing a dictionary is possible from a repository or from a dictionary, with absolute and relative paths.  
  Paths such as `.` or `..` are supported. #117
- Renamed `mountabilty` to `mountability` in some places. #118

### Fixed
- Fixed some XML sample files. #64
- Validated name of the GV file in `OneDictionaryToGv`. #65
- A bug when parsing empty set defined with mathematical symbol.
- `SentenceBuilder` now removes inequalities. #3
- Better handling of timeout in simplification. #73
- Fixed addition/removal of assertions in AbstractPolicy. #81
- Fixed order of declarations in pom.
- Fixed invalid generation of `CHANGED` when `ADDED` or `REMOVED`
  was expected in VKSD. #92
- Added missing `reset()` when a SAT4J solver is done. #106
- Removed caching of Projectors in S1000D ACT, CCT and PCT computations. #106
- Better progress information with S1000D generators. #110

### Removed
- Removed deprecated methods of `RepositoryImpl`. #95

### Security
- Updated dependencies:
    - org.apache.log4j-2.16.0. #116


## [0.1.0] - 2021-02-22
