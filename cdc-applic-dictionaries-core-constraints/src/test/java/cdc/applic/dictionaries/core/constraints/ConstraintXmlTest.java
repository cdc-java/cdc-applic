package cdc.applic.dictionaries.core.constraints;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.core.constraints.matrix.MatrixConstraint;
import cdc.applic.dictionaries.core.constraints.matrix.MatrixConstraintFeatures;
import cdc.applic.dictionaries.core.constraints.matrix.Order;
import cdc.applic.dictionaries.core.constraints.matrix.RowId;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.io.RepositoryXml;
import cdc.args.Factories;
import cdc.converters.ArgsConversion;
import cdc.io.xml.XmlWriter;
import cdc.util.lang.CollectionUtils;
import cdc.util.lang.FailureReaction;

class ConstraintXmlTest {
    private static final Logger LOGGER = LogManager.getLogger(ConstraintXmlTest.class);

    @Test
    void test() throws IOException {
        Factories.setConverter(ArgsConversion::convert);

        final RepositoryImpl repository1 = new RepositoryImpl();

        final RegistryImpl registry = repository1.registry().name("Registry").build();
        registry.booleanType().name("Boolean").build();
        registry.property().name("M1").type("Boolean").ordinal(0).build();
        registry.property().name("M2").type("Boolean").ordinal(1).build();
        registry.enumeratedType().name("Version").frozen(false).literals("V1", "V2", "V3").build();
        registry.enumeratedType().name("Standard").frozen(false).literals("S1", "S2", "S3").build();
        registry.integerType().name("Range").frozen(false).domain("1~9").build();
        registry.property().name("Version").type("Version").ordinal(2).build();
        registry.property().name("Standard").type("Standard").ordinal(3).build();
        registry.property().name("Range").type("Range").ordinal(4).build();
        registry.addConstraint(new AtLeastOneConstraint(registry).setExpressions())
                .getDescription()
                .description(Locale.FRENCH, "Un commentaire en français")
                .description(Locale.ENGLISH, "An English comment");
        registry.addConstraint(new AtLeastOneConstraint(registry).setExpressions("M1"));
        registry.addConstraint(new AtLeastOneConstraint(registry).setExpressions("M1", "M2"));

        registry.addConstraint(new AtMostOneConstraint(registry).setExpressions());
        registry.addConstraint(new AtMostOneConstraint(registry).setExpressions("M1"));
        registry.addConstraint(new AtMostOneConstraint(registry).setExpressions("M1", "M2"));

        registry.addConstraint(new ExactlyOneConstraint(registry).setExpressions());
        registry.addConstraint(new ExactlyOneConstraint(registry).setExpressions("M1"));
        registry.addConstraint(new ExactlyOneConstraint(registry).setExpressions("M1", "M2"));

        registry.addConstraint(new PropertyRestrictionConstraint(registry).setProperty("Version").setValues("V1, V2"));
        registry.addConstraint(new PropertyRestrictionConstraint(registry).setProperty("Standard").setValues("S1"));

        final MatrixConstraintFeatures features = MatrixConstraintFeatures.builder().order(Order.USER_ORDER).build();

        final MatrixConstraint matrix =
                registry.addConstraint(new MatrixConstraint(registry, features).setContext("Version = V1")
                                                                               .getKeysHeader()
                                                                               .setNames("Version",
                                                                                         "Standard",
                                                                                         "Range")
                                                                               .owner()
                                                                               .getColumnsHeader()
                                                                               .setNames("M1", "M2")
                                                                               .owner());

        matrix.getKeysHeader().createMissingRowIdsIfPossible();
        matrix.setCell(matrix.getKeysHeader().getUserSortedRowIds().get(0), "M1", "true");
        matrix.setCell(matrix.getKeysHeader().getUserSortedRowIds().get(0), "M2", "false");

        registry.createAssertion("M1");
        registry.createAssertion("M1 & M2");

        final File file1 = new File("target/" + getClass().getSimpleName() + "-1.xml");
        try (final XmlWriter writer = new XmlWriter(file1)) {
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT, true);
            writer.setIndentString("  ");
            RepositoryXml.Printer.write(writer, repository1);
        }

        final RepositoryXml.StAXLoader loader = new RepositoryXml.StAXLoader(FailureReaction.FAIL);
        final RepositoryImpl repository2 = loader.load(file1);

        final File file2 = new File("target/" + getClass().getSimpleName() + "-2.xml");
        try (final XmlWriter writer = new XmlWriter(file2)) {
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT, true);
            writer.setIndentString("  ");
            RepositoryXml.Printer.write(writer, repository2);
        }

        LOGGER.debug("Default RowId order");
        for (final RowId rowId : CollectionUtils.toSortedList(matrix.getKeysHeader().getRowIds(), RowId.DEFAULT_ORDER_COMPARATOR)) {
            LOGGER.debug(rowId);
        }

        LOGGER.debug("User RowId order");
        for (final RowId rowId : CollectionUtils.toSortedList(matrix.getKeysHeader().getRowIds(), RowId.USER_ORDER_COMPARATOR)) {
            LOGGER.debug(rowId);
        }

        assertTrue(true);
    }
}