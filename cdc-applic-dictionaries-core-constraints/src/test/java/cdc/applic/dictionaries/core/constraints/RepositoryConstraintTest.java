package cdc.applic.dictionaries.core.constraints;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.Constraint;
import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.impl.PolicyImpl;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.items.LocalAssertion;
import cdc.applic.expressions.literals.Name;
import cdc.util.function.IterableUtils;

class RepositoryConstraintTest {
    @Test
    void testBug82() {
        final RepositoryImpl repository1 = new RepositoryImpl();

        final RegistryImpl registry = repository1.registry().name("Registry").build();
        registry.booleanType().name("Boolean").build();
        registry.property().name("M1").type("Boolean").ordinal(0).build();
        final PolicyImpl policy = registry.policy().name("Policy").build();
        policy.setItemUsage(Name.of("M1"), DItemUsage.OPTIONAL);

        assertSame(0, IterableUtils.size(registry.getAssertions(LocalAssertion.class)));
        assertSame(0, IterableUtils.size(registry.getAllAssertions()));
        assertSame(0, IterableUtils.size(registry.getConstraints()));
        // assertSame(0, IterableUtils.size(registry.getAllConstraints()));

        assertSame(0, IterableUtils.size(policy.getAssertions(LocalAssertion.class)));
        assertSame(0, IterableUtils.size(policy.getAllAssertions()));
        assertSame(0, IterableUtils.size(policy.getConstraints()));
        // assertSame(0, IterableUtils.size(policy.getAllConstraints()));

        final Constraint constraint1 =
                registry.addConstraint(new PropertyRestrictionConstraint(registry).setProperty("M1").setValues("true"));
        assertSame(1, IterableUtils.size(registry.getAssertions(LocalAssertion.class)));
        assertSame(1, IterableUtils.size(registry.getAllAssertions()));
        assertSame(1, IterableUtils.size(registry.getConstraints()));
        // assertSame(1, IterableUtils.size(registry.getAllConstraints()));

        assertSame(0, IterableUtils.size(policy.getAssertions(LocalAssertion.class)));
        assertSame(1, IterableUtils.size(policy.getAllAssertions()));
        assertSame(0, IterableUtils.size(policy.getConstraints()));
        // assertSame(1, IterableUtils.size(policy.getAllConstraints()));

        registry.removeConstraint(constraint1);

        assertSame(0, IterableUtils.size(registry.getAssertions(LocalAssertion.class)));
        assertSame(0, IterableUtils.size(registry.getAllAssertions()));
        assertSame(0, IterableUtils.size(registry.getConstraints()));
        // assertSame(0, IterableUtils.size(registry.getAllConstraints()));

        assertSame(0, IterableUtils.size(policy.getAssertions(LocalAssertion.class)));
        assertSame(0, IterableUtils.size(policy.getAllAssertions()));
        assertSame(0, IterableUtils.size(policy.getConstraints()));
        // assertSame(0, IterableUtils.size(policy.getAllConstraints()));

        final Constraint constraint2 =
                policy.addConstraint(new PropertyRestrictionConstraint(policy).setProperty("M1").setValues("false"));

        assertSame(0, IterableUtils.size(registry.getAssertions(LocalAssertion.class)));
        assertSame(0, IterableUtils.size(registry.getAllAssertions()));
        assertSame(0, IterableUtils.size(registry.getConstraints()));
        // assertSame(0, IterableUtils.size(registry.getAllConstraints()));

        assertSame(1, IterableUtils.size(policy.getAssertions(LocalAssertion.class)));
        assertSame(1, IterableUtils.size(policy.getAllAssertions()));
        assertSame(1, IterableUtils.size(policy.getConstraints()));
        // assertSame(1, IterableUtils.size(policy.getAllConstraints()));

        policy.removeConstraint(constraint2);

        assertSame(0, IterableUtils.size(registry.getAssertions(LocalAssertion.class)));
        assertSame(0, IterableUtils.size(registry.getAllAssertions()));
        assertSame(0, IterableUtils.size(registry.getConstraints()));
        // assertSame(0, IterableUtils.size(registry.getAllConstraints()));

        assertSame(0, IterableUtils.size(policy.getAssertions(LocalAssertion.class)));
        assertSame(0, IterableUtils.size(policy.getAllAssertions()));
        assertSame(0, IterableUtils.size(policy.getConstraints()));
        // assertSame(0, IterableUtils.size(policy.getAllConstraints()));
    }
}