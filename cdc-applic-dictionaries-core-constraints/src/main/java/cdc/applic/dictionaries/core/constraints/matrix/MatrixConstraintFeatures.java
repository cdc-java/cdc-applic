package cdc.applic.dictionaries.core.constraints.matrix;

/**
 * Features used to configure a MatrixConstraint.
 *
 * @author Damien Carbonne
 */
public final class MatrixConstraintFeatures {
    public static final MatrixConstraintFeatures DEFAULT =
            builder().order(Order.DEFAULT_ORDER)
                     .build();

    private final Order order;

    private MatrixConstraintFeatures(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Order order = Order.DEFAULT_ORDER;

        protected Builder() {
            super();
        }

        public Builder order(Order order) {
            this.order = order;
            return this;
        }

        public MatrixConstraintFeatures build() {
            return new MatrixConstraintFeatures(order);
        }
    }
}