package cdc.applic.dictionaries.core.constraints.matrix;

import java.util.Set;

import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.literals.Name;

/**
 * Class dedicated to handling of columns.
 *
 * @author Damien Carbonne
 */
public class ColumnsHeader extends AbstractHeader<ColumnsHeader> {
    ColumnsHeader(MatrixConstraint constraint) {
        super(constraint);
    }

    @Override
    protected ColumnsHeader self() {
        return this;
    }

    @Override
    protected void checkProperty(Property property) {
        // Check that this property is not referenced in keys
        if (constraint.keysHeader.contains(property.getName())) {
            throw new IllegalArgumentException("Property declared as key: " + property.getName());
        }

        // All types are supported as columns
    }

    @Override
    protected void postProcess(Set<Name> removedNames,
                               Set<Name> addeNames) {
        // We can ignore added columns

        // Process removed columns
        for (final Name columnName : removedNames) {
            constraint.clearColumnCells(columnName);
        }
    }
}
