package cdc.applic.dictionaries.core.constraints.matrix;

import java.util.Arrays;
import java.util.Comparator;

import javax.json.stream.JsonGenerator;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Expressions;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

/**
 * Identification of a Row.
 * <p>
 * It is an array of {@link SItemSet sets}. Properties are internally sorted using default (alphabetical) order.<br>
 * A RowId is therefore not impacted by the order chosen by user.
 *
 * @author Damien Carbonne
 */
public class RowId {
    /** The constraint to which this RowId is related. */
    private final MatrixConstraint constraint;
    /** Sets associated to each key property, in the internal default (alphabetical) order. */
    private final SItemSet[] sets;
    private final boolean isPointlike;

    /**
     * A comparator of RowIds using default internal convention.
     * <p>
     * <b>WARNING</b>Compared RowIds must belong to the same constraint and must currently valid.
     */
    public static final Comparator<RowId> DEFAULT_ORDER_COMPARATOR = new DefaultOrderComparator();

    /**
     * A comparator of RowIds using user defined order.
     * <p>
     * <b>WARNING</b>Compared RowIds must belong to the same constraint and must currently valid.
     */
    public static final Comparator<RowId> USER_ORDER_COMPARATOR = new UserOrderComparator();

    /**
     * Creates a RowId from {@link SItemSet sets}.
     *
     * @param constraint The constraint.
     * @param order The order used by {@code sets}.
     * @param sets The key sets passed using {@code order}. They must correspond to keys.
     * @throws IllegalArgumentException When {@code constraint} or {@code sets} is {@code null},
     *             or {@code sets} is not compliant.
     */
    RowId(MatrixConstraint constraint,
          Order order,
          SItemSet[] sets) {
        Checks.isNotNull(constraint, "constraint");
        Checks.isTrue(sets.length == constraint.getKeysHeader().getNumberOfNames(), "Length mismatch");

        // Check that passed key sets are compliant with their corresponding key type
        for (int index = 0; index < sets.length; index++) {
            Checks.isTrue(sets[index] != null, "Null set at {}", index);
            Checks.isTrue(!sets[index].isEmpty(), "Empty set at {}", index);
            // The default index of the key set at index
            final int defaultIndex;
            if (order == Order.DEFAULT_ORDER) {
                defaultIndex = index;
            } else {
                defaultIndex = constraint.keysHeader.userIndexToDefaultIndex(index);
            }
            // Check compliance of the key set at index with its expected type
            Checks.isTrue(constraint.getKeysHeader().getDefaultSortedProperty(defaultIndex).getType().isCompliant(sets[index]),
                          "Non compliant set {} with {}",
                          sets[index],
                          constraint.getKeysHeader().getDefaultSortedName(defaultIndex));
        }

        // Checks whether all sets are singleton or not
        boolean onlySingletons = true;
        for (int index = 0; index < sets.length; index++) {
            if (!sets[index].isSingleton()) {
                onlySingletons = false;
            }
        }

        this.constraint = constraint;
        this.sets = sets.clone();
        if (order == Order.USER_ORDER) {
            // As sets were passed in user order, they are sorted
            // Internally sets are sorted using the default order
            for (int index = 0; index < sets.length; index++) {
                this.sets[constraint.keysHeader.userIndexToDefaultIndex(index)] = sets[index];
            }
        }
        this.isPointlike = onlySingletons;
    }

    /**
     * Creates a RowId from {@link Value values}.
     *
     * @param constraint The constraint.
     * @param order The order used by {@code values}.
     * @param values The values passed using {@code order}.
     * @throws IllegalArgumentException When {@code constraint} or {@code values} is {@code null},
     *             or {@code values} is not compliant.
     */
    RowId(MatrixConstraint constraint,
          Order order,
          Value[] values) {
        this(constraint, order, SItemSetUtils.toSetArray(values));
    }

    RowId(MatrixConstraint constraint,
          Order order,
          String[] contents) {
        this(constraint, order, SItemSetUtils.toSetArray(contents));
    }

    /**
     * Returns {@code true} if this RowId is related to a constraint.
     *
     * @param constraint The constraint.
     * @return {@code true} if this RowId is related to {@code constraint}.
     */
    public boolean matches(MatrixConstraint constraint) {
        return this.constraint == constraint;
    }

    public SItemSet getSet(Name keyName) {
        return sets[constraint.keysHeader.getDefaultSortedIndex(keyName)];
    }

    public boolean isPointlike() {
        return isPointlike;
    }

    public Expression toExpression() {
        Expression result = null;
        for (int index = 0; index < sets.length; index++) {
            final Name name = constraint.keysHeader.getDefaultSortedNames().get(index);
            final SItemSet set = sets[index];
            final Expression x;
            if (set.isSingleton()) {
                x = Expressions.SHORT_NARROW_NO_SIMPLIFY.equal(name, set.getSingletonValue());
            } else {
                x = Expressions.SHORT_NARROW_NO_SIMPLIFY.in(name, set);
            }

            if (result == null) {
                result = x;
            } else {
                result = Expressions.SHORT_NARROW_NO_SIMPLIFY.and(result, x);
            }
        }
        return result;
    }

    // TODO merge / split

    @Override
    public int hashCode() {
        return Arrays.hashCode(sets);
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof RowId)) {
            return false;
        }
        final RowId other = (RowId) object;
        return Arrays.equals(sets, other.sets);
    }

    @Override
    public String toString() {
        return Arrays.toString(sets);
    }

    /**
     * Writes this {@link RowId} to a JSON stream using a JSON generator.
     *
     * @param generator The JSON generator.
     * @param order The order used to write keys.
     */
    public void write(JsonGenerator generator,
                      Order order) {
        generator.writeStartArray();
        if (order == Order.DEFAULT_ORDER) {
            for (final SItemSet set : sets) {
                generator.write(set.getContent());
            }
        } else {
            for (int userIndex = 0; userIndex < sets.length; userIndex++) {
                final int defaultIndex = constraint.keysHeader.userIndexToDefaultIndex(userIndex);
                generator.write(sets[defaultIndex].getContent());
            }
        }
        generator.writeEnd();
    }

    private static class DefaultOrderComparator implements Comparator<RowId> {
        @Override
        public int compare(RowId o1,
                           RowId o2) {
            if (o1 == o2) {
                return 0;
            }

            if (o1 == null || o2 == null) {
                return o1 == null ? -1 : 1;
            }

            Checks.assertTrue(o1.constraint == o2.constraint, "Constraints mismatch.");

            final int length = Math.min(o1.sets.length, o2.sets.length);
            for (int defaultIndex = 0; defaultIndex < length; defaultIndex++) {
                final SItemSet s1 = o1.sets[defaultIndex];
                final SItemSet s2 = o2.sets[defaultIndex];
                final int cmp = SItemSetUtils.compare(s1, s2);
                if (cmp != 0) {
                    return cmp;
                }
            }
            return o1.sets.length - o2.sets.length;
        }
    }

    private static class UserOrderComparator implements Comparator<RowId> {
        @Override
        public int compare(RowId o1,
                           RowId o2) {
            if (o1 == o2) {
                return 0;
            }

            if (o1 == null || o2 == null) {
                return o1 == null ? -1 : 1;
            }

            Checks.assertTrue(o1.constraint == o2.constraint, "Constraints mismatch.");

            final int length = Math.min(o1.sets.length, o2.sets.length);
            for (int userIndex = 0; userIndex < length; userIndex++) {
                final int defaultIndex = o1.constraint.keysHeader.userIndexToDefaultIndex(userIndex);
                final SItemSet s1 = o1.sets[defaultIndex];
                final SItemSet s2 = o2.sets[defaultIndex];
                final int cmp = SItemSetUtils.compare(s1, s2);
                if (cmp != 0) {
                    return cmp;
                }
            }
            return o1.sets.length - o2.sets.length;
        }
    }
}