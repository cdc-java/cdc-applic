package cdc.applic.dictionaries.core.constraints.matrix;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.CountableType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.RealType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

/**
 * Class dedicated to handling of keys and rows.
 * <p>
 * Theoretically, rows are subsets of the Cartesian product of values of key properties.<br>
 * These subsets must not intersect and their union can/should differ from the full Cartesian product.<br>
 * Each subset may be a singleton.
 * <p>
 * If we only accept singletons, rows can be automatically computed if we can enumerate all values of each property.<br>
 * This is possible for {@link BooleanType}s, {@link EnumeratedType}s and {@link IntegerType}s.<br>
 * It is not possible for {@link PatternType}s, and considered as impossible or useless for {@code RealType}s.<br>
 * With {@link IntegerType}s, there is a risk for the number of rows to be too large.
 * <p>
 * So, the options are:
 * <ul>
 * <li>Compute all rows automatically.
 * <ul>
 * <li>It only relies on singletons.
 * <li>It excludes some types.
 * <li>It may lead to very large number of rows.
 * <li>It may force to split rows when grouping them would be a better choice.
 * </ul>
 * <li>Define all rows explicitly.
 * <ul>
 * <li>It allows grouping.<br>
 * That would reduce the number of generated assertions!
 * <li>It allows all types.
 * <li>It requires detection of conflicts (intersection of rows).<br>
 * <em>Really? Is there any problem if 2 rows overlap?</em>
 * <li>It requires a more advanced UI.<br>
 * We could offer, when possible, functions to generate all rows.<br>
 * And functions to merge rows.
 * </ul>
 * </ul>
 *
 * @author Damien Carbonne
 */
public class KeysHeader extends AbstractHeader<KeysHeader> {
    /**
     * Map of computed rowIds to their validity.
     */
    private final Map<RowId, RowIdValidity> rowIds = new HashMap<>();

    /** RowIds sorted with default order. */
    private final List<RowId> defaultSortedRowIds = new ArrayList<>();

    /** RowIds sorted with user order. */
    private final List<RowId> userSortedRowIds = new ArrayList<>();

    KeysHeader(MatrixConstraint constraint) {
        super(constraint);
    }

    @Override
    protected KeysHeader self() {
        return this;
    }

    @Override
    protected void checkProperty(Property property) {
        // Check that this property is not referenced in columns
        if (constraint.columnsHeader.contains(property.getName())) {
            throw new IllegalArgumentException("Property declared as column: " + property.getName());
        }

        // All types are supported as key names
    }

    @Override
    protected void postProcess(Set<Name> removedNames,
                               Set<Name> addeNames) {
        if (!addeNames.isEmpty() || !removedNames.isEmpty()) {
            removeAllRowIds();
        }
    }

    /**
     * Updates the validity of all RowIds.
     */
    public void updateAllRowIdsValidity() {
        for (final RowId rowId : rowIds.keySet()) {
            updateRowIdsValidity(rowId);
        }
    }

    /**
     * Updates the validity of one RowId.
     *
     * @param rowId the RowId.
     */
    public void updateRowIdsValidity(RowId rowId) {
        Checks.isNotNull(rowId, "rowId");
        Checks.isTrue(rowId.matches(constraint), "Constraint mismatch");

        if (constraint.prover != null) {
            final Expression expression = rowId.toExpression();
            try {
                if (constraint.prover.isSometimesTrue(expression)) {
                    rowIds.put(rowId, RowIdValidity.VALID);
                } else {
                    rowIds.put(rowId, RowIdValidity.UNSATISAFIABLE);
                }
            } catch (final ApplicException e) {
                rowIds.put(rowId, RowIdValidity.INVALID);
            }
        }
    }

    public RowId createRowIdIfAbsent(Order order,
                                     SItemSet... sets) {
        final RowId rowId = new RowId(constraint, order, sets);
        if (rowIds.containsKey(rowId)) { // TODO This does not work with sets
            rowIds.put(rowId, RowIdValidity.UNKNOWN);
            defaultSortedRowIds.add(rowId);
            defaultSortedRowIds.sort(RowId.DEFAULT_ORDER_COMPARATOR);
            userSortedRowIds.add(rowId);
            userSortedRowIds.sort(RowId.USER_ORDER_COMPARATOR);

            updateRowIdsValidity(rowId);
        }
        return rowId;
    }

    public RowId createRowIdIfAbsent(Order order,
                                     Value... values) {
        return createRowIdIfAbsent(order, SItemSetUtils.toSetArray(values));
    }

    public RowId createRowIdIfAbsent(Order order,
                                     String... contents) {
        return createRowIdIfAbsent(order, SItemSetUtils.toSetArray(contents));
    }

    public void removeRowId(RowId rowId) {
        if (rowIds.containsKey(rowId)) {
            constraint.clearRowCells(rowId);

            constraint.clearRowCells(rowId);
            rowIds.remove(rowId);
            defaultSortedRowIds.remove(rowId);
            userSortedRowIds.remove(rowId);
            // No need to sort
        }
    }

    public void removeAllRowIds() {
        constraint.clearAllCells();
        rowIds.clear();
        defaultSortedRowIds.clear();
        userSortedRowIds.clear();
    }

    /**
     * Removes all invalid RowIds.
     */
    public void removeAllInvalidRowIds() {
        final List<RowId> todo = new ArrayList<>();
        for (final RowId rowId : rowIds.keySet()) {
            if (getValidity(rowId) == RowIdValidity.INVALID) {
                todo.add(rowId);
            }
        }
        for (final RowId rowId : todo) {
            removeRowId(rowId);
        }
    }

    /**
     * Returns the number of Rows this KeysHeader can possibly support.
     * <p>
     * A negative value means the number can not be computed.<br>
     * This is typically the case when the type of a key property is not countable: {@link PatternType} and {@link RealType}.
     * <p>
     * <b>Note:</b> This number is computed assuming that all RowIds are points.
     *
     * @return The number of Rows this KeysHeader can possibly support.
     */
    public long getNumberOfPossiblePointlikeRows() {
        if (getNumberOfNames() == 0) {
            return 0L;
        } else {
            long count = 1;
            for (final Property property : getDefaultSortedProperties()) {
                final Type type = property.getType();
                if (type instanceof CountableType) {
                    final long extent = ((CountableType) type).getExtent();
                    if (extent == 0) {
                        return 0L;
                    } else if (extent > 0L) {
                        count *= extent;
                    } else {
                        return -1;
                    }
                }
            }
            return count;
        }
    }

    public boolean isEmptyOrHasOnlyPointlikeRowIds() {
        for (final RowId rowId : rowIds.keySet()) {
            if (!rowId.isPointlike()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns {@code true} if it is possible to automatically create RowIds.
     * <p>
     * This is the case when:
     * <ul>
     * <li>The number of possible RowIds is strictly positive.
     * <li>All RowIds are points. The is a current limitation.
     * </ul>
     * <p>
     * <b>WARNING:</b> This number of possible RowIds can be very large, in that case, user should be warned.
     *
     * @return {@code true} if it is possible to automatically create RowIds.
     *
     *
     */
    public boolean canAutoCreateRowIds() {
        return getNumberOfPossiblePointlikeRows() > 0 && isEmptyOrHasOnlyPointlikeRowIds();

    }

    public void createMissingRowIdsIfPossible() {
        if (canAutoCreateRowIds()) {
            if (!getDefaultSortedNames().isEmpty()) {
                final Value[] values = new Value[getDefaultSortedNames().size()];
                createMissingRowIdsRec(values, 0);
            }

            defaultSortedRowIds.clear();
            defaultSortedRowIds.addAll(rowIds.keySet());
            defaultSortedRowIds.sort(RowId.DEFAULT_ORDER_COMPARATOR);

            userSortedRowIds.clear();
            userSortedRowIds.addAll(rowIds.keySet());
            userSortedRowIds.sort(RowId.USER_ORDER_COMPARATOR);

            updateAllRowIdsValidity();
        }
    }

    private void createMissingRowIdsRec(Value[] stack,
                                        int index) {
        final Name name = getDefaultSortedNames().get(index);
        final Property property = constraint.getOwner().getAllowedProperty(name);
        final CountableType type = (CountableType) property.getType();
        for (final Value value : type.getCountableValues()) {
            stack[index] = value;
            if (index == stack.length - 1) {
                // Stack is full: create the row id
                final RowId rowId = new RowId(constraint, Order.DEFAULT_ORDER, stack);
                rowIds.computeIfAbsent(rowId, k -> RowIdValidity.UNKNOWN);
                // Do not fill defaultSortedRowIds and userSortedRowIds
            } else {
                createMissingRowIdsRec(stack, index + 1);
            }
        }
    }

    /**
     * @return The set of RowIds.
     */
    public Set<RowId> getRowIds() {
        return rowIds.keySet();
    }

    public List<RowId> getDefaultSortedRowIds() {
        return defaultSortedRowIds;
    }

    public List<RowId> getUserSortedRowIds() {
        return userSortedRowIds;
    }

    /**
     * @param rowId The row identifier.
     * @return The validity of the row identified by {@code rowId}.
     * @throws IllegalArgumentException When  {@code rowId} is not known.
     */
    public RowIdValidity getValidity(RowId rowId) {
        Checks.isTrue(rowIds.containsKey(rowId), "Invalid rowId:{}", rowId);

        return rowIds.get(rowId);
    }
}