package cdc.applic.dictionaries.core.constraints;

import cdc.applic.dictionaries.Constraints;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.core.encoding.Encoders;

/**
 * Constraint ensuring that exactly one among N expressions is valid.
 *
 * @author Damien Carbonne
 */
public final class ExactlyOneConstraint extends AbstractExpressionsConstraint<ExactlyOneConstraint> {
    public static final String CONSTRAINT_NAME = "EXACTLY_ONE";

    static {
        Constraints.register(CONSTRAINT_NAME, ExactlyOneConstraint::create);
    }

    public ExactlyOneConstraint(Dictionary dictionary) {
        super(dictionary);
    }

    @Override
    protected ExactlyOneConstraint self() {
        return this;
    }

    @Override
    protected void updateAssertions() {
        dictionary.removeRelatedAndDerivedAssertions(this);
        final Expression expression = Encoders.exactlyOneNegatable(Expression.toNodeList(expressions)).toExpression();
        dictionary.createAssertion(this, null, expression);
    }

    @Override
    public String getTypeName() {
        return CONSTRAINT_NAME;
    }

    @Override
    public ExactlyOneConstraint create(Dictionary dictionary) {
        return new ExactlyOneConstraint(dictionary);
    }

    public static ExactlyOneConstraint create(Dictionary dictionary,
                                              String params) {
        return new ExactlyOneConstraint(dictionary).setExpressions(fromJson(params));
    }

    public static void elaborate() {
        // Ignore
    }
}