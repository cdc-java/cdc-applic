package cdc.applic.dictionaries.core.constraints.matrix;

/**
 * Enumeration of possible orders to sort rows and columns.
 *
 * @author Damien Carbonne
 */
public enum Order {
    /** Default order. typically alphabetical order. */
    DEFAULT_ORDER,
    /** User defined order. */
    USER_ORDER
}