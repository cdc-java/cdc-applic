package cdc.applic.dictionaries.core.constraints.matrix;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParserFactory;

import cdc.applic.dictionaries.Constraints;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.checks.SemanticChecker;
import cdc.applic.dictionaries.core.constraints.AbstractConstraint;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.ConstraintAssertion;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Expressions;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.literals.Name;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.clauses.ProverImpl;
import cdc.io.json.AbstractJsonParser;
import cdc.io.json.JsonEvent;
import cdc.io.json.JsonParserWrapper;
import cdc.io.json.JsonpParser;
import cdc.io.json.JsonpUtils;
import cdc.util.lang.Checks;

/**
 * Class used to link a combination of key properties to values of other properties in a context.
 * <p>
 * Context is defined by an expression, and it determines the set of valid rows.<br>
 * Key properties define rows and column properties define columns.<br>
 * Rows are automatically deduced from key properties.<br>
 * In each cell it is possible to define the values of the column property that are valid for the corresponding row.
 * <p>
 * <b>Behavior</b>
 * <ul>
 * <li>Changing the <em>context</em> only impacts validity of each row.
 * <li>Rows are automatically deduced from <em>key columns</em>.<br>
 * It is the Cartesian product of all values of <em>key columns</em>.<br>
 * <b>Warning:</b> This may create a big number of rows.
 * <li>Adding or removing a <em>key column</em> clears all cells.<br>
 * <li>Moving a <em>key column</em> has no impact on generated assertions.
 * <li>Removing a <em>value column</em> clears all cells of that column.
 * <li>Adding a <em>value column</em> has no impact on cells.
 * <li>Moving <em>value column</em> has no impact on generated assertions.
 * </ul>
 *
 * @author Damien Carbonne
 */
public class MatrixConstraint extends AbstractConstraint {
    public static final String CONSTRAINT_NAME = "MATRIX";
    private static final Expressions BUILDER = Expressions.SHORT_NARROW_NO_SIMPLIFY;

    private static final String CELLS = "cells";
    private static final String COLUMN_NAMES = "columnNames";
    private static final String CONTEXT = "context";
    private static final String FEATURES = "features";
    private static final String KEY_NAMES = "keyNames";
    private static final String ORDER = "order";

    private DictionaryHandle dictionaryHandle = null;
    private ProverFeatures proverFeatures = ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES;
    protected Prover prover = null;

    /** Configuration of this constraint. */
    private final MatrixConstraintFeatures features;

    /** The context expression. */
    private Expression context = Expression.TRUE;

    /** The key header. */
    final KeysHeader keysHeader;

    /** The column header. */
    final ColumnsHeader columnsHeader;

    /** The defined cells. */
    private final Map<CellId, SItemSet> cells = new HashMap<>();

    static {
        Constraints.register(CONSTRAINT_NAME, MatrixConstraint::create2);
    }

    public MatrixConstraint(Dictionary dictionary,
                            MatrixConstraintFeatures features) {
        super(dictionary);
        this.features = features;
        this.keysHeader = new KeysHeader(this);
        this.columnsHeader = new ColumnsHeader(this);
    }

    @Override
    public String getTypeName() {
        return CONSTRAINT_NAME;
    }

    public DictionaryHandle getDictionaryHandle() {
        return dictionaryHandle;
    }

    public MatrixConstraintFeatures getFeatures() {
        return features;
    }

    public void setDictionaryHandle(DictionaryHandle dictionaryHandle) {
        Checks.isTrue(dictionaryHandle == null || dictionaryHandle.getDictionary() == dictionary,
                      "Non compliant dictionary");

        this.dictionaryHandle = dictionaryHandle;
        if (this.dictionaryHandle == null) {
            this.prover = null;
        } else {
            this.prover = new ProverImpl(dictionaryHandle, proverFeatures);
        }
    }

    public ProverFeatures getProverFeatures() {
        return proverFeatures;
    }

    public void setProverFeatures(ProverFeatures proverFeatures) {
        Checks.isNotNull(proverFeatures, "proverFeatures");
        this.proverFeatures = proverFeatures;
    }

    /**
     * Sets the context expression.
     * <p>
     * This will change validity of rows.
     *
     * @param context The context expression.
     * @return This constraint.
     * @throws IllegalArgumentException When {@code context} is {@code null}.
     */
    public MatrixConstraint setContext(Expression context) {
        Checks.isNotNull(context, "context");
        SemanticChecker.checkCompliance(dictionary, context);

        this.context = context;
        keysHeader.updateAllRowIdsValidity();
        return this;
    }

    /**
     * Sets the context expression.
     * <p>
     * This will change validity of rows.
     *
     * @param context The context expression.
     * @return This constraint.
     */
    public MatrixConstraint setContext(String context) {
        return setContext(new Expression(context));
    }

    /**
     * @return The context expression.
     */
    public Expression getContext() {
        return context;
    }

    /**
     * @return The key Header that must be used to change keys.
     */
    public KeysHeader getKeysHeader() {
        return keysHeader;
    }

    /**
     * @return The column Header that must be used to change columns.
     */
    public ColumnsHeader getColumnsHeader() {
        return columnsHeader;
    }

    /**
     * Clears all cells.
     * <p>
     * This will also remove all related assertions.
     *
     * @return This constraint.
     */
    public MatrixConstraint clearAllCells() {
        cells.clear();
        dictionary.removeRelatedAndDerivedAssertions(this);
        return this;
    }

    /**
     * Clears all cells in a row.
     * <p>
     * This will also remove or update related assertions.
     *
     * @param rowId The row identifier.
     * @return This constraint.
     */
    public MatrixConstraint clearRowCells(RowId rowId) {
        Checks.isNotNull(rowId, "rowId");

        final Set<CellId> cellIds = new HashSet<>();
        for (final CellId cellId : cells.keySet()) {
            if (cellId.getRowId().equals(rowId)) {
                cellIds.add(cellId);
            }
        }
        for (final CellId cellId : cellIds) {
            clearCell(cellId);
        }
        return this;
    }

    /**
     * Clears all cells in a column.
     * <p>
     * This will also remove or update related assertions.
     *
     * @param columnName The column name.
     * @return This constraint.
     */
    public MatrixConstraint clearColumnCells(Name columnName) {
        Checks.isNotNull(columnName, "columnName");

        final Set<CellId> cellIds = new HashSet<>();
        for (final CellId cellId : cells.keySet()) {
            if (cellId.getColumnName().equals(columnName)) {
                cellIds.add(cellId);
            }
        }
        for (final CellId cellId : cellIds) {
            clearCell(cellId);
        }
        return this;
    }

    /**
     * Clears a cell.
     * <p>
     * This will also remove or update related assertions.
     *
     * @param cellId The cell identifier.
     * @return This constraint.
     */
    public MatrixConstraint clearCell(CellId cellId) {
        return setCell(cellId, null);
    }

    /**
     * Sets the content of a cell.
     * <p>
     * This will also remove or update related assertions.
     *
     * @param cellId The cell identifier
     * @param set The optional content of the cell.<br>
     *            If {@code null} or empty, then this is like removing cell content.
     * @return This constraint.
     * @throws IllegalArgumentException When {@code cellId}, {@code set} is {@code null},<br>
     *             or when {@code cellId} is not compliant with this constraint,<br>
     *             or when {@code set} is not compliant with the column of {@code cellId}
     */
    public MatrixConstraint setCell(CellId cellId,
                                    SItemSet set) {
        Checks.isNotNull(cellId, "cellId");
        Checks.isNotNull(set, "set");
        Checks.isTrue(cellId.matches(this), "Constraint mismatch");

        if (set == null || set.isEmpty()) {
            if (cells.containsKey(cellId)) {
                cells.remove(cellId);
                // Remove related assertion
                final ConstraintAssertion ga = dictionary.getRelatedAssertion(this, cellId.toParams(features.getOrder()));
                dictionary.removeAssertion(ga);
            }
        } else {
            final Property property = columnsHeader.getPropery(cellId.getColumnName());
            Checks.isTrue(property.getType().isCompliant(set), "Non compliant set " + set + " with " + property.getName());
            final boolean update = cells.containsKey(cellId);
            if (update) {
                final ConstraintAssertion ga = dictionary.getRelatedAssertion(this, cellId.toParams(features.getOrder()));
                dictionary.removeAssertion(ga);
            }
            cells.put(cellId, set);
            final Expression expression = toExpession(cellId, set);
            dictionary.createAssertion(this, cellId.toParams(features.getOrder()), expression);
        }
        return this;
    }

    private Expression toExpession(CellId cellId,
                                   SItemSet set) {
        final Expression x = BUILDER.imp(cellId.getRowId().toExpression(),
                                         BUILDER.inOrEqual(cellId.getColumnName(), set));
        if (context.isTrue()) {
            return x;
        } else {
            return BUILDER.imp(context, x);
        }
    }

    /**
     * Sets the content of a cell.
     * <p>
     * This will also remove or update related assertions.
     *
     * @param rowId The row identifier.
     * @param columnName The column name.
     * @param set The optional content of the cell.<br>
     *            If {@code null} or empty, then this is like removing cell content.
     * @return This constraint.
     */
    public MatrixConstraint setCell(RowId rowId,
                                    String columnName,
                                    String set) {
        return setCell(new CellId(rowId, Name.of(columnName)),
                       SItemSetUtils.createBest(set));
    }

    /**
     * @return The set of identifiers of cells that have an associated content.
     */
    public Set<CellId> getCellIds() {
        return cells.keySet();
    }

    /**
     * Returns the content associated to cell, or {@code null}.
     *
     * @param cellId The cell identifier.
     * @return The content associated to {@code cellId} or {@code null}.
     */
    public SItemSet getCellOrNull(CellId cellId) {
        return cells.get(cellId);
    }

    @Override
    public String getParams() {
        return JsonpUtils.toString(generator -> {
            generator.writeStartObject(); // PARAMS

            // FEATURES
            generator.writeStartObject(FEATURES);
            generator.write(ORDER, features.getOrder().name());
            generator.writeEnd();

            // CONTEXT
            generator.write(CONTEXT, context.getContent());

            // KEY_NAMES
            generator.writeStartArray(KEY_NAMES);
            for (final Name name : getKeysHeader().getUserSortedNames()) {
                generator.write(name.getNonEscapedLiteral());
            }
            generator.writeEnd();

            // COLUMN_NAMES
            generator.writeStartArray(COLUMN_NAMES);
            for (final Name name : getColumnsHeader().getUserSortedNames()) {
                generator.write(name.getNonEscapedLiteral());
            }
            generator.writeEnd();

            // CELLS
            generator.writeStartArray(CELLS);
            final List<CellId> sortedCellIds = new ArrayList<>(getCellIds());
            sortedCellIds.sort(CellId.DEFAULT_ORDER_COMPARATOR);
            for (final CellId id : sortedCellIds) {
                generator.writeStartArray(); // CELL

                // ROW ID
                generator.writeStartArray();
                for (final Name name : getKeysHeader().getUserSortedNames()) {
                    generator.write(id.getRowId().getSet(name).getContent());
                }
                generator.writeEnd();

                // COLUMN NAME
                generator.write(id.getColumnName().getNonEscapedLiteral());

                // CELL CONTENT
                generator.write(getCellOrNull(id).getContent());

                generator.writeEnd(); // CELL
            }
            generator.writeEnd(); // CELLS

            generator.writeEnd(); // PARAMS
        });
    }

    private static final class Wrapper extends JsonParserWrapper {
        private final Dictionary dictionary;
        private MatrixConstraint result;

        public Wrapper(Dictionary dictionary,
                       AbstractJsonParser delegate) {
            super(delegate);
            this.dictionary = dictionary;
        }

        public MatrixConstraint parse() {
            next();
            parseMatrixConstraint();
            return result;
        }

        private void parseMatrixConstraint() {
            parseObject(name -> {
                if (FEATURES.equals(name)) {
                    // This should be parsed first
                    parseFeatures();
                } else if (CONTEXT.equals(name)) {
                    expect(JsonEvent.VALUE_STRING);
                    result.setContext(getStringValue());
                } else if (KEY_NAMES.equals(name)) {
                    parseArray(AbstractJsonParser::getStringValue,
                               s -> result.getKeysHeader().addName(s));
                } else if (COLUMN_NAMES.equals(name)) {
                    parseArray(AbstractJsonParser::getStringValue,
                               s -> result.getColumnsHeader().addName(s));
                } else if (CELLS.equals(name)) {
                    parseArray(this::parseCell);
                } else {
                    throw unexpectedKeyName(name);
                }
            });
        }

        private void parseFeatures() {
            parseObject(name -> {
                if (ORDER.equals(name)) {
                    expect(JsonEvent.VALUE_STRING);
                    final Order order = getEnumValue(Order.class);
                    final MatrixConstraintFeatures features =
                            MatrixConstraintFeatures.builder()
                                                    .order(order)
                                                    .build();
                    result = new MatrixConstraint(dictionary, features);
                } else {
                    throw unexpectedKeyName(name);
                }
            });
        }

        private void parseCell() {
            expect(JsonEvent.START_ARRAY);
            next();

            // ROW ID
            final RowId rowId = parseRowId();

            next().expect(JsonEvent.VALUE_STRING); // COLUMN NAME
            final String columnName = getStringValue();

            next().expect(JsonEvent.VALUE_STRING); // CELL CONTENT
            final String cellContent = getStringValue();

            result.setCell(rowId, columnName, cellContent);
            next().expect(JsonEvent.END_ARRAY); // END CELL
        }

        private RowId parseRowId() {
            final List<String> contents = parseArray(AbstractJsonParser::getStringValue);
            return result.getKeysHeader()
                         .createRowIdIfAbsent(Order.USER_ORDER,
                                              contents.toArray(new String[contents.size()]));
        }
    }

    public static MatrixConstraint create(Dictionary dictionary,
                                          String params) {
        final JsonParserFactory factory = Json.createParserFactory(null);

        try (final StringReader reader = new StringReader(params);
                final JsonParser parser = factory.createParser(reader)) {
            final JsonpParser wrapper = new JsonpParser(parser);

            wrapper.next().expect(JsonEvent.START_OBJECT);

            // FEATURES
            wrapper.next().expectKeyName(FEATURES);
            wrapper.next().expect(JsonEvent.START_OBJECT);
            wrapper.next().expectKeyName(ORDER);
            wrapper.next().expect(JsonEvent.VALUE_STRING);
            final Order order = wrapper.getEnumValue(Order.class);
            final MatrixConstraintFeatures features =
                    MatrixConstraintFeatures.builder()
                                            .order(order)
                                            .build();
            wrapper.next().expect(JsonEvent.END_OBJECT);

            final MatrixConstraint result = new MatrixConstraint(dictionary, features);

            // CONTEXT
            wrapper.next().expectKeyName(CONTEXT);
            wrapper.next().expect(JsonEvent.VALUE_STRING);
            final String context = wrapper.getStringValue();
            result.setContext(context);

            // KEY_NAMES
            wrapper.next().expectKeyName(KEY_NAMES);
            wrapper.next().expect(JsonEvent.START_ARRAY);
            while (wrapper.hasNext()) {
                wrapper.next();
                if (wrapper.isOn(JsonEvent.VALUE_STRING)) {
                    final String name = wrapper.getStringValue();
                    result.getKeysHeader().addName(name);
                } else {
                    wrapper.expect(JsonEvent.END_ARRAY);
                    break;
                }
            }

            // COLUMN_NAMES
            wrapper.next().expectKeyName(COLUMN_NAMES);
            wrapper.next().expect(JsonEvent.START_ARRAY);
            while (wrapper.hasNext()) {
                wrapper.next();
                if (wrapper.isOn(JsonEvent.VALUE_STRING)) {
                    final String name = wrapper.getStringValue();
                    result.getColumnsHeader().addName(name);
                } else {
                    wrapper.expect(JsonEvent.END_ARRAY);
                    break;
                }
            }

            // CELLS
            wrapper.next().expectKeyName(CELLS);
            wrapper.next().expect(JsonEvent.START_ARRAY); // START CELLS

            final List<String> contents = new ArrayList<>();
            while (wrapper.hasNext()) {
                wrapper.next();
                if (wrapper.isOn(JsonEvent.START_ARRAY)) { // START CELL
                    contents.clear();
                    wrapper.next().expect(JsonEvent.START_ARRAY); // START ROW ID
                    RowId rowId = null;
                    while (wrapper.hasNext()) {
                        wrapper.next();
                        if (wrapper.isOn(JsonEvent.VALUE_STRING)) {
                            final String set = wrapper.getStringValue();
                            contents.add(set);
                        } else {
                            wrapper.expect(JsonEvent.END_ARRAY); // END ROW ID
                            rowId = result.getKeysHeader()
                                          .createRowIdIfAbsent(Order.USER_ORDER,
                                                               contents.toArray(new String[contents.size()]));
                            break;
                        }
                    }

                    wrapper.next().expect(JsonEvent.VALUE_STRING); // COLUMN NAME
                    final String columnName = wrapper.getStringValue();

                    wrapper.next().expect(JsonEvent.VALUE_STRING); // CELL CONTENT
                    final String cellContent = wrapper.getStringValue();

                    result.setCell(rowId, columnName, cellContent);
                    wrapper.next().expect(JsonEvent.END_ARRAY); // END CELL
                } else {
                    wrapper.expect(JsonEvent.END_ARRAY); // END CELLS
                    break;
                }
            }
            wrapper.next().expect(JsonEvent.END_OBJECT);
            return result;
        }
    }

    public static MatrixConstraint create2(Dictionary dictionary,
                                           String params) {
        final JsonParserFactory factory = Json.createParserFactory(null);
        try (final StringReader reader = new StringReader(params);
                final JsonParser parser = factory.createParser(reader)) {
            final Wrapper wrapper = new Wrapper(dictionary, new JsonpParser(parser));
            return wrapper.parse();
        }
    }

    public static void elaborate() {
        // Ignore
    }
}