package cdc.applic.dictionaries.core.constraints.matrix;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

/**
 * Base class used to handle a set of {@link Name Names} for rows and columns.
 * <p>
 * A Header is, from the user point of view, a sorted list of names.<br>
 * Changing the order of names has no impact on the way cells are designated.
 *
 * @author Damien Carbonne
 * @param <H> The header type.
 */
public abstract class AbstractHeader<H extends AbstractHeader<H>> {
    /** The owner of the header. */
    protected final MatrixConstraint constraint;

    /** Set of names. */
    private final Set<Name> names = new HashSet<>();

    /** Sorted list of names, as defined by user. */
    private final List<Name> userSortedNames = new ArrayList<>();

    /** Sorted list of names, in alphabetical order. */
    private final List<Name> defaultSortedNames = new ArrayList<>();

    /** Sorted list of properties, in alphabetical order. */
    private final List<Property> defaultSortedProperties = new ArrayList<>();

    /** Index in defaultSortedNames of each name. */
    private final Map<Name, Integer> nameToDefaultIndex = new HashMap<>();

    private int[] userToDefaultIndex = new int[0];

    protected AbstractHeader(MatrixConstraint constraint) {
        this.constraint = constraint;
    }

    protected abstract H self();

    protected abstract void checkProperty(Property property);

    protected abstract void postProcess(Set<Name> removedNames,
                                        Set<Name> addeNames);

    /**
     * @return The MatrixConstraint that contains this Header.
     */
    public MatrixConstraint owner() {
        return constraint;
    }

    /**
     * @return The set of Names that define this Header.
     */
    public Set<Name> getNames() {
        return names;
    }

    public int getNumberOfNames() {
        return names.size();
    }

    public boolean contains(Name name) {
        return names.contains(name);
    }

    public Property getPropery(Name name) {
        return getDefaultSortedProperty(getDefaultSortedIndex(name));
    }

    /**
     * @return The list of Names that define this Header, using the default sort order.
     */
    public List<Name> getDefaultSortedNames() {
        return defaultSortedNames;
    }

    /**
     * @param index The index.
     * @return The Name that defines this Header at index, using the default sort order.
     */
    public Name getDefaultSortedName(int index) {
        return defaultSortedNames.get(index);
    }

    /**
     * @return The list of Properties that define this Header, using the default sort order.
     */
    public List<Property> getDefaultSortedProperties() {
        return defaultSortedProperties;
    }

    /**
     * @param index The index.
     * @return The Property that defines this Header at index, using the default sort order.
     */
    public Property getDefaultSortedProperty(int index) {
        return defaultSortedProperties.get(index);
    }

    /**
     * @return The list of Names that define this Header, using the user defined sort order.
     */
    public List<Name> getUserSortedNames() {
        return userSortedNames;
    }

    /**
     * Returns the index of a {@link Name} in the default order.
     *
     * @param name The {@link Name}.
     * @return The index of {@code name} in the default order.
     */
    public int getDefaultSortedIndex(Name name) {
        return nameToDefaultIndex.get(name);
    }

    /**
     * Converts a user index of a name to the default index.
     *
     * @param userIndex The user index.
     * @return The default index corresponding to {@code userIndex}.
     */
    public int userIndexToDefaultIndex(int userIndex) {
        return userToDefaultIndex[userIndex];
    }

    /**
     * Sets the names of this Header.
     *
     * @param names The user sorted names.
     * @return This Header.
     */
    public H setNames(List<Name> names) {
        Checks.isNotNull(names, "names");

        // Set of new names
        final Set<Name> newNames = new HashSet<>(names);

        Checks.assertTrue(names.size() == newNames.size(), "Duplicate names in {}", names);

        // Check names
        for (final Name name : names) {
            // Check that name is that of an allowed property
            final Property property = constraint.getOwner().getAllowedProperty(name);

            // Do additional checks
            checkProperty(property);
        }

        // Set of added names
        final Set<Name> addedNames = new HashSet<>();
        // Set of removed names
        final Set<Name> removedNames = new HashSet<>();

        // In all cases we consider user order changed.
        userSortedNames.clear();
        userSortedNames.addAll(names);

        if (!this.names.equals(newNames)) {
            // Some names have been added or removed

            // Compute added names
            addedNames.addAll(newNames);
            addedNames.removeAll(this.names);

            // Compute removed names
            removedNames.addAll(this.names);
            removedNames.removeAll(newNames);

            // Update names
            this.names.clear();
            this.names.addAll(names);

            // Update default sorted names
            defaultSortedNames.clear();
            defaultSortedNames.addAll(names);
            Collections.sort(defaultSortedNames);

            defaultSortedProperties.clear();
            for (final Name name : defaultSortedNames) {
                final Property property = constraint.getOwner().getAllowedProperty(name);
                defaultSortedProperties.add(property);
            }
        }
        // Otherwise, only order of names may have changed

        // In all case, update map from name to default index
        nameToDefaultIndex.clear();
        for (final Name name : defaultSortedNames) {
            nameToDefaultIndex.put(name, nameToDefaultIndex.size());
        }

        // In all case, update map from user index to default index
        userToDefaultIndex = new int[defaultSortedNames.size()];
        int userIndex = 0;
        for (final Name name : userSortedNames) {
            userToDefaultIndex[userIndex] = nameToDefaultIndex.get(name);
            userIndex++;
        }

        // Do additional stuff
        postProcess(removedNames, addedNames);

        return self();
    }

    /**
     * Sets the names of this Header.
     *
     * @param names The user sorted names.
     * @return This Header.
     */
    public H setNames(String... names) {
        return setNames(Name.toNames(names));
    }

    /**
     * Adds one name to this Header.
     *
     * @param name The name.
     * @return This Header.
     */
    public H addName(Name name) {
        final List<Name> newNames = new ArrayList<>(names);
        newNames.add(name);
        return setNames(newNames);
    }

    /**
     * Adds one name to this Header.
     *
     * @param name The name.
     * @return This Header.
     */
    public H addName(String name) {
        return addName(Name.of(name));
    }

    /**
     * Removes one name from this Header.
     *
     * @param name The name.
     * @return This Header.
     */
    public H removeName(Name name) {
        final List<Name> newNames = new ArrayList<>(names);
        newNames.remove(name);
        return setNames(newNames);
    }

    /**
     * Removes one name from this Header.
     *
     * @param name The name.
     * @return This Header.
     */
    public H removeName(String name) {
        return removeName(Name.of(name));
    }

    /**
     * Removes all names from this header.
     *
     * @return This Header.
     */
    public H removeAllNames() {
        return setNames(Collections.emptyList());
    }
}