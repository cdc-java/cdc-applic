package cdc.applic.dictionaries.core.constraints;

import cdc.applic.dictionaries.Constraints;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.core.encoding.Encoders;

/**
 * Constraint ensuring that at most one among N expressions is valid.
 *
 * @author Damien Carbonne
 */
public final class AtMostOneConstraint extends AbstractExpressionsConstraint<AtMostOneConstraint> {
    public static final String CONSTRAINT_NAME = "AT_MOST_ONE";

    static {
        Constraints.register(CONSTRAINT_NAME, AtMostOneConstraint::create);
    }

    public AtMostOneConstraint(Dictionary dictionary) {
        super(dictionary);
    }

    @Override
    protected AtMostOneConstraint self() {
        return this;
    }

    @Override
    protected void updateAssertions() {
        dictionary.removeRelatedAndDerivedAssertions(this);
        final Expression expression = Encoders.atMostOneNegatable(Expression.toNodeList(expressions)).toExpression();
        dictionary.createAssertion(this, null, expression);
    }

    @Override
    public String getTypeName() {
        return CONSTRAINT_NAME;
    }

    @Override
    public AtMostOneConstraint create(Dictionary dictionary) {
        return new AtMostOneConstraint(dictionary);
    }

    public static AtMostOneConstraint create(Dictionary dictionary,
                                             String params) {
        return new AtMostOneConstraint(dictionary).setExpressions(fromJson(params));
    }

    public static void elaborate() {
        // Ignore
    }
}