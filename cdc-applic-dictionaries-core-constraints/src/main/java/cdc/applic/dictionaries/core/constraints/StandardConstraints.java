package cdc.applic.dictionaries.core.constraints;

import cdc.applic.dictionaries.core.constraints.matrix.MatrixConstraint;

public final class StandardConstraints {
    private StandardConstraints() {
    }

    /**
     * Elaborates all standard constraints.
     * <p>
     * This ensure that their factories are registered.
     */
    public static void elaborate() {
        AtLeastOneConstraint.elaborate();
        AtMostOneConstraint.elaborate();
        ExactlyOneConstraint.elaborate();
        PropertyRestrictionConstraint.elaborate();
        MatrixConstraint.elaborate();
    }
}