package cdc.applic.dictionaries.core.constraints.matrix;

import java.util.Comparator;
import java.util.Objects;

import javax.json.stream.JsonGenerator;

import cdc.applic.expressions.literals.Name;
import cdc.io.json.JsonpUtils;

/**
 * Identification of a cell: row identifier + column name.
 *
 * @author Damien Carbonne
 */
public class CellId {
    private final RowId rowId;
    private final Name columnName;

    /**
     * A comparator of CellIds using default internal convention.
     * <p>
     * <b>WARNING</b>Compared CellIds must belong to the same constraint and must currently valid.
     */
    public static final Comparator<CellId> DEFAULT_ORDER_COMPARATOR = new DefaultOrderComparator();

    // TODO user order comparator

    public CellId(RowId rowId,
                  Name columnName) {
        this.rowId = rowId;
        this.columnName = columnName;
    }

    public boolean matches(MatrixConstraint constraint) {
        return rowId.matches(constraint);
    }

    public RowId getRowId() {
        return rowId;
    }

    public Name getColumnName() {
        return columnName;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rowId, columnName);
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (!(object instanceof CellId)) {
            return false;
        }
        final CellId other = (CellId) object;
        return rowId.equals(other.rowId)
                && columnName.equals(other.columnName);
    }

    @Override
    public String toString() {
        return "[" + rowId + ", " + columnName + "]";
    }

    /**
     * Writes this {@link CellId} to a JSON stream using a JSON generator.
     *
     * @param generator The JSON generator.
     * @param order The order used to write row keys.
     */
    public void write(JsonGenerator generator,
                      Order order) {
        generator.writeStartArray();
        rowId.write(generator, order);
        generator.write(columnName.getNonEscapedLiteral());
        generator.writeEnd();
    }

    public String toParams(Order order) {
        return JsonpUtils.toString(g -> write(g, order));
    }

    private static class DefaultOrderComparator implements Comparator<CellId> {
        @Override
        public int compare(CellId o1,
                           CellId o2) {

            if (o1 == o2) {
                return 0;
            }

            if (o1 == null || o2 == null) {
                return o1 == null ? -1 : 1;
            }

            final int rowIdCmp = RowId.DEFAULT_ORDER_COMPARATOR.compare(o1.getRowId(), o2.getRowId());
            if (rowIdCmp == 0) {
                return o1.getColumnName().compareTo(o2.getColumnName());
            } else {
                return rowIdCmp;
            }
        }
    }
}