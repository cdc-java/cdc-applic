package cdc.applic.dictionaries.core.constraints;

import cdc.applic.dictionaries.Constraints;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.core.encoding.Encoders;

/**
 * Constraint ensuring that at least one among N expressions is valid.
 *
 * @author Damien Carbonne
 */
public final class AtLeastOneConstraint extends AbstractExpressionsConstraint<AtLeastOneConstraint> {
    public static final String CONSTRAINT_NAME = "AT_LEAST_ONE";

    static {
        Constraints.register(CONSTRAINT_NAME, AtLeastOneConstraint::create);
    }

    public AtLeastOneConstraint(Dictionary dictionary) {
        super(dictionary);
    }

    @Override
    protected AtLeastOneConstraint self() {
        return this;
    }

    @Override
    protected void updateAssertions() {
        dictionary.removeRelatedAndDerivedAssertions(this);
        final Expression expression = Encoders.atLeastOne(Expression.toNodeList(expressions)).toExpression();
        dictionary.createAssertion(this, null, expression);
    }

    @Override
    public String getTypeName() {
        return CONSTRAINT_NAME;
    }

    @Override
    public AtLeastOneConstraint create(Dictionary dictionary) {
        return new AtLeastOneConstraint(dictionary);
    }

    public static AtLeastOneConstraint create(Dictionary dictionary,
                                              String params) {
        return new AtLeastOneConstraint(dictionary).setExpressions(fromJson(params));
    }

    public static void elaborate() {
        // Ignore
    }
}