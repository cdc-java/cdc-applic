package cdc.applic.dictionaries.core.constraints.matrix;

/**
 * Enumeration of possible validity of a {@link RowId}.
 *
 * @author Damien Carbonne
 */
public enum RowIdValidity {
    /**
     * RowId validity is undefined.
     * <p>
     * It was probably not yet computed because no Prover was provided.
     */
    UNKNOWN,

    /**
     * RowId is valid.
     */
    VALID,

    /**
     * RowId is semantically valid, but can not be satisfied.
     */
    UNSATISAFIABLE,

    /**
     * RowId is semantically invalid.
     * <p>
     * The associated expression is semantically invalid because since RowId was created some type values have been removed.
     */
    INVALID
}