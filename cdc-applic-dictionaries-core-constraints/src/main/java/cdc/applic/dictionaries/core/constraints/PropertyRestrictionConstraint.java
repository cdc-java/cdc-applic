package cdc.applic.dictionaries.core.constraints;

import java.io.StringReader;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParserFactory;

import cdc.applic.dictionaries.Constraints;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Expressions;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.content.UncheckedSet;
import cdc.applic.expressions.literals.Name;
import cdc.io.json.JsonEvent;
import cdc.io.json.JsonpParser;
import cdc.io.json.JsonpUtils;
import cdc.util.lang.Checks;

/**
 * A constraint that restricts the values that can be used for a property.
 *
 * @author Damien Carbonne
 */
public class PropertyRestrictionConstraint extends AbstractConstraint {
    public static final String CONSTRAINT_NAME = "PROPERTY_RESTRICTION";

    private static final String PROPERTY = "property";
    private static final String VALUES = "values";

    private Property property;
    private SItemSet values = UncheckedSet.EMPTY;

    static {
        Constraints.register(CONSTRAINT_NAME, PropertyRestrictionConstraint::create);
    }

    public PropertyRestrictionConstraint(Dictionary dictionary) {
        super(dictionary);
    }

    @Override
    public String getTypeName() {
        return CONSTRAINT_NAME;
    }

    private void updateAssertion() {
        dictionary.removeRelatedAndDerivedAssertions(this);
        if (property != null) {
            final Expression expression = Expressions.NO_FORMATTING_NO_SIMPLIFY.inOrEqual(property.getName(), values);
            dictionary.createAssertion(this, null, expression);
        }
    }

    public Property getProperty() {
        return property;
    }

    public PropertyRestrictionConstraint setProperty(Property property) {
        this.property = property;
        this.values = UncheckedSet.EMPTY;
        updateAssertion();
        return this;
    }

    public PropertyRestrictionConstraint setProperty(String propertyName) {
        return setProperty(dictionary.getOptionalAllowedProperty(Name.of(propertyName)).orElseThrow());
    }

    public SItemSet getValues() {
        return values;
    }

    public PropertyRestrictionConstraint setValues(SItemSet values) {
        Checks.isNotNull(values, "values");
        Checks.isNotNull(property, "property. Can not set values before property.");
        Checks.isTrue(property.getType().isCompliant(values), "Values not compliant with property.");

        this.values = values;
        updateAssertion();
        return this;
    }

    public PropertyRestrictionConstraint setValues(String content) {
        return setValues(SItemSetUtils.createBest(content));
    }

    @Override
    public String getParams() {
        return JsonpUtils.toString(generator -> {
            generator.writeStartObject();
            generator.writeKey(PROPERTY);
            generator.write(getProperty().getName().getNonEscapedLiteral());
            generator.writeKey(VALUES);
            generator.write(getValues().getContent());
            generator.writeEnd();
        });
    }

    public static PropertyRestrictionConstraint create(Dictionary dictionary,
                                                       String params) {
        final JsonParserFactory factory = Json.createParserFactory(null);

        try (final StringReader reader = new StringReader(params);
                final JsonParser parser = factory.createParser(reader)) {
            final JsonpParser wrapper = new JsonpParser(parser);
            wrapper.next().expect(JsonEvent.START_OBJECT);
            wrapper.next().expectKeyName(PROPERTY);
            final String propertyName = wrapper.next().getStringValue();
            wrapper.next().expectKeyName(VALUES);
            final String content = wrapper.next().getStringValue();
            wrapper.next().expect(JsonEvent.END_OBJECT);
            return new PropertyRestrictionConstraint(dictionary).setProperty(propertyName)
                                                                .setValues(content);
        }
    }

    public static void elaborate() {
        // Ignore
    }
}