package cdc.applic.dictionaries.core.constraints;

import cdc.applic.dictionaries.Constraint;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.impl.DescriptionImpl;
import cdc.util.lang.Checks;

/**
 * Base abstract implementation of Constraint.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractConstraint implements Constraint {
    protected final Dictionary dictionary;
    private final DescriptionImpl description = new DescriptionImpl();

    protected AbstractConstraint(Dictionary dictionary) {
        Checks.isNotNull(dictionary, "dictionary");
        this.dictionary = dictionary;
    }

    @Override
    public Dictionary getOwner() {
        return dictionary;
    }

    @Override
    public final DescriptionImpl getDescription() {
        return description;
    }
}