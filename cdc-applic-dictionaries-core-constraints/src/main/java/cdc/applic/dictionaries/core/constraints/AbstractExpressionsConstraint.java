package cdc.applic.dictionaries.core.constraints;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.stream.JsonParser;
import javax.json.stream.JsonParserFactory;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.expressions.Expression;
import cdc.io.json.JsonEvent;
import cdc.io.json.JsonpParser;
import cdc.io.json.JsonpUtils;

/**
 * Base abstract class for constraints that are defined by a list of expressions.
 *
 * @author Damien Carbonne
 *
 * @param <C> The concrete constraint type.
 */
public abstract class AbstractExpressionsConstraint<C extends AbstractExpressionsConstraint<C>> extends AbstractConstraint {
    protected final List<Expression> expressions = new ArrayList<>();

    protected AbstractExpressionsConstraint(Dictionary dictionary) {
        super(dictionary);
    }

    /**
     * @return This constraint.
     */
    protected abstract C self();

    /**
     * Method that is called each time associated generated assertions must be updated.
     */
    protected abstract void updateAssertions();

    public abstract C create(Dictionary dictionary);

    /**
     * @return The defining expressions.
     */
    public final List<Expression> getExpressions() {
        return expressions;
    }

    /**
     * Sets the expressions defining this constraint.
     *
     * @param expressions The expressions.
     * @return This constraint.
     */
    public final C setExpressions(List<Expression> expressions) {
        this.expressions.clear();
        this.expressions.addAll(expressions);
        updateAssertions();
        return self();
    }

    /**
     * Sets the expressions defining this constraint.
     *
     * @param expressions The expressions.
     * @return This constraint.
     */
    public final C setExpressions(String... expressions) {
        return setExpressions(Expression.toExpressionList(expressions));
    }

    @Override
    public final String getParams() {
        return toJson(expressions);
    }

    public static String toJson(List<Expression> expressions) {
        return JsonpUtils.toString(generator -> {
            generator.writeStartArray();
            for (final Expression expression : expressions) {
                generator.write(expression.getContent());
            }
            generator.writeEnd();
        });
    }

    public static List<Expression> fromJson(String s) {
        final List<Expression> list = new ArrayList<>();
        final JsonParserFactory factory = Json.createParserFactory(null);

        try (final StringReader reader = new StringReader(s);
                final JsonParser parser = factory.createParser(reader)) {
            final JsonpParser wrapper = new JsonpParser(parser);
            wrapper.next().expect(JsonEvent.START_ARRAY);
            while (wrapper.hasNext()) {
                wrapper.next();
                if (wrapper.isOn(JsonEvent.VALUE_STRING)) {
                    final String expression = wrapper.getStringValue();
                    list.add(new Expression(expression));
                } else {
                    wrapper.expect(JsonEvent.END_ARRAY);
                    break;
                }
            }
        }
        return list;
    }
}