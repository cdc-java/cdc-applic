package cdc.applic.charts;

import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import cdc.applic.expressions.Spacing;
import cdc.applic.expressions.SymbolType;
import cdc.gv.tools.GvEngine;
import cdc.gv.tools.GvFormat;

/**
 * Features used to configure a {@link DictionaryToGvGenerator}.
 *
 * @author Damien Carbonne
 */
public class DictionaryToGvGeneratorFeatures {
    public enum Hint {
        /** If enabled, the generated graphs are directed. */
        DIRECTED,
        /** If enabled, a graph is generated for each policy. */
        ALL_POLICIES,
        /** If enabled, types are displayed. */
        SHOW_TYPES,
        /** If enabled, descriptions are displayed. */
        SHOW_DESCRIPTIONS,
        /** If enabled, issues are displayed. */
        SHOW_ISSUES,
        /** If enabled, graphs are laid out horizontal. */
        HORIZONTAL,
        /** If enabled, types, propertied, aliases and assertions are grouped. */
        GROUPS
    }

    private final Set<Hint> hints;
    private final SymbolType symbolType;
    private final Spacing spacing;
    private final GvEngine engine;
    private final Set<GvFormat> formats;

    protected DictionaryToGvGeneratorFeatures(Set<Hint> hints,
                                              SymbolType symbolType,
                                              Spacing spacing,
                                              GvEngine engine,
                                              Set<GvFormat> formats) {
        this.hints = EnumSet.copyOf(hints);
        this.symbolType = symbolType;
        this.spacing = spacing;
        this.engine = engine;
        this.formats = Collections.unmodifiableSet(new HashSet<>(formats));
    }

    public boolean isEnabled(Hint hint) {
        return hints.contains(hint);
    }

    /**
     * @return The SymbolType to use to display expressions.
     */
    public SymbolType getSymbolType() {
        return symbolType;
    }

    /**
     * @return The Spacing to use to display expressions.
     */
    public Spacing getSpacing() {
        return spacing;
    }

    /**
     * @return The GvEngine to use to layout generated graph.
     */
    public GvEngine getEngine() {
        return engine;
    }

    /**
     * @return The GvFormats of images to generate. It may be empty.
     */
    public Set<GvFormat> getFormats() {
        return formats;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private final Set<Hint> hints = EnumSet.noneOf(Hint.class);
        private SymbolType symbolType = SymbolType.MATH;
        private Spacing spacing = Spacing.NARROW;
        private GvEngine engine = GvEngine.DOT;
        private final Set<GvFormat> formats = EnumSet.noneOf(GvFormat.class);

        protected Builder() {
            super();
        }

        public Builder hint(Hint hint) {
            this.hints.add(hint);
            return this;
        }

        public Builder symbolType(SymbolType symbolType) {
            this.symbolType = symbolType;
            return this;
        }

        public Builder spacing(Spacing spacing) {
            this.spacing = spacing;
            return this;
        }

        public Builder engine(GvEngine engine) {
            this.engine = engine;
            return this;
        }

        public Builder format(GvFormat format) {
            this.formats.add(format);
            return this;
        }

        public Builder formats(GvFormat... formats) {
            for (final GvFormat format : formats) {
                this.formats.add(format);
            }
            return this;
        }

        public DictionaryToGvGeneratorFeatures build() {
            return new DictionaryToGvGeneratorFeatures(hints,
                                                       symbolType,
                                                       spacing,
                                                       engine,
                                                       formats);
        }
    }
}