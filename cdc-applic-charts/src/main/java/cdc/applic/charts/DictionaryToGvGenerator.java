package cdc.applic.charts;

import java.io.File;
import java.io.IOException;

import cdc.applic.dictionaries.Dictionary;

/**
 * Interface implement by objects that can generate a GraphViz representation of a Dictionary.
 *
 * @author Damien Carbonne
 */
public interface DictionaryToGvGenerator {
    /**
     * Generates a GraphViz representation of a Dictionary.
     * <p>
     * This will generate a '.gv' file, named after the dictionary name.
     * It will also generate images, if asked.
     *
     * @param dictionary The dictionary.
     * @param outputDir The directory where files are generated.
     * @param features The features used for generation.
     * @throws IOException When an IO error occurs.
     */
    public void generate(Dictionary dictionary,
                         File outputDir,
                         DictionaryToGvGeneratorFeatures features) throws IOException;
}