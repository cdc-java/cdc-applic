package cdc.applic.publication.html;

/**
 * Definition of basic font attributes.
 *
 * @author Damien Carbonne
 */
public class FontSettings {
    public static final FontSettings DEFAULT = builder().build();

    private final String style;
    private final String face;
    private final String color;
    private final String size;
    private final boolean bold;
    private final boolean italic;
    private final boolean underline;

    protected FontSettings(Builder builder) {
        this.style = builder.style;
        this.face = builder.face;
        this.color = builder.color;
        this.size = builder.size;
        this.bold = builder.bold;
        this.italic = builder.italic;
        this.underline = builder.underline;
    }

    private static boolean isSet(String s) {
        return s != null && !s.isEmpty();
    }

    public boolean useStyle() {
        return isSet(style);
    }

    public boolean useLegacy() {
        return isSet(face) || isSet(color) || isSet(size);
    }

    public String getStyle() {
        return style;
    }

    public String getFace() {
        return face;
    }

    public String getColor() {
        return color;
    }

    public String getSize() {
        return size;
    }

    public boolean isBold() {
        return bold;
    }

    public boolean isItalic() {
        return italic;
    }

    public boolean isUnderline() {
        return underline;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String style;
        private String face;
        private String color;
        private String size;
        private boolean bold = false;
        private boolean italic = false;
        private boolean underline = false;

        protected Builder() {
            super();
        }

        public Builder style(String style) {
            this.style = style;
            return this;
        }

        public Builder face(String face) {
            this.face = face;
            return this;
        }

        public Builder color(String color) {
            this.color = color;
            return this;
        }

        public Builder size(String size) {
            this.size = size;
            return this;
        }

        public Builder bold() {
            this.bold = true;
            return this;
        }

        public Builder italic() {
            this.italic = true;
            return this;
        }

        public Builder underline() {
            this.underline = true;
            return this;
        }

        public FontSettings build() {
            return new FontSettings(this);
        }
    }
}