package cdc.applic.publication.html;

/**
 * Enumeration of possible categories of text to which a style can be associated.
 *
 * @author Damien Carbonne
 */
public enum Category {
    /** Text is anything. Used when no specific value is set. */
    DEFAULT,
    /** Text is informal expression. */
    INFORMAL,
    /** Text is a valid value (boolean, integer, string, real), or the empty set symbol. */
    VALUE,
    /** Text is an invalid value. */
    INVALID_VALUE,
    /** Text is a value whose validity is unknown. This is related to an unknown property. */
    UNKNOWN_VALUE,
    /** Text is a delimiter ({, }, ~, (, )). */
    DELIMITER,
    /** Text is an operator ({@code =, !=, <, ...}). */
    OPERATOR,
    /** Text is a prefix. */
    PREFIX,
    /** Text is a known property local name. */
    PROPERTY,
    /** Text is a known alias local name. */
    ALIAS,
    /** Text is unknown property or alias local name or prefix. */
    INVALID_NAME,
    /** Text is a space. */
    SPACE
}
