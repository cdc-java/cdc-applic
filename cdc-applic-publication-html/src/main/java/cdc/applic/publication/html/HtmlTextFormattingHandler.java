package cdc.applic.publication.html;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.InformalText;
import cdc.applic.expressions.literals.Name;
import cdc.applic.publication.FormattingHandler;
import cdc.applic.publication.NameValidity;
import cdc.applic.publication.Symbol;
import cdc.applic.publication.ValueValidity;
import cdc.util.lang.Checks;
import cdc.util.lang.UnexpectedValueException;

public class HtmlTextFormattingHandler implements FormattingHandler {
    private final HtmlTextSettings settings;
    private final HtmlHandler handler;

    public HtmlTextFormattingHandler(HtmlTextSettings settings,
                                     HtmlHandler handler) {
        Checks.isNotNull(settings, "settings");
        Checks.isNotNull(handler, "handler");

        this.settings = settings;
        this.handler = handler;
    }

    public HtmlTextSettings getSettings() {
        return settings;
    }

    public HtmlHandler getHandler() {
        return handler;
    }

    private void addAttribute(String name,
                              String value) {
        if (value != null && !value.isEmpty()) {
            handler.addAttribute(name, value);
        }
    }

    private void beginFont(FontSettings settings) {
        if (settings.useStyle()) {
            handler.startOpenTag("font");
            addAttribute("style", settings.getStyle());
            handler.endOpenTag();
        } else if (settings.useLegacy()) {
            handler.startOpenTag("font");
            addAttribute("face", settings.getFace());
            addAttribute("color", settings.getColor());
            addAttribute("size", settings.getSize());
            handler.endOpenTag();
        }
        if (settings.isBold()) {
            handler.openTag("b");
        }
        if (settings.isItalic()) {
            handler.openTag("i");
        }
        if (settings.isUnderline()) {
            handler.openTag("u");
        }
    }

    private void endFont(FontSettings settings) {
        if (settings.isUnderline()) {
            handler.closeTag("u");
        }
        if (settings.isItalic()) {
            handler.closeTag("i");
        }
        if (settings.isBold()) {
            handler.closeTag("b");
        }
        if (settings.useStyle() || settings.useLegacy()) {
            handler.closeTag("font");
        }
    }

    private void addText(String text,
                         Category category) {
        if (text != null) {
            beginFont(settings.getFontSettings(category));
            handler.addText(text);
            endFont(settings.getFontSettings(category));
        }
    }

    @Override
    public void beginExpression(Expression expression) {
        beginFont(settings.getFontSettings(Category.DEFAULT));
    }

    @Override
    public void appendSpace(String text) {
        addText(text, Category.SPACE);
    }

    @Override
    public void appendSymbol(Symbol symbol,
                             String text) {
        if (symbol.isValue()) {
            addText(text, Category.VALUE);
        } else if (symbol.isOperator()) {
            addText(text, Category.OPERATOR);
        } else {
            addText(text, Category.DELIMITER);
        }
    }

    @Override
    public void appendPrefix(Name name,
                             String text,
                             NameValidity validity) {
        switch (validity) {
        case VALID -> addText(text, Category.PREFIX);
        case INVALID -> addText(text, Category.INVALID_NAME);
        default -> throw new UnexpectedValueException(validity);
        }
    }

    @Override
    public void appendPropertyLocalName(Name name,
                                        String text,
                                        NameValidity validity) {
        switch (validity) {
        case VALID -> addText(text, Category.PROPERTY);
        case INVALID -> addText(text, Category.INVALID_NAME);
        default -> throw new UnexpectedValueException(validity);
        }
    }

    @Override
    public void appendAliasLocalName(Name name,
                                     String text) {
        addText(text, Category.ALIAS);
    }

    @Override
    public void appendInvalidRefLocalName(Name name,
                                          String text) {
        addText(text, Category.INVALID_NAME);
    }

    @Override
    public void appendValue(Value value,
                            String text,
                            ValueValidity validity) {
        switch (validity) {
        case VALID -> addText(text, Category.VALUE);
        case INVALID -> addText(text, Category.INVALID_VALUE);
        case UNKNOWN -> addText(text, Category.UNKNOWN_VALUE);
        default -> throw new UnexpectedValueException(validity);
        }
    }

    @Override
    public void appendInformalText(InformalText informal,
                                   String text) {
        addText(text, Category.INFORMAL);
    }

    @Override
    public void endExpression() {
        endFont(settings.getFontSettings(Category.DEFAULT));
    }
}