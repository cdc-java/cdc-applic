package cdc.applic.publication.html;

public interface HtmlHandler {
    public void startOpenTag(String name);

    public void addAttribute(String name,
                             String value);

    public void endOpenTag();

    public default void openTag(String name) {
        startOpenTag(name);
        endOpenTag();
    }

    public void addText(String text);

    public void closeTag(String name);
}