package cdc.applic.publication.html;

import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 * Definition of text styles for each {@link Category} of text.
 *
 * @author Damien Carbonne
 */
public class HtmlTextSettings {
    public static final HtmlTextSettings DEFAULT = builder().build();

    private final Map<Category, FontSettings> settings;

    protected HtmlTextSettings(Builder builder) {
        this.settings = Collections.unmodifiableMap(new HashMap<>(builder.settings));
    }

    public FontSettings getFontSettings(Category category) {
        return settings.getOrDefault(category, FontSettings.DEFAULT);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        protected final Map<Category, FontSettings> settings = new EnumMap<>(Category.class);

        public Builder set(Category category,
                           FontSettings settings) {
            this.settings.put(category, settings);
            return this;
        }

        public HtmlTextSettings build() {
            return new HtmlTextSettings(this);
        }
    }
}