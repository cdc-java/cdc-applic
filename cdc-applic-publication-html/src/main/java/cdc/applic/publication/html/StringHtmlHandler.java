package cdc.applic.publication.html;

import org.apache.commons.text.StringEscapeUtils;

public class StringHtmlHandler implements HtmlHandler {
    private final StringBuilder builder;

    public StringHtmlHandler() {
        this.builder = new StringBuilder();
    }

    public StringHtmlHandler(StringBuilder builder) {
        this.builder = builder;
    }

    public StringBuilder getStringBuilder() {
        return builder;
    }

    @Override
    public void startOpenTag(String name) {
        builder.append("<" + name);
    }

    @Override
    public void addAttribute(String name,
                             String value) {
        builder.append(" " + name + "=\"" + value + "\"");
    }

    @Override
    public void endOpenTag() {
        builder.append(">");
    }

    @Override
    public void addText(String text) {
        builder.append(StringEscapeUtils.escapeHtml3(text));
    }

    @Override
    public void closeTag(String name) {
        builder.append("</" + name + ">");
    }

    @Override
    public String toString() {
        return builder.toString();
    }
}