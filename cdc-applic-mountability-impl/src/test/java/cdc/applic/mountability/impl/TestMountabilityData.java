package cdc.applic.mountability.impl;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import cdc.applic.expressions.Expression;
import cdc.applic.mountability.Interchangeability;
import cdc.applic.mountability.MountabilityData;

public class TestMountabilityData implements MountabilityData<TestUsePoint, TestVariant> {
    private final List<TestUsePoint> usePoints = new ArrayList<>();

    public TestMountabilityData addUsePoint(TestUsePoint usePoint) {
        usePoints.add(usePoint);
        return this;
    }

    public void clear() {
        usePoints.clear();
    }

    @Override
    public Iterable<TestUsePoint> getUsePoints() {
        return usePoints;
    }

    @Override
    public List<TestVariant> getVariants(TestUsePoint usePoint) {
        return usePoint.getVariants();
    }

    @Override
    public Interchangeability getVariantInterchangeability(TestUsePoint usePoint,
                                                           TestVariant variant) {
        return variant.getInterchangeability();
    }

    @Override
    public Expression getVariantApplicability(TestUsePoint usePoint,
                                              TestVariant variant) {
        return variant.getApplicability();
    }

    public void print(PrintStream out) {
        out.println("=================================================");
        for (final TestUsePoint usePoint : usePoints) {
            out.println("UsePoint: " + usePoint.getId());
            for (final TestVariant variant : usePoint.getVariants()) {
                out.println("   Variant: " + variant);
            }
        }
        out.println("=================================================");
    }
}