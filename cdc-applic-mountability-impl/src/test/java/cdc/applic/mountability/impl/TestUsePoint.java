package cdc.applic.mountability.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cdc.applic.expressions.Expression;
import cdc.applic.mountability.Interchangeability;

public class TestUsePoint {
    private final int id;
    private final List<TestVariant> variants = new ArrayList<>();

    public TestUsePoint(int id) {
        this.id = id;
    }

    public TestUsePoint addVariant(Interchangeability interchangeability,
                                   Expression applicability) {
        variants.add(new TestVariant(variants.size() + 1,
                                     interchangeability,
                                     applicability));
        return this;
    }

    public TestUsePoint addVariant(Interchangeability interchangeability,
                                   String applicability) {
        return addVariant(interchangeability, new Expression(applicability));
    }

    public int getId() {
        return id;
    }

    public List<TestVariant> getVariants() {
        return variants;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,
                            variants);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof TestUsePoint)) {
            return false;
        }
        final TestUsePoint other = (TestUsePoint) object;
        return this.id == other.id
                && this.variants.equals(other.variants);
    }

    @Override
    public String toString() {
        return Integer.toString(getId());
    }
}