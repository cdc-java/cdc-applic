package cdc.applic.mountability.impl;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import cdc.applic.expressions.Expression;
import cdc.applic.mountability.Interchangeability;
import cdc.applic.mountability.MountabilityData;

/**
 * Default implementation of {@link MountabilityData}.
 */
public class Data implements MountabilityData<UsePoint, Variant> {
    private final List<UsePoint> usePoints = new ArrayList<>();

    public Data addUsePoint(UsePoint usePoint) {
        usePoints.add(usePoint);
        return this;
    }

    public void clear() {
        usePoints.clear();
    }

    @Override
    public Iterable<UsePoint> getUsePoints() {
        return usePoints;
    }

    @Override
    public String getUsePointId(UsePoint usePoint) {
        return usePoint.getId();
    }

    @Override
    public List<Variant> getVariants(UsePoint usePoint) {
        return usePoint.getVariants();
    }

    @Override
    public int getUsePointsCount() {
        return usePoints.size();
    }

    @Override
    public int getVariantsCount() {
        int count = 0;
        for (final UsePoint usePoint : usePoints) {
            count += usePoint.getVariants().size();
        }
        return count;
    }

    @Override
    public String getVariantLocalId(UsePoint usePoint,
                                    Variant variant) {
        return variant.getId();
    }

    @Override
    public Interchangeability getVariantInterchangeability(UsePoint usePoint,
                                                           Variant variant) {
        return variant.getInterchangeability();
    }

    @Override
    public Expression getVariantApplicability(UsePoint usePoint,
                                              Variant variant) {
        return variant.getApplicability();
    }

    public void print(PrintStream out) {
        out.println("=================================================");
        for (final UsePoint usePoint : usePoints) {
            out.println("UsePoint: " + usePoint.getId());
            for (final Variant variant : usePoint.getVariants()) {
                out.println("   Variant: " + variant);
            }
        }
        out.println("=================================================");
    }
}