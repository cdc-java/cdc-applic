package cdc.applic.mountability.impl;

import java.util.Objects;

import cdc.applic.expressions.Expression;
import cdc.applic.mountability.Interchangeability;

/**
 * Default implementation of Variant.
 */
public class Variant {
    private final String id;
    private final Interchangeability interchangeability;
    private final Expression applicability;
    private final Expression mountability;

    public Variant(String id,
                   Interchangeability interchangeability,
                   Expression applicability,
                   Expression mountability) {
        this.id = id;
        this.interchangeability = interchangeability;
        this.applicability = applicability;
        this.mountability = mountability;
    }

    public String getId() {
        return id;
    }

    public Interchangeability getInterchangeability() {
        return interchangeability;
    }

    public Expression getApplicability() {
        return applicability;
    }

    public Expression getMountability() {
        return mountability;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,
                            interchangeability,
                            applicability,
                            mountability);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Variant)) {
            return false;
        }
        final Variant other = (Variant) object;
        return Objects.equals(this.id, other.id)
                && this.interchangeability == other.interchangeability
                && Objects.equals(this.applicability, other.applicability)
                && Objects.equals(this.mountability, other.mountability);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append('[').append(getId());
        if (getInterchangeability() != null) {
            builder.append(' ').append(getInterchangeability());
        }
        builder.append(' ').append(getApplicability());
        if (getMountability() != null) {
            builder.append(' ').append(getMountability());
        }
        builder.append(')');
        return builder.toString();
    }
}