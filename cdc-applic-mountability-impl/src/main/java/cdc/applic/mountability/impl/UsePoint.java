package cdc.applic.mountability.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cdc.applic.expressions.Expression;
import cdc.applic.mountability.Interchangeability;

/**
 * Default implementation of UsePoint.
 */
public class UsePoint {
    private final String id;
    private final List<Variant> variants = new ArrayList<>();

    public UsePoint(String id) {
        this.id = id;
    }

    public UsePoint addVariant(String id,
                               Interchangeability interchangeability,
                               Expression applicability,
                               Expression mountability) {
        variants.add(new Variant(id,
                                 interchangeability,
                                 applicability,
                                 mountability));
        return this;
    }

    public UsePoint addVariant(String id,
                               Interchangeability interchangeability,
                               Expression applicability) {
        return addVariant(id, interchangeability, applicability, null);
    }

    public String getId() {
        return id;
    }

    public List<Variant> getVariants() {
        return variants;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,
                            variants);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof UsePoint)) {
            return false;
        }
        final UsePoint other = (UsePoint) object;
        return Objects.equals(this.id, other.id)
                && this.variants.equals(other.variants);
    }

    @Override
    public String toString() {
        return getId();
    }
}