package cdc.applic.mountability.impl.io;

import java.io.IOException;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.LexicalException;
import cdc.applic.expressions.SyntacticException;
import cdc.applic.mountability.Interchangeability;
import cdc.applic.mountability.impl.Data;
import cdc.applic.mountability.impl.UsePoint;
import cdc.applic.mountability.impl.Variant;
import cdc.applic.publication.ExpressionFormatter;
import cdc.io.xml.AbstractStAXLoader;
import cdc.io.xml.AbstractStAXParser;
import cdc.io.xml.XmlWriter;
import cdc.util.lang.FailureReaction;

/**
 * XML IO of mountability data.
 */
public final class DataXml {
    private static final String MOUNTABILITY_DATA = "mountability-data";
    private static final String USE_POINT = "use-point";
    private static final String VARIANT = "variant";
    private static final String ID = "id";
    private static final String INTERCHANGEABILITY = "interchangeability";
    private static final String APPLICABILITY = "applicability";
    private static final String MOUNTABILITY = "mountability";
    private static final String FORMATTED_MOUNTABILITY = "formatted-mountability";

    private DataXml() {
    }

    /**
     * XML writing of mountability data.
     */
    public static class Printer {
        private Formatting formatting = Formatting.LONG_NARROW;
        private ExpressionFormatter formatter = null;

        public Printer() {
            super();
        }

        public Printer setFormatting(Formatting formatting) {
            this.formatting = formatting;
            return this;
        }

        public Printer setFormatter(ExpressionFormatter formatter) {
            this.formatter = formatter;
            return this;
        }

        public void write(XmlWriter writer,
                          Data data) throws IOException {
            final String namespace = "https://www.gitlab.com/cdc-java";
            final String schema = "https://www.gitlab.com/cdc-java/applic-mountability.xsd";
            writer.beginDocument();
            writer.beginElement(MOUNTABILITY_DATA);
            writer.addDefaultNamespace(namespace);
            writer.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            writer.addAttribute("xsi:schemaLocation", namespace + " " + schema);

            for (final UsePoint usePoint : data.getUsePoints()) {
                writeUsePoint(writer, usePoint);
            }

            writer.endElement();
            writer.endDocument();
        }

        private void writeUsePoint(XmlWriter writer,
                                   UsePoint usePoint) throws IOException {
            writer.beginElement(USE_POINT);
            if (usePoint.getId() != null) {
                writer.addAttribute(ID, usePoint.getId());
            }
            for (final Variant variant : usePoint.getVariants()) {
                writeVariant(writer, variant);
            }
            writer.endElement();
        }

        private void writeVariant(XmlWriter writer,
                                  Variant variant) throws IOException {
            writer.beginElement(VARIANT);
            if (variant.getId() != null) {
                writer.addAttribute(ID, variant.getId());
            }
            if (variant.getInterchangeability() != null) {
                writer.addAttribute(INTERCHANGEABILITY, variant.getInterchangeability());
            }
            writer.addAttribute(APPLICABILITY, variant.getApplicability());
            if (variant.getMountability() != null) {
                writer.addAttribute(MOUNTABILITY, variant.getMountability().toInfix(formatting));
                if (formatter != null) {
                    writer.addAttribute(FORMATTED_MOUNTABILITY, formatter.formatAsString(variant.getMountability()));
                }
            }
            writer.endElement();
        }
    }

    /**
     * XML loading of mountability data.
     */
    public static class StAXLoader extends AbstractStAXLoader<Data> {
        /**
         * Creates a loader.
         *
         * @param reaction The reaction to adopt in case of failure in XML processing.
         * @param checkExpressions If {@code true}, expressions are checked for their syntax.
         *            This will interrupt loading. If one wants to analyze the full data set, then this should be passed to {@code
         *            false}.
         * @throws LexicalException When an expression is lexically invalid and {@code checkExpressions} is {@code true}.
         * @throws SyntacticException When an expression is syntactically invalid and {@code checkExpressions} is {@code true}.
         */
        public StAXLoader(FailureReaction reaction,
                          boolean checkExpressions) {
            super((reader,
                   systemId) -> new Parser(reader, systemId, reaction, checkExpressions));
        }

        /**
         * Creates a loader.
         *
         * @param reaction The reaction to adopt in case of failure in XML processing.
         * @throws LexicalException When an expression is lexically invalid.
         * @throws SyntacticException When an expression is syntactically invalid.
         */
        public StAXLoader(FailureReaction reaction) {
            this(reaction, true);

        }

        private static class Parser extends AbstractStAXParser<Data> {
            private final boolean checkExpressions;

            protected Parser(XMLStreamReader reader,
                             String systemId,
                             FailureReaction reaction,
                             boolean checkExpressions) {
                super(reader, systemId, reaction);
                this.checkExpressions = checkExpressions;
            }

            @Override
            protected Data parse() throws XMLStreamException {
                final String ctx = "parse()";
                trace(ctx);
                // Move to root start tag
                nextTag();

                if (isStartElement(MOUNTABILITY_DATA)) {
                    final Data result = parseMountabilityData();
                    next();
                    return result;
                } else {
                    throw unexpectedEvent();
                }
            }

            private Data parseMountabilityData() throws XMLStreamException {
                final String ctx = "parseMountabilityData()";
                trace(ctx);
                final Data data = new Data();
                nextTag();
                while (isStartElement(USE_POINT)) {
                    parseUsePoint(data);
                    nextTag();
                }
                return data;
            }

            private void parseUsePoint(Data data) throws XMLStreamException {
                final String ctx = "parseUsePoint()";
                trace(ctx);
                expectStartElement(ctx, USE_POINT);
                final String id = getAttributeValue(ID, null);
                final UsePoint usePoint = new UsePoint(id);
                nextTag();
                while (isStartElement(VARIANT)) {
                    parseVariant(usePoint);
                    nextTag();
                }
                expectEndElement(ctx, USE_POINT);

                data.addUsePoint(usePoint);
            }

            private void parseVariant(UsePoint usePoint) throws XMLStreamException {
                final String ctx = "parseVariant()";
                trace(ctx);
                expectStartElement(ctx, VARIANT);
                final String id = getAttributeValue(ID, null);
                final Interchangeability interchangeability =
                        getAttributeAsEnum(INTERCHANGEABILITY, Interchangeability.class, null);
                final String applicability = getAttributeValue(APPLICABILITY, null);
                final String mountability = getAttributeValue(MOUNTABILITY, null);
                nextTag();
                expectEndElement(ctx, VARIANT);

                usePoint.addVariant(id,
                                    interchangeability,
                                    new Expression(applicability, checkExpressions),
                                    mountability == null ? null : new Expression(mountability, checkExpressions));
            }
        }
    }
}