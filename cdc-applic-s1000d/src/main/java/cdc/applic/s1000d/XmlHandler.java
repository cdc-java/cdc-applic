package cdc.applic.s1000d;

import java.io.IOException;

/**
 * Interface abstracting the generation of an XML tree.
 * <p>
 * It only contains the necessary features for S1000D processing.
 * <p>
 * A typical use would be:
 * <pre>
 * <code>
 * beginElement("root");
 *    beginElement("child");
 *    addAttribute("id", "1");
 *    addAttribute("name", "Name1");
 *    endElement();
 *
 *    beginElement("child");
 *    addAttribute("id", "2");
 *    addAttribute("name", "Name2");
 *    endElement();
 * endElement();
 * </code>
 * </pre>
 *
 * @author Damien Carbonne
 */
public interface XmlHandler {
    /**
     * Invoked when an element is started.
     * <p>
     * Must match a corresponding {@link #endElement()}.
     *
     * @param name The element name.
     * @throws IOException When an IO error occurs.
     */
    public void beginElement(String name) throws IOException;

    /**
     * Invoked to add an attribute to current element (that is still open).
     * <p>
     * Must be called just after {@link #beginElement(String)} or another {@link #addAttribute(String, String)}.
     *
     * @param name The attribute name.
     * @param value The attribute value.
     * @throws IOException When an IO error occurs.
     */
    public void addAttribute(String name,
                             String value) throws IOException;

    /**
     * Invoked to add content to current element (that is still open).
     *
     * @param content The content.
     * @throws IOException When an IO error occurs.
     */
    public void addElementContent(String content) throws IOException;

    /**
     * Invoked when an element is closed.
     * <p>
     * Must match a corresponding {@link #beginElement(String)}.
     *
     * @throws IOException When an IO error occurs.
     */
    public void endElement() throws IOException;
}