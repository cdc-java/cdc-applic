package cdc.applic.s1000d;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.dictionaries.s1000d.S1000DType;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.content.SItemSet;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;

/**
 * Interface used to handle events related to CCT generation.
 *
 * @author Damien Carbonne
 */
public interface S1000DCctHandler extends IssuesHandler<Issue> {
    /**
     * Invoked when an CCT computation starts.
     *
     * @param handle The DictionaryHandle for which the CCT is computed.
     * @param additionalAssertion The additional assertion,
     *            used to reduce the applicability of passed dictionary.
     * @param convention The naming convention to use for generation.
     */
    public void beginCct(DictionaryHandle handle,
                         Expression additionalAssertion,
                         NamingConvention convention);

    /**
     * Invoked when generation of CCT condition types starts.
     */
    public void beginCctConditionTypes();

    /**
     * Invoked to notify addition of a condition type.
     * <p>
     * Exactly one of {@code pattern} and {@code domain} is defined.
     *
     * @param type The condition type.
     * @param pattern The optional pattern.
     * @param domain The optional domain,
     *            which is a subset of the domain of {@code type}.<br>
     *            <b>WARNING:</b> if {@code domain} is not {@code null} and empty, this is probably an error.
     * @param convention The naming convention to use for generation.
     */
    public void addCctConditionType(S1000DType type,
                                    String pattern,
                                    SItemSet domain,
                                    NamingConvention convention);

    /**
     * Invoked when generation of CCT condition types ends.
     */
    public void endCctConditionTypes();

    /**
     * Invoked when generation of CCT conditions starts.
     */
    public void beginCctConditions();

    /**
     * Invoked to generation of a CCT condition starts.
     *
     * @param condition The condition.
     * @param domain The condition domain,
     *            which is a subset of its {@code type} domain.
     * @param convention The naming convention to use for generation.
     */
    public void beginCctCondition(S1000DProperty condition,
                                  SItemSet domain,
                                  NamingConvention convention);

    /**
     * Invoked when to notify addition of a condition dependency.
     *
     * @param condition The condition.
     * @param conditionValues The values for which the dependency holds.
     * @param dependency The dependency condition.
     * @param dependencyValues The values of dependency.
     * @param convention The naming convention to use for generation.
     */
    public void addCctConditionDependency(S1000DProperty condition,
                                          SItemSet conditionValues,
                                          S1000DProperty dependency,
                                          SItemSet dependencyValues,
                                          NamingConvention convention);

    /**
     * Invoked when generation of a CCT condition ends.
     */
    public void endCctCondition();

    /**
     * Invoked when generation of CCT conditions ends.
     */
    public void endCctConditions();

    /**
     * Invoked when an CCT computation ends.
     */
    public void endCct();
}