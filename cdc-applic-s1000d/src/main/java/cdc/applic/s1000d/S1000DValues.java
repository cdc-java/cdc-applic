package cdc.applic.s1000d;

import java.util.Objects;

import cdc.applic.dictionaries.types.DomainedType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.expressions.IllegalOperationException;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.Value;

public class S1000DValues {
    private final SItemSet cutValues;
    private final SItemSet values;
    private final Filling filling;
    private final Interpretation interpretation;

    /**
     * Filling of values relatively to a reference set.
     * <p>
     * The reference set is the set of allowed values in the context of a PCT.<br>
     * It is a subset of all values declared by the type of associated property.<br>
     * Some values may be excluded because of assertions.
     *
     * @author Damien Carbonne
     */
    public enum Filling {
        /**
         * The set of values is empty.
         * <p>
         * <b>WARNING:</b> this probably signals an error when the associated type is a {@link DomainedType}.
         */
        EMPTY,

        /**
         * The set of values is a partial and a non empty subset of the reference set.
         */
        PARTIAL,

        /**
         * The set of values is non empty and equal to the reference set.
         */
        FULL
    }

    public enum Interpretation {
        /**
         * Interpretation of filling is normal. It can be done without any additional care.
         * <p>
         * This is used with sets associated to {@link DomainedType}s.
         */
        NORMAL,

        /**
         * Interpretation of filling is special. It must be done with care.
         * <p>
         * This is used with sets associated to {@link PatternType}s.
         */
        SPECIAL
    }

    public S1000DValues(SItemSet cutValues,
                        SItemSet values,
                        Filling filling,
                        Interpretation interpretation) {
        this.cutValues = cutValues;
        this.values = values;
        this.filling = filling;
        this.interpretation = interpretation;
    }

    public SItemSet getCutValues() {
        return cutValues;
    }

    public SItemSet getValues() {
        return values;
    }

    public Filling getFilling() {
        return filling;
    }

    public Interpretation getInterpretation() {
        return interpretation;
    }

    /**
     * @return {@code true} if {@code values} is a singleton.
     */
    public boolean isSingleton() {
        return values.isSingleton();
    }

    /**
     * @return The singleton value if {@code values} is a singleton.
     * @throws IllegalOperationException When {@code values} is not a singleton.
     */
    public Value getSingletonValue() {
        return values.getSingletonValue();
    }

    @Override
    public int hashCode() {
        return Objects.hash(values,
                            filling);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof S1000DValues)) {
            return false;
        }
        final S1000DValues other = (S1000DValues) object;
        return Objects.equals(values, other.values)
                && Objects.equals(filling, other.filling);
    }

    @Override
    public String toString() {
        return "[" + getValues()
                + "/" + getCutValues()
                + " " + getFilling()
                + " " + getInterpretation()
                + (isSingleton() ? " singleton" : "")
                + "]";
    }
}