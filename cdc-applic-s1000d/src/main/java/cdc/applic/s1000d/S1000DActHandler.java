package cdc.applic.s1000d;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.content.SItemSet;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;

/**
 * Interface used to handle events related to ACT generation.
 *
 * @author Damien Carbonne
 */
public interface S1000DActHandler extends IssuesHandler<Issue> {
    /**
     * Invoked when an ACT computation starts.
     *
     * @param handle The DictionaryHandle for which the ACT is computed.
     * @param additionalAssertion The additional assertion,
     *            used to reduce the applicability of passed dictionary.
     * @param convention The naming convention to use for generation.
     */
    public void beginAct(DictionaryHandle handle,
                         Expression additionalAssertion,
                         NamingConvention convention);

    /**
     * Invoked to notify addition of a product attribute.
     * <p>
     * Exactly one of {@code pattern} and {@code domain} is defined.
     *
     * @param property The product attribute.
     * @param pattern The optional pattern.
     * @param domain The optional domain,
     *            which is a subset of the domain of {@code property}.<br>
     *            <b>WARNING:</b> if {@code domain} is not {@code null} and is empty, this is probably an error.
     * @param convention The naming convention to use for generation.
     */
    public void addActProductAttribute(S1000DProperty property,
                                       String pattern,
                                       SItemSet domain,
                                       NamingConvention convention);

    /**
     * Invoked when an ACT computation ends.
     */
    public void endAct();
}