package cdc.applic.s1000d.issues;

import cdc.issues.Issue;

public final class S1000DIssues {
    public static final String DOMAIN = "S1000D";

    private S1000DIssues() {
    }

    public static Issue.Builder<?> builder() {
        return Issue.builder().domain(DOMAIN);
    }
}