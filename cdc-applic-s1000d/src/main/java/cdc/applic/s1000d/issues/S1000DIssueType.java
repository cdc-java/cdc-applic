package cdc.applic.s1000d.issues;

import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.issues.IssueSeverity;

public enum S1000DIssueType {
    CHECK_DICTIONARY(IssueSeverity.INFO),
    CHECKED_DICTIONARY(IssueSeverity.INFO),
    GENERATE_ACT(IssueSeverity.INFO),
    GENERATED_ACT(IssueSeverity.INFO),
    GENERATE_CCT(IssueSeverity.INFO),
    GENERATED_CCT(IssueSeverity.INFO),
    GENERATE_PCT(IssueSeverity.INFO),
    GENERATED_PCT(IssueSeverity.INFO),

    /**
     * A {@link Property} is not an  {@link S1000DProperty}.
     */
    NON_S1000D_PROPERTY(IssueSeverity.CRITICAL),

    /**
     * An {@link S1000DProperty} is not compliant because its {@link S1000DPropertyType} is not set.
     */
    NON_COMPLIANT_PROPERTY(IssueSeverity.MINOR),

    /**
     * No valid value can be set for a property.
     */
    CANNOT_SET_PROPERTY(IssueSeverity.CRITICAL),

    /**
     * Cannot set any value for any property.
     */
    CANNOT_SET_ANY_PROPERTY(IssueSeverity.CRITICAL),

    /**
     * No valid value can be set for master property.
     */
    CANNOT_SET_MASTER_PROPERTY(IssueSeverity.CRITICAL),

    /**
     * Some value are excluded (they can not be used).
     */
    EXCLUDED_VALUES(IssueSeverity.INFO),

    /**
     * Some products are excluded.
     * This means that some values of master property can not be set.
     */
    EXCLUDED_PRODUCTS(IssueSeverity.INFO),

    /**
     * Some properties can be set without any restriction.
     * <p>
     * For conditions, this is normal.
     * For Product attributes, this may be a problem.
     */
    NON_RESTRICTED_PROPERTIES(IssueSeverity.INFO),

    /**
     * Too many values are allowed for a product attribute.
     */
    TOO_MANY_VALUES(IssueSeverity.CRITICAL),

    /**
     * Something is used and is not supported by the target application.
     */
    NON_SUPPORTED_FEATURE(IssueSeverity.MINOR);

    private final IssueSeverity severity;

    private S1000DIssueType(IssueSeverity severity) {
        this.severity = severity;
    }

    public IssueSeverity getSeverity() {
        return severity;
    }
}