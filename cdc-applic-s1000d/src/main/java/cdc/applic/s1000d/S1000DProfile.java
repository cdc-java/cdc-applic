package cdc.applic.s1000d;

import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

/**
 * A profile is used to declare that certain features are supported
 * or not supported by a target application.
 * <p>
 * When one needs to generate something that would violate a profile, a message is generated.
 *
 * @author Damien Carbonne
 */
public final class S1000DProfile {
    public static final S1000DProfile ALL = builder().supportAll().build();

    private final Set<Feature> features;
    private final Set<Hint> hints;

    private S1000DProfile(Set<Feature> features,
                          Set<Hint> hints) {
        this.features = new HashSet<>(features);
        this.hints = new HashSet<>(hints);
    }

    /**
     * Enumeration of features possibly supported or not supported by target applications.
     * <p>
     * When a feature is disabled and met, an issue is generated.
     *
     * @author Damien Carbonne
     */
    public enum Feature {
        /**
         * If enabled, Integer types are supported by the target application.
         */
        INTEGER_TYPES,

        /**
         * If enabled, Integer ranges are supported by the target application.
         */
        INTEGER_RANGES,

        /**
         * If enabled, Real types are supported by the target application.
         */
        REAL_TYPES,

        /**
         * If enabled, Real ranges are supported by the target application.
         */
        REAL_RANGES,

        /**
         * If enabled, Boolean types are supported by the target application.
         */
        BOOLEAN_TYPES,

        /**
         * If enabled, Boolean ranges are supported by the target application.
         * <p>
         * At the moment this can not happen as boolean ranges are not supported by this library.
         */
        BOOLEAN_RANGES,

        /**
         * If enabled, String types enumerations are supported by the target application.
         */
        STRING_TYPES_ENUMS,

        /**
         * If enabled, String types patterns are supported by the target application.
         */
        STRING_TYPES_PATTERNS,

        /**
         * If enabled, String types ranges are supported by the target application.
         * <p>
         * At the moment this can not happen as string ranges are not supported by this library.
         */
        STRING_RANGES,

        /**
         * If enabled, condition, dependencies are supported by the target application.
         */
        CONDITION_DEPENDENCIES
    }

    public enum Hint {
        /**
         * If enabled, condition dependencies are not generated.
         * <p>
         * Condition dependencies may be very long to generate.
         */
        NO_CONDITION_DEPENDENCIES

        // TODO standard S1000D condition dependencies vs current one
    }

    public boolean isSupported(Feature feature) {
        return features.contains(feature);
    }

    public boolean isEnabled(Hint hint) {
        return hints.contains(hint);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private final Set<Feature> features = EnumSet.noneOf(Feature.class);
        private final Set<Hint> hints = EnumSet.noneOf(Hint.class);

        private Builder() {
        }

        public Builder support(Feature feature) {
            this.features.add(feature);
            return this;
        }

        public Builder support(Feature feature,
                               boolean enabled) {
            if (enabled) {
                this.features.add(feature);
            } else {
                this.features.remove(feature);
            }
            return this;
        }

        public Builder supportAll() {
            Collections.addAll(features, Feature.values());
            return this;
        }

        public Builder enable(Hint hint) {
            this.hints.add(hint);
            return this;
        }

        public S1000DProfile build() {
            return new S1000DProfile(features,
                                     hints);
        }
    }
}