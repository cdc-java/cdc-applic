package cdc.applic.s1000d;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.util.debug.ControlledPrintable;
import cdc.util.debug.Verbosity;
import cdc.util.lang.Checks;

/**
 * PCT definition, usable to accumulate events generated by {@link S1000DPctHandler}.
 *
 * @author Damien Carbonne
 */
public class S1000DPct implements ControlledPrintable {
    private final NamingConvention namingConvention;
    private final List<Product> products;

    protected S1000DPct(NamingConvention convention,
                        List<Product> product) {
        this.namingConvention = Checks.isNotNull(convention, "convention");
        this.products = Collections.unmodifiableList(new ArrayList<>(product));
    }

    public static class Product implements ControlledPrintable {
        private final NamingConvention namingConvention;
        private final List<ProductProperty> properties;

        protected Product(NamingConvention convention,
                          List<ProductProperty> properties) {
            this.namingConvention = convention;
            this.properties = Collections.unmodifiableList(new ArrayList<>(properties));
        }

        public NamingConvention getNamingConvention() {
            return namingConvention;
        }

        public List<ProductProperty> getProperties() {
            return properties;
        }

        public int getNumberOfProperties(S1000DPropertyType type) {
            return (int) getProperties().stream()
                                        .filter(p -> p.getProperty().getS1000DPropertyType() == type)
                                        .count();
        }

        /**
         * Returns the product property that matches a property id.
         *
         * @param propertyId The property id of the searched product property.
         * @return The product property that matches {@code propertyId} or {@code null}.
         */
        public ProductProperty getMatchingPropertyIdOrNull(String propertyId) {
            for (final S1000DPct.ProductProperty property : getProperties()) {
                if (property.getProperty().getS1000DId().equals(propertyId)) {
                    return property;
                }
            }
            return null;
        }

        /**
         * Returns a list of product properties that match a type id.
         *
         * @param typeId The type id of searched product properties.
         * @return A list of product properties that match {@code typeId}.
         */
        public List<S1000DPct.ProductProperty> getMatchingTypeIds(String typeId) {
            final List<S1000DPct.ProductProperty> list = new ArrayList<>();
            for (final S1000DPct.ProductProperty property : getProperties()) {
                if (property.getProperty().getType().getS1000DId().equals(typeId)) {
                    list.add(property);
                }
            }
            return list;
        }

        @Override
        public int hashCode() {
            return Objects.hash(properties);
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof Product)) {
                return false;
            }
            final Product other = (Product) object;
            return Objects.equals(properties, other.properties);
        }

        @Override
        public String toString() {
            return "[" + getProperties()
                    + "]";
        }

        @Override
        public void print(PrintStream out,
                          int level,
                          Verbosity verbosity) {
            indent(out, level);
            out.println("Properties (" + getProperties().size() + ")");
            indent(out, level + 1);
            out.println("Product Attributes (" + getNumberOfProperties(S1000DPropertyType.PRODUCT_ATTRIBUTE) + ")");
            indent(out, level + 1);
            out.println("Product Conditions (" + getNumberOfProperties(S1000DPropertyType.PRODUCT_CONDITION) + ")");

            if (verbosity != Verbosity.ESSENTIAL) {
                for (final ProductProperty property : getProperties()) {
                    property.print(out, level + 1, verbosity);
                }
            }
        }
    }

    public static class ProductProperty implements ControlledPrintable {
        private final S1000DProperty property;
        private final S1000DValues values;

        protected ProductProperty(S1000DProperty property,
                                  S1000DValues values) {
            this.property = property;
            this.values = values;
        }

        public S1000DProperty getProperty() {
            return property;
        }

        public S1000DValues getValues() {
            return values;
        }

        @Override
        public int hashCode() {
            return Objects.hash(property,
                                values);
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof ProductProperty)) {
                return false;
            }
            final ProductProperty other = (ProductProperty) object;
            return Objects.equals(property, other.property)
                    && Objects.equals(values, other.values);
        }

        @Override
        public String toString() {
            return "[" + getProperty().getS1000DPropertyType()
                    + " " + getProperty().getName()
                    + " " + getValues()
                    + "]";
        }

        @Override
        public void print(PrintStream out,
                          int level,
                          Verbosity verbosity) {
            indent(out, level);
            out.println(this);
        }
    }

    public NamingConvention getNamingfConvention() {
        return namingConvention;
    }

    public List<Product> getProducts() {
        return products;
    }

    @Override
    public void print(PrintStream out,
                      int level,
                      Verbosity verbosity) {
        indent(out, level);
        out.println("Products (" + getProducts().size() + ")");
        if (verbosity != Verbosity.ESSENTIAL) {
            int index = 0;
            for (final S1000DPct.Product product : getProducts()) {
                index++;
                indent(out, level + 1);
                out.println("Product #" + index);
                product.print(out, level + 2, verbosity);
            }
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private NamingConvention namingConvention = NamingConvention.DEFAULT;
        private final List<Product> products = new ArrayList<>();

        protected Builder() {
        }

        public Builder namingConvention(NamingConvention convention) {
            this.namingConvention = convention;
            return this;
        }

        public ProductBuilder addProduct() {
            return new ProductBuilder(this);
        }

        public S1000DPct build() {
            return new S1000DPct(namingConvention, products);
        }
    }

    public static class ProductBuilder {
        private final Builder builder;
        private final List<ProductProperty> properties = new ArrayList<>();

        protected ProductBuilder(Builder builder) {
            this.builder = builder;
        }

        public ProductBuilder addProductProperty(S1000DProperty property,
                                                 S1000DValues values) {
            Checks.isNotNull(property, "property");
            Checks.isNotNull(values, "values");
            Checks.isTrue(property.getS1000DPropertyType().isPctCandidate(),
                          "Property '" + property.getName() + "' not compliant with PCT: " + property.getS1000DPropertyType());

            properties.add(new ProductProperty(property, values));
            return this;
        }

        public Builder build() {
            builder.products.add(new Product(builder.namingConvention,
                                             properties));
            return builder;
        }
    }
}