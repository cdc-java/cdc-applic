package cdc.applic.s1000d;

import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.Expression;

/**
 * Interface implemented by classes that can convert an S1000D XML tree to an {@link Expression}.
 * <p>
 * <b>Note:</b> currently, conversion of logic engine are not supported.
 * <ul>
 * <li>A node that contains {@code <expression>} (logic engine) cannot be converted.
 * </ul>
 *
 * @author Damien Carbonne
 * @param <E> The element type.
 */
public interface S1000DToExpressionConverter<E> {
    /**
     * Converts an S1000D XML {@code <applic>} to an {@link Expression}.
     *
     * @param applic The {@code <applic>} node.
     * @param source The {@link XmlSource} that is used to access XML structure rooted on {@code applic}.
     * @return The corresponding expression.
     * @throws IllegalArgumentException When {@code applic} or {@code source} is {@code null}.
     * @throws ApplicException When there is a semantic issue (e.g., can not resolve a property),
     *             or when there is a conversion error.
     */
    public Expression convertToExpression(E applic,
                                          XmlSource<E> source);
}