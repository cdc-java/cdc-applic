package cdc.applic.s1000d;

import java.io.IOException;

import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.Expression;

/**
 * Interface implemented by classes that can convert an {@link Expression} to S1000D.
 *
 * @author Damien Carbonne
 */
public interface ExpressionToS1000DConverter {
    /**
     * Converts an expression to S1000D XML.
     *
     * @param expression The expression to convert.
     * @param handler The XML handler.
     * @throws IllegalArgumentException When {@code expression} or {@code handler} is {@code null}.
     * @throws ApplicException When {@code expression} contains items that can not be converted to S1000D.<br>
     *             This happens when negation, xor, inequalities, ... are used.
     * @throws IOException When an IO error occurs.
     */
    public void convertToS1000D(Expression expression,
                                XmlHandler handler) throws IOException;
}