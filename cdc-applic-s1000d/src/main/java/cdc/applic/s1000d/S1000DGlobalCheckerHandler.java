package cdc.applic.s1000d;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;

public interface S1000DGlobalCheckerHandler extends IssuesHandler<Issue> {
    public void beginGlobalChecks(DictionaryHandle handle,
                                  Expression additionalAssertion);

    public void endGlobalChecks();
}