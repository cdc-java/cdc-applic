package cdc.applic.s1000d;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.util.events.ProgressController;

/**
 * Interface of class used to generate CCT.
 * <p>
 * <b>WARNING:</b> no global checks are done. One must use {@link S1000DGlobalChecker} for that.
 *
 * @author Damien Carbonne
 */
public interface S1000DCctGenerator {
    /**
     * Generates the CCT corresponding to a Dictionary.
     *
     * @param handle The DictionaryHandle to use.
     * @param additionalAssertion The additional assertion,
     *            used to reduce the applicability of passed dictionary.
     * @param convention The naming convention that should be used for generation.
     * @param handler The handler to notify.
     * @param controller The progress controller.
     * @param profile The profile. When something is not supported, an issue is generated.
     * @throws IllegalArgumentException When {@code handle}, {@code controller}, {@code profile},
     *             {@code additionalAssertion} or {@code handler} is {@code null}.
     */
    public void generateCct(DictionaryHandle handle,
                            Expression additionalAssertion,
                            NamingConvention convention,
                            S1000DCctHandler handler,
                            ProgressController controller,
                            S1000DProfile profile);

    public default void generateCct(DictionaryHandle handle,
                                    Expression additionalAssertion,
                                    NamingConvention convention,
                                    S1000DCctHandler handler,
                                    ProgressController controller) {
        generateCct(handle,
                    additionalAssertion,
                    convention,
                    handler,
                    controller,
                    S1000DProfile.ALL);
    }
}