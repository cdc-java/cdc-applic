package cdc.applic.s1000d;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.content.Value;

/**
 * Interface used to handle events related to PCT generation.
 * <p>
 * Computation of PCT depends on results of ACT and CCT computations.
 *
 * @author Damien Carbonne
 */
public interface S1000DPctHandler extends S1000DGlobalCheckerHandler, S1000DActHandler, S1000DCctHandler {
    /**
     * Invoked when an PCT computation starts.
     *
     * @param handle The DictionaryHandle for which the PCT is computed.
     * @param additionalAssertion The additional assertion,
     *            used to reduce the applicability of passed dictionary.
     * @param convention The naming convention to use for generation.
     * @param masterProperty The master property, that should be a discrete primary product attribute.
     */
    public void beginPct(DictionaryHandle handle,
                         Expression additionalAssertion,
                         NamingConvention convention,
                         S1000DProperty masterProperty);

    /**
     * Invoked when generation of data related to a product starts.
     *
     * @param masterValue The value of {@code masterProperty} that characterizes the product.
     * @param convention The naming convention to use for generation.
     */
    public void beginPctProduct(Value masterValue,
                                NamingConvention convention);

    /**
     * Invoked to characterize a property of current product.
     *
     * @param property The property to add.
     * @param values The possible values of {@code property} when {@code masterProperty}
     *            is equal to {@code masterValue}.
     * @param convention The naming convention to use for generation.
     */
    public void addPctProductProperty(S1000DProperty property,
                                      S1000DValues values,
                                      NamingConvention convention);

    /**
     * Invoked when generation of data related to a product ends.
     */
    public void endPctProduct();

    /**
     * Invoked when an PCT computation ends.
     */
    public void endPct();
}