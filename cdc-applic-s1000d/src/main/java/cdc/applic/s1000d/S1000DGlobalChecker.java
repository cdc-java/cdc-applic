package cdc.applic.s1000d;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.util.events.ProgressController;

/**
 * Interface of classes used to globally check S1000D data.
 *
 * @author Damien Carbonne
 */
public interface S1000DGlobalChecker {
    public void check(DictionaryHandle handle,
                      Expression additionalAssertion,
                      NamingConvention convention,
                      S1000DGlobalCheckerHandler handler,
                      ProgressController controller,
                      S1000DProfile profile);
}