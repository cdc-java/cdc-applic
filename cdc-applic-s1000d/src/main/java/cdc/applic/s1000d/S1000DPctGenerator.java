package cdc.applic.s1000d;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.expressions.Expression;
import cdc.util.events.ProgressController;

/**
 * Interface of class used to generate PCT.
 * <p>
 * <b>Note:</b>When generating a PCT, ACT and CCT are also generated.
 *
 * @author Damien Carbonne
 */
public interface S1000DPctGenerator {
    /**
     * Generates the PCT corresponding to a Dictionary.
     *
     * @param handle The DictionaryHandle to use.
     * @param additionalAssertion The additional assertion,
     *            used to reduce the applicability of passed dictionary.
     * @param convention The naming convention that should be used for generation.
     * @param masterProperty The master property, that should be a discrete primary product attribute.
     * @param handler The handler to notify.
     * @param controller The progress controller.
     * @param profile The profile. When something is not supported, an issue is generated.
     * @throws IllegalArgumentException When {@code handle}, {@code controller}, {@code profile},
     *             {@code additionalAssertion}, {@code masterProperty} or {@code handler} is {@code null},<br>
     *             or when {@code masterProperty} is not a PRIMARY or SECONDARY PRODUCT_ATTRIBUTE,<br>
     *             or when type of {@code masterProperty} is not supported.
     */
    public void generatePct(DictionaryHandle handle,
                            Expression additionalAssertion,
                            NamingConvention convention,
                            S1000DProperty masterProperty,
                            S1000DPctHandler handler,
                            ProgressController controller,
                            S1000DProfile profile);

    public default void generatePct(DictionaryHandle handle,
                                    Expression additionalAssertion,
                                    NamingConvention convention,
                                    S1000DProperty masterProperty,
                                    S1000DPctHandler handler,
                                    ProgressController controller) {
        generatePct(handle,
                    additionalAssertion,
                    convention,
                    masterProperty,
                    handler,
                    controller,
                    S1000DProfile.ALL);
    }
}