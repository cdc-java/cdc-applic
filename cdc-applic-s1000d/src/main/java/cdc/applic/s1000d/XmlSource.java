package cdc.applic.s1000d;

/**
 * Interface abstracting the reading of an XML tree.
 * <p>
 * It only contains the necessary features for S1000D processing.
 *
 * @author Damien Carbonne
 *
 * @param <E> The Element type.
 */
public interface XmlSource<E> {
    /**
     * @param parent The parent element.
     * @return The children elements of {@code parent}.<br>
     *         All child nodes that are not elements are ignored.
     */
    public Iterable<? extends E> getChildren(E parent);

    /**
     * @param element The element.
     * @return The name of {@code element}.
     */
    public String getName(E element);

    /**
     * @param element The element.
     * @param name The attribute name.
     * @param def The value to return if {@code element} has no attribute named{@code name}.
     * @return The value of attribute named {@code name}, or {@code def} if no such attribute exists.
     */
    public String getAttributeValue(E element,
                                    String name,
                                    String def);

    public default boolean hasAttribute(E element,
                                        String name) {
        return getAttributeValue(element, name, null) != null;
    }

    /**
     * @param element The element.
     * @return The text content of {@code element} or {@code null}.
     */
    public String getElementContent(E element);

    public default boolean hasElementContent(E element) {
        return getElementContent(element) != null;
    }
}