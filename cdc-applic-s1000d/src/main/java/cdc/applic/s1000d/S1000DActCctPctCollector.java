package cdc.applic.s1000d;

import java.io.PrintStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.dictionaries.s1000d.S1000DType;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.Value;
import cdc.issues.Issue;
import cdc.issues.IssuesCollector;
import cdc.util.debug.ControlledPrintable;
import cdc.util.debug.Verbosity;

public class S1000DActCctPctCollector implements S1000DPctHandler, ControlledPrintable {
    private static final Logger LOGGER = LogManager.getLogger(S1000DActCctPctCollector.class);
    private final S1000DAct.Builder actBuilder = S1000DAct.builder();
    private S1000DAct act;
    private final S1000DCct.Builder cctBuilder = S1000DCct.builder();
    private S1000DCct cct;
    private final S1000DPct.Builder pctBuilder = S1000DPct.builder();
    private S1000DPct pct;
    private S1000DPct.ProductBuilder productBuilder;

    private final IssuesCollector<Issue> issuesCollector = new IssuesCollector<>();

    public IssuesCollector<Issue> getIssuesCollector() {
        return issuesCollector;
    }

    public S1000DAct getAct() {
        return act;
    }

    public S1000DCct getCct() {
        return cct;
    }

    public S1000DPct getPct() {
        return pct;
    }

    @Override
    public void beginGlobalChecks(DictionaryHandle handle,
                                  Expression additionalAssertion) {
        LOGGER.debug("beginGlobalChecks(dictionary: {}, additionlAssertion: {})",
                     handle.getDictionary().getName(),
                     additionalAssertion);
    }

    @Override
    public void endGlobalChecks() {
        LOGGER.debug("endGlobalChecks()");
    }

    @Override
    public void issue(Issue issue) {
        LOGGER.info("issue({})", issue);
        issuesCollector.issue(issue);
    }

    @Override
    public void beginAct(DictionaryHandle handle,
                         Expression additionalAssertion,
                         NamingConvention convention) {
        LOGGER.debug("beginAct(dictionary: {}, additionalAssertion: {}, convention: {})",
                     handle.getDictionary().getName(),
                     additionalAssertion,
                     convention);
    }

    @Override
    public void addActProductAttribute(S1000DProperty property,
                                       String pattern,
                                       SItemSet domain,
                                       NamingConvention convention) {
        LOGGER.debug("   addActProductAttribute(name: {}, pattern: {}, domain: {}, convention: {})",
                     property.getName(),
                     pattern,
                     domain,
                     convention);
        actBuilder.addProductAttribute(property, pattern, domain);
    }

    @Override
    public void endAct() {
        LOGGER.debug("endAct()");
        act = actBuilder.build();
        for (final S1000DAct.ProductAttribute attribute : act.getProductAttributes()) {
            LOGGER.debug("   product attribute: {}", attribute);
        }
    }

    @Override
    public void beginCct(DictionaryHandle handle,
                         Expression additionalAssertion,
                         NamingConvention convention) {
        LOGGER.debug("beginCct(dictionary: {}, additionalAssertion: {}, convention: {})",
                     handle.getDictionary().getName(),
                     additionalAssertion,
                     convention);
    }

    @Override
    public void beginCctConditionTypes() {
        LOGGER.debug("   beginCctConditionTypes()");
    }

    @Override
    public void addCctConditionType(S1000DType type,
                                    String pattern,
                                    SItemSet domain,
                                    NamingConvention convention) {
        LOGGER.debug("      addCctConditionType(name: {}, pattern: {}, domain: {}, convention: {})",
                     type.getName(),
                     pattern,
                     domain,
                     convention);
        cctBuilder.addConditionType(type, pattern, domain);
    }

    @Override
    public void endCctConditionTypes() {
        LOGGER.debug("   endCctConditionTypes()");
    }

    @Override
    public void beginCctConditions() {
        LOGGER.debug("   beginCctConditions()");
    }

    @Override
    public void beginCctCondition(S1000DProperty property,
                                  SItemSet domain,
                                  NamingConvention convention) {
        LOGGER.debug("      beginCctCondition(name: {}, domain: {}, convention: {})",
                     property.getName(),
                     domain,
                     convention);
        cctBuilder.addCondition(property, domain);
    }

    @Override
    public void addCctConditionDependency(S1000DProperty condition,
                                          SItemSet conditionValues,
                                          S1000DProperty dependency,
                                          SItemSet dependencyValues,
                                          NamingConvention convention) {
        LOGGER.debug("         addCctConditionDependency(condition: {} in {}, depends on {} in {}, convention: {})",
                     condition.getName(),
                     conditionValues,
                     dependency,
                     dependencyValues,
                     convention);
        cctBuilder.addConditionDependency(condition,
                                          conditionValues,
                                          dependency,
                                          dependencyValues);
    }

    @Override
    public void endCctCondition() {
        LOGGER.debug("      endCctCondition()");
    }

    @Override
    public void endCctConditions() {
        LOGGER.debug("   endCctConditions()");
    }

    @Override
    public void endCct() {
        LOGGER.debug("endCct()");
        cct = cctBuilder.build();
        for (final S1000DCct.ConditionType type : cct.getConditionTypes()) {
            LOGGER.debug("   condition type: {}", type);
        }
        for (final S1000DCct.Condition condition : cct.getConditions()) {
            LOGGER.debug("   condition: {}", condition);
        }
    }

    @Override
    public void beginPct(DictionaryHandle handle,
                         Expression additionalExpression,
                         NamingConvention convention,
                         S1000DProperty masterProperty) {
        LOGGER.debug("beginPct(dictionary: {}, additionalAssertion: {}, convention: {}, masterProperty: {})",
                     handle.getDictionary().getName(),
                     additionalExpression,
                     convention,
                     masterProperty.getName());
    }

    @Override
    public void beginPctProduct(Value masterValue,
                                NamingConvention convention) {
        LOGGER.debug("   beginPctProduct(masterValue: {}, convention: {})",
                     masterValue,
                     convention);
        productBuilder = pctBuilder.addProduct();
    }

    @Override
    public void addPctProductProperty(S1000DProperty property,
                                      S1000DValues values,
                                      NamingConvention convention) {
        LOGGER.debug("      addPctProductProperty(property: {}, values: {}, convention: {})",
                     property.getName(),
                     values,
                     convention);
        productBuilder = productBuilder.addProductProperty(property, values);
    }

    @Override
    public void endPctProduct() {
        LOGGER.debug("   endPctProduct()");
        productBuilder.build();
    }

    @Override
    public void endPct() {
        LOGGER.debug("endPct()");
        pct = pctBuilder.build();
        for (final S1000DPct.Product product : pct.getProducts()) {
            LOGGER.debug("   product: {}", product);
        }
    }

    @Override
    public void print(PrintStream out,
                      int level,
                      Verbosity verbosity) {
        indent(out, level);
        out.println("Issues (" + getIssuesCollector().getIssues().size() + ")");
        if (verbosity != Verbosity.ESSENTIAL) {
            for (final Issue issue : getIssuesCollector().getIssues()) {
                indent(out, level + 1);
                out.println(issue);

            }
        }

        if (act != null) {
            indent(out, level);
            out.println("ACT");
            act.print(out, level + 1, verbosity);
        }

        if (cct != null) {
            indent(out, level);
            out.println("CCT");
            cct.print(out, level + 1, verbosity);
        }

        if (pct != null) {
            indent(out, level);
            out.println("PCT");
            pct.print(out, level + 1, verbosity);
        }
    }
}