package cdc.applic.s1000d;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.s1000d.S1000DProductIdentifier;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;

class S1000DKindTest {
    @Test
    void testIsCompliantWith() {
        assertTrue(S1000DProductIdentifier.NOT_APPLICABLE.isCompliantWith(S1000DPropertyType.PRODUCT_CONDITION));
        assertFalse(S1000DProductIdentifier.NONE.isCompliantWith(S1000DPropertyType.PRODUCT_CONDITION));
        assertFalse(S1000DProductIdentifier.PRIMARY.isCompliantWith(S1000DPropertyType.PRODUCT_CONDITION));
        assertFalse(S1000DProductIdentifier.SECONDARY.isCompliantWith(S1000DPropertyType.PRODUCT_CONDITION));

        assertTrue(S1000DProductIdentifier.NOT_APPLICABLE.isCompliantWith(S1000DPropertyType.EXTERNAL_CONDITION));
        assertFalse(S1000DProductIdentifier.NONE.isCompliantWith(S1000DPropertyType.EXTERNAL_CONDITION));
        assertFalse(S1000DProductIdentifier.PRIMARY.isCompliantWith(S1000DPropertyType.EXTERNAL_CONDITION));
        assertFalse(S1000DProductIdentifier.SECONDARY.isCompliantWith(S1000DPropertyType.EXTERNAL_CONDITION));

        assertFalse(S1000DProductIdentifier.NOT_APPLICABLE.isCompliantWith(S1000DPropertyType.PRODUCT_ATTRIBUTE));
        assertTrue(S1000DProductIdentifier.NONE.isCompliantWith(S1000DPropertyType.PRODUCT_ATTRIBUTE));
        assertTrue(S1000DProductIdentifier.PRIMARY.isCompliantWith(S1000DPropertyType.PRODUCT_ATTRIBUTE));
        assertTrue(S1000DProductIdentifier.SECONDARY.isCompliantWith(S1000DPropertyType.PRODUCT_ATTRIBUTE));

        assertTrue(S1000DProductIdentifier.NOT_APPLICABLE.isCompliantWith(S1000DPropertyType.UNDEFINED));
        assertFalse(S1000DProductIdentifier.NONE.isCompliantWith(S1000DPropertyType.UNDEFINED));
        assertFalse(S1000DProductIdentifier.PRIMARY.isCompliantWith(S1000DPropertyType.UNDEFINED));
        assertFalse(S1000DProductIdentifier.SECONDARY.isCompliantWith(S1000DPropertyType.UNDEFINED));
    }
}