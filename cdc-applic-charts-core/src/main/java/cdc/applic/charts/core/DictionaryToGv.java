package cdc.applic.charts.core;

import java.io.File;
import java.io.IOException;
import java.util.EnumSet;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.SystemUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.Registry;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.io.RepositoryXml;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.Spacing;
import cdc.applic.expressions.SymbolType;
import cdc.gv.tools.GvEngine;
import cdc.gv.tools.GvFormat;
import cdc.gv.tools.GvToAny;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.files.SearchPath;
import cdc.util.lang.FailureReaction;

/**
 * Utility used to create GV graph of policies.
 * <p>
 * GV files can be converted to images if asked (e.g., using {@link GvToAny}).
 *
 * @author Damien Carbonne
 */
public final class DictionaryToGv {
    static final Logger LOGGER = LogManager.getLogger(DictionaryToGv.class);
    private final ApiMainArgs xmargs;
    static final String DEFAULT_FONT = "Arial";
    static final String DEFAULT_MATH_FONT = SystemUtils.IS_OS_WINDOWS ? "Cambria Math" : DEFAULT_FONT;

    public static class BaseArgs {
        /** Name of the directory where files are generated. */
        public File outputDir;
        /** Paths where GV layout engines can be found. */
        public SearchPath paths = new SearchPath();
        /** Engine to use for layout. */
        public GvEngine engine;
        /** Output formats to use. */
        public final Set<GvFormat> formats = EnumSet.noneOf(GvFormat.class);
        /** Enabling of features. */
        public final FeatureMask<Feature> features = new FeatureMask<>();
        public String fontName = DEFAULT_FONT;
        public String mathFontName = DEFAULT_MATH_FONT;

        /**
         * Enumeration of possible boolean options.
         */
        public enum Feature implements OptionEnum {
            DIRECTED("directed", "Create a directed graph."),
            UNDIRECTED("undirected", "Create an undirected graph. (default)"),
            SHORT("short-symbols", "Use short symbols for expressions."),
            LONG("long-symbols", "Use long symbols for expressions."),
            MATH("math-symbols", "Use math symbols for expressions. (default)"),
            NARROW("narrow-spacing", "Use narrow spacing for expressions. (default)"),
            WIDE("wide-spacing", "Use wide spacing for expressions."),
            ALL_POLICIES("all-policies", "If enabled, generate graphs for all policies."),
            SHOW_TYPES("show-types", "If enabled, types are displayed."),
            SHOW_DESCRIPTIONS("show-descritions", "If enabled, descriptions are displayed."),
            SHOW_ISSUES("show-issues", "If enabled, issues are displayed."),
            HORIZONTAL("horizontal", "If enabled and dot engine is used, horizontal layout is used."),
            GROUP("group",
                  "If enabled and dot engine is used, nodes are grouped by categories (types, properties, aliases and assertions).");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }

        public SymbolType getSymbolType() {
            if (features.isEnabled(BaseArgs.Feature.LONG)) {
                return SymbolType.LONG;
            } else if (features.isEnabled(BaseArgs.Feature.SHORT)) {
                return SymbolType.SHORT;
            } else {
                return SymbolType.MATH;
            }
        }

        public Spacing getSpacing() {
            if (features.isEnabled(BaseArgs.Feature.WIDE)) {
                return Spacing.WIDE;
            } else {
                return Spacing.NARROW;
            }
        }

        public Formatting getFormatting() {
            return Formatting.builder()
                             .symbolType(getSymbolType())
                             .spacing(getSpacing())
                             .build();
        }
    }

    /**
     * Main arguments used with CLI.
     *
     * @author Damien Carbonne
     */
    public static class MainArgs extends BaseArgs {
        /** Name of the XML repository file. */
        public File repositoryFile;
        /** Optional path of the dictionary. */
        public String dictionaryPath;
    }

    /**
     * Main arguments used with API.
     *
     * @author Damien Carbonne
     */
    public static class ApiMainArgs extends BaseArgs {
        public ApiMainArgs() {
            super();
        }

        public ApiMainArgs(MainArgs margs) throws IOException {
            final RepositoryXml.StAXLoader loader = new RepositoryXml.StAXLoader(FailureReaction.WARN);
            final RepositoryImpl repository = loader.load(margs.repositoryFile);
            if (margs.dictionaryPath == null) {
                this.dictionary = repository.getRegistries().get(0);
            } else {
                this.dictionary = repository.getDictionary(margs.dictionaryPath, Dictionary.class);
            }
            this.outputDir = margs.outputDir;
            this.paths = margs.paths;
            this.engine = margs.engine;
            this.formats.addAll(margs.formats);
            this.features.addAll(margs.features);
            this.fontName = margs.fontName;
            this.mathFontName = margs.mathFontName;
        }

        public Dictionary dictionary;
    }

    private DictionaryToGv(ApiMainArgs xmargs) {
        this.xmargs = xmargs;
    }

    private void execute() throws IOException {
        if (xmargs.features.isEnabled(BaseArgs.Feature.ALL_POLICIES)) {
            final Registry registry = xmargs.dictionary.getRegistry();
            for (final Dictionary dictionary : registry.getSortedDescendants(true)) {
                final OneDictionaryToGv generator = new OneDictionaryToGv(xmargs, dictionary);
                generator.generateGv();
                generator.generateImages();
            }
        } else {
            final OneDictionaryToGv generator = new OneDictionaryToGv(xmargs, xmargs.dictionary);
            generator.generateGv();
            generator.generateImages();
        }
    }

    public static void generate(ApiMainArgs xmargs) throws IOException {
        new DictionaryToGv(xmargs).execute();
    }

    public static void execute(MainArgs margs) throws IOException {
        final ApiMainArgs xmargs = new ApiMainArgs(margs);
        generate(xmargs);
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String REPOSITORY_FILE = "repository";
        private static final String DICTIONARY_PATH = "dictionary";
        private static final String FONT_NAME = "font";
        private static final String MATH_FONT_NAME = "math-font";

        public MainSupport() {
            super(DictionaryToGv.class, LOGGER);
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(REPOSITORY_FILE)
                                    .desc("Name of the mandatory input XML repository file.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(DICTIONARY_PATH)
                                    .desc("Path of the optional dictionary to use. If none is passed, uses the first registry.\n"
                                            + "It has the form '/registryName[/policyName]*'.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(OUTPUT_DIR)
                                    .desc("Name of mandatory output directory where files are generated.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(PATH)
                                    .desc("Optional directory(ies) where the Graphviz binaries can be found.")
                                    .hasArgs()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(FONT_NAME)
                                    .desc("Optional name of the font to use for standard text. (default: " + DEFAULT_FONT + ").")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(MATH_FONT_NAME)
                                    .desc("Optional name of the font to use for expressions. (default: " + DEFAULT_MATH_FONT + ").")
                                    .hasArg()
                                    .build());
            addGroupedNoArgOptions(options, GvEngine.class, false);
            addNoArgOptions(options, GvFormat.class);
            addNoArgOptions(options, BaseArgs.Feature.class);
            createGroup(options, false, BaseArgs.Feature.DIRECTED, BaseArgs.Feature.UNDIRECTED);
            createGroup(options, false, BaseArgs.Feature.SHORT, BaseArgs.Feature.LONG, BaseArgs.Feature.MATH);
            createGroup(options, false, BaseArgs.Feature.NARROW, BaseArgs.Feature.WIDE);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            margs.repositoryFile = getValueAsFile(cl, REPOSITORY_FILE, IS_FILE);
            margs.dictionaryPath = getValueAsString(cl, DICTIONARY_PATH, null);
            margs.outputDir = getValueAsFile(cl, OUTPUT_DIR, IS_DIRECTORY);
            if (cl.hasOption(PATH)) {
                margs.paths = new SearchPath(cl.getOptionValues(PATH));
            }
            for (final GvEngine engine : GvEngine.values()) {
                if (cl.hasOption(engine.getName())) {
                    margs.engine = engine;
                }
            }
            for (final GvFormat format : GvFormat.values()) {
                if (cl.hasOption(format.getName())) {
                    margs.formats.add(format);
                }
            }
            if (!margs.formats.isEmpty() && margs.engine == null) {
                margs.engine = GvEngine.SFDP;
            }
            margs.fontName = cl.getOptionValue(FONT_NAME, DEFAULT_FONT);
            margs.mathFontName = cl.getOptionValue(MATH_FONT_NAME, DEFAULT_MATH_FONT);
            setMask(cl, BaseArgs.Feature.class, margs.features::setEnabled);
            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws IOException {
            DictionaryToGv.execute(margs);
            return null;
        }
    }
}