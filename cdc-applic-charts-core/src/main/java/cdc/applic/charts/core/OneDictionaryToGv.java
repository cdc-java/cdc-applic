package cdc.applic.charts.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.Described;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.RefSyn;
import cdc.applic.dictionaries.Registry;
import cdc.applic.dictionaries.checks.DictionaryIssue;
import cdc.applic.dictionaries.checks.EmptyDictionaryIssue;
import cdc.applic.dictionaries.checks.InvalidTypeDictionaryIssue;
import cdc.applic.dictionaries.core.checks.DictionaryChecker;
import cdc.applic.dictionaries.core.visitors.CollectNamedDItemNames;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.Assertion;
import cdc.applic.dictionaries.items.DItem;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.items.UserDefinedAssertion;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.dictionaries.visitors.AddMissingPrefixes;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.Named;
import cdc.applic.expressions.literals.SName;
import cdc.gv.GvWriter;
import cdc.gv.atts.GvArrowType;
import cdc.gv.atts.GvClusterAttributes;
import cdc.gv.atts.GvDirType;
import cdc.gv.atts.GvEdgeAttributes;
import cdc.gv.atts.GvEdgeStyle;
import cdc.gv.atts.GvFixedSize;
import cdc.gv.atts.GvGraphAttributes;
import cdc.gv.atts.GvLabelLoc;
import cdc.gv.atts.GvNodeAttributes;
import cdc.gv.atts.GvNodeShape;
import cdc.gv.atts.GvNodeStyle;
import cdc.gv.atts.GvRankDir;
import cdc.gv.atts.GvRankType;
import cdc.gv.atts.GvSubgraphAttributes;
import cdc.gv.colors.GvColor;
import cdc.gv.colors.GvX11Colors;
import cdc.gv.tools.GvToAny;
import cdc.util.files.Files;
import cdc.util.strings.StringUtils;

/**
 * Class used to generate files associated to one dictionary.
 *
 * @author Damien Carbonne
 */
class OneDictionaryToGv {
    private final DictionaryToGv.ApiMainArgs xmargs;
    private final Dictionary dictionary;
    private final Set<Assertion> declaredAssertions = new HashSet<>();
    /** Issues found in the dictionary. */
    private final List<DictionaryIssue> issues = new ArrayList<>();
    /** Objects (DItems, Types) that are impacted by issues. */
    private final Set<Object> impliedObjects = new HashSet<>();
    private final boolean emptyDictionary;

    private static final int MAX_LINE_LENGTH = 50;

    private static final double SIZE = 0.12;
    private static final GvColor FONT_DARK = GvX11Colors.BLACK;
    private static final GvColor FONT_MEDIUM = GvX11Colors.GREY;

    private static final GvColor TYPE_MEDIUM = new GvColor(222, 151, 255);
    private static final GvColor TYPE_DARK = new GvColor(195, 67, 255);

    private static final GvColor PROPERTY_LIGHT = new GvColor(253, 243, 202);
    private static final GvColor PROPERTY_MEDIUM = new GvColor(253, 222, 101);
    private static final GvColor PROPERTY_DARK = new GvColor(255, 206, 0);

    private static final GvColor ALIAS_LIGHT = new GvColor(223, 234, 187);
    private static final GvColor ALIAS_MEDIUM = new GvColor(202, 234, 94);
    private static final GvColor ALIAS_DARK = new GvColor(181, 234, 0);

    private static final GvColor ASSERTION_LIGHT = new GvColor(230, 233, 255);
    private static final GvColor ASSERTION_MEDIUM = new GvColor(153, 165, 255);
    private static final GvColor ASSERTION_DARK = new GvColor(77, 98, 255);

    private static final GvColor ISSUE_MEDIUM = new GvColor(254, 152, 152);
    private static final GvColor ISSUE_DARK = new GvColor(255, 77, 77);

    public OneDictionaryToGv(DictionaryToGv.ApiMainArgs xmargs,
                             Dictionary dictionary) {
        this.xmargs = xmargs;
        this.dictionary = dictionary;

        // Collect declared assertions
        for (final UserDefinedAssertion assertion : dictionary.getAssertions(UserDefinedAssertion.class)) {
            declaredAssertions.add(assertion);
        }

        // Check dictionary
        final DictionaryChecker checker = new DictionaryChecker(dictionary);
        checker.check(issues);

        boolean empty = false;
        for (final DictionaryIssue issue : issues) {
            for (final DItem item : issue.getImpliedItems()) {
                impliedObjects.add(item);
            }
            if (issue instanceof EmptyDictionaryIssue) {
                empty = true;
            } else if (issue instanceof InvalidTypeDictionaryIssue) {
                impliedObjects.add(((InvalidTypeDictionaryIssue) issue).getIssueType());
            }
        }
        this.emptyDictionary = empty;
    }

    /**
     *
     * @param object The object.
     * @return {@code true} if {@code object} has an issue.
     */
    private boolean hasIssue(Object object) {
        return impliedObjects.contains(object);
    }

    /**
     * @param assertion The assertion.
     * @return {@code true} if {@code assertion} is declared.
     */
    private boolean isDeclared(Assertion assertion) {
        return declaredAssertions.contains(assertion);
    }

    /**
     * @param item The NamedDItem.
     * @return The usage of {@code item} in dictionary.
     */
    private DItemUsage getUsage(NamedDItem item) {
        return dictionary.getEffectiveItemUsage(item);
    }

    /**
     * Converts a Name to a String that can be used as an Id.
     *
     * @param name The Name.
     * @return The corresponding id.
     */
    private static String toId(Name name) {
        return name.getNonEscapedLiteral();
    }

    /**
     * @param item The DItem.
     * @return The Id of {@code item}.
     */
    private static String getId(DItem item) {
        if (item instanceof NamedDItem) {
            return toId(((NamedDItem) item).getName());
        } else {
            return ((Assertion) item).getExpression().toInfix(Formatting.SHORT_NARROW);
        }
    }

    private static String getId(Type type) {
        return type.getName() + "_Type";
    }

    /**
     * @param expression The expression.
     * @return A list of all Ids of DItems used by {@code expression}.
     */
    private Set<String> collectUsedIds(Expression expression) {
        final Set<String> set = new HashSet<>();
        for (final Name name : CollectNamedDItemNames.collect(AddMissingPrefixes.execute(expression.getRootNode(), dictionary))) {
            set.add(toId(dictionary.convertItemName(name, NamingConvention.DEFAULT)));
        }
        return set;
    }

    protected String getLabel(Property property) {
        final StringBuilder builder = new StringBuilder();
        builder.append(property.getName().getNonEscapedLiteral());
        addSynonyms(builder, property.getNames());
        builder.append("\n\n ");
        addSynonymsFill(builder, property.getNames());
        return builder.toString();
    }

    protected String getLabel(Alias alias) {
        final StringBuilder builder = new StringBuilder();
        builder.append(alias.getName().getNonEscapedLiteral());
        addSynonyms(builder, alias.getNames());
        builder.append("\n\n");
        builder.append(alias.getExpression().toInfix(xmargs.getFormatting()));
        addSynonymsFill(builder, alias.getNames());
        return builder.toString();
    }

    protected String getLabel(Assertion assertion) {
        final StringBuilder builder = new StringBuilder();
        builder.append("\n\n");
        builder.append(assertion.getExpression().toInfix(xmargs.getFormatting()));
        return builder.toString();
    }

    /**
     * @param item The DItem.
     * @return The attributes to be used for node representing {@code item}.
     */
    protected GvNodeAttributes getNodeAttributes(DItem item) {
        final GvNodeAttributes atts = new GvNodeAttributes();

        atts.setStyle(GvNodeStyle.FILLED)
            .setFixedSize(GvFixedSize.SHAPE)
            .setWidth(SIZE)
            .setHeight(SIZE)
            .setFontName(xmargs.mathFontName)
            .setFontSize(10);

        final boolean hasIssue = hasIssue(item);

        if (item instanceof Property) {
            final Property property = (Property) item;
            atts.setShape(GvNodeShape.CIRCLE)
                .setLabel(getLabel(property));
            if (getUsage(property) == DItemUsage.FORBIDDEN) {
                atts.setFillColor(PROPERTY_LIGHT)
                    .setColor(hasIssue ? ISSUE_MEDIUM : PROPERTY_MEDIUM)
                    .setFontColor(hasIssue ? ISSUE_MEDIUM : FONT_MEDIUM);
            } else {
                atts.setFillColor(PROPERTY_MEDIUM)
                    .setColor(hasIssue ? ISSUE_DARK : PROPERTY_DARK)
                    .setFontColor(hasIssue ? ISSUE_DARK : FONT_DARK);
            }
        } else if (item instanceof Alias) {
            final Alias alias = (Alias) item;
            atts.setShape(GvNodeShape.BOX)
                .setLabel(getLabel(alias));
            if (getUsage(alias) == DItemUsage.FORBIDDEN) {
                atts.setFillColor(ALIAS_LIGHT)
                    .setColor(hasIssue ? ISSUE_MEDIUM : ALIAS_MEDIUM)
                    .setFontColor(hasIssue ? ISSUE_MEDIUM : FONT_MEDIUM);
            } else {
                atts.setFillColor(ALIAS_MEDIUM)
                    .setColor(hasIssue ? ISSUE_DARK : ALIAS_DARK)
                    .setFontColor(hasIssue ? ISSUE_DARK : FONT_DARK);
            }
        } else { // Assertion
            final Assertion assertion = (Assertion) item;
            atts.setShape(GvNodeShape.TRIANGLE)
                .setLabel(getLabel(assertion));

            if (isDeclared((Assertion) item)) {
                atts.setFillColor(ASSERTION_MEDIUM)
                    .setColor(hasIssue ? ISSUE_DARK : ASSERTION_DARK)
                    .setFontColor(hasIssue ? ISSUE_DARK : ASSERTION_DARK);
            } else {
                atts.setFillColor(ASSERTION_LIGHT)
                    .setColor(hasIssue ? ISSUE_MEDIUM : ASSERTION_MEDIUM)
                    .setFontColor(hasIssue ? ISSUE_MEDIUM : ASSERTION_MEDIUM);
            }
        }

        return atts;
    }

    private static void addSynonyms(StringBuilder builder,
                                    RefSyn<?> names) {
        final List<NamingConvention> conventions = new ArrayList<>(names.getNonDefaultNamingConventions());
        conventions.sort(Named.NAME_COMPARATOR);
        if (!conventions.isEmpty()) {
            for (final NamingConvention convention : conventions) {
                builder.append('\n');
                builder.append(names.getValue(convention));
                builder.append(" (");
                builder.append(convention.getName());
                builder.append(')');
            }
        }
    }

    private static void addSynonymsFill(StringBuilder builder,
                                        RefSyn<?> names) {
        final Set<NamingConvention> conventions = names.getNonDefaultNamingConventions();
        for (int index = 0; index < conventions.size(); index++) {
            builder.append("\n ");
        }
    }

    protected String getLabel(Type type) {
        final String def = type.getDefinition();
        final StringBuilder builder = new StringBuilder();

        builder.append(type.getName());
        addSynonyms(builder, type.getNames());

        builder.append("\n\n");
        if (def.isEmpty()) {
            builder.append(" ");
        } else {
            builder.append(def);
        }
        addSynonymsFill(builder, type.getNames());
        return builder.toString();
    }

    protected GvNodeAttributes getNodeAttributes(Type type) {
        final GvNodeAttributes atts = new GvNodeAttributes();

        atts.setStyle(GvNodeStyle.FILLED)
            .setFixedSize(GvFixedSize.SHAPE)
            .setWidth(SIZE)
            .setHeight(SIZE)
            .setFontName(xmargs.fontName)
            .setFontSize(10)
            .setColor(TYPE_DARK)
            .setFillColor(TYPE_MEDIUM)
            .setFontColor(FONT_DARK)
            .setShape(GvNodeShape.CIRCLE)
            .setLabel(getLabel(type));

        if (hasIssue(type)) {
            atts.setColor(ISSUE_DARK)
                .setFontColor(ISSUE_DARK);
        }
        return atts;
    }

    private static GvNodeAttributes getDescriptionNodeAttributes(Locale locale,
                                                                 String content) {
        final GvNodeAttributes atts = new GvNodeAttributes();
        atts.setStyle(GvNodeStyle.FILLED)
            .setShape(GvNodeShape.BOX)
            .setColor(GvX11Colors.BLACK)
            .setPenWidth(0.01)
            .setFillColor(GvX11Colors.LIGHTYELLOW1)
            .setFontSize(6)
            .setMargin(0.01)
            .setHeight(0.1)
            .setWidth(0.1)
            .setLabel(StringUtils.wrap(content, MAX_LINE_LENGTH, true).replace("\n", "\\l") + "\\l");
        return atts;
    }

    private static String getLabel(DictionaryIssue issue) {
        final StringBuilder builder = new StringBuilder();
        builder.append(issue.getSeverity());
        builder.append("\\l");
        builder.append(StringUtils.wrap(issue.getDescription(), MAX_LINE_LENGTH, true).replace("\n", "\\l"));
        builder.append("\\l");
        return builder.toString();
    }

    protected GvNodeAttributes getNodeAttributes(DictionaryIssue issue) {
        final GvNodeAttributes atts = new GvNodeAttributes();
        atts.setStyle(GvNodeStyle.FILLED)
            .setShape(GvNodeShape.BOX)
            .setColor(GvX11Colors.RED)
            .setPenWidth(0.01)
            .setFillColor(GvX11Colors.LIGHTPINK)
            .setFontSize(6)
            .setMargin(0.01)
            .setHeight(0.1)
            .setWidth(0.1)
            .setLabel(getLabel(issue));
        return atts;
    }

    private GvGraphAttributes getGraphAttributes() {
        final GvGraphAttributes atts = new GvGraphAttributes();
        final SName prefix =
                dictionary instanceof Registry
                        ? ((Registry) dictionary).getPrefix().orElse(null)
                        : null;
        atts.setLabel(dictionary.getName() + (prefix == null ? "" : " (" + prefix.getNonEscapedLiteral() + ")"))
            .setFontName(xmargs.fontName)
            .setFontSize(20)
            .setLabelLoc(GvLabelLoc.TOP)
            .setMargin(0.2)
            .setNodeSep(0.01);
        if (emptyDictionary) {
            atts.setFontColor(ISSUE_DARK);
        }
        if (xmargs.features.isEnabled(DictionaryToGv.BaseArgs.Feature.HORIZONTAL)) {
            atts.setRankDir(GvRankDir.RL);
        }
        return atts;
    }

    private static GvSubgraphAttributes getSubGraphAttributes() {
        final GvSubgraphAttributes atts = new GvSubgraphAttributes();
        atts.setRank(GvRankType.SAME);
        return atts;
    }

    private static GvClusterAttributes getClusterAttributes() {
        final GvClusterAttributes atts = new GvClusterAttributes();
        atts.setLabel("")
            .setPenWidth(0.0);
        return atts;
    }

    private GvEdgeAttributes getAliasEdgeAttributes(Alias alias) {
        final GvEdgeAttributes atts =
                new GvEdgeAttributes().setPenWidth(0.5)
                                      .setArrowHead(GvArrowType.NORMAL)
                                      .setArrowSize(0.5)
                                      .setDir(GvDirType.FORWARD);
        if (getUsage(alias) == DItemUsage.FORBIDDEN) {
            atts.setColor(ALIAS_MEDIUM)
                .setStyle(GvEdgeStyle.DASHED);
        } else {
            atts.setColor(ALIAS_DARK);
        }
        return atts;
    }

    private static GvEdgeAttributes getDescriptionEdgeAttributes() {
        return new GvEdgeAttributes().setPenWidth(0.01)
                                     .setArrowHead(GvArrowType.NONE)
                                     .setColor(GvX11Colors.BLACK)
                                     .setStyle(GvEdgeStyle.DOTTED)
                                     .setConstraint(false);
    }

    private GvEdgeAttributes getPropertyTypeEdgeAttributes(Property property) {
        final GvEdgeAttributes atts =
                new GvEdgeAttributes().setPenWidth(0.5)
                                      .setArrowHead(GvArrowType.NONE)
                                      .setArrowSize(0.5)
                                      .setDir(GvDirType.FORWARD)
                                      .setColor(TYPE_DARK)
                                      .setStyle(GvEdgeStyle.SOLID);
        if (hasIssue(property.getType())) {
            atts.setColor(ISSUE_DARK);
        }
        return atts;
    }

    protected GvEdgeAttributes getAssertionEdgeAttributes(Assertion assertion) {
        final GvEdgeAttributes atts =
                new GvEdgeAttributes().setPenWidth(0.5)
                                      .setArrowHead(GvArrowType.NONE)
                                      .setDir(GvDirType.FORWARD);
        if (isDeclared(assertion)) {
            atts.setColor(ASSERTION_DARK);
        } else {
            atts.setColor(ASSERTION_MEDIUM)
                .setStyle(GvEdgeStyle.DASHED);
        }
        return atts;
    }

    private static GvEdgeAttributes getIssueEdgeAttributes() {
        return new GvEdgeAttributes().setPenWidth(0.01)
                                     .setArrowHead(GvArrowType.NONE)
                                     .setColor(GvX11Colors.RED)
                                     .setStyle(GvEdgeStyle.DOTTED)
                                     .setConstraint(false);
    }

    private File getGvFile() {
        return new File(xmargs.outputDir, Files.toValidBasename(dictionary.getName()) + ".gv");
    }

    public void generateGv() throws IOException {
        final File file = getGvFile();
        DictionaryToGv.LOGGER.info("Generate {}", file);
        try (final GvWriter writer = new GvWriter(file)) {
            writer.beginGraph(dictionary.getName(),
                              xmargs.features.isEnabled(DictionaryToGv.BaseArgs.Feature.DIRECTED),
                              getGraphAttributes());

            addIsues(writer);

            // Collect and sort all NamedDItems
            final List<NamedDItem> sortedItems = new ArrayList<>();
            for (final NamedDItem item : dictionary.getRegistry().getDeclaredItems()) {
                sortedItems.add(item);
            }
            Collections.sort(sortedItems, Named.NAME_COMPARATOR);

            // Collect and sort all Types
            final Set<Type> types = new HashSet<>();
            for (final NamedDItem item : sortedItems) {
                if (item instanceof Property) {
                    types.add(((Property) item).getType());
                }
            }
            final List<Type> sortedTypes = new ArrayList<>(types);
            Collections.sort(sortedTypes, Type.NAME_COMPARATOR);

            // Write all types (if necessary)
            if (xmargs.features.isEnabled(DictionaryToGv.BaseArgs.Feature.SHOW_TYPES)) {
                writer.println();
                writer.addComment("Types");
                for (final Type type : sortedTypes) {
                    addType(writer, type);
                }

                if (xmargs.features.isEnabled(DictionaryToGv.BaseArgs.Feature.GROUP)) {
                    writer.println();
                    writer.addComment("Types constraints");
                    writer.beginSubgraph(null, getSubGraphAttributes());
                    for (final Type type : sortedTypes) {
                        writer.addNode(getId(type), null);
                    }
                    writer.endSubgraph();
                }
            }

            // Write all Properties
            writer.println();
            writer.addComment("Properties");
            for (final NamedDItem item : sortedItems) {
                if (item instanceof Property) {
                    addProperty(writer, (Property) item);
                }
            }

            if (xmargs.features.isEnabled(DictionaryToGv.BaseArgs.Feature.GROUP)) {
                writer.println();
                writer.addComment("Properties constraints");
                writer.beginSubgraph(null, getSubGraphAttributes());
                for (final NamedDItem item : sortedItems) {
                    if (item instanceof Property) {
                        writer.addNode(getId(item), null);
                    }
                }
                writer.endSubgraph();
            }

            // Write all Aliases
            writer.println();
            writer.addComment("Aliases");
            if (xmargs.features.isEnabled(DictionaryToGv.BaseArgs.Feature.GROUP)) {
                writer.beginCluster("aliases", getClusterAttributes());
            }
            for (final NamedDItem item : sortedItems) {
                if (item instanceof Alias) {
                    addAlias(writer, (Alias) item);
                }
            }
            if (xmargs.features.isEnabled(DictionaryToGv.BaseArgs.Feature.GROUP)) {
                writer.endCluster();
            }

            writer.println();
            writer.addComment("Aliases edges");
            for (final NamedDItem item : sortedItems) {
                if (item instanceof Alias) {
                    addAliasEdges(writer, (Alias) item);
                }
            }

            // Collect all assertions
            final Set<Assertion> assertions = new HashSet<>();
            for (final Assertion assertion : dictionary.getAllAssertions()) {
                assertions.add(assertion);
            }
            // Sort all assertions
            final List<Assertion> sortedAssertions = new ArrayList<>(assertions);
            Collections.sort(sortedAssertions, DItem.COMPARATOR);

            writer.println();
            writer.addComment("Assertions");
            for (final Assertion assertion : sortedAssertions) {
                addAssertion(writer, assertion);
            }

            if (xmargs.features.isEnabled(DictionaryToGv.BaseArgs.Feature.GROUP)) {
                writer.println();
                writer.addComment("Assertions constraints");
                writer.beginSubgraph(null, getSubGraphAttributes());
                for (final Assertion assertion : sortedAssertions) {
                    writer.addNode(getId(assertion), null);
                }
                writer.endSubgraph();
            }

            writer.endGraph();
            writer.flush();
            DictionaryToGv.LOGGER.info("Done");
        }
    }

    public void generateImages() {
        if (!xmargs.formats.isEmpty()) {
            final GvToAny.MainArgs omargs = new GvToAny.MainArgs();
            omargs.input = getGvFile();
            omargs.engine = xmargs.engine;
            omargs.formats.addAll(xmargs.formats);
            omargs.paths = xmargs.paths;
            omargs.outputDir = xmargs.outputDir;
            omargs.setEnabled(GvToAny.MainArgs.Feature.VERBOSE, true);
            GvToAny.execute(omargs);
        }
    }

    private void addIsues(GvWriter writer) throws IOException {
        if (xmargs.features.isEnabled(DictionaryToGv.BaseArgs.Feature.SHOW_ISSUES)) {
            writer.println();
            writer.addComment("Issues");
            int index = 0;
            for (final DictionaryIssue issue : issues) {
                final String issueId = "issue_" + index;
                writer.addNode(issueId, getNodeAttributes(issue));
                for (final DItem item : issue.getImpliedItems()) {
                    writer.addEdge(issueId, getId(item), getIssueEdgeAttributes());
                }
                index++;
            }
        }
    }

    private void addDescriptions(GvWriter writer,
                                 String id,
                                 Described described) throws IOException {
        if (xmargs.features.isEnabled(DictionaryToGv.BaseArgs.Feature.SHOW_DESCRIPTIONS)) {
            for (final Locale locale : described.getDescription().getLocales()) {
                final String content = described.getDescription().getContent(locale);
                addDescription(writer, id, locale, content);
            }
        }
    }

    private static void addDescription(GvWriter writer,
                                       String id,
                                       Locale locale,
                                       String content) throws IOException {
        final String descrId = id + "_" + locale.getLanguage();
        writer.addNode(descrId, getDescriptionNodeAttributes(locale, content));
        writer.addEdge(id, descrId, getDescriptionEdgeAttributes());
        writer.beginSubgraph("sub_" + id + "_" + locale.getLanguage(), getSubGraphAttributes());
        writer.addNode(id, null);
        writer.addNode(descrId, null);
        writer.endSubgraph();
    }

    private void addType(GvWriter writer,
                         Type type) throws IOException {
        final String id = getId(type);
        writer.addNode(id, getNodeAttributes(type));
        addDescriptions(writer, id, type);
    }

    private void addAlias(GvWriter writer,
                          Alias alias) throws IOException {
        final String id = getId(alias);
        writer.addNode(id, getNodeAttributes(alias));
        addDescriptions(writer, id, alias);
    }

    private void addAliasEdges(GvWriter writer,
                               Alias alias) throws IOException {
        final String itemId = getId(alias);
        final Set<String> ids = collectUsedIds(alias.getExpression());
        for (final String id : ids) {
            writer.addEdge(itemId, id, getAliasEdgeAttributes(alias));
        }
    }

    private void addProperty(GvWriter writer,
                             Property property) throws IOException {
        final String id = getId(property);
        writer.addNode(id, getNodeAttributes(property));
        if (xmargs.features.isEnabled(DictionaryToGv.BaseArgs.Feature.SHOW_TYPES)) {
            writer.addEdge(id, getId(property.getType()), getPropertyTypeEdgeAttributes(property));
        }
        addDescriptions(writer, id, property);
    }

    private void addAssertion(GvWriter writer,
                              Assertion assertion) throws IOException {
        final String assertionId = getId(assertion);
        writer.addNode(assertionId, getNodeAttributes(assertion));

        // Collect NamedDItems that are used by the assertion
        final Set<String> ids = collectUsedIds(assertion.getExpression());
        for (final String id : ids) {
            writer.addEdge(assertionId, id, getAssertionEdgeAttributes(assertion));
        }
        addDescriptions(writer, assertionId, assertion);
    }
}