package cdc.applic.charts.core;

import java.io.File;
import java.io.IOException;

import cdc.applic.charts.DictionaryToGvGenerator;
import cdc.applic.charts.DictionaryToGvGeneratorFeatures;
import cdc.applic.charts.core.DictionaryToGv.ApiMainArgs;
import cdc.applic.dictionaries.Dictionary;
import cdc.util.files.SearchPath;
import cdc.util.lang.Checks;

/**
 * Implementation of {@link DictionaryToGv}.
 */
public class DictionaryToGvGeneratorImpl implements DictionaryToGvGenerator {
    private final SearchPath paths;

    public DictionaryToGvGeneratorImpl(SearchPath paths) {
        Checks.isNotNull(paths, "paths");
        this.paths = paths;
    }

    @Override
    public void generate(Dictionary dictionary,
                         File outputDir,
                         DictionaryToGvGeneratorFeatures features) throws IOException {
        final DictionaryToGv.ApiMainArgs xmargs = new ApiMainArgs();
        xmargs.dictionary = dictionary;
        xmargs.paths = paths;
        xmargs.engine = features.getEngine();
        xmargs.formats.addAll(features.getFormats());
        xmargs.outputDir = outputDir;

        if (features.isEnabled(DictionaryToGvGeneratorFeatures.Hint.DIRECTED)) {
            xmargs.features.add(DictionaryToGv.BaseArgs.Feature.DIRECTED);
        } else {
            xmargs.features.add(DictionaryToGv.BaseArgs.Feature.UNDIRECTED);
        }

        if (features.isEnabled(DictionaryToGvGeneratorFeatures.Hint.GROUPS)) {
            xmargs.features.add(DictionaryToGv.BaseArgs.Feature.GROUP);
        }

        if (features.isEnabled(DictionaryToGvGeneratorFeatures.Hint.HORIZONTAL)) {
            xmargs.features.add(DictionaryToGv.BaseArgs.Feature.HORIZONTAL);
        }

        if (features.isEnabled(DictionaryToGvGeneratorFeatures.Hint.ALL_POLICIES)) {
            xmargs.features.add(DictionaryToGv.BaseArgs.Feature.ALL_POLICIES);
        }

        if (features.isEnabled(DictionaryToGvGeneratorFeatures.Hint.SHOW_TYPES)) {
            xmargs.features.add(DictionaryToGv.BaseArgs.Feature.SHOW_TYPES);
        }

        if (features.isEnabled(DictionaryToGvGeneratorFeatures.Hint.SHOW_DESCRIPTIONS)) {
            xmargs.features.add(DictionaryToGv.BaseArgs.Feature.SHOW_DESCRIPTIONS);
        }

        if (features.isEnabled(DictionaryToGvGeneratorFeatures.Hint.SHOW_ISSUES)) {
            xmargs.features.add(DictionaryToGv.BaseArgs.Feature.SHOW_ISSUES);
        }

        switch (features.getSymbolType()) {
        case LONG -> xmargs.features.add(DictionaryToGv.BaseArgs.Feature.LONG);
        case MATH -> xmargs.features.add(DictionaryToGv.BaseArgs.Feature.MATH);
        case SHORT -> xmargs.features.add(DictionaryToGv.BaseArgs.Feature.SHORT);
        }

        switch (features.getSpacing()) {
        case NARROW -> xmargs.features.add(DictionaryToGv.BaseArgs.Feature.NARROW);
        case WIDE -> xmargs.features.add(DictionaryToGv.BaseArgs.Feature.WIDE);
        }

        DictionaryToGv.generate(xmargs);
    }
}