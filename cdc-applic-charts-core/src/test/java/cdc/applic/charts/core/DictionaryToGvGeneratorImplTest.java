package cdc.applic.charts.core;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;

import cdc.applic.charts.DictionaryToGvGenerator;
import cdc.applic.charts.DictionaryToGvGeneratorFeatures;
import cdc.applic.charts.DictionaryToGvGeneratorFeatures.Hint;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.io.RepositoryXml;
import cdc.gv.tools.GvFormat;
import cdc.util.files.SearchPath;
import cdc.util.lang.FailureReaction;

class DictionaryToGvGeneratorImplTest {
    @Test
    void testCoverage() throws IOException {
        final RepositoryXml.StAXLoader loader = new RepositoryXml.StAXLoader(FailureReaction.WARN);
        final RepositoryImpl repository = loader.load("src/test/resources/repository.xml");

        final DictionaryToGvGenerator generator = new DictionaryToGvGeneratorImpl(SearchPath.EMPTY);

        generator.generate(repository.getRegistry("/Registry"),
                           new File("target"),
                           DictionaryToGvGeneratorFeatures.builder()
                                                          .format(GvFormat.PDF)
                                                          .hint(Hint.HORIZONTAL)
                                                          .hint(Hint.SHOW_TYPES)
                                                          .hint(Hint.SHOW_DESCRIPTIONS)
                                                          .hint(Hint.SHOW_ISSUES)
                                                          .hint(Hint.GROUPS)
                                                          .build());
        assertTrue(true);
    }
}