package cdc.applic.consistency.impl;

import static org.hamcrest.MatcherAssert.assertThat;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.xmlunit.builder.Input;
import org.xmlunit.matchers.CompareMatcher;

import cdc.applic.consistency.Composition;
import cdc.applic.consistency.impl.io.ConsistencyDataXml;
import cdc.applic.dictionaries.impl.PolicyImpl;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.Expression;
import cdc.io.xml.XmlWriter;
import cdc.util.lang.FailureReaction;

class ConsistencyDataXmlTest {
    @Test
    void testIO() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final PolicyImpl policy = registry.policy().name("Policy").build();

        final ConsistencyDataImpl data1 = new ConsistencyDataImpl("TestData", registry);
        final BlockDefImpl b1 = data1.createRootBlock("B1", "Block 1", registry, Expression.TRUE, Composition.ANY);
        final BlockDefImpl b2 = data1.createRootBlock("B2", "Block 2", registry, Expression.TRUE, Composition.ANY);
        b1.createBlockInc(data1, "B2");
        b1.createBlockDef(data1, "B1.1", "Block 1.1", registry, Expression.TRUE, Composition.AT_LEAST_ONE);
        b1.createBlockInc(data1, "B1.1");
        b2.createReference(data1, "R1", "Ref 1", policy, Expression.TRUE, "B1");

        final File file1 = new File("target/consistency-data1.xml");
        try (final XmlWriter writer = new XmlWriter(file1)) {
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
            ConsistencyDataXml.Printer.write(writer, data1);
        }

        final ConsistencyDataXml.StAXLoader loader = new ConsistencyDataXml.StAXLoader(FailureReaction.WARN, repository);
        final ConsistencyDataImpl data2 = loader.load(file1);

        final File file2 = new File("target/consistency-data2.xml");
        try (final XmlWriter writer = new XmlWriter(file2)) {
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
            ConsistencyDataXml.Printer.write(writer, data2);
        }

        assertThat(Input.fromFile(file1), CompareMatcher.isSimilarTo(Input.fromFile(file2)));

        // final Validator v = Validator.forLanguage(Languages.W3C_XML_SCHEMA_NS_URI);
        // v.setSchemaSource(Input.fromFile("src/main/resources/cdc/applic/applic-consistency.xsd").build());
        // final ValidationResult r = v.validateInstance(Input.fromFile(file1).build());
        // final Iterator<ValidationProblem> probs = r.getProblems().iterator();
        // int count = 0;
        // while (probs.hasNext()) {
        // count++;
        // LOGGER.error(probs.next().toString());
        // }
        // assertTrue(count == 0);
    }
}