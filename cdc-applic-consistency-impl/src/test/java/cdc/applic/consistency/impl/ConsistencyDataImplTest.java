package cdc.applic.consistency.impl;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.consistency.Composition;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.Expression;

class ConsistencyDataImplTest {
    private static final Logger LOGGER = LogManager.getLogger(ConsistencyDataImplTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    @Test
    void test() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("registry").build();
        final ConsistencyDataImpl data = new ConsistencyDataImpl("TestData", registry);
        final BlockDefImpl b1 = data.createRootBlock("B1", "Block 1", registry, Expression.TRUE, Composition.ANY);
        final BlockDefImpl b2 = data.createRootBlock("B2", "Block 2", registry, Expression.TRUE, Composition.ANY);
        b2.createReference(data, "R1", "Ref 1", registry, Expression.TRUE, "B1");
        b1.createBlockInc(data, "B2");
        // b2.createBlockInc(data, "B2"); // FIXME
        data.print(OUT, 0);
        assertTrue(true);
    }
}