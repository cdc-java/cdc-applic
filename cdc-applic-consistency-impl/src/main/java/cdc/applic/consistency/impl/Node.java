package cdc.applic.consistency.impl;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.expressions.Expression;
import cdc.util.debug.Printable;

/**
 * Base interface of all elements in the model: {@link Block} and {@link Reference}.}
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-31", forRemoval = true)
public interface Node extends Printable {
    /**
     * @return The node identifier.
     */
    public String getId();

    /**
     * @return The node label.
     */
    public String getLabel();

    /**
     * @return The configuration dictionary used to interpret applicability.
     */
    public Dictionary getDictionary();

    /**
     * @return The node applicability.
     */
    public Expression getApplicability();
}