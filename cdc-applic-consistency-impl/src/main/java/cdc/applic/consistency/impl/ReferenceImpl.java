package cdc.applic.consistency.impl;

import java.io.PrintStream;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.expressions.Expression;

/**
 * {@link Reference} implementation.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-31", forRemoval = true)
public class ReferenceImpl extends NodeImpl implements Reference {
    private final String targetId;

    protected ReferenceImpl(String id,
                            String label,
                            Dictionary dictionary,
                            Expression applicability,
                            String targetId) {
        super(id, label, dictionary, applicability);
        this.targetId = targetId;
    }

    @Override
    public String getTargetId() {
        return targetId;
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        out.println("Ref '" + getId() + "' '" + getLabel() + "' '" + getDictionary().getName() + "' " + getApplicability() + " '"
                + getTargetId() + "'");
    }
}