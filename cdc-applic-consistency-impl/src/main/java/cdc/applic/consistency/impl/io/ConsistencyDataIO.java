package cdc.applic.consistency.impl.io;

import java.io.File;
import java.io.IOException;

import cdc.applic.consistency.impl.ConsistencyDataImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.io.RepositoryIo;
import cdc.util.files.Files;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.UnexpectedValueException;

@Deprecated(since = "2024-12-31", forRemoval = true)
public final class ConsistencyDataIO {
    private ConsistencyDataIO() {
    }

    /**
     * Enumeration of formats supported by {@link RepositoryIo}.
     *
     * @author Damien Carbonne
     */
    public enum Format {
        XML;

        public static Format from(File file) {
            final String ext = Files.getExtension(file);
            for (final Format format : values()) {
                if (format.name().equalsIgnoreCase(ext)) {
                    return format;
                }
            }
            return null;
        }
    }

    public static ConsistencyDataImpl load(File file,
                                           RepositoryImpl repository) throws IOException {
        final Format format = Format.from(file);
        if (format == null) {
            throw new IllegalArgumentException("Can not load " + file);
        } else {
            switch (format) {
            case XML:
                final ConsistencyDataXml.StAXLoader loader = new ConsistencyDataXml.StAXLoader(FailureReaction.FAIL, repository);
                return loader.load(file);
            default:
                throw new UnexpectedValueException(format);
            }
        }
    }
}