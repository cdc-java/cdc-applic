package cdc.applic.consistency.impl;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.expressions.Expression;

/**
 * Base {@link Node} implementation.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-31", forRemoval = true)
public abstract class NodeImpl implements Node {
    private final String id;
    private final String label;
    private final Dictionary dictionary;
    private final Expression applicability;

    protected NodeImpl(String id,
                       String label,
                       Dictionary dictionary,
                       Expression applicability) {
        this.id = id;
        this.label = label;
        this.dictionary = dictionary;
        this.applicability = applicability;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public Dictionary getDictionary() {
        return dictionary;
    }

    @Override
    public Expression getApplicability() {
        return applicability;
    }
}
