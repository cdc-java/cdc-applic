package cdc.applic.consistency.impl;

import java.util.List;
import java.util.Set;

import cdc.applic.consistency.Composition;

/**
 * A Block is a composition node.
 * <p>
 * General composition is supported: a block can be component of several other parent blocks.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-31", forRemoval = true)
public interface Block extends Node {
    /**
     * @return The composition of this block. This applies to its children.
     */
    public Composition getComposition();

    /**
     * @return The parents blocks of this block.
     */
    public Set<? extends Block> getParents();

    /**
     * @return The children nodes of this block.
     */
    public List<? extends Node> getChildren();
}