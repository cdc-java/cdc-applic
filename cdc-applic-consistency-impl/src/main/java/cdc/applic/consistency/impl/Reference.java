package cdc.applic.consistency.impl;

/**
 * A Reference is a leaf node, declared in a block.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-31", forRemoval = true)
public interface Reference extends Node {
    /**
     * @return The identifier of the referenced block.
     */
    public String getTargetId();
}