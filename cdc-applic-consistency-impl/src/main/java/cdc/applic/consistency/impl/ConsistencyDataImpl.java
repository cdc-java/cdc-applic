package cdc.applic.consistency.impl;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.applic.consistency.Composition;
import cdc.applic.consistency.ConsistencyData;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.Registry;
import cdc.applic.expressions.Expression;
import cdc.util.debug.Printable;

/**
 * Implementation of {@link ConsistencyData}.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-31", forRemoval = true)
public class ConsistencyDataImpl implements ConsistencyData<Node, String, Block, Reference>, Printable {
    private final String systemId;
    private final Registry registry;
    private final List<BlockDefImpl> rootBlocks = new ArrayList<>();
    private final Map<String, Node> nodes = new HashMap<>();
    private final Map<String, Set<BlockIncImpl>> toFix = new HashMap<>();

    public ConsistencyDataImpl(String systemId,
                               Registry registry) {
        this.systemId = systemId;
        this.registry = registry;
    }

    protected void addNode(Node node) {
        if (node instanceof final BlockIncImpl n) {
            if (nodes.containsKey(node.getId())) {
                fix(n);
            } else {
                final Set<BlockIncImpl> set = toFix.computeIfAbsent(node.getId(), k -> new HashSet<>());
                set.add(n);
            }
        } else {
            if (nodes.containsKey(node.getId())) {
                throw new IllegalArgumentException("Duplicate id: " + node.getId());
            } else {
                nodes.put(node.getId(), node);
                if (node instanceof BlockDefImpl && toFix.containsKey(node.getId())) {
                    for (final BlockIncImpl block : toFix.get(node.getId())) {
                        fix(block);
                    }
                    toFix.remove(node.getId());
                }
            }
        }
    }

    private void fix(BlockIncImpl block) {
        block.fix(this);
    }

    public BlockDefImpl createRootBlock(String id,
                                        String label,
                                        Dictionary dictionary,
                                        Expression applicability,
                                        Composition composition) {
        final BlockDefImpl block = new BlockDefImpl(id, label, dictionary, applicability, composition);
        addNode(block);
        rootBlocks.add(block);
        return block;
    }

    public BlockDefImpl createRootBlock(String id,
                                        String label,
                                        Dictionary dictionary,
                                        String applicability,
                                        Composition composition) {
        return createRootBlock(id, label, dictionary, new Expression(applicability), composition);
    }

    public BlockDefImpl createBlockDef(String parentId,
                                       String id,
                                       String label,
                                       Dictionary dictionary,
                                       Expression applicability,
                                       Composition composition) {
        final BlockDefImpl parent = getNode(parentId, BlockDefImpl.class);
        return parent.createBlockDef(this, id, label, dictionary, applicability, composition);
    }

    public BlockDefImpl createBlockDef(String parentId,
                                       String id,
                                       String label,
                                       Dictionary dictionary,
                                       String applicability,
                                       Composition composition) {
        return createBlockDef(parentId, id, label, dictionary, new Expression(applicability), composition);
    }

    public BlockIncImpl createBlockInc(String parentId,
                                       String id) {
        final BlockDefImpl parent = getNode(parentId, BlockDefImpl.class);
        return parent.createBlockInc(this, id);
    }

    public ReferenceImpl createReference(String parentId,
                                         String id,
                                         String label,
                                         Dictionary dictionary,
                                         Expression applicability,
                                         String targetId) {
        final BlockDefImpl parent = getNode(parentId, BlockDefImpl.class);
        return parent.createReference(this, id, label, dictionary, applicability, targetId);
    }

    public ReferenceImpl createReference(String parentId,
                                         String id,
                                         String label,
                                         Dictionary dictionary,
                                         String applicability,
                                         String targetId) {
        return createReference(parentId, id, label, dictionary, new Expression(applicability), targetId);
    }

    @Override
    public String getSystemId() {
        return systemId;
    }

    @Override
    public Registry getRegistry() {
        return registry;
    }

    @Override
    public Iterable<BlockDefImpl> getRootBlocks() {
        return rootBlocks;
    }

    public Node getNode(String id) {
        return nodes.get(id);
    }

    public <T extends Node> T getNode(String id,
                                      Class<T> cls) {
        return cls.cast(getNode(id));
    }

    @Override
    public String getNodeId(Node node) {
        return node.getId();
    }

    @Override
    public boolean isBlock(Node node) {
        return node instanceof Block;
    }

    @Override
    public boolean isReference(Node node) {
        return node instanceof Reference;
    }

    @Override
    public String getNodeDisplayLabel(Node node) {
        return node.getLabel();
    }

    @Override
    public Dictionary getNodeDictionary(Node node) {
        return node.getDictionary();
    }

    @Override
    public Expression getNodeLocalApplicability(Node node) {
        return node.getApplicability();
    }

    @Override
    public Iterable<? extends Node> getBlockChildren(Block parent) {
        return parent.getChildren();
    }

    @Override
    public Iterable<? extends Block> getBlockParents(Block child) {
        return child.getParents();
    }

    @Override
    public Composition getBlockComposition(Block block) {
        return block.getComposition();
    }

    @Override
    public Block getReferenceTarget(Reference ref) {
        return getNode(ref.getTargetId(), Block.class);
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        out.println(getRegistry().getName());
        for (final BlockDefImpl root : getRootBlocks()) {
            root.print(out, level);
        }
    }
}