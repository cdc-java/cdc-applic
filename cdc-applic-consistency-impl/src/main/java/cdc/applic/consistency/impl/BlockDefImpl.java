package cdc.applic.consistency.impl;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cdc.applic.consistency.Composition;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.expressions.Expression;

/**
 * Implementation of {@link Block} b=y definition.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-31", forRemoval = true)
public class BlockDefImpl extends NodeImpl implements Block {
    private final Composition composition;
    private final Set<BlockDefImpl> parents = new HashSet<>();
    private final List<Node> children = new ArrayList<>();

    protected BlockDefImpl(String id,
                           String label,
                           Dictionary dictionary,
                           Expression applicability,
                           Composition composition) {
        super(id, label, dictionary, applicability);
        this.composition = composition;
    }

    protected void addParent(BlockDefImpl parent) {
        parents.add(parent);
    }

    public BlockDefImpl createBlockDef(ConsistencyDataImpl data,
                                       String id,
                                       String label,
                                       Dictionary dictionary,
                                       Expression applicability,
                                       Composition composition) {
        final BlockDefImpl block = new BlockDefImpl(id, label, dictionary, applicability, composition);
        data.addNode(block);
        children.add(block);
        block.parents.add(this);
        return block;
    }

    public BlockIncImpl createBlockInc(ConsistencyDataImpl data,
                                       String id) {
        // TODO check that id is not an ancestor of this block
        final BlockIncImpl block = new BlockIncImpl(this, id);
        data.addNode(block);
        children.add(block);
        return block;
    }

    public ReferenceImpl createReference(ConsistencyDataImpl data,
                                         String id,
                                         String label,
                                         Dictionary dictionary,
                                         Expression applicability,
                                         String targetId) {
        final ReferenceImpl ref = new ReferenceImpl(id, label, dictionary, applicability, targetId);
        data.addNode(ref);
        children.add(ref);
        return ref;
    }

    @Override
    public Composition getComposition() {
        return composition;
    }

    @Override
    public Set<BlockDefImpl> getParents() {
        return parents;
    }

    @Override
    public List<Node> getChildren() {
        return children;
    }

    public boolean isOver(BlockDefImpl other) {
        // TODO
        return false;
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        out.println("BlockDef '" + getId() + "' '" + getLabel() + "' '" + getDictionary().getName() + "' " + getApplicability()
                + " " + getComposition());
        for (final Node child : getChildren()) {
            child.print(out, level + 1);
        }
    }
}