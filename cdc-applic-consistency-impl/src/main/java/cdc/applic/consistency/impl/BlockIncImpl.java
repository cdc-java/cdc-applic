package cdc.applic.consistency.impl;

import java.io.PrintStream;
import java.util.List;
import java.util.Set;

import cdc.applic.consistency.Composition;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.expressions.Expression;

/**
 * Implementation of {@link Block} b=y inclusion.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-31", forRemoval = true)
public class BlockIncImpl implements Block {
    private final BlockDefImpl owner;
    private final String id;
    private BlockDefImpl def = null;

    protected void fix(ConsistencyDataImpl data) {
        if (def == null) {
            def = data.getNode(id, BlockDefImpl.class);
            if (def != null) {
                if (def.isOver(owner)) {
                    throw new IllegalStateException("Composition cycle detected");
                }
                def.addParent(owner);
            }
        }
    }

    public BlockIncImpl(BlockDefImpl owner,
                        String id) {
        this.owner = owner;
        this.id = id;
    }

    private BlockDefImpl getDef() {
        return def;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getLabel() {
        return getDef().getLabel();
    }

    @Override
    public Dictionary getDictionary() {
        return getDef().getDictionary();
    }

    @Override
    public Expression getApplicability() {
        return getDef().getApplicability();
    }

    @Override
    public Composition getComposition() {
        return getDef().getComposition();
    }

    @Override
    public Set<? extends Block> getParents() {
        return getDef().getParents();
    }

    @Override
    public List<? extends Node> getChildren() {
        return getDef().getChildren();
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        out.println("BlockInc '" + getId() + "'");
    }
}