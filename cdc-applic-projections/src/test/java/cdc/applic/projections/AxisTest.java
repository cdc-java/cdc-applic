package cdc.applic.projections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.literals.Name;

class AxisTest {
    private final RepositorySupport support = new RepositorySupport();

    public AxisTest() {
        support.registry.alias().name("A").expression("true").ordinal(0).build();
    }

    @Test
    void testEnumeratedType() {
        final Property property = support.registry.getProperty(Name.of("E"));
        final Axis axis = new Axis(property);
        assertEquals(Axis.Kind.PROPERTY, axis.getKind());
        assertEquals(StringSet.class, axis.getCheckedSetClass());
        assertEquals(property, axis.getItemOrNull());
        assertEquals(property, axis.getPropertyOrNull());
        assertEquals(null, axis.getAliasOrNull());
        assertEquals(null, axis.getExpressionOrNull());
        assertFalse(axis.isContextual());
        assertEquals(property.hashCode(), axis.hashCode());
        assertEquals("[PROPERTY E]", axis.toString());
    }

    @Test
    void testPatternType() {
        final Property property = support.registry.getProperty(Name.of("P"));
        final Axis axis = new Axis(property);
        assertEquals(Axis.Kind.PROPERTY, axis.getKind());
        assertEquals(StringSet.class, axis.getCheckedSetClass());
        assertEquals(property, axis.getItemOrNull());
        assertEquals(property, axis.getPropertyOrNull());
        assertEquals(null, axis.getAliasOrNull());
        assertEquals(null, axis.getExpressionOrNull());
        assertTrue(axis.isContextual());
        assertEquals(property.hashCode(), axis.hashCode());
        assertEquals("[PROPERTY P]", axis.toString());
    }

    @Test
    void testBooleanType() {
        final Property property = support.registry.getProperty(Name.of("B"));
        final Axis axis = new Axis(property);
        assertEquals(Axis.Kind.PROPERTY, axis.getKind());
        assertEquals(BooleanSet.class, axis.getCheckedSetClass());
        assertEquals(property, axis.getItemOrNull());
        assertEquals(property, axis.getPropertyOrNull());
        assertEquals(null, axis.getAliasOrNull());
        assertEquals(null, axis.getExpressionOrNull());
        assertFalse(axis.isContextual());
        assertEquals(property.hashCode(), axis.hashCode());
        assertEquals("[PROPERTY B]", axis.toString());
    }

    @Test
    void testIntegerType() {
        final Property property = support.registry.getProperty(Name.of("I"));
        final Axis axis = new Axis(property);
        assertEquals(Axis.Kind.PROPERTY, axis.getKind());
        assertEquals(IntegerSet.class, axis.getCheckedSetClass());
        assertEquals(property, axis.getItemOrNull());
        assertEquals(property, axis.getPropertyOrNull());
        assertEquals(null, axis.getAliasOrNull());
        assertEquals(null, axis.getExpressionOrNull());
        assertTrue(axis.isContextual());
        assertEquals(property.hashCode(), axis.hashCode());
        assertEquals("[PROPERTY I]", axis.toString());
    }

    @Test
    void testRealType() {
        final Property property = support.registry.getProperty(Name.of("R"));
        final Axis axis = new Axis(property);
        assertEquals(Axis.Kind.PROPERTY, axis.getKind());
        assertEquals(RealSet.class, axis.getCheckedSetClass());
        assertEquals(property, axis.getItemOrNull());
        assertEquals(property, axis.getPropertyOrNull());
        assertEquals(null, axis.getAliasOrNull());
        assertEquals(null, axis.getExpressionOrNull());
        assertTrue(axis.isContextual());
        assertEquals(property.hashCode(), axis.hashCode());
        assertEquals("[PROPERTY R]", axis.toString());
    }

    @Test
    void testAlias() {
        final Alias alias = support.registry.getAlias(Name.of("A"));
        final Axis axis = new Axis(alias);
        assertEquals(Axis.Kind.ALIAS, axis.getKind());
        assertEquals(BooleanSet.class, axis.getCheckedSetClass());
        assertEquals(alias, axis.getItemOrNull());
        assertEquals(null, axis.getPropertyOrNull());
        assertEquals(alias, axis.getAliasOrNull());
        assertEquals(null, axis.getExpressionOrNull());
        assertFalse(axis.isContextual());
        assertEquals(alias.hashCode(), axis.hashCode());
        assertEquals("[ALIAS A]", axis.toString());
    }

    @Test
    void testExpression() {
        final Expression expression = new Expression("true");
        final Axis axis = new Axis(expression);
        assertEquals(Axis.Kind.EXPRESSION, axis.getKind());
        assertEquals(BooleanSet.class, axis.getCheckedSetClass());
        assertEquals(null, axis.getItemOrNull());
        assertEquals(null, axis.getPropertyOrNull());
        assertEquals(null, axis.getAliasOrNull());
        assertEquals(expression, axis.getExpressionOrNull());
        assertFalse(axis.isContextual());
        assertEquals(expression.hashCode(), axis.hashCode());
        assertEquals("[EXPRESSION true]", axis.toString());
    }

    @Test
    void testEquals() {
        final Property property = support.registry.getProperty(Name.of("E"));
        final Axis axis1a = new Axis(property);
        final Axis axis1b = new Axis(property);
        final Expression expression = new Expression("true");
        final Axis axis2 = new Axis(expression);

        assertEquals(axis1a, axis1a);
        assertEquals(axis1a, axis1b);
        assertNotEquals(axis1a, axis2);
        assertNotEquals(axis1a, null);
        assertNotEquals(axis1a, "Hello");
    }

    @Test
    void testCompareTo() {
        final Axis axise = new Axis(support.registry.getProperty(Name.of("E")));
        final Axis axisp = new Axis(support.registry.getProperty(Name.of("P")));
        final Axis axisi = new Axis(support.registry.getProperty(Name.of("I")));
        final Axis axisr = new Axis(support.registry.getProperty(Name.of("R")));
        final Axis axisb = new Axis(support.registry.getProperty(Name.of("B")));
        final Axis axisa = new Axis(support.registry.getAlias(Name.of("A")));
        final Axis axisx = new Axis(new Expression("true"));

        assertTrue(axise.compareTo(axisa) < 0);
        assertTrue(axisp.compareTo(axisa) < 0);
        assertTrue(axisi.compareTo(axisa) < 0);
        assertTrue(axisr.compareTo(axisa) < 0);
        assertTrue(axisb.compareTo(axisa) < 0);

        assertTrue(axise.compareTo(axisx) < 0);
        assertTrue(axisp.compareTo(axisx) < 0);
        assertTrue(axisi.compareTo(axisx) < 0);
        assertTrue(axisr.compareTo(axisx) < 0);
        assertTrue(axisb.compareTo(axisx) < 0);
        assertTrue(axisa.compareTo(axisx) < 0);

        assertSame(0, axise.compareTo(axise));
        assertSame(0, axisx.compareTo(axisx));
    }
}