package cdc.applic.projections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.literals.Name;

class ShadowPieceTest {
    private final RepositorySupport support = new RepositorySupport();

    public ShadowPieceTest() {
        support.registry.alias().name("A").expression("true").build();
    }

    @Test
    void test() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         new ShadowPiece();
                     });

        final Axis axis1 = new Axis(support.registry.getProperty(Name.of("E")));
        final Axis axis2 = new Axis(support.registry.getProperty(Name.of("I")));
        final AxisPiece piece1a = new AxisPiece(axis1, SItemSetUtils.createBest("E1"));
        final AxisPiece piece1b = new AxisPiece(axis1, SItemSetUtils.createBest("E1"));
        final AxisPiece piece2a = new AxisPiece(axis2, SItemSetUtils.createBest("10"));

        final ShadowPiece spiece1 = new ShadowPiece(piece1a);

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         new ShadowPiece(piece1a, piece1b);
                     });
        final ShadowPiece spiece2 = new ShadowPiece(piece1a, piece2a);

        assertEquals(1, spiece1.size());
        assertEquals(2, spiece2.size());

        assertEquals(1, spiece1.getAxes().size());
        assertTrue(spiece1.getAxes().contains(axis1));

        assertEquals(2, spiece2.getAxes().size());
        assertTrue(spiece2.getAxes().contains(axis1));
        assertTrue(spiece2.getAxes().contains(axis2));

        assertEquals(piece1a, spiece1.getAxisPiece(axis1));
        assertEquals(piece1a.getSet(), spiece1.getAxisSet(axis1));

        assertEquals("[[[PROPERTY E]:{E1}]]", spiece1.toString());
        assertEquals("[[[PROPERTY E]:{E1}]x[[PROPERTY I]:{10}]]", spiece2.toString());

        assertEquals(spiece1, spiece1);
        assertNotEquals(spiece1, spiece2);
        assertNotEquals(spiece1, null);
        assertNotEquals(spiece1, "Hello");

        assertNotEquals(spiece1.hashCode(), spiece2.hashCode());
    }
}