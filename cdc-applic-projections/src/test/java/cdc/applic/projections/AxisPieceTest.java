package cdc.applic.projections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.literals.Name;

class AxisPieceTest {
    private final RepositorySupport support = new RepositorySupport();

    public AxisPieceTest() {
        support.registry.alias().name("A").expression("true").ordinal(0).build();
    }

    @Test
    void testEnumeratedType() {
        final Axis axis = new Axis(support.registry.getProperty(Name.of("E")));
        final AxisPiece spiece = new AxisPiece(axis, SItemSetUtils.createBest("E1"));
        final AxisPiece ipiece = new AxisPiece(axis, StringValue.of("E1"));

        assertEquals(axis, spiece.getAxis());
        assertEquals(axis, ipiece.getAxis());
        assertEquals(spiece.getSet(), ipiece.getSet());
        assertEquals(spiece.hashCode(), ipiece.hashCode());

        assertEquals("[[PROPERTY E]:{E1}]", spiece.toString());
        assertEquals("[[PROPERTY E]:{E1}]", ipiece.toString());

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         new AxisPiece(axis, SItemSetUtils.createBest("10"));
                     });

    }

    @Test
    void testEquals() {
        final Axis axis1 = new Axis(support.registry.getProperty(Name.of("E")));
        final Axis axis2 = new Axis(support.registry.getProperty(Name.of("I")));
        final AxisPiece piece1a = new AxisPiece(axis1, SItemSetUtils.createBest("E1"));
        final AxisPiece piece1b = new AxisPiece(axis1, SItemSetUtils.createBest("E1"));
        final AxisPiece piece2 = new AxisPiece(axis1, SItemSetUtils.createBest("E2"));
        final AxisPiece piece3 = new AxisPiece(axis2, SItemSetUtils.createBest("10"));

        assertEquals(piece1a, piece1a);
        assertEquals(piece1a, piece1b);
        assertNotEquals(piece1a, piece2);
        assertNotEquals(piece1a, piece3);
        assertNotEquals(piece1a, null);
        assertNotEquals(piece1a, "Hello");
    }
}