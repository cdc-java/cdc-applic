package cdc.applic.projections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.literals.Name;
import cdc.applic.proofs.ProverMatching;
import cdc.util.lang.CollectionUtils;

class ShadowTest {
    private final RepositorySupport support = new RepositorySupport();

    public ShadowTest() {
        support.registry.alias().name("A").expression("true").build();
    }

    @Test
    void test() {
        final Axis axis1 = new Axis(support.registry.getProperty(Name.of("E")));
        final Axis axis2 = new Axis(support.registry.getProperty(Name.of("I")));
        final AxisPiece piece1a = new AxisPiece(axis1, SItemSetUtils.createBest("E1"));
        final AxisPiece piece1b = new AxisPiece(axis1, SItemSetUtils.createBest("E2"));
        final AxisPiece piece2 = new AxisPiece(axis2, SItemSetUtils.createBest("10"));

        final Shadow shadow1DNS = new Shadow(piece1a,
                                             piece1b,
                                             null);

        final Shadow shadow1DN = new Shadow(axis1,
                                            SItemSetUtils.createBest("E1"),
                                            null,
                                            null);

        final ShadowPiece spiece1a2 = new ShadowPiece(piece1a, piece2);
        final ShadowPiece spiece1b2 = new ShadowPiece(piece1b, piece2);

        final Shadow shadow2DNS = new Shadow(spiece1a2,
                                             spiece1b2,
                                             null);

        final Shadow shadow2DA = new Shadow(null,
                                            null,
                                            CollectionUtils.toSet(spiece1a2));

        assertEquals(shadow1DNS, shadow1DNS);
        assertNotEquals(shadow1DNS, shadow1DN);
        assertNotEquals(shadow1DNS, null);
        assertNotEquals(shadow1DNS, "Hello");

        assertEquals(shadow1DNS.hashCode(), shadow1DNS.hashCode());

        assertEquals(1, shadow2DNS.getShadowPieces(ProverMatching.NEVER).size());
        assertEquals(1, shadow2DNS.getShadowPieces(ProverMatching.SOMETIMES).size());
        assertEquals(0, shadow2DNS.getShadowPieces(ProverMatching.ALWAYS).size());

        assertTrue(shadow2DA.getAxisPieces(axis1, ProverMatching.NEVER).isEmpty());
        assertTrue(shadow2DA.getAxisPieces(axis1, ProverMatching.SOMETIMES).isEmpty());
        assertEquals(1, shadow2DA.getAxisPieces(axis1, ProverMatching.ALWAYS).size());

        assertEquals(StringSet.EMPTY, shadow2DA.getAxisSet(axis1, ProverMatching.NEVER));
        assertEquals(StringSet.EMPTY, shadow2DA.getAxisSet(axis1, ProverMatching.SOMETIMES));
        assertEquals(SItemSetUtils.createBest("E1"), shadow2DA.getAxisSet(axis1, ProverMatching.ALWAYS));

        // Coverage
        assertNotEquals(shadow2DNS.toString(), shadow2DA.toString());
    }
}