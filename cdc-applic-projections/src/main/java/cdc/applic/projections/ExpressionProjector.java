package cdc.applic.projections;

import java.util.List;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;
import cdc.applic.proofs.ProverFeatures;

/**
 * Interface implemented by objects that can project an Expression on a subspace.
 *
 * @author Damien Carbonne
 */
public interface ExpressionProjector {
    /**
     * @return The DictionaryHandle used by this Projector.
     */
    public DictionaryHandle getDictionaryHandle();

    /**
     * @return The prover features used by this Projector.
     */
    public ProverFeatures getProverFeatures();

    /**
     * @return The axes defining the projection space.
     */
    public List<Axis> getAxes();

    /**
     * Projects a node.
     *
     * @param node The node.
     * @return The projection of {@code node}.
     */
    public Shadow project(Node node);

    /**
     * Projects an expression.
     *
     * @param expression The expression.
     * @return The projection of {@code expression}.
     */
    public Shadow project(Expression expression);
}