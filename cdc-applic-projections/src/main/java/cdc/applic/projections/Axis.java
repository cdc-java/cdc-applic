package cdc.applic.projections;

import java.util.Objects;

import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.CheckedSet;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

/**
 * Definition of an axis onto which projection can be done.
 * <p>
 * An axis is associated to a set of values.<br>
 * One can build an axis from:
 * <ul>
 * <li>A {@link Property}. The associated type is the type of the property.
 * <li>An {@link Alias}. The associated type is a {@link BooleanType}.
 * <li>An {@link Expression}. The associated type is a {@link BooleanType}.
 * </ul>
 *
 * @author Damien Carbonne
 */
public class Axis implements Comparable<Axis> {
    /** Property, Alias or Expression, in accordance with {link {@link #kind} */
    private final Object data;

    /** Kind of {@link #data}. */
    private final Kind kind;
    private final boolean contextual;

    /**
     * Enumeration of possible axis kinds.
     * <p>
     * <b>WARNING:</b> Order of declarations matters for comparison.
     *
     * @author Damien Carbonne
     */
    public enum Kind {
        PROPERTY,
        ALIAS,
        EXPRESSION
    }

    /**
     * Creates an Axis from a {@link NamedDItem}.
     *
     * @param item The item.
     * @throws IllegalArgumentException When {@code item} is {@code null}.
     */
    public Axis(NamedDItem item) {
        Checks.isNotNull(item, "item");
        this.data = item;
        if (item instanceof final Property property) {
            this.kind = Kind.PROPERTY;
            final Type type = property.getType();
            if (type instanceof BooleanType || type instanceof EnumeratedType) {
                this.contextual = false;
            } else {
                this.contextual = true;
            }
        } else {
            this.kind = Kind.ALIAS;
            this.contextual = false;
        }
    }

    /**
     * Creates an Axis from an {@link Expression}.
     *
     * @param expression The expression.
     * @throws IllegalArgumentException When {@code expression} is {@code null}.
     */
    public Axis(Expression expression) {
        Checks.isNotNull(expression, "expression");
        this.data = expression;
        this.kind = Kind.EXPRESSION;
        this.contextual = false;
    }

    /**
     * @return The kind of this axis.
     */
    public Kind getKind() {
        return kind;
    }

    /**
     * @return {@code true} if this axis is contextual,
     *         that is if the cut of this axis depends on the projected node.
     */
    public boolean isContextual() {
        return contextual;
    }

    /**
     * @return The {@link NamedDItem} of this axis, or {@code null}.
     */
    public NamedDItem getItemOrNull() {
        return data instanceof final NamedDItem d ? d : null;
    }

    /**
     * @return The {@link Property} of this axis, or {@code null}.
     */
    public Property getPropertyOrNull() {
        return data instanceof final Property d ? d : null;
    }

    /**
     * @return The {@link Alias} of this axis, or {@code null}.
     */
    public Alias getAliasOrNull() {
        return data instanceof final Alias d ? d : null;
    }

    /**
     * @return The {@link Expression} of this axis, or {@code null}.
     */
    public Expression getExpressionOrNull() {
        return data instanceof final Expression d ? d : null;
    }

    /**
     * @return The label associated to this axis.
     *         There is always a non null result.
     */
    public String getLabel() {
        return data instanceof final NamedDItem d
                ? d.getName().getNonEscapedLiteral()
                : ((Expression) data).getContent();
    }

    /**
     * @return The Name associated to this axis or {@code null}.
     *         There is a valid Name when this axis is associated to a Property or Alias.
     */
    public Name getNameOrNull() {
        return data instanceof final NamedDItem d ? d.getName() : null;
    }

    public Class<? extends CheckedSet<?, ?>> getCheckedSetClass() {
        if (data instanceof final Property d) {
            return Type.getCheckedSetClass(d.getType());
        } else {
            return BooleanSet.class;
        }
    }

    @Override
    public int hashCode() {
        return data.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Axis)) {
            return false;
        }
        final Axis other = (Axis) object;
        return Objects.equals(data, other.data);
    }

    @Override
    public int compareTo(Axis other) {
        final int kindCmp = getKind().compareTo(other.getKind());
        if (kindCmp == 0) {
            if (data instanceof NamedDItem) {
                return getItemOrNull().getName().compareTo(other.getItemOrNull().getName());
            } else {
                return getExpressionOrNull().compareTo(other.getExpressionOrNull());
            }
        } else {
            return kindCmp;
        }
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append('[')
               .append(getKind())
               .append(' ');
        if (getExpressionOrNull() == null) {
            builder.append(getItemOrNull().getName());
        } else {
            builder.append(getExpressionOrNull());
        }
        builder.append(']');
        return builder.toString();
    }
}