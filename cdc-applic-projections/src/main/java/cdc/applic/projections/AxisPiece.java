package cdc.applic.projections;

import java.util.Objects;

import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.util.lang.Checks;

/**
 * Definition of an elementary piece of Axis.
 * <p>
 * It is an ({@link Axis}, {@link SItemSet}) pair.<br>
 * The set must be compliant with axis.
 *
 * @author Damien Carbonne
 */
public class AxisPiece {
    private final Axis axis;
    private final SItemSet set;

    public AxisPiece(Axis axis,
                     SItemSet set) {
        Checks.isNotNull(axis, "axis");
        Checks.isNotNull(set, "set");
        Checks.isFalse(set.isEmpty(), "empty set");
        Checks.isTrue(set.isCompliantWith(axis.getCheckedSetClass()), "Non compliant axis {} and set {}", axis, set);

        this.axis = axis;
        this.set = set;
    }

    public AxisPiece(Axis axis,
                     SItem item) {
        this(axis,
             SItemSetUtils.createBest(item));
    }

    /**
     * @return The Axis of this AxisPiece.
     */
    public Axis getAxis() {
        return axis;
    }

    /**
     * @return The SItemSet of this AxisPiece.
     */
    public SItemSet getSet() {
        return set;
    }

    @Override
    public int hashCode() {
        return Objects.hash(axis, set);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof AxisPiece)) {
            return false;
        }
        final AxisPiece other = (AxisPiece) object;
        return axis.equals(other.axis)
                && set.equals(other.set);
    }

    @Override
    public String toString() {
        return "[" + axis + ":" + set + "]";
    }
}