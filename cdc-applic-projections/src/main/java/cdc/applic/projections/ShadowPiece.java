package cdc.applic.projections;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cdc.applic.expressions.content.SItemSet;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;

/**
 * Definition of a piece of shadow.
 *
 * @author Damien Carbonne
 */
public class ShadowPiece {
    private final Map<Axis, AxisPiece> map;

    public ShadowPiece(Collection<AxisPiece> pieces) {
        Checks.isNotNullOrEmpty(pieces, "pieces");

        final Map<Axis, AxisPiece> tmp = new HashMap<>();
        for (final AxisPiece piece : pieces) {
            tmp.put(piece.getAxis(), piece);
        }
        this.map = Collections.unmodifiableMap(tmp);

        if (map.size() != pieces.size()) {
            throw new IllegalArgumentException("Duplicate axes");
        }
    }

    public ShadowPiece(AxisPiece... pieces) {
        this(Arrays.asList(pieces));
    }

    public ShadowPiece(AxisPiece piece) {
        final Map<Axis, AxisPiece> tmp = new HashMap<>();
        tmp.put(piece.getAxis(), piece);
        this.map = Collections.unmodifiableMap(tmp);
    }

    public int size() {
        return map.size();
    }

    public Set<Axis> getAxes() {
        return map.keySet();
    }

    public AxisPiece getAxisPiece(Axis axis) {
        return map.get(axis);
    }

    public SItemSet getAxisSet(Axis axis) {
        return map.get(axis).getSet();
    }

    @Override
    public int hashCode() {
        return map.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof ShadowPiece)) {
            return false;
        }
        final ShadowPiece other = (ShadowPiece) object;
        return map.equals(other.map);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        boolean first = true;
        builder.append('[');
        for (final Axis axis : IterableUtils.toSortedList(getAxes())) {
            if (first) {
                first = false;
            } else {
                builder.append('x');
            }
            builder.append(getAxisPiece(axis));
        }
        builder.append(']');
        return builder.toString();
    }
}