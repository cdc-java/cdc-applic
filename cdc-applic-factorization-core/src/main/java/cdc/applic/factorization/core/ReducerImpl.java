package cdc.applic.factorization.core;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Expressions;
import cdc.applic.factorization.ApplicabilityExtractor;
import cdc.applic.factorization.FactorizationFeatures;
import cdc.applic.factorization.Reducer;
import cdc.applic.factorization.events.ReduceEvent;
import cdc.applic.factorization.handlers.ReduceHandler;
import cdc.applic.simplification.SimplifierFeatures;
import cdc.util.lang.Checks;

public final class ReducerImpl extends AbstractImpl implements Reducer {
    private static final Logger LOGGER = LogManager.getLogger(ReducerImpl.class);

    public ReducerImpl(DictionaryHandle handle,
                       SimplifierFeatures simplifierFeatures) {
        super(handle, simplifierFeatures);
    }

    @Override
    public <T> void reduce(List<? extends T> objects,
                           Expression targetApplicability,
                           ApplicabilityExtractor<? super T> extractor,
                           FactorizationFeatures features,
                           ReduceHandler<T> handler) {
        LOGGER.debug("reduce({}, {})", objects, targetApplicability);
        Checks.isNotNull(targetApplicability, "applicability");
        Checks.isNotNull(objects, "objects");
        Checks.isNotNull(extractor, "extractor");
        Checks.isNotNull(handler, "handler");

        handler.processBegin();

        for (final T object : objects) {
            LOGGER.debug("   object: {}", object);
            final Expression applicability = extractor.getApplicability(object);
            final Expression newApplicability = Expressions.SHORT_NARROW_NO_SIMPLIFY.and(applicability, targetApplicability);
            LOGGER.debug("      new applicability: {}", newApplicability);
            final boolean hasSolutions = prover.isSometimesTrue(newApplicability.getRootNode());
            if (hasSolutions) {
                // Simplify the target expression in any case
                final Expression newSimplified = simplify(newApplicability, features);
                // Did the simplification change anything ?
                // We can not rely on equivalence because we can miss some
                // simplification opportunities such as 'X or False' <-> 'X'
                final boolean areSame = applicability.getRootNode().equals(newSimplified.getRootNode());
                if (areSame) {
                    LOGGER.debug("      unchanged {}", applicability);
                } else {
                    // Keep this object with its new applicability
                    LOGGER.debug("      change {} into {}", applicability, newSimplified);
                    handler.processChangeObjectApplicability(object, newSimplified);
                }
            } else {
                // Remove this object
                LOGGER.debug("      remove");
                handler.processRemoveObject(object);
            }
        }
        handler.processEnd();
    }

    @Override
    public <T> List<ReduceEvent<T>> reduce(List<? extends T> objects,
                                           Expression targetApplicability,
                                           ApplicabilityExtractor<? super T> extractor,
                                           FactorizationFeatures features) {

        final List<ReduceEvent<T>> events = new ArrayList<>();
        final ReduceHandler<T> handler = new ReduceHandler<T>() {
            @Override
            public void processChangeObjectApplicability(T object,
                                                         Expression applicability) {
                events.add(ReduceEvent.newChangeObjectApplicability(object, applicability));
            }

            @Override
            public void processRemoveObject(T object) {
                events.add(ReduceEvent.newRemoveObject(object));
            }
        };
        reduce(objects, targetApplicability, extractor, features, handler);
        return events;
    }
}