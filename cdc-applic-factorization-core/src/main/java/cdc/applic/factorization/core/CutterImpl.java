package cdc.applic.factorization.core;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.factorization.ApplicabilityExtractor;
import cdc.applic.factorization.Cutter;
import cdc.applic.factorization.FactorizationFeatures;
import cdc.applic.factorization.events.CutEvent;
import cdc.applic.factorization.handlers.CutHandler;
import cdc.applic.simplification.SimplifierFeatures;
import cdc.util.lang.Checks;

public final class CutterImpl extends AbstractImpl implements Cutter {
    public CutterImpl(DictionaryHandle handle,
                      SimplifierFeatures simplifierFeatures) {
        super(handle, simplifierFeatures);
    }

    @Override
    public <T> void cut(List<? extends T> objects,
                        Expression targetApplicability,
                        ApplicabilityExtractor<? super T> applicabilityExtractor,
                        FactorizationFeatures features,
                        CutHandler<T> handler) {
        Checks.isNotNull(targetApplicability, "applicability");
        Checks.isNotNull(objects, "objects");
        Checks.isNotNull(applicabilityExtractor, "applicabilityExtractor");
        Checks.isNotNull(handler, "handler");

        handler.processBegin();

        if (!objects.isEmpty()) {
            final Node target = targetApplicability.getRootNode();
            final Node notTarget = new NotNode(target);
            for (final T object : objects) {
                final Node applicability = applicabilityExtractor.getApplicability(object).getRootNode();
                final Node excluded = new AndNode(applicability, notTarget);
                final Node included = new AndNode(applicability, target);

                final boolean ex = prover.isSometimesTrue(excluded);
                final boolean in = prover.isSometimesTrue(included);

                if (ex) {
                    if (in) {
                        handler.processCutObjectApplicability(object,
                                                              simplify(excluded, features),
                                                              simplify(included, features));
                    } else {
                        handler.processExcludeObjectApplicability(object);
                    }
                } else {
                    if (in) {
                        handler.processIncludeObjectApplicability(object);
                    } else {
                        // Both sets are empty
                        // It may be due to target applicability or (inclusive) object applicability
                        handler.processDegenerateObjectApplicability(object);
                    }
                }
            }
        }

        handler.processEnd();
    }

    @Override
    public <T> List<CutEvent<T>> cut(List<? extends T> objects,
                                     Expression targetApplicability,
                                     ApplicabilityExtractor<? super T> applicabilityExtractor,
                                     FactorizationFeatures features) {
        final List<CutEvent<T>> events = new ArrayList<>();
        final CutHandler<T> handler = new CutHandler<T>() {
            @Override
            public void processExcludeObjectApplicability(T object) {
                events.add(CutEvent.newExcludeObjectApplicability(object,
                                                                  applicabilityExtractor.getApplicability(object)));
            }

            @Override
            public void processIncludeObjectApplicability(T object) {
                events.add(CutEvent.newIncludeObjectApplicability(object,
                                                                  applicabilityExtractor.getApplicability(object)));
            }

            @Override
            public void processCutObjectApplicability(T object,
                                                      Expression excludedApplicability,
                                                      Expression includedApplicability) {
                events.add(CutEvent.newCutObjectApplicability(object,
                                                              excludedApplicability,
                                                              includedApplicability));
            }

            @Override
            public void processDegenerateObjectApplicability(T object) {
                events.add(CutEvent.newDegeneratedObjectApplicability(object));
            }
        };
        cut(objects, targetApplicability, applicabilityExtractor, features, handler);
        return events;
    }
}