package cdc.applic.factorization.core;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.Nodes;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.factorization.FactorizationFeatures;
import cdc.applic.factorization.FactorizationFeatures.Hint;
import cdc.applic.factorization.Partitioner;
import cdc.applic.proofs.Prover;
import cdc.applic.simplification.SimplifierFeatures;
import cdc.util.lang.Checks;

public class PartitionerImpl extends AbstractImpl implements Partitioner {
    private static final Logger LOGGER = LogManager.getLogger(PartitionerImpl.class);

    public PartitionerImpl(DictionaryHandle handle,
                           SimplifierFeatures simplifierFeatures) {
        super(handle, simplifierFeatures);
    }

    @Override
    public Result partition(List<Expression> inputs,
                            FactorizationFeatures features) {
        return partition(inputs, Collections.emptyList(), features);
    }

    @Override
    public Result partition(List<Expression> inputs,
                            List<Set<Integer>> inputPartitions,
                            FactorizationFeatures features) {
        LOGGER.debug("partition({}, ...)", inputs);
        final Solver solver = new Solver(inputs, inputPartitions, features);
        solver.solve();
        return solver.result;
    }

    /**
     * Internal class that does the job.
     *
     * @author Damien Carbonne
     */
    class Solver {
        final ResultImpl result;

        /**
         * External knowledge of partitioning.
         * Promise that 2 expressions referenced by a set don't intersect.
         */
        final Collection<Set<Integer>> inputPartitions;
        final FactorizationFeatures features;

        /**
         * This array is recursively filled with all possible combinations
         * of values.
         * <p>
         * When totally filled, cells that contain true are used to construct
         * an intersection, and cells that contain false are used to construct
         * a union.
         * <p>
         * In the end (terminate), we check whether intersection - union is
         * meaningful or not.
         * <p>
         * If 2 cells have indices that belong to the same input partition, we
         * know that intersection is empty and that result is meaningless.
         * In that case, computations can be interrupted.
         */
        final boolean[] array;

        /**
         * Array of BitSets, used when inputPartitions is meaningful.
         * <p>
         * In that case, its size us the number of input expressions.
         * The associated BitSet indicates which partitions set contain the
         * input expression.
         */
        final BitSet[] masks;

        public Solver(List<Expression> inputs,
                      List<Set<Integer>> inputPartitions,
                      FactorizationFeatures features) {
            this.result = new ResultImpl(inputs);
            this.inputPartitions = inputPartitions;
            this.features = features;
            this.array = new boolean[inputs.size()];
            if (inputPartitions.isEmpty()) {
                masks = null;
            } else {
                masks = new BitSet[inputs.size()];
                for (int index = 0; index < masks.length; index++) {
                    masks[index] = new BitSet(inputPartitions.size());
                }
                for (int setIndex = 0; setIndex < inputPartitions.size(); setIndex++) {
                    final Set<Integer> set = inputPartitions.get(setIndex);
                    for (final int inputIndex : set) {
                        // If input index is not in range [0..inputs.size()[,
                        // an exception will be raised.
                        masks[inputIndex].set(setIndex);
                    }
                }
                // Here, we could check that promise is valid
                if (features.isEnabled(Hint.CHECK)) {
                    checkPartitions();
                }
            }
        }

        /**
         * Check that partition promise is correct.
         */
        private void checkPartitions() {
            int count = 0;
            for (final Set<Integer> set : inputPartitions) {
                // The list of expressions of current (supposedly) partition
                final List<Expression> x = new ArrayList<>();
                for (final Integer index : set) {
                    x.add(result.inputs.get(index));
                }
                final boolean valid = prover.alwaysAtMostOne(x.toArray(new Expression[x.size()]));
                if (!valid) {
                    count++;
                    LOGGER.error("Expressions are not disjoint {}", x);
                }
            }
            Checks.assertTrue(count == 0, "Partitionning errors.");
        }

        public void solve() {
            LOGGER.debug("solve()");
            if (!result.inputs.isEmpty()) {
                iterate(0);
                result.compress(prover);
            }
        }

        /**
         * @param index The index of the last cell set to true in array.
         * @return {@code true} if, using external knowledge, there is any hope
         *         that the result will be meaningful (not empty).
         */
        private boolean anyHope(int index) {
            Checks.assertTrue(array[index], "array[index] is not true");
            // Check that any true cell before the one designated by index
            // does not belong to set that contains index
            if (masks != null) {
                // The sets to which index belong
                final BitSet ref = masks[index];
                for (int i = 0; i < index; i++) {
                    if (array[i]) {
                        // A true cell
                        // The sets to which input i belongs
                        final BitSet bs = masks[i];
                        if (ref.intersects(bs)) {
                            // index and i belong to a common partition
                            // (as promised)
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        /**
         * Recursively fill array.
         *
         * @param index The index of array that must be filled,
         *            once with {@code true}, once with {@code false}.
         */
        private void iterate(int index) {
            LOGGER.debug("iterate({}, {}, ...)", array, index);
            if (index == array.length - 1) {
                array[index] = false;
                terminate();
                array[index] = true;
                if (anyHope(index)) {
                    terminate();
                }
            } else {
                array[index] = false;
                iterate(index + 1);
                array[index] = true;
                if (anyHope(index)) {
                    iterate(index + 1);
                }
            }
        }

        /**
         * Called when array is totally filled.
         */
        private void terminate() {
            LOGGER.debug("terminate({}, ...)", array);
            // Intersection of nodes corresponding to true indices.
            Node anded = TrueNode.INSTANCE;
            // Union of nodes corresponding to false indices.
            Node ored = FalseNode.INSTANCE;
            int andedCount = 0;
            for (int index = 0; index < array.length; index++) {
                if (array[index]) {
                    anded = Nodes.andSimplified(anded, result.inputs.get(index).getRootNode());
                    andedCount++;
                } else {
                    ored = Nodes.orSimplified(ored, result.inputs.get(index).getRootNode());
                }
            }
            LOGGER.debug("   inter: {}", anded);
            LOGGER.debug("   union: {}", ored);
            if (andedCount > 0) {
                // Intersection - union
                final Node n = Nodes.and(anded, Nodes.notSimplified(ored));
                if (prover.isSometimesTrue(n)) {
                    final Expression x = simplify(n, features);
                    if (!result.outputs.containsKey(x)) {
                        result.outputs.put(x, new HashSet<>());
                    }
                    // Collect indices of input expressions that play a role in n
                    // All expressions that were used for intersection
                    // and were not excluded by removal union.
                    final Set<Integer> set = result.outputs.get(x);
                    for (int index = 0; index < array.length; index++) {
                        if (array[index]) {
                            // played a role in intersection
                            final Node p = Nodes.and(n, result.inputs.get(index).getRootNode());
                            if (prover.isSometimesTrue(p)) {
                                // was not excluded by removal of union
                                set.add(index);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Implementation, of Result.
     *
     * @author Damien Carbonne
     */
    static class ResultImpl implements Result {
        final List<Expression> inputs = new ArrayList<>();
        final Map<Expression, Set<Integer>> outputs = new HashMap<>();

        public ResultImpl(List<Expression> inputs) {
            this.inputs.addAll(inputs);
        }

        @Override
        public List<Expression> getInputExpressions() {
            return inputs;
        }

        @Override
        public Set<Expression> getOutputExpressions() {
            return outputs.keySet();
        }

        @Override
        public Set<Integer> getInputIndices(Expression output) {
            Checks.containKey(outputs, output, "output");
            return outputs.get(output);
        }

        /**
         * Merges keys that are equivalent.
         *
         * @param prover The prover used to check equivalence.
         */
        public void compress(Prover prover) {
            LOGGER.debug("compress()");
            final Set<Expression> keys = new HashSet<>(outputs.keySet());
            while (!keys.isEmpty()) {
                // pick next key
                final Expression x = keys.iterator().next();
                // remove x from keys to compare
                keys.remove(x);
                // Set of expressions that are equivalent to x
                final Set<Expression> toRemove = new HashSet<>();
                // Compare remaining keys with x
                for (final Expression y : keys) {
                    if (prover.areAlwaysEquivalent(x, y)) {
                        LOGGER.debug("merge {} and {}", x, y);
                        // x and y are equivalent
                        // Transfer indices of y to x
                        outputs.get(x).addAll(outputs.get(y));
                        // Remove y from output
                        outputs.remove(y);
                        // y should be removed from remaining keys
                        toRemove.add(y);
                    }
                }
                // Remove all y keys that are equivalent to x
                // Their indices have been transferred
                keys.removeAll(toRemove);
            }
        }

        @Override
        public void print(PrintStream out,
                          int level) {
            indent(out, level);
            out.println("inputs:");
            for (int index = 0; index < getInputExpressions().size(); index++) {
                indent(out, level + 1);
                out.println(index + ": " + getInputExpressions().get(index));
            }
            out.println("outputs:");
            for (final Expression x : getOutputExpressions()) {
                indent(out, level + 1);
                out.print(x);
                out.print(':');
                for (final int index : getInputIndices(x)) {
                    out.print(" " + index);
                }
                out.println();
            }
        }
    }
}