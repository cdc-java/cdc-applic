package cdc.applic.factorization.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Expressions;
import cdc.applic.factorization.ApplicabilityExtractor;
import cdc.applic.factorization.DefinitionAnalyzer;
import cdc.applic.factorization.FactorizationFeatures;
import cdc.applic.factorization.Merger;
import cdc.applic.factorization.events.MergeEvent;
import cdc.applic.factorization.handlers.MergeHandler;
import cdc.applic.simplification.SimplifierFeatures;
import cdc.util.lang.Checks;

public final class MergerImpl extends AbstractImpl implements Merger {
    private static final Logger LOGGER = LogManager.getLogger(MergerImpl.class);

    public MergerImpl(DictionaryHandle handle,
                      SimplifierFeatures simplifierFeatures) {
        super(handle, simplifierFeatures);
    }

    @Override
    public <T> void merge(List<? extends T> objects,
                          DefinitionAnalyzer<T> analyzer,
                          ApplicabilityExtractor<? super T> extractor,
                          FactorizationFeatures features,
                          MergeHandler<T> handler) {
        LOGGER.debug("merge({})", objects);

        Checks.isNotNull(objects, "objects");
        Checks.isNotNull(analyzer, "analyzer");
        Checks.isNotNull(extractor, "extractor");
        Checks.isNotNull(handler, "handler");

        handler.processBegin();
        // list size
        final int size = objects.size();
        if (size > 1) {
            // hashes of objects.
            // we compute them once.
            final int[] hashes = new int[objects.size()];
            for (int index = 0; index < size; index++) {
                final T object = objects.get(index);
                hashes[index] = analyzer.getDefinitionHash(object);
            }

            // Map from changed objects to their applicability
            final Map<T, Expression> changed = new HashMap<>();
            // map from removed objects to their replacement
            final Map<T, T> removed = new HashMap<>();

            // Compare each pair of objects
            for (int index1 = 0; index1 < size - 1; index1++) {
                final T object1 = objects.get(index1);
                final int hash1 = hashes[index1];
                LOGGER.debug("   object1: {} {}", object1, hash1);
                if (!removed.containsKey(object1)) {
                    for (int index2 = index1 + 1; index2 < size; index2++) {
                        final T object2 = objects.get(index2);
                        final int hash2 = hashes[index2];
                        LOGGER.debug("      object2: {} {}", object2, hash2);
                        if (!removed.containsKey(object2)
                                && hash1 == hash2
                                && analyzer.haveSameDefinition(object1, object2)) {
                            // Merge applicability of object1 and object2 into object1
                            // Keep object 1 and remove object 2
                            LOGGER.debug("         same definition, remove {}", object2);
                            final Expression expression1;
                            final Expression expression2 = extractor.getApplicability(object2);
                            if (changed.containsKey(object1)) {
                                expression1 = changed.get(object1);
                            } else {
                                expression1 = extractor.getApplicability(object1);
                            }
                            changed.put(object1, Expressions.SHORT_NARROW_NO_SIMPLIFY.or(expression1, expression2));
                            removed.put(object2, object1);
                        }
                    }
                }
            }
            if (!changed.isEmpty()) {
                for (final Map.Entry<T, Expression> entry : changed.entrySet()) {
                    final Expression simplified = simplify(entry.getValue(), features);
                    handler.processChangeObjectApplicability(entry.getKey(), simplified);
                }
                for (final Map.Entry<T, T> entry : removed.entrySet()) {
                    handler.processRemoveObject(entry.getKey(), entry.getValue());
                }
            }
        }
        handler.processEnd();
    }

    @Override
    public <T> List<MergeEvent<T>> merge(List<? extends T> objects,
                                         DefinitionAnalyzer<T> analyzer,
                                         ApplicabilityExtractor<? super T> extractor,
                                         FactorizationFeatures features) {
        final List<MergeEvent<T>> events = new ArrayList<>();
        final MergeHandler<T> handler = new MergeHandler<T>() {
            @Override
            public void processChangeObjectApplicability(T object,
                                                         Expression applicability) {
                events.add(MergeEvent.newChangeObjectApplicability(object, applicability));
            }

            @Override
            public void processRemoveObject(T object,
                                            T replacement) {
                events.add(MergeEvent.newRemoveObject(object, replacement));
            }
        };
        merge(objects, analyzer, extractor, features, handler);
        return events;
    }
}