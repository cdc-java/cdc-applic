package cdc.applic.factorization.core;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Expressions;
import cdc.applic.factorization.ApplicabilityExtractor;
import cdc.applic.factorization.FactorizationFeatures;
import cdc.applic.factorization.Splitter;
import cdc.applic.factorization.events.SplitEvent;
import cdc.applic.factorization.handlers.SplitHandler;
import cdc.applic.simplification.SimplifierFeatures;
import cdc.util.lang.Checks;

public final class SplitterImpl extends AbstractImpl implements Splitter {
    private static final Logger LOGGER = LogManager.getLogger(SplitterImpl.class);

    public SplitterImpl(DictionaryHandle handle,
                        SimplifierFeatures simplifierFeatures) {
        super(handle, simplifierFeatures);
    }

    @Override
    public <T> void split(List<? extends T> objects,
                          Expression targetApplicability,
                          ApplicabilityExtractor<? super T> extractor,
                          FactorizationFeatures features,
                          SplitHandler<T> handler) {
        LOGGER.debug("split({}, {})", objects, targetApplicability);
        Checks.isNotNull(targetApplicability, "applicability");
        Checks.isNotNull(objects, "objects");
        Checks.isNotNull(extractor, "extractor");
        Checks.isNotNull(handler, "handler");

        if (!objects.isEmpty()) {
            final List<Expression> alphas = new ArrayList<>();
            for (final T object : objects) {
                alphas.add(extractor.getApplicability(object));
            }
            final boolean disjoint = prover.alwaysAtMostOne(Expression.toNodeArray(alphas));
            Checks.isTrue(disjoint, "Invalid objects: their applicabilities are not disjoint");
        }

        handler.processBegin();

        if (objects.isEmpty()) {
            handler.processCreateObject(targetApplicability);
        } else {
            final Expression notTargetApplicability = Expressions.SHORT_NARROW_NO_SIMPLIFY.not(targetApplicability);
            T reused = null;
            for (final T object : objects) {
                LOGGER.debug("   object: {}", object);
                final Expression applicability = extractor.getApplicability(object);
                final Expression newApplicability = Expressions.SHORT_NARROW_NO_SIMPLIFY.and(applicability, notTargetApplicability);
                LOGGER.debug("      new applicability: {} {}", newApplicability, newApplicability.getRootNode());
                final boolean hasSolutions = prover.isSometimesTrue(newApplicability);
                if (hasSolutions) {
                    LOGGER.debug("      has solution");
                    final boolean disjoint = prover.alwaysAtMostOne(applicability, targetApplicability);
                    LOGGER.debug("      disjoint: {}", disjoint);
                    if (disjoint) {
                        LOGGER.debug("      keep");
                        handler.processKeepObject(object);
                    } else {
                        // Keep this object with its new applicability
                        final Expression simplified = simplify(newApplicability, features);
                        LOGGER.debug("      reduce to: {}", simplified);
                        handler.processReduceObjectApplicability(object, simplified);
                    }
                } else {
                    LOGGER.debug("      has NO solution");
                    if (reused == null) {
                        LOGGER.debug("      reuse with: {}", targetApplicability);
                        handler.processReuseObject(object, targetApplicability);
                        reused = object;
                    } else {
                        // Remove this object
                        LOGGER.debug("      remove");
                        handler.processRemoveObject(object, reused);
                    }
                }
            }
            if (reused == null) {
                handler.processCreateObject(targetApplicability);
            }
        }
        handler.processEnd();
    }

    @Override
    public <T> List<SplitEvent<T>> split(List<? extends T> objects,
                                         Expression targetApplicability,
                                         ApplicabilityExtractor<? super T> extractor,
                                         FactorizationFeatures features) {
        final List<SplitEvent<T>> events = new ArrayList<>();
        final SplitHandler<T> handler = new SplitHandler<T>() {
            @Override
            public void processKeepObject(T object) {
                events.add(SplitEvent.newKeepObject(object));
            }

            @Override
            public void processReduceObjectApplicability(T object,
                                                         Expression applicability) {
                events.add(SplitEvent.newReduceObjectApplicability(object, applicability));
            }

            @Override
            public void processRemoveObject(T object,
                                            T replacement) {
                events.add(SplitEvent.newRemoveObject(object, replacement));
            }

            @Override
            public void processReuseObject(T object,
                                           Expression applicability) {
                events.add(SplitEvent.newReuseObject(object, applicability));
            }

            @Override
            public void processCreateObject(Expression applicability) {
                events.add(SplitEvent.newCreateObject(applicability));
            }

        };
        split(objects, targetApplicability, extractor, features, handler);
        return events;
    }
}