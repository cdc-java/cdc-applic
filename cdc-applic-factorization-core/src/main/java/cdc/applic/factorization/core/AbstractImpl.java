package cdc.applic.factorization.core;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;
import cdc.applic.factorization.FactorizationFeatures;
import cdc.applic.proofs.core.clauses.ProverImpl;
import cdc.applic.simplification.Simplifier;
import cdc.applic.simplification.SimplifierFeatures;
import cdc.applic.simplification.core.SimplifierImpl;
import cdc.util.lang.Checks;

class AbstractImpl {
    private final SimplifierFeatures simplifierFeatures;
    private final Simplifier simplifier;
    protected final ProverImpl prover;

    protected AbstractImpl(DictionaryHandle handle,
                           SimplifierFeatures simplifierFeatures) {
        Checks.isNotNull(handle, "handle");
        Checks.isNotNull(simplifierFeatures, "simplifierFeatures");

        this.simplifierFeatures = simplifierFeatures;
        this.simplifier = new SimplifierImpl(handle);
        this.prover = new ProverImpl(handle, simplifierFeatures.getProverFeatures());
    }

    public final DictionaryHandle getDictionaryHandle() {
        return prover.getDictionaryHandle();
    }

    public final Dictionary getDictionary() {
        return prover.getDictionaryHandle().getDictionary();
    }

    protected final Expression simplify(Node node,
                                        FactorizationFeatures features) {
        if (features.isEnabled(FactorizationFeatures.Hint.SIMPLIFY)) {
            return new Expression(simplifier.simplify(node, simplifierFeatures)
                                            .getValue()
                                            .toInfix(features.getFormatting()));
        } else {
            return new Expression(node.toInfix(features.getFormatting()));
        }
    }

    protected final Expression simplify(Expression expression,
                                        FactorizationFeatures features) {
        return simplify(expression.getRootNode(), features);
    }
}