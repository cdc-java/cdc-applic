package cdc.applic.factorization.core;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.factorization.ApplicabilityExtractor;
import cdc.applic.factorization.Extender;
import cdc.applic.factorization.FactorizationFeatures;
import cdc.applic.factorization.events.ExtendEvent;
import cdc.applic.factorization.handlers.ExtendHandler;
import cdc.applic.simplification.SimplifierFeatures;
import cdc.util.lang.Checks;

public final class ExtenderImpl extends AbstractImpl implements Extender {
    private static final Logger LOGGER = LogManager.getLogger(ExtenderImpl.class);

    public ExtenderImpl(DictionaryHandle handle,
                        SimplifierFeatures simplifierFeatures) {
        super(handle, simplifierFeatures);
    }

    @Override
    public <T> void extend(List<? extends T> objects,
                           Expression extensionApplicability,
                           ApplicabilityExtractor<? super T> applicabilityExtractor,
                           FactorizationFeatures features,
                           ExtendHandler<T> handler) {
        LOGGER.debug("extend({}, {})", objects, extensionApplicability);
        Checks.isNotNull(extensionApplicability, "applicability");
        Checks.isNotNull(objects, "objects");
        Checks.isNotNull(applicabilityExtractor, "applicabilityExtractor");
        Checks.isNotNull(handler, "handler");

        handler.processBegin();
        for (final T object : objects) {
            LOGGER.debug("   object: {}", object);
            final Expression applicability = applicabilityExtractor.getApplicability(object);
            // Checks that the extended applicability is included or not in the
            // object applicability.
            // If it is included, there is no need to extend object applicability
            final boolean included = prover.contains(applicability, extensionApplicability);
            if (!included) {
                final Node newApplicability = new OrNode(applicability.getRootNode(),
                                                         extensionApplicability.getRootNode());
                final Expression simplified = simplify(newApplicability, features);
                handler.processChangeObjectApplicability(object, simplified);
            }
        }
        handler.processEnd();
    }

    @Override
    public <T> List<ExtendEvent<T>> extend(List<? extends T> objects,
                                           Expression extensionApplicability,
                                           ApplicabilityExtractor<? super T> applicabilityExtractor,
                                           FactorizationFeatures features) {
        final List<ExtendEvent<T>> events = new ArrayList<>();
        final ExtendHandler<T> handler =
                (object,
                 applicability) -> events.add(ExtendEvent.newChangeObjectApplicability(object, applicability));
        extend(objects, extensionApplicability, applicabilityExtractor, features, handler);
        return events;
    }
}