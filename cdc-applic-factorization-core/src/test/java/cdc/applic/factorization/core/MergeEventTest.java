package cdc.applic.factorization.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;
import cdc.applic.factorization.events.MergeEvent;

class MergeEventTest {
    @Test
    void test() {
        final String object = "Hello";
        final String replacement = "World";
        final MergeEvent<String> r1a = MergeEvent.newRemoveObject(object, replacement);
        final MergeEvent<String> r1b = MergeEvent.newRemoveObject(object, replacement);
        final MergeEvent<String> r2 = MergeEvent.newRemoveObject(object, object);
        final MergeEvent<String> c1 = MergeEvent.newChangeObjectApplicability(object, Expression.TRUE);
        final MergeEvent<String> c2 = MergeEvent.newChangeObjectApplicability(object, Expression.FALSE);
        final MergeEvent<String> c3 = MergeEvent.newChangeObjectApplicability(replacement, Expression.TRUE);

        assertEquals(r1a, r1a);
        assertEquals(r1a, r1b);
        assertNotEquals(r1a, null);
        assertNotEquals(r1a, "Hello");
        assertNotEquals(r1a, r2);
        assertNotEquals(r1a, c1);
        assertNotEquals(c1, c2);
        assertNotEquals(c1, c3);

        assertEquals(object, r1a.getObject());
        assertEquals(object, c1.getObject());

        assertEquals(r1a.hashCode(), r1b.hashCode());
        assertEquals(r1a.toString(), r1b.toString());

        assertEquals(null, r1a.getApplicability());
        assertEquals(Expression.TRUE, c1.getApplicability());

        assertEquals(replacement, r1a.getReplacement());
        assertEquals(null, c1.getReplacement());
    }
}