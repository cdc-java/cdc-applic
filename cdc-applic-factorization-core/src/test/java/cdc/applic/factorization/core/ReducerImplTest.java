package cdc.applic.factorization.core;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;
import cdc.applic.factorization.FactorizationFeatures;
import cdc.applic.factorization.Reducer;
import cdc.applic.factorization.events.ReduceEvent;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.simplification.SimplifierFeatures;

class ReducerImplTest extends FactorizationSupport {
    private static final Logger LOGGER = LogManager.getLogger(ReducerImplTest.class);
    protected final Reducer reducer;

    public ReducerImplTest() {
        this.reducer = new ReducerImpl(registryHandle,
                                       SimplifierFeatures.builder()
                                                         .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES)
                                                         .allHints()
                                                         .noLimits()
                                                         .build());
    }

    private void check(List<Configured> objects,
                       String targetExpression,
                       List<ReduceEvent<Configured>> expected) {
        LOGGER.debug("==========================================");
        LOGGER.debug("check(" + objects + ", " + targetExpression + ")");
        final List<ReduceEvent<Configured>> events = reducer.reduce(objects,
                                                                    new Expression(targetExpression),
                                                                    extractor,
                                                                    FactorizationFeatures.SIMPLIFY_CHECK_SHORT_NARROW);
        LOGGER.debug("events  : " + events);
        LOGGER.debug("expected: " + expected);
        assertEquals(expected, events);
    }

    @Test
    void testReduceNoObject() {
        check(asList(),
              "true",
              asList());
    }

    @Test
    void testReduceOneObjectSmaller() {
        final Configured object1 = newObject(1, "true", null, null);

        check(asList(object1),
              "Version=V1",
              asList(ReduceEvent.newChangeObjectApplicability(object1, new Expression("Version=V1"))));
    }

    @Test
    void testReduceOneObjectSame() {
        final Configured object1 = newObject(1, "true", null, null);

        check(asList(object1),
              "true",
              asList());
    }

    @Test
    void testReduceOneObjectGreater() {
        final Configured object1 = newObject(1, "Version=V1", null, null);

        check(asList(object1),
              "true",
              asList());
    }
}