package cdc.applic.factorization.core;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;
import cdc.applic.factorization.Extender;
import cdc.applic.factorization.FactorizationFeatures;
import cdc.applic.factorization.events.ExtendEvent;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.simplification.SimplifierFeatures;

class ExtenderImplTest extends FactorizationSupport {
    private static final Logger LOGGER = LogManager.getLogger(ExtenderImplTest.class);
    protected final Extender extender;

    public ExtenderImplTest() {
        super();
        this.extender = new ExtenderImpl(registryHandle,
                                         SimplifierFeatures.builder()
                                                           .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES)
                                                           .allHints()
                                                           .noLimits()
                                                           .build());
    }

    private void check(List<Configured> objects,
                       String targetExpression,
                       List<ExtendEvent<Configured>> expected) {
        LOGGER.debug("==========================================");
        LOGGER.debug("check(" + objects + ", " + targetExpression + ")");
        final List<ExtendEvent<Configured>> events = extender.extend(objects,
                                                                     new Expression(targetExpression),
                                                                     extractor,
                                                                     FactorizationFeatures.SIMPLIFY_CHECK_SHORT_NARROW);
        LOGGER.debug("events  : " + events);
        LOGGER.debug("expected: " + expected);
        assertEquals(expected, events);
    }

    @Test
    void testExtendOneObjectSmaller() {
        final Configured object1 = newObject(1, "true", null, null);

        check(asList(object1),
              "Version=V1",
              asList());
    }

    @Test
    void testExtendOneObjectSame() {
        final Configured object1 = newObject(1, "true", null, null);

        check(asList(object1),
              "true",
              asList());
    }

    @Test
    void testExtendOneObjectGreater() {
        final Configured object1 = newObject(1, "Version=V1", null, null);

        check(asList(object1),
              "true",
              asList(ExtendEvent.newChangeObjectApplicability(object1, new Expression("true"))));
    }
}