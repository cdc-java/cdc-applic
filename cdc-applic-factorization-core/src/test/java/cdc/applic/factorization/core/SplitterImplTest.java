package cdc.applic.factorization.core;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;
import cdc.applic.factorization.FactorizationFeatures;
import cdc.applic.factorization.Splitter;
import cdc.applic.factorization.events.SplitEvent;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.simplification.SimplifierFeatures;

class SplitterImplTest extends FactorizationSupport {
    private static final Logger LOGGER = LogManager.getLogger(SplitterImplTest.class);
    protected final Splitter splitter;

    public SplitterImplTest() {
        this.splitter = new SplitterImpl(registryHandle,
                                         SimplifierFeatures.builder()
                                                           .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES)
                                                           .allHints()
                                                           .noLimits()
                                                           .build());
    }

    private void check(List<Configured> objects,
                       String targetExpression,
                       List<SplitEvent<Configured>> expected) {
        LOGGER.debug("==========================================");
        LOGGER.debug("check(" + objects + ", " + targetExpression + ")");
        final List<SplitEvent<Configured>> events = splitter.split(objects,
                                                                   new Expression(targetExpression),
                                                                   extractor,
                                                                   FactorizationFeatures.SIMPLIFY_CHECK_SHORT_NARROW);
        LOGGER.debug("events  : " + events);
        LOGGER.debug("expected: " + expected);
        assertEquals(expected, events);
    }

    @Test
    void testSplitNoObject() {
        check(asList(),
              "Version=V1",
              asList(SplitEvent.newCreateObject(new Expression("Version=V1"))));
    }

    @Test
    void testSplitNoDisjoint() {
        assertThrows(IllegalArgumentException.class, () -> {
            final Configured object1 = newObject(1, "true", null, null);
            final Configured object2 = newObject(2, "true", null, null);
            check(asList(object1, object2),
                  "Version=V1",
                  asList(SplitEvent.newCreateObject(new Expression("Version=V1"))));
        });
    }

    @Test
    void testSplitOneObjectSmaller() {
        final Configured object1 = newObject(1, "true", null, null);

        check(asList(object1),
              "Version=V1",
              asList(SplitEvent.newReduceObjectApplicability(object1, new Expression("Version<:{V2,V3,V4,V5,V6,V7}")),
                     SplitEvent.newCreateObject(new Expression("Version=V1"))));
    }

    @Test
    void testSplitOneObjectSame() {
        final Configured object1 = newObject(1, "true", null, null);

        check(asList(object1),
              "true",
              asList(SplitEvent.newReuseObject(object1, new Expression("true"))));
    }

    @Test
    void testSplitOneObjectGreater1() {
        final Configured object1 = newObject(1, "Version=V1", null, null);

        check(asList(object1),
              "true",
              asList(SplitEvent.newReuseObject(object1, new Expression("true"))));
    }

    @Test
    void testSplitOneObjectGreater2() {
        final Configured object1 = newObject(1, "Version=V1", null, null);

        check(asList(object1),
              "Version<:{V1,V2}",
              asList(SplitEvent.newReuseObject(object1, new Expression("Version<:{V1,V2}"))));
    }

    @Test
    void testSplitTwoObjectsSmaller() {
        final Configured object1 = newObject(1, "Version=V1", null, null);
        final Configured object2 = newObject(2, "Version<:{V2,V3}", null, null);

        check(asList(object1, object2),
              "Version=V2",
              asList(SplitEvent.newKeepObject(object1),
                     SplitEvent.newReduceObjectApplicability(object2, new Expression("Version=V3")),
                     SplitEvent.newCreateObject(new Expression("Version=V2"))));
    }

    @Test
    void testSplitTwoObjectsSame() {
        final Configured object1 = newObject(1, "Version=V1", null, null);
        final Configured object2 = newObject(2, "Version<:{V2,V3}", null, null);

        check(asList(object1, object2),
              "Version=V1",
              asList(SplitEvent.newReuseObject(object1, new Expression("Version=V1")),
                     SplitEvent.newKeepObject(object2)));
    }

    @Test
    void testSplitTwoObjectsGreater() {
        final Configured object1 = newObject(1, "Version=V1", null, null);
        final Configured object2 = newObject(2, "Version<:{V2,V3}", null, null);

        check(asList(object1, object2),
              "Version<:{V1,V4}",
              asList(SplitEvent.newReuseObject(object1, new Expression("Version<:{V1,V4}")),
                     SplitEvent.newKeepObject(object2)));
    }
}