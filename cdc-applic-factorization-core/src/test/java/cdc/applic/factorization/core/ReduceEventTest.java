package cdc.applic.factorization.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;
import cdc.applic.factorization.events.ReduceEvent;

class ReduceEventTest {
    @Test
    void test() {
        final String object = "Hello";
        final ReduceEvent<String> r1a = ReduceEvent.newRemoveObject(object);
        final ReduceEvent<String> r1b = ReduceEvent.newRemoveObject(object);
        final ReduceEvent<String> r2 = ReduceEvent.newRemoveObject("World");
        final ReduceEvent<String> c1 = ReduceEvent.newChangeObjectApplicability(object, Expression.TRUE);
        final ReduceEvent<String> c2 = ReduceEvent.newChangeObjectApplicability(object, Expression.FALSE);

        assertEquals(r1a, r1a);
        assertEquals(r1a, r1b);
        assertNotEquals(r1a, null);
        assertNotEquals(r1a, "Hello");
        assertNotEquals(r1a, r2);
        assertNotEquals(r1a, c1);
        assertNotEquals(c1, c2);

        assertEquals(object, r1a.getObject());
        assertEquals(object, c1.getObject());

        assertEquals(null, r1a.getApplicability());
        assertEquals(Expression.TRUE, c1.getApplicability());

        assertEquals(r1a.hashCode(), r1b.hashCode());
        assertEquals(r1a.toString(), r1b.toString());
    }
}