package cdc.applic.factorization.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;
import cdc.applic.factorization.events.SplitEvent;

class SplitEventTest {
    @Test
    void test() {
        final String object = "Hello";
        final String replacement = "World";
        final SplitEvent<String> remove1a = SplitEvent.newRemoveObject(object, replacement);
        final SplitEvent<String> remove1b = SplitEvent.newRemoveObject(object, replacement);
        final SplitEvent<String> remove2 = SplitEvent.newRemoveObject(object, object);
        final SplitEvent<String> remove3 = SplitEvent.newRemoveObject(replacement, replacement);
        final SplitEvent<String> keep1 = SplitEvent.newKeepObject(object);
        final SplitEvent<String> create1 = SplitEvent.newCreateObject(Expression.TRUE);
        final SplitEvent<String> reduce1 = SplitEvent.newReduceObjectApplicability(object, Expression.TRUE);
        final SplitEvent<String> reuse1 = SplitEvent.newReuseObject(object, Expression.TRUE);
        final SplitEvent<String> reuse2 = SplitEvent.newReuseObject(object, Expression.FALSE);

        assertEquals(remove1a, remove1a);
        assertEquals(remove1a, remove1b);
        assertNotEquals(remove1a, null);
        assertNotEquals(remove1a, "Hello");
        assertNotEquals(remove1a, keep1);
        assertNotEquals(remove1a, remove2);
        assertNotEquals(remove1a, remove3);
        assertNotEquals(reuse1, reuse2);

        assertEquals(remove1a.hashCode(), remove1b.hashCode());
        assertEquals(remove1a.toString(), remove1b.toString());

        assertEquals(SplitEvent.Type.CREATE_OBJECT, create1.getType());
        assertEquals(SplitEvent.Type.KEEP_OBJECT, keep1.getType());
        assertEquals(SplitEvent.Type.REDUCE_OBJECT_APPLICABILITY, reduce1.getType());
        assertEquals(SplitEvent.Type.REMOVE_OBJECT, remove1a.getType());
        assertEquals(SplitEvent.Type.REUSE_OBJECT, reuse1.getType());

        assertEquals(object, remove1a.getObject());
        assertEquals(object, keep1.getObject());
        assertEquals(null, create1.getObject());
        assertEquals(object, reduce1.getObject());
        assertEquals(object, reuse1.getObject());

        assertEquals(null, remove1a.getApplicability());
        assertEquals(null, keep1.getApplicability());
        assertEquals(Expression.TRUE, create1.getApplicability());
        assertEquals(Expression.TRUE, reduce1.getApplicability());
        assertEquals(Expression.TRUE, reuse1.getApplicability());

        assertEquals(replacement, remove1a.getReplacement());
        assertEquals(null, keep1.getReplacement());
        assertEquals(null, create1.getReplacement());
        assertEquals(null, reduce1.getReplacement());
        assertEquals(null, reuse1.getReplacement());
    }
}