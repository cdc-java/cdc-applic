package cdc.applic.factorization.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;
import cdc.applic.factorization.events.ExtendEvent;

class ExtendEventTest {
    @Test
    void test() {
        final String object = "Hello";
        final ExtendEvent<String> c1a = ExtendEvent.newChangeObjectApplicability(object, Expression.TRUE);
        final ExtendEvent<String> c1b = ExtendEvent.newChangeObjectApplicability(object, Expression.TRUE);
        final ExtendEvent<String> c2 = ExtendEvent.newChangeObjectApplicability(object, Expression.FALSE);
        final ExtendEvent<String> c3 = ExtendEvent.newChangeObjectApplicability("World", Expression.FALSE);
        final ExtendEvent<String> i1 = ExtendEvent.newIgnoreObject(object);

        assertEquals(c1a, c1a);
        assertEquals(c1a, c1b);
        assertNotEquals(c1a, i1);
        assertNotEquals(c1a, null);
        assertNotEquals(c1a, "Hello");
        assertNotEquals(c1a, c2);
        assertNotEquals(c1a, c3);

        assertEquals(ExtendEvent.Type.CHANGE_OBJECT_APPLICABILITY, c1a.getType());
        assertEquals(ExtendEvent.Type.IGNORE_OBJECT, i1.getType());

        assertEquals(c1a.hashCode(), c1b.hashCode());
        assertEquals(c1a.toString(), c1b.toString());

        assertEquals(object, c1a.getObject());
        assertEquals(object, i1.getObject());
        assertEquals(Expression.TRUE, c1a.getApplicability());
        assertEquals(null, i1.getApplicability());
    }
}