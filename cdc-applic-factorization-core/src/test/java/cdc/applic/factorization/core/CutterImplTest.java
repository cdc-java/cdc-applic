package cdc.applic.factorization.core;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;
import cdc.applic.factorization.Cutter;
import cdc.applic.factorization.FactorizationFeatures;
import cdc.applic.factorization.events.CutEvent;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.simplification.SimplifierFeatures;

class CutterImplTest extends FactorizationSupport {
    private static final Logger LOGGER = LogManager.getLogger(CutterImplTest.class);
    protected final Cutter cutter;

    public CutterImplTest() {
        this.cutter = new CutterImpl(registryHandle,
                                     SimplifierFeatures.builder()
                                                       .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES)
                                                       .allHints()
                                                       .noLimits()
                                                       .build());
    }

    private void check(List<Configured> objects,
                       String targetExpression,
                       List<CutEvent<Configured>> expected) {
        LOGGER.debug("==========================================");
        LOGGER.debug("check(" + objects + ", " + targetExpression + ")");
        final List<CutEvent<Configured>> events = cutter.cut(objects,
                                                             new Expression(targetExpression),
                                                             extractor,
                                                             FactorizationFeatures.SIMPLIFY_CHECK_SHORT_NARROW);
        LOGGER.debug("events  : " + events);
        LOGGER.debug("expected: " + expected);
        assertEquals(expected, events);
    }

    @Test
    void testEmpty() {
        check(asList(),
              "Version=V1",
              asList());
    }

    @Test
    void testCut() {
        final Configured object = newObject(1, "true");

        check(asList(object),
              "Version=V1",
              asList(CutEvent.newCutObjectApplicability(object, x("Version<:{V2,V3,V4,V5,V6,V7}"), x("Version=V1"))));
    }

    @Test
    void testInclude() {
        final Configured object = newObject(1, "Version=V1");

        check(asList(object),
              "Version=V1",
              asList(CutEvent.newIncludeObjectApplicability(object, x("Version=V1"))));
    }

    @Test
    void testExclude() {
        final Configured object = newObject(1, "Version=V2");

        check(asList(object),
              "Version=V1",
              asList(CutEvent.newExcludeObjectApplicability(object, x("Version=V2"))));
    }

    @Test
    void testDegenerated() {
        final Configured object = newObject(1, "false");

        check(asList(object),
              "Version=V1",
              asList(CutEvent.newDegeneratedObjectApplicability(object)));
    }
}