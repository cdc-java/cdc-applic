package cdc.applic.factorization.core;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;
import cdc.applic.factorization.FactorizationFeatures;
import cdc.applic.factorization.Merger;
import cdc.applic.factorization.events.MergeEvent;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.simplification.SimplifierFeatures;

class MergerImplTest extends FactorizationSupport {
    private static final Logger LOGGER = LogManager.getLogger(MergerImplTest.class);
    protected final Merger merger;

    MergerImplTest() {
        this.merger = new MergerImpl(registryHandle,
                                     SimplifierFeatures.builder()
                                                       .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES)
                                                       .allHints()
                                                       .noLimits()
                                                       .build());
    }

    private void check(List<Configured> objects,
                       List<MergeEvent<Configured>> expected) {
        LOGGER.debug("==========================================");
        LOGGER.debug("check(" + objects + ")");
        final List<MergeEvent<Configured>> events = merger.merge(objects,
                                                                 analyzer,
                                                                 extractor,
                                                                 FactorizationFeatures.SIMPLIFY_CHECK_SHORT_NARROW);
        LOGGER.debug("events  : " + events);
        LOGGER.debug("expected: " + expected);
        assertEquals(expected, events);
    }

    @Test
    void testMergeNoObject() {
        check(asList(),
              asList());
    }

    @Test
    void testMergeOneObject() {
        final Configured object1 = newObject(1, "Version=V1", null, null);
        check(asList(object1),
              asList());
    }

    @Test
    void testMergeTwoObjectsDifferentDefDisjointApp() {
        final Configured object1 = newObject(1, "Version=V1", "Hello", null);
        final Configured object2 = newObject(2, "Version=V2", null, null);
        check(asList(object1, object2),
              asList());
    }

    @Test
    void testMergeTwoObjectsSameDefDisjointApp() {
        final Configured object1 = newObject(1, "Version=V1", null, null);
        final Configured object2 = newObject(2, "Version=V2", null, null);
        check(asList(object1, object2),
              asList(MergeEvent.newChangeObjectApplicability(object1, new Expression("Version<:{V1,V2}")),
                     MergeEvent.newRemoveObject(object2, object1)));
    }
}