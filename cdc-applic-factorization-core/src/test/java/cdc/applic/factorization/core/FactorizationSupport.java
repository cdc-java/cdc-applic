package cdc.applic.factorization.core;

import java.util.Objects;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.Expression;
import cdc.applic.factorization.ApplicabilityExtractor;
import cdc.applic.factorization.DefinitionAnalyzer;

class FactorizationSupport {
    protected final RepositoryImpl repository = new RepositoryImpl();
    protected final RegistryImpl registry = repository.registry().name("Registry").build();
    protected final DictionaryHandle registryHandle = new DictionaryHandle(registry);
    protected final ApplicabilityExtractor<Configured> extractor = new ConfiguredApplicabilityExtractor();
    protected final DefinitionAnalyzer<Configured> analyzer = new ConfiguredDefinitionComparator();

    protected static Expression x(String expression) {
        return new Expression(expression);
    }

    protected static interface Configured {
        public int getId();

        public Expression getApplicability();

        public void setApplicability(Expression applicability);

        public String getValue1();

        public void setValue1(String value);

        public String getValue2();

        public void setValue2(String value);
    }

    protected static class ConfiguredImpl implements Configured {
        private final int id;
        private Expression applicability = Expression.TRUE;
        private String value1 = null;
        private String value2 = null;

        @Override
        public int getId() {
            return id;
        }

        public ConfiguredImpl(int id) {
            this.id = id;
        }

        @Override
        public Expression getApplicability() {
            return applicability;
        }

        @Override
        public void setApplicability(Expression applicability) {
            this.applicability = applicability;
        }

        @Override
        public String getValue1() {
            return value1;
        }

        @Override
        public void setValue1(String value) {
            this.value1 = value;
        }

        @Override
        public String getValue2() {
            return value2;
        }

        @Override
        public void setValue2(String value) {
            this.value2 = value;
        }

        @Override
        public String toString() {
            return "[" + getId() + ", " + getValue1() + ", " + getValue2() + ", " + getApplicability() + "]";
        }
    }

    protected static ConfiguredImpl newObject(int id,
                                              String applicability,
                                              String value1,
                                              String value2) {
        final ConfiguredImpl result = new ConfiguredImpl(id);
        result.setApplicability(new Expression(applicability));
        result.setValue1(value1);
        result.setValue2(value2);
        return result;
    }

    protected static ConfiguredImpl newObject(int id,
                                              String applicability) {
        return newObject(id, applicability, null, null);
    }

    protected static class ConfiguredApplicabilityExtractor implements ApplicabilityExtractor<Configured> {
        public ConfiguredApplicabilityExtractor() {
            super();
        }

        @Override
        public Expression getApplicability(Configured object) {
            return object.getApplicability();
        }
    }

    protected static class ConfiguredDefinitionComparator implements DefinitionAnalyzer<Configured> {
        public ConfiguredDefinitionComparator() {
            super();
        }

        @Override
        public int getDefinitionHash(Configured object) {
            return Objects.hash(object.getValue1(),
                                object.getValue2());
        }

        @Override
        public boolean haveSameDefinition(Configured object1,
                                          Configured object2) {
            return Objects.equals(object1.getValue1(), object2.getValue1())
                    && Objects.equals(object1.getValue2(), object2.getValue2());
        }
    }

    FactorizationSupport() {
        registry.enumeratedType().name("Service").frozen(false).literals("S1", "S2", "S3", "S4").build();
        registry.enumeratedType().name("Version").frozen(false).literals("V1", "V2", "V3", "V4", "V5", "V6", "V7").build();
        registry.integerType().name("Rank").frozen(true).domain("1~999").build();

        registry.property().name("Service").type("Service").ordinal(0).build();
        registry.property().name("Version").type("Version").ordinal(1).build();
        registry.property().name("Rank").type("Rank").ordinal(2).build();

        registry.createAssertion("Service = S1 <-> Version = V1");
        registry.createAssertion("Service = S2 <-> Version in {V2,V3}");
        registry.createAssertion("Service = S3 <-> Version in {V4,V5}");
        registry.createAssertion("Service = S4 <-> Version in {V6,V7}");

        registry.createAssertion("Version = V1 -> Rank in {1~50}");
        registry.createAssertion("Version = V2 -> Rank in {1~200}");
        registry.createAssertion("Version = V3 -> Rank in {1~100}");
        registry.createAssertion("Version = V4 -> Rank in {1~100}");
        registry.createAssertion("Version = V5 -> Rank in {1~100}");
        registry.createAssertion("Version = V6 -> Rank in {1~100}");
        registry.createAssertion("Version = V7 -> Rank in {1~100}");
    }
}