package cdc.applic.factorization.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;
import cdc.applic.factorization.events.CutEvent;

class CutEventTest {
    @Test
    void test() {
        final String object = "Hello";
        final CutEvent<String> d1a = CutEvent.newDegeneratedObjectApplicability(object);
        final CutEvent<String> d1b = CutEvent.newDegeneratedObjectApplicability(object);
        final CutEvent<String> d2 = CutEvent.newDegeneratedObjectApplicability("World");
        final CutEvent<String> c1 = CutEvent.newCutObjectApplicability(object, Expression.FALSE, Expression.TRUE);
        final CutEvent<String> c2 = CutEvent.newCutObjectApplicability(object, Expression.FALSE, Expression.FALSE);
        final CutEvent<String> c3 = CutEvent.newCutObjectApplicability(object, Expression.TRUE, Expression.FALSE);
        final CutEvent<String> e1 = CutEvent.newExcludeObjectApplicability(object, Expression.TRUE);
        final CutEvent<String> i1 = CutEvent.newIncludeObjectApplicability(object, Expression.TRUE);

        assertEquals(d1a, d1a);
        assertEquals(d1a, d1b);
        assertNotEquals(d1a, c1);
        assertNotEquals(d1a, e1);
        assertNotEquals(d1a, i1);
        assertNotEquals(d1a, null);
        assertNotEquals(d1a, "Hello");
        assertNotEquals(d1a, d2);
        assertNotEquals(c1, c2);
        assertNotEquals(c1, c3);

        assertEquals(d1a.hashCode(), d1b.hashCode());
        assertEquals(d1a.toString(), d1b.toString());

        assertEquals(CutEvent.Type.DEGENERATE_OBJECT_APPLICABILITY, d1a.getType());
        assertEquals(object, d1a.getObject());
        assertEquals(Expression.FALSE, d1a.getIncludedApplicability());
        assertEquals(Expression.FALSE, d1a.getExcludedApplicability());

        assertEquals(CutEvent.Type.CUT_OBJECT_APPLICABILITY, c1.getType());
        assertEquals(object, c1.getObject());
        assertEquals(Expression.TRUE, c1.getIncludedApplicability());
        assertEquals(Expression.FALSE, c1.getExcludedApplicability());

        assertEquals(CutEvent.Type.EXCLUDE_OBJECT_APPLICABILITY, e1.getType());
        assertEquals(object, e1.getObject());
        assertEquals(Expression.FALSE, e1.getIncludedApplicability());
        assertEquals(Expression.TRUE, e1.getExcludedApplicability());

        assertEquals(CutEvent.Type.INCLUDE_OBJECT_APPLICABILITY, i1.getType());
        assertEquals(object, i1.getObject());
        assertEquals(Expression.TRUE, i1.getIncludedApplicability());
        assertEquals(Expression.FALSE, i1.getExcludedApplicability());
    }
}