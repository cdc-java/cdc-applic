package cdc.applic.factorization.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;
import cdc.applic.factorization.FactorizationFeatures;
import cdc.applic.factorization.Partitioner;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.simplification.SimplifierFeatures;

class PartitionerImplTest extends FactorizationSupport {
    private static final Logger LOGGER = LogManager.getLogger(PartitionerImplTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    protected final Partitioner partitioner;

    private static Set<Integer> s(int... values) {
        final Set<Integer> set = new HashSet<>();
        for (final int value : values) {
            set.add(value);
        }
        return set;
    }

    public PartitionerImplTest() {
        this.partitioner = new PartitionerImpl(registryHandle,
                                               SimplifierFeatures.builder()
                                                                 .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES)
                                                                 .allHints()
                                                                 .noLimits()
                                                                 .build());
    }

    @Test
    void test0() {
        final List<Expression> inputs = new ArrayList<>();
        final Partitioner.Result result =
                partitioner.partition(inputs,
                                      FactorizationFeatures.SIMPLIFY_CHECK_SHORT_NARROW);
        result.print(OUT);
        assertEquals(inputs, result.getInputExpressions());
        assertTrue(result.getOutputExpressions().isEmpty());
    }

    @Test
    void test1() {
        final List<Expression> inputs = new ArrayList<>();
        inputs.add(x("Version = V1"));
        final Partitioner.Result result =
                partitioner.partition(inputs,
                                      FactorizationFeatures.SIMPLIFY_CHECK_SHORT_NARROW);
        result.print(OUT);
        assertEquals(inputs, result.getInputExpressions());
        assertSame(1, result.getOutputExpressions().size());
        assertEquals(s(0), result.getInputIndices(x("Version=V1")));
    }

    @Test
    void test2A() {
        final List<Expression> inputs = new ArrayList<>();
        inputs.add(x("Version = V1"));
        inputs.add(x("Version = V1"));
        final Partitioner.Result result =
                partitioner.partition(inputs,
                                      FactorizationFeatures.SIMPLIFY_CHECK_SHORT_NARROW);
        result.print(OUT);
        assertEquals(inputs, result.getInputExpressions());
        assertSame(1, result.getOutputExpressions().size());
        assertEquals(s(0, 1), result.getInputIndices(x("Version=V1")));
    }

    @Test
    void test2B() {
        final List<Expression> inputs = new ArrayList<>();
        inputs.add(x("Version = V1"));
        inputs.add(x("Version = V2"));
        final Partitioner.Result result =
                partitioner.partition(inputs,
                                      FactorizationFeatures.SIMPLIFY_CHECK_SHORT_NARROW);
        result.print(OUT);
        assertEquals(inputs, result.getInputExpressions());
        assertSame(2, result.getOutputExpressions().size());
        assertEquals(s(0), result.getInputIndices(x("Version=V1")));
        assertEquals(s(1), result.getInputIndices(x("Version=V2")));
    }

    @Test
    void test2C() {
        final List<Expression> inputs = new ArrayList<>();
        inputs.add(x("Version<:{V1,V2}"));
        inputs.add(x("Version<:{V2,V3}"));
        final Partitioner.Result result =
                partitioner.partition(inputs,
                                      FactorizationFeatures.SIMPLIFY_CHECK_SHORT_NARROW);
        result.print(OUT);
        assertEquals(inputs, result.getInputExpressions());
        assertSame(3, result.getOutputExpressions().size());
        assertEquals(s(0), result.getInputIndices(x("Version=V1")));
        assertEquals(s(0, 1), result.getInputIndices(x("Version=V2")));
        assertEquals(s(1), result.getInputIndices(x("Version=V3")));
    }

    @Test
    void testHints() {
        final List<Expression> inputs = new ArrayList<>();
        inputs.add(x("Version = V1"));
        inputs.add(x("Version = V2"));
        inputs.add(x("Version = V3"));
        inputs.add(x("Version<:{V1,V2}"));
        inputs.add(x("Version<:{V3,V4}"));
        final List<Set<Integer>> hints = new ArrayList<>();
        hints.add(s(0, 1, 2));
        hints.add(s(3, 4));
        final Partitioner.Result result =
                partitioner.partition(inputs,
                                      hints,
                                      FactorizationFeatures.SIMPLIFY_CHECK_SHORT_NARROW);
        result.print(OUT);
        assertEquals(inputs, result.getInputExpressions());
        assertSame(4, result.getOutputExpressions().size());
        assertEquals(s(0, 3), result.getInputIndices(x("Version=V1")));
        assertEquals(s(1, 3), result.getInputIndices(x("Version=V2")));
        assertEquals(s(2, 4), result.getInputIndices(x("Version=V3")));
        assertEquals(s(4), result.getInputIndices(x("Version=V4")));
    }
}