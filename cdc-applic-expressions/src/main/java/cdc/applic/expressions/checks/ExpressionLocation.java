package cdc.applic.expressions.checks;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;
import cdc.issues.locations.AbstractLocation;

public class ExpressionLocation extends AbstractLocation {
    private final String context;
    private final String expression;
    private final int beginIndex;
    private final int endIndex;

    public static final String TAG = "ExpressionLocation";

    public ExpressionLocation(String context,
                              String expression,
                              int beginIndex,
                              int endIndex) {
        this.context = context;
        this.expression = expression;
        this.beginIndex = beginIndex;
        this.endIndex = endIndex;
    }

    public ExpressionLocation(String context,
                              String expression) {
        this(context,
             expression,
             0,
             -1);
    }

    public ExpressionLocation(String context,
                              Expression expression) {
        this(context,
             expression.getContent());
    }

    public ExpressionLocation(String context,
                              Node node) {
        this(context,
             node.compress());
    }

    public String getContext() {
        return context;
    }

    public final String getExpression() {
        return expression;
    }

    public final int getBeginIndex() {
        return beginIndex;
    }

    public final int getEndIndex() {
        return endIndex;
    }

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    public String getPath() {
        return context == null
                ? expression
                : context + ":" + expression;
    }

    @Override
    public String getAnchor() {
        if (beginIndex >= 0) {
            if (endIndex >= 0) {
                return beginIndex + "-" + endIndex;
            } else if (beginIndex > 0) {
                return beginIndex + "-";
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return super.equals(object);
    }
}