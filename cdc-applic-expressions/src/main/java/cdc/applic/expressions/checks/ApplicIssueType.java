package cdc.applic.expressions.checks;

import cdc.issues.IssueSeverity;
import cdc.issues.IssueSeverityItem;
import cdc.util.lang.Checks;

/**
 * Enumeration of issue types.
 * <p>
 * <b>WARNING:</b> order of declarations matters.
 *
 * @author Damien Carbonne
 */
public enum ApplicIssueType implements IssueSeverityItem {
    /**
     * No issue was detected.
     */
    NO_ISSUE,

    /**
     * The expression does not respect writing rules.
     */
    WRITING_ISSUE,

    /**
     * The expression contains a semantic error.
     * <p>
     * The expression is lexically and syntactically correct.<br>
     * It is usually fatal.
     */
    SEMANTIC_ISSUE,

    /**
     * The expression contains a syntactic error.
     * <p>
     * The expression is lexically correct.<br>
     * It is always fatal.
     */
    SYNTACTIC_ISSUE,

    /**
     * The expression contains a lexical error.
     * <p>
     * This is the most severe level.<br>
     * It is always fatal.
     */
    LEXICAL_ISSUE,

    /**
     * The dictionary contains a fatal issue.
     */
    DICTIONARY_ISSUE;

    @Override
    public IssueSeverity getSeverity() {
        if (this == NO_ISSUE) {
            return IssueSeverity.INFO;
        } else if (this == WRITING_ISSUE) {
            return IssueSeverity.MINOR;
        } else {
            return IssueSeverity.CRITICAL;
        }
    }

    /**
     * Returns The most severe type.
     *
     * @param left The first type.
     * @param right The second type.
     * @return The most sever type among {@code left} and {@code right}.
     */
    public static ApplicIssueType mostSevere(ApplicIssueType left,
                                             ApplicIssueType right) {
        Checks.isNotNull(left, "left");
        Checks.isNotNull(right, "right");

        final int ordinal = Math.max(left.ordinal(), right.ordinal());
        return ApplicIssueType.values()[ordinal];
    }
}