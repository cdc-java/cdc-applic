package cdc.applic.expressions.checks;

import cdc.applic.expressions.LexicalException;

/**
 * Specialization of Issue dedicated to Lexical problems.
 * <p>
 * They are associated to a {@link LexicalException}.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-21", forRemoval = true)
public final class LexicalIssue extends ApplicIssue {
    private LexicalIssue(Builder builder) {
        super(builder.name(ApplicIssueType.LEXICAL_ISSUE));
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public LexicalIssue(String context,
                        LexicalException cause) {
        this(builder().location(new ExpressionLocation(context,
                                                       cause.getExpression(),
                                                       cause.getBeginIndex(),
                                                       cause.getEndIndex()))
                      .description(cause.getFullMessage())
                      .cause(cause));
        // super(ApplicIssueType.LEXICAL_ISSUE,
        // null,
        // new ExpressionLocation(context,
        // cause.getExpression(),
        // cause.getBeginIndex(),
        // cause.getEndIndex()),
        // cause.getFullMessage(),
        // cause);
    }

    @Override
    @Deprecated(since = "2024-12-21", forRemoval = true)
    public LexicalException getCause() {
        return (LexicalException) super.getCause();
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public LexicalException.Detail getDetail() {
        return getCause().getDetail();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends ApplicIssue.Builder<Builder> {
        private Builder() {
        }

        public Builder cause(String context,
                             LexicalException cause) {
            return super.cause(context, cause);
        }

        @Override
        public LexicalIssue build() {
            return new LexicalIssue(this);
        }
    }
}