package cdc.applic.expressions.checks;

import cdc.applic.expressions.SyntacticException;

/**
 * Specialization of Issue dedicated to Syntax problems.
 * <p>
 * They are associated to a {@link SyntacticException}.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-21", forRemoval = true)
public final class SyntaxIssue extends ApplicIssue {
    private SyntaxIssue(Builder builder) {
        super(builder.name(ApplicIssueType.SYNTACTIC_ISSUE));
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public SyntaxIssue(String context,
                       SyntacticException cause) {
        this(builder().location(new ExpressionLocation(context,
                                                       cause.getExpression(),
                                                       cause.getBeginIndex(),
                                                       cause.getEndIndex()))
                      .description(cause.getFullMessage())
                      .cause(cause));
        // super(ApplicIssueType.SYNTACTIC_ISSUE,
        // null,
        // new ExpressionLocation(context,
        // cause.getExpression(),
        // cause.getBeginIndex(),
        // cause.getEndIndex()),
        // cause.getFullMessage(),
        // cause);
    }

    @Override
    @Deprecated(since = "2024-12-21", forRemoval = true)
    public SyntacticException getCause() {
        return (SyntacticException) super.getCause();
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public SyntacticException.Detail getDetail() {
        return getCause().getDetail();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends ApplicIssue.Builder<Builder> {
        private Builder() {
        }

        public Builder cause(String context,
                             SyntacticException cause) {
            return super.cause(context, cause);
        }

        @Override
        public SyntaxIssue build() {
            return new SyntaxIssue(this);
        }
    }
}