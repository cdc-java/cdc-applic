package cdc.applic.expressions.checks;

import java.util.function.Consumer;

import cdc.issues.IssueSeverity;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;
import cdc.issues.rules.RuleUtils;

public final class ApplicRuleUtils {
    private ApplicRuleUtils() {
    }

    public static final String DOMAIN = "Applic";

    public static Rule define(String name,
                              Consumer<RuleDescription.Builder<?>> descriptionBuilder,
                              IssueSeverity severity) {
        return RuleUtils.define(DOMAIN,
                                name,
                                descriptionBuilder,
                                severity);
    }
}