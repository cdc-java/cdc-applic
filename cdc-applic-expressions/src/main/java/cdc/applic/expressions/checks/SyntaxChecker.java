package cdc.applic.expressions.checks;

import java.util.List;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.LexicalException;
import cdc.applic.expressions.SyntacticException;
import cdc.util.lang.Checks;

/**
 * A {@link Checker} that checks that the syntax of an {@link Expression} is correct.
 *
 * @author Damien Carbonne
 */
public class SyntaxChecker implements Checker {
    public static final String RULE_NAME = "Syntax correctness";
    public static final SyntaxChecker INSTANCE = new SyntaxChecker();

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }

    @Override
    public boolean isCompliant(CheckedData data) {
        Checks.isNotNull(data, "data");
        Checks.isNotNull(data.getExpression(), "data.expression");

        try {
            data.getExpression().getRootNode();
        } catch (final LexicalException | SyntacticException e) {
            return false;
        }
        return true;
    }

    @Override
    public void checkCompliance(CheckedData data) {
        Checks.isNotNull(data, "data");
        // This will do lexical and syntax checks
        data.getNode();
    }

    @Override
    public void check(CheckedData data,
                      List<ApplicIssue> issues) {
        Checks.isNotNull(data, "data");
        Checks.isNotNull(issues, "issues");

        try {
            data.getNode();
        } catch (final LexicalException e) {
            issues.add(LexicalIssue.builder()
                                   .cause(data.getContext(), e)
                                   .build());
        } catch (final SyntacticException e) {
            issues.add(SyntaxIssue.builder()
                                  .cause(data.getContext(), e)
                                  .build());
        }
    }
}