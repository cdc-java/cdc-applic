package cdc.applic.expressions.checks;

import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.Expression;
import cdc.issues.IssueSeverity;
import cdc.issues.StructuredDescription;
import cdc.issues.checks.AbstractRuleChecker;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;

public class ExpressionMustBeSyntacticallyValid extends AbstractRuleChecker<Expression> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final String NAME = "EXPRESSION_MUST_BE_SYNTACTICALLY_VALID";

    protected static String describe(String item) {
        return RuleDescription.format("An {%wrap} must have a valid syntax.",
                                      item);
    }

    public static final Rule RULE =
            ApplicRuleUtils.define(NAME,
                                   description -> description.text(describe("expression"))
                                                             .appliesTo("All expressions"),

                                   SEVERITY);

    protected ExpressionMustBeSyntacticallyValid(SnapshotManager manager,
                                                 Rule rule) {
        super(manager,
              Expression.class,
              rule);
    }

    @Override
    protected String getHeader(Expression object) {
        return "The expression '" + object.getContent() + "'";
    }

    @Override
    public CheckResult check(CheckContext context,
                             Expression object,
                             Location location) {
        try {
            object.getRootNode();
            return CheckResult.SUCCESS;
        } catch (final ApplicException e) {
            final StructuredDescription.Builder<?> description = StructuredDescription.builder();

            description.text(getHeader(object))
                       .violation("is syntactically invalid.")
                       .violation(e.getMessage());

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;
        }
    }
}