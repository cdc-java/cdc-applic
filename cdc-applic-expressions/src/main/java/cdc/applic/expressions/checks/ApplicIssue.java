package cdc.applic.expressions.checks;

import java.util.List;
import java.util.Objects;

import cdc.applic.expressions.ExpressionException;
import cdc.issues.Issue;
import cdc.issues.locations.Location;
import cdc.util.lang.Checks;
import cdc.util.lang.CollectionUtils;

/**
 * Description of an issue.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-21", forRemoval = true)
public class ApplicIssue extends Issue {
    public static final String DOMAIN = ApplicRuleUtils.DOMAIN;
    private final Throwable cause;

    protected ApplicIssue(Builder<?> builder) {
        super(builder.domain(DOMAIN));
        this.cause = builder.cause;
        Checks.isTrue(getLocations().length > 0, "Empty locations");
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public ApplicIssue(ApplicIssueType type,
                       String project,
                       List<? extends Location> locations,
                       String description,
                       Throwable cause) {
        this(builder().name(type)
                      .severity(type.getSeverity())
                      .project(project)
                      .description(description)
                      .locations(List.copyOf(locations)) // FIXME remove copyOf when cdc-issue is fixed
                      .cause(cause));
        // super(DOMAIN,
        // type,
        // Params.NO_PARAMS,
        // project,
        // locations,
        // null,
        // type.getSeverity(),
        // description,
        // Params.NO_PARAMS);
        // this.cause = cause;
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public ApplicIssue(ApplicIssueType type,
                       String project,
                       List<? extends Location> locations,
                       String description) {
        this(type,
             project,
             locations,
             description,
             null);
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public ApplicIssue(ApplicIssueType type,
                       String project,
                       Location location,
                       String description,
                       Throwable cause) {
        this(type,
             project,
             CollectionUtils.toList(location),
             description,
             cause);
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public ApplicIssue(ApplicIssueType type,
                       String project,
                       Location location,
                       String description) {
        this(type,
             project,
             location,
             description,
             null);
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public ApplicIssueType getIssueType() {
        return getName(ApplicIssueType.class);
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public Throwable getCause() {
        return cause;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode()
                + Objects.hash(cause);
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        final ApplicIssue other = (ApplicIssue) object;
        return Objects.equals(cause, other.cause);
    }

    public static ApplicIssueType getMostSevereType(List<ApplicIssue> issues) {
        Checks.isNotNull(issues, "issues");
        ApplicIssueType max = ApplicIssueType.NO_ISSUE;
        for (final ApplicIssue issue : issues) {
            max = ApplicIssueType.mostSevere(max, issue.getIssueType());
        }
        return max;
    }

    public static Builder<?> builder() {
        return new Builder<>();
    }

    public static class Builder<B extends Builder<B>> extends Issue.Builder<B> {
        private Throwable cause;

        protected Builder() {
            super();
        }

        @Deprecated(since = "2024-12-21", forRemoval = true)
        protected B cause(Throwable cause) {
            this.cause = cause;
            return self();
        }

        @Deprecated(since = "2024-12-21", forRemoval = true)
        protected B cause(String context,
                          ExpressionException cause) {
            cause(cause);
            location(new ExpressionLocation(context,
                                            cause.getExpression(),
                                            cause.getBeginIndex(),
                                            cause.getEndIndex()));
            description(cause.getFullMessage());
            return self();
        }

        // private B fix() {
        // domain(DOMAIN);
        // return self();
        // }

        @Override
        public ApplicIssue build() {
            return new ApplicIssue(this);
        }
    }
}