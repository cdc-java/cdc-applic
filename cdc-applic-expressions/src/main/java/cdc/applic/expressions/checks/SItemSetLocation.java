package cdc.applic.expressions.checks;

import cdc.applic.expressions.content.SItemSet;
import cdc.issues.locations.AbstractLocation;

public class SItemSetLocation extends AbstractLocation {
    private final String context;
    private final SItemSet set;

    public static final String TAG = "SItemSetLocation";

    public SItemSetLocation(String context,
                            SItemSet set) {
        this.context = context;
        this.set = set;
    }

    public final String getContext() {
        return context;
    }

    public final SItemSet getSet() {
        return set;
    }

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    public String getPath() {
        return context == null
                ? set.getContent()
                : context + ":" + set.getContent();
    }

    @Override
    public String getAnchor() {
        return null;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return super.equals(object);
    }
}