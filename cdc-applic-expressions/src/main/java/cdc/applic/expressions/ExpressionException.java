package cdc.applic.expressions;

import cdc.util.lang.Checks;

public abstract class ExpressionException extends ApplicException {
    private static final long serialVersionUID = 1L;
    protected static final String DEFAULT_INFO = "Invalid expression";

    private final String info;
    private final String expression;
    private final int beginIndex;
    private final int endIndex;

    protected ExpressionException(String info,
                                  String expression,
                                  int beginIndex,
                                  int endIndex) {
        super(info + ": '" + expression + "'", null);
        Checks.isNotNull(expression, "expression");
        Checks.isTrue(beginIndex >= 0, "");

        this.info = info;
        this.expression = expression;
        this.beginIndex = beginIndex;
        this.endIndex = endIndex;
    }

    protected ExpressionException(String info,
                                  String expression) {
        this(info,
             expression,
             0,
             expression.length());
    }

    protected ExpressionException(String expression) {
        this(DEFAULT_INFO,
             expression);
    }

    public final String getInfo() {
        return info;
    }

    /**
     * @return The expression text.
     */
    public final String getExpression() {
        return expression;
    }

    /**
     * @return The begin index (inclusive) in {@code expression} of the error location.
     */
    public final int getBeginIndex() {
        return beginIndex;
    }

    /**
     * @return The end index (exclusive) in {@code expression} of the error location.
     */
    public final int getEndIndex() {
        return endIndex;
    }

    public final String getSlice() {
        final StringBuilder builder = new StringBuilder();
        final int begin = getBeginIndex();
        final int end = getEndIndex() >= 0 ? getEndIndex() : getExpression().length();
        for (int index = 0; index < begin; index++) {
            builder.append(" ");
        }
        for (int index = begin; index < end; index++) {
            builder.append("^");
        }
        return builder.toString();
    }

    public abstract String getFullMessage();
}