package cdc.applic.expressions;

public class ExpressionsFeatures {
    private final Formatting formatting;
    private final boolean simplify;

    protected ExpressionsFeatures(Formatting formatting,
                                  boolean simplify) {
        this.formatting = formatting;
        this.simplify = simplify;
    }

    public Formatting getFormatting() {
        return formatting;
    }

    public boolean simplify() {
        return simplify;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Formatting formatting = null;
        private boolean simplify = false;

        protected Builder() {
            super();
        }

        public Builder formatting(Formatting formatting) {
            this.formatting = formatting;
            return this;
        }

        public Builder simplify(boolean simplify) {
            this.simplify = simplify;
            return this;
        }

        public ExpressionsFeatures build() {
            return new ExpressionsFeatures(formatting, simplify);
        }
    }
}