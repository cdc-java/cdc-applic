package cdc.applic.expressions.literals;

/**
 * Possible escaping contexts.
 * <p>
 * Literals that must be escaped depend on that context.
 *
 * @author Damien Carbonne
 */
public enum EscapingContext {
    /**
     * The literal is:
     * <ul>
     * <li>a property name,
     * <li>an alias name (reference),
     * <li>a string item.
     * </ul>
     * In that case, if the non escaped literal is a number or a boolean, it must be escaped.<br>
     * If the literal contains some separation characters or patterns, it must be escaped.<br>
     * Otherwise, it does not need to be escaped.
     */
    NAME,

    /**
     * The literal is:
     * <ul>
     * <li>an integer item,
     * <li>a real item.
     * </ul>
     * In that case, the literal <em>MUST NOT</em> be escaped.
     */
    NUMBER,

    /**
     * The literal is:
     * <ul>
     * <li>a boolean item.
     * </ul>
     * In that case, the literal <em>MUST NOT</em> be escaped.
     */
    BOOLEAN
}