package cdc.applic.expressions.literals;

public enum LiteralKind {
    /**
     * Invalid literal: null, empty or contains illegal characters.
     */
    NONE,

    /**
     * Boolean literal.
     */
    BOOLEAN,

    /**
     * Integer Literal.
     */
    INTEGER,

    /**
     * Real literal.
     */
    REAL,

    /**
     * Valid literal which is not a boolean, integer or real.
     * <p>
     * An escaped literal is also considered as a restricted string.
     */
    RESTRICTED_STRING
}