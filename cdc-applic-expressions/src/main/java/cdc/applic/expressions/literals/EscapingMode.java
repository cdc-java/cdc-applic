package cdc.applic.expressions.literals;

/**
 * Enumeration of literal escaping modes.
 *
 * @author Damien Carbonne
 */
public enum EscapingMode {
    /**
     * The literal is not escaped, whether it is necessary or not.
     */
    NON_ESCAPED,

    /**
     * The literal is escaped, whether it is necessary or not.
     */
    ESCAPED,

    /**
     * The literal is safe to be used in a particular context.
     * <p>
     * It is escaped if necessary, not escaped otherwise.
     */
    PROTECTED
}