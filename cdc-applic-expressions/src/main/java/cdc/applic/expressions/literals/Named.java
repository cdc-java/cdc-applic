package cdc.applic.expressions.literals;

import java.util.Comparator;

/**
 * Interface implemented by objects that have a {@link Name}.
 *
 * @author Damien Carbonne
 */
public interface Named {
    public static Comparator<Named> NAME_COMPARATOR =
            Comparator.comparing(Named::getName);

    /**
     * @return The name of this object.
     */
    public Name getName();
}