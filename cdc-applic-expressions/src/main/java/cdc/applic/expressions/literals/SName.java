package cdc.applic.expressions.literals;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import cdc.util.lang.Checks;

/**
 * Class used to safely encapsulate a string in a {@link EscapingContext#NAME} context.
 *
 * @author Damien Carbonne
 */
public class SName implements Comparable<SName> {
    public static final Comparator<SName> COMPARATOR =
            Comparator.nullsFirst(Comparator.naturalOrder());

    /**
     * Literal used to designate all unknown value with pattern types.
     * <p>
     * Known values are values that are explicitly used in known expressions (assertions or user defined).<br>
     * This depends on the context.
     */
    public static final String LITERAL_PATTERN_UNKNOWN = "???";

    /** The non-escaped literal. */
    private final String nonEscaped;

    /**
     * Creates a SName from an <em>escaped</em> or <em>non-escaped</em> literal.
     *
     * @param literal The <em>escaped</em> or <em>non-escaped</em> literal.
     * @param escaped {@code true} if {@code literal} is <em>escaped</em>.
     * @throws IllegalArgumentException When {@code literal} and {@code escaped} don't match.
     */
    protected SName(String literal,
                    boolean escaped) {
        this.nonEscaped = EscapingUtils.toDoubleQuotesNonEscaped(literal, escaped);
    }

    /**
     * Creates a SName from the non-escaped literal.
     *
     * @param literal The non-escaped literal.
     */
    protected SName(String literal) {
        this(literal, false);
    }

    /**
     * @param literal The escaped or non-escaped literal.
     * @param escaped If {@code true}, {@code literal} is escaped.
     * @return {@code null} when {@code literal} is {@code null},
     *         or a new SName built from {@code literal} and {@code escaped}.
     * @throws IllegalArgumentException When {@code literal} and {@code escaped} don't match.
     */
    public static SName of(String literal,
                           boolean escaped) {
        return literal == null ? null : new SName(literal, escaped);
    }

    /**
     * @param literal The non-escaped literal.
     * @return {@code null} when {@code literal} is {@code null},
     *         or a new SName built from {@code literal}.
     */
    public static SName of(String literal) {
        return literal == null ? null : new SName(literal);
    }

    public boolean isPatternUnknwown() {
        return LITERAL_PATTERN_UNKNOWN.equals(nonEscaped);
    }

    /**
     * @return {@code true} if the literal needs to be escaped.
     */
    public final boolean needsEscape() {
        return EscapingUtils.needsDoubleQuotesEscape(nonEscaped);
    }

    /**
     * @return The escaped version of the literal, whether escaping is necessary or not.
     */
    public final String getEscapedLiteral() {
        return EscapingUtils.escapeWithDoubleQuotes(nonEscaped);
    }

    /**
     * @return The non-escaped version of the literal, whether escaping is necessary or not.
     */
    public final String getNonEscapedLiteral() {
        return nonEscaped;
    }

    /**
     * @return The protected version of the literal.
     */
    public final String getProtectedLiteral() {
        return getLiteral(EscapingMode.PROTECTED);
    }

    /**
     * @param mode The escaping mode.
     * @return The literal escaped if necessary or required, depending on {@code mode}.
     */
    public final String getLiteral(EscapingMode mode) {
        Checks.isNotNull(mode, "mode");

        if (mode == EscapingMode.ESCAPED) {
            return EscapingUtils.escapeWithDoubleQuotes(nonEscaped);
        } else if (mode == EscapingMode.NON_ESCAPED) {
            return nonEscaped;
        } else {
            return needsEscape()
                    ? EscapingUtils.escapeWithDoubleQuotes(nonEscaped)
                    : nonEscaped;
        }
    }

    @Override
    public int compareTo(SName other) {
        // Make sure LITERAL_PATTERN_ANY is greater than any other value
        final int cmp = getNonEscapedLiteral().compareTo(other.getNonEscapedLiteral());
        if (cmp == 0) {
            return 0;
        } else if (LITERAL_PATTERN_UNKNOWN.equals(getNonEscapedLiteral())) {
            return 1;
        } else if (LITERAL_PATTERN_UNKNOWN.equals(other.getNonEscapedLiteral())) {
            return -1;
        } else {
            return cmp;
        }
    }

    @Override
    public int hashCode() {
        return nonEscaped.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof SName)) {
            return false;
        }
        final SName other = (SName) object;
        return this.nonEscaped.equals(other.nonEscaped);
    }

    @Override
    public String toString() {
        return getLiteral(EscapingMode.PROTECTED);
    }

    public static List<SName> toSNames(String... names) {
        final List<SName> list = new ArrayList<>();
        for (final String name : names) {
            list.add(new SName(name));
        }
        return list;
    }
}