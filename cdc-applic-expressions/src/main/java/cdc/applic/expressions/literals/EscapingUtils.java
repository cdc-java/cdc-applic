package cdc.applic.expressions.literals;

import cdc.applic.expressions.parsing.OneCharEscapes;
import cdc.util.lang.Checks;

/**
 * Escape handling.
 * <ul>
 * <li>Two characters are forbidden in literals by S1000D: '~' and '|'.
 * <li>Integers and Reals must be escaped when used as string values (but this is not recommended).
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class EscapingUtils {
    private EscapingUtils() {
    }

    /**
     * Returns {@code true} when a character is legal (S1000D rules) in a literal.
     * <ul>
     * <li>It <em>MUST NOT</em> be '~'.
     * <li>It <em>MUST NOT</em> be '|'.
     * </ul>
     *
     * @param c The character.
     * @return {@code true} when {@code c} is a legal literal character.
     */
    public static boolean isLegal(char c) {
        return c != '~' && c != '|';
    }

    /**
     * Returns {@code true} when a string is legal literal.
     * <p>
     * <b>WARNING:</b> A legal literal is not necessarily valid.
     * <ul>
     * <li>It <em>MUST NOT</em> be null.
     * <li>It <em>MUST NOT</em> be empty.
     * <li>It <em>MUST NOT</em> contain illegal characters ('~' and '|').
     * </ul>
     *
     * @param s The string.
     * @return {@code true} when {@code s} is legal.
     */
    public static boolean isLegalLiteral(String s) {
        if (s == null || s.isEmpty()) {
            return false;
        } else {
            for (int index = 0; index < s.length(); index++) {
                final char c = s.charAt(index);
                if (!isLegal(c)) {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * Returns {@code true} when a character needs to be escaped with '"'.
     * <p>
     * Presence of certain characters in a literal necessitates escaping whatever the context is.
     *
     * @param c The character to test
     * @return {@code true} when {@code c} needs to be escaped.
     */
    private static boolean needsDoubleQuotesEscape(char c) {
        return OneCharEscapes.BEST_MATCHER.test(c);
    }

    /**
     * Returns {@code true} when a string needs to be escaped with '"' in name context.
     * <p>
     * Result is independent of the legality of the string as a literal.
     *
     * @param s The tested string.
     * @return {@code true} when {@code s} needs to be escaped in {@code context}.
     */
    public static boolean needsDoubleQuotesEscape(String s) {
        // FIXME this does not handle all cases
        if (s != null) {
            for (int index = 0; index < s.length(); index++) {
                if (needsDoubleQuotesEscape(s.charAt(index))) {
                    return true;
                }
            }
            return LiteralUtils.isBooleanLiteral(s)
                    || LiteralUtils.isSpecialStringLiteral(s)
                    || LiteralUtils.startsAsNumber(s); // TODO is number
        }
        return false;
    }

    /**
     * Escapes a string with '"', whether it is needed or not.
     * <p>
     * The '"' escaped version of s is built by replacing all occurrences of '"' by '""'
     * and by prepending and appending '"'.
     * <br>
     * <code>
     * escapeWithDoubleQuotes("abc") = "\"abc\""<br>
     * escapeWithDoubleQuotes("ab\"c") = "\"ab\"\"c\""
     * </code>
     *
     * @param s The string to escape.
     * @return The escaped version of {@code s}.
     */
    public static String escapeWithDoubleQuotes(String s) {
        final StringBuilder builder = new StringBuilder();
        builder.append('"');
        for (int index = 0; index < s.length(); index++) {
            final char c = s.charAt(index);
            if (c == '"') {
                builder.append("\"\"");
            } else {
                builder.append(c);
            }
        }
        builder.append('"');
        return builder.toString();
    }

    /**
     * Escapes a string with '$'.
     * <p>
     * The '$' escaped version of s is built by replacing all occurrences of '$' by '$$'
     * and by prepending and appending '$'.
     * <br>
     * <code>
     * escapeWithDollars("abc") = "$abc$"<br>
     * escapeWithDollars("ab$c") = "$ab$$c$"
     * </code>
     *
     * @param s The string to dollar.
     * @return The dollared version of {@code s}.
     */
    public static String escapeWithDollars(String s) {
        final StringBuilder builder = new StringBuilder();
        builder.append('$');
        for (int index = 0; index < s.length(); index++) {
            final char c = s.charAt(index);
            if (c == '$') {
                builder.append("$$");
            } else {
                builder.append(c);
            }
        }
        builder.append('$');
        return builder.toString();
    }

    /**
     * Escapes a string only of it is needed.
     *
     * @param s The string to possibly escape.
     * @return {@code s} or the escaped version of {@code s}.
     */
    public static String escapeWithDoubleQuotesIfNeeded(String s) {
        return needsDoubleQuotesEscape(s) ? escapeWithDoubleQuotes(s) : s;
    }

    /**
     * Returns the unescaped version of a '"' escaped string.
     * <p>
     * This removes first and last character (supposedly '"') and all characters that
     * follow '"' (supposedly another '"').
     * <p>
     * {@code unescapeDoubleQuotes("abc") = "b"} (Invalid call)<br>
     * {@code unescapeDoubleQuotes("\"ab\"\"c\"" = "ab\"c"} (Valid call)
     *
     * @param s The '"' escaped string.
     * @return The unescaped version of s.
     * @throws IllegalArgumentException When s is {@code null} or not a '"' escaped string.
     */
    public static String unescapeDoubleQuotes(String s) {
        // Checks.isNotNull(s, "s");
        // Checks.isTrue(s.length() >= 2, "'" + s + "' is too short for unescape");
        // Checks.isTrue(s.charAt(0) == '"' && s.charAt(s.length() - 1) == '"', "'" + s + "' is not un escaped string");

        final StringBuilder builder = new StringBuilder();
        int index = 1;
        while (index < s.length() - 1) {
            final char c = s.charAt(index);
            builder.append(c);
            if (c == '"') {
                index++;
            }
            index++;
        }
        return builder.toString();
    }

    /**
     * Returns the unescaped version of a '$' escaped string.
     * <p>
     * This removes first and last character (supposedly '$') and all characters that
     * follow '$' (supposedly another '$').
     * <p>
     * {@code unescapeDollars("abc") = "b"} (Invalid call)<br>
     * {@code unescapeDollars("$ab$$c$" = "ab$c"} (Valid call)
     *
     * @param s The '$' escaped string.
     * @return The unescaped version of s.
     * @throws IllegalArgumentException When s is {@code null} or not a dollar string.
     */
    public static String unescapeDollars(String s) {
        final StringBuilder builder = new StringBuilder();
        int index = 1;
        while (index < s.length() - 1) {
            final char c = s.charAt(index);
            builder.append(c);
            if (c == '$') {
                index++;
            }
            index++;
        }
        return builder.toString();
    }

    /**
     * Find the index of the last char of an escaped section.
     * <p>
     * It is the first '"' not followed by another '"'.
     *
     * @param chars The characters.
     * @param from The index of the beginning of the escaped section. It must contain '"'.
     * @return The index of the last char of the escaped section (containing '"'), or -1.
     * @throws IllegalArgumentException When the character at {@code from} is not '"'.
     */
    public static int getEndIndexOfEscapeSection(char[] chars,
                                                 int from) {
        Checks.isTrue(chars[from] == '"', "Unexpected char at {} in '{}'", from, chars);
        final int len = chars.length;
        int pos = from + 1;
        while (pos < len) {
            if (chars[pos] == '"') {
                pos++;
                if (pos < len && chars[pos] == '"') {
                    // We found an escaped '"'
                    // One char after '""'
                    pos++;
                    // continue exploration
                } else {
                    // pos is one char after closing '"'
                    return pos - 1;
                }
            } else {
                pos++;
            }
        }
        // We did'nt find a matching closing '"'
        return -1;
    }

    public static int getEndIndexOfEscapeSection(String s,
                                                 int from) {
        return getEndIndexOfEscapeSection(s.toCharArray(), from);
    }

    /**
     * Returns the index of the first path separator.
     * <p>
     * Escaped sections re skipped.
     * The number of escaped sections is not analyzed.
     *
     * @param chars The characters.
     * @param from The index of the first tested character.
     * @return The index of the first path separator or -1.
     * @throws IllegalArgumentException When a non-closed escaped section is met during search.
     */
    public static int getPathSeparatorIndex(char[] chars,
                                            int from) {
        final int len = chars.length;
        int pos = from;
        while (pos < len) {
            if (chars[pos] == '"') {
                pos = getEndIndexOfEscapeSection(chars, from);
                if (pos == -1) {
                    throw new IllegalArgumentException("Non-closed escaped section.");
                } else {
                    pos++;
                }
            } else if (chars[pos] == LiteralUtils.PATH_SEPARATOR) {
                return pos;
            } else {
                pos++;
            }
        }
        return -1;
    }

    public static int getPathSeparatorIndex(String s,
                                            int from) {
        return getPathSeparatorIndex(s.toCharArray(), from);
    }

    /**
     * Returns {@code true} when a string is a valid escaped or unescaped text.
     * <p>
     * <b>WARNING:</b> this does not mean it is a valid literal.
     *
     * @param s The string to test.
     * @return {@code true} when {@code s} is a valid escaped or unescaped text.
     */
    public static boolean isValidText(String s) {
        return isValidUnescapedText(s) || isValidDoubleQuotesEscapedText(s);
    }

    /**
     * Returns {@code true} when a string is a valid unescaped text.
     * <p>
     * <b>WARNING:</b> This does not mean it is a valid literal.<br>
     * Such a text must:
     * <ul>
     * <li>not be {@code null},
     * <li>not be empty,
     * <li>not use any character that needs escape.
     * </ul>
     *
     * @param s The string to test.
     * @return {@code true} when {@code s} is a valid unescaped text.
     */
    public static boolean isValidUnescapedText(String s) {
        return isLegalLiteral(s) && !needsDoubleQuotesEscape(s);
    }

    /**
     * Returns true when a string is a valid '"' escaped text.
     * <p>
     * <b>WARNING:</b> This does not mean it is a valid literal.<br>
     * Such a text must:
     * <ul>
     * <li>not be null,
     * <li>start and finish with '"',
     * <li>not be empty (excluding surrounding '"'),
     * <li>contain pairs of successive '"' (excluding surrounding '"').
     * </ul>
     *
     * @param s The string to test.
     * @return {@code true} when a {@code s} is a valid '"' escaped text.
     */
    public static boolean isValidDoubleQuotesEscapedText(String s) {
        if (!isLegalLiteral(s) || s.length() < 3 || s.charAt(0) != '"' || s.charAt(s.length() - 1) != '"') {
            return false;
        }
        int count = 0;
        for (int index = 1; index < s.length() - 1; index++) {
            final char c = s.charAt(index);
            if (c == '"') {
                count++;
            } else {
                if ((count % 2) != 0) {
                    return false;
                }
                count = 0;
            }
        }
        return (count % 2) == 0;
    }

    /**
     * Returns true when a string is a valid '$' escaped text.
     * <p>
     * Such a text must:
     * <ul>
     * <li>not be null,
     * <li>start and finish with '$',
     * <li>not be empty (excluding surrounding '$'),
     * <li>contain pairs of successive '$' (excluding surrounding '$').
     * </ul>
     *
     * @param s The string to test.
     * @return {@code true} when a {@code s} is a valid '$' escaped text.
     */
    public static boolean isValidDollarsEscapedText(String s) {
        if (s == null || s.length() < 3 || s.charAt(0) != '$' || s.charAt(s.length() - 1) != '$') {
            return false;
        }
        int count = 0;
        for (int index = 1; index < s.length() - 1; index++) {
            final char c = s.charAt(index);
            if (c == '$') {
                count++;
            } else {
                if ((count % 2) != 0) {
                    return false;
                }
                count = 0;
            }
        }
        return (count % 2) == 0;
    }

    /**
     * @param s The string to test.
     * @return {@code true} if {@code s} looks like a '"' escaped text.<br>
     *         This is not 100% sure and should be used when escaping has been checked by other means.
     */
    public static boolean isProbablyDoubleQuotesEscaped(String s) {
        return s != null
                && !s.isEmpty()
                && s.charAt(0) == '"'
                && s.charAt(s.length() - 1) == '"';
    }

    /**
     * Returns the <em>non-escaped</em> view of a '"' <em>escaped</em> or <em>non-escaped</em> literal.
     *
     * @param literal The literal.
     * @param escaped {@code true} if {@code literal} is <em>'"' escaped</em>.
     * @return The <em>non-escaped</em> view of {@code literal}.
     * @throws IllegalArgumentException When {@code literal} and {@code escaped} don't match.
     */
    public static String toDoubleQuotesNonEscaped(String literal,
                                                  boolean escaped) {
        if (escaped) {
            if (EscapingUtils.isValidDoubleQuotesEscapedText(literal)) {
                return unescapeDoubleQuotes(literal);
            } else {
                throw new IllegalArgumentException("Invalid '\"' escaped text: '" + literal + "'");
            }
        } else {
            if (isLegalLiteral(literal)) {
                return literal;
            } else {
                throw new IllegalArgumentException("Invalid '\"' non-escaped text: '" + literal + "'");
            }
        }
    }

    /**
     * Returns the <em>non-escaped</em> view of a '"' <em>escaped</em> or <em>non-escaped</em> literal.
     *
     * @param text The text.
     * @param escaped {@code true} if {@code literal} is <em>'$' escaped</em>.
     * @return The <em>non-escaped</em> view of {@code text}.
     * @throws IllegalArgumentException When {@code text} and {@code escaped} don't match.
     */
    public static String toDollarNonEscaped(String text,
                                            boolean escaped) {
        if (escaped) {
            if (EscapingUtils.isValidDollarsEscapedText(text)) {
                return unescapeDollars(text);
            } else {
                throw new IllegalArgumentException("Invalid '$' escaped text: '" + text + "'");
            }
        } else {
            return text;
        }
    }
}