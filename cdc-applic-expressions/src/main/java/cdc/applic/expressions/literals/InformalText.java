package cdc.applic.expressions.literals;

import java.util.Comparator;

public class InformalText implements Comparable<InformalText> {
    public static final Comparator<InformalText> COMPARATOR =
            Comparator.nullsFirst(Comparator.naturalOrder());

    /** The non-escaped text. */
    private final String nonEscaped;

    /**
     * Creates an InformalText from an <em>escaped</em> or <em>non-escaped</em> text.
     *
     * @param text The <em>escaped</em> or <em>non-escaped</em> text.
     * @param escaped {@code true} if {@code text} is <em>escaped</em>.
     * @throws IllegalArgumentException When {@code text} and {@code escaped} don't match.
     */
    protected InformalText(String text,
                           boolean escaped) {
        this.nonEscaped = EscapingUtils.toDollarNonEscaped(text, escaped);
    }

    /**
     * Creates an InformalText from the non-escaped text.
     *
     * @param text The non-escaped text.
     */
    protected InformalText(String text) {
        this(text, false);
    }

    /**
     * @param text The escaped or non-escaped text.
     * @param escaped If {@code true}, {@code text} is escaped.
     * @return {@code null} when {@code text} is {@code null},
     *         or a new SName built from {@code text} and {@code escaped}.
     * @throws IllegalArgumentException When {@code text} and {@code escaped} don't match.
     */
    public static InformalText of(String text,
                                  boolean escaped) {
        return text == null ? null : new InformalText(text, escaped);
    }

    /**
     * @param text The non-escaped text.
     * @return {@code null} when {@code text} is {@code null},
     *         or a new SName built from {@code text}.
     */
    public static InformalText of(String text) {
        return text == null ? null : new InformalText(text);
    }

    /**
     * @return The escaped version of the text, whether escaping is necessary or not.
     */
    public final String getEscapedText() {
        return EscapingUtils.escapeWithDollars(nonEscaped);
    }

    /**
     * @return The non-escaped version of the text, whether escaping is necessary or not.
     */
    public final String getNonEscapedText() {
        return nonEscaped;
    }

    @Override
    public int compareTo(InformalText other) {
        return getNonEscapedText().compareTo(other.getNonEscapedText());
    }

    @Override
    public int hashCode() {
        return nonEscaped.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof InformalText)) {
            return false;
        }
        final InformalText other = (InformalText) object;
        return this.nonEscaped.equals(other.nonEscaped);
    }

    @Override
    public String toString() {
        return getNonEscapedText();
    }
}