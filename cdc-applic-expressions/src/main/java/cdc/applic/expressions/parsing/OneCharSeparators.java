package cdc.applic.expressions.parsing;

import cdc.util.function.BitSetCharPredicate;
import cdc.util.function.BooleanArrayCharPredicate;
import cdc.util.function.CharPredicate;
import cdc.util.function.MultiplyShiftCharPredicate;

/**
 * Recognition of separators that hold on one character.
 * <p>
 * <b>WARNING:</b> inside an escaped text, those chars should not be recognized as separators.
 *
 * @author Damien Carbonne
 */
public final class OneCharSeparators {
    /**
     * String containing characters that are considered as one character separators.
     */
    static final String CHARS = OneCharTokens.CHARS + "!<>" + Spaces.CHARS;

    /**
     * Perfect hash table for {@link #CHARS}.
     * <p>
     * '(' is used as the filling character.<br>
     * <b>WARNING:</b> this is computed and must match {@link #CHARS}.
     */
    static final String STABLE =
            "\u2004\u2005(\u2006(\u2007(\u2008\u2009(\u200a(\u200b(((\t\n(\u000b(\f(\r(∅((((∈\u00a0∉((\u205f((\u303f((((((((((((\u180e¬\u3000⊤(⊥\u0020(!≠(((\u2028(\u2029≤&≥((()→((\u202f↔,((.((≮∧≯∨≰(≱{(|(}(~(((((\u1680((<(=\u0085>((((((((\u2000\ufeff\u2001(\u2002↮\u2003(";

    /**
     * Conversion of {@link #STABLE} as a char array
     */
    static final char[] TABLE = STABLE.toCharArray();

    /**
     * The multiplier to use for {@link #TABLE}.
     */
    static final int MULTIPLY = 60_263_717;

    /**
     * The shift to use for {@link #TABLE}.
     */
    static final int SHIFT = 25;

    private OneCharSeparators() {
    }

    public static final CharPredicate MULTIPLY_SHIFT_MATCHER =
            new MultiplyShiftCharPredicate(STABLE,
                                           MULTIPLY,
                                           SHIFT);

    public static final CharPredicate MULTIPLY_SHIFT_INLINE_MATCHER =
            c -> TABLE[(c * MULTIPLY) >>> SHIFT] == c;

    public static final CharPredicate BIT_SET_MATCHER =
            new BitSetCharPredicate(CHARS);

    public static final CharPredicate BOOLEAN_ARRAY_MATCHER =
            new BooleanArrayCharPredicate(CHARS);

    /**
     * Matcher that must be used to identify one character separators.
     */
    public static final CharPredicate BEST_MATCHER = MULTIPLY_SHIFT_INLINE_MATCHER;
}