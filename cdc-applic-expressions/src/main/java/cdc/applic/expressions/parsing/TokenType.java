package cdc.applic.expressions.parsing;

/**
 * Enumeration of possible token types found in an expression.
 * <p>
 * <b>WARNING:</b> Order of declarations matters.
 *
 * @author Damien Carbonne
 */
public enum TokenType {
    /** Open parenthesis: {@code '('} */
    OPEN_PAREN,

    /** Close parenthesis: {@code ')'} */
    CLOSE_PAREN,

    // Start of binary operators
    /** Implication: 'imp' (case insensitive), {@code '->'}, &rarr; */
    IMPL,

    /** Equivalence: 'iff" (case insensitive), {@code '<->'}, &harr; */
    EQUIV,

    /** Xor: 'xor" (case insensitive), {@code '>-<'}, &nharr; */
    XOR,

    /** Logical or: 'or' (case insensitive), {@code '|'}, &or; */
    OR,

    /** Logical and: 'and' (case insensitive), {@code '&'}, &and; */
    AND,
    // End of binary operators

    /** Negation: 'not' (case insensitive), {@code '!'}, &not; */
    NOT,

    /** Equality: {@code '='} */
    EQUAL,

    /** Inequality: {@code '!='}, &ne; */
    NOT_EQUAL,

    /** Is in set: {@code 'in'} (case insensitive), {@code '<:'}, &isin; */
    IN,

    /** Is not in set: {@code 'not in'} (case insensitive), {@code '!<:'}, &notin; */
    NOT_IN,

    /** Less than: {@code '<'}, &lt; */
    LESS,

    /** Not less than: {@code '!<'}, &nlt; */
    NOT_LESS,

    /** Less then or equal to: {@code '<='}, &le; */
    LESS_OR_EQUAL,

    /** Neither less than nor equal to: {@code '!<='}, &nle; */
    NEITHER_LESS_NOR_EQUAL,

    /** Greater than: {@code '>'}, &gt; */
    GREATER,

    /** Not greater than: {@code '!>'}, &ngt; */
    NOT_GREATER,

    /** Greater than or equal to: {@code '>='}, &ge; */
    GREATER_OR_EQUAL,

    /** Neither greater than nor equal to: {@code '!>='}, &nge; */
    NEITHER_GREATER_NOR_EQUAL,

    /** Text surrounded with {@code '$...$'}, used to define an informal expression. */
    DOLLAR_TEXT,

    // Start of items
    /** String value (item), Reference (not an item) or Property name (not an item). */
    TEXT,

    /** String value (item), Reference (not an item) or Property name (not an item). Surrounded with {@code '"..."'}. */
    DOUBLE_QUOTES_TEXT,

    /** Integer value */
    INTEGER,

    /** Real value */
    REAL,

    /** Contradiction: : {@code 'false'} (case insensitive), &perp; */
    FALSE,

    /** Tautology: {@code 'true'} (case insensitive), &top; */
    TRUE,
    // End of items

    /** Empty (maths) set. */
    EMPTY_SET,

    /** Range bounds separator: {@code '~'}, {@code 'to'} */
    TO,

    /** Values and ranges separator in sets: {@code ','} */
    ITEMS_SEP,

    /** Path separator in names: {@code '.'} */
    PATH_SEP,

    /** Open set: <code>'{'</code> */
    OPEN_SET,

    /** Close set: <code>'}'</code> */
    CLOSE_SET,

    EPSILON;

    private TokenType() {
    }

    /**
     * Returns {@code true} when this TokenType is considered binary operator from a node perspective.
     * <p>
     * The answer is positive for {@link #AND}, {@link #OR}, {@link #IMPL}, {@link #XOR} and {@link #EQUIV}.
     * <p>
     * <b>WARNING:</b> {@link #EQUAL}, {@link #NOT_EQUAL}, {@link #IN}, {@link #NOT_IN}, ... are not handled
     * as binary nodes by this implementation.
     *
     * @return {@code true} when this TokenType is considered binary.
     */
    public boolean isBinary() {
        // This seems to be the fastest way to yield this result.
        // Accessing static variables seems longer.
        // Using switch was longer
        // Using explicit equality and || was also longer.
        // This type of implementation has a constant time.
        return this.ordinal() >= IMPL.ordinal() && this.ordinal() <= AND.ordinal();
    }

    /**
     * Returns {@code true} when this TokenType is a considered as an item.
     * <p>
     * An item is a value and that can be found at the right side of a property of equality operators or in sets.<br>
     * Note that {@link #TEXT} and {@link #DOUBLE_QUOTES_TEXT} can also be used as property or alias names.
     *
     * @return {@code true} when this TokenType is a considered as an item:
     *         {@link #TEXT}, {@link #DOUBLE_QUOTES_TEXT}, {@link #INTEGER}, {@link #REAL}, {@link #FALSE} or {@link #TRUE}.
     */
    public boolean isItem() {
        return this.ordinal() >= TEXT.ordinal() && this.ordinal() <= TRUE.ordinal();
    }

    /**
     * @return {@code true} when this TokenType is a considered as a text item:
     *         {@link #TEXT} or {@link #DOUBLE_QUOTES_TEXT}.
     */
    public boolean isTextItem() {
        return this == TEXT || this == DOUBLE_QUOTES_TEXT;
    }

    /**
     * TODO extend to Strings ?
     *
     * @return {@code true} when this TokenType supports Range:
     *         {@link #INTEGER} or {@link #REAL}.
     */
    public boolean supportsRange() {
        return this == INTEGER || this == REAL;
    }
}