package cdc.applic.expressions.parsing;

import cdc.util.function.BitSetCharPredicate;
import cdc.util.function.BooleanArrayCharPredicate;
import cdc.util.function.CharPredicate;
import cdc.util.function.MultiplyShiftCharPredicate;

/**
 * Recognition of characters that must be escaped.
 * <p>
 * Generated with MultiplyShiftHashSearcher.
 *
 * @author Damien Carbonne
 */
public final class OneCharEscapes {
    /**
     * String containing characters that must be escaped, wherever they are in a literal.
     */
    static final String CHARS = "\"$" + OneCharSeparators.CHARS;

    /**
     * Perfect hash table for {@link #CHARS}.
     * <p>
     * '(' is used as the filling character.<br>
     * <b>WARNING:</b> this is computed and must match {@link #CHARS}.
     */
    static final String STABLE =
            "(((↔((\u2000↮(\u3000(\u2001≠(((\u2002((((\u2003((((\u2004\ufeff(((\u2005≤\u0085\u0020(\u2006≥(!(\u2007\u00a0(\"\t\u2008<((\n\u2009=($\u000b\u200a>((\f\u200b(∅&\r((((((\u303f((\u180e((∈)\u2028((≮∉\u2029((≯((((≰,(((≱(⊤(¬\u205f.⊥(∧\u1680({(∨((|\u202f(((}→(((~((";

    /**
     * Conversion of {@link #STABLE} as a char array
     */
    static final char[] TABLE = STABLE.toCharArray();

    /**
     * The multiplier to use for {@link #TABLE}.
     */
    static final int MULTIPLY = 169_894_104;

    /**
     * The shift to use for {@link #TABLE}.
     */
    static final int SHIFT = 25;

    private OneCharEscapes() {
    }

    public static final CharPredicate MULTIPLY_SHIFT_MATCHER =
            new MultiplyShiftCharPredicate(STABLE,
                                           MULTIPLY,
                                           SHIFT);

    public static final CharPredicate MULTIPLY_SHIFT_INLINE_MATCHER =
            c -> TABLE[(c * MULTIPLY) >>> SHIFT] == c;

    public static final CharPredicate BIT_SET_MATCHER =
            new BitSetCharPredicate(CHARS);

    public static final CharPredicate BOOLEAN_ARRAY_MATCHER =
            new BooleanArrayCharPredicate(CHARS);

    /**
     * Matcher that must be used to identify one character separators.
     */
    public static final CharPredicate BEST_MATCHER = MULTIPLY_SHIFT_INLINE_MATCHER;
}