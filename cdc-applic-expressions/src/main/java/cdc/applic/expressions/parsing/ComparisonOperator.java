package cdc.applic.expressions.parsing;

/**
 * Enumeration of comparison operators.
 * <p>
 * They match value nodes.
 *
 * @author Damien Carbonne
 */
public enum ComparisonOperator {
    /** &pi; = &upsilon; */
    EQUAL,
    /** &pi; &ne; &upsilon; */
    NOT_EQUAL,
    /** &pi; &lt; &upsilon; */
    LESS,
    /** &pi; &nlt; &upsilon; */
    NOT_LESS,
    /** &pi; &le; &upsilon; */
    LESS_OR_EQUAL,
    /** &pi; &nle; &upsilon; */
    NEITHER_LESS_NOR_EQUAL,
    /** &pi; &gt; &upsilon; */
    GREATER,
    /** &pi; &ngt; &upsilon; */
    NOT_GREATER,
    /** &pi; &ge; &upsilon; */
    GREATER_OR_EQUAL,
    /** &pi; &nge; &upsilon; */
    NEITHER_GREATER_NOR_EQUAL;

    private static final ComparisonOperator[] OPPOSITE = {
            NOT_EQUAL,
            EQUAL,
            NOT_LESS,
            LESS,
            NEITHER_LESS_NOR_EQUAL,
            LESS_OR_EQUAL,
            NOT_GREATER,
            GREATER,
            NEITHER_GREATER_NOR_EQUAL,
            GREATER_OR_EQUAL
    };

    /**
     * @return {@code true} when this operator is negative.
     */
    public boolean isNegative() {
        return this == NOT_EQUAL
                || this == NOT_LESS
                || this == NOT_GREATER
                || this == NEITHER_LESS_NOR_EQUAL
                || this == NEITHER_GREATER_NOR_EQUAL;
    }

    /**
     * @return The negation of this operator.
     */
    public ComparisonOperator negate() {
        return OPPOSITE[this.ordinal()];
    }

    /**
     * @return {@code true} if the set of values designated by this operator includes the reference value.
     */
    public boolean includesEqual() {
        return this == EQUAL
                || this == NOT_LESS
                || this == LESS_OR_EQUAL
                || this == NOT_GREATER
                || this == GREATER_OR_EQUAL;
    }

    /**
     * @return {@code true} if the set of values designated by this operator includes values that are less than the reference value.
     */
    public boolean includesLess() {
        return this == NOT_EQUAL
                || this == LESS
                || this == LESS_OR_EQUAL
                || this == NOT_GREATER
                || this == NEITHER_GREATER_NOR_EQUAL;
    }

    /**
     * @return {@code true} if the set of values designated by this operator includes values that are greater than the reference value.
     */
    public boolean includesGreater() {
        return this == NOT_EQUAL
                || this == NOT_LESS
                || this == NEITHER_LESS_NOR_EQUAL
                || this == GREATER
                || this == GREATER_OR_EQUAL;
    }

    /**
     * @return {@code true} if the set of values designated by this operator includes values that are not related to the reference value.
     */
    public boolean includesUnrelated() {
        return this == NOT_EQUAL
                || this == NOT_LESS
                || this == NEITHER_LESS_NOR_EQUAL
                || this == NOT_GREATER
                || this == NEITHER_GREATER_NOR_EQUAL;
    }
}