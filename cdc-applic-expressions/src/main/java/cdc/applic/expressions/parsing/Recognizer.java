package cdc.applic.expressions.parsing;

import java.util.function.Consumer;

import cdc.applic.expressions.LexicalException;
import cdc.applic.expressions.SyntacticException;

/**
 * Class used to check syntactical validity of an Expression.
 * <p>
 * This class is not reentrant.
 *
 * @author Damien Carbonne
 */
public class Recognizer {
    private final Analyzer analyzer = new Analyzer();

    private static final Consumer<ParsingEvent> VOID = e -> {
        // Ignore
    };

    public Recognizer() {
        super();
    }

    /**
     * Checks the syntactical validity of an expression.
     *
     * @param expression The expression to check.
     * @return {@code true} if the expression is syntactically valid.
     */
    public boolean isValid(String expression) {
        try {
            recognize(expression);
            return true;
        } catch (final LexicalException | SyntacticException e) {
            // Ignore e
            return false;
        }
    }

    /**
     * Recognizes an expression from a syntactical point of view.
     * <p>
     * If the expression is syntactically invalid, an exception is raised.
     *
     * @param expression The expression to recognize.
     * @throws LexicalException When the expression is lexically invalid.
     * @throws SyntacticException When the expression is syntactically invalid.
     */
    public void recognize(String expression) {
        analyzer.analyze(expression, VOID);
    }
}