package cdc.applic.expressions.parsing;

import cdc.util.function.BitSetCharPredicate;
import cdc.util.function.BooleanArrayCharPredicate;
import cdc.util.function.CharPredicate;
import cdc.util.function.MultiplyShiftCharPredicate;

/**
 * Recognition of space characters.
 * <p>
 * <b>WARNING:</b> inside an escaped text, those chars should not be recognized as spaces.
 *
 * @author Damien Carbonne
 */
public final class Spaces {
    /**
     * String containing characters that are considered as spaces.
     */
    static final String CHARS = ""
            + "\t"     // HORIZONTAL TABULATION (U+0009)
            + "\n"     // LINE FEED (U+000A)
            + "\u000B" // VERTICAL TABULATION
            + "\f"     // FORM FEED (U+000C)
            + "\r"     // CARRIAGE RETURN (U+000D)
            + "\u0020" // SPACE
            + "\u0085" // NEXT LINE
            + "\u00A0" // NO-BREAK SPACE
            + "\u1680" // OGHAM SPACE MARK
            + "\u180E" // MONGOLIAN VOWEL SEPARATOR
            + "\u2000" // EN QUAD
            + "\u2001" // EM QUAD
            + "\u2002" // EN SPACE
            + "\u2003" // EM SPACE
            + "\u2004" // THREE-PER-EM SPACE
            + "\u2005" // FOUR-PER-EM SPACE
            + "\u2006" // SIX-PER-EM SPACE
            + "\u2007" // FIGURE SPACE
            + "\u2008" // PUNCTUATION SPACE
            + "\u2009" // THIN SPACE
            + "\u200A" // HAIR SPACE
            + "\u200B" // ZERO WIDTH SPACE
            + "\u2028" // LINE SEPARATOR
            + "\u2029" // PARAGRAPH SEPARATOR
            + "\u202F" // NARROW NO-BREAK SPACE
            + "\u205F" // MEDIUM MATHEMATICAL SPACE
            + "\u3000" // IDEOGRAPHIC SPACE
            + "\u303F" // IDEOGRAPHIC HALF FILL SPACE
            + "\uFEFF";// ZERO WIDTH NO-BREAK SPACE

    /**
     * Perfect hash table for {@link #CHARS}.
     * <p>
     * ' ' is used as the filling character.<br>
     * <b>WARNING:</b> This is computed and must match {@link #CHARS}.
     */
    static final String STABLE =
            "\u2002\u2028\u0085\r\u0020\n \u2009\u202f\u2006\u2003\u2029\u2000 \u205f\u000b\u303f\u200a\u3000\u2007\u00a0\u2004\u2001\u180e\u1680\f\ufeff\t\u200b\u2008 \u2005";

    /**
     * Conversion of {@link #STABLE} as a char array.
     */
    static final char[] TABLE = STABLE.toCharArray();

    /**
     * The multiplier to use for {@link #TABLE}.
     */
    static final int MULTIPLY = 1_359_161_067;

    /**
     * The shift to use for {@link #TABLE}.
     */
    static final int SHIFT = 27;

    private Spaces() {
    }

    public static final CharPredicate MULTIPLY_SHIFT_MATCHER =
            new MultiplyShiftCharPredicate(STABLE,
                                           MULTIPLY,
                                           SHIFT);

    public static final CharPredicate MULTIPLY_SHIFT_INLINE_MATCHER =
            c -> TABLE[(c * MULTIPLY) >>> SHIFT] == c;

    public static final CharPredicate BIT_SET_MATCHER =
            new BitSetCharPredicate(CHARS);

    public static final CharPredicate BOOLEAN_ARRAY_MATCHER =
            new BooleanArrayCharPredicate(CHARS);

    /**
     * Matcher that must be used to identify one character tokens.
     */
    public static final CharPredicate BEST_MATCHER = MULTIPLY_SHIFT_INLINE_MATCHER;
}