package cdc.applic.expressions.parsing;

public class ParsingEvent {
    private final Token token;
    private final Info info;

    public enum Info {
        VALUE,
        LOW_VALUE,
        HIGH_VALUE,
        REF_PREFIX,
        REF_LOCAL_NAME,
        PROPERTY_PREFIX,
        PROPERTY_LOCAL_NAME,
        OTHER
    }

    public ParsingEvent(Token token,
                        Info info) {
        this.token = token;
        this.info = info;
    }

    public Token getToken() {
        return token;
    }

    public Info getInfo() {
        return info;
    }

    @Override
    public String toString() {
        return "[" + token + " " + info + "]";
    }
}