package cdc.applic.expressions.parsing;

import cdc.util.lang.Checks;

/**
 * Enumeration of operators (IMPLICATION, EQUIVALENCE, OR, AND, NOT) for Parsing stack, plus a sentinel.
 * <p>
 * Note that we don't consider equality and inclusion as operators, here.
 * Their corresponding nodes, in AST, are leaves.
 *
 * @author Damien Carbonne
 *
 */
public enum OperatorType {
    /** Sentinel operator, with lowest precedence. */
    SENTINEL(0),
    /** Equivalence operator. */
    EQUIVALENCE(1),
    /** Xor operator. */
    XOR(1),
    /** Implication operator. */
    IMPLICATION(2),
    /** Logical binary or operator. */
    OR(3),
    /** Logical binary and operator. */
    AND(4),
    /** Logical not operator. */
    NOT(5);

    /** Precedence of the operator. */
    private final int precedence;

    private OperatorType(int precedence) {
        this.precedence = precedence;
    }

    public int getPrecedence() {
        return precedence;
    }

    /**
     * Converts a TokenType to OperatorType.
     *
     * @param type The TokenType.
     * @return The corresponding OperatorType.
     * @throws IllegalArgumentException when {@code type} is {@code null} or can not be converted.
     */
    public static OperatorType from(TokenType type) {
        Checks.isNotNull(type, "type");
        switch (type) {
        case AND:
            return OperatorType.AND;
        case OR:
            return OperatorType.OR;
        case NOT:
            return OperatorType.NOT;
        case EQUIV:
            return OperatorType.EQUIVALENCE;
        case XOR:
            return OperatorType.XOR;
        case IMPL:
            return OperatorType.IMPLICATION;
        default:
            throw new IllegalArgumentException("Can not convert " + type + " to OperatorType");
        }
    }
}