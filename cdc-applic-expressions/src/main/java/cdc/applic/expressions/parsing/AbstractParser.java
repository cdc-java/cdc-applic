package cdc.applic.expressions.parsing;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

import cdc.applic.expressions.SyntacticException;

/**
 * Base class of Parser and Recognizer.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractParser {
    /** The tokenizer. */
    protected final Tokenizer tokenizer = new Tokenizer();

    /**
     * Tokens corresponding to items.
     * <p>
     * They can be used inside a set or as right member of an equality operator.
     */
    protected static final Set<TokenType> ITEM_TOKEN_TYPES;
    static {
        final Set<TokenType> tmp = EnumSet.noneOf(TokenType.class);
        for (final TokenType type : TokenType.values()) {
            if (type.isItem()) {
                tmp.add(type);
            }
        }
        ITEM_TOKEN_TYPES = Collections.unmodifiableSet(tmp);
    }

    protected static final Set<TokenType> TEXT_TOKEN_TYPES;
    static {
        TEXT_TOKEN_TYPES =
                Collections.unmodifiableSet(EnumSet.of(TokenType.TEXT,
                                                       TokenType.DOUBLE_QUOTES_TEXT));
    }

    /**
     * Tokens that mark the start of a set.
     */
    protected static final Set<TokenType> ITEM_SET_TOKEN_TYPES;
    static {
        ITEM_SET_TOKEN_TYPES =
                Collections.unmodifiableSet(EnumSet.of(TokenType.OPEN_SET,
                                                       TokenType.EMPTY_SET));
    }

    /**
     * Creates an InvalidExpression.
     *
     * @param detail The exception detail.
     * @param info Information describing the error.
     * @return A new InvalidExpression instance initialized with {@code detail} and {@code info}.
     */
    protected final SyntacticException newInvalidExpression(SyntacticException.Detail detail,
                                                            String info) {
        return new SyntacticException(detail,
                                      info,
                                      tokenizer.getExpression(),
                                      tokenizer.getBeginIndex(),
                                      tokenizer.getEndIndex());
    }

    /**
     * Checks that current token has an expected type.
     *
     * @param context A string describing the context where this method is
     *            called. Used in case of error.
     * @param strict If {@code true}, a strict interpretation is used.
     * @param type The expected token type.
     * @throws SyntacticException When the expression is invalid.
     */
    protected final void check(String context,
                               boolean strict,
                               TokenType type) {
        if (tokenizer.getTokenType() != type) {
            throw newInvalidExpression(SyntacticException.Detail.UNEXPECTED_TOKEN,
                                       "[" + context + "] Unexpected token: " + tokenizer.getToken() + ", expected: " + type);
        }
    }

    /**
     * Checks that current token has an expected type.
     * <p>
     * If so, consumes it. Otherwise, generates an error.
     *
     * @param context A string describing the context where this method is
     *            called. Used in case of error.
     * @param strict If {@code true}, a strict interpretation is used.
     * @param type The expected token type.
     * @throws SyntacticException When the expression is invalid.
     */
    protected final void expect(String context,
                                boolean strict,
                                TokenType type) {
        check(context, strict, type);
        tokenizer.next(strict);
    }

    /**
     * Checks that current token has an expected type among several ones.
     *
     * @param context A string describing the context where this method is
     *            called. Used in case of error.
     * @param strict If {@code true}, a strict interpretation is used.
     * @param types The expected token types.
     * @throws SyntacticException When the expression is invalid.
     */
    protected final void check(String context,
                               boolean strict,
                               Set<TokenType> types) {
        if (!types.contains(tokenizer.getTokenType())) {
            // No matching token found
            final StringBuilder builder = new StringBuilder();
            builder.append('[');
            builder.append(context);
            builder.append("] Unexpected token: ");
            builder.append(tokenizer.getToken());
            builder.append(", expected one of:");
            for (final TokenType type : types) {
                builder.append(" ");
                builder.append(type);
            }
            throw newInvalidExpression(SyntacticException.Detail.UNEXPECTED_TOKEN,
                                       builder.toString());
        }
    }

    /**
     * Checks that current token has an expected type among several ones.
     * <p>
     * If so, consumes it. Otherwise,generates an error.
     *
     * @param context A string describing the context where this method is
     *            called. Used in case of error.
     * @param strict If {@code true}, a strict interpretation is used.
     * @param types The expected token types.
     * @throws SyntacticException When the expression is invalid.
     */
    protected final void expect(String context,
                                boolean strict,
                                Set<TokenType> types) {
        check(context, strict, types);
        tokenizer.next(strict);
    }
}