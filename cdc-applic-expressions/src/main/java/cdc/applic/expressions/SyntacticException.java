package cdc.applic.expressions;

/**
 * Exception raised to indicate that an expression is syntactically invalid.
 * <p>
 * Information on the location of the problem is embedded.
 * <p>
 * Error is found during parsing.
 *
 * @author Damien Carbonne
 */
public class SyntacticException extends ExpressionException {
    private static final long serialVersionUID = 1L;
    private final Detail detail;

    /**
     * Enumeration of exception details.
     *
     * @author Damien Carbonne
     */
    public enum Detail {
        /** During parsing, an invalid range is found. */
        INVALID_RANGE,
        /** During parsing, an unexpected token is found. */
        UNEXPECTED_TOKEN,
        /** During parsing, end of expression is reached and more tokens are expected. */
        UNEXPECTED_END
    }

    public SyntacticException(Detail detail,
                              String info,
                              String expression,
                              int beginIndex,
                              int endIndex) {
        super(info, expression, beginIndex, endIndex);

        this.detail = detail;
    }

    public SyntacticException(Detail detail,
                              String info,
                              String expression) {
        this(detail,
             info,
             expression,
             0,
             expression.length());
    }

    public SyntacticException(Detail detail,
                              String expression) {
        this(detail,
             DEFAULT_INFO,
             expression);
    }

    /**
     * @return The exception detail.
     */
    public Detail getDetail() {
        return detail;
    }

    @Override
    public String getFullMessage() {
        final StringBuilder builder = new StringBuilder();
        builder.append(getDetail());
        builder.append(" ");
        builder.append(getInfo());
        builder.append("\n");
        builder.append("   '" + getExpression() + "'\n");
        builder.append("    " + getSlice());
        return builder.toString();
    }
}