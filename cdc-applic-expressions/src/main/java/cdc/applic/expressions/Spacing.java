package cdc.applic.expressions;

/**
 * Enumeration of possible spacings to write an expression.
 *
 * @author Damien Carbonne
 */
public enum Spacing {
    /**
     * All non-required spaces should be avoided.
     */
    NARROW,
    /**
     * Spaces should be inserted, even when not needed, to make expression appear better.
     */
    WIDE
}