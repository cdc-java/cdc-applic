package cdc.applic.expressions;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Simple Expression pool that can be used to share Expressions.
 * <p>
 * An Expression is functionally immutable.
 *
 * @author Damien Carbonne
 */
public class ExpressionPool {
    private final Map<String, Expression> map = new HashMap<>();

    public void clear() {
        map.clear();
    }

    /**
     * @return The set of contents to which an expression is associated.
     */
    public Set<String> getExpressionContents() {
        return map.keySet();
    }

    /**
     * @param content The content.
     * @return The shared Expression associated to {@code content}.
     */
    public Expression get(String content) {
        return map.computeIfAbsent(content, Expression::new);
    }

    /**
     * @param expression The Expression.
     * @return The shared Expression associated to {@code expression}.
     */
    public Expression get(Expression expression) {
        return get(expression.getContent());
    }
}