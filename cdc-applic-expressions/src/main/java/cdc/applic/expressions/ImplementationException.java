package cdc.applic.expressions;

public class ImplementationException extends ApplicException {
    private static final long serialVersionUID = 1L;

    public ImplementationException(String message,
                                   Throwable cause) {
        super(message, cause);
    }

    public ImplementationException(String message) {
        this(message, null);
    }
}