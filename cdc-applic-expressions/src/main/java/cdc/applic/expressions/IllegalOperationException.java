package cdc.applic.expressions;

public class IllegalOperationException extends ApplicException {
    private static final long serialVersionUID = 1L;

    public IllegalOperationException(String message,
                                     Throwable cause) {
        super(message, cause);
    }

    public IllegalOperationException(String message) {
        this(message, null);
    }
}