package cdc.applic.expressions;

/**
 * Exception raised to indicate that an expression is lexically invalid.
 * <p>
 * Information on the location of the problem is embedded.
 * <p>
 * Error is found during tokenizing.
 *
 * @author Damien Carbonne
 */
public class LexicalException extends ExpressionException {
    private static final long serialVersionUID = -1139923600878287349L;
    private final Detail detail;

    /**
     * Enumeration of exception details.
     *
     * @author Damien Carbonne
     */
    public enum Detail {
        /** During tokenization, an invalid (real or integer) number is found. */
        INVALID_NUMBER,
        /** During tokenization, a (real or integer) number is not followed by a boundary. */
        MISSING_BOUNDARY,
        /** During tokenization, closing quotes are missing. */
        MISSING_CLOSING_DOUBLE_QUOTES,
        /** During tokenization, closing dollar is missing. */
        MISSING_CLOSING_DOLLAR,
        /** During tokenization, an empty '"' escaped text is found. */
        EMPTY_DOUBLE_QUOTES_ESCAPED_TEXT,
        /** During tokenization, an empty '$' escaped text is found. */
        EMPTY_DOLLAR_ESCAPED_TEXT
    }

    public LexicalException(Detail detail,
                            String info,
                            String expression,
                            int beginIndex,
                            int endIndex) {
        super(info, expression, beginIndex, endIndex);

        this.detail = detail;
    }

    /**
     * @return The exception detail.
     */
    public Detail getDetail() {
        return detail;
    }

    @Override
    public String getFullMessage() {
        final StringBuilder builder = new StringBuilder();
        builder.append(getDetail());
        builder.append(" ");
        builder.append(getInfo());
        builder.append("\n");
        builder.append("   '" + getExpression() + "'\n");
        builder.append("    " + getSlice());
        return builder.toString();
    }
}