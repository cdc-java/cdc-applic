package cdc.applic.expressions.ast;

import java.util.Optional;

import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.applic.expressions.parsing.ComparisonOperator;

public final class GreaterOrEqualNode extends AbstractValueNode implements InequalityNode {
    public static final String KIND = "GREATER_OR_EQUAL";

    public GreaterOrEqualNode(Name propertyName,
                              Value value) {
        super(propertyName,
              value);
    }

    @Override
    public ComparisonOperator getComparisonOperator() {
        return ComparisonOperator.GREATER_OR_EQUAL;
    }

    @Override
    public boolean isNegative() {
        return false;
    }

    @Override
    public NeitherGreaterNorEqualNode negate() {
        return new NeitherGreaterNorEqualNode(getName(), getValue());
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_GREATER_OR_EQUAL;
    }

    @Override
    public GreaterOrEqualNode create(Name propertyName,
                                     Value value) {
        return new GreaterOrEqualNode(propertyName, value);
    }

    @Override
    public GreaterOrEqualNode setName(Name name) {
        return new GreaterOrEqualNode(name, getValue());
    }

    @Override
    public GreaterOrEqualNode setPrefixIfMissing(Optional<SName> prefix) {
        return getName().hasPrefix() || prefix.isEmpty()
                ? this
                : new GreaterOrEqualNode(getName().setPrefix(prefix), getValue());
    }

    @Override
    public GreaterOrEqualNode removePrefix() {
        return getName().hasPrefix()
                ? new GreaterOrEqualNode(getName().removePrefix(), getValue())
                : this;
    }
}