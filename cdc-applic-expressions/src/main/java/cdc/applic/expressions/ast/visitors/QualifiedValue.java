package cdc.applic.expressions.ast.visitors;

import java.util.Objects;

/**
 * (value, quality) pair.
 *
 * @author Damien Carbonne
 *
 * @param <T> The value type.
 */
public class QualifiedValue<T> {
    private final T value;
    private final Quality quality;

    /**
     * Enumeration of possible qualities.
     * <p>
     * <b>WARNING:</b> order of declarations matters.
     */
    public enum Quality {
        SUCCESS,
        PARTIAL,
        FAILURE;

        public Quality merge(Quality other) {
            return ordinal() >= other.ordinal()
                    ? this
                    : other;
        }
    }

    public QualifiedValue(T value,
                          Quality quality) {
        this.value = value;
        this.quality = quality;
    }

    public T getValue() {
        return value;
    }

    public Quality getQuality() {
        return quality;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value,
                            quality);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof QualifiedValue)) {
            return false;
        }
        final QualifiedValue<?> other = (QualifiedValue<?>) object;
        return Objects.equals(value, other.value)
                && quality == other.quality;
    }

    @Override
    public String toString() {
        return "[" + value + ", " + quality + "]";
    }
}