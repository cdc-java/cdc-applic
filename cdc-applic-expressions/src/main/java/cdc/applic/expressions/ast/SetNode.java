package cdc.applic.expressions.ast;

import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.literals.Name;

public interface SetNode extends PropertyNode {

    @Override
    public abstract SetNode negate();

    /**
     * @return The set of items.
     */
    public SItemSet getSet();

    /**
     * @return The checked set of items.<br>
     *         It may be {@code null} if set can not be converted to a checked set.
     */
    public SItemSet getCheckedSet();

    /**
     * @return The best set of items.<br>
     *         It is a checked set if possible.
     */
    public SItemSet getBestSet();

    public abstract SetNode create(Name propertyName,
                                   SItemSet set);
}