package cdc.applic.expressions.ast;

import java.io.PrintStream;
import java.util.Optional;

import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.util.debug.Printables;

/**
 * A RefNode represents a boolean property or an alias.
 */
public final class RefNode extends AbstractNamedNode implements ParsingNode, SortableNode, AtomNode, RenamableNode {
    public static final String KIND = "REF";
    private int hash = 0;

    public RefNode(Name name) {
        super(name);
    }

    public RefNode(String literal,
                   boolean escaped) {
        this(Name.of(literal, escaped));
    }

    public RefNode(String literal) {
        this(literal, false);
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_ATOM;
    }

    @Override
    public final void buildInfix(Formatting formatting,
                                 StringBuilder builder) {
        builder.append(getName());
    }

    @Override
    public String getSortingText() {
        return getName().getSortingLiteral();
    }

    @Override
    public RefNode setName(Name name) {
        return new RefNode(name);
    }

    @Override
    public RefNode setPrefixIfMissing(Optional<SName> prefix) {
        return getName().hasPrefix() || prefix.isEmpty()
                ? this
                : new RefNode(getName().setPrefix(prefix));
    }

    @Override
    public RefNode removePrefix() {
        return getName().hasPrefix()
                ? new RefNode(getName().removePrefix())
                : this;
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        Printables.indent(out, level);
        out.println(getKind() + " " + getName());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof RefNode)) {
            return false;
        }
        final RefNode other = (RefNode) object;
        return name.equals(other.name);
    }

    @Override
    public int hashCode() {
        if (hash == 0) {
            hash = name.hashCode();
        }
        return hash;
    }

    @Override
    public String toString() {
        return getKind() + "(" + getName() + ")";
    }
}