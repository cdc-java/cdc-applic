package cdc.applic.expressions.ast;

/**
 * Base interface of inequality nodes:
 * <ul>
 * <li>&pi; &lt; &upsilon;
 * <li>&pi; &le; &upsilon;
 * <li>&pi; &gt; &upsilon;
 * <li>&pi; &ge; &upsilon;
 * <li>&pi; &nlt; &upsilon;
 * <li>&pi; &nle; &upsilon;
 * <li>&pi; &ngt; &upsilon;
 * <li>&pi; &nge; &upsilon;
 * </ul>
 *
 * @author Damien Carbonne
 */
public interface InequalityNode extends ValueNode {
    // Ignore
}