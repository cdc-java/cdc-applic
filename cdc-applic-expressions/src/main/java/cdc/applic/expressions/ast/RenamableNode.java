package cdc.applic.expressions.ast;

import java.util.Optional;

import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;

/**
 * Interface implemented by nodes that can be renamed.
 *
 * @author Damien Carbonne
 */
public interface RenamableNode extends NamedNode {

    public RenamableNode setName(Name name);

    /**
     * @param prefix The prefix.
     * @return This node if its name contains a prefix, or a new node whose name contains {@code prefix}.
     */
    public RenamableNode setPrefixIfMissing(Optional<SName> prefix);

    /**
     * @return The node i its name has no prefix, or a new node whose name has no prefix.
     */
    public RenamableNode removePrefix();
}