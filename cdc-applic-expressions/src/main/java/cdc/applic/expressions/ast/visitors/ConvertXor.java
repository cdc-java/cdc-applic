package cdc.applic.expressions.ast.visitors;

import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.expressions.ast.XorNode;

/**
 * Utility class used to convert <em>xor</em> nodes (&nharr;) by recursively applying this
 * rewriting rule:
 * <ul>
 * <li>&alpha; &nharr; &beta; &equiv; (&alpha; &or; &beta;) &and; &not; (&beta; &and; &alpha;)
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class ConvertXor extends AbstractConverter {
    private static final ConvertXor CONVERTER = new ConvertXor();

    private ConvertXor() {
    }

    public static Node execute(Node node) {
        return node.accept(CONVERTER);
    }

    @Override
    public Node visitBinary(AbstractBinaryNode node) {
        final Node result;
        if (node instanceof XorNode) {
            // We could use:
            // (alpha | beta) & (!alpha | !beta)
            // (alpha | beta) & !(alpha & beta)
            // (!alpha & beta) | (alpha & !beta)
            final Node alpha = node.getAlpha().accept(this);
            final Node beta = node.getBeta().accept(this);
            final Node alphaOrBeta = new OrNode(alpha, beta);
            final Node alphaAndBeta = new AndNode(beta, alpha);

            result = new AndNode(alphaOrBeta, new NotNode(alphaAndBeta));
        } else {
            result = super.visitBinary(node);
        }
        return result;
    }
}