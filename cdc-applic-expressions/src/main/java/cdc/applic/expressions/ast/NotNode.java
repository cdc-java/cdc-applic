package cdc.applic.expressions.ast;

public final class NotNode extends AbstractUnaryNode implements NegativeNode {
    public static final String KIND = "NOT";

    public NotNode(Node alpha) {
        super(alpha);
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_NOT;
    }

    @Override
    public final NotNode create(Node alpha) {
        return new NotNode(alpha);
    }

    public static Node simplestNot(Node node) {
        if (node instanceof NotNode) {
            return ((NotNode) node).getAlpha();
        } else {
            return new NotNode(node);
        }
    }
}