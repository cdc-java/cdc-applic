package cdc.applic.expressions.ast;

import java.util.Optional;

import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.applic.expressions.parsing.ComparisonOperator;

public final class NotLessNode extends AbstractValueNode implements InequalityNode, NegativeNode {
    public static final String KIND = "NOT_LESS";

    public NotLessNode(Name propertyName,
                       Value value) {
        super(propertyName,
              value);
    }

    @Override
    public ComparisonOperator getComparisonOperator() {
        return ComparisonOperator.NOT_LESS;
    }

    @Override
    public boolean isNegative() {
        return true;
    }

    @Override
    public LessNode negate() {
        return new LessNode(getName(), getValue());
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_NOT_LESS;
    }

    @Override
    public NotLessNode create(Name propertyName,
                              Value value) {
        return new NotLessNode(propertyName, value);
    }

    @Override
    public NotLessNode setName(Name name) {
        return new NotLessNode(name, getValue());
    }

    @Override
    public NotLessNode setPrefixIfMissing(Optional<SName> prefix) {
        return getName().hasPrefix() || prefix.isEmpty()
                ? this
                : new NotLessNode(getName().setPrefix(prefix), getValue());
    }

    @Override
    public NotLessNode removePrefix() {
        return getName().hasPrefix()
                ? new NotLessNode(getName().removePrefix(), getValue())
                : this;
    }
}