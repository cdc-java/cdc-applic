package cdc.applic.expressions.ast;

/**
 * Marking interface of nodes that are produced during parsing.
 *
 * @author Damien Carbonne
 */
public interface ParsingNode extends Node {
    // Ignore
}