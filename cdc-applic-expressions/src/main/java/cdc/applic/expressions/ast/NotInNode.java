package cdc.applic.expressions.ast;

import java.util.Optional;

import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;

public final class NotInNode extends AbstractSetNode implements NegativeNode {
    public static final String KIND = "NOT_IN";

    public NotInNode(Name propertyName,
                     SItemSet set) {
        super(propertyName,
              set);
    }

    @Override
    public boolean isNegative() {
        return true;
    }

    @Override
    public InNode negate() {
        return new InNode(getName(), getSet());
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_NOT_IN;
    }

    @Override
    public NotInNode create(Name propertyName,
                            SItemSet set) {
        return new NotInNode(propertyName, set);
    }

    @Override
    public NotInNode setName(Name name) {
        return new NotInNode(name, getSet());
    }

    @Override
    public NotInNode setPrefixIfMissing(Optional<SName> prefix) {
        return getName().hasPrefix() || prefix.isEmpty()
                ? this
                : new NotInNode(getName().setPrefix(prefix), getSet());
    }

    @Override
    public NotInNode removePrefix() {
        return getName().hasPrefix()
                ? new NotInNode(getName().removePrefix(), getSet())
                : this;
    }
}