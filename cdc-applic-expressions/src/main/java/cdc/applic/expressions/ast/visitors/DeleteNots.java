package cdc.applic.expressions.ast.visitors;

import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;

/**
 * A visitor that deletes {@link NotNode}s.
 * <p>
 * <b>WARNING:</b> This must be used with care!
 *
 * @author Damien Carbonne
 */
public final class DeleteNots extends AbstractConverter {
    private static final DeleteNots VISITOR = new DeleteNots();

    private DeleteNots() {
    }

    public static Node execute(Node node) {
        return node.accept(VISITOR);
    }

    @Override
    public Node visitUnary(AbstractUnaryNode node) {
        return node.getAlpha().accept(this);
    }
}