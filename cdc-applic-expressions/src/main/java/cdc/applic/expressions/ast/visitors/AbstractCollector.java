package cdc.applic.expressions.ast.visitors;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import cdc.util.lang.Checks;

/**
 * Base class of collectors.
 * <p>
 * They collect information and accumulate it in a collection.
 *
 * @author Damien Carbonne
 *
 * @param <T> Type of the collected information.
 */
public class AbstractCollector<T> extends AbstractAnalyzer {
    protected final Collection<T> collection;

    protected AbstractCollector(Collection<T> collection) {
        Checks.isNotNull(collection, "collection");
        this.collection = collection;
    }

    protected void add(T element) {
        collection.add(element);
    }

    protected void addAll(Collection<? extends T> elements) {
        collection.addAll(elements);
    }

    /**
     * @return The collection.
     */
    public Collection<T> getCollection() {
        return collection;
    }

    /**
     * @return The collection as a set.
     */
    public Set<T> getSet() {
        return (Set<T>) collection;
    }

    /**
     * @return The collection as a list.
     */
    public List<T> getList() {
        return (List<T>) collection;
    }
}