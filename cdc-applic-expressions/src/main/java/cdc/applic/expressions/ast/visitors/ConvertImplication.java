package cdc.applic.expressions.ast.visitors;

import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.EquivalenceNode;
import cdc.applic.expressions.ast.ImplicationNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.OrNode;

/**
 * Utility class used to convert <em>implication</em> nodes (&rarr;) by recursively applying this rewriting
 * rule:
 * <ul>
 * <li>&alpha; &rarr; &beta; &equiv; &not; &alpha; &or; &beta;
 * </ul>
 *
 * <b>Warning</b>: This should be called:
 * <ul>
 * <li>after equivalences (&harr;) have been converted using {@link ConvertEquivalence}.
 * </ul>
 *
 * <b>Note</b>: This is an adaptation of AIMA Java implementation.
 *
 * @author Damien Carbonne
 */
public final class ConvertImplication extends AbstractConverter {
    private static final ConvertImplication CONVERTER = new ConvertImplication();

    private ConvertImplication() {
    }

    public static Node execute(Node node) {
        return node.accept(CONVERTER);
    }

    @Override
    public Node visitBinary(AbstractBinaryNode node) {
        final Node result;
        if (node instanceof ImplicationNode) {
            final Node alpha = node.getAlpha().accept(this);
            final Node beta = node.getBeta().accept(this);
            final Node notAlpha = new NotNode(alpha);

            result = new OrNode(notAlpha, beta);
        } else if (node instanceof EquivalenceNode) {
            throw new IllegalArgumentException("Unexpected equivalence found");
        } else {
            result = super.visitBinary(node);
        }
        return result;
    }
}