package cdc.applic.expressions.ast.visitors;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.EquivalenceNode;
import cdc.applic.expressions.ast.ImplicationNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;

/**
 * Class used to keep certain nodes and remove others.
 * <p>
 * The resulting node is generally not equivalent to the input one.<br>
 * <b>WARNING:</b> This should be used with care. One can even question the usefulness of this class.
 *
 * @author Damien Carbonne
 */
public final class NodeFilter extends AbstractConverter {
    private final Predicate<? super Node> predicate;

    private NodeFilter(Predicate<? super Node> predicate) {
        this.predicate = predicate;
    }

    public static Node execute(Node node,
                               Predicate<? super Node> predicate) {
        final NodeFilter visitor = new NodeFilter(predicate);
        return node.accept(visitor);
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (predicate.test(node)) {
            return node;
        } else {
            return null;
        }
    }

    @Override
    public Node visitUnary(AbstractUnaryNode node) {
        if (predicate.test(node)) {
            // Apply filter on child
            final Node alpha = node.getAlpha().accept(this);
            if (alpha == null) {
                // !null ~~> null
                return null;
            } else {
                return super.visitUnary(node);
            }
        } else {
            return null;
        }
    }

    @Override
    public Node visitBinary(AbstractBinaryNode node) {
        if (predicate.test(node)) {
            // Apply filter on children
            final Node alpha = node.getAlpha().accept(this);
            final Node beta = node.getBeta().accept(this);
            if (alpha == null) {
                if (beta == null) {
                    // Both children are null
                    return null;
                } else {
                    if (node instanceof EquivalenceNode) {
                        // null <-> b ~~> b & !b
                        // Do not replace by false, on purpose
                        return new AndNode(beta, new NotNode(beta));
                    } else {
                        // null & b ~~> b
                        // null | b ~~> b
                        // null -> b ~~> b
                        return beta;
                    }
                }
            } else {
                if (beta == null) {
                    if (node instanceof ImplicationNode) {
                        // a -> null ~~> !a
                        return new NotNode(alpha);
                    } else if (node instanceof EquivalenceNode) {
                        // a <-> null ~~> a & !a
                        // Do not replace by false, on purpose
                        return new AndNode(alpha, new NotNode(alpha));
                    } else {
                        // a & null ~~> a
                        // a | null ~~> a
                        return alpha;
                    }
                } else {
                    // Both children are valid
                    return node.create(alpha, beta);
                }
            }
        } else {
            return null;
        }
    }

    @Override
    public Node visitNary(AbstractNaryNode node) {
        if (predicate.test(node)) {
            // Apply filter on children
            final List<Node> children = new ArrayList<>();
            for (final Node child : node.getChildren()) {
                final Node tmp = child.accept(this);
                if (tmp != null) {
                    children.add(tmp);
                }
            }
            if (children.isEmpty()) {
                return null;
            } else {
                return node.create(children);
            }
        } else {
            return null;
        }
    }
}