package cdc.applic.expressions.ast;

import java.util.Optional;

import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;

public final class InNode extends AbstractSetNode {
    public static final String KIND = "IN";

    public InNode(Name propertyName,
                  SItemSet set) {
        super(propertyName,
              set);
    }

    @Override
    public boolean isNegative() {
        return false;
    }

    @Override
    public NotInNode negate() {
        return new NotInNode(getName(), getSet());
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_IN;
    }

    @Override
    public InNode create(Name propertyName,
                         SItemSet set) {
        return new InNode(propertyName, set);
    }

    @Override
    public InNode setName(Name name) {
        return new InNode(name, getSet());
    }

    @Override
    public InNode setPrefixIfMissing(Optional<SName> prefix) {
        return getName().hasPrefix() || prefix.isEmpty()
                ? this
                : new InNode(getName().setPrefix(prefix), getSet());
    }

    @Override
    public InNode removePrefix() {
        return getName().hasPrefix()
                ? new InNode(getName().removePrefix(), getSet())
                : this;
    }
}