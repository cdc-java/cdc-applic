package cdc.applic.expressions.ast;

public final class OrNode extends AbstractBinaryNode {
    public static final String KIND = "OR";

    public OrNode(Node alpha,
                  Node beta) {
        super(alpha,
              beta);
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_OR;
    }

    @Override
    public OrNode create(Node alpha,
                         Node beta) {
        return new OrNode(alpha, beta);
    }
}