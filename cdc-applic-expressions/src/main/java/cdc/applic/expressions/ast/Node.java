package cdc.applic.expressions.ast;

import java.util.Comparator;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.util.debug.Printable;

/**
 * Base interface of parsing and atom nodes.
 *
 * @author Damien Carbonne
 */
public interface Node extends Printable {
    public static final Comparator<Node> COMPARATOR =
            Comparator.comparing(Node::toString);

    /**
     * @return The kind of this node as a string.
     */
    public String getKind();

    /**
     * @return The number of children nodes.
     */
    public int getChildrenCount();

    /**
     * Returns the child located at an index.
     *
     * @param index The index.
     * @return The child at {@code index}.
     */
    public Node getChildAt(int index);

    /**
     * @return The height of the tree under this node. The height of a leaf node is 1.
     */
    public int getHeight();

    /**
     * @return The kerning of this node.
     */
    public NodeKerning getKerning();

    /**
     * @param formatting RThe formatting
     * @return An infix string representation of this node, using {@code formatting}.
     */
    public String toInfix(Formatting formatting);

    public default String compress() {
        return toInfix(Formatting.SHORT_NARROW);
    }

    public void buildInfix(Formatting formatting,
                           StringBuilder builder);

    public default Expression toExpression(Formatting formatting) {
        return new Expression(toInfix(formatting), false);
    }

    public default Expression toExpression() {
        return toExpression(Formatting.SHORT_NARROW);
    }

    public <R> R accept(Visitor<R> visitor);
}