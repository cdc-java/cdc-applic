package cdc.applic.expressions.ast;

public interface PropertyNode extends NamedNode, ParsingNode, SortableNode, RenamableNode {
    /**
     * @return {@code true} if this is a negative node.
     */
    public boolean isNegative();

    /**
     * @return The node with the negated definition.
     */
    public PropertyNode negate();

    @Override
    default String getSortingText() {
        return getName().getSortingLiteral();
    }
}