package cdc.applic.expressions.ast;

import java.util.function.Predicate;

public final class NodePredicates {
    private NodePredicates() {
    }

    /**
     * Predicate that returns {@code true} for all nodes.
     */
    public static final Predicate<Node> IS_NODE = node -> true;

    /**
     * Predicate that returns {@code true} when a node is an operator.
     */
    public static final Predicate<Node> IS_OPERATOR = AbstractOperatorNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is a {@link FalseNode}.
     */
    public static final Predicate<Node> IS_FALSE = FalseNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is a {@link TrueNode}.
     */
    public static final Predicate<Node> IS_TRUE = TrueNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is an {@link InformalNode}.
     */
    public static final Predicate<Node> IS_INFORMAL = InformalNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is a {@link AbstractLeafNode}.
     */
    public static final Predicate<Node> IS_LEAF = AbstractLeafNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is a {@link AbstractUnaryNode}.
     */
    public static final Predicate<Node> IS_UNARY = AbstractUnaryNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is a {@link AbstractBinaryNode}.
     */
    public static final Predicate<Node> IS_BINARY = AbstractBinaryNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is a {@link AbstractNaryNode}.
     */
    public static final Predicate<Node> IS_NARY = AbstractNaryNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is a {@link AndNode}.
     */
    public static final Predicate<Node> IS_BINARY_AND = AndNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is a {@link OrNode}.
     */
    public static final Predicate<Node> IS_BINARY_OR = OrNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is a {@link NaryAndNode}.
     */
    public static final Predicate<Node> IS_NARY_AND = NaryAndNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is a {@link NaryOrNode}.
     */
    public static final Predicate<Node> IS_NARY_OR = NaryOrNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is a {@link ImplicationNode}.
     */
    public static final Predicate<Node> IS_IMPLICATION = ImplicationNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is a {@link EquivalenceNode}.
     */
    public static final Predicate<Node> IS_EQUIVALENCE = EquivalenceNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is a {@link NotNode}.
     */
    public static final Predicate<Node> IS_NOT = NotNode.class::isInstance;

    public static final Predicate<Node> IS_IMPLICATION_OR_EQUIVALENCE =
            node -> node instanceof ImplicationNode
                    || node instanceof EquivalenceNode;

    /**
     * Predicate that returns {@code true} when a node is a {@link XorNode}.
     */
    public static final Predicate<Node> IS_XOR = XorNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is a {@link NegativeNode}.
     */
    public static final Predicate<Node> IS_NEGATIVE = NegativeNode.class::isInstance;

    /**
     * Predicate that returns {@code true} when a node is an {@link InequalityNode}.
     */
    public static final Predicate<Node> IS_INEQUALITY = InequalityNode.class::isInstance;
}