package cdc.applic.expressions.ast.visitors;

import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.Visitor;

/**
 * Base class of analyzers.
 * <p>
 * An analyzer is a visitor that can collect information on visited nodes,
 * without applying any transformation.<br>
 * Default implementations of visit methods simply traverse the tree without creating anything.
 *
 * @author Damien Carbonne
 */
public class AbstractAnalyzer implements Visitor<Void> {
    @Override
    public Void visitLeaf(AbstractLeafNode node) {
        return null;
    }

    @Override
    public Void visitUnary(AbstractUnaryNode node) {
        node.getAlpha().accept(this);
        return null;
    }

    @Override
    public Void visitBinary(AbstractBinaryNode node) {
        node.getAlpha().accept(this);
        node.getBeta().accept(this);
        return null;
    }

    @Override
    public Void visitNary(AbstractNaryNode node) {
        for (final Node child : node.getChildren()) {
            child.accept(this);
        }
        return null;
    }
}