package cdc.applic.expressions.ast;

import java.io.PrintStream;

import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.Spacing;
import cdc.util.debug.Printables;
import cdc.util.lang.Checks;

/**
 * Base class of unary nodes.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractUnaryNode extends AbstractOperatorNode {
    private final Node alpha;

    protected AbstractUnaryNode(Node alpha) {
        Checks.isNotNull(alpha, "alpha");
        this.alpha = alpha;
    }

    @Override
    public final int getChildrenCount() {
        return 1;
    }

    @Override
    public final Node getChildAt(int index) {
        if (index == 0) {
            return alpha;
        } else {
            throw new IllegalArgumentException("Invalid index (" + index + ")");
        }
    }

    public final Node getAlpha() {
        return alpha;
    }

    public abstract AbstractUnaryNode create(Node alpha);

    @Override
    public final int getHeight() {
        return 1 + alpha.getHeight();
    }

    @Override
    public final void print(PrintStream out,
                            int level) {
        Printables.indent(out, level);
        if (getAlpha() instanceof AbstractLeafNode) {
            out.print(getKind() + " ");
            getAlpha().print(out, 0);
        } else {
            out.println(getKind());
            getAlpha().print(out, level + 1);
        }
    }

    @Override
    public final void buildInfix(Formatting formatting,
                                 StringBuilder builder) {
        final boolean alphaParen = needParentheses(getAlpha());

        getKerning().addSymbol(builder, formatting);
        // Always use NARROW spacing if possible, whatever is asked
        getKerning().addSpace(builder, formatting.getSymbolType(), Spacing.NARROW, alphaParen);
        NodeKerning.addOpenParenthese(builder, alphaParen);
        getAlpha().buildInfix(formatting, builder);
        NodeKerning.addCloseParenthese(builder, alphaParen);
    }

    @Override
    public <R> R accept(Visitor<R> visitor) {
        return visitor.visitUnary(this);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof AbstractUnaryNode)) {
            return false;
        }
        final AbstractUnaryNode other = (AbstractUnaryNode) object;
        // We don't compare classes as NotNode is the only AbstractUnaryNode specialization
        return getAlpha().equals(other.getAlpha());
    }

    @Override
    public int hashCode() {
        return alpha.hashCode();
    }

    @Override
    public String toString() {
        return getKind() + "(" + getAlpha() + ")";
    }
}