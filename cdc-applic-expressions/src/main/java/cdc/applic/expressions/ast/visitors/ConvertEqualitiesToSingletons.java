package cdc.applic.expressions.ast.visitors;

import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.EqualityNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotInNode;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.content.Value;

/**
 * Converter that applies the following rewriting rules:
 * <ul>
 * <li>&pi;=&upsilon; &equiv; &pi;&isin;{&upsilon;}
 * <li>&pi;&ne;&upsilon; &equiv; &pi;&notin;{&upsilon;}
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class ConvertEqualitiesToSingletons extends AbstractConverter {
    private static final ConvertEqualitiesToSingletons CONVERTER = new ConvertEqualitiesToSingletons();

    private ConvertEqualitiesToSingletons() {
    }

    public static Node execute(Node node) {
        return node.accept(CONVERTER);
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (node instanceof EqualityNode) {
            final EqualityNode n = (EqualityNode) node;
            final Value value = n.getValue();
            final SItemSet set = SItemSetUtils.createBest(value);
            return node instanceof EqualNode
                    ? new InNode(n.getName(), set)
                    : new NotInNode(n.getName(), set);
        } else {
            return node;
        }
    }
}