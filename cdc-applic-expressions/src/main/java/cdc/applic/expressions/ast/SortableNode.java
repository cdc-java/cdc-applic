package cdc.applic.expressions.ast;

/**
 * Base interface of nodes that can be sorted.
 * <p>
 * This corresponds to all {@link ParsingNode}s that are also {@link LeafNode}s.
 *
 * @author Damien Carbonne
 */
public interface SortableNode extends Node {
    /**
     * @return The text that must be used to sort nodes.<br>
     *         For a named nodes, it should be the node name.
     */
    public String getSortingText();
}