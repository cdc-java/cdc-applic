package cdc.applic.expressions.ast;

import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.parsing.ComparisonOperator;

public interface ValueNode extends PropertyNode {
    @Override
    public ValueNode negate();

    public ComparisonOperator getComparisonOperator();

    public ValueNode create(Name propertyName,
                            Value value);

    public Value getValue();
}