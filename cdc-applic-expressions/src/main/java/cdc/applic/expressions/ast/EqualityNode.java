package cdc.applic.expressions.ast;

/**
 * Base interface of equality nodes:
 * <ul>
 * <li>&pi; = &upsilon;
 * <li>&pi; &ne; &upsilon;
 * </ul>
 *
 * @author Damien Carbonne
 */
public interface EqualityNode extends ValueNode {
    // Ignore
}