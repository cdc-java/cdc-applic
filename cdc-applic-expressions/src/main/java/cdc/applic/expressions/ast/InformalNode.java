package cdc.applic.expressions.ast;

import java.io.PrintStream;

import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.literals.InformalText;
import cdc.util.debug.Printables;
import cdc.util.lang.Checks;

/**
 * Representation of an informal leaf expression.
 */
public final class InformalNode extends AbstractLeafNode implements ParsingNode, SortableNode, AtomNode {
    public static final String KIND = "INFORMAL";
    private final InformalText text;
    private int hash = 0;

    public InformalNode(InformalText text) {
        this.text = Checks.isNotNull(text, "text");
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_ATOM;
    }

    public InformalText getText() {
        return text;
    }

    public String getEscapedText() {
        return text.getEscapedText();
    }

    @Override
    public final void buildInfix(Formatting formatting,
                                 StringBuilder builder) {
        builder.append(getEscapedText());
    }

    @Override
    public String getSortingText() {
        return getEscapedText();
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        Printables.indent(out, level);
        out.println(getKind() + " " + getText());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof InformalNode)) {
            return false;
        }
        final InformalNode other = (InformalNode) object;
        return text.equals(other.text);
    }

    @Override
    public int hashCode() {
        if (hash == 0) {
            hash = text.hashCode();
        }
        return hash;
    }

    @Override
    public String toString() {
        return getKind() + "(" + getText() + ")";
    }
}