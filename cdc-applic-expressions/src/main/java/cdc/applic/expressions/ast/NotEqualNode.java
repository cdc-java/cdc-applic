package cdc.applic.expressions.ast;

import java.util.Optional;

import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.applic.expressions.parsing.ComparisonOperator;

public final class NotEqualNode extends AbstractValueNode implements EqualityNode, NegativeNode {
    public static final String KIND = "NOT_EQUAL";

    public NotEqualNode(Name propertyName,
                        Value value) {
        super(propertyName,
              value);
    }

    @Override
    public ComparisonOperator getComparisonOperator() {
        return ComparisonOperator.NOT_EQUAL;
    }

    @Override
    public boolean isNegative() {
        return true;
    }

    @Override
    public EqualNode negate() {
        return new EqualNode(getName(), getValue());
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_NOT_EQUAL;
    }

    @Override
    public NotEqualNode create(Name propertyName,
                               Value value) {
        return new NotEqualNode(propertyName, value);
    }

    @Override
    public NotEqualNode setName(Name name) {
        return new NotEqualNode(name, getValue());
    }

    @Override
    public NotEqualNode setPrefixIfMissing(Optional<SName> prefix) {
        return getName().hasPrefix() || prefix.isEmpty()
                ? this
                : new NotEqualNode(getName().setPrefix(prefix), getValue());
    }

    @Override
    public NotEqualNode removePrefix() {
        return getName().hasPrefix()
                ? new NotEqualNode(getName().removePrefix(), getValue())
                : this;
    }
}