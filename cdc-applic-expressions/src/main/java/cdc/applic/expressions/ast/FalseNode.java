package cdc.applic.expressions.ast;

import java.io.PrintStream;

import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.literals.Name;
import cdc.util.debug.Printables;

public final class FalseNode extends AbstractNamedNode implements ParsingNode, SortableNode, AtomNode, NegativeNode {
    public static final String KIND = "FALSE";
    public static final Name FALSE = Name.of("false", false);
    public static final FalseNode INSTANCE = new FalseNode();

    private FalseNode() {
        super(FALSE);
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_FALSE;
    }

    @Override
    public final void buildInfix(Formatting formatting,
                                 StringBuilder builder) {
        getKerning().addSymbol(builder, formatting);
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        Printables.indent(out, level);
        out.println(getKind());
    }

    @Override
    public String getSortingText() {
        return getName().getSortingLiteral();
    }

    @Override
    public boolean equals(Object object) {
        // There is only one instance of this class
        return this == object;
    }

    @Override
    public int hashCode() {
        return getKind().hashCode();
    }

    @Override
    public String toString() {
        return getKind();
    }
}