package cdc.applic.expressions.ast;

import java.util.Optional;

import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.applic.expressions.parsing.ComparisonOperator;

public final class LessOrEqualNode extends AbstractValueNode implements InequalityNode {
    public static final String KIND = "LESS_OR_EQUAL";

    public LessOrEqualNode(Name propertyName,
                           Value value) {
        super(propertyName,
              value);
    }

    @Override
    public ComparisonOperator getComparisonOperator() {
        return ComparisonOperator.LESS_OR_EQUAL;
    }

    @Override
    public boolean isNegative() {
        return false;
    }

    @Override
    public NeitherLessNorEqualNode negate() {
        return new NeitherLessNorEqualNode(getName(), getValue());
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_LESS_OR_EQUAL;
    }

    @Override
    public LessOrEqualNode create(Name propertyName,
                                  Value value) {
        return new LessOrEqualNode(propertyName, value);
    }

    @Override
    public LessOrEqualNode setName(Name name) {
        return new LessOrEqualNode(name, getValue());
    }

    @Override
    public LessOrEqualNode setPrefixIfMissing(Optional<SName> prefix) {
        return getName().hasPrefix() || prefix.isEmpty()
                ? this
                : new LessOrEqualNode(getName().setPrefix(prefix), getValue());
    }

    @Override
    public LessOrEqualNode removePrefix() {
        return getName().hasPrefix()
                ? new LessOrEqualNode(getName().removePrefix(), getValue())
                : this;
    }
}