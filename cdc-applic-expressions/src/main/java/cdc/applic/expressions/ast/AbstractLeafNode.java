package cdc.applic.expressions.ast;

/**
 * Base class of leaf nodes.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractLeafNode extends AbstractNode implements LeafNode {
    @Override
    public final int getChildrenCount() {
        return 0;
    }

    @Override
    public final Node getChildAt(int index) {
        throw new IllegalArgumentException("Invalid index (" + index + ")");
    }

    @Override
    public final int getHeight() {
        return 1;
    }

    @Override
    public <R> R accept(Visitor<R> visitor) {
        return visitor.visitLeaf(this);
    }
}