package cdc.applic.expressions.ast;

public final class EquivalenceNode extends AbstractBinaryNode {
    public static final String KIND = "EQUIVALENCE";

    public EquivalenceNode(Node alpha,
                           Node beta) {
        super(alpha,
              beta);
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_EQUIV;
    }

    @Override
    public final EquivalenceNode create(Node alpha,
                                        Node beta) {
        return new EquivalenceNode(alpha, beta);
    }
}