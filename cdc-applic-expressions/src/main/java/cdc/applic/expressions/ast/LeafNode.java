package cdc.applic.expressions.ast;

/**
 * Marking interface of nodes that are leaves.
 */
public interface LeafNode extends Node {
    // Ignore
}