package cdc.applic.expressions.ast;

import java.util.Optional;

import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.applic.expressions.parsing.ComparisonOperator;

public final class GreaterNode extends AbstractValueNode implements InequalityNode {
    public static final String KIND = "GREATER";

    public GreaterNode(Name propertyName,
                       Value value) {
        super(propertyName,
              value);
    }

    @Override
    public ComparisonOperator getComparisonOperator() {
        return ComparisonOperator.GREATER;
    }

    @Override
    public boolean isNegative() {
        return false;
    }

    @Override
    public NotGreaterNode negate() {
        return new NotGreaterNode(getName(), getValue());
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_GREATER;
    }

    @Override
    public GreaterNode create(Name propertyName,
                              Value value) {
        return new GreaterNode(propertyName, value);
    }

    @Override
    public GreaterNode setName(Name name) {
        return new GreaterNode(name, getValue());
    }

    @Override
    public GreaterNode setPrefixIfMissing(Optional<SName> prefix) {
        return getName().hasPrefix() || prefix.isEmpty()
                ? this
                : new GreaterNode(getName().setPrefix(prefix.get()), getValue());
    }

    @Override
    public GreaterNode removePrefix() {
        return getName().hasPrefix()
                ? new GreaterNode(getName().removePrefix(), getValue())
                : this;
    }
}