package cdc.applic.expressions.ast.visitors;

import java.util.function.Predicate;

import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.Node;
import cdc.util.lang.Checks;

/**
 * Utility class used to check whether an AST matches or not a predicate.
 *
 * @author Damien Carbonne
 */
public final class PredicateMatcher extends AbstractAnalyzer {
    public boolean found = false;
    private final Predicate<? super Node> predicate;

    private PredicateMatcher(Predicate<? super Node> predicate) {
        this.predicate = predicate;
    }

    /**
     * Returns {@code true} if at least one node in the tree rooted in a node matches a Predicate.
     *
     * @param node The root node of traversal.
     * @param predicate The predicate.
     * @return {@code true} if at least one node in the tree rooted in {@code node} matches {@code predicate}.
     */
    public static boolean matches(Node node,
                                  Predicate<? super Node> predicate) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(predicate, "predicate");

        final PredicateMatcher visitor = new PredicateMatcher(predicate);
        node.accept(visitor);
        return visitor.found;
    }

    @Override
    public Void visitLeaf(AbstractLeafNode node) {
        if (!found && predicate.test(node)) {
            found = true;
        }
        return null;
    }

    @Override
    public Void visitUnary(AbstractUnaryNode node) {
        if (!found) {
            if (predicate.test(node)) {
                found = true;
            } else {
                node.getAlpha().accept(this);
            }
        }
        return null;
    }

    @Override
    public Void visitBinary(AbstractBinaryNode node) {
        if (!found) {
            if (predicate.test(node)) {
                found = true;
            } else {
                node.getAlpha().accept(this);
                if (!found) {
                    node.getBeta().accept(this);
                }
            }
        }
        return null;
    }

    @Override
    public Void visitNary(AbstractNaryNode node) {
        if (!found) {
            if (predicate.test(node)) {
                found = true;
            } else {
                for (final Node child : node.getChildren()) {
                    child.accept(this);
                    if (found) {
                        return null;
                    }
                }
            }
        }
        return null;
    }
}