package cdc.applic.expressions.ast;

public final class XorNode extends AbstractBinaryNode {
    public static final String KIND = "XOR";

    public XorNode(Node alpha,
                   Node beta) {
        super(alpha,
              beta);
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_XOR;
    }

    @Override
    public XorNode create(Node alpha,
                          Node beta) {
        return new XorNode(alpha, beta);
    }
}