package cdc.applic.expressions.ast;

import cdc.applic.expressions.Formatting;
import cdc.util.lang.Checks;

abstract class AbstractNode implements Node {
    @Override
    public final String toInfix(Formatting formatting) {
        Checks.isNotNull(formatting, "formatting");

        final StringBuilder builder = new StringBuilder();
        buildInfix(formatting, builder);
        return builder.toString();
    }
}