package cdc.applic.expressions.ast;

public final class AndNode extends AbstractBinaryNode {
    public static final String KIND = "AND";

    public AndNode(Node alpha,
                   Node beta) {
        super(alpha,
              beta);
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_AND;
    }

    @Override
    public AndNode create(Node alpha,
                          Node beta) {
        return new AndNode(alpha, beta);
    }
}