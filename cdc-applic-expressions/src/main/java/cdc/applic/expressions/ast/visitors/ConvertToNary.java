package cdc.applic.expressions.ast.visitors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.OrNode;
import cdc.util.lang.Checks;

/**
 * Utility class used to replace binary &and; and &or; nodes with nary nodes, if meaningful.
 * In addition, all nary nodes that can be merged are merged, recursively.
 *
 * @author Damien Carbonne
 */
public final class ConvertToNary extends AbstractConverter {
    private static final ConvertToNary[] CONVERTER = new ConvertToNary[Variant.values().length];
    private final Variant variant;
    static {
        for (final Variant value : Variant.values()) {
            CONVERTER[value.ordinal()] = new ConvertToNary(value);
        }
    }

    private ConvertToNary(Variant variant) {
        this.variant = variant;
    }

    public enum Variant {
        ALWAYS,
        WHEN_NECESSARY
    }

    public static Node execute(Node node,
                               Variant variant) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(variant, "variant");

        return node.accept(CONVERTER[variant.ordinal()]);
    }

    private Node newAnd(List<Node> children) {
        if (variant == Variant.ALWAYS) {
            return new NaryAndNode(children);
        } else {
            return NaryAndNode.createSimplestAnd(children);
        }
    }

    private Node newOr(List<Node> children) {
        if (variant == Variant.ALWAYS) {
            return new NaryOrNode(children);
        } else {
            return NaryOrNode.createSimplestOr(children);
        }
    }

    private static void addAndChildren(Node node,
                                       List<Node> children) {
        if (node instanceof AndNode) {
            children.add(((AndNode) node).getAlpha());
            children.add(((AndNode) node).getBeta());
        } else if (node instanceof NaryAndNode) {
            Collections.addAll(children, ((NaryAndNode) node).getChildren());
        } else {
            children.add(node);
        }
    }

    private static void addOrChildren(Node node,
                                      List<Node> children) {
        if (node instanceof OrNode) {
            children.add(((OrNode) node).getAlpha());
            children.add(((OrNode) node).getBeta());
        } else if (node instanceof NaryOrNode) {
            Collections.addAll(children, ((NaryOrNode) node).getChildren());
        } else {
            children.add(node);
        }
    }

    @Override
    public Node visitBinary(AbstractBinaryNode node) {
        if (node instanceof AndNode) {
            final Node s1 = node.getAlpha().accept(this);
            final Node s2 = node.getBeta().accept(this);
            final List<Node> children = new ArrayList<>();
            addAndChildren(s1, children);
            addAndChildren(s2, children);
            return newAnd(children);
        } else if (node instanceof OrNode) {
            final Node s1 = node.getAlpha().accept(this);
            final Node s2 = node.getBeta().accept(this);
            final List<Node> children = new ArrayList<>();
            addOrChildren(s1, children);
            addOrChildren(s2, children);
            return newOr(children);
        } else {
            return super.visitBinary(node);
        }
    }

    @Override
    public Node visitNary(AbstractNaryNode node) {
        if (node instanceof NaryAndNode) {
            final List<Node> children = new ArrayList<>();
            for (final Node child : node.getChildren()) {
                final Node s = child.accept(this);
                if (s instanceof AndNode || s instanceof NaryAndNode) {
                    addAndChildren(s, children);
                } else {
                    children.add(s);
                }
            }
            return newAnd(children);
        } else {
            final List<Node> children = new ArrayList<>();
            for (final Node child : node.getChildren()) {
                final Node s = child.accept(this);
                if (s instanceof OrNode || s instanceof NaryOrNode) {
                    addOrChildren(s, children);
                } else {
                    children.add(s);
                }
            }
            return newOr(children);
        }
    }
}