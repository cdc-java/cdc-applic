package cdc.applic.expressions.ast;

import java.util.Optional;

import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.applic.expressions.parsing.ComparisonOperator;

public final class NeitherLessNorEqualNode extends AbstractValueNode implements InequalityNode, NegativeNode {
    public static final String KIND = "NEITHER_LESS_NOR_EQUAL";

    public NeitherLessNorEqualNode(Name propertyName,
                                   Value value) {
        super(propertyName,
              value);
    }

    @Override
    public ComparisonOperator getComparisonOperator() {
        return ComparisonOperator.NEITHER_LESS_NOR_EQUAL;
    }

    @Override
    public boolean isNegative() {
        return true;
    }

    @Override
    public LessOrEqualNode negate() {
        return new LessOrEqualNode(getName(), getValue());
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_NEITHER_LESS_NOR_EQUAL;
    }

    @Override
    public NeitherLessNorEqualNode create(Name propertyName,
                                          Value value) {
        return new NeitherLessNorEqualNode(propertyName, value);
    }

    @Override
    public NeitherLessNorEqualNode setName(Name name) {
        return new NeitherLessNorEqualNode(name, getValue());
    }

    @Override
    public NeitherLessNorEqualNode setPrefixIfMissing(Optional<SName> prefix) {
        return getName().hasPrefix() || prefix.isEmpty()
                ? this
                : new NeitherLessNorEqualNode(getName().setPrefix(prefix), getValue());
    }

    @Override
    public NeitherLessNorEqualNode removePrefix() {
        return getName().hasPrefix()
                ? new NeitherLessNorEqualNode(getName().removePrefix(), getValue())
                : this;
    }
}