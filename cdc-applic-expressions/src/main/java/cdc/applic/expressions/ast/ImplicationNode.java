package cdc.applic.expressions.ast;

public final class ImplicationNode extends AbstractBinaryNode {
    public static final String KIND = "IMPLICATION";

    public ImplicationNode(Node alpha,
                           Node beta) {
        super(alpha,
              beta);
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_IMPL;
    }

    @Override
    public ImplicationNode create(Node alpha,
                                  Node beta) {
        return new ImplicationNode(alpha, beta);
    }
}