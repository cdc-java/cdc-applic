package cdc.applic.expressions.ast;

import java.util.Optional;

import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.applic.expressions.parsing.ComparisonOperator;

public final class NotGreaterNode extends AbstractValueNode implements InequalityNode, NegativeNode {
    public static final String KIND = "NOT_GREATER";

    public NotGreaterNode(Name propertyName,
                          Value value) {
        super(propertyName,
              value);
    }

    @Override
    public ComparisonOperator getComparisonOperator() {
        return ComparisonOperator.NOT_GREATER;
    }

    @Override
    public boolean isNegative() {
        return true;
    }

    @Override
    public GreaterNode negate() {
        return new GreaterNode(getName(), getValue());
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_NOT_GREATER;
    }

    @Override
    public NotGreaterNode create(Name propertyName,
                                 Value value) {
        return new NotGreaterNode(propertyName, value);
    }

    @Override
    public NotGreaterNode setName(Name name) {
        return new NotGreaterNode(name, getValue());
    }

    @Override
    public NotGreaterNode setPrefixIfMissing(Optional<SName> prefix) {
        return getName().hasPrefix() || prefix.isEmpty()
                ? this
                : new NotGreaterNode(getName().setPrefix(prefix), getValue());
    }

    @Override
    public NotGreaterNode removePrefix() {
        return getName().hasPrefix()
                ? new NotGreaterNode(getName().removePrefix(), getValue())
                : this;
    }
}