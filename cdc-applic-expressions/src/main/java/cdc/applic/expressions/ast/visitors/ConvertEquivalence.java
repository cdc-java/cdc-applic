package cdc.applic.expressions.ast.visitors;

import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.EquivalenceNode;
import cdc.applic.expressions.ast.ImplicationNode;
import cdc.applic.expressions.ast.Node;

/**
 * Utility class used to convert <em>equivalence</em> nodes (&harr;) by recursively applying this
 * rewriting rule:
 * <ul>
 * <li>&alpha; &harr; &beta; &equiv; (&alpha; &rarr; &beta;) &and; (&beta; &rarr; &alpha;)
 * </ul>
 *
 * <b>Note</b>: This is an adaptation of AIMA Java implementation.
 *
 * @author Damien Carbonne
 */
public final class ConvertEquivalence extends AbstractConverter {
    private static final ConvertEquivalence CONVERTER = new ConvertEquivalence();

    private ConvertEquivalence() {
    }

    public static Node execute(Node node) {
        return node.accept(CONVERTER);
    }

    @Override
    public Node visitBinary(AbstractBinaryNode node) {
        final Node result;
        if (node instanceof EquivalenceNode) {
            // We could use:
            // (alpha & beta) | (!alpha & !beta)
            // (alpha & beta) | !(alpha | beta)
            // (!alpha | beta) & (alpha | !beta)
            final Node alpha = node.getAlpha().accept(this);
            final Node beta = node.getBeta().accept(this);
            final Node alphaImpliesBeta = new ImplicationNode(alpha, beta);
            final Node betaImpliesAlpha = new ImplicationNode(beta, alpha);

            result = new AndNode(alphaImpliesBeta, betaImpliesAlpha);
        } else {
            result = super.visitBinary(node);
        }
        return result;
    }
}