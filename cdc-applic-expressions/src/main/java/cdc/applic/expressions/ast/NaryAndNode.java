package cdc.applic.expressions.ast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cdc.util.lang.Checks;

public final class NaryAndNode extends AbstractNaryNode {
    public static final String KIND = "NARY_AND";

    public NaryAndNode(Node... children) {
        super(children);
    }

    public NaryAndNode(List<? extends Node> children) {
        super(children);
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_AND;
    }

    @Override
    public final NaryAndNode create(Node... children) {
        return new NaryAndNode(children);
    }

    @Override
    public NaryAndNode create(List<Node> children) {
        return new NaryAndNode(children);
    }

    /**
     * Creates the simplest <em>and</em> node combining a list of nodes.
     * <p>
     * If {@code nodes} contains 1 node, returns this node.<br>
     * If {@code nodes} contains 2 nodes, returns an OrNode.<br>
     * If {@code nodes} contains more than 2 nodes, returns an NaryOrNode.
     *
     * @param nodes The nodes to combine as an Or node.
     * @return The simplest and node combining {@code nodes}.
     * @throws IllegalArgumentException When {@code nodes} is {@code null} or empty.
     */
    public static Node createSimplestAnd(List<? extends Node> nodes) {
        Checks.isNotNullOrEmpty(nodes, NODES);

        if (nodes.size() == 1) {
            return nodes.get(0);
        } else if (nodes.size() == 2) {
            return new AndNode(nodes.get(0), nodes.get(1));
        } else {
            return new NaryAndNode(nodes);
        }
    }

    /**
     * Creates the simplest <em>and</em> node combining an array of nodes.
     * <p>
     * If {@code nodes} contains 1 node, returns this node.<br>
     * If {@code nodes} contains 2 nodes, returns an AndNode.<br>
     * If {@code nodes} contains more than 2 nodes, returns an NaryAndNode.
     *
     * @param nodes The nodes to combine as an Or node.
     * @return The simplest and node combining {@code nodes}.
     * @throws IllegalArgumentException When {@code nodes} is {@code null} or empty.
     */
    public static Node createSimplestAnd(Node... nodes) {
        Checks.isNotNullOrEmpty(nodes, NODES);

        if (nodes.length == 1) {
            return nodes[0];
        } else if (nodes.length == 2) {
            return new AndNode(nodes[0], nodes[1]);
        } else {
            return new NaryAndNode(nodes);
        }
    }

    public static Node mergeSimplestAnd(Node... nodes) {
        Checks.isNotNullOrEmpty(nodes, NODES);

        final List<Node> children = new ArrayList<>();
        for (final Node node : nodes) {
            if (node instanceof final NaryAndNode n) {
                Collections.addAll(children, n.getChildren());
            } else if (node instanceof final AndNode n) {
                children.add(n.getAlpha());
                children.add(n.getBeta());
            } else {
                children.add(node);
            }
        }
        return createSimplestAnd(children);
    }
}