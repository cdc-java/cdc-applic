package cdc.applic.expressions.ast;

import cdc.applic.expressions.literals.Name;

/**
 * Base class of nodes that have a name.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractNamedNode extends AbstractLeafNode implements NamedNode {
    protected final Name name;

    protected AbstractNamedNode(Name name) {
        this.name = name;
    }

    @Override
    public final Name getName() {
        return name;
    }
}