package cdc.applic.expressions.ast;

import java.util.Optional;

import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.applic.expressions.parsing.ComparisonOperator;

public final class LessNode extends AbstractValueNode implements InequalityNode {
    public static final String KIND = "LESS";

    public LessNode(Name propertyName,
                    Value value) {
        super(propertyName,
              value);
    }

    @Override
    public ComparisonOperator getComparisonOperator() {
        return ComparisonOperator.LESS;
    }

    @Override
    public boolean isNegative() {
        return false;
    }

    @Override
    public NotLessNode negate() {
        return new NotLessNode(getName(), getValue());
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_LESS;
    }

    @Override
    public LessNode create(Name propertyName,
                           Value value) {
        return new LessNode(propertyName, value);
    }

    @Override
    public LessNode setName(Name name) {
        return new LessNode(name, getValue());
    }

    @Override
    public LessNode setPrefixIfMissing(Optional<SName> prefix) {
        return getName().hasPrefix() || prefix.isEmpty()
                ? this
                : new LessNode(getName().setPrefix(prefix), getValue());
    }

    @Override
    public LessNode removePrefix() {
        return getName().hasPrefix()
                ? new LessNode(getName().removePrefix(), getValue())
                : this;
    }
}