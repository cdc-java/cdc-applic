package cdc.applic.expressions.ast;

/**
 * Marking interface of nodes that are atoms.
 * <p>
 * Those are the only nodes that can be used to create sentences.<br>
 * Nodes that are not atoms must be transformed into atoms to create sentences.
 */
public interface AtomNode extends LeafNode {
    // Ignore
}