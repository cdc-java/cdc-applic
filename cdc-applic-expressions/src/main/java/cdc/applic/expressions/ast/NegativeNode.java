package cdc.applic.expressions.ast;

/**
 * Marking interface implemented by Nodes that are explicitly negative.
 * <p>
 * <b>Note:</b> ImplicationNode and EquivalenceNode are not marked as
 * negative nodes.
 *
 * @author Damien Carbonne
 */
public interface NegativeNode extends Node {
    // Ignore
}