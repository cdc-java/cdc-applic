package cdc.applic.expressions.ast.visitors;

import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractSetNode;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotEqualNode;
import cdc.applic.expressions.content.Range;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.Value;

/**
 * Converter that applies the following rewriting rules:
 * <ul>
 * <li>&pi;&isin;{&omega;} &equiv; &pi;=&omega;
 * <li>&pi;&notin;{&omega;} &equiv; &pi;&ne;&omega;
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class ConvertSingletonsToEquals extends AbstractConverter {
    private static final ConvertSingletonsToEquals CONVERTER = new ConvertSingletonsToEquals();

    private ConvertSingletonsToEquals() {
    }

    public static Node execute(Node node) {
        return node.accept(CONVERTER);
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (node instanceof AbstractSetNode) {
            final AbstractSetNode n = (AbstractSetNode) node;
            final SItemSet set = n.getSet();
            if (set.isSingleton()) {
                final SItem item = set.getItems().iterator().next();
                final Value value = item instanceof Value
                        ? (Value) item
                        : ((Range) item).getMin();
                return node instanceof InNode
                        ? new EqualNode(n.getName(), value)
                        : new NotEqualNode(n.getName(), value);
            } else {
                return super.visitLeaf(node);
            }
        } else {
            return super.visitLeaf(node);
        }
    }
}