package cdc.applic.expressions.ast;

/**
 * Interface implemented by objects that need to be notified during AST
 * traversal.
 * <p>
 * A visit method is defined for each node arity.<br>
 * This facilitates the writing of many algorithms whose behavior directly depends on this parameter.<br>
 * In addition, each method has a specific name, even if overloading could have been used.
 *
 * @author D. Carbonne
 *
 * @param <R> Class of the objects returned by the visit.
 */
public interface Visitor<R> {
    /**
     * Invoked when a leaf node is visited.
     *
     * @param node The visited node.
     * @return The visit result.
     */
    public R visitLeaf(AbstractLeafNode node);

    /**
     * Invoked when an unary node is visited.
     *
     * @param node The visited node.
     * @return The visit result.
     */
    public R visitUnary(AbstractUnaryNode node);

    /**
     * Invoked when a binary node is visited.
     *
     * @param node The visited node.
     * @return The visit result.
     */
    public R visitBinary(AbstractBinaryNode node);

    /**
     * Invoked when an n-ary node is visited.
     *
     * @param node The visited node.
     * @return The visit result.
     */
    public R visitNary(AbstractNaryNode node);
}