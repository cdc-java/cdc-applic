package cdc.applic.expressions.ast;

import java.util.Optional;

import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.applic.expressions.parsing.ComparisonOperator;

public final class NeitherGreaterNorEqualNode extends AbstractValueNode implements InequalityNode, NegativeNode {
    public static final String KIND = "NEITHER_GREATER_NOR_EQUAL";

    public NeitherGreaterNorEqualNode(Name propertyName,
                                      Value value) {
        super(propertyName,
              value);
    }

    @Override
    public ComparisonOperator getComparisonOperator() {
        return ComparisonOperator.NEITHER_GREATER_NOR_EQUAL;
    }

    @Override
    public boolean isNegative() {
        return true;
    }

    @Override
    public GreaterOrEqualNode negate() {
        return new GreaterOrEqualNode(getName(), getValue());
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_NEITHER_GREATER_NOR_EQUAL;
    }

    @Override
    public NeitherGreaterNorEqualNode create(Name propertyName,
                                             Value value) {
        return new NeitherGreaterNorEqualNode(propertyName, value);
    }

    @Override
    public NeitherGreaterNorEqualNode setName(Name name) {
        return new NeitherGreaterNorEqualNode(name, getValue());
    }

    @Override
    public NeitherGreaterNorEqualNode setPrefixIfMissing(Optional<SName> prefix) {
        return getName().hasPrefix() || prefix.isEmpty()
                ? this
                : new NeitherGreaterNorEqualNode(getName().setPrefix(prefix), getValue());
    }

    @Override
    public NeitherGreaterNorEqualNode removePrefix() {
        return getName().hasPrefix()
                ? new NeitherGreaterNorEqualNode(getName().removePrefix(), getValue())
                : this;
    }
}