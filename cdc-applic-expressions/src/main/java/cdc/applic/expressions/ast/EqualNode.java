package cdc.applic.expressions.ast;

import java.util.Optional;

import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.applic.expressions.parsing.ComparisonOperator;

public final class EqualNode extends AbstractValueNode implements EqualityNode {
    public static final String KIND = "EQUAL";

    public EqualNode(Name propertyName,
                     Value value) {
        super(propertyName,
              value);
    }

    @Override
    public ComparisonOperator getComparisonOperator() {
        return ComparisonOperator.EQUAL;
    }

    @Override
    public boolean isNegative() {
        return false;
    }

    @Override
    public NotEqualNode negate() {
        return new NotEqualNode(getName(), getValue());
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_EQUAL;
    }

    @Override
    public EqualNode create(Name propertyName,
                            Value value) {
        return new EqualNode(propertyName, value);
    }

    @Override
    public EqualNode setName(Name name) {
        return new EqualNode(name, getValue());
    }

    @Override
    public EqualNode setPrefixIfMissing(Optional<SName> prefix) {
        return getName().hasPrefix() || prefix.isEmpty()
                ? this
                : new EqualNode(getName().setPrefix(prefix), getValue());
    }

    @Override
    public EqualNode removePrefix() {
        return getName().hasPrefix()
                ? new EqualNode(getName().removePrefix(), getValue())
                : this;
    }
}