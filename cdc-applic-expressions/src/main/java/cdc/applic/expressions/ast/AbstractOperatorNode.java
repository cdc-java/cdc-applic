package cdc.applic.expressions.ast;

/**
 * Base class of operator nodes.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractOperatorNode extends AbstractNode implements ParsingNode {
    /**
     * Returns {@code true} when parentheses should be used around a child node of the node.
     *
     * @param child The child node.
     * @return {@code true} if parentheses should be used around {@code child}.
     */
    public final boolean needParentheses(Node child) {
        if (child.getKerning().getPrecedence() == getKerning().getPrecedence()) {
            return child.getKerning().isAmbiguous();
        } else {
            return child.getKerning().getPrecedence() < getKerning().getPrecedence();
        }
    }
}