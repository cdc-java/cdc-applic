package cdc.applic.expressions.ast;

import cdc.applic.expressions.literals.Named;

public interface NamedNode extends LeafNode, Named {
    // Ignore
}