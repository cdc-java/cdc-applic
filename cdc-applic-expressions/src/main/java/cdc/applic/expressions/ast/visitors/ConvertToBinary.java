package cdc.applic.expressions.ast.visitors;

import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.OrNode;

/**
 * Utility class used to recursively replace nary nodes by appropriate binary nodes.
 * <ul>
 * <li>&alpha;<sub>1</sub> &and; &alpha;<sub>2</sub> &and; &alpha;<sub>3</sub>
 * &and; ... &equiv; (...((&alpha;<sub>1</sub> &and; &alpha;<sub>2</sub>) &and;
 * &alpha;<sub>3</sub>) &and; ...)</li>
 * <li>&alpha;<sub>1</sub> &or; &alpha;<sub>2</sub> &or; &alpha;<sub>3</sub>
 * &or; ... &equiv; (...((&alpha;<sub>1</sub> &or; &alpha;<sub>2</sub>) &or;
 * &alpha;<sub>3</sub>) &or; ...)</li>
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class ConvertToBinary extends AbstractConverter {
    private static final ConvertToBinary CONVERTER = new ConvertToBinary();

    private ConvertToBinary() {
    }

    public static Node execute(Node node) {
        return node.accept(CONVERTER);
    }

    @Override
    public Node visitNary(AbstractNaryNode node) {
        final Node[] children = node.getChildren().clone();
        for (int index = 0; index < children.length; index++) {
            children[index] = children[index].accept(this);
        }
        Node result = children[0];
        for (int index = 1; index < children.length; index++) {
            if (node instanceof NaryAndNode) {
                result = new AndNode(result, children[index]);
            } else {
                // It must be an nary or node
                result = new OrNode(result, children[index]);
            }
        }
        return result;
    }
}