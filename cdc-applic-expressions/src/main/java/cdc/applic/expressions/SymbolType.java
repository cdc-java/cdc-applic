package cdc.applic.expressions;

/**
 * Enumeration of standard symbol types.
 *
 * @author Damien Carbonne
 */
public enum SymbolType {
    /**
     * Short ASCII text symbol.
     */
    SHORT,

    /**
     * Long ASCII text symbol.
     */
    LONG,

    /**
     * Mathematical (non ASCII) symbol.
     * <p>
     * <b>Note:</b> several symbols are possible. One has been chosen.
     * Other can be used as output by using publication module.
     */
    MATH
}