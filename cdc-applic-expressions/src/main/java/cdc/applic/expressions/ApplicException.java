package cdc.applic.expressions;

/**
 * Base class of am exception raised in this library.
 *
 * @author Damien Carbonne
 */
public class ApplicException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ApplicException(String message,
                           Throwable cause) {
        super(message, cause);
    }
}