package cdc.applic.expressions;

import java.util.Objects;

public class Formatting {
    public static final Formatting SHORT_NARROW = new Formatting(SymbolType.SHORT, Spacing.NARROW);
    public static final Formatting LONG_NARROW = new Formatting(SymbolType.LONG, Spacing.NARROW);
    public static final Formatting MATH_NARROW = new Formatting(SymbolType.MATH, Spacing.NARROW);
    public static final Formatting SHORT_WIDE = new Formatting(SymbolType.SHORT, Spacing.WIDE);
    public static final Formatting LONG_WIDE = new Formatting(SymbolType.LONG, Spacing.WIDE);
    public static final Formatting MATH_WIDE = new Formatting(SymbolType.MATH, Spacing.WIDE);

    private final SymbolType symbolType;
    private final Spacing spacing;

    protected Formatting(SymbolType symbolType,
                         Spacing spacing) {
        this.symbolType = symbolType;
        this.spacing = spacing;
    }

    public SymbolType getSymbolType() {
        return symbolType;
    }

    public Spacing getSpacing() {
        return spacing;
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbolType,
                            spacing);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Formatting)) {
            return false;
        }
        final Formatting other = (Formatting) object;
        return symbolType == other.symbolType
                && spacing == other.spacing;
    }

    @Override
    public String toString() {
        return "(" + getSymbolType() + ", " + getSpacing() + ")";
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private SymbolType symbolType = SymbolType.SHORT;
        private Spacing spacing = Spacing.NARROW;

        protected Builder() {
            super();
        }

        public Builder symbolType(SymbolType symbolType) {
            this.symbolType = symbolType;
            return this;
        }

        public Builder spacing(Spacing spacing) {
            this.spacing = spacing;
            return this;
        }

        public Formatting build() {
            return new Formatting(symbolType, spacing);
        }
    }
}