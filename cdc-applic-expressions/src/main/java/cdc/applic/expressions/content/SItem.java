package cdc.applic.expressions.content;

/**
 * Base interface of things that can be used to define a set: {@link Value} and {@link Range}.
 *
 * @author Damien Carbonne
 */
public interface SItem {
    /**
     * Returns the class of the associated {@link CheckedSet}:
     * <ul>
     * <li>{@link BooleanSet} for {@link BooleanValue}
     * <li>{@link IntegerSet} for {@link IntegerValue} and {@link IntegerRange}
     * <li>{@link RealSet} for {@link RealValue} and {@link RealRange}
     * <li>{@link StringSet} for {@link StringValue}
     * </ul>
     *
     * @return The class of the associated {@link CheckedSet}.
     */
    public Class<? extends CheckedSet<?, ?>> getCheckedSetClass();

    /**
     * Returns {@code true} if this item is compliant with a set class:
     * <ul>
     * <li>{@code true} for {@link BooleanValue} and {@link BooleanSet}.class or {@link UncheckedSet}.class
     * <li>{@code true} for {@link IntegerValue} or {@link IntegerRange} and {@link IntegerSet}.class or {@link UncheckedSet}.class
     * <li>{@code true} for {@link RealValue} or {@link RealRange} and {@link RealSet}.class or {@link UncheckedSet}.class
     * <li>{@code true} for {@link StringValue} and {@link StringSet}.class or {@link UncheckedSet}.class
     * </ul>
     *
     * @param cls The set class.
     * @return {@code true} if this item is compliant with {@code cls}.
     */
    public default boolean isCompliantWith(Class<? extends SItemSet> cls) {
        return cls.equals(UncheckedSet.class) || cls.equals(getCheckedSetClass());
    }

    public String getNonEscapedLiteral();

    public String getProtectedLiteral();

    public static IllegalArgumentException newIllegalConversion(SItem item,
                                                                Class<? extends SItemSet> kind) {
        return new IllegalArgumentException("Can not convert " + item + " to: " + (kind == null ? "null" : kind.getSimpleName()));
    }
}