package cdc.applic.expressions.content;

/**
 * Base interface of real items: {@link RealValue} and {@link RealRange}.
 *
 * @author Damien Carbonne
 */
public interface RealSItem extends SItem {
    @Override
    default Class<? extends CheckedSet<?, ?>> getCheckedSetClass() {
        return RealSet.class;
    }
}