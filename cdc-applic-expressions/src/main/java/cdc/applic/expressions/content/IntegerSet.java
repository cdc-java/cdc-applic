package cdc.applic.expressions.content;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import cdc.applic.expressions.SyntacticException;
import cdc.applic.expressions.parsing.SItemsParsing;
import cdc.graphs.PartialOrderPosition;
import cdc.util.lang.Checks;

/**
 * Implementation of {@link SItemSet} containing {@link IntegerSItem}s.
 * <p>
 * Order of definition of SItems is lost.<br>
 * Duplicates are removed.
 *
 * @author Damien Carbonne
 */
public final class IntegerSet extends AbstractRangeSet<IntegerValue, IntegerRange, IntegerSet>
        implements CountableSet {

    public static final Comparator<IntegerSet> LEXICOGRAPHIC_COMPARATOR = lexicographicComparator();

    /** The empty {@link IntegerSet}. */
    public static final IntegerSet EMPTY = of();

    private IntegerSet(List<IntegerRange> ranges) {
        super(IntegerDomain.INSTANCE,
              ranges);
    }

    private IntegerSet() {
        super(IntegerDomain.INSTANCE);
    }

    private IntegerSet(Collection<? extends IntegerSItem> items) {
        super(IntegerDomain.INSTANCE,
              toRanges(items));
    }

    private IntegerSet(String content) {
        this(SItemsParsing.toIntegerRanges(content));
    }

    @SafeVarargs
    private IntegerSet(IntegerSItem... items) {
        super(IntegerDomain.INSTANCE,
              toRanges(items));
    }

    private static List<IntegerRange> toRanges(Collection<? extends IntegerSItem> items) {
        final List<IntegerRange> result = new ArrayList<>();
        for (final IntegerSItem item : items) {
            if (item instanceof IntegerValue) {
                result.add(IntegerRange.of((IntegerValue) item));
            } else {
                result.add((IntegerRange) item);
            }
        }
        return result;
    }

    private static List<IntegerRange> toRanges(IntegerSItem... items) {
        final List<IntegerRange> result = new ArrayList<>();
        for (final IntegerSItem item : items) {
            if (item instanceof IntegerValue) {
                result.add(IntegerRange.of((IntegerValue) item));
            } else {
                result.add((IntegerRange) item);
            }
        }
        return result;
    }

    @Override
    protected IntegerSet create(List<IntegerRange> ranges) {
        return new IntegerSet(ranges);
    }

    @Override
    protected IntegerSet adapt(SItemSet set) {
        return convert(set);
    }

    @Override
    public IntegerSet empty() {
        return EMPTY;
    }

    @Override
    protected IntegerSet self() {
        return this;
    }

    /**
     * Converts an {@link SItemSet} set to an {@link IntegerSet}.
     *
     * @param set The set.
     * @return The conversion of {@code set} to an {@link IntegerSet}.
     * @throws IllegalArgumentException When {@code set} can not be converted to an {@link IntegerSet}.
     */
    public static IntegerSet convert(SItemSet set) {
        Checks.isNotNull(set, "set");
        if (set instanceof IntegerSet) {
            return (IntegerSet) set;
        } else if (set.isEmpty()) {
            return EMPTY;
        } else if (set.isCompliantWith(IntegerSet.class)) {
            // Should not throw ClassCastException
            return new IntegerSet(convert(set.getItems(), IntegerSItem.class));
        } else {
            throw new IllegalArgumentException("Can not convert this set to " + IntegerSet.class.getSimpleName());
        }
    }

    /**
     * Creates an {@link IntegerSet} from the String representation of its content.
     *
     * @param content The content.
     * @return A new instance of {@link IntegerSet}.
     * @throws SyntacticException When {@code content} can not be parsed as an {@link IntegerSet}.
     */
    public static IntegerSet of(String content) {
        return new IntegerSet(content);
    }

    /**
     * Creates an {@link IntegerSet} from an array of {@link IntegerSItem IntegerSItems}.
     *
     * @param items The items.
     * @return A new instance of {@link IntegerSet}.
     * @throws IllegalArgumentException When an item is {@code null}.
     */
    public static IntegerSet of(IntegerSItem... items) {
        return new IntegerSet(items);
    }

    /**
     * Creates an {@link IntegerSet} from a collection of {@link IntegerSItem IntegerSItems}.
     *
     * @param items The items.
     * @return A new instance of {@link IntegerSet}.
     * @throws IllegalArgumentException When an item is {@code null}.
     */
    public static IntegerSet of(Collection<? extends IntegerSItem> items) {
        return new IntegerSet(items);
    }

    /**
     * Creates an {@link IntegerSet} from one {@link IntegerSItem}.
     *
     * @param item The item.
     * @return A new instance of {@link IntegerSet}.
     * @throws IllegalArgumentException When {@code value} is {@code null}.
     */
    public static IntegerSet of(IntegerSItem item) {
        return new IntegerSet(item);
    }

    @Override
    public long getExtent() {
        long count = 0;
        for (final IntegerRange range : getRanges()) {
            count += range.getExtent();
        }
        return count;
    }

    public boolean contains(int value) {
        return contains(IntegerValue.of(value));
    }

    public IntegerSet union(int value) {
        return union(IntegerValue.of(value));
    }

    public IntegerSet intersection(int value) {
        return intersection(IntegerValue.of(value));
    }

    public IntegerSet remove(int value) {
        return remove(IntegerValue.of(value));
    }

    public List<IntegerValue> getValues() {
        final List<IntegerValue> values = new ArrayList<>();
        for (final IntegerRange range : getRanges()) {
            if (!range.isEmpty()) {
                final int min = range.getMin().getNumber();
                final int max = range.getMax().getNumber();
                for (int value = min; value < max; value++) {
                    values.add(IntegerValue.of(value));
                }
                values.add(IntegerValue.of(max));
            }
        }
        return values;
    }

    public static IntegerSet of(IntegerValue value,
                                PartialOrderPosition position) {
        return AbstractRangeSet.toSet(IntegerDomain.INSTANCE,
                                      EMPTY,
                                      IntegerSet::of,
                                      value,
                                      position);
    }
}