package cdc.applic.expressions.content;

public interface CountableSet extends SItemSet {
    /**
     * The value used to indicate that the number of values in the set is undefined.
     */
    public static final long UNDEFINED_NUMBER_OF_VALUES = -1L;

    /**
     * @return The number of values contained in this set, or {@link CountableSet#UNDEFINED_NUMBER_OF_VALUES}.
     */
    public long getExtent();
}