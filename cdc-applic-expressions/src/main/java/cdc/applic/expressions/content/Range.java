package cdc.applic.expressions.content;

import cdc.applic.expressions.IllegalOperationException;

/**
 * Base interface of items that are ranges.
 *
 * @author Damien Carbonne
 */
public interface Range extends SItem {
    public static final String BOUNDS_SEPARATOR = "~";

    /**
     * @return The minimum value.
     */
    public Value getMin();

    /**
     * @return The maximum value.
     */
    public Value getMax();

    /**
     * @return {@code true} if this range is empty.
     */
    public boolean isEmpty();

    /**
     * @return {@code true} if this range is a singleton.
     */
    public boolean isSingleton();

    /**
     * @return The singleton value if this range is a singleton.
     * @throws IllegalOperationException When this range is not a singleton.
     */
    public Value getSingletonValue();

    /**
     * Returns {@code true} if this range contains a value.
     *
     * @param value The value.
     * @return {@code true} if this range contains {@code value}.
     * @throws IllegalArgumentException When {@code value} is not compliant with this Range.
     */
    public boolean contains(Value value);

    /**
     * Returns {@code true} if this range contains another range.
     *
     * @param range The range.
     * @return {@code true} if this range contains {@code range}.
     * @throws IllegalArgumentException When {@code range} is not compliant with this Range.
     */
    public boolean contains(Range range);

    /**
     * Returns {@code true} if this range intersects another range.
     *
     * @param range The range.
     * @return {@code true} if this range intersects {@code range}.
     * @throws IllegalArgumentException When {@code range} is not compliant with this Range.
     */
    public boolean intersects(Range range);

    /**
     * Returns the intersection of this range with another range.
     *
     * @param range The range.
     * @return The intersection of this range with {@code range}.
     * @throws IllegalArgumentException When {@code range} is not compliant with this Range.
     */
    public Range intersectionWith(Range range);
}