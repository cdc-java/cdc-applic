package cdc.applic.expressions.content;

/**
 * Relative position of 2 items.
 * <p>
 * Used for standard values and ranges.
 *
 * @author Damien Carbonne
 */
public enum Position {
    /**
     * Item 1 is before (less than) and disjoint from Item 2.
     * <pre>
     * ===I1===
     *              ===I2===
     * </pre>
     */
    DISJOINT_PRED,

    /**
     * Item 1 is before (less than) and adjoint to Item 2.
     * <pre>
     * ===I1===
     *         ===I2===
     * </pre>
     */
    ADJOINT_PRED,

    /**
     * Item 1 and Item 2 are overlapping (ranges) or equal (simple values).
     * <pre>
     * ===I1===
     *       ===I2===
     * ========I1========
     *       ===I2===
     *             ===I1===
     *       ===I2===
     * </pre>
     */
    OVERLAPPING,

    /**
     * Item 1 is after (greater than) and adjoint to Item 2.
     * <pre>
     *         ===I1===
     * ===I2===
     * </pre>
     */
    ADJOINT_SUCC,

    /**
     * Item 1 is after (greater than) and disjoint from Item 2.
     * <pre>
     *              ===I1===
     * ===I2===
     * </pre>
     */
    DISJOINT_SUCC;
    
    public boolean isAdjoint() {
        return this == ADJOINT_PRED || this == ADJOINT_SUCC;
    }
    
    public boolean isDisjoint() {
        return this == DISJOINT_PRED || this == DISJOINT_SUCC;
    }
}