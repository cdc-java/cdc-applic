package cdc.applic.expressions.content;

import java.util.Comparator;

import cdc.util.lang.Checks;

/**
 * Domain implementation dedicated to IntegerValue and IntegerRange.
 *
 * @author Damien Carbonne
 */
public final class IntegerDomain extends AbstractDomain<IntegerValue, IntegerRange> {
    public static final IntegerDomain INSTANCE = new IntegerDomain();

    private IntegerDomain() {
    }

    @Override
    public Class<IntegerValue> getValueClass() {
        return IntegerValue.class;
    }

    @Override
    public Class<IntegerRange> getRangeClass() {
        return IntegerRange.class;
    }

    @Override
    public IntegerValue min() {
        return IntegerValue.MIN_VALUE;
    }

    @Override
    public IntegerValue max() {
        return IntegerValue.MAX_VALUE;
    }

    @Override
    public IntegerValue pred(IntegerValue value) {
        Checks.isTrue(!value.equals(min()), "{} has no predecessor", value);
        return IntegerValue.of(value.getNumber() - 1);
    }

    @Override
    public IntegerValue succ(IntegerValue value) {
        Checks.isTrue(!value.equals(max()), "{} has no successor", value);
        return IntegerValue.of(value.getNumber() + 1);
    }

    @Override
    public IntegerRange create(IntegerValue value) {
        return IntegerRange.of(value);
    }

    @Override
    public IntegerRange create(IntegerValue min,
                               IntegerValue max) {
        return IntegerRange.of(min, max);
    }

    @Override
    public Comparator<IntegerRange> minComparator() {
        return IntegerRange.MIN_COMPARATOR;
    }

    @Override
    public Comparator<IntegerRange> maxComparator() {
        return IntegerRange.MAX_COMPARATOR;
    }

    public IntegerValue pred(int value) {
        return pred(IntegerValue.of(value));
    }

    public IntegerValue succ(int value) {
        return succ(IntegerValue.of(value));
    }

    public boolean adjoint(int x,
                           int y) {
        final Position pos = position(IntegerValue.of(x), IntegerValue.of(y));
        return pos == Position.ADJOINT_PRED || pos == Position.ADJOINT_SUCC;
    }

    public Position position(int x,
                             int y) {
        return position(IntegerValue.of(x), IntegerValue.of(y));
    }
}