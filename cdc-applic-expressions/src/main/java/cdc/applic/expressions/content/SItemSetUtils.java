package cdc.applic.expressions.content;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import cdc.applic.expressions.parsing.SItemsParsing;
import cdc.util.lang.Checks;

/**
 * Utilities dedicated to {@link SItemSet}.
 *
 * @author Damien Carbonne
 */
public final class SItemSetUtils {
    private SItemSetUtils() {
    }

    /**
     * Returns {@code true} if 2 sets intersect.
     *
     * @param s1 The first set.
     * @param s2 The second set.
     * @return {@code true} if {@code s1} and {@code s2} intersect.
     */
    public static boolean areIntersected(SItemSet s1,
                                         SItemSet s2) {
        return !s1.intersection(s2).isEmpty();
    }

    /**
     * Computes a list of minimal disjoint sets that can be combined to obtain a list of input sets.
     * <ul>
     * <li>The obtained sets are all disjoint and non-empty.
     * <li>each non-empty input set can be obtained by the union of one or more obtained sets.
     * </ul>
     *
     * For example:
     * <ul>
     * <li>[] -> []
     * <li>[{}] -> []
     * <li>[{1,2}, {1,3}] -> [{1}, {2}, {3}]
     * <li>[{1,2,3}, {1,2}] -> [{1,2}, {3}]
     * </ul>
     *
     * @param <S> The set type.
     * @param sets The list of input sets.
     * @return The list of disjoint sets.
     */
    public static <S extends CheckedSet<S, ?>>
            List<S> minimalDisjoints(List<S> sets) {
        // The result, which is built incrementally
        final List<S> list = new ArrayList<>();
        // Iterate on input sets, and update result at each step
        for (final S set : sets) {
            minimalDisjointsStep(list, set);
        }
        return list;
    }

    /**
     * Step of computation for {@link #minimalDisjoints(List)}.
     *
     * @param <S> The set type.
     * @param disjoints The list of disjoint sets, that will be updated.
     * @param set The set to take into account.
     */
    private static <S extends CheckedSet<S, ?>>
            void minimalDisjointsStep(List<S> disjoints,
                                      S set) {
        if (set.isEmpty()) {
            // Ignore
        } else if (disjoints.isEmpty()) {
            // We know set is not empty
            disjoints.add(set);
        } else {
            // The new list of disjoint sets
            final List<S> nextDisjoints = new ArrayList<>();

            // Iterate on each existing disjoint
            // It can either be unchanged or split into 2 sets
            for (final S disjoint : disjoints) {
                if (areIntersected(disjoint, set)) {
                    // disjoint less set
                    final S disjoint1 = disjoint.remove(set);
                    // intersection of disjoint and set
                    final S disjoint2 = disjoint.remove(disjoint1);
                    set = set.remove(disjoint2);
                    if (disjoint1.isEmpty()) {
                        // The existing disjoint is totally included in set
                        // It is therefore unchanged
                        nextDisjoints.add(disjoint);
                    } else {
                        // existing disjoint has a partial intersection with set
                        // It is split into and replaced by 2 new disjoint sets
                        nextDisjoints.add(disjoint1);
                        nextDisjoints.add(disjoint2);
                    }
                } else {
                    // disjoint and set don't intersect
                    // The existing disjoint is unchanged
                    nextDisjoints.add(disjoint);
                }
            }
            // Update disjoint sets
            disjoints.clear();
            disjoints.addAll(nextDisjoints);
            if (!set.isEmpty()) {
                disjoints.add(set);
            }
        }
    }

    /**
     * @param cls The set class.
     * @return The empty set corresponding to {@code cls}.
     */
    public static SItemSet getEmptySet(Class<? extends SItemSet> cls) {
        Checks.isNotNull(cls, "cls");
        if (UncheckedSet.class.equals(cls)) {
            return UncheckedSet.EMPTY;
        } else if (BooleanSet.class.equals(cls)) {
            return BooleanSet.EMPTY;
        } else if (IntegerSet.class.equals(cls)) {
            return IntegerSet.EMPTY;
        } else if (RealSet.class.equals(cls)) {
            return RealSet.EMPTY;
        } else {
            return StringSet.EMPTY;
        }
    }

    /**
     * Creates the best set corresponding to a content.
     * <p>
     * If {@code content} defines an empty set, or contains non homogeneous items,
     * the returned set with be unchecked.
     *
     * @param content The content.
     * @return The best set corresponding to {@code content}.
     */
    public static SItemSet createBest(String content) {
        return createBest(SItemsParsing.toSItems(content));
    }

    /**
     * Creates the best set corresponding to a collection of items.
     * <p>
     * If {@code items} is empty, or contains non homogeneous items,
     * the returned set with be unchecked.
     *
     * @param items The items.
     * @return The best set corresponding to {@code items}.
     */
    public static SItemSet createBest(Collection<? extends SItem> items) {
        final UncheckedSet unchecked = UncheckedSet.of(items);
        final SItemSet checked = unchecked.getChecked();
        return checked == null
                ? unchecked
                : checked;
    }

    /**
     * Creates the best set corresponding to an array of items.
     * <p>
     * If {@code items} is empty, or contains non homogeneous items,
     * the returned set with be unchecked.
     *
     * @param items The items.
     * @return The best set corresponding to {@code items}.
     */
    public static SItemSet createBest(SItem... items) {
        final UncheckedSet unchecked = UncheckedSet.of(items);
        final SItemSet checked = unchecked.getChecked();
        return checked == null
                ? unchecked
                : checked;
    }

    /**
     * Normalizes a set using alphabetical order.
     * <p>
     * <b>WARNING:</b> This normalization does dot take into account types hints.
     * Use {@code NormalizeSets} for that.<br>
     * This function should only be used for tests.
     *
     * @param set The set.
     * @return The normalized conversion of {@code set}.
     */
    public static SItemSet normalizeSet(SItemSet set) {
        if (set.isEmpty() || set.isSingleton()) {
            return set;
        } else {
            final SItemSet checked = set.getChecked();
            if (checked == null) {
                throw new IllegalArgumentException();
            } else if (checked instanceof StringSet) {
                return normalizeStringSet((StringSet) set);
            } else {
                return checked;
            }
        }
    }

    private static StringSet normalizeStringSet(StringSet set) {
        final List<StringValue> values = new ArrayList<>(set.getItems());
        Collections.sort(values);
        return StringSet.of(values);
    }

    /**
     * Compares 2 SItemSets.
     * <p>
     * If {@code s1} and {@code s2} have the same type, the corresponding comparator is used.
     * Otherwise, their string representations are compared.
     *
     * @param s1 The 1st set.
     * @param s2 The 2nd set.
     * @return The comparison of {@code s1} and {@code s2}.
     */
    public static int compare(SItemSet s1,
                              SItemSet s2) {

        if (s1 == s2) {
            return 0;
        }
        if (s1 == null || s2 == null) {
            return s1 == null ? -1 : 1;
        }

        if (s1.getClass().equals(s2.getClass())) {
            if (s1 instanceof final BooleanSet s) {
                return s.compareTo((BooleanSet) s2);
            } else if (s1 instanceof final StringSet s) {
                return s.compareTo((StringSet) s2);
            } else if (s1 instanceof final IntegerSet s) {
                return s.compareTo((IntegerSet) s2);
            } else {
                return ((RealSet) s1).compareTo((RealSet) s2);
            }
        } else {
            return s1.toString().compareTo(s2.toString());
        }
    }

    /**
     * Converts a Value to a Singleton.
     *
     * @param value The value.
     * @return A singleton SItemSet containing {@code value} or {@code null} if {@code value} is {@code null}.
     */
    public static SItemSet toSet(Value value) {
        return value == null ? null : UncheckedSet.of(value).getBest();
    }

    /**
     * Converts an array of Values to an array of SItemSets.
     *
     * @param values The values.
     * @return An array containing the conversion of each value of {@code values} to an SItemSet.
     */
    public static SItemSet[] toSetArray(Value... values) {
        final SItemSet[] array = new SItemSet[values.length];
        for (int index = 0; index < array.length; index++) {
            array[index] = toSet(values[index]);
        }
        return array;
    }

    public static SItemSet[] toSetArray(String... contents) {
        final SItemSet[] array = new SItemSet[contents.length];
        for (int index = 0; index < array.length; index++) {
            array[index] = createBest(contents[index]);
        }
        return array;
    }
}