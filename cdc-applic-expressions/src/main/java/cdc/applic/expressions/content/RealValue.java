package cdc.applic.expressions.content;

import cdc.applic.expressions.literals.LiteralUtils;
import cdc.graphs.PartialOrderPosition;
import cdc.graphs.PartiallyComparable;

/**
 * Class representing a single real value.
 *
 * @author Damien Carbonne
 */
public final class RealValue implements RealSItem, Value, Comparable<RealValue>, PartiallyComparable<RealValue> {
    private final double number;

    public static final RealValue MIN_VALUE = new RealValue(-Double.MAX_VALUE);
    public static final RealValue MAX_VALUE = new RealValue(Double.MAX_VALUE);

    private RealValue(double number) {
        // Special handling of -0.0
        // -0.0 != 0.0 in Java (and ...)
        this.number = number == -0.0 ? 0.0 : number;
    }

    private RealValue(String literal) {
        if (!LiteralUtils.isRealLiteral(literal)) {
            throw new IllegalArgumentException("Illegal real literal (" + literal + ")");
        }
        final double x = Double.parseDouble(literal);
        this.number = x == -0.0 ? 0.0 : x;
    }

    /**
     * Creates a {@link RealValue} from a {@code double}.
     *
     * @param number The number.
     * @return A new {@link RealValue} from {@code number}.
     */
    public static RealValue of(double number) {
        return new RealValue(number);
    }

    /**
     * Creates a {@link RealValue} from a its String representation.
     *
     * @param literal The real literal.
     * @return A new {@link RealValue} from {@code literal}.
     * @throws IllegalArgumentException When {@code literal} can not be recognized as a {@code real}.
     */
    public static RealValue of(String literal) {
        return new RealValue(literal);
    }

    public double getNumber() {
        return number;
    }

    @Override
    public String getNonEscapedLiteral() {
        return Double.toString(number);
    }

    @Override
    public String getProtectedLiteral() {
        return getNonEscapedLiteral();
    }

    @Override
    public int compareTo(RealValue o) {
        return Double.compare(number, o.number);
    }

    @Override
    public PartialOrderPosition partialCompareTo(RealValue o) {
        return PartiallyComparable.partialCompare(this, o);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof RealValue)) {
            return false;
        }
        final RealValue other = (RealValue) object;
        return number == other.number;
    }

    @Override
    public int hashCode() {
        return Double.hashCode(number);
    }

    @Override
    public String toString() {
        return getProtectedLiteral();
    }
}