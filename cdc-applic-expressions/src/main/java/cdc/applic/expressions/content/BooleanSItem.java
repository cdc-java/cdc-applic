package cdc.applic.expressions.content;

/**
 * Base interface of boolean items: {@link BooleanValue}.
 * <p>
 * <b>Note:</b> there are no {@code BooleanRange} as this does not seem useful
 * and their use may be counter-intuitive.
 *
 * @author Damien Carbonne
 */
public interface BooleanSItem extends SItem {
    @Override
    default Class<? extends CheckedSet<?, ?>> getCheckedSetClass() {
        return BooleanSet.class;
    }
}