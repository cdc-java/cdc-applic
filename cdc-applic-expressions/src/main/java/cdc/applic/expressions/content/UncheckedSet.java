package cdc.applic.expressions.content;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import cdc.applic.expressions.SyntacticException;
import cdc.applic.expressions.parsing.SItemsParsing;
import cdc.util.lang.Checks;

/**
 * Implementation of {@link SItemSet} containing any {@link SItem}.
 * <p>
 * Such a set may be semantically invalid.
 * <p>
 * Order of definition of SItems is preserved.<br>
 * Duplicates are kept.
 *
 * @author Damien Carbonne
 */
public final class UncheckedSet extends AbstractSItemSet {
    private final List<SItem> items;

    /**
     * The empty {@link UncheckedSet}.
     */
    public static final UncheckedSet EMPTY = of();

    /**
     * Checked view of this set.
     */
    private SItemSet checked = null;

    private UncheckedSet(Collection<? extends SItem> items,
                         boolean check) {
        Checks.isNotNull(items, "items");

        if (items.isEmpty()) {
            this.items = Collections.emptyList();
        } else {
            final List<SItem> tmp = new ArrayList<>(items);
            this.items = Collections.unmodifiableList(tmp);
        }
        if (check) {
            for (final SItem item : items) {
                if (item == null) {
                    throw new IllegalArgumentException("Null item");
                }
            }
        }
    }

    private IllegalArgumentException invalidSet() {
        return new IllegalArgumentException("Invalid set: " + this);
    }

    /**
     * Creates an {@link UncheckedSet} from the String representation of its content.
     *
     * @param content The content.
     * @return A new instance of {@link UncheckedSet}.
     * @throws SyntacticException When {@code content} can not be parsed as an {@link UncheckedSet}.
     */
    public static UncheckedSet of(String content) {
        return new UncheckedSet(SItemsParsing.toSItems(content), false);
    }

    /**
     * Creates an {@link UncheckedSet} from an array of {@link SItem SItems}.
     *
     * @param items The items.
     * @return A new instance of {@link UncheckedSet}.
     * @throws IllegalArgumentException When an item is {@code null}.
     */
    public static UncheckedSet of(SItem... items) {
        return of(Arrays.asList(items));
    }

    /**
     * Creates an {@link UncheckedSet} from a collection of {@link SItem SItems}.
     *
     * @param items The items.
     * @return A new instance of {@link UncheckedSet}.
     * @throws IllegalArgumentException When an item is {@code null}.
     */
    public static UncheckedSet of(Collection<? extends SItem> items) {
        return new UncheckedSet(items, true);
    }

    private void buildChecked() {
        if (checked == null) {
            if (isEmpty()) {
                checked = this;
            } else {
                if (isCompliantWith(BooleanSet.class)) {
                    final BooleanSet result = BooleanSet.convert(this);
                    checked = result;
                } else if (isCompliantWith(IntegerSet.class)) {
                    final IntegerSet result = IntegerSet.convert(this);
                    checked = result;
                } else if (isCompliantWith(RealSet.class)) {
                    final RealSet result = RealSet.convert(this);
                    checked = result;
                } else if (isCompliantWith(StringSet.class)) {
                    final StringSet result = StringSet.convert(this);
                    checked = result;
                }
                // else conversion to checked set is not possible
            }
        }
    }

    @Override
    public SItemSet getChecked() {
        buildChecked();
        return checked;
    }

    @Override
    public SItemSet getBest() {
        buildChecked();
        return checked == null ? this : checked;
    }

    @Override
    public boolean isEmpty() {
        return items.isEmpty();
    }

    @Override
    public boolean containsRanges() {
        for (final SItem item : items) {
            if (item instanceof Range) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isSingleton() {
        if (items.size() == 1) {
            final SItem item = items.get(0);
            return item instanceof Value || ((Range) item).isSingleton();
        } else {
            return false;
        }
    }

    @Override
    public Value getSingletonValue() {
        checkIsSingleton();
        final SItem item = items.get(0);
        return item instanceof Value
                ? (Value) item
                : ((Range) item).getSingletonValue();
    }

    @Override
    public boolean isValid() {
        // Ignore MIXED kind
        return isEmpty()
                || isCompliantWith(BooleanSet.class)
                || isCompliantWith(IntegerSet.class)
                || isCompliantWith(RealSet.class)
                || isCompliantWith(StringSet.class);
    }

    @Override
    public boolean isChecked() {
        return isEmpty();
    }

    @Override
    public List<SItem> getItems() {
        return items;
    }

    @Override
    public boolean contains(SItem item) {
        Checks.isNotNull(item, ITEM);

        if (isEmpty()) {
            return false;
        } else {
            buildChecked();
            if (checked != null) {
                return checked.contains(item);
            } else {
                throw invalidSet();
            }
        }
    }

    @Override
    public SItemSet union(SItem item) {
        Checks.isNotNull(item, ITEM);

        if (isEmpty()) {
            return SItemSetUtils.createBest(item);
        } else {
            buildChecked();
            if (checked != null) {
                return checked.union(item);
            } else {
                throw invalidSet();
            }
        }
    }

    @Override
    public SItemSet union(SItemSet set) {
        Checks.isNotNull(set, SET);

        if (isEmpty()) {
            return set;
        } else if (set.isEmpty()) {
            return this;
        } else {
            buildChecked();
            if (checked != null) {
                return checked.union(set.getChecked());
            } else {
                throw invalidSet();
            }
        }
    }

    @Override
    public SItemSet intersection(SItem item) {
        Checks.isNotNull(item, ITEM);

        if (isEmpty()) {
            return this;
        } else {
            buildChecked();
            if (checked != null) {
                return checked.intersection(item);
            } else {
                throw invalidSet();
            }
        }
    }

    @Override
    public SItemSet intersection(SItemSet set) {
        Checks.isNotNull(set, SET);

        if (isEmpty()) {
            return this;
        } else if (set.isEmpty()) {
            return set;
        } else {
            buildChecked();
            if (checked != null) {
                return checked.intersection(set.getChecked());
            } else {
                throw invalidSet();
            }
        }
    }

    @Override
    public SItemSet remove(SItem item) {
        Checks.isNotNull(item, ITEM);

        if (isEmpty()) {
            return this;
        } else {
            buildChecked();
            if (checked != null) {
                return checked.remove(item);
            } else {
                throw invalidSet();
            }
        }
    }

    @Override
    public SItemSet remove(SItemSet set) {
        Checks.isNotNull(set, SET);

        if (isEmpty() || set.isEmpty()) {
            return this;
        } else {
            buildChecked();
            if (checked != null) {
                return checked.remove(set.getChecked());
            } else {
                throw invalidSet();
            }
        }
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (isEmpty() && object instanceof SItemSet) {
            final SItemSet other = (SItemSet) object;
            if (other.isEmpty()) {
                return true;
            }
        }
        if (!(object instanceof UncheckedSet)) {
            return false;
        }
        final UncheckedSet other = (UncheckedSet) object;
        return items.equals(other.items);
    }

    @Override
    public int hashCode() {
        return items.hashCode();
    }
}