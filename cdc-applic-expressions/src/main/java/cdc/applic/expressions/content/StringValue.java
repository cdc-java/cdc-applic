package cdc.applic.expressions.content;

import cdc.applic.expressions.literals.SName;

/**
 * Class representing a single string value.
 *
 * @author Damien Carbonne
 */
public final class StringValue extends SName implements StringSItem, Value {
    /**
     * Value used to designate all unknown values with pattern types.
     * <p>
     * Special processing is associated to this value that should not be used with normal types.
     */
    public static final StringValue PATTERN_UNKNOWN = of(SName.LITERAL_PATTERN_UNKNOWN);

    private StringValue(String literal,
                        boolean escaped) {
        super(literal, escaped);
    }

    /**
     * Creates a StringValue from an <em>escaped</em> or <em>non-escaped</em> literal.
     *
     * @param literal The literal.
     * @param escaped {@code true} if {@code literal} is <em>escaped</em>.
     * @return A new StringValue or {@code null} if {@code literal} is {@code null}.
     */
    public static StringValue of(String literal,
                                 boolean escaped) {
        return literal == null ? null : new StringValue(literal, escaped);
    }

    /**
     * Creates a StringValue from a <em>non-escaped</em> literal.
     *
     * @param literal The <em>non-escaped</em> literal.
     * @return A new StringValue or {@code null} if {@code literal} is {@code null}.
     */
    public static StringValue of(String literal) {
        return of(literal, false);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof StringValue)) {
            return false;
        }
        final StringValue other = (StringValue) object;
        return getNonEscapedLiteral().equals(other.getNonEscapedLiteral());
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}