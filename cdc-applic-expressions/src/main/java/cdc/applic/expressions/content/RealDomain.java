package cdc.applic.expressions.content;

import java.util.Comparator;

import cdc.util.lang.Checks;

/**
 * Domain implementation dedicated to RealValue and RealRange.
 *
 * @author Damien Carbonne
 */
public final class RealDomain extends AbstractDomain<RealValue, RealRange> {
    public static final RealDomain INSTANCE = new RealDomain();

    private RealDomain() {
    }

    @Override
    public Class<RealValue> getValueClass() {
        return RealValue.class;
    }

    @Override
    public Class<RealRange> getRangeClass() {
        return RealRange.class;
    }

    @Override
    public RealValue min() {
        return RealValue.MIN_VALUE;
    }

    @Override
    public RealValue max() {
        return RealValue.MAX_VALUE;
    }

    @Override
    public RealValue pred(RealValue value) {
        Checks.isTrue(!value.equals(min()), "{} has no predecessor", value);
        return RealValue.of(Math.nextDown(value.getNumber()));
    }

    @Override
    public RealValue succ(RealValue value) {
        Checks.isTrue(!value.equals(max()), "{} has no successor", value);
        return RealValue.of(Math.nextUp(value.getNumber()));
    }

    @Override
    public RealRange create(RealValue value) {
        return RealRange.of(value);
    }

    @Override
    public RealRange create(RealValue min,
                            RealValue max) {
        return RealRange.of(min, max);
    }

    @Override
    public Comparator<RealRange> minComparator() {
        return RealRange.MIN_COMPARATOR;
    }

    @Override
    public Comparator<RealRange> maxComparator() {
        return RealRange.MAX_COMPARATOR;
    }

    public RealValue pred(double value) {
        return pred(RealValue.of(value));
    }

    public RealValue succ(double value) {
        return succ(RealValue.of(value));
    }

    public boolean adjoint(double x,
                           double y) {
        final Position pos = position(RealValue.of(x), RealValue.of(y));
        return pos == Position.ADJOINT_PRED || pos == Position.ADJOINT_SUCC;
    }

    public Position position(double x,
                             double y) {
        return position(RealValue.of(x), RealValue.of(y));
    }
}