package cdc.applic.expressions.content;

import cdc.util.lang.Checks;

public final class RangeUtils {
    private RangeUtils() {
    }

    public static Range create(Value min,
                               Value max) {
        if (min instanceof IntegerValue && max instanceof IntegerValue) {
            return IntegerRange.of((IntegerValue) min, (IntegerValue) max);
        } else if (min instanceof RealValue && max instanceof RealValue) {
            return RealRange.of((RealValue) min, (RealValue) max);
        } else {
            throw new IllegalArgumentException("Non compliant values or non supported types: " + min + " " + max);
        }
    }

    public static <V extends Value & Comparable<? super V>,
                   R extends AbstractRange<V, R>>
            R removeSimple(Domain<V, R> domain,
                           R r1,
                           R r2) {
        if (r1.isEmpty() || r2.isEmpty()) {
            return r1;
        } else {
            final V min = ComparableUtils.max(r1.getMin(), r2.getMin());
            final V max = ComparableUtils.min(r1.getMax(), r2.getMax());
            if (min.compareTo(max) <= 0) {
                // There is an intersection
                if (min.equals(r1.getMin())) {
                    return domain.create(domain.succ(max), r1.getMax());
                } else if (max.equals(r1.getMax())) {
                    return domain.create(r1.getMin(), domain.pred(min));
                } else {
                    throw new IllegalArgumentException("Can not remove" + r1 + " with " + r2);
                }
            } else {
                return r1;
            }
        }
    }

    private static final AbstractRange<?, ?>[] EMPTY = new AbstractRange[0];

    private static <R extends AbstractRange<?, ?>> R[] emptyArray() {
        @SuppressWarnings("unchecked")
        final R[] result = (R[]) EMPTY;
        return result;
    }

    private static <R extends AbstractRange<?, ?>> R[] newArray(R r) {
        @SuppressWarnings("unchecked")
        final R[] result = (R[]) new AbstractRange[1];
        result[0] = r;
        return result;
    }

    private static <R extends AbstractRange<?, ?>> R[] newArray(R r1,
                                                                R r2) {
        @SuppressWarnings("unchecked")
        final R[] result = (R[]) new AbstractRange[2];
        result[0] = r1;
        result[1] = r2;
        return result;
    }

    public static <V extends Value & Comparable<? super V>,
                   R extends AbstractRange<V, R>>
            R[] remove(Domain<V, R> domain,
                       R r1,
                       R r2) {
        if (r1.isEmpty()) {
            return emptyArray();
        } else if (r2.isEmpty()) {
            return newArray(r1);
        } else {
            final V min = ComparableUtils.max(r1.getMin(), r2.getMin());
            final V max = ComparableUtils.min(r1.getMax(), r2.getMax());
            if (min.compareTo(max) <= 0) {
                // There is an intersection
                if (min.equals(r1.getMin())) {
                    // r1.getMin() >= r2.getMin()
                    final R range = domain.create(domain.succ(max), r1.getMax());
                    if (range.isEmpty()) {
                        // ...===R1===
                        // ===R2========
                        return emptyArray();
                    } else {
                        // ...===R1===
                        // ===R2===+++
                        return newArray(range);
                    }
                } else if (max.equals(r1.getMax())) {
                    // r1.getMin() < r2.getMin()
                    // r1.getMax() <= r2.getMax()
                    final R range = domain.create(r1.getMin(), domain.pred(min));
                    return newArray(range);
                } else {
                    // r1.getMin() < r2.getMin()
                    // r1.getMax() > r2.getMax()
                    return newArray(domain.create(r1.getMin(), domain.pred(min)),
                                    domain.create(domain.succ(max), r1.getMax()));
                }
            } else {
                // There is no intersection
                return newArray(r1);
            }
        }
    }

    public static <V extends Value & Comparable<? super V>,
                   R extends AbstractRange<V, R>>
            R intersection(Domain<V, R> domain,
                           R r1,
                           R r2) {
        return domain.create(ComparableUtils.max(r1.getMin(), r2.getMin()),
                             ComparableUtils.min(r1.getMax(), r2.getMax()));
    }

    public static <V extends Value & Comparable<? super V>,
                   R extends AbstractRange<V, R>>
            boolean intersected(R r1,
                                R r2) {
        final V min = ComparableUtils.max(r1.getMin(), r2.getMin());
        final V max = ComparableUtils.min(r1.getMax(), r2.getMax());
        return min.compareTo(max) <= 0;
    }

    public static <V extends Value & Comparable<? super V>,
                   R extends AbstractRange<V, R>>
            boolean mergeable(Domain<V, R> domain,
                              R r1,
                              R r2) {
        final V min = ComparableUtils.max(r1.getMin(), r2.getMin());
        final V max = ComparableUtils.min(r1.getMax(), r2.getMax());
        return min.compareTo(max) <= 0 || domain.adjoint(min, max);
    }

    public static <V extends Value & Comparable<? super V>,
                   R extends AbstractRange<V, R>>
            R union(Domain<V, R> domain,
                    R r1,
                    R r2) {
        final V min = ComparableUtils.max(r1.getMin(), r2.getMin());
        final V max = ComparableUtils.min(r1.getMax(), r2.getMax());
        if (min.compareTo(max) <= 0 || domain.adjoint(min, max)) {
            return domain.create(ComparableUtils.min(r1.getMin(), r2.getMin()),
                                 ComparableUtils.max(r1.getMax(), r2.getMax()));
        } else {
            throw new IllegalArgumentException("Can not merge " + r1 + " with " + r2);
        }
    }

    public static <V extends Value & Comparable<? super V>,
                   R extends AbstractRange<V, R>>
            Position position(Domain<V, R> domain,
                              R r1,
                              R r2) {
        Checks.isFalse(r1.isEmpty(), "Empty r1");
        Checks.isFalse(r2.isEmpty(), "Empty r2");

        if (ComparableUtils.lessThan(r1.getMax(), r2.getMin())) {
            if (r1.getMax().equals(domain.pred(r2.getMin()))) {
                return Position.ADJOINT_PRED;
            } else {
                return Position.DISJOINT_PRED;
            }
        } else if (ComparableUtils.greaterThan(r1.getMin(), r2.getMax())) {
            if (domain.pred(r1.getMin()).equals(r2.getMax())) {
                return Position.ADJOINT_SUCC;
            } else {
                return Position.DISJOINT_SUCC;
            }
        } else {
            return Position.OVERLAPPING;
        }
    }

    public static <V extends Value & Comparable<? super V>,
                   R extends AbstractRange<V, R>>
            boolean adjoint(Domain<V, R> domain,
                            R r1,
                            R r2) {
        final Position pos = position(domain, r1, r2);
        return pos == Position.ADJOINT_PRED || pos == Position.ADJOINT_SUCC;
    }
}