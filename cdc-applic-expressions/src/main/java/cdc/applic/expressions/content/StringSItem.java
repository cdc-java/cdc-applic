package cdc.applic.expressions.content;

/**
 * Base interface of string items: {@link StringValue}.
 * <p>
 * <b>Note:</b> there are no {@code StringRange} because no solution has been found to manipulate them.<br>
 * In addition, should the lexicographic order always apply?<br>
 * Would this be expected with enumerated types?
 *
 * @author Damien Carbonne
 */
public interface StringSItem extends SItem {
    @Override
    default Class<? extends CheckedSet<?, ?>> getCheckedSetClass() {
        return StringSet.class;
    }
}