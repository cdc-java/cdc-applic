package cdc.applic.expressions.content;

/**
 * Base interface of integer items: {@link IntegerValue} and {@link IntegerRange}.
 *
 * @author Damien Carbonne
 */
public interface IntegerSItem extends SItem {
    @Override
    default Class<? extends CheckedSet<?, ?>> getCheckedSetClass() {
        return IntegerSet.class;
    }
}