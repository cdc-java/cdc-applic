package cdc.applic.expressions.content;

import cdc.applic.expressions.literals.LiteralUtils;
import cdc.graphs.PartialOrderPosition;
import cdc.graphs.PartiallyComparable;

/**
 * Class representing a single integer value.
 *
 * @author Damien Carbonne
 */
public final class IntegerValue implements IntegerSItem, Value, Comparable<IntegerValue>, PartiallyComparable<IntegerValue> {
    private final int number;

    public static final IntegerValue MIN_VALUE = new IntegerValue(Integer.MIN_VALUE);
    public static final IntegerValue MAX_VALUE = new IntegerValue(Integer.MAX_VALUE);

    private IntegerValue(int number) {
        this.number = number;
    }

    private IntegerValue(String literal) {
        if (!LiteralUtils.isIntegerLiteral(literal)) {
            throw new IllegalArgumentException("Illegal integer literal (" + literal + ")");
        }
        this.number = Integer.parseInt(literal);
    }

    /**
     * Creates an {@link IntegerValue} from an {@code int}.
     *
     * @param number The number.
     * @return A new {@link IntegerValue} from {@code number}.
     */
    public static IntegerValue of(int number) {
        return new IntegerValue(number);
    }

    /**
     * Creates an {@link IntegerValue} from a its String representation.
     *
     * @param literal The integer literal.
     * @return A new {@link IntegerValue} from {@code literal}.
     * @throws IllegalArgumentException When {@code literal} can not be recognized as an {@code integer}.
     */
    public static IntegerValue of(String literal) {
        return new IntegerValue(literal);
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String getNonEscapedLiteral() {
        return Integer.toString(number);
    }

    @Override
    public String getProtectedLiteral() {
        return getNonEscapedLiteral();
    }

    @Override
    public int compareTo(IntegerValue o) {
        return Integer.compare(number, o.number);
    }

    @Override
    public PartialOrderPosition partialCompareTo(IntegerValue o) {
        return PartiallyComparable.partialCompare(this, o);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof IntegerValue)) {
            return false;
        }
        final IntegerValue other = (IntegerValue) object;
        return number == other.number;
    }

    @Override
    public int hashCode() {
        return number;
    }

    @Override
    public String toString() {
        return getProtectedLiteral();
    }
}