package cdc.applic.expressions.content;

public final class ComparableUtils {
    private ComparableUtils() {
    }

    public static <V extends Comparable<? super V>> V min(V x,
                                                          V y) {
        return x.compareTo(y) <= 0 ? x : y;
    }

    public static <V extends Comparable<? super V>> V max(V x,
                                                          V y) {
        return x.compareTo(y) >= 0 ? x : y;
    }

    public static <V extends Comparable<? super V>> boolean lessThan(V x,
                                                                     V y) {
        return x.compareTo(y) < 0;
    }

    public static <V extends Comparable<? super V>> boolean lessOrEqual(V x,
                                                                        V y) {
        return x.compareTo(y) <= 0;
    }

    public static <V extends Comparable<? super V>> boolean equal(V x,
                                                                  V y) {
        return x.compareTo(y) == 0;
    }

    public static <V extends Comparable<? super V>> boolean greaterThan(V x,
                                                                        V y) {
        return x.compareTo(y) > 0;
    }

    public static <V extends Comparable<? super V>> boolean greaterOrEqual(V x,
                                                                           V y) {
        return x.compareTo(y) >= 0;
    }
}