package cdc.applic.expressions.content;

import java.util.Comparator;

/**
 * Class representing an integer range.
 *
 * @author Damien Carbonne
 */
public final class IntegerRange extends AbstractRange<IntegerValue, IntegerRange> implements IntegerSItem {
    public static final Comparator<IntegerRange> MIN_COMPARATOR = minComparator();
    public static final Comparator<IntegerRange> MAX_COMPARATOR = maxComparator();

    private IntegerRange(IntegerValue value) {
        super(value);
    }

    private IntegerRange(IntegerValue min,
                         IntegerValue max) {
        super(min, max);
    }

    private IntegerRange(int value) {
        this(IntegerValue.of(value));
    }

    private IntegerRange(int min,
                         int max) {
        this(IntegerValue.of(min),
             IntegerValue.of(max));
    }

    @Override
    protected IntegerRange self() {
        return this;
    }

    @Override
    public IntegerDomain getDomain() {
        return IntegerDomain.INSTANCE;
    }

    /**
     * Creates an {@link IntegerRange} from a single {@link IntegerValue}.
     *
     * @param value The value.
     * @return A new {@link IntegerRange} from {@code value}.
     */
    public static IntegerRange of(IntegerValue value) {
        return new IntegerRange(value);
    }

    /**
     * Creates an {@link IntegerRange} from 2 {@link IntegerValue IntegerValues}.
     *
     * @param min The min value.
     * @param max The max value.
     * @return A new {@link IntegerRange} from {@code min} and {@code max}.
     */
    public static IntegerRange of(IntegerValue min,
                                  IntegerValue max) {
        return new IntegerRange(min, max);
    }

    /**
     * Creates an {@link IntegerRange} from a single {@code int}.
     *
     * @param value The value.
     * @return A new {@link IntegerRange} from {@code value}.
     */
    public static IntegerRange of(int value) {
        return new IntegerRange(value);
    }

    /**
     * Creates an {@link IntegerRange} from 2 {@code ints}.
     *
     * @param min The min value.
     * @param max The max value.
     * @return A new {@link IntegerRange} from {@code min} and {@code max}.
     */
    public static IntegerRange of(int min,
                                  int max) {
        return new IntegerRange(min, max);
    }

    public boolean contains(int value) {
        return containsValue(IntegerValue.of(value));
    }

    public long getExtent() {
        return isEmpty()
                ? 0L
                : getMax().getNumber() - getMin().getNumber() + 1;
    }
}