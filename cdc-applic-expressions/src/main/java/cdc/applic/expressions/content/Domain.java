package cdc.applic.expressions.content;

import java.util.Comparator;

import cdc.util.debug.Printable;

/**
 * A Domain describes characteristics of discrete set of ordered values.
 * <p>
 * Can be used for numerical integer and real values. We consider real values as discrete.
 * Can also be used for texts.
 *
 * @author Damien Carbonne
 *
 * @param <V> The value type.
 * @param <R> The range type.
 */
public interface Domain<V extends Value & Comparable<? super V>,
                        R extends AbstractRange<V, R>>
        extends Printable {
    /**
     * @return The value class.
     */
    public Class<V> getValueClass();

    /**
     * @return The range class.
     */
    public Class<R> getRangeClass();

    // TODO min may not exist
    /**
     * @return The minimum value of the domain.
     */
    public V min();

    // TODO max may not exist
    /**
     * @return The maximum value of the domain.
     */
    public V max();

    /**
     * Returns the immediate predecessor of a value.
     *
     * @param value The value.
     * @return The predecessor of {@code value}.
     * @throws IllegalArgumentException When {@code value} has no predecessor.
     */
    public V pred(V value);

    /**
     * Returns the immediate successor of a value.
     *
     * @param value The value.
     * @return The successor of {@code value}.
     * @throws IllegalArgumentException When {@code value} has no successor.
     */
    public V succ(V value);

    /**
     * Creates a new singleton range.
     *
     * @param value The singleton value.
     * @return A singleton range containing  {@code value}.
     */
    public R create(V value);

    /**
     * Creates a new range.
     *
     * @param min The minimum value.
     * @param max The maximum value.
     * @return A new range.
     */
    public R create(V min,
                    V max);

    public Comparator<R> minComparator();

    public Comparator<R> maxComparator();

    /**
     * Returns {@code true} if 2 values are adjoint.
     *
     * @param x The first value.
     * @param y The second value.
     * @return {@code true} if {@code x} and {@code y} are adjoint.
     */
    public default boolean adjoint(V x,
                                   V y) {
        final Position pos = position(x, y);
        return pos == Position.ADJOINT_PRED || pos == Position.ADJOINT_SUCC;
    }

    /**
     * Returns the relative position of 2 values.
     *
     * @param x The first value.
     * @param y The second value.
     * @return The relative position of {@code x} and {@code y}.
     */
    public default Position position(V x,
                                     V y) {
        final int cmp = x.compareTo(y);
        final Position result;
        if (cmp == 0) {
            result = Position.OVERLAPPING;
        } else if (cmp < 0) {
            result = x.compareTo(pred(y)) == 0
                    ? Position.ADJOINT_PRED
                    : Position.DISJOINT_PRED;
        } else {
            result = x.compareTo(succ(y)) == 0
                    ? Position.ADJOINT_SUCC
                    : Position.DISJOINT_SUCC;
        }
        return result;
    }
}