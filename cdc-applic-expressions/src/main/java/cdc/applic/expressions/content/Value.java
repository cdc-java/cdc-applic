package cdc.applic.expressions.content;

import java.util.Comparator;

/**
 * Base interface of items that are simple values.
 *
 * @author Damien Carbonne
 */
public interface Value extends SItem {
    public static final Comparator<Value> COMPARATOR = ValueUtils::compare;
}