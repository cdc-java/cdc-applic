package cdc.applic.expressions.content;

import java.io.PrintStream;

public abstract class AbstractDomain<V extends Value & Comparable<? super V>,
                                     R extends AbstractRange<V, R>>
        implements Domain<V, R> {
    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        out.println(getClass().getSimpleName() + " <" + getValueClass().getSimpleName()
                + ", " + getRangeClass().getSimpleName() + ">");
        indent(out, level + 1);
        out.println("min: " + min());
        indent(out, level + 1);
        out.println("max: " + max());
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}