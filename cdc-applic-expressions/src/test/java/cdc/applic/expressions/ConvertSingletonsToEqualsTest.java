package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.visitors.ConvertSingletonsToEquals;

class ConvertSingletonsToEqualsTest {
    private static void check(String expression,
                              String expected) {
        final Expression x = new Expression(expression);
        final Expression x1 = ConvertSingletonsToEquals.execute(x.getRootNode())
                                                       .toExpression(Formatting.SHORT_NARROW);
        assertEquals(expected, x1.toString());
    }

    @Test
    void test() {
        check("A and B", "A&B");
        check("E=E1", "E=E1");
        check("E<:{E1}", "E=E1");
        check("E!<:{E1}", "E!=E1");
        check("E<:{E1,E2}", "E<:{E1,E2}");
        check("E!<:{E1,E2}", "E!<:{E1,E2}");
        check("I<:{1~2}", "I<:{1~2}");
        check("I<:{1~1}", "I=1");
        check("I!<:{1~2}", "I!<:{1~2}");
        check("I!<:{1~1}", "I!=1");
    }
}