package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.Named;

class NamedTest {
    private static class X implements Named {
        private final Name name;

        public X(String s) {
            this.name = Name.of(s);
        }

        @Override
        public Name getName() {
            return name;
        }
    }

    @Test
    void testCompare() {
        final X x1 = new X("A");
        final X x2 = new X("B");
        assertEquals(0, Named.NAME_COMPARATOR.compare(x1, x1));
        assertEquals(0, Named.NAME_COMPARATOR.compare(x2, x2));
        assertTrue(Named.NAME_COMPARATOR.compare(x1, x2) < 0);
        assertTrue(Named.NAME_COMPARATOR.compare(x2, x1) > 0);
    }
}