package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.literals.EscapingMode;

class StringValueTest {
    private static void testConstructor(String unescaped,
                                        String escaped) {
        final StringValue unescapedValue = StringValue.of(unescaped, false);
        final StringValue unescapedValue2 = StringValue.of(unescaped);
        final StringValue escapedValue = StringValue.of(escaped, true);

        assertEquals(unescapedValue, unescapedValue2);
        assertEquals(unescaped, escapedValue.getNonEscapedLiteral());
        assertEquals(escaped, escapedValue.getEscapedLiteral());
        assertEquals(escaped, escapedValue.getLiteral(EscapingMode.ESCAPED));
        assertEquals(unescaped, escapedValue.getLiteral(EscapingMode.NON_ESCAPED));

        assertEquals(unescaped, unescapedValue.getNonEscapedLiteral());
        assertEquals(escaped, unescapedValue.getEscapedLiteral());
        assertEquals(escaped, unescapedValue.getLiteral(EscapingMode.ESCAPED));
        assertEquals(unescaped, unescapedValue.getLiteral(EscapingMode.NON_ESCAPED));

        assertEquals(escapedValue, escapedValue);
        assertEquals(escapedValue, unescapedValue);
        assertNotEquals(escapedValue, null);

        if (escapedValue.needsEscape()) {
            assertEquals(escaped, escapedValue.getLiteral(EscapingMode.PROTECTED));
        } else {
            assertEquals(unescaped, escapedValue.getLiteral(EscapingMode.PROTECTED));
        }
    }

    @Test
    void testConstructor() {
        testConstructor("a", "\"a\"");
        testConstructor("10", "\"10\"");
    }

    @Test
    void testInvalidArg() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         StringValue.of("Hello", true);
                     });
    }

    @Test
    void testEquals() {
        assertEquals(StringValue.of("Hello", false), StringValue.of("\"Hello\"", true));
        assertNotEquals(StringValue.of("Hello", false), StringValue.of("Hel lo", false));
        assertNotEquals(StringValue.of("Hello", false), null);
        assertNotEquals(StringValue.of("Hello", false), "Hello");
    }

    @Test
    void testToString() {
        assertEquals("Hello", StringValue.of("Hello", false).toString());
        assertEquals("Hello", StringValue.of("\"Hello\"", true).toString());
        assertEquals("\"Hel lo\"", StringValue.of("Hel lo", false).toString());
        assertEquals("\"Hel lo\"", StringValue.of("\"Hel lo\"", true).toString());
        assertEquals("\"\"\"Hel lo\"\"\"", StringValue.of("\"Hel lo\"", false).toString());
    }

    @Test
    void testHashCode() {
        assertEquals(StringValue.of("Hello", false).hashCode(), StringValue.of("Hello", false).hashCode());
        assertNotEquals(StringValue.of("Hello", false).hashCode(), StringValue.of("Hel lo", false).hashCode());
    }

    @Test
    void testCompare() {
        assertTrue(StringValue.of("Hello").compareTo(StringValue.of("World")) < 0);
    }
}