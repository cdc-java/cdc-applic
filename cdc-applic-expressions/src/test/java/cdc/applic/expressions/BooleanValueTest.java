package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.UncheckedSet;

class BooleanValueTest {
    private static void testConstructor(boolean value,
                                        String literal) {
        final BooleanValue fromValue = BooleanValue.of(value);
        final BooleanValue fromLiteral = BooleanValue.of(literal);

        assertEquals(fromValue, fromLiteral);
        assertEquals(value, fromValue.getValue());
        assertEquals(value, fromLiteral.getValue());
        assertNotEquals(value, fromValue);
        assertNotEquals(value, fromLiteral);
        assertTrue(fromValue.isCompliantWith(BooleanSet.class));
        assertTrue(fromValue.isCompliantWith(UncheckedSet.class));
        assertFalse(fromValue.isCompliantWith(IntegerSet.class));
        assertFalse(fromValue.isCompliantWith(RealSet.class));
        assertFalse(fromValue.isCompliantWith(StringSet.class));

        assertEquals(fromValue.getCheckedSetClass(), BooleanSet.class);
    }

    @Test
    void testConstructor() {
        testConstructor(true, "true");
        testConstructor(true, "TRUE");
        testConstructor(false, "false");
        testConstructor(false, "FALSE");
    }

    @Test
    void testInvalidArg() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanValue.of("0");
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanValue.of("1");
                     });
    }

    @Test
    void testEquals() {
        assertEquals(BooleanValue.TRUE, BooleanValue.TRUE);
        assertEquals(BooleanValue.FALSE, BooleanValue.FALSE);
        assertNotEquals(BooleanValue.FALSE, BooleanValue.TRUE);
        assertNotEquals(BooleanValue.TRUE, BooleanValue.FALSE);
        assertNotEquals(BooleanValue.TRUE, true);
        assertNotEquals(BooleanValue.TRUE, "true");
    }

    @Test
    void testNegate() {
        assertEquals(BooleanValue.FALSE, BooleanValue.TRUE.negate());
        assertEquals(BooleanValue.TRUE, BooleanValue.FALSE.negate());
    }

    @Test
    void testGetProtectedLiteral() {
        assertEquals("true", BooleanValue.TRUE.getProtectedLiteral());
        assertEquals("false", BooleanValue.FALSE.getProtectedLiteral());
    }

    @Test
    void testToString() {
        assertEquals("true", BooleanValue.TRUE.toString());
        assertEquals("false", BooleanValue.FALSE.toString());
    }

    @Test
    void testHashCode() {
        assertNotEquals(BooleanValue.TRUE.hashCode(), BooleanValue.FALSE.hashCode());
    }

    @Test
    void testCompareTo() {
        assertTrue(BooleanValue.FALSE.compareTo(null) > 0);
        assertTrue(BooleanValue.TRUE.compareTo(null) > 0);
        assertTrue(BooleanValue.FALSE.compareTo(BooleanValue.TRUE) < 0);
        assertSame(0, BooleanValue.TRUE.compareTo(BooleanValue.TRUE));
        assertTrue(BooleanValue.TRUE.compareTo(BooleanValue.FALSE) > 0);
    }
}