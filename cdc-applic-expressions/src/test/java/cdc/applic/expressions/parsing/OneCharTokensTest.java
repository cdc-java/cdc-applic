package cdc.applic.expressions.parsing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.util.lang.UnexpectedValueException;

class OneCharTokensTest {
    @Test
    void testMatcher() {
        // Should test power of 2
        assertEquals(64, OneCharTokens.STABLE.length());

        for (char c = 0; c < 65535; c++) {
            final boolean bbs = OneCharTokens.BIT_SET_MATCHER.test(c);
            final boolean bba = OneCharTokens.BOOLEAN_ARRAY_MATCHER.test(c);
            final boolean bms = OneCharTokens.MULTIPLY_SHIFT_MATCHER.test(c);
            final boolean bms2 = OneCharTokens.MULTIPLY_SHIFT_INLINE_MATCHER.test(c);
            assertEquals(bbs, bba, "'" + c + "' bit set: " + bbs + " boolean array: " + bba);
            assertEquals(bbs, bms, "'" + c + "' bit set: " + bbs + " multiply shift: " + bba);
            assertEquals(bbs, bms2, "'" + c + "' bit set: " + bbs + " multiply shift inline: " + bba);
        }
    }

    @Test
    void testMapper() {
        for (char c = 0; c < 65535; c++) {
            if (OneCharTokens.BEST_MATCHER.test(c)) {
                final TokenType st = OneCharTokens.SWITCH_MAPPER.apply(c);
                final TokenType mst = OneCharTokens.MULTIPLY_SHIFT_INLINE_MAPPER.apply(c);
                assertEquals(st, mst);
            }
        }
        assertThrows(UnexpectedValueException.class,
                     () -> {
                         OneCharTokens.SWITCH_MAPPER.apply(' ');
                     });
    }
}