package cdc.applic.expressions.parsing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class OneCharEscapesTest {
    @Test
    void testMatcher() {
        // Should test power of 2
        assertEquals(128, OneCharEscapes.STABLE.length());

        for (char c = 0; c < 65535; c++) {
            final boolean bbs = OneCharEscapes.BIT_SET_MATCHER.test(c);
            final boolean bba = OneCharEscapes.BOOLEAN_ARRAY_MATCHER.test(c);
            final boolean bms = OneCharEscapes.MULTIPLY_SHIFT_MATCHER.test(c);
            final boolean bms2 = OneCharEscapes.MULTIPLY_SHIFT_INLINE_MATCHER.test(c);
            assertEquals(bbs, bba, "'" + c + "' bit set: " + bbs + " boolean array: " + bba);
            assertEquals(bbs, bms, "'" + c + "' bit set: " + bbs + " multiply shift: " + bba);
            assertEquals(bbs, bms2, "'" + c + "' bit set: " + bbs + " multiply shift inline: " + bba);
        }
    }
}