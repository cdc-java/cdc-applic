package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.parsing.OperatorType;
import cdc.applic.expressions.parsing.TokenType;

class OperatorTypeTest {
    @Test
    void testFrom() {
        assertEquals(OperatorType.AND, OperatorType.from(TokenType.AND));
        assertEquals(OperatorType.OR, OperatorType.from(TokenType.OR));
        assertEquals(OperatorType.NOT, OperatorType.from(TokenType.NOT));
        assertEquals(OperatorType.IMPLICATION, OperatorType.from(TokenType.IMPL));
        assertEquals(OperatorType.EQUIVALENCE, OperatorType.from(TokenType.EQUIV));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         OperatorType.from(null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         OperatorType.from(TokenType.CLOSE_PAREN);
                     });
    }
}