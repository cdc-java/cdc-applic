package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.LexicalIssue;
import cdc.applic.expressions.checks.SyntaxChecker;
import cdc.applic.expressions.checks.SyntaxIssue;
import cdc.issues.Diagnosis;

class SyntaxCheckerTest {
    private static final Logger LOGGER = LogManager.getLogger(SyntaxCheckerTest.class);

    private static void checkLexical(String expression,
                                     LexicalException.Detail detail) {
        LOGGER.debug("checkLexical({})", expression);
        final boolean compliant = detail == null;
        assertEquals(compliant, SyntaxChecker.INSTANCE.isCompliant(expression));
        final List<ApplicIssue> issues = new ArrayList<>();
        SyntaxChecker.INSTANCE.check(expression, issues);
        LOGGER.debug(SyntaxChecker.INSTANCE.getRuleName() + " issues: " + issues);

        final Diagnosis<ApplicIssue> diagnosis = SyntaxChecker.INSTANCE.check(expression);
        LOGGER.debug(SyntaxChecker.INSTANCE.getRuleName() + " " + diagnosis);

        if (compliant) {
            assertEquals(0, issues.size());
        } else {
            assertEquals(1, issues.size());
            final ApplicIssue issue = issues.get(0);
            assertEquals(LexicalException.class, issue.getCause().getClass());
            assertEquals(detail, ((LexicalIssue) issue).getDetail());
        }
    }

    private static void checkSyntactic(String expression,
                                       SyntacticException.Detail detail) {
        LOGGER.debug("checkSyntactic({})", expression);
        final boolean compliant = detail == null;
        assertEquals(compliant, SyntaxChecker.INSTANCE.isCompliant(expression));
        final List<ApplicIssue> issues = new ArrayList<>();
        SyntaxChecker.INSTANCE.check(expression, issues);
        LOGGER.debug(SyntaxChecker.INSTANCE.getRuleName() + " issues: " + issues);

        final Diagnosis<ApplicIssue> diagnosis = SyntaxChecker.INSTANCE.check(expression);
        LOGGER.debug(SyntaxChecker.INSTANCE.getRuleName() + " " + diagnosis);

        if (compliant) {
            assertEquals(0, issues.size());
        } else {
            assertEquals(1, issues.size());
            final ApplicIssue issue = issues.get(0);
            assertEquals(SyntacticException.class, issue.getCause().getClass());
            assertEquals(detail, ((SyntaxIssue) issue).getDetail());
        }
    }

    @Test
    void test() {
        checkLexical("", null);

        checkLexical("1.1.1", LexicalException.Detail.MISSING_BOUNDARY);
        checkLexical("1.a", LexicalException.Detail.INVALID_NUMBER);
        checkLexical("\"1", LexicalException.Detail.MISSING_CLOSING_DOUBLE_QUOTES);
        checkLexical("\"\"", LexicalException.Detail.EMPTY_DOUBLE_QUOTES_ESCAPED_TEXT);

        checkSyntactic("", null);
        checkSyntactic("A A", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkSyntactic("{M", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkSyntactic("V in {M", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkSyntactic("1~a", SyntacticException.Detail.UNEXPECTED_TOKEN);
    }
}