package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.expressions.ast.visitors.IsCNF;
import cdc.applic.expressions.ast.visitors.IsDNF;

class IsCNFDNFTest {
    private static void check(String expression,
                              boolean expectedCNF,
                              boolean expectedDNF) {
        final Expression x = new Expression(expression);
        assertEquals(expectedCNF, IsCNF.isCNF(ConvertToNary.execute(x.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY)));
        assertEquals(expectedDNF, IsDNF.isDNF(ConvertToNary.execute(x.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY)));
    }

    @Test
    void test() {
        check("true", true, true);
        check("false", true, true);
        check("X", true, true);
        check("!X", true, true);
        check("X -> X", false, false);
        check("X <-> X", false, false);
        check("!!X", false, false);

        check("X or X", true, true);
        check("X and X", true, true);

        check("X or X or X", true, true);
        check("X and X and X", true, true);

        check("(X or X) and (X or X)", true, false);
        check("(X and X) or (X and X)", false, true);

        check("(X or X) and (X or X) and (X or X)", true, false);
        check("(X and X) or (X and X) or (X and X)", false, true);

        check("X or ((X or X) and (X or X) and (X or X))", false, false);
        check("X and ((X and X) or (X and X) or (X and X))", false, false);

        check("X and (X or ((X or X) and (X or X) and (X or X)))", false, false);
        check("X or (X and ((X and X) or (X and X) or (X and X)))", false, false);

        check("(X or X or X) and (X or X or X)", true, false);
        check("(X and X and X) or (X and X and X)", false, true);

        check("!(X or X or X) and (!X or X or X)", false, false);
        check("!(X and X and X) or (!X and X and X)", false, false);
        check("!(X and !X or X and X or X and X) and (X and X or X and X or X and X)", false, false);
        check("!((X or !X) and (X or X) and (X or X)) or ((X or X) and (X or X) and (X or X))", false, false);
    }
}