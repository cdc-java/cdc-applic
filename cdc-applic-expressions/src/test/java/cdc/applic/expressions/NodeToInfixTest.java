package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.visitors.ConvertToNary;

class NodeToInfixTest {
    private static void check(String expression,
                              Formatting formatting,
                              String expected) {
        final Expression x = new Expression(expression);
        assertEquals(expected, x.toInfix(formatting));
        assertEquals(expected, ConvertToNary.execute(x.getRootNode(), ConvertToNary.Variant.ALWAYS).toInfix(formatting));
    }

    private static void checkShortNarrow(String expression,
                                         String expected) {
        check(expression, Formatting.SHORT_NARROW, expected);
    }

    private static void checkShortWide(String expression,
                                       String expected) {
        check(expression, Formatting.SHORT_WIDE, expected);
    }

    private static void checkLongNarrow(String expression,
                                        String expected) {
        check(expression, Formatting.LONG_NARROW, expected);
    }

    private static void checkLongWide(String expression,
                                      String expected) {
        check(expression, Formatting.LONG_WIDE, expected);
    }

    private static void checkMathNarrow(String expression,
                                        String expected) {
        check(expression, Formatting.MATH_NARROW, expected);
    }

    private static void checkMathWide(String expression,
                                      String expected) {
        check(expression, Formatting.MATH_WIDE, expected);
    }

    @Test
    void testShortNarrow() {
        checkShortNarrow("A in {}", "A<:{}");
        checkShortNarrow("true", "true");
        checkShortNarrow("false", "false");
        checkShortNarrow("true & false", "true&false");
        checkShortNarrow("A", "A");
        checkShortNarrow("! A", "!A");
        checkShortNarrow("A and B", "A&B");
        checkShortNarrow("A and B and C", "A&B&C");
        checkShortNarrow("A or B", "A|B");
        checkShortNarrow("A or B or C", "A|B|C");
        checkShortNarrow("A -> B", "A->B");
        checkShortNarrow("A -> B -> C", "(A->B)->C");
        checkShortNarrow("A <-> B", "A<->B");
        checkShortNarrow("A <-> B <-> C", "(A<->B)<->C");
        checkShortNarrow("A = A1", "A=A1");
        checkShortNarrow("A != A1", "A!=A1");
        checkShortNarrow("A in {A1}", "A<:{A1}");
        checkShortNarrow("A not in {A1}", "A!<:{A1}");
        checkShortNarrow("A = A1 & B != B1", "A=A1&B!=B1");
        checkShortNarrow("A in {A1} & B not in {B1}", "A<:{A1}&B!<:{B1}");
        checkShortNarrow("!A  & !A", "!A&!A");
        checkShortNarrow("!A & (C|D)", "!A&(C|D)");
        checkShortNarrow("A < B", "A<B");
        checkShortNarrow("A <= B", "A<=B");
        checkShortNarrow("A !< B", "A!<B");
        checkShortNarrow("A !<= B", "A!<=B");
        checkShortNarrow("A > B", "A>B");
        checkShortNarrow("A >= B", "A>=B");
        checkShortNarrow("A !> B", "A!>B");
        checkShortNarrow("A !>= B", "A!>=B");

    }

    @Test
    void testShortWide() {
        checkShortWide("A in {}", "A <: {}");
        checkShortWide("true", "true");
        checkShortWide("false", "false");
        checkShortWide("true & false", "true & false");
        checkShortWide("A", "A");
        checkShortWide("! A", "!A");
        checkShortWide("A and B", "A & B");
        checkShortWide("A and B and C", "A & B & C");
        checkShortWide("A or B", "A | B");
        checkShortWide("A or B or C", "A | B | C");
        checkShortWide("A -> B", "A -> B");
        checkShortWide("A -> B -> C", "(A -> B) -> C");
        checkShortWide("A <-> B", "A <-> B");
        checkShortWide("A <-> B <-> C", "(A <-> B) <-> C");
        checkShortWide("A = A1", "A = A1");
        checkShortWide("A != A1", "A != A1");
        checkShortWide("A in {A1}", "A <: {A1}");
        checkShortWide("A not in {A1}", "A !<: {A1}");
        checkShortWide("A = A1 & B != B1", "A = A1 & B != B1");
        checkShortWide("A in {A1} & B not in {B1}", "A <: {A1} & B !<: {B1}");
        checkShortWide("!A  & !A", "!A & !A");
        checkShortWide("!A & (C|D)", "!A & (C | D)");
        checkShortWide("A < B", "A < B");
        checkShortWide("A <= B", "A <= B");
        checkShortWide("A !< B", "A !< B");
        checkShortWide("A !<= B", "A !<= B");
        checkShortWide("A > B", "A > B");
        checkShortWide("A >= B", "A >= B");
        checkShortWide("A !> B", "A !> B");
        checkShortWide("A !>= B", "A !>= B");
    }

    @Test
    void testLongNarrow() {
        checkLongNarrow("A in {}", "A in{}");
        checkLongNarrow("true", "true");
        checkLongNarrow("false", "false");
        checkLongNarrow("true & false", "true and false");
        checkLongNarrow("A", "A");
        checkLongNarrow("! A", "not A");
        checkLongNarrow("A and B", "A and B");
        checkLongNarrow("A and B and C", "A and B and C");
        checkLongNarrow("A or B", "A or B");
        checkLongNarrow("A or B or C", "A or B or C");
        checkLongNarrow("A -> B", "A imp B");
        checkLongNarrow("A -> B -> C", "(A imp B)imp C");
        checkLongNarrow("A <-> B", "A iff B");
        checkLongNarrow("A <-> B <-> C", "(A iff B)iff C");
        checkLongNarrow("A = A1", "A=A1");
        checkLongNarrow("A != A1", "A!=A1");
        checkLongNarrow("A in {A1}", "A in{A1}");
        checkLongNarrow("A not in {A1}", "A not in{A1}");
        checkLongNarrow("A = A1 & B != B1", "A=A1 and B!=B1");
        checkLongNarrow("A in {A1} & B not in {B1}", "A in{A1} and B not in{B1}");
        checkLongNarrow("!A  & !A", "not A and not A");
        checkLongNarrow("!A & (C|D)", "not A and(C or D)");
        checkLongNarrow("A < B", "A<B");
        checkLongNarrow("A <= B", "A<=B");
        checkLongNarrow("A !< B", "A!<B");
        checkLongNarrow("A !<= B", "A!<=B");
        checkLongNarrow("A > B", "A>B");
        checkLongNarrow("A >= B", "A>=B");
        checkLongNarrow("A !> B", "A!>B");
        checkLongNarrow("A !>= B", "A!>=B");
    }

    @Test
    void testLongWide() {
        checkLongWide("A in {}", "A in {}");
        checkLongWide("true", "true");
        checkLongWide("false", "false");
        checkLongWide("true & false", "true and false");
        checkLongWide("A", "A");
        checkLongWide("! A", "not A");
        checkLongWide("A and B", "A and B");
        checkLongWide("A and B and C", "A and B and C");
        checkLongWide("A or B", "A or B");
        checkLongWide("A or B or C", "A or B or C");
        checkLongWide("A -> B", "A imp B");
        checkLongWide("A -> B -> C", "(A imp B) imp C");
        checkLongWide("A <-> B", "A iff B");
        checkLongWide("A <-> B <-> C", "(A iff B) iff C");
        checkLongWide("A = A1", "A = A1");
        checkLongWide("A != A1", "A != A1");
        checkLongWide("A in {A1}", "A in {A1}");
        checkLongWide("A not in {A1}", "A not in {A1}");
        checkLongWide("A = A1 & B != B1", "A = A1 and B != B1");
        checkLongWide("A in {A1} & B not in {B1}", "A in {A1} and B not in {B1}");
        checkLongWide("!A  & !A", "not A and not A");
        checkLongWide("!A & (C|D)", "not A and (C or D)");
        checkLongWide("A < B", "A < B");
        checkLongWide("A <= B", "A <= B");
        checkLongWide("A !< B", "A !< B");
        checkLongWide("A !<= B", "A !<= B");
        checkLongWide("A > B", "A > B");
        checkLongWide("A >= B", "A >= B");
        checkLongWide("A !> B", "A !> B");
        checkLongWide("A !>= B", "A !>= B");
    }

    @Test
    void testMathNarrow() {
        checkMathNarrow("A in {}", "A∈∅");
        checkMathNarrow("true", "⊤");
        checkMathNarrow("false", "⊥");
        checkMathNarrow("true & false", "⊤∧⊥");
        checkMathNarrow("A", "A");
        checkMathNarrow("! A", "¬A");
        checkMathNarrow("A and B", "A∧B");
        checkMathNarrow("A and B and C", "A∧B∧C");
        checkMathNarrow("A or B", "A∨B");
        checkMathNarrow("A or B or C", "A∨B∨C");
        checkMathNarrow("A -> B", "A→B");
        checkMathNarrow("A -> B -> C", "(A→B)→C");
        checkMathNarrow("A <-> B", "A↔B");
        checkMathNarrow("A <-> B <-> C", "(A↔B)↔C");
        checkMathNarrow("A = A1", "A=A1");
        checkMathNarrow("A != A1", "A≠A1");
        checkMathNarrow("A in {A1}", "A∈{A1}");
        checkMathNarrow("A not in {A1}", "A∉{A1}");
        checkMathNarrow("A = A1 & B != B1", "A=A1∧B≠B1");
        checkMathNarrow("A in {A1} & B not in {B1}", "A∈{A1}∧B∉{B1}");
        checkMathNarrow("!A  & !A", "¬A∧¬A");
        checkMathNarrow("!A & (C|D)", "¬A∧(C∨D)");
        checkMathNarrow("A < B", "A<B");
        checkMathNarrow("A <= B", "A≤B");
        checkMathNarrow("A !< B", "A≮B");
        checkMathNarrow("A !<= B", "A≰B");
        checkMathNarrow("A > B", "A>B");
        checkMathNarrow("A >= B", "A≥B");
        checkMathNarrow("A !> B", "A≯B");
        checkMathNarrow("A !>= B", "A≱B");
    }

    @Test
    void testMathWide() {
        checkMathWide("A in {}", "A ∈ ∅");
        checkMathWide("true", "⊤");
        checkMathWide("false", "⊥");
        checkMathWide("true & false", "⊤ ∧ ⊥");
        checkMathWide("A", "A");
        checkMathWide("! A", "¬A");
        checkMathWide("A and B", "A ∧ B");
        checkMathWide("A and B and C", "A ∧ B ∧ C");
        checkMathWide("A or B", "A ∨ B");
        checkMathWide("A or B or C", "A ∨ B ∨ C");
        checkMathWide("A -> B", "A → B");
        checkMathWide("A -> B -> C", "(A → B) → C");
        checkMathWide("A <-> B", "A ↔ B");
        checkMathWide("A <-> B <-> C", "(A ↔ B) ↔ C");
        checkMathWide("A = A1", "A = A1");
        checkMathWide("A != A1", "A ≠ A1");
        checkMathWide("A in {A1}", "A ∈ {A1}");
        checkMathWide("A not in {A1}", "A ∉ {A1}");
        checkMathWide("A = A1 & B != B1", "A = A1 ∧ B ≠ B1");
        checkMathWide("A in {A1} & B not in {B1}", "A ∈ {A1} ∧ B ∉ {B1}");
        checkMathWide("!A  & !A", "¬A ∧ ¬A");
        checkMathWide("!A & (C|D)", "¬A ∧ (C ∨ D)");
        checkMathWide("A < B", "A < B");
        checkMathWide("A <= B", "A ≤ B");
        checkMathWide("A !< B", "A ≮ B");
        checkMathWide("A !<= B", "A ≰ B");
        checkMathWide("A > B", "A > B");
        checkMathWide("A >= B", "A ≥ B");
        checkMathWide("A !> B", "A ≯ B");
        checkMathWide("A !>= B", "A ≱ B");
    }
}