package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.function.Predicate;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NodePredicates;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.expressions.ast.visitors.PredicateMatcher;

class PredicateMatcherTest {

    private static void check(String expression,
                              Predicate<? super Node> predicate,
                              boolean expected) {
        final Expression e = new Expression(expression);
        final Node n = ConvertToNary.execute(e.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY);

        assertEquals(expected, PredicateMatcher.matches(n, predicate));
    }

    @Test
    void test() {
        check("a or b or c", NodePredicates.IS_LEAF, true);
        check("a or b or c", NodePredicates.IS_UNARY, false);
        check("a or b or c", NodePredicates.IS_BINARY, false);
        check("a or b or c", NodePredicates.IS_NARY, true);
        check("a or b or not c", NodePredicates.IS_UNARY, true);
        check("a or b", NodePredicates.IS_BINARY, true);
        check("not a or not b", NodePredicates.IS_BINARY, true);
        check("not a or not b", NodePredicates.IS_UNARY, true);
        check("a or not b", NodePredicates.IS_UNARY, true);
        check("a or not b", NodePredicates.IS_NARY, false);
    }
}