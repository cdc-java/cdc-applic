package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;

class NameTest {
    @Test
    void testConstructorNullLocal() {
        final Name n = Name.of(SName.of("Foo"));
        assertNull(n.getPrefix());
        assertEquals(SName.of("Foo"), n.getLocal());
        assertEquals("Foo", n.toString());
    }

    @Test
    void testConstructorPrefixLocal() {
        final Name n = Name.of(SName.of("Pref"), SName.of("Foo"));
        assertEquals(SName.of("Pref"), n.getPrefix());
        assertEquals(SName.of("Foo"), n.getLocal());
        assertEquals("Pref.Foo", n.toString());
    }

    @Test
    void testConstructorString1() {
        final Name n = Name.of("A.B");
        assertEquals(SName.of("A"), n.getPrefix());
        assertEquals(SName.of("B"), n.getLocal());
        assertEquals("A.B", n.toString());
    }

    @Test
    void testConstructorString2() {
        final Name n = Name.of("\"A\".B");
        assertEquals(SName.of("A"), n.getPrefix());
        assertEquals(SName.of("B"), n.getLocal());
        assertEquals("A.B", n.toString());
    }

    @Test
    void testConstructorString3() {
        final Name n = Name.of("\" A\".B");
        assertEquals(SName.of(" A"), n.getPrefix());
        assertEquals(SName.of("B"), n.getLocal());
        assertEquals("\" A\".B", n.toString());
    }

    @Test
    void testConstructorString4() {
        final Name n = Name.of("A.\"B\"");
        assertEquals(SName.of("A"), n.getPrefix());
        assertEquals(SName.of("B"), n.getLocal());
        assertEquals("A.B", n.toString());
    }

    @Test
    void testConstructorString5() {
        final Name n = Name.of("A.\"B \"");
        assertEquals(SName.of("A"), n.getPrefix());
        assertEquals(SName.of("B "), n.getLocal());
        assertEquals("A.\"B \"", n.toString());
    }
}