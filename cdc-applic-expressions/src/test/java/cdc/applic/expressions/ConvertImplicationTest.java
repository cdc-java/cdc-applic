package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.visitors.ConvertImplication;

class ConvertImplicationTest {
    private static void check(String expression,
                              String expected) {
        final Expression x = new Expression(expression);
        final Expression x1 = ConvertImplication.execute(x.getRootNode())
                                                .toExpression(Formatting.SHORT_NARROW);
        assertEquals(expected, x1.toString());
    }

    @Test
    void test() {
        check("", "true");
        check("A -> B", "!A|B");
        check("A and B", "A&B");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         check("A <-> B", null);
                     });
    }
}