package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AbstractSetNode;
import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.AbstractValueNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.EquivalenceNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.GreaterNode;
import cdc.applic.expressions.ast.GreaterOrEqualNode;
import cdc.applic.expressions.ast.ImplicationNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.LessNode;
import cdc.applic.expressions.ast.LessOrEqualNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.NeitherGreaterNorEqualNode;
import cdc.applic.expressions.ast.NeitherLessNorEqualNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotEqualNode;
import cdc.applic.expressions.ast.NotGreaterNode;
import cdc.applic.expressions.ast.NotInNode;
import cdc.applic.expressions.ast.NotLessNode;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.ValueUtils;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.parsing.ComparisonOperator;

class NodeTest {
    private static final Logger LOGGER = LogManager.getLogger(NodeTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();
    // Use alphabetical order, ignoring Node
    private final AndNode nAnd = new AndNode(TrueNode.INSTANCE, FalseNode.INSTANCE);
    private final EqualNode nEqual = new EqualNode(Name.of("Prop"), ValueUtils.create("1"));
    private final EquivalenceNode nEquivalence = new EquivalenceNode(TrueNode.INSTANCE, FalseNode.INSTANCE);
    private final FalseNode nFalse = FalseNode.INSTANCE;
    private final GreaterNode nGreater = new GreaterNode(Name.of("Prop"), ValueUtils.create("1"));
    private final GreaterOrEqualNode nGreaterOrEqual = new GreaterOrEqualNode(Name.of("Prop"), ValueUtils.create("1"));
    private final ImplicationNode nImplication = new ImplicationNode(TrueNode.INSTANCE, FalseNode.INSTANCE);
    private final InNode nIn = new InNode(Name.of("Prop"), BooleanSet.TRUE);
    private final LessNode nLess = new LessNode(Name.of("Prop"), ValueUtils.create("1"));
    private final LessOrEqualNode nLessOrEqual = new LessOrEqualNode(Name.of("Prop"), ValueUtils.create("1"));
    private final NaryAndNode nNaryAnd = new NaryAndNode(TrueNode.INSTANCE, nFalse);
    private final NaryOrNode nNaryOr = new NaryOrNode(TrueNode.INSTANCE, nFalse);
    private final NeitherGreaterNorEqualNode nNeitherGreaterNorEqual =
            new NeitherGreaterNorEqualNode(Name.of("Prop"), ValueUtils.create("1"));
    private final NeitherLessNorEqualNode nNeitherLessNorEqual =
            new NeitherLessNorEqualNode(Name.of("Prop"), ValueUtils.create("1"));
    private final NotNode nNot = new NotNode(TrueNode.INSTANCE);
    private final NotEqualNode nNotEqual = new NotEqualNode(Name.of("Prop"), ValueUtils.create("1"));
    private final NotGreaterNode nNotGreater = new NotGreaterNode(Name.of("Prop"), ValueUtils.create("1"));
    private final NotInNode nNotIn = new NotInNode(Name.of("Prop"), BooleanSet.TRUE);
    private final NotLessNode nNotLess = new NotLessNode(Name.of("Prop"), ValueUtils.create("1"));
    private final OrNode nOr = new OrNode(TrueNode.INSTANCE, FalseNode.INSTANCE);
    private final RefNode nRef = new RefNode("Ref");
    private final TrueNode nTrue = TrueNode.INSTANCE;

    private final Node[] nodes = {
            nAnd,
            nEqual,
            nEquivalence,
            nFalse,
            nGreater,
            nGreaterOrEqual,
            nImplication,
            nIn,
            nLess,
            nLessOrEqual,
            nNaryAnd,
            nNaryOr,
            nNeitherGreaterNorEqual,
            nNeitherLessNorEqual,
            nNot,
            nNotEqual,
            nNotGreater,
            nNotIn,
            nNotLess,
            nOr,
            nRef,
            nTrue
    };

    @Test
    void testGetKind() {
        assertEquals("AND", nAnd.getKind());
        assertEquals("EQUAL", nEqual.getKind());
        assertEquals("EQUIVALENCE", nEquivalence.getKind());
        assertEquals("FALSE", nFalse.getKind());
        assertEquals("GREATER", nGreater.getKind());
        assertEquals("GREATER_OR_EQUAL", nGreaterOrEqual.getKind());
        assertEquals("IMPLICATION", nImplication.getKind());
        assertEquals("IN", nIn.getKind());
        assertEquals("LESS", nLess.getKind());
        assertEquals("LESS_OR_EQUAL", nLessOrEqual.getKind());
        assertEquals("NARY_AND", nNaryAnd.getKind());
        assertEquals("NARY_OR", nNaryOr.getKind());
        assertEquals("NEITHER_GREATER_NOR_EQUAL", nNeitherGreaterNorEqual.getKind());
        assertEquals("NEITHER_LESS_NOR_EQUAL", nNeitherLessNorEqual.getKind());
        assertEquals("NOT", nNot.getKind());
        assertEquals("NOT_EQUAL", nNotEqual.getKind());
        assertEquals("NOT_GREATER", nNotGreater.getKind());
        assertEquals("NOT_IN", nNotIn.getKind());
        assertEquals("NOT_LESS", nNotLess.getKind());
        assertEquals("OR", nOr.getKind());
        assertEquals("REF", nRef.getKind());
        assertEquals("TRUE", nTrue.getKind());
    }

    @Test
    void testIsNegative() {
        assertFalse(nEqual.isNegative());
        assertFalse(nGreater.isNegative());
        assertFalse(nGreaterOrEqual.isNegative());
        assertFalse(nIn.isNegative());
        assertFalse(nLess.isNegative());
        assertFalse(nLessOrEqual.isNegative());

        assertTrue(nNeitherGreaterNorEqual.isNegative());
        assertTrue(nNeitherLessNorEqual.isNegative());
        assertTrue(nNotEqual.isNegative());
        assertTrue(nNotGreater.isNegative());
        assertTrue(nNotLess.isNegative());
        assertTrue(nNotIn.isNegative());
    }

    @Test
    void testChildren() {
        for (final Node node : nodes) {
            assertThrows(IllegalArgumentException.class,
                         () -> {
                             node.getChildAt(-1);
                         });
            if (node instanceof AbstractBinaryNode) {
                assertEquals(2, node.getChildrenCount());
                assertNotSame(null, node.getChildAt(0));
                assertNotSame(null, node.getChildAt(1));
                assertThrows(IllegalArgumentException.class,
                             () -> {
                                 node.getChildAt(2);
                             });
                assertTrue(node.getHeight() > 1);
            } else if (node instanceof AbstractNaryNode) {
                assertTrue(node.getChildrenCount() >= 1);
                assertNotSame(null, node.getChildAt(0));
                assertThrows(IllegalArgumentException.class,
                             () -> {
                                 node.getChildAt(node.getChildrenCount());
                             });
                assertTrue(node.getHeight() > 1);
            } else if (node instanceof AbstractUnaryNode) {
                assertEquals(1, node.getChildrenCount());
                assertNotNull(node.getChildAt(0));
                assertThrows(IllegalArgumentException.class,
                             () -> {
                                 node.getChildAt(1);
                             });
                assertTrue(node.getHeight() > 1);
            } else if (node instanceof AbstractLeafNode) {
                assertEquals(0, node.getChildrenCount());
                assertThrows(IllegalArgumentException.class,
                             () -> {
                                 node.getChildAt(0);
                             });
                assertEquals(1, node.getHeight());
            }
        }
    }

    @Test
    void testCreate() {
        for (final Node node : nodes) {
            if (node instanceof AbstractValueNode) {
                final AbstractValueNode n = (AbstractValueNode) node;
                assertEquals(n, n.create(n.getName(), n.getValue()));
            } else if (node instanceof AbstractSetNode) {
                final AbstractSetNode n = (AbstractSetNode) node;
                assertEquals(n, n.create(n.getName(), n.getSet()));
            } else if (node instanceof AbstractUnaryNode) {
                final AbstractUnaryNode n = (AbstractUnaryNode) node;
                assertEquals(n, n.create(n.getAlpha()));
            } else if (node instanceof AbstractBinaryNode) {
                final AbstractBinaryNode n = (AbstractBinaryNode) node;
                assertEquals(n, n.create(n.getAlpha(), n.getBeta()));
            } else if (node instanceof AbstractNaryNode) {
                final AbstractNaryNode n = (AbstractNaryNode) node;
                assertEquals(n, n.create(n.getChildren()));
                assertEquals(n, n.create(n.getChildrenAsList()));
            }
        }

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         new NaryAndNode();
                     });
    }

    @Test
    void testEqual() {
        assertEquals(TrueNode.INSTANCE, TrueNode.INSTANCE);
        assertNotEquals(TrueNode.INSTANCE, FalseNode.INSTANCE);

        assertEquals(FalseNode.INSTANCE, FalseNode.INSTANCE);
        assertNotEquals(FalseNode.INSTANCE, TrueNode.INSTANCE);

        assertEquals(nRef, nRef);
        assertEquals(nRef, new RefNode(Name.of("Ref")));
        assertNotEquals(nRef, null);

        assertEquals(nIn, nIn);
        assertNotEquals(nIn, null);
        assertEquals(nIn, new InNode(Name.of("Prop"), BooleanSet.TRUE));
        assertNotEquals(nIn, new InNode(Name.of("Prop1"), BooleanSet.TRUE));
        assertNotEquals(nIn, new InNode(Name.of("Prop"), BooleanSet.FALSE));

        assertEquals(nNotIn, nNotIn);
        assertNotEquals(nNotIn, null);
        assertEquals(nNotIn, new NotInNode(Name.of("Prop"), BooleanSet.TRUE));
        assertNotEquals(nNotIn, new NotInNode(Name.of("Prop1"), BooleanSet.TRUE));
        assertNotEquals(nNotIn, new NotInNode(Name.of("Prop"), BooleanSet.FALSE));

        assertEquals(nEqual, nEqual);
        assertNotEquals(nEqual, null);
        assertEquals(nEqual, new EqualNode(Name.of("Prop"), ValueUtils.create("1")));
        assertNotEquals(nEqual, new EqualNode(Name.of("Prop"), ValueUtils.create("2")));
        assertNotEquals(nEqual, new EqualNode(Name.of("Prop1"), ValueUtils.create("1")));

        assertEquals(nNotEqual, nNotEqual);
        assertNotEquals(nNotEqual, null);
        assertEquals(nNotEqual, new NotEqualNode(Name.of("Prop"), ValueUtils.create("1")));
        assertNotEquals(nNotEqual, new NotEqualNode(Name.of("Prop"), ValueUtils.create("2")));
        assertNotEquals(nNotEqual, new NotEqualNode(Name.of("Prop1"), ValueUtils.create("1")));

        assertEquals(nLess, nLess);
        assertNotEquals(nLess, null);
        assertEquals(nLess, new LessNode(Name.of("Prop"), ValueUtils.create("1")));
        assertNotEquals(nLess, new LessNode(Name.of("Prop"), ValueUtils.create("2")));
        assertNotEquals(nLess, new LessNode(Name.of("Prop1"), ValueUtils.create("1")));

        assertEquals(nLessOrEqual, nLessOrEqual);
        assertNotEquals(nLessOrEqual, null);
        assertEquals(nLessOrEqual, new LessOrEqualNode(Name.of("Prop"), ValueUtils.create("1")));
        assertNotEquals(nLessOrEqual, new LessOrEqualNode(Name.of("Prop"), ValueUtils.create("2")));
        assertNotEquals(nLessOrEqual, new LessOrEqualNode(Name.of("Prop1"), ValueUtils.create("1")));

        assertEquals(nGreater, nGreater);
        assertNotEquals(nGreater, null);
        assertEquals(nGreater, new GreaterNode(Name.of("Prop"), ValueUtils.create("1")));
        assertNotEquals(nGreater, new GreaterNode(Name.of("Prop"), ValueUtils.create("2")));
        assertNotEquals(nGreater, new GreaterNode(Name.of("Prop1"), ValueUtils.create("1")));

        assertEquals(nGreaterOrEqual, nGreaterOrEqual);
        assertNotEquals(nGreaterOrEqual, null);
        assertEquals(nGreaterOrEqual, new GreaterOrEqualNode(Name.of("Prop"), ValueUtils.create("1")));
        assertNotEquals(nGreaterOrEqual, new GreaterOrEqualNode(Name.of("Prop"), ValueUtils.create("2")));
        assertNotEquals(nGreaterOrEqual, new GreaterOrEqualNode(Name.of("Prop1"), ValueUtils.create("1")));

        assertEquals(nNotLess, nNotLess);
        assertNotEquals(nNotLess, null);
        assertEquals(nNotLess, new NotLessNode(Name.of("Prop"), ValueUtils.create("1")));
        assertNotEquals(nNotLess, new NotLessNode(Name.of("Prop"), ValueUtils.create("2")));
        assertNotEquals(nNotLess, new NotLessNode(Name.of("Prop1"), ValueUtils.create("1")));

        assertEquals(nNotGreater, nNotGreater);
        assertNotEquals(nNotGreater, null);
        assertEquals(nNotGreater, new NotGreaterNode(Name.of("Prop"), ValueUtils.create("1")));
        assertNotEquals(nNotGreater, new NotGreaterNode(Name.of("Prop"), ValueUtils.create("2")));
        assertNotEquals(nNotGreater, new NotGreaterNode(Name.of("Prop1"), ValueUtils.create("1")));

        assertEquals(nNeitherLessNorEqual, nNeitherLessNorEqual);
        assertNotEquals(nNeitherLessNorEqual, null);
        assertEquals(nNeitherLessNorEqual, new NeitherLessNorEqualNode(Name.of("Prop"), ValueUtils.create("1")));
        assertNotEquals(nNeitherLessNorEqual, new NeitherLessNorEqualNode(Name.of("Prop"), ValueUtils.create("2")));
        assertNotEquals(nNeitherLessNorEqual, new NeitherLessNorEqualNode(Name.of("Prop1"), ValueUtils.create("1")));

        assertEquals(nNeitherGreaterNorEqual, nNeitherGreaterNorEqual);
        assertNotEquals(nNeitherGreaterNorEqual, null);
        assertEquals(nNeitherGreaterNorEqual, new NeitherGreaterNorEqualNode(Name.of("Prop"), ValueUtils.create("1")));
        assertNotEquals(nNeitherGreaterNorEqual, new NeitherGreaterNorEqualNode(Name.of("Prop"), ValueUtils.create("2")));
        assertNotEquals(nNeitherGreaterNorEqual, new NeitherGreaterNorEqualNode(Name.of("Prop1"), ValueUtils.create("1")));

        assertEquals(nNot, nNot);
        assertNotEquals(nNot, null);
        assertEquals(nNot, new NotNode(TrueNode.INSTANCE));
        assertNotEquals(nNot, new NotNode(FalseNode.INSTANCE));

        assertEquals(nNaryAnd, nNaryAnd);
        assertNotEquals(nNaryAnd, nNaryOr);
        assertEquals(nNaryAnd, new NaryAndNode(nNaryAnd.getChildren()));
        assertNotEquals(nNaryAnd, null);
        assertNotEquals(nNaryAnd, new NaryAndNode(TrueNode.INSTANCE));
        assertNotEquals(new NaryAndNode(FalseNode.INSTANCE), new NaryAndNode(TrueNode.INSTANCE));

        assertEquals(nAnd, nAnd);
        assertNotEquals(nAnd, nOr);
        assertEquals(nAnd, new AndNode(nAnd.getAlpha(), nAnd.getBeta()));
        assertNotEquals(nAnd, null);
        assertNotEquals(nAnd, new AndNode(TrueNode.INSTANCE, TrueNode.INSTANCE));
        assertNotEquals(new AndNode(FalseNode.INSTANCE, FalseNode.INSTANCE), new AndNode(TrueNode.INSTANCE, TrueNode.INSTANCE));
    }

    @Test
    void testHashCode() {
        for (final Node node : nodes) {
            assertEquals(node.hashCode(), node.hashCode());
        }
    }

    @Test
    void testNot() {
        assertEquals(TrueNode.INSTANCE, NotNode.simplestNot(new NotNode(TrueNode.INSTANCE)));
        assertEquals(NotNode.simplestNot(TrueNode.INSTANCE), NotNode.simplestNot(TrueNode.INSTANCE));
    }

    @Test
    void testNaryOr() {
        final Node n = TrueNode.INSTANCE;
        assertEquals(n, NaryOrNode.createSimplestOr(n));
        assertEquals(new OrNode(n, n), NaryOrNode.createSimplestOr(n, n));
        assertEquals(new NaryOrNode(n, n, n), NaryOrNode.createSimplestOr(n, n, n));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         NaryOrNode.createSimplestOr();
                     });

        assertEquals(n, NaryOrNode.createSimplestOr(Arrays.asList(n)));
        assertEquals(new OrNode(n, n), NaryOrNode.createSimplestOr(Arrays.asList(n, n)));
        assertEquals(new NaryOrNode(n, n, n), NaryOrNode.createSimplestOr(Arrays.asList(n, n, n)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         NaryOrNode.createSimplestOr(Collections.emptyList());
                     });

        assertEquals(new NaryOrNode(n, n, n, n), NaryOrNode.mergeSimplestOr(n, new OrNode(n, n), new NaryOrNode(n)));
    }

    @Test
    void testNaryAnd() {
        final Node n = TrueNode.INSTANCE;
        assertEquals(n, NaryAndNode.createSimplestAnd(n));
        assertEquals(new AndNode(n, n), NaryAndNode.createSimplestAnd(n, n));
        assertEquals(new NaryAndNode(n, n, n), NaryAndNode.createSimplestAnd(n, n, n));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         NaryAndNode.createSimplestAnd();
                     });

        assertEquals(n, NaryAndNode.createSimplestAnd(Arrays.asList(n)));
        assertEquals(new AndNode(n, n), NaryAndNode.createSimplestAnd(Arrays.asList(n, n)));
        assertEquals(new NaryAndNode(n, n, n), NaryAndNode.createSimplestAnd(Arrays.asList(n, n, n)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         NaryAndNode.createSimplestAnd(Collections.emptyList());
                     });

        assertEquals(new NaryAndNode(n, n, n, n), NaryAndNode.mergeSimplestAnd(n, new AndNode(n, n), new NaryAndNode(n)));
    }

    @Test
    void testSet() {
        assertEquals(nIn.getSet().getChecked(), nIn.getCheckedSet());
    }

    @Test
    void testPrint() {
        // For coverage
        final Expression x = new Expression("A & B & C | !(!A & !B)");
        x.getRootNode().print(OUT);
        assertTrue(true);
    }

    @Test
    void testMisc() {
        final Expression x = new Expression("A & B & C | !(!A & !B)");
        final Node node = x.getRootNode();

        assertEquals("A&B&C|!(!A&!B)", node.compress());
        assertEquals("A&B&C|!(!A&!B)", node.toExpression().getContent());
    }

    @Test
    void testGetComparisonOperator() {
        assertEquals(ComparisonOperator.EQUAL, nEqual.getComparisonOperator());
        assertEquals(ComparisonOperator.NOT_EQUAL, nNotEqual.getComparisonOperator());
        assertEquals(ComparisonOperator.LESS, nLess.getComparisonOperator());
        assertEquals(ComparisonOperator.NOT_LESS, nNotLess.getComparisonOperator());
        assertEquals(ComparisonOperator.GREATER, nGreater.getComparisonOperator());
        assertEquals(ComparisonOperator.NOT_GREATER, nNotGreater.getComparisonOperator());
        assertEquals(ComparisonOperator.LESS_OR_EQUAL, nLessOrEqual.getComparisonOperator());
        assertEquals(ComparisonOperator.NEITHER_LESS_NOR_EQUAL, nNeitherLessNorEqual.getComparisonOperator());
        assertEquals(ComparisonOperator.GREATER_OR_EQUAL, nGreaterOrEqual.getComparisonOperator());
        assertEquals(ComparisonOperator.NEITHER_GREATER_NOR_EQUAL, nNeitherGreaterNorEqual.getComparisonOperator());
    }

    private static void checkNegate(AbstractValueNode node) {
        final AbstractValueNode n = node.negate();
        assertEquals(n.getComparisonOperator(), node.getComparisonOperator().negate());
        assertEquals(node.getName(), n.getName());
        assertEquals(node.getValue(), n.getValue());
    }

    private static void checkNegate(AbstractSetNode node) {
        final AbstractSetNode n = node.negate();
        assertEquals(node.getName(), n.getName());
        assertEquals(node.getSet(), n.getSet());
    }

    @Test
    void testNegate() {
        for (final Node node : nodes) {
            if (node instanceof AbstractValueNode) {
                checkNegate((AbstractValueNode) node);
            } else if (node instanceof AbstractSetNode) {
                checkNegate((AbstractSetNode) node);
            }
        }
    }
}