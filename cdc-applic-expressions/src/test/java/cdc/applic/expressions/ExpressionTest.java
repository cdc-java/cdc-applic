package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.Node;

class ExpressionTest {
    @Test
    void test() {
        assertTrue(Expression.TRUE.isTrue());
        assertFalse(Expression.TRUE.isFalse());
        assertFalse(Expression.FALSE.isTrue());
        assertTrue(Expression.FALSE.isFalse());
        assertTrue(Expression.fromString("true").isTrue());
        assertTrue(Expression.fromString("TRUE").isTrue());
        assertTrue(Expression.fromString("false").isFalse());
        assertTrue(Expression.fromString("FALSE").isFalse());

        assertEquals(Expression.TRUE, Expression.TRUE);
        assertNotEquals(Expression.TRUE, null);
        assertNotEquals(Expression.TRUE, "true");
        assertEquals(Expression.TRUE, Expression.fromString("true"));
        assertNotEquals(Expression.TRUE, Expression.fromString("TRUE"));

        assertEquals("A", Expression.asString(Expression.fromString("A")));

        assertTrue(Expression.isValid("A"));
        assertFalse(Expression.isValid("and"));

        assertEquals("true", Expression.TRUE.getContent());
        assertEquals(Expression.TRUE.toString(), Expression.TRUE.getContent());
        assertEquals(null, Expression.fromString(null));
        assertEquals(Expression.TRUE, Expression.fromString(""));
        assertEquals(Expression.TRUE, Expression.fromString("  "));

        assertTrue(Expression.TRUE.isValid());

        final Expression invalid = new Expression("and", false);
        assertFalse(invalid.isValid());
        final Expression valid = new Expression("A", false);
        assertTrue(valid.isValid());

        assertNotEquals(Expression.FALSE.hashCode(), Expression.TRUE.hashCode());
    }

    private static void checkToInfix(String expected,
                                     String expression,
                                     Formatting formatting) {
        final Expression x = new Expression(expression);
        final String actual = x.toInfix(formatting);
        assertEquals(expected, actual);
    }

    private static void checkToInfixShortNarrow(String expected,
                                                String expression) {
        checkToInfix(expected, expression, Formatting.SHORT_NARROW);
    }

    private static void checkToInfixShortWide(String expected,
                                              String expression) {
        checkToInfix(expected, expression, Formatting.SHORT_WIDE);
    }

    @Test
    void testToInfixShortNarrow() {
        checkToInfixShortNarrow("true", "true");
        checkToInfixShortNarrow("false", "false");
        checkToInfixShortNarrow("!A", "not A");
        checkToInfixShortNarrow("!!A", "not not A");
        checkToInfixShortNarrow("A&B", "A and B");
        checkToInfixShortNarrow("A&B&C", "A and B and C");
        checkToInfixShortNarrow("A|B", "A or B");
        checkToInfixShortNarrow("A|B|C", "A or B or C");
        checkToInfixShortNarrow("!(!A&!B)", "not (not A and not B)");
        checkToInfixShortNarrow("A<B", "A < B");
        checkToInfixShortNarrow("A!<B", "A !< B");
        checkToInfixShortNarrow("A<=B", "A <= B");
        checkToInfixShortNarrow("A!<=B", "A !<= B");
        checkToInfixShortNarrow("A>B", "A > B");
        checkToInfixShortNarrow("A!>B", "A !> B");
        checkToInfixShortNarrow("A>=B", "A >= B");
        checkToInfixShortNarrow("A!>=B", "A !>= B");
    }

    @Test
    void testToInfixShortWide() {
        checkToInfixShortWide("true", "true");
        checkToInfixShortWide("false", "false");
        checkToInfixShortWide("!A", "not A");
        checkToInfixShortWide("!!A", "not not A");
        checkToInfixShortWide("A & B", "A and B");
        checkToInfixShortWide("A & B & C", "A and B and C");
        checkToInfixShortWide("A | B", "A or B");
        checkToInfixShortWide("A | B | C", "A or B or C");
        checkToInfixShortWide("!(!A & !B)", "not (not A and not B)");
        checkToInfixShortWide("A < B", "A < B");
        checkToInfixShortWide("A !< B", "A !< B");
        checkToInfixShortWide("A <= B", "A <= B");
        checkToInfixShortWide("A !<= B", "A !<= B");
        checkToInfixShortWide("A > B", "A > B");
        checkToInfixShortWide("A !> B", "A !> B");
        checkToInfixShortWide("A >= B", "A >= B");
        checkToInfixShortWide("A !>= B", "A !>= B");
    }

    @Test
    void testCompare() {
        assertTrue(new Expression("Hello").compareTo(new Expression("World")) < 0);
    }

    @Test
    void testMisc() {
        final Expression x = new Expression("Hello");
        assertEquals(Expression.toNode(x), x.getRootNode());

        final Node[] narray = { x.getRootNode() };
        final Expression[] earray = { x };
        final List<Node> nlist = Arrays.asList(narray);
        final List<Expression> elist = Arrays.asList(earray);

        assertArrayEquals(narray, Expression.toNodeArray(x));
        assertEquals(nlist, Expression.toNodeList(x));
        assertArrayEquals(narray, Expression.toNodeArray(elist));
        assertEquals(nlist, Expression.toNodeList(elist));
        assertEquals("X&Y", new Expression("X and Y").compress().getContent());
    }
}