package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.RealValue;

class IntegerRangeTest {
    private static IntegerValue v(int value) {
        return IntegerValue.of(value);
    }

    private static IntegerRange r(int value) {
        return IntegerRange.of(value);
    }

    private static IntegerRange r(int min,
                                  int max) {
        return IntegerRange.of(min, max);
    }

    @Test
    void test() {
        assertEquals(r(1).getCheckedSetClass(), IntegerSet.class);

        assertTrue(r(1).isSingleton());
        assertFalse(r(1, 2).isSingleton());
        assertFalse(r(1).isEmpty());
        assertTrue(r(1, 0).isEmpty());
        assertTrue(r(1).contains(1));
        assertFalse(r(1).contains(2));
        assertFalse(r(1).containsValue(v(2)));
        assertFalse(r(1).containsValue(v(2)));
        assertFalse(r(1).contains(0));
        assertFalse(r(1, 0).contains(0));
        assertFalse(r(1, 0).contains(1));
        assertEquals(v(1), r(1, 2).getMin());
        assertEquals(v(2), r(1, 2).getMax());
        assertEquals(v(1), r(1, 0).getMin());
        assertEquals(v(0), r(1, 0).getMax());

        assertTrue(r(1, 2).containsRange(r(10, 9)));
        assertTrue(r(1, 2).containsRange(r(1)));
        assertTrue(r(1, 2).containsRange(r(2)));
        assertTrue(r(1, 2).containsRange(r(1, 2)));
        assertFalse(r(1, 2).containsRange(r(3)));
        assertFalse(r(1, 2).containsRange(r(0)));
        assertFalse(r(1, 2).containsRange(r(0, 1)));
        assertFalse(r(1, 2).containsRange(r(0, 2)));
        assertFalse(r(1, 2).containsRange(r(0, 3)));
        assertFalse(r(1, 2).containsRange(r(1, 3)));
        assertFalse(r(1, 2).containsRange(r(2, 3)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r(1).contains(RealValue.of(0.0));
                     });
    }

    @Test
    void testCompare() {
        assertSame(0, r(1).compareTo(r(1)));
        assertSame(0, r(1, 2).compareTo(r(1)));
        assertSame(0, r(1, 2).compareTo(r(2, 3)));

        assertTrue(r(1, 2).compareTo(r(3)) < 0);
        assertTrue(r(3).compareTo(r(1, 2)) > 0);

        assertTrue(r(1, 2).compareTo(r(0)) > 0);
        assertTrue(r(0).compareTo(r(1, 2)) < 0);

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r(0).compareTo(r(1, 0));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         r(1, 0).compareTo(r(0));
                     });
    }

    @Test
    void testEquals() {
        final IntegerRange r = r(1);
        assertEquals(r, r);
        assertEquals(r(1, 0), r(2, 1));
        assertEquals(r(1, 2), r(1, 2));
        assertNotEquals(r(1, 2), r(1, 3));
        assertNotEquals(r(1, 2), r(0, 2));
        assertNotEquals(r(1, 2), r(2, 1));
        assertNotEquals(r(2, 1), r(1, 2));

        assertNotEquals(r(1, 2), null);
        assertNotEquals(r(1, 2), "hello");
    }

    @Test
    void testHashCode() {
        assertEquals(r(0).hashCode(), r(0).hashCode());
        assertEquals(r(1, 0).hashCode(), r(2, 1).hashCode());
    }

    @Test
    void testToString() {
        assertEquals("0~1", r(0, 1).toString());
        assertEquals("0", r(0).toString());
        assertEquals("0~-1", r(0, -1).toString());
    }

    @Test
    void testComparators() {
        assertTrue(IntegerRange.MIN_COMPARATOR.compare(r(1), r(2)) < 0);
        assertSame(0, IntegerRange.MIN_COMPARATOR.compare(r(1), r(1)));
        assertTrue(IntegerRange.MIN_COMPARATOR.compare(r(1), r(0)) > 0);

        assertTrue(IntegerRange.MAX_COMPARATOR.compare(r(1), r(2)) < 0);
        assertSame(0, IntegerRange.MAX_COMPARATOR.compare(r(1), r(1)));
        assertTrue(IntegerRange.MAX_COMPARATOR.compare(r(1), r(0)) > 0);
    }

    @Test
    void testMisc() {
        final IntegerRange r = r(1);
        assertEquals(v(1), r.getSingletonValue());
    }
}