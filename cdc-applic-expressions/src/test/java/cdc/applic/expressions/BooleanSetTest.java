package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.AbstractSItemSet;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.UncheckedSet;

class BooleanSetTest {
    @Test
    void checkEmpty() {
        assertTrue(BooleanSet.EMPTY.isEmpty());
        assertEquals(BooleanSet.EMPTY, IntegerSet.EMPTY);
        assertEquals(BooleanSet.EMPTY, UncheckedSet.EMPTY);
        assertEquals(BooleanSet.EMPTY, RealSet.EMPTY);
        assertEquals(BooleanSet.EMPTY, BooleanSet.EMPTY);
        assertEquals(BooleanSet.EMPTY, StringSet.EMPTY);
    }

    private static void checkConstruction(String expectedContent,
                                          String s) {
        final BooleanSet set = BooleanSet.of(s);
        assertEquals(expectedContent, set.getContent());
        assertTrue(set.isValid());
        assertTrue(set.isChecked());
        assertEquals(set, set.getChecked());

        assertEquals(true, set.isCompliantWith(BooleanSet.class));
        assertEquals(set.isEmpty(), set.isCompliantWith(IntegerSet.class));
        assertEquals(set.isEmpty(), set.isCompliantWith(RealSet.class));
        assertEquals(set.isEmpty(), set.isCompliantWith(StringSet.class));
        assertTrue(set.isCompliantWith(UncheckedSet.class));
    }

    @Test
    void testConstruction() {
        checkConstruction("", "");
        checkConstruction("true", "true");
        checkConstruction("true", " true ");
        checkConstruction("false", " FALSE  ");
        checkConstruction("false,true", " true, false, TRUE, FALSE  ");

        assertTrue(BooleanSet.TRUE.contains(BooleanValue.TRUE));
        assertTrue(BooleanSet.TRUE.contains((SItem) BooleanValue.TRUE));
        assertFalse(BooleanSet.TRUE.contains(IntegerValue.of(0)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.of((BooleanValue) null);
                     });
    }

    @Test
    void testConvert() {
        assertEquals(BooleanSet.EMPTY, BooleanSet.convert(IntegerSet.EMPTY));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.convert(null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.convert(IntegerSet.of("0"));
                     });
    }

    private static void checkUnion(BooleanSet set1,
                                   AbstractSItemSet set2,
                                   String expectedContent) {
        final BooleanSet set = set1.union(set2);
        assertEquals(expectedContent, set.getContent());

        BooleanSet s = set1;
        for (final SItem item : set2.getItems()) {
            s = s.union(item);
        }
        assertEquals(expectedContent, s.getContent());
    }

    private static void checkUnion(String s1,
                                   String s2,
                                   String expectedContent) {
        checkUnion(BooleanSet.of(s1), BooleanSet.of(s2), expectedContent);
        checkUnion(BooleanSet.of(s1), UncheckedSet.of(s2), expectedContent);
    }

    @Test
    void testUnion() {
        checkUnion("", "", "");
        checkUnion("false", "", "false");
        checkUnion("true", "", "true");
        checkUnion("false,true", "", "false,true");

        checkUnion("", "false", "false");
        checkUnion("false", "false", "false");
        checkUnion("true", "false", "false,true");
        checkUnion("false,true", "false", "false,true");

        checkUnion("", "true", "true");
        checkUnion("false", "true", "false,true");
        checkUnion("true", "true", "true");
        checkUnion("false,true", "true", "false,true");

        checkUnion("", "false,true", "false,true");
        checkUnion("false", "false,true", "false,true");
        checkUnion("true", "false,true", "false,true");
        checkUnion("false,true", "false,true", "false,true");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.union((SItem) null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.union(IntegerValue.of(0));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.union((SItemSet) null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.union(IntegerSet.of("0"));
                     });
    }

    private static void checkIntersection(BooleanSet set1,
                                          AbstractSItemSet set2,
                                          String expectedContent) {
        final BooleanSet set = set1.intersection(set2);
        assertEquals(expectedContent, set.getContent());
    }

    private static void checkIntersection(String s1,
                                          String s2,
                                          String expectedContent) {
        checkIntersection(BooleanSet.of(s1), BooleanSet.of(s2), expectedContent);
        checkIntersection(BooleanSet.of(s1), UncheckedSet.of(s2), expectedContent);
    }

    @Test
    void testIntersection() {
        checkIntersection("", "", "");
        checkIntersection("false", "", "");
        checkIntersection("true", "", "");
        checkIntersection("false,true", "", "");

        checkIntersection("", "false", "");
        checkIntersection("false", "false", "false");
        checkIntersection("true", "false", "");
        checkIntersection("false,true", "false", "false");

        checkIntersection("", "true", "");
        checkIntersection("false", "true", "");
        checkIntersection("true", "true", "true");
        checkIntersection("false,true", "true", "true");

        checkIntersection("", "false,true", "");
        checkIntersection("false", "false,true", "false");
        checkIntersection("true", "false,true", "true");
        checkIntersection("false,true", "false,true", "false,true");

        assertEquals(BooleanSet.EMPTY, BooleanSet.EMPTY.intersection(BooleanValue.TRUE));
        assertEquals(BooleanSet.EMPTY, BooleanSet.FALSE.intersection(BooleanValue.TRUE));
        assertEquals(BooleanSet.TRUE, BooleanSet.TRUE.intersection(BooleanValue.TRUE));
        assertEquals(BooleanSet.TRUE, BooleanSet.FALSE_TRUE.intersection(BooleanValue.TRUE));

        assertEquals(BooleanSet.EMPTY, BooleanSet.EMPTY.intersection(BooleanValue.FALSE));
        assertEquals(BooleanSet.FALSE, BooleanSet.FALSE.intersection(BooleanValue.FALSE));
        assertEquals(BooleanSet.EMPTY, BooleanSet.TRUE.intersection(BooleanValue.FALSE));
        assertEquals(BooleanSet.FALSE, BooleanSet.FALSE_TRUE.intersection(BooleanValue.FALSE));

        assertEquals(BooleanSet.TRUE, BooleanSet.FALSE_TRUE.intersection(BooleanSet.TRUE));
        assertEquals(BooleanSet.EMPTY, BooleanSet.FALSE.intersection((SItem) BooleanValue.TRUE));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.intersection((SItem) null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.intersection((SItemSet) null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.intersection(IntegerValue.of(0));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.intersection(IntegerSet.of("0"));
                     });
    }

    private static void checkRemove(BooleanSet set1,
                                    AbstractSItemSet set2,
                                    String expectedContent) {
        final BooleanSet set = set1.remove(set2);
        assertEquals(expectedContent, set.getContent());

        BooleanSet s = set1;
        for (final SItem item : set2.getItems()) {
            s = s.remove(item);
        }
        assertEquals(expectedContent, s.getContent());
    }

    private static void checkRemove(String s1,
                                    String s2,
                                    String expectedContent) {
        checkRemove(BooleanSet.of(s1), BooleanSet.of(s2), expectedContent);
        checkRemove(BooleanSet.of(s1), UncheckedSet.of(s2), expectedContent);
    }

    @Test
    void testRemove() {
        checkRemove("", "", "");
        checkRemove("TRUE", "", "true");
        checkRemove("", "TRUE", "");
        checkRemove("", "FALSE", "");
        checkRemove("TRUE,FALSE", "", "false,true");
        checkRemove("TRUE", "TRUE", "");
        checkRemove("TRUE,FALSE", "TRUE", "false");
        checkRemove("TRUE,FALSE", "TRUE", "false");
        checkRemove("TRUE", "FALSE", "true");
        checkRemove("TRUE,FALSE", "FALSE", "true");
        checkRemove("TRUE", "", "true");
        checkRemove("TRUE,FALSE", "", "false,true");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.remove((SItem) null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.remove(IntegerValue.of(0));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.remove((SItemSet) null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         BooleanSet.EMPTY.remove(IntegerSet.of("0"));
                     });
    }

    @Test
    void testContains() {
        assertTrue(BooleanSet.EMPTY.contains(BooleanSet.EMPTY));
        assertFalse(BooleanSet.EMPTY.contains(BooleanSet.FALSE));
        assertFalse(BooleanSet.EMPTY.contains(BooleanSet.TRUE));
        assertFalse(BooleanSet.EMPTY.contains(BooleanSet.FALSE_TRUE));

        assertTrue(BooleanSet.FALSE.contains(BooleanSet.EMPTY));
        assertTrue(BooleanSet.FALSE.contains(BooleanSet.FALSE));
        assertFalse(BooleanSet.FALSE.contains(BooleanSet.TRUE));
        assertFalse(BooleanSet.FALSE.contains(BooleanSet.FALSE_TRUE));

        assertTrue(BooleanSet.TRUE.contains(BooleanSet.EMPTY));
        assertFalse(BooleanSet.TRUE.contains(BooleanSet.FALSE));
        assertTrue(BooleanSet.TRUE.contains(BooleanSet.TRUE));
        assertFalse(BooleanSet.TRUE.contains(BooleanSet.FALSE_TRUE));

        assertTrue(BooleanSet.FALSE_TRUE.contains(BooleanSet.EMPTY));
        assertTrue(BooleanSet.FALSE_TRUE.contains(BooleanSet.FALSE));
        assertTrue(BooleanSet.FALSE_TRUE.contains(BooleanSet.TRUE));
        assertTrue(BooleanSet.FALSE_TRUE.contains(BooleanSet.FALSE_TRUE));

    }

    @Test
    void testEquals() {
        assertEquals(BooleanSet.FALSE, BooleanSet.FALSE);
        assertEquals(BooleanSet.EMPTY, IntegerSet.EMPTY);
        assertEquals(BooleanSet.EMPTY, RealSet.EMPTY);
        assertEquals(BooleanSet.EMPTY, StringSet.EMPTY);
        assertEquals(BooleanSet.EMPTY, UncheckedSet.EMPTY);
        assertNotEquals(BooleanSet.EMPTY, IntegerSet.of(IntegerValue.of(1)));
        assertNotEquals(BooleanSet.EMPTY, null);
        assertNotEquals(BooleanSet.EMPTY, "true");
        assertNotEquals(BooleanSet.FALSE, BooleanSet.TRUE);
        assertNotEquals(BooleanSet.FALSE, null);
        assertNotEquals(BooleanSet.FALSE, "false");

        assertEquals(BooleanSet.EMPTY, BooleanSet.of());
        assertEquals(BooleanSet.FALSE, BooleanSet.of(BooleanValue.FALSE));
        assertEquals(BooleanSet.TRUE, BooleanSet.of(BooleanValue.TRUE));
        assertEquals(BooleanSet.FALSE_TRUE, BooleanSet.of(BooleanValue.FALSE, BooleanValue.TRUE));
        assertEquals(BooleanSet.FALSE_TRUE, BooleanSet.of(BooleanValue.TRUE, BooleanValue.FALSE));
    }

    @Test
    void testIsFull() {
        assertTrue(BooleanSet.FALSE_TRUE.isFull());
        assertFalse(BooleanSet.FALSE.isFull());
        assertFalse(BooleanSet.TRUE.isFull());
        assertFalse(BooleanSet.EMPTY.isFull());
    }

    @Test
    void testToString() {
        assertEquals("{}", BooleanSet.EMPTY.toString());
        assertEquals("{true}", BooleanSet.TRUE.toString());
        assertEquals("{false}", BooleanSet.FALSE.toString());
        assertEquals("{false,true}", BooleanSet.FALSE_TRUE.toString());
    }

    @Test
    void testHashCode() {
        assertNotEquals(BooleanSet.TRUE.hashCode(), BooleanSet.FALSE.hashCode());
        assertNotEquals(BooleanSet.EMPTY.hashCode(), BooleanSet.FALSE.hashCode());
    }

    @Test
    void testLexicographicComparator() {
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.EMPTY, null) > 0);
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.FALSE, null) > 0);
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.TRUE, null) > 0);
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.FALSE_TRUE, null) > 0);

        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(null, BooleanSet.EMPTY) < 0);
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(null, BooleanSet.FALSE) < 0);
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(null, BooleanSet.TRUE) < 0);
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(null, BooleanSet.FALSE_TRUE) < 0);

        assertSame(0, BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.EMPTY, BooleanSet.EMPTY));
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.EMPTY, BooleanSet.FALSE) < 0);
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.EMPTY, BooleanSet.TRUE) < 0);
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.EMPTY, BooleanSet.FALSE_TRUE) < 0);

        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.FALSE, BooleanSet.EMPTY) > 0);
        assertSame(0, BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.FALSE, BooleanSet.FALSE));
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.FALSE, BooleanSet.TRUE) < 0);
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.FALSE, BooleanSet.FALSE_TRUE) < 0);

        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.TRUE, BooleanSet.EMPTY) > 0);
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.TRUE, BooleanSet.FALSE) > 0);
        assertSame(0, BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.TRUE, BooleanSet.TRUE));
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.TRUE, BooleanSet.FALSE_TRUE) > 0);

        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.FALSE_TRUE, BooleanSet.EMPTY) > 0);
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.FALSE_TRUE, BooleanSet.FALSE) > 0);
        assertTrue(BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.FALSE_TRUE, BooleanSet.TRUE) < 0);
        assertSame(0, BooleanSet.LEXICOGRAPHIC_COMPARATOR.compare(BooleanSet.FALSE_TRUE, BooleanSet.FALSE_TRUE));
    }

    @Test
    void testMisc() {
        assertEquals(BooleanValue.FALSE, BooleanSet.FALSE.getSingletonValue());
    }
}