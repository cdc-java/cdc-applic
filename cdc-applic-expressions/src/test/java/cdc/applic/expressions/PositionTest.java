package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.Position;

class PositionTest {
    @Test
    void testIsAdjoint() {
        assertTrue(Position.ADJOINT_PRED.isAdjoint());
        assertTrue(Position.ADJOINT_SUCC.isAdjoint());
        assertFalse(Position.DISJOINT_PRED.isAdjoint());
        assertFalse(Position.DISJOINT_SUCC.isAdjoint());
        assertFalse(Position.OVERLAPPING.isAdjoint());
    }

    @Test
    void testIsDisjoint() {
        assertFalse(Position.ADJOINT_PRED.isDisjoint());
        assertFalse(Position.ADJOINT_SUCC.isDisjoint());
        assertTrue(Position.DISJOINT_PRED.isDisjoint());
        assertTrue(Position.DISJOINT_SUCC.isDisjoint());
        assertFalse(Position.OVERLAPPING.isDisjoint());
    }
}