package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.expressions.ast.visitors.MergeSets;

class MergeSetsTest {
    private static void check(String expression,
                              String expected) {
        final Expression x = new Expression(expression);
        final Expression x1 = MergeSets.execute(ConvertToNary.execute(x.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY))
                                       .toExpression(Formatting.SHORT_NARROW);
        assertEquals(expected, x1.toString());
    }

    @Test
    void test() {
        check("A", "A");
        check("A and B", "A&B");
        check("A and B and C", "A&B&C");

        check("E in {E1} or F in {F1}", "E<:{E1}|F<:{F1}");

        check("E in {E1} or E in {E2}", "E<:{E1,E2}");
        check("E in {E1} or E not in {E2}", "E!<:{E2}");
        check("E not in {E1} or E in {E2}", "E!<:{E1}");
        check("E not in {E1} or E not in {E2}", "E!<:{}");
        check("E in {E1} and E in {E2}", "E<:{}");
        check("E in {E1} and E not in {E2}", "E<:{E1}");
        check("E not in {E1} and E in {E2}", "E<:{E2}");
        check("E not in {E1} and E not in {E2}", "E!<:{E1,E2}");

        check("E in {E1,E2} or E in {E2,E3}", "E<:{E1,E2,E3}");
        check("E in {E1,E2} or E not in {E2,E3}", "E!<:{E3}");
        check("E not in {E1,E2} or E in {E2,E3}", "E!<:{E1}");
        check("E not in {E1,E2} or E not in {E2,E3}", "E!<:{E2}");
        check("E in {E1,E2} and E in {E2,E3}", "E<:{E2}");
        check("E in {E1,E2} and E not in {E2,E3}", "E<:{E1}");
        check("E not in {E1,E2} and E in {E2,E3}", "E<:{E3}");
        check("E not in {E1,E2} and E not in {E2,E3}", "E!<:{E1,E2,E3}");

        check("E in {E1} or E in {E2} or E in {E3}", "E<:{E1,E2,E3}");
        check("E in {E1} or E in {E2} or F in {F1} or E in {E3}", "F<:{F1}|E<:{E1,E2,E3}");
        check("E in {E1} or F in {F1} or G in {G1} or H in {H1}", "E<:{E1}|F<:{F1}|G<:{G1}|H<:{H1}");

    }
}