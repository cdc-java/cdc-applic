package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.DeleteNots;

class DeleteNotsTest {
    private static void test(String expected,
                             String expression) {
        final Expression x = new Expression(expression);
        final Node node = DeleteNots.execute(x.getRootNode());
        assertEquals(expected, node.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void test() {
        test("true", "true");
        test("A&B", "A and B");
        test("A&B", "not A and B");
        test("A&B", "not A and not B");
        test("A&B", "not not A and not B");
        test("A&B", "not (not A and not B)");
    }
}