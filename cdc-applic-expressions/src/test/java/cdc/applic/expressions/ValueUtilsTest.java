package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.ValueUtils;

class ValueUtilsTest {
    @Test
    void test() {
        assertTrue(ValueUtils.create("1").isCompliantWith(IntegerSet.class));
        assertTrue(ValueUtils.create("1.0").isCompliantWith(RealSet.class));
        assertTrue(ValueUtils.create("true").isCompliantWith(BooleanSet.class));
        assertTrue(ValueUtils.create("A").isCompliantWith(StringSet.class));
        assertTrue(ValueUtils.create("\"A\"").isCompliantWith(StringSet.class));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         ValueUtils.create("~");
                     });
    }
}