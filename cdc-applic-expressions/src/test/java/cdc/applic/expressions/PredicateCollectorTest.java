package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NodePredicates;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.expressions.ast.visitors.PredicateCollector;

class PredicateCollectorTest {
    private static void check(String expression,
                              Predicate<? super Node> predicate,
                              int expectedSize) {
        final Expression e = new Expression(expression);
        final Node n = ConvertToNary.execute(e.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY);
        final Set<Node> nodes = new HashSet<>();
        assertEquals(expectedSize, PredicateCollector.collect(n, predicate, nodes).size());
    }

    @Test
    void test() {
        check("a or b", NodePredicates.IS_LEAF, 2);
        check("a or b or c", NodePredicates.IS_LEAF, 3);
        check("a or b or c", NodePredicates.IS_NARY, 1);
        check("a or b or not c", NodePredicates.IS_UNARY, 1);
        check("a or b or not c", NodePredicates.IS_BINARY, 0);
        check("a or b", NodePredicates.IS_BINARY, 1);
    }
}