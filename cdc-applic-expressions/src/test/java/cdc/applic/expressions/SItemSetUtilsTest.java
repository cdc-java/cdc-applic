package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.UncheckedSet;

class SItemSetUtilsTest {
    private static List<IntegerSet> toSets(String... contents) {
        final List<IntegerSet> list = new ArrayList<>();
        for (final String content : contents) {
            list.add(IntegerSet.of(content));
        }
        return list;
    }

    void check(List<IntegerSet> sets,
               List<IntegerSet> expected) {
        final Set<IntegerSet> actual = new HashSet<>(SItemSetUtils.minimalDisjoints(sets));
        final Set<IntegerSet> ex2 = new HashSet<>(expected);
        assertEquals(ex2, actual);
    }

    private static void check(String content,
                              Class<?> expected) {
        final SItemSet set = UncheckedSet.of(content);
        final SItemSet best1 = SItemSetUtils.createBest(set.getItems());
        final SItemSet best2 = SItemSetUtils.createBest(set.getItems().toArray(new SItem[0]));

        assertEquals(expected, best1.getClass());
        assertEquals(expected, best2.getClass());
    }

    @Test
    void test() {
        check("A, 1", UncheckedSet.class);
        check("1", IntegerSet.class);
        check("1.0", RealSet.class);
        check("A", StringSet.class);
    }

    @Test
    void testGetEmptySet() {
        assertEquals(BooleanSet.EMPTY, SItemSetUtils.getEmptySet(BooleanSet.class));
        assertEquals(StringSet.EMPTY, SItemSetUtils.getEmptySet(StringSet.class));
        assertEquals(IntegerSet.EMPTY, SItemSetUtils.getEmptySet(IntegerSet.class));
        assertEquals(RealSet.EMPTY, SItemSetUtils.getEmptySet(RealSet.class));
        assertEquals(UncheckedSet.EMPTY, SItemSetUtils.getEmptySet(UncheckedSet.class));
    }

    @Test
    void testMinmimalDisjoints() {
        check(toSets(), toSets());
        check(toSets(""), toSets());
        check(toSets("1"), toSets("1"));
        check(toSets("1, 2, 3", "1, 2"), toSets("1, 2", "3"));
        check(toSets("1, 2, 3, 4", "1, 2", "1, 3, 4"), toSets("1", "2", "3, 4"));
        check(toSets("1, 2, 3", "1, 2, 3"), toSets("1, 2, 3"));
        check(toSets("1, 2, 3", "", "1, 2, 3"), toSets("1, 2, 3"));
        check(toSets("1~5", "1, 3~6"), toSets("2", "1, 3~5", "6"));
        check(toSets("1", "1,2,3~6"), toSets("1", "2~6"));
    }
}