package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.parsing.Token;
import cdc.applic.expressions.parsing.TokenType;
import cdc.applic.expressions.parsing.Tokenizer;

class TokenizerTest {
    private static final Logger LOGGER = LogManager.getLogger(TokenizerTest.class);

    private static List<Token> tokenize(String expression,
                                        boolean strict) {
        final List<Token> tokens = new ArrayList<>();
        final Tokenizer tokenizer = new Tokenizer();
        tokenizer.init(expression);
        while (tokenizer.hasMoreTokens()) {
            tokens.add(tokenizer.nextToken(strict));
        }
        return tokens;
    }

    private static class Reduced {
        private final TokenType type;
        private final String text;

        public Reduced(TokenType type,
                       String text) {
            this.type = type;
            this.text = text;
        }

        public Reduced(Token token) {
            this(token.getType(),
                 token.getText());
        }

        @Override
        public int hashCode() {
            return Objects.hash(type,
                                text);
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (!(object instanceof Reduced)) {
                return false;
            }
            final Reduced other = (Reduced) object;
            return type == other.type
                    && text.equals(other.text);
        }

        @Override
        public String toString() {
            return "[" + type + " '" + text + "']";
        }
    }

    private static Reduced r(TokenType type,
                             String text) {
        return new Reduced(type, text);
    }

    private static void checkSuccess(String expression,
                                     boolean strict,
                                     Reduced... expected) {
        LOGGER.debug("----------------------------------");
        LOGGER.debug("expression:'" + expression + "'");
        try {
            final List<Token> tokens = tokenize(expression, strict);
            assertEquals(expected.length, tokens.size(), "Number of tokens");
            for (int index = 0; index < tokens.size(); index++) {
                assertEquals(expected[index], new Reduced(tokens.get(index)), "Token at " + index);
            }
        } catch (final LexicalException e) {
            LOGGER.error("Failed to tokenize: '" + expression + "'", e);
            throw e;
        }
    }

    private static void checkSuccessStrict(String expression,
                                           Reduced... expected) {
        checkSuccess(expression, true, expected);
    }

    private static void checkSuccessLoose(String expression,
                                          Reduced... expected) {
        checkSuccess(expression, false, expected);
    }

    private static void checkFailure(String expression,
                                     boolean strict,
                                     LexicalException.Detail detail) {
        LOGGER.debug("----------------------------------");
        LOGGER.debug("expression:'" + expression + "'");
        try {
            tokenize(expression, strict);
        } catch (final LexicalException e) {
            assertEquals(detail, e.getDetail());
            LOGGER.debug(e.getFullMessage());
            throw e;
        }
    }

    private static void checkFailureStrict(String expression,
                                           LexicalException.Detail detail) {
        checkFailure(expression, true, detail);
    }

    @Test
    void testGetText() {
        final Tokenizer tokenizer = new Tokenizer();
        tokenizer.init("a");
        assertEquals("", tokenizer.getText());
    }

    @Test
    void testOneTokenMath() {
        checkSuccessStrict("∅", r(TokenType.EMPTY_SET, "∅"));
        checkSuccessStrict("∧", r(TokenType.AND, "∧"));
        checkSuccessStrict("∨", r(TokenType.OR, "∨"));
        checkSuccessStrict("∈", r(TokenType.IN, "∈"));
        checkSuccessStrict("¬", r(TokenType.NOT, "¬"));
        checkSuccessStrict("∉", r(TokenType.NOT_IN, "∉"));
        checkSuccessStrict("≠", r(TokenType.NOT_EQUAL, "≠"));
        checkSuccessStrict("→", r(TokenType.IMPL, "→"));
        checkSuccessStrict("↔", r(TokenType.EQUIV, "↔"));
        checkSuccessStrict("↮", r(TokenType.XOR, "↮"));
        checkSuccessStrict("⊤", r(TokenType.TRUE, "⊤"));
        checkSuccessStrict("⊥", r(TokenType.FALSE, "⊥"));
        checkSuccessStrict("<", r(TokenType.LESS, "<"));
        checkSuccessStrict(">", r(TokenType.GREATER, ">"));
        checkSuccessStrict("≮", r(TokenType.NOT_LESS, "≮"));
        checkSuccessStrict("≯", r(TokenType.NOT_GREATER, "≯"));
        checkSuccessStrict("≤", r(TokenType.LESS_OR_EQUAL, "≤"));
        checkSuccessStrict("≥", r(TokenType.GREATER_OR_EQUAL, "≥"));
        checkSuccessStrict("≰", r(TokenType.NEITHER_LESS_NOR_EQUAL, "≰"));
        checkSuccessStrict("≱", r(TokenType.NEITHER_GREATER_NOR_EQUAL, "≱"));
    }

    @Test
    void testOneToken() {
        checkSuccessStrict("and", r(TokenType.AND, "and"));
        checkSuccessStrict("AND", r(TokenType.AND, "AND"));
        checkSuccessStrict("&", r(TokenType.AND, "&"));

        checkSuccessStrict("or", r(TokenType.OR, "or"));
        checkSuccessStrict("OR", r(TokenType.OR, "OR"));
        checkSuccessStrict("|", r(TokenType.OR, "|"));

        checkSuccessStrict("in", r(TokenType.IN, "in"));
        checkSuccessStrict("IN", r(TokenType.IN, "IN"));
        checkSuccessStrict("<:", r(TokenType.IN, "<:"));

        checkSuccessStrict("not", r(TokenType.NOT, "not"));
        checkSuccessStrict("NOT", r(TokenType.NOT, "NOT"));
        checkSuccessStrict("!", r(TokenType.NOT, "!"));

        checkSuccessStrict("not in", r(TokenType.NOT_IN, "not in"));
        checkSuccessStrict("NOT IN", r(TokenType.NOT_IN, "NOT IN"));
        checkSuccessStrict("!<:", r(TokenType.NOT_IN, "!<:"));

        checkSuccessStrict("imp", r(TokenType.IMPL, "imp"));
        checkSuccessStrict("->", r(TokenType.IMPL, "->"));

        checkSuccessStrict("iff", r(TokenType.EQUIV, "iff"));
        checkSuccessStrict("<->", r(TokenType.EQUIV, "<->"));

        checkSuccessStrict("xor", r(TokenType.XOR, "xor"));
        checkSuccessStrict(">-<", r(TokenType.XOR, ">-<"));

        checkSuccessStrict("=", r(TokenType.EQUAL, "="));
        checkSuccessStrict("!=", r(TokenType.NOT_EQUAL, "!="));

        checkSuccessStrict("<", r(TokenType.LESS, "<"));
        checkSuccessStrict("!<", r(TokenType.NOT_LESS, "!<"));

        checkSuccessStrict("<=", r(TokenType.LESS_OR_EQUAL, "<="));
        checkSuccessStrict("!<=", r(TokenType.NEITHER_LESS_NOR_EQUAL, "!<="));

        checkSuccessStrict(">", r(TokenType.GREATER, ">"));
        checkSuccessStrict("!>", r(TokenType.NOT_GREATER, "!>"));

        checkSuccessStrict(">=", r(TokenType.GREATER_OR_EQUAL, ">="));
        checkSuccessStrict("!>=", r(TokenType.NEITHER_GREATER_NOR_EQUAL, "!>="));

        checkSuccessStrict("(", r(TokenType.OPEN_PAREN, "("));
        checkSuccessStrict(")", r(TokenType.CLOSE_PAREN, ")"));

        checkSuccessStrict("{", r(TokenType.OPEN_SET, "{"));
        checkSuccessStrict("}", r(TokenType.CLOSE_SET, "}"));

        checkSuccessStrict("to", r(TokenType.TO, "to"));
        checkSuccessStrict("TO", r(TokenType.TO, "TO"));
        checkSuccessStrict("~", r(TokenType.TO, "~"));

        checkSuccessStrict(",", r(TokenType.ITEMS_SEP, ","));

        checkSuccessStrict(".", r(TokenType.PATH_SEP, "."));

        checkSuccessStrict("true", r(TokenType.TRUE, "true"));
        checkSuccessStrict("TRUE", r(TokenType.TRUE, "TRUE"));

        checkSuccessStrict("false", r(TokenType.FALSE, "false"));
        checkSuccessStrict("FALSE", r(TokenType.FALSE, "FALSE"));

        checkSuccessStrict("10", r(TokenType.INTEGER, "10"));
        checkSuccessStrict("+10", r(TokenType.INTEGER, "+10"));
        checkSuccessStrict("-10", r(TokenType.INTEGER, "-10"));

        checkSuccessStrict("10.0", r(TokenType.REAL, "10.0"));
        checkSuccessStrict("+10.0", r(TokenType.REAL, "+10.0"));
        checkSuccessStrict("-10.0", r(TokenType.REAL, "-10.0"));
        checkSuccessStrict("10.0e10", r(TokenType.REAL, "10.0e10"));
        checkSuccessStrict("10.0e+10", r(TokenType.REAL, "10.0e+10"));
        checkSuccessStrict("10.0e-10", r(TokenType.REAL, "10.0e-10"));
        checkSuccessStrict("10.0E+10", r(TokenType.REAL, "10.0E+10"));
        checkSuccessStrict("10.0E-10", r(TokenType.REAL, "10.0E-10"));

        // checkSuccessStrict("$", r(TokenType.TEXT, "$"));
        checkSuccessStrict("ä", r(TokenType.TEXT, "ä"));
        checkSuccessStrict("value", r(TokenType.TEXT, "value"));
        checkSuccessStrict("a", r(TokenType.TEXT, "a"));
        checkSuccessStrict("an", r(TokenType.TEXT, "an"));
        checkSuccessStrict("aa", r(TokenType.TEXT, "aa"));
        checkSuccessStrict("ana", r(TokenType.TEXT, "ana"));
        checkSuccessStrict("aad", r(TokenType.TEXT, "aad"));
        checkSuccessStrict("anda", r(TokenType.TEXT, "anda"));

        checkSuccessStrict("o", r(TokenType.TEXT, "o"));
        checkSuccessStrict("oo", r(TokenType.TEXT, "oo"));
        checkSuccessStrict("oro", r(TokenType.TEXT, "oro"));

        checkSuccessStrict("i", r(TokenType.TEXT, "i"));
        checkSuccessStrict("ini", r(TokenType.TEXT, "ini"));
        checkSuccessStrict("im", r(TokenType.TEXT, "im"));
        checkSuccessStrict("imi", r(TokenType.TEXT, "imi"));
        checkSuccessStrict("impi", r(TokenType.TEXT, "impi"));
        checkSuccessStrict("if", r(TokenType.TEXT, "if"));
        checkSuccessStrict("ifg", r(TokenType.TEXT, "ifg"));
        checkSuccessStrict("ig", r(TokenType.TEXT, "ig"));
        checkSuccessStrict("iffi", r(TokenType.TEXT, "iffi"));

        checkSuccessStrict("f", r(TokenType.TEXT, "f"));
        checkSuccessStrict("fa", r(TokenType.TEXT, "fa"));
        checkSuccessStrict("fb", r(TokenType.TEXT, "fb"));
        checkSuccessStrict("fal", r(TokenType.TEXT, "fal"));
        checkSuccessStrict("fam", r(TokenType.TEXT, "fam"));
        checkSuccessStrict("fals", r(TokenType.TEXT, "fals"));
        checkSuccessStrict("fala", r(TokenType.TEXT, "fala"));
        checkSuccessStrict("falsf", r(TokenType.TEXT, "falsf"));
        checkSuccessStrict("falte", r(TokenType.TEXT, "falte"));
        checkSuccessStrict("faase", r(TokenType.TEXT, "faase"));
        checkSuccessStrict("fblse", r(TokenType.TEXT, "fblse"));
        checkSuccessStrict("falsef", r(TokenType.TEXT, "falsef"));

        checkSuccessStrict("t", r(TokenType.TEXT, "t"));
        checkSuccessStrict("ta", r(TokenType.TEXT, "ta"));
        checkSuccessStrict("too", r(TokenType.TEXT, "too"));
        checkSuccessStrict("tr", r(TokenType.TEXT, "tr"));
        checkSuccessStrict("tru", r(TokenType.TEXT, "tru"));
        checkSuccessStrict("truf", r(TokenType.TEXT, "truf"));
        checkSuccessStrict("tra", r(TokenType.TEXT, "tra"));
        checkSuccessStrict("truet", r(TokenType.TEXT, "truet"));
        checkSuccessStrict("trae", r(TokenType.TEXT, "trae"));
        checkSuccessStrict("tbue", r(TokenType.TEXT, "tbue"));

        checkSuccessStrict("n", r(TokenType.TEXT, "n"));
        checkSuccessStrict("no", r(TokenType.TEXT, "no"));
        checkSuccessStrict("noo", r(TokenType.TEXT, "noo"));
        checkSuccessStrict("notn", r(TokenType.TEXT, "notn"));
        checkSuccessStrict("nat", r(TokenType.TEXT, "nat"));

        checkSuccessStrict("+", r(TokenType.TEXT, "+"));
        checkSuccessStrict("-", r(TokenType.TEXT, "-"));
        checkSuccessStrict("+a", r(TokenType.TEXT, "+a"));
        checkSuccessStrict("-a", r(TokenType.TEXT, "-a"));

        checkSuccessStrict("\"$\"", r(TokenType.DOUBLE_QUOTES_TEXT, "\"$\""));
        checkSuccessStrict("\"value\"", r(TokenType.DOUBLE_QUOTES_TEXT, "\"value\""));
        checkSuccessStrict("\"value\"   ", r(TokenType.DOUBLE_QUOTES_TEXT, "\"value\""));
        checkSuccessStrict("\"false\"", r(TokenType.DOUBLE_QUOTES_TEXT, "\"false\""));
        checkSuccessStrict("\"true\"", r(TokenType.DOUBLE_QUOTES_TEXT, "\"true\""));
        checkSuccessStrict("\"or\"", r(TokenType.DOUBLE_QUOTES_TEXT, "\"or\""));
        checkSuccessStrict("\"a b\"", r(TokenType.DOUBLE_QUOTES_TEXT, "\"a b\""));
        checkSuccessStrict("\"-10\"", r(TokenType.DOUBLE_QUOTES_TEXT, "\"-10\""));
        checkSuccessStrict("\"\"\"\"", r(TokenType.DOUBLE_QUOTES_TEXT, "\"\"\"\""));
        checkSuccessStrict("notä", r(TokenType.TEXT, "notä"));

        checkSuccessStrict("$XXX$", r(TokenType.DOLLAR_TEXT, "$XXX$"));
        checkSuccessStrict("$$$$", r(TokenType.DOLLAR_TEXT, "$$$$"));
    }

    @Test
    void testSpecialCases() {
        checkSuccessStrict("a", r(TokenType.TEXT, "a"));
        checkSuccessStrict("a.b",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.PATH_SEP, "."),
                           r(TokenType.TEXT, "b"));
        checkSuccessLoose("a.b",
                          r(TokenType.TEXT, "a.b"));
        checkSuccessStrict("a.b.c",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.PATH_SEP, "."),
                           r(TokenType.TEXT, "b"),
                           r(TokenType.PATH_SEP, "."),
                           r(TokenType.TEXT, "c"));
        checkSuccessLoose("a.b.c",
                          r(TokenType.TEXT, "a.b.c"));
        checkSuccessStrict("S1.1",
                           r(TokenType.TEXT, "S1"),
                           r(TokenType.PATH_SEP, "."),
                           r(TokenType.INTEGER, "1"));
        checkSuccessLoose("S1.1",
                          r(TokenType.TEXT, "S1.1"));
        checkSuccessStrict("S1.1.1",
                           r(TokenType.TEXT, "S1"),
                           r(TokenType.PATH_SEP, "."),
                           r(TokenType.REAL, "1.1"));
        checkSuccessLoose("S1.1.1",
                          r(TokenType.TEXT, "S1.1.1"));
        checkSuccessLoose("S1.1.1.1",
                          r(TokenType.TEXT, "S1.1.1.1"));
    }

    @Test
    void testManyTokens() {
        checkSuccessStrict("not∅",
                           r(TokenType.NOT, "not"),
                           r(TokenType.EMPTY_SET, "∅"));
        checkSuccessStrict("not<->",
                           r(TokenType.NOT, "not"),
                           r(TokenType.EQUIV, "<->"));
        checkSuccessStrict("!aa",
                           r(TokenType.NOT, "!"),
                           r(TokenType.TEXT, "aa"));
        checkSuccessStrict("!<a",
                           r(TokenType.NOT_LESS, "!<"),
                           r(TokenType.TEXT, "a"));
        checkSuccessStrict("!<:<",
                           r(TokenType.NOT_IN, "!<:"),
                           r(TokenType.LESS, "<"));
        checkSuccessStrict("!<:>",
                           r(TokenType.NOT_IN, "!<:"),
                           r(TokenType.GREATER, ">"));
        checkSuccessStrict("!<:>=",
                           r(TokenType.NOT_IN, "!<:"),
                           r(TokenType.GREATER_OR_EQUAL, ">="));
        checkSuccessStrict("!<:!<",
                           r(TokenType.NOT_IN, "!<:"),
                           r(TokenType.NOT_LESS, "!<"));
        checkSuccessStrict("!<:!<=",
                           r(TokenType.NOT_IN, "!<:"),
                           r(TokenType.NEITHER_LESS_NOR_EQUAL, "!<="));
        checkSuccessStrict("not an",
                           r(TokenType.NOT, "not"),
                           r(TokenType.TEXT, "an"));
        checkSuccessStrict("not ia",
                           r(TokenType.NOT, "not"),
                           r(TokenType.TEXT, "ia"));
        checkSuccessStrict("not ini",
                           r(TokenType.NOT, "not"),
                           r(TokenType.TEXT, "ini"));
        checkSuccessStrict("a<:",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.IN, "<:"));
        checkSuccessStrict("a<->",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.EQUIV, "<->"));
        checkSuccessStrict("a->",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.IMPL, "->"));
        checkSuccessStrict(" a -> b ",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.IMPL, "->"),
                           r(TokenType.TEXT, "b"));
        checkSuccessStrict(" a <-> b ",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.EQUIV, "<->"),
                           r(TokenType.TEXT, "b"));
        checkSuccessStrict(" a = b ",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.EQUAL, "="),
                           r(TokenType.TEXT, "b"));
        checkSuccessStrict(" a != b ",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.NOT_EQUAL, "!="),
                           r(TokenType.TEXT, "b"));

        checkSuccessStrict(" a < b ",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.LESS, "<"),
                           r(TokenType.TEXT, "b"));
        checkSuccessStrict(" a <- b ",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.LESS, "<"),
                           r(TokenType.TEXT, "-"),
                           r(TokenType.TEXT, "b"));
        checkSuccessStrict(" a <a> b ",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.LESS, "<"),
                           r(TokenType.TEXT, "a"),
                           r(TokenType.GREATER, ">"),
                           r(TokenType.TEXT, "b"));
        checkSuccessStrict("-<",
                           r(TokenType.TEXT, "-"),
                           r(TokenType.LESS, "<"));
        checkSuccessStrict("<>",
                           r(TokenType.LESS, "<"),
                           r(TokenType.GREATER, ">"));
        checkSuccessStrict("<>-<>",
                           r(TokenType.LESS, "<"),
                           r(TokenType.XOR, ">-<"),
                           r(TokenType.GREATER, ">"));
        checkSuccessStrict("<-<>",
                           r(TokenType.LESS, "<"),
                           r(TokenType.TEXT, "-"),
                           r(TokenType.LESS, "<"),
                           r(TokenType.GREATER, ">"));
        checkSuccessStrict("not<+>",
                           r(TokenType.NOT, "not"),
                           r(TokenType.LESS, "<"),
                           r(TokenType.TEXT, "+"),
                           r(TokenType.GREATER, ">"));
        checkSuccessStrict("not<--",
                           r(TokenType.NOT, "not"),
                           r(TokenType.LESS, "<"),
                           r(TokenType.TEXT, "--"));
        checkSuccessStrict("<-",
                           r(TokenType.LESS, "<"),
                           r(TokenType.TEXT, "-"));
        checkSuccessStrict("<--",
                           r(TokenType.LESS, "<"),
                           r(TokenType.TEXT, "--"));
        checkSuccessStrict("<+-",
                           r(TokenType.LESS, "<"),
                           r(TokenType.TEXT, "+-"));
        checkSuccessStrict("a.b",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.PATH_SEP, "."),
                           r(TokenType.TEXT, "b"));
        checkSuccessStrict("a . b",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.PATH_SEP, "."),
                           r(TokenType.TEXT, "b"));
        checkSuccessStrict("\"a \"   .   b",
                           r(TokenType.DOUBLE_QUOTES_TEXT, "\"a \""),
                           r(TokenType.PATH_SEP, "."),
                           r(TokenType.TEXT, "b"));

        checkSuccessStrict("a.1",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.PATH_SEP, "."),
                           r(TokenType.INTEGER, "1"));

        checkSuccessStrict("1.1 a",
                           r(TokenType.REAL, "1.1"),
                           r(TokenType.TEXT, "a"));

        checkSuccessStrict("\"1\".a",
                           r(TokenType.DOUBLE_QUOTES_TEXT, "\"1\""),
                           r(TokenType.PATH_SEP, "."),
                           r(TokenType.TEXT, "a"));

        checkSuccessStrict("a $X$",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.DOLLAR_TEXT, "$X$"));
        checkSuccessStrict("a$X$",
                           r(TokenType.TEXT, "a$X$"));
        checkSuccessStrict("a$",
                           r(TokenType.TEXT, "a$"));

        checkSuccessStrict("a \"X\"",
                           r(TokenType.TEXT, "a"),
                           r(TokenType.DOUBLE_QUOTES_TEXT, "\"X\""));
        checkSuccessStrict("a\"X\"",
                           r(TokenType.TEXT, "a\"X\""));
        checkSuccessStrict("a\"",
                           r(TokenType.TEXT, "a\""));
    }

    @Test
    void testInvalidToken() {
        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("\"", LexicalException.Detail.MISSING_CLOSING_DOUBLE_QUOTES);
                     });
        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("\"\"\"", LexicalException.Detail.MISSING_CLOSING_DOUBLE_QUOTES);
                     });
        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("\"\"\"   ", LexicalException.Detail.MISSING_CLOSING_DOUBLE_QUOTES);
                     });

        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("\"\"", LexicalException.Detail.EMPTY_DOUBLE_QUOTES_ESCAPED_TEXT);
                     });

        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("$", LexicalException.Detail.MISSING_CLOSING_DOLLAR);
                     });
        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("$$$", LexicalException.Detail.MISSING_CLOSING_DOLLAR);
                     });
        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("$$$   ", LexicalException.Detail.MISSING_CLOSING_DOLLAR);
                     });

        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("$$", LexicalException.Detail.EMPTY_DOLLAR_ESCAPED_TEXT);
                     });

        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("10.", LexicalException.Detail.INVALID_NUMBER);
                     });
        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("10.a", LexicalException.Detail.INVALID_NUMBER);
                     });
        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("+10.", LexicalException.Detail.INVALID_NUMBER);
                     });
        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("-10.", LexicalException.Detail.INVALID_NUMBER);
                     });
        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("10.0e", LexicalException.Detail.INVALID_NUMBER);
                     });
        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("10.0e+", LexicalException.Detail.INVALID_NUMBER);
                     });
        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("10.0e-", LexicalException.Detail.INVALID_NUMBER);
                     });
        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("10.0ee", LexicalException.Detail.INVALID_NUMBER);
                     });
        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("10a", LexicalException.Detail.MISSING_BOUNDARY);
                     });
        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("10.0a", LexicalException.Detail.MISSING_BOUNDARY);
                     });
        assertThrows(LexicalException.class,
                     () -> {
                         checkFailureStrict("10.0.", LexicalException.Detail.MISSING_BOUNDARY);
                     });
    }

    @Test
    void testNextToken() {
        final Tokenizer tokenizer = new Tokenizer();
        tokenizer.init("");
        assertEquals("", tokenizer.getExpression());
        for (int index = 0; index < 10; index++) {
            final Token token = tokenizer.nextToken(true);
            assertEquals(TokenType.EPSILON, token.getType());
        }
    }
}