package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.ApplicIssueType;
import cdc.applic.expressions.checks.SyntaxIssue;
import cdc.issues.Diagnosis;

class DiagnosisTest {
    private static final Logger LOGGER = LogManager.getLogger(DiagnosisTest.class);

    @Test
    void test() {
        final Diagnosis<ApplicIssue> ok = Diagnosis.of();
        assertTrue(ok.getIssues().isEmpty());
        assertTrue(ok.isOk());
        assertEquals(ApplicIssueType.NO_ISSUE, ApplicIssue.getMostSevereType(ok.getIssues()));

        LOGGER.debug("diag: " + ok);

        final Diagnosis<ApplicIssue> ko =
                Diagnosis.of(SyntaxIssue.builder().cause("XXX",
                                                             new SyntacticException(SyntacticException.Detail.UNEXPECTED_TOKEN,
                                                                                    "xx"))
                                            .build());
        assertEquals(1, ko.getIssues().size());
        LOGGER.debug("diag: " + ko);
    }
}