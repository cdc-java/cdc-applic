package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

class ExpressionsTest {
    private static final Logger LOGGER = LogManager.getLogger(ExpressionsTest.class);

    private static Expressions x = Expressions.SHORT_NARROW_NO_SIMPLIFY;
    private static Expressions y = Expressions.SHORT_NARROW_SIMPLIFY;

    void check(String expected,
               Expression expression) {
        LOGGER.debug(expression);
        assertEquals(expected, expression.getContent());
    }

    @Test
    void testNoSimplify() {
        check("P=10", x.equal("P", "10"));
        check("P=10", x.equal(x.name("P"), x.value(10)));
        check("P=10.0", x.equal("P", "10.0"));
        check("P=10.0", x.equal(x.name("P"), x.value(10.0)));

        check("P!=10", x.notEqual("P", "10"));

        check("\"P P\"=10", x.equal("P P", "10"));
        check("\"P P\"=10", x.equal(x.name("P P", false), x.value("10")));

        check("P<:{10}", x.in("P", "10"));
        check("P<:{10.0}", x.in("P", "10.0"));
        check("P<:{10,12~15,17}", x.in(x.name("P"), x.set(x.value(10), x.range(12, 15), x.range(17))));
        check("P<:{10.0,12.0~15.0,17.0}", x.in(x.name("P"), x.set(x.value(10.0), x.range(12.0, 15.0), x.range(17.0))));

        check("P!<:{10}", x.notIn("P", "10"));

        check("A", x.ref("A"));
        check("!A", x.not(x.ref("A")));
        check("A->B", x.imp(x.ref("A"), x.ref("B")));
        check("A<->B", x.iff(x.ref("A"), x.ref("B")));
        check("A", x.or(x.ref("A")));
        check("A", x.and(x.ref("A")));
        check("A|B", x.or(x.ref("A"), x.ref("B")));
        check("A&B", x.and(x.ref("A"), x.ref("B")));
        check("A|B|C", x.or(x.ref("A"), x.ref("B"), x.ref("C")));
        check("A&B&C", x.and(x.ref("A"), x.ref("B"), x.ref("C")));

        check("A&B->C", x.imp(x.and(x.ref("A"), x.ref("B")), x.ref("C")));
        check("A&B->C", x.imp(x.and(Arrays.asList(x.ref("A"), x.ref("B"))), x.ref("C")));
        check("A&B->!C", x.imp(x.and(x.ref("A"), x.ref("B")), x.not(x.ref("C"))));
        check("!A&B->!C", x.imp(x.and(x.not(x.ref("A")), x.ref("B")), x.not(x.ref("C"))));
        check("!(A&B)->C", x.imp(x.not(x.and(x.ref("A"), x.ref("B"))), x.ref("C")));
        check("!(A|B)->C", x.imp(x.not(x.or(x.ref("A"), x.ref("B"))), x.ref("C")));
        check("!(A|B)->C", x.imp(x.not(x.or(Arrays.asList(x.ref("A"), x.ref("B")))), x.ref("C")));
    }

    @Test
    void testSimplify() {
        check("P=10", y.equal("P", "10"));
        check("P=10", y.equal(y.name("P"), y.value(10)));
        check("P=10.0", y.equal("P", "10.0"));
        check("P=10.0", y.equal(y.name("P"), y.value(10.0)));

        check("P!=10", y.notEqual("P", "10"));

        check("\"P P\"=10", y.equal("P P", "10"));
        check("\"P P\"=10", y.equal(y.name("P P", false), y.value("10")));

        check("P<:{10}", y.in("P", "10"));
        check("P<:{10.0}", y.in("P", "10.0"));
        check("P<:{10,12~15,17}", y.in(y.name("P"), y.set(y.value(10), y.range(12, 15), y.range(17))));
        check("P<:{10.0,12.0~15.0,17.0}", y.in(y.name("P"), y.set(y.value(10.0), y.range(12.0, 15.0), y.range(17.0))));

        check("P!<:{10}", y.notIn("P", "10"));

        check("A", y.ref("A"));
        check("!A", y.not(y.ref("A")));
        check("A->B", y.imp(y.ref("A"), y.ref("B")));
        check("A<->B", y.iff(y.ref("A"), y.ref("B")));
        check("A", y.or(y.ref("A")));
        check("A", y.and(y.ref("A")));
        check("A|B", y.or(y.ref("A"), y.ref("B")));
        check("A&B", y.and(y.ref("A"), y.ref("B")));
        check("A|B|C", y.or(y.ref("A"), y.ref("B"), y.ref("C")));
        check("A&B&C", y.and(y.ref("A"), y.ref("B"), y.ref("C")));

        check("A&B->C", y.imp(y.and(y.ref("A"), y.ref("B")), y.ref("C")));
        check("A&B->C", y.imp(y.and(Arrays.asList(y.ref("A"), y.ref("B"))), y.ref("C")));
        check("A&B->!C", y.imp(y.and(y.ref("A"), y.ref("B")), y.not(y.ref("C"))));
        check("!A&B->!C", y.imp(y.and(y.not(y.ref("A")), y.ref("B")), y.not(y.ref("C"))));
        check("!(A&B)->C", y.imp(y.not(y.and(y.ref("A"), y.ref("B"))), y.ref("C")));
        check("!(A|B)->C", y.imp(y.not(y.or(y.ref("A"), y.ref("B"))), y.ref("C")));
        check("!(A|B)->C", y.imp(y.not(y.or(Arrays.asList(y.ref("A"), y.ref("B")))), y.ref("C")));

        check("A&B", y.and(y.ref("A"), y.ref("B"), y.ref("A")));
        check("A", y.and(y.ref("A"), y.ref("A"), y.ref("A")));
        check("A", y.and(y.ref("A"), Expression.TRUE));
        check("false", y.and(y.ref("A"), Expression.FALSE));

        check("A|B", y.or(y.ref("A"), y.ref("B"), y.ref("A")));
        check("A", y.or(y.ref("A"), y.ref("A"), y.ref("A")));
        check("true", y.or(y.ref("A"), Expression.TRUE));
        check("A", y.or(y.ref("A"), Expression.FALSE));
    }
}