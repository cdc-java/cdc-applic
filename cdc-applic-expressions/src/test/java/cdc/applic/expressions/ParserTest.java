package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.parsing.Parser;

class ParserTest {
    private static final Logger LOGGER = LogManager.getLogger(ParserTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();
    private static final PrintStream ERR = IoBuilder.forLogger(LOGGER).setLevel(Level.ERROR).buildPrintStream();

    private final Parser parser = new Parser();

    private void checkValid(String expression) {
        try {
            final Node node = parser.parse(expression);
            LOGGER.debug("-------------------------------");
            LOGGER.debug(expression);
            node.print(OUT);

        } catch (final SyntacticException e) {
            LOGGER.error("Failed (unexpected) to parse: '" + expression + "'", e);
            assertFalse(true);
        }
    }

    private void checkInvalid(String expression,
                              SyntacticException.Detail expectedDetail) {
        try {
            final Node node = parser.parse(expression);
            LOGGER.debug("-------------------------------");
            LOGGER.debug(expression);
            node.print(OUT);
            assertTrue(false);
        } catch (final SyntacticException e) {
            LOGGER.debug("Failed (expected) to parse: '" + expression + "'", e);
            assertEquals(expectedDetail, e.getDetail());
        }
    }

    private void checkInvalid(String expression,
                              LexicalException.Detail expectedDetail) {
        try {
            final Node node = parser.parse(expression);
            LOGGER.debug("-------------------------------");
            LOGGER.debug(expression);
            node.print(OUT);
            assertTrue(false);
        } catch (final LexicalException e) {
            LOGGER.debug("Failed (expected) to parse: '" + expression + "'", e);
            assertEquals(expectedDetail, e.getDetail());
        }
    }

    private void check(String expression,
                       String expected) {
        try {
            LOGGER.debug("-------------------------------");
            LOGGER.debug(expression);
            // LOGGER.debug(Tokenizer.tokenize(expression));
            final Node node = parser.parse(expression);
            LOGGER.debug(node);
            node.print(OUT);
            try {
                assertEquals(expected, node.toString());
            } catch (final AssertionError e) {
                node.print(ERR);
                throw e;
            }
        } catch (final LexicalException | SyntacticException e) {
            LOGGER.error("Failed to parse: '" + expression + "'", e);
            assertNull(expected);
        }
    }

    @Test
    void testRef() {
        check("A", "REF(A)"); // sname
        check("A.B", "REF(A.B)"); // prefix + sname
        check("\"A\".\"B\"", "REF(A.B)"); // useless escaping
        checkInvalid("A.B.C", SyntacticException.Detail.UNEXPECTED_TOKEN);
        check("\"A.B\"", "REF(\"A.B\")"); // sname
        check("\"A.B.C\"", "REF(\"A.B.C\")"); // sname
        check("\"A.B\".C", "REF(\"A.B\".C)"); // prefix + sname
        check("A.\"B.C\"", "REF(A.\"B.C\")"); // prefix + sname
        checkInvalid("\"A.B.C", LexicalException.Detail.MISSING_CLOSING_DOUBLE_QUOTES);
    }

    @Test
    void testValid() {
        check("\"TRue\"", "REF(\"TRue\")");
        check("\"A B\"", "REF(\"A B\")");
        check("M1000 = \"TRue\"", "EQUAL(M1000,\"TRue\")");
        check("Biplace", "REF(Biplace)");
        check("\"Bi-Place\"", "REF(Bi-Place)");
        check("\"to\" = \"to\"", "EQUAL(\"to\",\"to\")");
        check("\"and\" = \"and\"", "EQUAL(\"and\",\"and\")");

        check("Version = IN", "EQUAL(Version,\"IN\")");
        check("Version = \"$\"", "EQUAL(Version,\"$\")");
        check("Version IN {\"$\"}", "IN(Version,{\"$\"})");
        check("Version != IN", "NOT_EQUAL(Version,\"IN\")");
        check("Version in {IN}", "IN(Version,{\"IN\"})");
        check("Version not in {IN}", "NOT_IN(Version,{\"IN\"})");
        check("Version in {IN,IN}", "IN(Version,{\"IN\",\"IN\"})");
        check("Version = V1", "EQUAL(Version,V1)");
        check("\"La Version\" = V1", "EQUAL(\"La Version\",V1)");
        check("Not (Version = V1)", "NOT(EQUAL(Version,V1))");
        check("Version in {}", "IN(Version,{})");
        check("Version in {V1}", "IN(Version,{V1})");
        check("Version in {V1, V2}", "IN(Version,{V1,V2})");
        check("Version in {1, 2, 3~4}", "IN(Version,{1,2,3~4})");
        check("Version in {-1, 2, 3~4}", "IN(Version,{-1,2,3~4})");
        check("Version in {-1, 2, -10~4}", "IN(Version,{-1,2,-10~4})");
        check("Version = V1 or Version = V2", "OR(EQUAL(Version,V1),EQUAL(Version,V2))");
        check("Version = V1 or Version = V2 and Standard = S1",
              "OR(EQUAL(Version,V1),AND(EQUAL(Version,V2),EQUAL(Standard,S1)))");
        check("(Version = V1 or Version = V2) and Standard = S1",
              "AND(OR(EQUAL(Version,V1),EQUAL(Version,V2)),EQUAL(Standard,S1))");
        check("M1000 = true", "EQUAL(M1000,true)");
        check("TRUE", "TRUE");
        check("true", "TRUE");
        check("FALSE", "FALSE");
        check("false", "FALSE");
        check("(FALSE)", "FALSE");
        check("(((M1000)))", "REF(M1000)");
        check("Version = V1 or true", "OR(EQUAL(Version,V1),TRUE)");
        checkValid("Version = V1 or Version = V2 or Version = V3");
        check("Version = V1 or Version = V2 or Version = V3",
              "OR(OR(EQUAL(Version,V1),EQUAL(Version,V2)),EQUAL(Version,V3))");
        check("(Version = V1 or Version = V2) or Version = V3",
              "OR(OR(EQUAL(Version,V1),EQUAL(Version,V2)),EQUAL(Version,V3))");
        check("Version = V1 or (Version = V2 or Version = V3)",
              "OR(EQUAL(Version,V1),OR(EQUAL(Version,V2),EQUAL(Version,V3)))");
        check("A -> B", "IMPLICATION(REF(A),REF(B))");
        check("A <-> B", "EQUIVALENCE(REF(A),REF(B))");
        check("(A -> B) -> C", "IMPLICATION(IMPLICATION(REF(A),REF(B)),REF(C))");
        check("A -> (B -> C)", "IMPLICATION(REF(A),IMPLICATION(REF(B),REF(C)))");
        check("A <-> B <-> C", "EQUIVALENCE(EQUIVALENCE(REF(A),REF(B)),REF(C))");
        check("A <-> (B <-> C)", "EQUIVALENCE(REF(A),EQUIVALENCE(REF(B),REF(C)))");
        check("A >-< B", "XOR(REF(A),REF(B))");
        check("A >-< B >-< C", "XOR(XOR(REF(A),REF(B)),REF(C))");
        check("A >-< (B >-< C)", "XOR(REF(A),XOR(REF(B),REF(C)))");
        check("A <-> B >-< C", "XOR(EQUIVALENCE(REF(A),REF(B)),REF(C))");
        check("\"A\" = B", "EQUAL(A,B)");
        check("\"A \" = B", "EQUAL(\"A \",B)");
        check("\"A,\" = B", "EQUAL(\"A,\",B)");
        check("\"A A\" = B", "EQUAL(\"A A\",B)");
        check("A = \" B \"", "EQUAL(A,\" B \")");
        check("A in { \" B \" }", "IN(A,{\" B \"})");
        check("A in { \"Hello World!\" , \"How Are You?\" }", "IN(A,{\"Hello World!\",\"How Are You?\"})");
        check("not A", "NOT(REF(A))");
        check("A != X", "NOT_EQUAL(A,X)");
        check("A !<: {X}", "NOT_IN(A,{X})");
        check("not (A)", "NOT(REF(A))");

        check("A !<: {}", "NOT_IN(A,{})");
        check("A !<: ∅", "NOT_IN(A,{})");
        check("A !<: ∅ or true", "OR(NOT_IN(A,{}),TRUE)");

        check("A = B", "EQUAL(A,B)");
        check("A != B", "NOT_EQUAL(A,B)");
        check("A > B", "GREATER(A,B)");
        check("A >= B", "GREATER_OR_EQUAL(A,B)");
        check("A !> B", "NOT_GREATER(A,B)");
        check("A !>= B", "NEITHER_GREATER_NOR_EQUAL(A,B)");
        check("A < B", "LESS(A,B)");
        check("A <= B", "LESS_OR_EQUAL(A,B)");
        check("A !< B", "NOT_LESS(A,B)");
        check("A !<= B", "NEITHER_LESS_NOR_EQUAL(A,B)");

        check("A = 1", "EQUAL(A,1)");
        check("A != 1", "NOT_EQUAL(A,1)");
        check("A > 1", "GREATER(A,1)");
        check("A >= 1", "GREATER_OR_EQUAL(A,1)");
        check("A !> 1", "NOT_GREATER(A,1)");
        check("A !>= 1", "NEITHER_GREATER_NOR_EQUAL(A,1)");
        check("A < 1", "LESS(A,1)");
        check("A <= 1", "LESS_OR_EQUAL(A,1)");
        check("A !< 1", "NOT_LESS(A,1)");
        check("A !<= 1", "NEITHER_LESS_NOR_EQUAL(A,1)");

        check("A = 1.0", "EQUAL(A,1.0)");
        check("A != 1.0", "NOT_EQUAL(A,1.0)");
        check("A > 1.0", "GREATER(A,1.0)");
        check("A >= 1.0", "GREATER_OR_EQUAL(A,1.0)");
        check("A !> 1.0", "NOT_GREATER(A,1.0)");
        check("A !>= 1.0", "NEITHER_GREATER_NOR_EQUAL(A,1.0)");
        check("A < 1.0", "LESS(A,1.0)");
        check("A <= 1.0", "LESS_OR_EQUAL(A,1.0)");
        check("A !< 1.0", "NOT_LESS(A,1.0)");
        check("A !<= 1.0", "NEITHER_LESS_NOR_EQUAL(A,1.0)");

        // true is parsed a TrueNode when it is alone.
        // In a property node, it is a string value.

        check("A = true", "EQUAL(A,true)");
        check("A != true", "NOT_EQUAL(A,true)");
        check("A > true", "GREATER(A,true)");
        check("A >= true", "GREATER_OR_EQUAL(A,true)");
        check("A !> true", "NOT_GREATER(A,true)");
        check("A !>= true", "NEITHER_GREATER_NOR_EQUAL(A,true)");
        check("A < true", "LESS(A,true)");
        check("A <= true", "LESS_OR_EQUAL(A,true)");
        check("A !< true", "NOT_LESS(A,true)");
        check("A !<= true", "NEITHER_LESS_NOR_EQUAL(A,true)");

        check("$X$", "INFORMAL(X)");
        check("$X$ or $Y$", "OR(INFORMAL(X),INFORMAL(Y))");
        check("$X$ and $Y$", "AND(INFORMAL(X),INFORMAL(Y))");
        check("!$X$ and not $Y$", "AND(NOT(INFORMAL(X)),NOT(INFORMAL(Y)))");
        check("!$X$", "NOT(INFORMAL(X))");
        check("!($X$ and $Y$)", "NOT(AND(INFORMAL(X),INFORMAL(Y)))");
    }

    @Test
    void testValidReserved() {
        check("A = IN", "EQUAL(A,\"IN\")");
        check("A != IN", "NOT_EQUAL(A,\"IN\")");
        check("A > IN", "GREATER(A,\"IN\")");
        check("A >= IN", "GREATER_OR_EQUAL(A,\"IN\")");
        check("A !> IN", "NOT_GREATER(A,\"IN\")");
        check("A !>= IN", "NEITHER_GREATER_NOR_EQUAL(A,\"IN\")");
        check("A < IN", "LESS(A,\"IN\")");
        check("A <= IN", "LESS_OR_EQUAL(A,\"IN\")");
        check("A !< IN", "NOT_LESS(A,\"IN\")");
        check("A !<= IN", "NEITHER_LESS_NOR_EQUAL(A,\"IN\")");

        check("A = IFF", "EQUAL(A,\"IFF\")");
        check("A != IFF", "NOT_EQUAL(A,\"IFF\")");
        check("A > IFF", "GREATER(A,\"IFF\")");
        check("A >= IFF", "GREATER_OR_EQUAL(A,\"IFF\")");
        check("A !> IFF", "NOT_GREATER(A,\"IFF\")");
        check("A !>= IFF", "NEITHER_GREATER_NOR_EQUAL(A,\"IFF\")");
        check("A < IFF", "LESS(A,\"IFF\")");
        check("A <= IFF", "LESS_OR_EQUAL(A,\"IFF\")");
        check("A !< IFF", "NOT_LESS(A,\"IFF\")");
        check("A !<= IFF", "NEITHER_LESS_NOR_EQUAL(A,\"IFF\")");

        check("A = NOT", "EQUAL(A,\"NOT\")");
        check("A != NOT", "NOT_EQUAL(A,\"NOT\")");
        check("A > NOT", "GREATER(A,\"NOT\")");
        check("A >= NOT", "GREATER_OR_EQUAL(A,\"NOT\")");
        check("A !> NOT", "NOT_GREATER(A,\"NOT\")");
        check("A !>= NOT", "NEITHER_GREATER_NOR_EQUAL(A,\"NOT\")");
        check("A < NOT", "LESS(A,\"NOT\")");
        check("A <= NOT", "LESS_OR_EQUAL(A,\"NOT\")");
        check("A !< NOT", "NOT_LESS(A,\"NOT\")");
        check("A !<= NOT", "NEITHER_LESS_NOR_EQUAL(A,\"NOT\")");

        check("A = IMP", "EQUAL(A,\"IMP\")");
        check("A != IMP", "NOT_EQUAL(A,\"IMP\")");
        check("A > IMP", "GREATER(A,\"IMP\")");
        check("A >= IMP", "GREATER_OR_EQUAL(A,\"IMP\")");
        check("A !> IMP", "NOT_GREATER(A,\"IMP\")");
        check("A !>= IMP", "NEITHER_GREATER_NOR_EQUAL(A,\"IMP\")");
        check("A < IMP", "LESS(A,\"IMP\")");
        check("A <= IMP", "LESS_OR_EQUAL(A,\"IMP\")");
        check("A !< IMP", "NOT_LESS(A,\"IMP\")");
        check("A !<= IMP", "NEITHER_LESS_NOR_EQUAL(A,\"IMP\")");

        check("A = TO", "EQUAL(A,\"TO\")");
        check("A != TO", "NOT_EQUAL(A,\"TO\")");
        check("A > TO", "GREATER(A,\"TO\")");
        check("A >= TO", "GREATER_OR_EQUAL(A,\"TO\")");
        check("A !> TO", "NOT_GREATER(A,\"TO\")");
        check("A !>= TO", "NEITHER_GREATER_NOR_EQUAL(A,\"TO\")");
        check("A < TO", "LESS(A,\"TO\")");
        check("A <= TO", "LESS_OR_EQUAL(A,\"TO\")");
        check("A !< TO", "NOT_LESS(A,\"TO\")");
        check("A !<= TO", "NEITHER_LESS_NOR_EQUAL(A,\"TO\")");

        check("A = OR", "EQUAL(A,\"OR\")");
        check("A != OR", "NOT_EQUAL(A,\"OR\")");
        check("A > OR", "GREATER(A,\"OR\")");
        check("A >= OR", "GREATER_OR_EQUAL(A,\"OR\")");
        check("A !> OR", "NOT_GREATER(A,\"OR\")");
        check("A !>= OR", "NEITHER_GREATER_NOR_EQUAL(A,\"OR\")");
        check("A < OR", "LESS(A,\"OR\")");
        check("A <= OR", "LESS_OR_EQUAL(A,\"OR\")");
        check("A !< OR", "NOT_LESS(A,\"OR\")");
        check("A !<= OR", "NEITHER_LESS_NOR_EQUAL(A,\"OR\")");

        check("A = AND", "EQUAL(A,\"AND\")");
        check("A != AND", "NOT_EQUAL(A,\"AND\")");
        check("A > AND", "GREATER(A,\"AND\")");
        check("A >= AND", "GREATER_OR_EQUAL(A,\"AND\")");
        check("A !> AND", "NOT_GREATER(A,\"AND\")");
        check("A !>= AND", "NEITHER_GREATER_NOR_EQUAL(A,\"AND\")");
        check("A < AND", "LESS(A,\"AND\")");
        check("A <= AND", "LESS_OR_EQUAL(A,\"AND\")");
        check("A !< AND", "NOT_LESS(A,\"AND\")");
        check("A !<= AND", "NEITHER_LESS_NOR_EQUAL(A,\"AND\")");

        check("A = XOR", "EQUAL(A,\"XOR\")");
        check("A != XOR", "NOT_EQUAL(A,\"XOR\")");
        check("A > XOR", "GREATER(A,\"XOR\")");
        check("A >= XOR", "GREATER_OR_EQUAL(A,\"XOR\")");
        check("A !> XOR", "NOT_GREATER(A,\"XOR\")");
        check("A !>= XOR", "NEITHER_GREATER_NOR_EQUAL(A,\"XOR\")");
        check("A < XOR", "LESS(A,\"XOR\")");
        check("A <= XOR", "LESS_OR_EQUAL(A,\"XOR\")");
        check("A !< XOR", "NOT_LESS(A,\"XOR\")");
        check("A !<= XOR", "NEITHER_LESS_NOR_EQUAL(A,\"XOR\")");

        check("A in {IN, NOT, AND, OR, IMP, IFF, TO}", "IN(A,{\"IN\",\"NOT\",\"AND\",\"OR\",\"IMP\",\"IFF\",\"TO\"})");
        check("A not in {IN, NOT, AND, OR, IMP, IFF, TO}", "NOT_IN(A,{\"IN\",\"NOT\",\"AND\",\"OR\",\"IMP\",\"IFF\",\"TO\"})");
    }

    @Test
    void testNot() {
        check("NOT A", "NOT(REF(A))");
        check("NOT NOT A", "NOT(NOT(REF(A)))");
        check("NOT (NOT A)", "NOT(NOT(REF(A)))");
        check("NOT (A or B)", "NOT(OR(REF(A),REF(B)))");
        check("NOT (NOT A or B)", "NOT(OR(NOT(REF(A)),REF(B)))");
        check("NOT (NOT A or NOT B)", "NOT(OR(NOT(REF(A)),NOT(REF(B))))");
        check("NOT $A$", "NOT(INFORMAL(A))");
        check("NOT \"A\"", "NOT(REF(A))");
    }

    @Test
    void testTo() {
        check("A IN {10 TO 20}", "IN(A,{10~20})");
    }

    @Test
    void testPrecedence() {
        check("A or B or C", "OR(OR(REF(A),REF(B)),REF(C))");
        check("A and B and C", "AND(AND(REF(A),REF(B)),REF(C))");
        check("A -> B -> C", "IMPLICATION(IMPLICATION(REF(A),REF(B)),REF(C))");
        check("(A <-> B) <-> C", "EQUIVALENCE(EQUIVALENCE(REF(A),REF(B)),REF(C))");
    }

    @Test
    void testInvalid() {
        checkInvalid("Value in { A to B }", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("Value in { 1~B }", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("Value in { 1~BBBB }", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("Version in {M}}", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("Version in {M", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("()", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("(FALSE))", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("((FALSE)", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("FALSE = FALSE", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("XXX = ", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("XXX = {}", SyntacticException.Detail.UNEXPECTED_TOKEN);
        // checkInvalid("x = and", SyntacticException.Detail.UNEXPECTED_TOKEN);
        // checkInvalid("x = or", SyntacticException.Detail.UNEXPECTED_TOKEN);
        // checkInvalid("x = to", SyntacticException.Detail.UNEXPECTED_TOKEN);
        // checkInvalid("x = imp", SyntacticException.Detail.UNEXPECTED_TOKEN);
        // checkInvalid("x = iff", SyntacticException.Detail.UNEXPECTED_TOKEN);
        // checkInvalid("x = not", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("x in {1.0~10}", SyntacticException.Detail.UNEXPECTED_TOKEN);
        // checkInvalid("x < not", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("x < <", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("x < !", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("x ! <", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("x !<", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("a.$X$", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("a=$X$", SyntacticException.Detail.UNEXPECTED_TOKEN);
        checkInvalid("a in{$X$}", SyntacticException.Detail.UNEXPECTED_TOKEN);
    }

    @Test
    void testPrefix() {
        check("P.A", "REF(P.A)");
        check("P.A or Q.B or R.C", "OR(OR(REF(P.A),REF(Q.B)),REF(R.C))");
        check("P.A xor Q.B xor R.C", "XOR(XOR(REF(P.A),REF(Q.B)),REF(R.C))");
        check("NOT P.A", "NOT(REF(P.A))");
        check("P.A = AND", "EQUAL(P.A,\"AND\")");
        check("P.Version = IN", "EQUAL(P.Version,\"IN\")");
        check("P.A = IFF", "EQUAL(P.A,\"IFF\")");
    }
}