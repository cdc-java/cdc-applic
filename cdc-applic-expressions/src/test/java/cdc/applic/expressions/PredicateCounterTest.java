package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.function.Predicate;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NodePredicates;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.expressions.ast.visitors.PredicateCounter;

class PredicateCounterTest {
    private static void check(String expression,
                              Predicate<? super Node> predicate,
                              int expected) {
        final Expression e = new Expression(expression);
        final Node n = ConvertToNary.execute(e.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY);

        assertEquals(expected, PredicateCounter.count(n, predicate));
    }

    @Test
    void test() {
        check("true", NodePredicates.IS_FALSE, 0);
        check("true", NodePredicates.IS_TRUE, 1);
        check("true", NodePredicates.IS_BINARY, 0);
        check("true", NodePredicates.IS_NARY, 0);
        check("false", NodePredicates.IS_FALSE, 1);
        check("x or false", NodePredicates.IS_FALSE, 1);
        check("not x or false", NodePredicates.IS_FALSE, 1);
        check("not x or false", NodePredicates.IS_BINARY, 1);
        check("not x or false", NodePredicates.IS_UNARY, 1);
        check("not x or false", NodePredicates.IS_LEAF, 2);
        check("a or b or c", NodePredicates.IS_NARY, 1);
        check("a or b or c", NodePredicates.IS_BINARY, 0);
        check("x and y", NodePredicates.IS_BINARY_AND, 1);
        check("x or y", NodePredicates.IS_BINARY_OR, 1);
    }
}