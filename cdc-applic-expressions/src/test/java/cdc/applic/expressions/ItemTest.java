package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.SItem;

class ItemTest {
    @Test
    void test() {
        assertEquals(IllegalArgumentException.class, SItem.newIllegalConversion(null, null).getClass());
    }
}