package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.SyntacticException.Detail;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.RealRange;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.parsing.SItemsParsing;

class SItemsParsingTest {
    private static void checkInvalidContent(Function<String, ?> f,
                                            String content,
                                            Detail expectedDetail) {
        try {
            f.apply(content);
            assertFalse(true, "Expected invalid content");
        } catch (final SyntacticException e) {
            assertEquals(expectedDetail, e.getDetail());
        }
    }

    private static <T> void checkValidContent(Function<String, List<T>> f,
                                              String content,
                                              List<T> expected) {
        final List<T> result = f.apply(content);
        assertEquals(expected, result);
    }

    private static void checkBooleansSetParsing(String content,
                                                List<BooleanValue> expected) {
        checkValidContent(SItemsParsing::toBooleanValues, content, expected);
    }

    private static void checkIntegersSetParsing(String content,
                                                List<IntegerRange> expected) {
        checkValidContent(SItemsParsing::toIntegerRanges, content, expected);
    }

    private static void checkRealsSetParsing(String content,
                                             List<RealRange> expected) {
        checkValidContent(SItemsParsing::toRealRanges, content, expected);
    }

    private static void checkStringsSetParsing(String content,
                                               List<StringValue> expected) {
        checkValidContent(SItemsParsing::toStringValues, content, expected);
    }

    private static void checkUncheckedSetParsing(String content,
                                                 List<SItem> expected) {
        checkValidContent(SItemsParsing::toSItems, content, expected);
    }

    @Test
    void testBooleansSetParsing() {
        checkBooleansSetParsing("true", Arrays.asList(BooleanValue.TRUE));
        checkBooleansSetParsing("true, false", Arrays.asList(BooleanValue.TRUE, BooleanValue.FALSE));
    }

    @Test
    void testInvalidBooleanContent() {
        checkInvalidContent(SItemsParsing::toBooleanValues, "a", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toBooleanValues, ",", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toBooleanValues, ",true", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toBooleanValues, "~", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toBooleanValues, "true,,", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toBooleanValues, "true true", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toBooleanValues, "true false", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toBooleanValues, "false true", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toBooleanValues, "false false", Detail.UNEXPECTED_TOKEN);

        checkInvalidContent(SItemsParsing::toBooleanValues, "true, ", Detail.UNEXPECTED_END);

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         SItemsParsing.createRange(BooleanValue.TRUE, BooleanValue.TRUE);
                     });
    }

    @Test
    void testIntegersSetParsing() {
        checkIntegersSetParsing("", Arrays.asList());
        checkIntegersSetParsing("10", Arrays.asList(IntegerRange.of(10)));
        checkIntegersSetParsing("10~11", Arrays.asList(IntegerRange.of(10, 11)));
        checkIntegersSetParsing("10~11, 12", Arrays.asList(IntegerRange.of(10, 11), IntegerRange.of(12)));
        checkIntegersSetParsing("10,10", Arrays.asList(IntegerRange.of(10), IntegerRange.of(10)));
        checkIntegersSetParsing("11,10,11", Arrays.asList(IntegerRange.of(11), IntegerRange.of(10), IntegerRange.of(11)));
    }

    @Test
    void testInvalidIntegerContent() {
        checkInvalidContent(SItemsParsing::toIntegerRanges, "a", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toIntegerRanges, "10.0", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toIntegerRanges, ",", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toIntegerRanges, "~", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toIntegerRanges, "10,,", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toIntegerRanges, "10 10", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toIntegerRanges, "10~10.0", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toIntegerRanges, "10~20 10", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toIntegerRanges, "10~20~", Detail.UNEXPECTED_TOKEN);

        checkInvalidContent(SItemsParsing::toIntegerRanges, "10,", Detail.UNEXPECTED_END);
        checkInvalidContent(SItemsParsing::toIntegerRanges, "10~", Detail.UNEXPECTED_END);
        checkInvalidContent(SItemsParsing::toIntegerRanges, "10~20,", Detail.UNEXPECTED_END);
    }

    @Test
    void testRealsSetParsing() {
        checkRealsSetParsing("", Arrays.asList());
        checkRealsSetParsing("10.0", Arrays.asList(RealRange.of(10.0)));
        checkRealsSetParsing("10.0~11.0", Arrays.asList(RealRange.of(10.0, 11.0)));
        checkRealsSetParsing("10.0~11.0, 12.0", Arrays.asList(RealRange.of(10.0, 11.0), RealRange.of(12.0)));
    }

    @Test
    void testInvalidRealContent() {
        checkInvalidContent(SItemsParsing::toRealRanges, "a", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toRealRanges, "10", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toRealRanges, ",", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toRealRanges, "~", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toRealRanges, "10.0,,", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toRealRanges, "10.0 10.0", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toRealRanges, "10.0~10,", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toRealRanges, "10.0~20.0 10.0", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toRealRanges, "10.0~20.0~", Detail.UNEXPECTED_TOKEN);

        checkInvalidContent(SItemsParsing::toRealRanges, "10.0,", Detail.UNEXPECTED_END);
        checkInvalidContent(SItemsParsing::toRealRanges, "10.0~", Detail.UNEXPECTED_END);
        checkInvalidContent(SItemsParsing::toRealRanges, "10.0~20.0,", Detail.UNEXPECTED_END);
    }

    @Test
    void testStringsSetParsing() {
        checkStringsSetParsing("", Arrays.asList());
        checkStringsSetParsing("A", Arrays.asList(StringValue.of("A", false)));
        checkStringsSetParsing("\"A\"", Arrays.asList(StringValue.of("A", false)));
        checkStringsSetParsing("A,B", Arrays.asList(StringValue.of("A", false), StringValue.of("B", false)));
        checkStringsSetParsing("A,B,A", Arrays.asList(StringValue.of("A", false), StringValue.of("B", false)));
    }

    @Test
    void testInvalidStringContent() {
        checkInvalidContent(SItemsParsing::toStringValues, "10", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toStringValues, ",", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toStringValues, "~", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toStringValues, "A A", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toStringValues, "A,,", Detail.UNEXPECTED_TOKEN);

        checkInvalidContent(SItemsParsing::toStringValues, "A,", Detail.UNEXPECTED_END);
    }

    @Test
    void testUncheckedSetParsing() {
        checkUncheckedSetParsing("", Arrays.asList());
        checkUncheckedSetParsing("10.0, 10, 10  ~  20, 10.0  ~  20.0, A, true, \"and\", FALSE",
                                 Arrays.asList(RealValue.of(10.0),
                                               IntegerValue.of(10),
                                               IntegerRange.of(10, 20),
                                               RealRange.of(10.0, 20.0),
                                               StringValue.of("A", false),
                                               BooleanValue.TRUE,
                                               StringValue.of("and", false),
                                               BooleanValue.FALSE));
        checkUncheckedSetParsing("10  ~  20",
                                 Arrays.asList(IntegerRange.of(10, 20)));
    }

    @Test
    void testInvalidUncheckedContent() {
        checkInvalidContent(SItemsParsing::toSItems, "{", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toSItems, ",", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toSItems, "~", Detail.UNEXPECTED_TOKEN);
        checkInvalidContent(SItemsParsing::toSItems, "a a", Detail.UNEXPECTED_TOKEN);

        checkInvalidContent(SItemsParsing::toSItems, "a, 10~10.0", Detail.INVALID_RANGE);

        checkInvalidContent(SItemsParsing::toSItems, "A,", Detail.UNEXPECTED_END);
        checkInvalidContent(SItemsParsing::toSItems, "A~", Detail.UNEXPECTED_END);
    }
}