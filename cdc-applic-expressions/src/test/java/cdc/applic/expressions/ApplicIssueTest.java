package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.ApplicIssueType;
import cdc.issues.locations.DefaultLocation;

class ApplicIssueTest {
    private static ApplicIssue issue(ApplicIssueType type,
                                     String description) {
        return ApplicIssue.builder()
                          .name(type)
                          .location(new DefaultLocation("systemId", "path"))
                          .description(description)
                          .build();
    }

    private static void checkGetMostSevereType(ApplicIssueType expected,
                                               ApplicIssue... issues) {
        assertEquals(expected, ApplicIssue.getMostSevereType(Arrays.asList(issues)));
    }

    @Test
    void testGetMostSevereType() {
        final ApplicIssue no = issue(ApplicIssueType.NO_ISSUE, "No Issue");
        final ApplicIssue wri = issue(ApplicIssueType.WRITING_ISSUE, "Writing Issue");
        final ApplicIssue sem = issue(ApplicIssueType.SEMANTIC_ISSUE, "Semantic Issue");
        final ApplicIssue syn = issue(ApplicIssueType.SYNTACTIC_ISSUE, "Syntactic Issue");
        final ApplicIssue pol = issue(ApplicIssueType.DICTIONARY_ISSUE, "Dictionary Issue");

        checkGetMostSevereType(ApplicIssueType.NO_ISSUE);

        checkGetMostSevereType(ApplicIssueType.NO_ISSUE, no);
        checkGetMostSevereType(ApplicIssueType.WRITING_ISSUE, wri);
        checkGetMostSevereType(ApplicIssueType.SEMANTIC_ISSUE, sem);
        checkGetMostSevereType(ApplicIssueType.SYNTACTIC_ISSUE, syn);
        checkGetMostSevereType(ApplicIssueType.DICTIONARY_ISSUE, pol);

        checkGetMostSevereType(ApplicIssueType.NO_ISSUE, no, no);
        checkGetMostSevereType(ApplicIssueType.WRITING_ISSUE, no, wri);
        checkGetMostSevereType(ApplicIssueType.SEMANTIC_ISSUE, no, sem);
        checkGetMostSevereType(ApplicIssueType.SYNTACTIC_ISSUE, no, syn);
        checkGetMostSevereType(ApplicIssueType.DICTIONARY_ISSUE, no, pol);

        checkGetMostSevereType(ApplicIssueType.WRITING_ISSUE, wri, no);
        checkGetMostSevereType(ApplicIssueType.WRITING_ISSUE, wri, wri);
        checkGetMostSevereType(ApplicIssueType.SEMANTIC_ISSUE, wri, sem);
        checkGetMostSevereType(ApplicIssueType.SYNTACTIC_ISSUE, wri, syn);
        checkGetMostSevereType(ApplicIssueType.DICTIONARY_ISSUE, wri, pol);

        checkGetMostSevereType(ApplicIssueType.SEMANTIC_ISSUE, sem, no);
        checkGetMostSevereType(ApplicIssueType.SEMANTIC_ISSUE, sem, wri);
        checkGetMostSevereType(ApplicIssueType.SEMANTIC_ISSUE, sem, sem);
        checkGetMostSevereType(ApplicIssueType.SYNTACTIC_ISSUE, sem, syn);
        checkGetMostSevereType(ApplicIssueType.DICTIONARY_ISSUE, sem, pol);

        checkGetMostSevereType(ApplicIssueType.SYNTACTIC_ISSUE, syn, no);
        checkGetMostSevereType(ApplicIssueType.SYNTACTIC_ISSUE, syn, wri);
        checkGetMostSevereType(ApplicIssueType.SYNTACTIC_ISSUE, syn, sem);
        checkGetMostSevereType(ApplicIssueType.SYNTACTIC_ISSUE, syn, syn);
        checkGetMostSevereType(ApplicIssueType.DICTIONARY_ISSUE, syn, pol);

        checkGetMostSevereType(ApplicIssueType.DICTIONARY_ISSUE, pol, no);
        checkGetMostSevereType(ApplicIssueType.DICTIONARY_ISSUE, pol, wri);
        checkGetMostSevereType(ApplicIssueType.DICTIONARY_ISSUE, pol, sem);
        checkGetMostSevereType(ApplicIssueType.DICTIONARY_ISSUE, pol, syn);
        checkGetMostSevereType(ApplicIssueType.DICTIONARY_ISSUE, pol, pol);
    }
}