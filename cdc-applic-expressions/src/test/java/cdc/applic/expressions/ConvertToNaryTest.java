package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.expressions.literals.Name;

class ConvertToNaryTest {
    private static final Logger LOGGER = LogManager.getLogger(ConvertToNaryTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    private static void checkAlways(String expression,
                                    String expected) {
        final Expression e = new Expression(expression);
        final Node n = ConvertToNary.execute(e.getRootNode(), ConvertToNary.Variant.ALWAYS);
        assertEquals(expected, n.toString());
        final Node n1 = ConvertToNary.execute(n, ConvertToNary.Variant.ALWAYS);
        assertEquals(n, n1);
    }

    @Test
    void testAlways() {
        checkAlways("true", "TRUE");
        checkAlways("a or b", "NARY_OR(REF(a),REF(b))");
        checkAlways("a or b or c", "NARY_OR(REF(a),REF(b),REF(c))");
        checkAlways("a or b and c", "NARY_OR(REF(a),NARY_AND(REF(b),REF(c)))");
        checkAlways("a and b", "NARY_AND(REF(a),REF(b))");
        checkAlways("a and b and c", "NARY_AND(REF(a),REF(b),REF(c))");
        checkAlways("a or b -> c", "IMPLICATION(NARY_OR(REF(a),REF(b)),REF(c))");
        checkAlways("a or b <-> c", "EQUIVALENCE(NARY_OR(REF(a),REF(b)),REF(c))");
        checkAlways("a and (b or c)", "NARY_AND(REF(a),NARY_OR(REF(b),REF(c)))");
        checkAlways("a or (b and c)", "NARY_OR(REF(a),NARY_AND(REF(b),REF(c)))");
    }

    private static void checkAlwaysSpecial(Node node,
                                           String expected) {
        final Node n = ConvertToNary.execute(node, ConvertToNary.Variant.ALWAYS);
        assertEquals(expected, n.toString());
    }

    @Test
    void testAlwaysSpecial() {
        final RefNode a = new RefNode(Name.of("a", false));
        final RefNode b = new RefNode(Name.of("b", false));
        final RefNode c = new RefNode(Name.of("c", false));
        final RefNode d = new RefNode(Name.of("d", false));

        checkAlwaysSpecial(new NaryAndNode(a, b, c, d),
                           "NARY_AND(REF(a),REF(b),REF(c),REF(d))");

        checkAlwaysSpecial(new NaryAndNode(new NaryAndNode(a, b), new NaryAndNode(c, d)),
                           "NARY_AND(REF(a),REF(b),REF(c),REF(d))");

        checkAlwaysSpecial(new NaryAndNode(new AndNode(a, b), new NaryAndNode(c, d)),
                           "NARY_AND(REF(a),REF(b),REF(c),REF(d))");

        checkAlwaysSpecial(new NaryAndNode(new NaryAndNode(a, b), new AndNode(c, d)),
                           "NARY_AND(REF(a),REF(b),REF(c),REF(d))");

        checkAlwaysSpecial(new NaryAndNode(new AndNode(a, b), new AndNode(c, d)),
                           "NARY_AND(REF(a),REF(b),REF(c),REF(d))");

        checkAlwaysSpecial(new NaryOrNode(new NaryOrNode(a, b), new NaryOrNode(c, d)),
                           "NARY_OR(REF(a),REF(b),REF(c),REF(d))");

        checkAlwaysSpecial(new NaryAndNode(new AndNode(a, b), new AndNode(c, d)),
                           "NARY_AND(REF(a),REF(b),REF(c),REF(d))");

        checkAlwaysSpecial(new NaryAndNode(new AndNode(a, b), new OrNode(c, d)),
                           "NARY_AND(REF(a),REF(b),NARY_OR(REF(c),REF(d)))");

        checkAlwaysSpecial(new NaryAndNode(new AndNode(a, b), c),
                           "NARY_AND(REF(a),REF(b),REF(c))");
    }

    private static void checkWhenNecessary(String expression,
                                           String expected) {
        final Expression e = new Expression(expression);
        final Node n = ConvertToNary.execute(e.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY);
        assertEquals(expected, n.toString());
        final Node n1 = ConvertToNary.execute(n, ConvertToNary.Variant.WHEN_NECESSARY);
        assertEquals(n, n1);
    }

    @Test
    void testWhenNecessary() {
        checkWhenNecessary("true", "TRUE");
        checkWhenNecessary("a or b", "OR(REF(a),REF(b))");
        checkWhenNecessary("a or b or c", "NARY_OR(REF(a),REF(b),REF(c))");
        checkWhenNecessary("a and b", "AND(REF(a),REF(b))");
        checkWhenNecessary("a and b and c", "NARY_AND(REF(a),REF(b),REF(c))");
        checkWhenNecessary("a and (b or c)", "AND(REF(a),OR(REF(b),REF(c)))");
        checkWhenNecessary("a and (b or c or d)", "AND(REF(a),NARY_OR(REF(b),REF(c),REF(d)))");
        checkWhenNecessary("a or (b and c)", "OR(REF(a),AND(REF(b),REF(c)))");
        checkWhenNecessary("a or (b and c and d)", "OR(REF(a),NARY_AND(REF(b),REF(c),REF(d)))");
    }

    private static void checkWhenNecessarySpecial(Node node,
                                                  String expected) {
        final Node n = ConvertToNary.execute(node, ConvertToNary.Variant.WHEN_NECESSARY);
        assertEquals(expected, n.toString());
    }

    @Test
    void testWhenNecessarSpecial() {
        final RefNode a = new RefNode(Name.of("a", false));
        final RefNode b = new RefNode(Name.of("b", false));
        final RefNode c = new RefNode(Name.of("c", false));
        final RefNode d = new RefNode(Name.of("d", false));

        checkWhenNecessarySpecial(new NaryAndNode(a, b, c, d),
                                  "NARY_AND(REF(a),REF(b),REF(c),REF(d))");

        checkWhenNecessarySpecial(new NaryAndNode(new NaryAndNode(a, b), new NaryAndNode(c, d)),
                                  "NARY_AND(REF(a),REF(b),REF(c),REF(d))");

        checkWhenNecessarySpecial(new NaryAndNode(new AndNode(a, b), new NaryAndNode(c, d)),
                                  "NARY_AND(REF(a),REF(b),REF(c),REF(d))");

        checkWhenNecessarySpecial(new NaryAndNode(new NaryAndNode(a, b), new AndNode(c, d)),
                                  "NARY_AND(REF(a),REF(b),REF(c),REF(d))");

        checkWhenNecessarySpecial(new NaryAndNode(new AndNode(a, b), new AndNode(c, d)),
                                  "NARY_AND(REF(a),REF(b),REF(c),REF(d))");

        checkWhenNecessarySpecial(new NaryOrNode(new NaryOrNode(a, b), new NaryOrNode(c, d)),
                                  "NARY_OR(REF(a),REF(b),REF(c),REF(d))");

        checkWhenNecessarySpecial(new NaryAndNode(new AndNode(a, b), new AndNode(c, d)),
                                  "NARY_AND(REF(a),REF(b),REF(c),REF(d))");

        checkWhenNecessarySpecial(new NaryAndNode(new AndNode(a, b), new OrNode(c, d)),
                                  "NARY_AND(REF(a),REF(b),OR(REF(c),REF(d)))");

        checkWhenNecessarySpecial(new NaryAndNode(new AndNode(a, b), c),
                                  "NARY_AND(REF(a),REF(b),REF(c))");
    }

    @Test
    void testPrint() {
        final Expression e = new Expression("a or b or c");
        final Node n = ConvertToNary.execute(e.getRootNode(), ConvertToNary.Variant.ALWAYS);
        n.print(OUT);
        assertTrue(true);
    }
}