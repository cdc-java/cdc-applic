package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.UncheckedSet;

class IntegerValueTest {
    private static void testConstructor(int value,
                                        String literal) {
        final IntegerValue fromValue = IntegerValue.of(value);
        final IntegerValue fromLiteral = IntegerValue.of(literal);

        assertEquals(fromValue, fromLiteral);
        assertEquals(value, fromValue.getNumber());
        assertEquals(value, fromLiteral.getNumber());
        assertNotEquals(value, fromValue);
        assertNotEquals(value, fromLiteral);

        assertTrue(fromValue.isCompliantWith(IntegerSet.class));
        assertTrue(fromValue.isCompliantWith(UncheckedSet.class));
        assertFalse(fromValue.isCompliantWith(BooleanSet.class));
        assertFalse(fromValue.isCompliantWith(RealSet.class));
        assertFalse(fromValue.isCompliantWith(StringSet.class));
    }

    @Test
    void testConstructor() {
        testConstructor(1, "1");
        testConstructor(-1, "-1");
    }

    @Test
    void testInvalidArg() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         IntegerValue.of("x");
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         IntegerValue.of("1.0");
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         IntegerValue.of("1 ");
                     });
    }

    @Test
    void testEquals() {
        final IntegerValue zero = IntegerValue.of(0);
        final IntegerValue one = IntegerValue.of(1);
        assertEquals(zero, zero);
        assertEquals(one, one);
        assertNotEquals(zero, one);
        assertNotEquals(one, zero);
        assertNotEquals(one, null);
        assertNotEquals(one, "1");
    }

    @Test
    void testGetProtectedLiteral() {
        assertEquals("1", IntegerValue.of(1).getProtectedLiteral());
    }

    @Test
    void testToString() {
        assertEquals("1", IntegerValue.of(1).toString());
    }

    @Test
    void testHashCode() {
        final IntegerValue zero = IntegerValue.of(0);
        final IntegerValue one = IntegerValue.of(1);
        assertNotEquals(zero.hashCode(), one.hashCode());
    }
}