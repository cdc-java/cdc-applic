package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.parsing.Recognizer;

class RecognizerTest {
    private final Recognizer recognizer = new Recognizer();

    @Test
    void testValid() {
        assertTrue(recognizer.isValid("Alias"));
        assertTrue(recognizer.isValid("Version = V1"));
        assertTrue(recognizer.isValid("Version = IN"));
        assertTrue(recognizer.isValid("Version in {}"));
        assertTrue(recognizer.isValid("Version in {IN}"));
        assertTrue(recognizer.isValid("Version in {V1}"));
        assertTrue(recognizer.isValid("Version in {V1, V2}"));
        assertTrue(recognizer.isValid("Version in {1, 2, 3~4}"));
        assertTrue(recognizer.isValid("Version = V1 or Version = V2"));
        assertTrue(recognizer.isValid("Version = V1 or Version = V2 and Standard = S1"));
        assertTrue(recognizer.isValid("M1000 = TRUE"));
        assertTrue(recognizer.isValid("TRUE"));
        assertTrue(recognizer.isValid("FALSE"));
        assertTrue(recognizer.isValid("(FALSE)"));
        assertTrue(recognizer.isValid("Version = V1 or true"));
        assertTrue(recognizer.isValid("A -> B"));
        assertTrue(recognizer.isValid("A <-> B"));
        assertTrue(recognizer.isValid("\"A A\" <-> B"));
        assertTrue(recognizer.isValid("\"A Property\" = \"A Value\""));
        assertTrue(recognizer.isValid("\"A Property\" = \"A \"\"Value\""));
        assertTrue(recognizer.isValid("\"A Property\" in { \"A Value\" }"));
        assertTrue(recognizer.isValid("\"A Property\" in { \"A Value\", \"Another Value\" }"));
        assertTrue(recognizer.isValid("\"A Property\" = \" \""));
        assertTrue(recognizer.isValid("Temp = 1.0"));
        assertTrue(recognizer.isValid("Temp != 1.0"));
        assertTrue(recognizer.isValid("Temp in  {1.0, 2.0}"));
        assertTrue(recognizer.isValid("Temp in {1.0, 2.0, 3.0~4.0}"));
        assertTrue(recognizer.isValid("Temp not in {1.0, 2.0, 3.0~4.0}"));
        assertTrue(recognizer.isValid("(Value = A)"));
        assertTrue(recognizer.isValid("(Value)"));
        assertTrue(recognizer.isValid("not Value"));
        assertTrue(recognizer.isValid("! Value"));

        assertTrue(recognizer.isValid("A > B"));
        assertTrue(recognizer.isValid("A > 1"));
        assertTrue(recognizer.isValid("A > 1.0"));
        assertTrue(recognizer.isValid("A >= B"));
        assertTrue(recognizer.isValid("A >= 1"));
        assertTrue(recognizer.isValid("A >= 1.0"));

        assertTrue(recognizer.isValid("A !> B"));
        assertTrue(recognizer.isValid("A !> 1"));
        assertTrue(recognizer.isValid("A !> 1.0"));
        assertTrue(recognizer.isValid("A !>= B"));
        assertTrue(recognizer.isValid("A !>= 1"));
        assertTrue(recognizer.isValid("A !>= 1.0"));

        assertTrue(recognizer.isValid("A < B"));
        assertTrue(recognizer.isValid("A < 1"));
        assertTrue(recognizer.isValid("A < 1.0"));
        assertTrue(recognizer.isValid("A <= B"));
        assertTrue(recognizer.isValid("A <= 1"));
        assertTrue(recognizer.isValid("A <= 1.0"));

        assertTrue(recognizer.isValid("A !< B"));
        assertTrue(recognizer.isValid("A !< 1"));
        assertTrue(recognizer.isValid("A !< 1.0"));
        assertTrue(recognizer.isValid("A !<= B"));
        assertTrue(recognizer.isValid("A !<= 1"));
        assertTrue(recognizer.isValid("A !<= 1.0"));

        assertTrue(recognizer.isValid("Value in ∅"));
        assertTrue(recognizer.isValid("Value not in ∅"));
        assertTrue(recognizer.isValid("Value not in ∅ or true"));

        assertTrue(recognizer.isValid("A = AND"));
        assertTrue(recognizer.isValid("A = IFF"));
        assertTrue(recognizer.isValid("A = IMP"));
        assertTrue(recognizer.isValid("A = IN"));
        assertTrue(recognizer.isValid("A = NOT"));
        assertTrue(recognizer.isValid("A = OR"));
        assertTrue(recognizer.isValid("A = TO"));

        assertTrue(recognizer.isValid("A IN {AND,IFF,IMP,IN,NOT,OR,TO}"));
        assertTrue(recognizer.isValid("A IN {10 TO 20}"));
        assertTrue(recognizer.isValid("A IN {10 TO 20}"));
    }

    @Test
    void testInvalid() {
        assertFalse(recognizer.isValid("∅"));
        assertFalse(recognizer.isValid("Value = ∅"));
        assertFalse(recognizer.isValid("(Value = A"));
        assertFalse(recognizer.isValid("(Version = V1"));
        assertFalse(recognizer.isValid("Value = A)"));
        assertFalse(recognizer.isValid("Value ="));
        assertFalse(recognizer.isValid("Value =)"));
        assertFalse(recognizer.isValid("Value)"));
        assertFalse(recognizer.isValid("Value in { A to B }"));
        assertFalse(recognizer.isValid("Value in { 1-B }"));
        assertFalse(recognizer.isValid("Value in { 1-BBBB }"));
        assertFalse(recognizer.isValid("Version in {M}}"));
        assertFalse(recognizer.isValid("Version in {M"));
        assertFalse(recognizer.isValid("()"));
        assertFalse(recognizer.isValid("(FALSE))"));
        assertFalse(recognizer.isValid("((FALSE)"));
        assertFalse(recognizer.isValid("FALSE = FALSE"));
        assertFalse(recognizer.isValid("XXX = "));
        assertFalse(recognizer.isValid("XXX = {}"));
        assertFalse(recognizer.isValid("XXX in {,}"));
        assertFalse(recognizer.isValid("XXX in {A,}"));
        assertFalse(recognizer.isValid("\"A Property\" = \"A \"Value\""));
        assertFalse(recognizer.isValid("\"A Property\" in {,}"));
        assertFalse(recognizer.isValid("\"A Property\" in {\"\"}"));
        assertFalse(recognizer.isValid("\"A Property\" in {A,}"));
        assertFalse(recognizer.isValid("\"A Property\" in { \"A \"Value\" }"));
        assertFalse(recognizer.isValid("\"A Property\" in { \"A Value\", }"));
        assertFalse(recognizer.isValid("\"A Property\" in { \"A Value\", \"}"));
        assertFalse(recognizer.isValid("\"A Property\" in { \"A Value\", \"\"}"));
    }

    @Test
    void testPrefix() {
        assertTrue(recognizer.isValid("P.A"));
        assertTrue(recognizer.isValid("P.A > B"));
        assertTrue(recognizer.isValid("A >= P.B"));
    }
}