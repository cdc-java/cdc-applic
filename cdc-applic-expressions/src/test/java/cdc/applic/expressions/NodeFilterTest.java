package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.function.Predicate;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NodePredicates;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.expressions.ast.visitors.NodeFilter;

class NodeFilterTest {
    private static void test(String expected,
                             String expression,
                             Predicate<? super Node> predicate) {
        final Expression x = new Expression(expression);
        final Node node =
                NodeFilter.execute(ConvertToNary.execute(x.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY), predicate);
        assertEquals(expected, node == null ? null : node.toInfix(Formatting.SHORT_NARROW));
    }

    private static void testRemoveNot(String expected,
                                      String expression) {
        test(expected, expression, NodePredicates.IS_NOT.negate());
    }

    private static void testRemoveTrue(String expected,
                                       String expression) {
        test(expected, expression, NodePredicates.IS_TRUE.negate());
    }

    private static void testRemoveOr(String expected,
                                     String expression) {
        test(expected, expression, n -> !(n instanceof OrNode || n instanceof NaryOrNode));
    }

    @Test
    void testRemoveNot() {
        testRemoveNot("true", "true");
        testRemoveNot("A", "A");
        testRemoveNot("A&B", "A and B");
        testRemoveNot("B", "not A and B");
        testRemoveNot(null, "not A and not B");
        testRemoveNot(null, "not not A and not B");
        testRemoveNot(null, "not (not A and not B)");
    }

    @Test
    void testRemoveTrue() {
        testRemoveTrue(null, "true");
        testRemoveTrue(null, "! true");
        testRemoveTrue("!false", "! (true and false)");
        testRemoveTrue("false&false", "false and false");
        testRemoveTrue("false", "false and true");
        testRemoveTrue("false", "true and false");
        testRemoveTrue("false->false", "false imp false");
        testRemoveTrue("!false", "false imp true");
        testRemoveTrue("false", "true imp false");
        testRemoveTrue("false<->false", "false iff false");
        testRemoveTrue("false&!false", "false iff true");
        testRemoveTrue("false&!false", "true iff false");
        testRemoveTrue("false", "true and false and true");
        testRemoveTrue(null, "true and true and true");
    }

    @Test
    void testRemoveOr() {
        testRemoveOr("true", "true");
        testRemoveOr("true&true", "true and true");
        testRemoveOr(null, "true or true");
        testRemoveOr(null, "true or true or true");
    }
}