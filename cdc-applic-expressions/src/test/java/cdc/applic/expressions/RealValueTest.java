package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.UncheckedSet;

class RealValueTest {
    private static void testConstructor(double value,
                                        String literal) {
        final RealValue fromValue = RealValue.of(value);
        final RealValue fromLiteral = RealValue.of(literal);

        assertEquals(fromValue, fromLiteral);
        assertEquals(value, fromValue.getNumber());
        assertEquals(value, fromLiteral.getNumber());
        assertNotEquals(value, fromValue);
        assertNotEquals(value, fromLiteral);

        assertTrue(fromValue.isCompliantWith(RealSet.class));
        assertTrue(fromValue.isCompliantWith(UncheckedSet.class));
        assertFalse(fromValue.isCompliantWith(BooleanSet.class));
        assertFalse(fromValue.isCompliantWith(IntegerSet.class));
        assertFalse(fromValue.isCompliantWith(StringSet.class));
    }

    @Test
    void testConstructor() {
        testConstructor(0.0, "0.0");
        testConstructor(0.0, "-0.0");
        testConstructor(1.0, "1.0");
        testConstructor(-1.0, "-1.0");
    }

    @Test
    void testInvalidArg() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RealValue.of("x");
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RealValue.of("1");
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RealValue.of("1.0 ");
                     });
    }

    @Test
    void testEquals() {
        final RealValue zero = RealValue.of(0.0);
        final RealValue one = RealValue.of(1.0);
        assertEquals(zero, zero);
        assertEquals(one, one);
        assertNotEquals(zero, one);
        assertNotEquals(one, zero);
        assertNotEquals(one, null);
        assertNotEquals(one, "1.0");
    }

    @Test
    void testGetProtectedLiteral() {
        assertEquals("1.0", RealValue.of(1.0).getProtectedLiteral());
    }

    @Test
    void testToString() {
        assertEquals("1.0", RealValue.of(1.0).toString());
    }

    @Test
    void testHashCode() {
        final RealValue zero = RealValue.of(0.0);
        final RealValue one = RealValue.of(1.0);
        assertNotEquals(zero.hashCode(), one.hashCode());
    }
}