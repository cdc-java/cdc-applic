package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.checks.CheckedData;

class CheckedDataTest {
    @Test
    void test() {
        final Expression x = new Expression("A");
        final Node n = x.getRootNode();
        final CheckedData cd1 = new CheckedData(x);
        final CheckedData cd2 = new CheckedData(n);

        assertEquals(x, cd1.getExpression());
        assertEquals(n, cd1.getNode());
        assertEquals(null, cd2.getExpression());
        assertEquals(n, cd2.getNode());
    }
}