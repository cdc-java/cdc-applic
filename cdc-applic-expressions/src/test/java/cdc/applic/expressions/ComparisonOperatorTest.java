package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.parsing.ComparisonOperator;

class ComparisonOperatorTest {
    @Test
    void testNegate() {
        assertEquals(ComparisonOperator.EQUAL, ComparisonOperator.NOT_EQUAL.negate());
        assertEquals(ComparisonOperator.NOT_EQUAL, ComparisonOperator.EQUAL.negate());
        assertEquals(ComparisonOperator.LESS, ComparisonOperator.NOT_LESS.negate());
        assertEquals(ComparisonOperator.NOT_LESS, ComparisonOperator.LESS.negate());
        assertEquals(ComparisonOperator.GREATER, ComparisonOperator.NOT_GREATER.negate());
        assertEquals(ComparisonOperator.NOT_GREATER, ComparisonOperator.GREATER.negate());
        assertEquals(ComparisonOperator.LESS_OR_EQUAL, ComparisonOperator.NEITHER_LESS_NOR_EQUAL.negate());
        assertEquals(ComparisonOperator.NEITHER_LESS_NOR_EQUAL, ComparisonOperator.LESS_OR_EQUAL.negate());
        assertEquals(ComparisonOperator.GREATER_OR_EQUAL, ComparisonOperator.NEITHER_GREATER_NOR_EQUAL.negate());
        assertEquals(ComparisonOperator.NEITHER_GREATER_NOR_EQUAL, ComparisonOperator.GREATER_OR_EQUAL.negate());
    }

    @Test
    void testIsNegative() {
        assertFalse(ComparisonOperator.EQUAL.isNegative());
        assertTrue(ComparisonOperator.NOT_EQUAL.isNegative());
        assertFalse(ComparisonOperator.LESS.isNegative());
        assertTrue(ComparisonOperator.NOT_LESS.isNegative());
        assertFalse(ComparisonOperator.GREATER.isNegative());
        assertTrue(ComparisonOperator.NOT_GREATER.isNegative());
        assertFalse(ComparisonOperator.LESS_OR_EQUAL.isNegative());
        assertTrue(ComparisonOperator.NEITHER_LESS_NOR_EQUAL.isNegative());
        assertFalse(ComparisonOperator.GREATER_OR_EQUAL.isNegative());
        assertTrue(ComparisonOperator.NEITHER_GREATER_NOR_EQUAL.isNegative());
    }

    @Test
    void testIncludesEqual() {
        assertTrue(ComparisonOperator.EQUAL.includesEqual());
        assertFalse(ComparisonOperator.NOT_EQUAL.includesEqual());
        assertFalse(ComparisonOperator.LESS.includesEqual());
        assertTrue(ComparisonOperator.NOT_LESS.includesEqual());
        assertFalse(ComparisonOperator.GREATER.includesEqual());
        assertTrue(ComparisonOperator.NOT_GREATER.includesEqual());
        assertTrue(ComparisonOperator.LESS_OR_EQUAL.includesEqual());
        assertFalse(ComparisonOperator.NEITHER_LESS_NOR_EQUAL.includesEqual());
        assertTrue(ComparisonOperator.GREATER_OR_EQUAL.includesEqual());
        assertFalse(ComparisonOperator.NEITHER_GREATER_NOR_EQUAL.includesEqual());
    }

    @Test
    void testIncludesLess() {
        assertFalse(ComparisonOperator.EQUAL.includesLess());
        assertTrue(ComparisonOperator.NOT_EQUAL.includesLess());
        assertTrue(ComparisonOperator.LESS.includesLess());
        assertFalse(ComparisonOperator.NOT_LESS.includesLess());
        assertFalse(ComparisonOperator.GREATER.includesLess());
        assertTrue(ComparisonOperator.NOT_GREATER.includesLess());
        assertTrue(ComparisonOperator.LESS_OR_EQUAL.includesLess());
        assertFalse(ComparisonOperator.NEITHER_LESS_NOR_EQUAL.includesLess());
        assertFalse(ComparisonOperator.GREATER_OR_EQUAL.includesLess());
        assertTrue(ComparisonOperator.NEITHER_GREATER_NOR_EQUAL.includesLess());
    }

    @Test
    void testIncludesGreater() {
        assertFalse(ComparisonOperator.EQUAL.includesGreater());
        assertTrue(ComparisonOperator.NOT_EQUAL.includesGreater());
        assertFalse(ComparisonOperator.LESS.includesGreater());
        assertTrue(ComparisonOperator.NOT_LESS.includesGreater());
        assertTrue(ComparisonOperator.GREATER.includesGreater());
        assertFalse(ComparisonOperator.NOT_GREATER.includesGreater());
        assertFalse(ComparisonOperator.LESS_OR_EQUAL.includesGreater());
        assertTrue(ComparisonOperator.NEITHER_LESS_NOR_EQUAL.includesGreater());
        assertTrue(ComparisonOperator.GREATER_OR_EQUAL.includesGreater());
        assertFalse(ComparisonOperator.NEITHER_GREATER_NOR_EQUAL.includesGreater());
    }

    @Test
    void testIncludesUnrelated() {
        assertFalse(ComparisonOperator.EQUAL.includesUnrelated());
        assertTrue(ComparisonOperator.NOT_EQUAL.includesUnrelated());
        assertFalse(ComparisonOperator.LESS.includesUnrelated());
        assertTrue(ComparisonOperator.NOT_LESS.includesUnrelated());
        assertFalse(ComparisonOperator.GREATER.includesUnrelated());
        assertTrue(ComparisonOperator.NOT_GREATER.includesUnrelated());
        assertFalse(ComparisonOperator.LESS_OR_EQUAL.includesUnrelated());
        assertTrue(ComparisonOperator.NEITHER_LESS_NOR_EQUAL.includesUnrelated());
        assertFalse(ComparisonOperator.GREATER_OR_EQUAL.includesUnrelated());
        assertTrue(ComparisonOperator.NEITHER_GREATER_NOR_EQUAL.includesUnrelated());
    }
}