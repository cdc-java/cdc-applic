package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.EquivalenceNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.ImplicationNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.NodePredicates;
import cdc.applic.expressions.ast.NotEqualNode;
import cdc.applic.expressions.ast.NotInNode;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.ValueUtils;
import cdc.applic.expressions.literals.Name;

class NodePredicatesTest {
    private final AndNode nBinaryAnd = new AndNode(TrueNode.INSTANCE, FalseNode.INSTANCE);
    private final OrNode nBinaryOr = new OrNode(TrueNode.INSTANCE, FalseNode.INSTANCE);
    private final EqualNode nEqual = new EqualNode(Name.of("Prop", false), ValueUtils.create("1"));
    private final EquivalenceNode nEquivalence = new EquivalenceNode(TrueNode.INSTANCE, FalseNode.INSTANCE);
    private final FalseNode nFalse = FalseNode.INSTANCE;
    private final ImplicationNode nImplication = new ImplicationNode(TrueNode.INSTANCE, FalseNode.INSTANCE);
    private final InNode nIn = new InNode(Name.of("Prop", false), BooleanSet.TRUE);
    private final NaryAndNode nNaryAnd = new NaryAndNode(TrueNode.INSTANCE, nFalse);
    private final NaryOrNode nNaryOr = new NaryOrNode(TrueNode.INSTANCE, nFalse);
    private final NotNode nNot = new NotNode(TrueNode.INSTANCE);
    private final NotEqualNode nNotEqual = new NotEqualNode(Name.of("Prop", false), ValueUtils.create("1"));
    private final NotInNode nNotIn = new NotInNode(Name.of("Prop", false), BooleanSet.TRUE);
    private final RefNode nRef = new RefNode(Name.of("Ref", false));
    private final TrueNode nTrue = TrueNode.INSTANCE;

    @Test
    void testIsUnary() {
        assertFalse(NodePredicates.IS_UNARY.test(nBinaryAnd));
        assertFalse(NodePredicates.IS_UNARY.test(nBinaryOr));
        assertFalse(NodePredicates.IS_UNARY.test(nEqual));
        assertFalse(NodePredicates.IS_UNARY.test(nEquivalence));
        assertFalse(NodePredicates.IS_UNARY.test(nFalse));
        assertFalse(NodePredicates.IS_UNARY.test(nImplication));
        assertFalse(NodePredicates.IS_UNARY.test(nIn));
        assertFalse(NodePredicates.IS_UNARY.test(nNaryAnd));
        assertFalse(NodePredicates.IS_UNARY.test(nNaryOr));
        assertTrue(NodePredicates.IS_UNARY.test(nNot));
        assertFalse(NodePredicates.IS_UNARY.test(nNotEqual));
        assertFalse(NodePredicates.IS_UNARY.test(nNotIn));
        assertFalse(NodePredicates.IS_UNARY.test(nRef));
        assertFalse(NodePredicates.IS_UNARY.test(nTrue));
    }

    @Test
    void testIsBinary() {
        assertTrue(NodePredicates.IS_BINARY.test(nBinaryAnd));
        assertTrue(NodePredicates.IS_BINARY.test(nBinaryOr));
        assertFalse(NodePredicates.IS_BINARY.test(nEqual));
        assertTrue(NodePredicates.IS_BINARY.test(nEquivalence));
        assertFalse(NodePredicates.IS_BINARY.test(nFalse));
        assertTrue(NodePredicates.IS_BINARY.test(nImplication));
        assertFalse(NodePredicates.IS_BINARY.test(nIn));
        assertFalse(NodePredicates.IS_BINARY.test(nNaryAnd));
        assertFalse(NodePredicates.IS_BINARY.test(nNaryOr));
        assertFalse(NodePredicates.IS_BINARY.test(nNot));
        assertFalse(NodePredicates.IS_BINARY.test(nNotEqual));
        assertFalse(NodePredicates.IS_BINARY.test(nNotIn));
        assertFalse(NodePredicates.IS_BINARY.test(nRef));
        assertFalse(NodePredicates.IS_BINARY.test(nTrue));
    }

    @Test
    void testIsOperator() {
        assertTrue(NodePredicates.IS_OPERATOR.test(nBinaryAnd));
        assertTrue(NodePredicates.IS_OPERATOR.test(nBinaryOr));
        assertFalse(NodePredicates.IS_OPERATOR.test(nEqual));
        assertTrue(NodePredicates.IS_OPERATOR.test(nEquivalence));
        assertFalse(NodePredicates.IS_OPERATOR.test(nFalse));
        assertTrue(NodePredicates.IS_OPERATOR.test(nImplication));
        assertFalse(NodePredicates.IS_OPERATOR.test(nIn));
        assertTrue(NodePredicates.IS_OPERATOR.test(nNaryAnd));
        assertTrue(NodePredicates.IS_OPERATOR.test(nNaryOr));
        assertTrue(NodePredicates.IS_OPERATOR.test(nNot));
        assertFalse(NodePredicates.IS_OPERATOR.test(nNotEqual));
        assertFalse(NodePredicates.IS_OPERATOR.test(nNotIn));
        assertFalse(NodePredicates.IS_OPERATOR.test(nRef));
        assertFalse(NodePredicates.IS_OPERATOR.test(nTrue));
    }

    @Test
    void testIsNaryAnd() {
        assertFalse(NodePredicates.IS_NARY_AND.test(nBinaryAnd));
        assertFalse(NodePredicates.IS_NARY_AND.test(nBinaryOr));
        assertFalse(NodePredicates.IS_NARY_AND.test(nEqual));
        assertFalse(NodePredicates.IS_NARY_AND.test(nEquivalence));
        assertFalse(NodePredicates.IS_NARY_AND.test(nFalse));
        assertFalse(NodePredicates.IS_NARY_AND.test(nImplication));
        assertFalse(NodePredicates.IS_NARY_AND.test(nIn));
        assertTrue(NodePredicates.IS_NARY_AND.test(nNaryAnd));
        assertFalse(NodePredicates.IS_NARY_AND.test(nNaryOr));
        assertFalse(NodePredicates.IS_NARY_AND.test(nNot));
        assertFalse(NodePredicates.IS_NARY_AND.test(nNotEqual));
        assertFalse(NodePredicates.IS_NARY_AND.test(nNotIn));
        assertFalse(NodePredicates.IS_NARY_AND.test(nRef));
        assertFalse(NodePredicates.IS_NARY_AND.test(nTrue));
    }

    @Test
    void testIsNaryOr() {
        assertFalse(NodePredicates.IS_NARY_OR.test(nBinaryAnd));
        assertFalse(NodePredicates.IS_NARY_OR.test(nBinaryOr));
        assertFalse(NodePredicates.IS_NARY_OR.test(nEqual));
        assertFalse(NodePredicates.IS_NARY_OR.test(nEquivalence));
        assertFalse(NodePredicates.IS_NARY_OR.test(nFalse));
        assertFalse(NodePredicates.IS_NARY_OR.test(nImplication));
        assertFalse(NodePredicates.IS_NARY_OR.test(nIn));
        assertFalse(NodePredicates.IS_NARY_OR.test(nNaryAnd));
        assertTrue(NodePredicates.IS_NARY_OR.test(nNaryOr));
        assertFalse(NodePredicates.IS_NARY_OR.test(nNot));
        assertFalse(NodePredicates.IS_NARY_OR.test(nNotEqual));
        assertFalse(NodePredicates.IS_NARY_OR.test(nNotIn));
        assertFalse(NodePredicates.IS_NARY_OR.test(nRef));
        assertFalse(NodePredicates.IS_NARY_OR.test(nTrue));
    }

    @Test
    void testIsImplication() {
        assertFalse(NodePredicates.IS_IMPLICATION.test(nBinaryAnd));
        assertFalse(NodePredicates.IS_IMPLICATION.test(nBinaryOr));
        assertFalse(NodePredicates.IS_IMPLICATION.test(nEqual));
        assertFalse(NodePredicates.IS_IMPLICATION.test(nEquivalence));
        assertFalse(NodePredicates.IS_IMPLICATION.test(nFalse));
        assertTrue(NodePredicates.IS_IMPLICATION.test(nImplication));
        assertFalse(NodePredicates.IS_IMPLICATION.test(nIn));
        assertFalse(NodePredicates.IS_IMPLICATION.test(nNaryAnd));
        assertFalse(NodePredicates.IS_IMPLICATION.test(nNaryOr));
        assertFalse(NodePredicates.IS_IMPLICATION.test(nNot));
        assertFalse(NodePredicates.IS_IMPLICATION.test(nNotEqual));
        assertFalse(NodePredicates.IS_IMPLICATION.test(nNotIn));
        assertFalse(NodePredicates.IS_IMPLICATION.test(nRef));
        assertFalse(NodePredicates.IS_IMPLICATION.test(nTrue));
    }

    @Test
    void testIsEquivalence() {
        assertFalse(NodePredicates.IS_EQUIVALENCE.test(nBinaryAnd));
        assertFalse(NodePredicates.IS_EQUIVALENCE.test(nBinaryOr));
        assertFalse(NodePredicates.IS_EQUIVALENCE.test(nEqual));
        assertTrue(NodePredicates.IS_EQUIVALENCE.test(nEquivalence));
        assertFalse(NodePredicates.IS_EQUIVALENCE.test(nFalse));
        assertFalse(NodePredicates.IS_EQUIVALENCE.test(nImplication));
        assertFalse(NodePredicates.IS_EQUIVALENCE.test(nIn));
        assertFalse(NodePredicates.IS_EQUIVALENCE.test(nNaryAnd));
        assertFalse(NodePredicates.IS_EQUIVALENCE.test(nNaryOr));
        assertFalse(NodePredicates.IS_EQUIVALENCE.test(nNot));
        assertFalse(NodePredicates.IS_EQUIVALENCE.test(nNotEqual));
        assertFalse(NodePredicates.IS_EQUIVALENCE.test(nNotIn));
        assertFalse(NodePredicates.IS_EQUIVALENCE.test(nRef));
        assertFalse(NodePredicates.IS_EQUIVALENCE.test(nTrue));
    }

    @Test
    void testIsImplicationOrEquivalence() {
        assertFalse(NodePredicates.IS_IMPLICATION_OR_EQUIVALENCE.test(nBinaryAnd));
        assertFalse(NodePredicates.IS_IMPLICATION_OR_EQUIVALENCE.test(nBinaryOr));
        assertFalse(NodePredicates.IS_IMPLICATION_OR_EQUIVALENCE.test(nEqual));
        assertTrue(NodePredicates.IS_IMPLICATION_OR_EQUIVALENCE.test(nEquivalence));
        assertFalse(NodePredicates.IS_IMPLICATION_OR_EQUIVALENCE.test(nFalse));
        assertTrue(NodePredicates.IS_IMPLICATION_OR_EQUIVALENCE.test(nImplication));
        assertFalse(NodePredicates.IS_IMPLICATION_OR_EQUIVALENCE.test(nIn));
        assertFalse(NodePredicates.IS_IMPLICATION_OR_EQUIVALENCE.test(nNaryAnd));
        assertFalse(NodePredicates.IS_IMPLICATION_OR_EQUIVALENCE.test(nNaryOr));
        assertFalse(NodePredicates.IS_IMPLICATION_OR_EQUIVALENCE.test(nNot));
        assertFalse(NodePredicates.IS_IMPLICATION_OR_EQUIVALENCE.test(nNotEqual));
        assertFalse(NodePredicates.IS_IMPLICATION_OR_EQUIVALENCE.test(nNotIn));
        assertFalse(NodePredicates.IS_IMPLICATION_OR_EQUIVALENCE.test(nRef));
        assertFalse(NodePredicates.IS_IMPLICATION_OR_EQUIVALENCE.test(nTrue));
    }

    @Test
    void testIsNegative() {
        assertFalse(NodePredicates.IS_NEGATIVE.test(nBinaryAnd));
        assertFalse(NodePredicates.IS_NEGATIVE.test(nBinaryOr));
        assertFalse(NodePredicates.IS_NEGATIVE.test(nEqual));
        assertFalse(NodePredicates.IS_NEGATIVE.test(nEquivalence));
        assertTrue(NodePredicates.IS_NEGATIVE.test(nFalse));
        assertFalse(NodePredicates.IS_NEGATIVE.test(nImplication));
        assertFalse(NodePredicates.IS_NEGATIVE.test(nIn));
        assertFalse(NodePredicates.IS_NEGATIVE.test(nNaryAnd));
        assertFalse(NodePredicates.IS_NEGATIVE.test(nNaryOr));
        assertTrue(NodePredicates.IS_NEGATIVE.test(nNot));
        assertTrue(NodePredicates.IS_NEGATIVE.test(nNotEqual));
        assertTrue(NodePredicates.IS_NEGATIVE.test(nNotIn));
        assertFalse(NodePredicates.IS_NEGATIVE.test(nRef));
        assertFalse(NodePredicates.IS_NEGATIVE.test(nTrue));
    }
}