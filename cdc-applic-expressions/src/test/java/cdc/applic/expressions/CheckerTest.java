package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.Checker;

class CheckerTest {
    private static final Checker CHECKER = new Checker() {
        @Override
        public String getRuleName() {
            return "RULE";
        }

        @Override
        public boolean isCompliant(CheckedData data) {
            return true;
        }

        @Override
        public void checkCompliance(CheckedData data) {
            // Ignore
        }

        @Override
        public void check(CheckedData data,
                          List<ApplicIssue> issues) {
            // Ignore
        }
    };

    @Test
    void test() {
        // Code coverage
        final List<ApplicIssue> issues = new ArrayList<>();
        CHECKER.check(new CheckedData(TrueNode.INSTANCE), issues);
        CHECKER.isCompliant(TrueNode.INSTANCE);

        assertEquals("'Hello'", Checker.wrap("Hello"));
    }
}