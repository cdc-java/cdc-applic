package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.expressions.ast.visitors.DistributeAndOverOr;

class DistributeAndOverOrTest {
    private static void check(String expression,
                              String expected) {
        final Expression x = new Expression(expression);
        final Expression x1 = DistributeAndOverOr.execute(x.getRootNode())
                                                 .toExpression(Formatting.SHORT_NARROW);
        assertEquals(expected, x1.toString());
    }

    @Test
    void test() {
        check("", "true");
        check("A and B", "A&B");
        check("A or B", "A|B");
        check("(A and B) or C", "A&B|C");
        check("(A or B) and C", "A&C|B&C");
        check("A and (B or C)", "A&B|A&C");
        check("A or (B and C)", "A|B&C");

        check("(A and B) or (C and D)", "A&B|C&D");
        check("(A or B) and (C or D)", "A&C|B&C|A&D|B&D");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         check("A <-> B", null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         check("A -> B", null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         final Expression x = new Expression("A & B");
                         DistributeAndOverOr.execute(ConvertToNary.execute(x.getRootNode(), ConvertToNary.Variant.ALWAYS));
                     });
    }
}