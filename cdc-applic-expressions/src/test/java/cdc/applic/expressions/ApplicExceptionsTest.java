package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class ApplicExceptionsTest {
    @Test
    void test() {
        // Code coverage
        new ApplicException("Exception", null);
        new ImplementationException("Exception");
        new IllegalOperationException("Exception");
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         new SyntacticException(SyntacticException.Detail.INVALID_RANGE,
                                                "",
                                                "",
                                                -1,
                                                0);
                     });

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         new LexicalException(LexicalException.Detail.EMPTY_DOUBLE_QUOTES_ESCAPED_TEXT,
                                              "",
                                              "",
                                              -1,
                                              0);
                     });

        final SyntacticException x = new SyntacticException(SyntacticException.Detail.INVALID_RANGE,
                                                            "Info",
                                                            "XXX",
                                                            0,
                                                            -1);
        assertEquals("^^^", x.getSlice());
    }
}