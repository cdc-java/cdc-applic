package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.parsing.TokenType;

class TokenTypeTest {
    @Test
    void testIsBinary() {
        final Set<TokenType> types = new HashSet<>();
        types.add(TokenType.AND);
        types.add(TokenType.OR);
        types.add(TokenType.EQUIV);
        types.add(TokenType.IMPL);
        types.add(TokenType.XOR);
        for (final TokenType type : TokenType.values()) {
            assertEquals(type.isBinary(), types.contains(type));
        }
    }

    @Test
    void testIsSetElement() {
        final Set<TokenType> types = new HashSet<>();
        types.add(TokenType.DOUBLE_QUOTES_TEXT);
        types.add(TokenType.TEXT);
        types.add(TokenType.INTEGER);
        types.add(TokenType.REAL);
        types.add(TokenType.FALSE);
        types.add(TokenType.TRUE);
        for (final TokenType type : TokenType.values()) {
            assertEquals(type.isItem(), types.contains(type));
        }
    }

    @Test
    void testSupportsRange() {
        final Set<TokenType> types = new HashSet<>();
        types.add(TokenType.INTEGER);
        types.add(TokenType.REAL);
        for (final TokenType type : TokenType.values()) {
            assertEquals(type.supportsRange(), types.contains(type));
        }
    }
}