package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.parsing.Analyzer;
import cdc.applic.expressions.parsing.ParsingEvent;
import cdc.applic.expressions.parsing.TokenType;

class AnalyzerTest {
    private static class Bucket {
        final TokenType type;
        final ParsingEvent.Info info;

        public Bucket(TokenType type,
                      ParsingEvent.Info info) {
            this.type = type;
            this.info = info;
        }
    }

    private static Bucket b(TokenType type,
                            ParsingEvent.Info info) {
        return new Bucket(type, info);
    }

    private static void check(String expression,
                              Bucket... expected) {
        final List<ParsingEvent> actual = Analyzer.analyze(expression);
        assertEquals(expected.length,
                     actual.size(),
                     "Length mismatch for '" + expression + "', " + actual);
        for (int i = 0; i < actual.size(); i++) {
            assertEquals(expected[i].type,
                         actual.get(i).getToken().getType(),
                         "Token type mismatch for '" + expression + "' at " + i);
            assertEquals(expected[i].info,
                         actual.get(i).getInfo(),
                         "Info mismatch for '" + expression + "' at " + i);
        }
    }

    @Test
    void testSimple() {
        check("A",
              b(TokenType.TEXT, ParsingEvent.Info.REF_LOCAL_NAME),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("TRUE",
              b(TokenType.TRUE, ParsingEvent.Info.OTHER),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("FALSE",
              b(TokenType.FALSE, ParsingEvent.Info.OTHER),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("not TRUE",
              b(TokenType.NOT, ParsingEvent.Info.OTHER),
              b(TokenType.TRUE, ParsingEvent.Info.OTHER),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A.B",
              b(TokenType.TEXT, ParsingEvent.Info.REF_PREFIX),
              b(TokenType.PATH_SEP, ParsingEvent.Info.OTHER),
              b(TokenType.TEXT, ParsingEvent.Info.REF_LOCAL_NAME),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));

        check("A = 10",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.EQUAL, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.VALUE),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A < 10",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.LESS, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.VALUE),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A <= 10",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.LESS_OR_EQUAL, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.VALUE),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A > 10",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.GREATER, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.VALUE),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A >= 10",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.GREATER_OR_EQUAL, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.VALUE),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));

        check("A != 10",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.NOT_EQUAL, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.VALUE),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A !< 10",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.NOT_LESS, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.VALUE),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A !<= 10",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.NEITHER_LESS_NOR_EQUAL, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.VALUE),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A !> 10",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.NOT_GREATER, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.VALUE),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A !>= 10",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.NEITHER_GREATER_NOR_EQUAL, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.VALUE),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));

        check("A = 10.0",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.EQUAL, ParsingEvent.Info.OTHER),
              b(TokenType.REAL, ParsingEvent.Info.VALUE),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));

        check("A <: {}",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.IN, ParsingEvent.Info.OTHER),
              b(TokenType.OPEN_SET, ParsingEvent.Info.OTHER),
              b(TokenType.CLOSE_SET, ParsingEvent.Info.OTHER),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));

        check("A <: ∅",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.IN, ParsingEvent.Info.OTHER),
              b(TokenType.EMPTY_SET, ParsingEvent.Info.OTHER),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));

        check("A <: {10}",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.IN, ParsingEvent.Info.OTHER),
              b(TokenType.OPEN_SET, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.VALUE),
              b(TokenType.CLOSE_SET, ParsingEvent.Info.OTHER),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));

        check("A <: {10, 11}",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.IN, ParsingEvent.Info.OTHER),
              b(TokenType.OPEN_SET, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.VALUE),
              b(TokenType.ITEMS_SEP, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.VALUE),
              b(TokenType.CLOSE_SET, ParsingEvent.Info.OTHER),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));

        check("A <: {10~11}",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.IN, ParsingEvent.Info.OTHER),
              b(TokenType.OPEN_SET, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.LOW_VALUE),
              b(TokenType.TO, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.HIGH_VALUE),
              b(TokenType.CLOSE_SET, ParsingEvent.Info.OTHER),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));

        check("A <: {10~11,12}",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.IN, ParsingEvent.Info.OTHER),
              b(TokenType.OPEN_SET, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.LOW_VALUE),
              b(TokenType.TO, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.HIGH_VALUE),
              b(TokenType.ITEMS_SEP, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.VALUE),
              b(TokenType.CLOSE_SET, ParsingEvent.Info.OTHER),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));

        check("A.B !<: {10~11,12}",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_PREFIX),
              b(TokenType.PATH_SEP, ParsingEvent.Info.OTHER),
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.NOT_IN, ParsingEvent.Info.OTHER),
              b(TokenType.OPEN_SET, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.LOW_VALUE),
              b(TokenType.TO, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.HIGH_VALUE),
              b(TokenType.ITEMS_SEP, ParsingEvent.Info.OTHER),
              b(TokenType.INTEGER, ParsingEvent.Info.VALUE),
              b(TokenType.CLOSE_SET, ParsingEvent.Info.OTHER),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));

        check("A = OR",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.EQUAL, ParsingEvent.Info.OTHER),
              b(TokenType.TEXT, ParsingEvent.Info.VALUE),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A = true",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.EQUAL, ParsingEvent.Info.OTHER),
              b(TokenType.TRUE, ParsingEvent.Info.VALUE),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A = false",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.EQUAL, ParsingEvent.Info.OTHER),
              b(TokenType.FALSE, ParsingEvent.Info.VALUE),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A.B = OR",
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_PREFIX),
              b(TokenType.PATH_SEP, ParsingEvent.Info.OTHER),
              b(TokenType.TEXT, ParsingEvent.Info.PROPERTY_LOCAL_NAME),
              b(TokenType.EQUAL, ParsingEvent.Info.OTHER),
              b(TokenType.TEXT, ParsingEvent.Info.VALUE),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A OR B",
              b(TokenType.TEXT, ParsingEvent.Info.REF_LOCAL_NAME),
              b(TokenType.OR, ParsingEvent.Info.OTHER),
              b(TokenType.TEXT, ParsingEvent.Info.REF_LOCAL_NAME),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A XOR B",
              b(TokenType.TEXT, ParsingEvent.Info.REF_LOCAL_NAME),
              b(TokenType.XOR, ParsingEvent.Info.OTHER),
              b(TokenType.TEXT, ParsingEvent.Info.REF_LOCAL_NAME),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A AND B",
              b(TokenType.TEXT, ParsingEvent.Info.REF_LOCAL_NAME),
              b(TokenType.AND, ParsingEvent.Info.OTHER),
              b(TokenType.TEXT, ParsingEvent.Info.REF_LOCAL_NAME),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A IMP B",
              b(TokenType.TEXT, ParsingEvent.Info.REF_LOCAL_NAME),
              b(TokenType.IMPL, ParsingEvent.Info.OTHER),
              b(TokenType.TEXT, ParsingEvent.Info.REF_LOCAL_NAME),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("A IFF B",
              b(TokenType.TEXT, ParsingEvent.Info.REF_LOCAL_NAME),
              b(TokenType.EQUIV, ParsingEvent.Info.OTHER),
              b(TokenType.TEXT, ParsingEvent.Info.REF_LOCAL_NAME),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("!A",
              b(TokenType.NOT, ParsingEvent.Info.OTHER),
              b(TokenType.TEXT, ParsingEvent.Info.REF_LOCAL_NAME),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("!(A)",
              b(TokenType.NOT, ParsingEvent.Info.OTHER),
              b(TokenType.OPEN_PAREN, ParsingEvent.Info.OTHER),
              b(TokenType.TEXT, ParsingEvent.Info.REF_LOCAL_NAME),
              b(TokenType.CLOSE_PAREN, ParsingEvent.Info.OTHER),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
        check("$X$",
              b(TokenType.DOLLAR_TEXT, ParsingEvent.Info.OTHER),
              b(TokenType.EPSILON, ParsingEvent.Info.OTHER));
    }
}