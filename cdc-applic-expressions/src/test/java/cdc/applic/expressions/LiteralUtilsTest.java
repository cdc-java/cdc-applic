package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.literals.LiteralKind;
import cdc.applic.expressions.literals.LiteralUtils;

class LiteralUtilsTest {
    @Test
    void testIsBooleanLiteral() {
        assertFalse(LiteralUtils.isBooleanLiteral(null));
        assertFalse(LiteralUtils.isBooleanLiteral(""));
        assertFalse(LiteralUtils.isBooleanLiteral("1"));
        assertFalse(LiteralUtils.isBooleanLiteral("A"));
        assertTrue(LiteralUtils.isBooleanLiteral("true"));
        assertTrue(LiteralUtils.isBooleanLiteral("false"));
        assertTrue(LiteralUtils.isBooleanLiteral("TRUE"));
        assertTrue(LiteralUtils.isBooleanLiteral("FALSE"));
    }

    @Test
    void testIsSpecialStringLiteral() {
        assertFalse(LiteralUtils.isSpecialStringLiteral(null));
        assertFalse(LiteralUtils.isSpecialStringLiteral(""));

        assertFalse(LiteralUtils.isSpecialStringLiteral("ix"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("xn"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("ox"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("xr"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("tx"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("xo"));

        assertTrue(LiteralUtils.isSpecialStringLiteral("in"));
        assertTrue(LiteralUtils.isSpecialStringLiteral("IN"));
        assertTrue(LiteralUtils.isSpecialStringLiteral("or"));
        assertTrue(LiteralUtils.isSpecialStringLiteral("OR"));
        assertTrue(LiteralUtils.isSpecialStringLiteral("to"));
        assertTrue(LiteralUtils.isSpecialStringLiteral("TO"));

        assertFalse(LiteralUtils.isSpecialStringLiteral("anx"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("axd"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("xnd"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("ifx"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("ixf"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("xff"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("imx"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("ixp"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("xmp"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("nox"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("nxt"));
        assertFalse(LiteralUtils.isSpecialStringLiteral("xot"));

        assertTrue(LiteralUtils.isSpecialStringLiteral("and"));
        assertTrue(LiteralUtils.isSpecialStringLiteral("AND"));
        assertTrue(LiteralUtils.isSpecialStringLiteral("iff"));
        assertTrue(LiteralUtils.isSpecialStringLiteral("IFF"));
        assertTrue(LiteralUtils.isSpecialStringLiteral("imp"));
        assertTrue(LiteralUtils.isSpecialStringLiteral("IMP"));
        assertTrue(LiteralUtils.isSpecialStringLiteral("not"));
        assertTrue(LiteralUtils.isSpecialStringLiteral("NOT"));
    }

    @Test
    void testIsIntegerLiteral() {
        assertFalse(LiteralUtils.isIntegerLiteral(""));
        assertTrue(LiteralUtils.isIntegerLiteral("0"));
        assertTrue(LiteralUtils.isIntegerLiteral("+0"));
        assertTrue(LiteralUtils.isIntegerLiteral("-1"));
        assertTrue(LiteralUtils.isIntegerLiteral("01"));
        assertTrue(LiteralUtils.isIntegerLiteral("12"));
        assertTrue(LiteralUtils.isIntegerLiteral("+001"));
        assertFalse(LiteralUtils.isIntegerLiteral("12 "));
        assertFalse(LiteralUtils.isIntegerLiteral(" 12"));
        assertFalse(LiteralUtils.isIntegerLiteral("1 2"));
    }

    @Test
    void testIsRealLiteral() {
        assertFalse(LiteralUtils.isRealLiteral("+"));
        assertFalse(LiteralUtils.isRealLiteral("-"));
        assertFalse(LiteralUtils.isRealLiteral("1"));
        assertFalse(LiteralUtils.isRealLiteral("1 "));
        assertFalse(LiteralUtils.isRealLiteral("+1"));
        assertFalse(LiteralUtils.isRealLiteral("1."));
        assertFalse(LiteralUtils.isRealLiteral("+1."));
        assertFalse(LiteralUtils.isRealLiteral("-1."));
        assertFalse(LiteralUtils.isRealLiteral("1.0e"));
        assertFalse(LiteralUtils.isRealLiteral("1.0E"));
        assertFalse(LiteralUtils.isRealLiteral("1.0a"));
        assertFalse(LiteralUtils.isRealLiteral("+1.0e"));
        assertFalse(LiteralUtils.isRealLiteral("-1.0e"));
        assertFalse(LiteralUtils.isRealLiteral("1.0e+"));
        assertFalse(LiteralUtils.isRealLiteral("+1.0e+"));
        assertFalse(LiteralUtils.isRealLiteral("-1.0e+"));
        assertFalse(LiteralUtils.isRealLiteral("1.0e-"));
        assertFalse(LiteralUtils.isRealLiteral("+1.0e-"));
        assertFalse(LiteralUtils.isRealLiteral("-1.0e-"));
        assertFalse(LiteralUtils.isRealLiteral("1.0e+a"));
        assertFalse(LiteralUtils.isRealLiteral("+1.0e+a"));
        assertFalse(LiteralUtils.isRealLiteral("-1.0e+a"));

        assertTrue(LiteralUtils.isRealLiteral("1.0"));
        assertTrue(LiteralUtils.isRealLiteral("+1.0"));
        assertTrue(LiteralUtils.isRealLiteral("-1.0"));
        assertFalse(LiteralUtils.isRealLiteral("1.0e"));
        assertTrue(LiteralUtils.isRealLiteral("1.0e1"));
        assertTrue(LiteralUtils.isRealLiteral("+1.0e+1"));
        assertTrue(LiteralUtils.isRealLiteral("-1.0e-1"));
    }

    @Test
    void testIsRestrictedStringLiteral() {
        assertTrue(LiteralUtils.isRestrictedStringLiteral("A"));
        assertFalse(LiteralUtils.isRestrictedStringLiteral("1"));
        assertFalse(LiteralUtils.isRestrictedStringLiteral(""));
        assertFalse(LiteralUtils.isRestrictedStringLiteral("true"));
        assertFalse(LiteralUtils.isRestrictedStringLiteral("1.0"));
    }

    @Test
    void testStartsAsNumber() {
        assertTrue(LiteralUtils.startsAsNumber("1"));
        assertTrue(LiteralUtils.startsAsNumber("+1"));
        assertTrue(LiteralUtils.startsAsNumber("-1"));
        assertTrue(LiteralUtils.startsAsNumber("1A"));
        assertTrue(LiteralUtils.startsAsNumber("+1A"));
        assertTrue(LiteralUtils.startsAsNumber("-1A"));

        assertFalse(LiteralUtils.startsAsNumber(null));
        assertFalse(LiteralUtils.startsAsNumber(""));
        assertFalse(LiteralUtils.startsAsNumber("+"));
        assertFalse(LiteralUtils.startsAsNumber("-"));
    }

    @Test
    void testGetKind() {
        assertEquals(LiteralKind.BOOLEAN, LiteralUtils.getKind("true"));
        assertEquals(LiteralKind.BOOLEAN, LiteralUtils.getKind("TRUE"));
        assertEquals(LiteralKind.BOOLEAN, LiteralUtils.getKind("false"));
        assertEquals(LiteralKind.BOOLEAN, LiteralUtils.getKind("FALSE"));
        assertEquals(LiteralKind.INTEGER, LiteralUtils.getKind("10"));
        assertEquals(LiteralKind.INTEGER, LiteralUtils.getKind("0010"));
        assertEquals(LiteralKind.INTEGER, LiteralUtils.getKind("+10"));
        assertEquals(LiteralKind.INTEGER, LiteralUtils.getKind("-10"));
        assertEquals(LiteralKind.REAL, LiteralUtils.getKind("10.0"));
        assertEquals(LiteralKind.REAL, LiteralUtils.getKind("+10.0"));
        assertEquals(LiteralKind.REAL, LiteralUtils.getKind("-10.0e+3"));
        assertEquals(LiteralKind.RESTRICTED_STRING, LiteralUtils.getKind("A"));
        assertEquals(LiteralKind.RESTRICTED_STRING, LiteralUtils.getKind("A A"));
        assertEquals(LiteralKind.RESTRICTED_STRING, LiteralUtils.getKind("A-A"));
        assertEquals(LiteralKind.RESTRICTED_STRING, LiteralUtils.getKind(" "));
        assertEquals(LiteralKind.RESTRICTED_STRING, LiteralUtils.getKind("+"));
        assertEquals(LiteralKind.RESTRICTED_STRING, LiteralUtils.getKind("-"));
        assertEquals(LiteralKind.RESTRICTED_STRING, LiteralUtils.getKind("+ 10"));
        assertEquals(LiteralKind.RESTRICTED_STRING, LiteralUtils.getKind("- 10"));
        assertEquals(LiteralKind.RESTRICTED_STRING, LiteralUtils.getKind(" TRUE"));
        assertEquals(LiteralKind.RESTRICTED_STRING, LiteralUtils.getKind(" FALSE"));
        assertEquals(LiteralKind.NONE, LiteralUtils.getKind(""));
        assertEquals(LiteralKind.NONE, LiteralUtils.getKind(null));
    }
}