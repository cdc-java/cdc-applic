package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.literals.EscapingMode;
import cdc.applic.expressions.literals.SName;

class SNameTest {
    private static void testConstructor(String unescaped,
                                        String escaped) {
        final SName escapedName = SName.of(escaped, true);
        final SName unescapedName = SName.of(unescaped, false);
        final SName unescapedName2 = SName.of(unescaped);

        assertEquals(unescapedName, unescapedName2);

        assertEquals(unescaped, escapedName.getNonEscapedLiteral());
        assertEquals(escaped, escapedName.getEscapedLiteral());
        assertEquals(escaped, escapedName.getLiteral(EscapingMode.ESCAPED));
        assertEquals(unescaped, escapedName.getLiteral(EscapingMode.NON_ESCAPED));

        assertEquals(unescaped, unescapedName.getNonEscapedLiteral());
        assertEquals(escaped, unescapedName.getEscapedLiteral());
        assertEquals(escaped, unescapedName.getLiteral(EscapingMode.ESCAPED));
        assertEquals(unescaped, unescapedName.getLiteral(EscapingMode.NON_ESCAPED));

        assertEquals(escapedName, escapedName);
        assertEquals(escapedName, unescapedName);
        assertNotEquals(escapedName, null);

        if (escapedName.needsEscape()) {
            assertEquals(escaped, escapedName.getLiteral(EscapingMode.PROTECTED));
        } else {
            assertEquals(unescaped, escapedName.getLiteral(EscapingMode.PROTECTED));
        }
    }

    @Test
    void testConstructor() {
        testConstructor("a", "\"a\"");
        testConstructor("10", "\"10\"");
        testConstructor("...", "\"...\"");
    }

    @Test
    void testLiteralPatternUnknown() {
        final SName name = SName.of(SName.LITERAL_PATTERN_UNKNOWN);
        assertTrue(name.isPatternUnknwown());
    }

    @Test
    void testInvalidArg() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         SName.of("", true);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         SName.of("", false);
                     });
    }

    @Test
    void testHashCode() {
        assertEquals(SName.of("Hello", false).hashCode(), SName.of("Hello", false).hashCode());
    }

    @Test
    void testCompare() {
        final SName name1 = SName.of("A");
        final SName name2 = SName.of("B");
        final SName any = SName.of(SName.LITERAL_PATTERN_UNKNOWN);
        assertEquals(0, name1.compareTo(name1));
        assertEquals(0, name2.compareTo(name2));
        assertTrue(name1.compareTo(name2) < 0);
        assertTrue(name2.compareTo(name1) > 0);
        assertTrue(any.compareTo(name1) > 0);
        assertTrue(any.compareTo(name2) > 0);
        assertTrue(name1.compareTo(any) < 0);
        assertTrue(name2.compareTo(any) < 0);
    }
}