package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.checks.ApplicIssueType;

class IssueTypeTest {
    @Test
    void test() {
        for (final ApplicIssueType type1 : ApplicIssueType.values()) {
            for (final ApplicIssueType type2 : ApplicIssueType.values()) {
                if (type1.ordinal() <= type2.ordinal()) {
                    assertEquals(type2, ApplicIssueType.mostSevere(type1, type2));
                } else {
                    assertEquals(type1, ApplicIssueType.mostSevere(type1, type2));
                }
            }
        }
    }
}