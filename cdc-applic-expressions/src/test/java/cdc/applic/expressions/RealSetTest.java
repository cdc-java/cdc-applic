package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.RealRange;
import cdc.applic.expressions.content.RealSItem;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.UncheckedSet;

class RealSetTest {
    private static RealValue v(double value) {
        return RealValue.of(value);
    }

    private static RealRange r(double value) {
        return RealRange.of(value);
    }

    private static RealRange r(double min,
                               double max) {
        return RealRange.of(min, max);
    }

    @SafeVarargs
    private static RealSet s(RealRange... ranges) {
        return RealSet.of(ranges);
    }

    @SafeVarargs
    private static void checkOfItems(String expected,
                                     RealSItem... items) {
        final RealSet set1 = RealSet.of(items);
        final RealSet set2 = RealSet.of(Arrays.asList(items));
        assertEquals(expected, set1.toString());
        assertEquals(set1, set1);
        assertEquals(set1, set2);

        assertEquals(set1.isEmpty(), set1.isCompliantWith(BooleanSet.class));
        assertEquals(set1.isEmpty(), set1.isCompliantWith(IntegerSet.class));
        assertTrue(set1.isCompliantWith(RealSet.class));
        assertEquals(set1.isEmpty(), set1.isCompliantWith(StringSet.class));
        assertTrue(set1.isCompliantWith(UncheckedSet.class));
    }

    @Test
    void testOfItems() {
        assertTrue(RealSet.EMPTY.isEmpty());

        checkOfItems("{}");
        checkOfItems("{1.0}", v(1));
        checkOfItems("{1.0}", r(1));
        checkOfItems("{}", r(1.0, -1));
        checkOfItems("{1.0~10.0}", r(1.0, 10));
        checkOfItems("{1.0~10.0,20.0}", r(1.0, 10), r(20));
        checkOfItems("{1.0~10.0,12.0}", r(1.0, 10), r(12));
        checkOfItems("{1.0~10.0,11.0}", r(1.0, 10), r(11));
        checkOfItems("{1.0~10.0}", r(1.0, 10), r(10));
        checkOfItems("{1.0~20.0}", r(1.0, 10), r(9, 20));
        checkOfItems("{1.0~10.0}", r(1.0, 10), r(20, 10));
        checkOfItems("{}", r(11.0, 10), r(20, 10));
        checkOfItems("{1.0,2.0}", r(1), r(2));

        checkOfItems("{1.0}", r(1));

        checkOfItems("{1.0,2.0}", r(1), r(2));
        checkOfItems("{1.0,2.0}", r(2), r(1));

        checkOfItems("{-1.0,1.0~2.0}", r(-1), r(1.0, 2));
        checkOfItems("{-1.0,1.0~2.0}", r(1.0, 2), r(-1));
        checkOfItems("{0.0,1.0~2.0}", r(0), r(1.0, 2));
        checkOfItems("{0.0,1.0~2.0}", r(1.0, 2), r(0));
        checkOfItems("{1.0~2.0}", r(1), r(1.0, 2));
        checkOfItems("{1.0~2.0}", r(1.0, 2), r(1));
        checkOfItems("{1.0~2.0}", r(2), r(1.0, 2));
        checkOfItems("{1.0~2.0}", r(1.0, 2), r(2));
        checkOfItems("{1.0~2.0,3.0}", r(3), r(1.0, 2));
        checkOfItems("{1.0~2.0,3.0}", r(1.0, 2), r(3));
        checkOfItems("{1.0~2.0,4.0}", r(4), r(1.0, 2));
        checkOfItems("{1.0~2.0,4.0}", r(1.0, 2), r(4));

        checkOfItems("{0.0~3.0}", r(1.0, 2), r(0, 3));
    }

    @Test
    void testContains() {
        assertFalse(s().contains(1));

        assertFalse(s(r(1.0, 2)).contains(0));
        assertTrue(s(r(1.0, 2)).contains(1));
        assertTrue(s(r(1.0, 2)).contains(2));
        assertFalse(s(r(1.0, 2)).contains(3));

        assertTrue(s().contains(r(1.0, 0)));
        assertTrue(s(r(1)).contains(r(1.0, 0)));

        assertFalse(s(r(1.0, 2), r(4.0, 5)).contains(r(0)));
        assertTrue(s(r(1.0, 2), r(4.0, 5)).contains(r(1)));
        assertTrue(s(r(1.0, 2), r(4.0, 5)).contains(r(2)));
        assertFalse(s(r(1.0, 2), r(4.0, 5)).contains(r(3)));
        assertTrue(s(r(1.0, 2), r(4.0, 5)).contains(r(4)));
        assertTrue(s(r(1.0, 2), r(4.0, 5)).contains(r(5)));
        assertFalse(s(r(1.0, 2), r(4.0, 5)).contains(r(6)));
        assertTrue(s(r(1.0, 2), r(4.0, 5)).contains(r(1.0, 2)));
        assertTrue(s(r(1.0, 2), r(4.0, 5)).contains(r(4.0, 5)));

        assertFalse(s(r(1.0, 2), r(4.0, 5)).contains(r(0, 1)));
    }

    @Test
    void testUnion() {
        assertEquals(s(r(1)), s().union(1));
        assertEquals(s(r(1)), s(r(1)).union(1));

        assertEquals(s(r(1)), s().union(r(1)));
        assertEquals(s(r(2)), s().union(r(2)));

        assertEquals(s(r(1)), s(r(1)).union(r(1)));
        assertEquals(s(r(1.0), r(2)), s(r(1)).union(r(2)));

        assertEquals(s(r(1)), s(r(1)).union(s()));
        assertEquals(s(r(1.0), r(2)), s(r(1)).union(s(r(2))));
    }

    @Test
    void testIntersection() {
        assertEquals(s(), s().intersection(1));
        assertEquals(s(r(1)), s(r(1)).intersection(1));
        assertEquals(s(), s(r(2)).intersection(1));

        assertEquals(s(), s().intersection(r(1)));
        assertEquals(s(r(1)), s(r(1)).intersection(r(1)));
        assertEquals(s(), s(r(2)).intersection(r(1)));
        assertEquals(s(r(2)), s(r(2)).intersection(r(1.0, 2)));

        assertEquals(s(), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 0)));
        assertEquals(s(r(1)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 1)));
        assertEquals(s(r(1.0, 2)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 2)));
        assertEquals(s(r(1.0, 3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 3)));
        assertEquals(s(r(1.0, 3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 4)));
        assertEquals(s(r(1.0, 3), r(5)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 5)));
        assertEquals(s(r(1.0, 3), r(5.0, 6)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 6)));
        assertEquals(s(r(1.0, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 7)));
        assertEquals(s(r(1.0, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(0, 8)));

        assertEquals(s(r(1)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 1)));
        assertEquals(s(r(1.0, 2)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 2)));
        assertEquals(s(r(1.0, 3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 3)));
        assertEquals(s(r(1.0, 3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 4)));
        assertEquals(s(r(1.0, 3), r(5)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 5)));
        assertEquals(s(r(1.0, 3), r(5.0, 6)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 6)));
        assertEquals(s(r(1.0, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 7)));
        assertEquals(s(r(1.0, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(1.0, 8)));

        assertEquals(s(r(2)), s(r(1.0, 3), r(5.0, 7)).intersection(r(2, 2)));
        assertEquals(s(r(2, 3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(2, 3)));
        assertEquals(s(r(2, 3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(2, 4)));
        assertEquals(s(r(2, 3), r(5)), s(r(1.0, 3), r(5.0, 7)).intersection(r(2, 5)));
        assertEquals(s(r(2, 3), r(5.0, 6)), s(r(1.0, 3), r(5.0, 7)).intersection(r(2, 6)));
        assertEquals(s(r(2, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(2, 7)));
        assertEquals(s(r(2, 3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(2, 8)));

        assertEquals(s(r(3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(3, 3)));
        assertEquals(s(r(3)), s(r(1.0, 3), r(5.0, 7)).intersection(r(3, 4)));
        assertEquals(s(r(3), r(5)), s(r(1.0, 3), r(5.0, 7)).intersection(r(3, 5)));
        assertEquals(s(r(3), r(5.0, 6)), s(r(1.0, 3), r(5.0, 7)).intersection(r(3, 6)));
        assertEquals(s(r(3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(3, 7)));
        assertEquals(s(r(3), r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(3, 8)));

        assertEquals(s(), s(r(1.0, 3), r(5.0, 7)).intersection(r(4.0, 4)));
        assertEquals(s(r(5)), s(r(1.0, 3), r(5.0, 7)).intersection(r(4.0, 5)));
        assertEquals(s(r(5.0, 6)), s(r(1.0, 3), r(5.0, 7)).intersection(r(4.0, 6)));
        assertEquals(s(r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(4.0, 7)));
        assertEquals(s(r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(4.0, 8)));

        assertEquals(s(r(5)), s(r(1.0, 3), r(5.0, 7)).intersection(r(5.0, 5)));
        assertEquals(s(r(5.0, 6)), s(r(1.0, 3), r(5.0, 7)).intersection(r(5.0, 6)));
        assertEquals(s(r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(5.0, 7)));
        assertEquals(s(r(5.0, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(5.0, 8)));

        assertEquals(s(r(6)), s(r(1.0, 3), r(5.0, 7)).intersection(r(6, 6)));
        assertEquals(s(r(6, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(6, 7)));
        assertEquals(s(r(6, 7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(6, 8)));

        assertEquals(s(r(7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(7, 7)));
        assertEquals(s(r(7)), s(r(1.0, 3), r(5.0, 7)).intersection(r(7, 8)));

        assertEquals(s(), s(r(1.0, 3), r(5.0, 7)).intersection(r(8, 8)));

        assertEquals(s(r(3), r(5.0, 7), r(9)), s(r(1.0, 3), r(5.0, 7), r(9, 11)).intersection(r(3, 9)));

        assertEquals(s(r(3), r(5.0, 7), r(9)), s(r(1.0, 3), r(5.0, 7), r(9, 11)).intersection(s(r(3, 9))));

        assertEquals(s(), s().intersection(s()));
        assertEquals(s(), s(r(1)).intersection(s()));
        assertEquals(s(), s(r(1)).intersection((SItemSet) s()));
    }

    @Test
    void testRemove() {
        assertEquals(s(), s().remove(1));
        assertEquals(s(), s(r(1)).remove(1));
    }

    @Test
    void testConvert() {
        assertEquals(RealSet.EMPTY, RealSet.convert(BooleanSet.EMPTY));
        assertEquals(RealSet.of("1.0"), RealSet.convert(RealSet.of("1.0")));
        assertEquals(RealSet.of("1.0"), RealSet.convert(UncheckedSet.of("1.0")));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RealSet.convert(BooleanSet.TRUE);
                     });

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RealSet.convert(IntegerSet.of("0"));
                     });
    }

    @Test
    void testEquals() {
        assertEquals(s(r(1), r(2)), s(r(2), r(1)));
        assertNotEquals(s(r(1), r(2)), null);
        assertNotEquals(s(r(1), r(2)), 1);
        assertNotEquals(s(), 1);
        assertEquals(s(), s());
        assertEquals(s(), s(r(1.0, 0)));
        assertNotEquals(s(), s(r(0)));
    }

    @Test
    void testHashCode() {
        assertEquals(s().hashCode(), s().hashCode());
        assertEquals(s().hashCode(), s(r(1.0, 0)).hashCode());
        assertEquals(s(r(1)).hashCode(), s(r(1)).hashCode());
    }

    @Test
    void testMisc() {
        final RealSet s1 = s(r(1));
        assertEquals(v(1), s1.getSingletonValue());
    }

    @Test
    void testBug199() {
        final RealSet set = RealSet.of("10.0,9.0,8.0");
        assertTrue(set.contains(8.0));
    }
}