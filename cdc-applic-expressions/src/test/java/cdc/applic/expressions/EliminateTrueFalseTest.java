package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.expressions.ast.visitors.EliminateTrueFalse;

class EliminateTrueFalseTest {

    private static void check(String expression,
                              String expected) {
        final Expression x = new Expression(expression);
        final Node node = EliminateTrueFalse.execute(ConvertToNary.execute(x.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY));
        assertEquals(expected, node.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void test() {
        check("true", "true");
        check("!true", "false");
        check("!false", "true");
        check("not X", "!X");
        check("X or Y", "X|Y");
        check("X or true", "true");
        check("true or X", "true");
        check("X or false", "X");
        check("false or X", "X");
        check("X and true", "X");
        check("true and X", "X");
        check("X and false", "false");
        check("false and X", "false");
        check("X or true or Y", "true");
        check("X or false or Y", "X|Y");
        check("X and true and Y", "X&Y");
        check("X and false and Y", "false");
        check("X and false and false", "false");
        check("X and true and true", "X");
        check("true and true and true", "true");
        check("true or true or true", "true");
        check("false and false and false", "false");
        check("false or false or false", "false");
    }
}