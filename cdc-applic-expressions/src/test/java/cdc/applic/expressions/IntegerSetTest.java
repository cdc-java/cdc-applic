package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.UncheckedSet;

class IntegerSetTest {
    // private static final Logger LOGGER = LogManager.getLogger(RangeSetTest.class);

    private static IntegerValue v(int value) {
        return IntegerValue.of(value);
    }

    private static IntegerRange r(int value) {
        return IntegerRange.of(value);
    }

    private static IntegerRange r(int min,
                                  int max) {
        return IntegerRange.of(min, max);
    }

    @SafeVarargs
    private static IntegerSet s(IntegerRange... ranges) {
        return IntegerSet.of(ranges);
    }

    @SafeVarargs
    private static void checkOfRanges(String expected,
                                      IntegerRange... ranges) {
        // LOGGER.info(Arrays.toString(ranges) + " " + expected);
        final IntegerSet set1 = IntegerSet.of(ranges);
        final IntegerSet set2 = IntegerSet.of(Arrays.asList(ranges));
        // LOGGER.info(Arrays.toString(ranges) + " " + set1);
        assertEquals(expected, set1.toString());
        assertEquals(set1, set1);
        assertEquals(set1, set2);
        assertTrue(set1.isChecked());
        assertTrue(set1.isValid());
        assertEquals(set1, set1.getChecked());
        assertEquals(set1, set1.getBest());

        assertEquals(set1.isEmpty(), set1.isCompliantWith(BooleanSet.class));
        assertTrue(set1.isCompliantWith(IntegerSet.class));
        assertEquals(set1.isEmpty(), set1.isCompliantWith(RealSet.class));
        assertEquals(set1.isEmpty(), set1.isCompliantWith(StringSet.class));
        assertTrue(set1.isCompliantWith(UncheckedSet.class));
    }

    private static void checkOfString(String expected,
                                      String content) {
        final IntegerSet set = IntegerSet.of(content);
        assertEquals(expected, set.toString());
    }

    @Test
    void testOfRanges() {
        assertTrue(IntegerSet.EMPTY.isEmpty());

        assertFalse(r(1).isEmpty());
        assertTrue(r(1).isSingleton());
        assertTrue(r(1, 1).isSingleton());
        assertFalse(r(1, 2).isSingleton());

        checkOfRanges("{}");
        checkOfRanges("{1}", r(1), r(1));
        checkOfRanges("{1}", r(1));
        checkOfRanges("{}", r(1, -1));
        checkOfRanges("{1~10}", r(1, 10));
        checkOfRanges("{1~10,20}", r(1, 10), r(20));
        checkOfRanges("{1~10,12}", r(1, 10), r(12));
        checkOfRanges("{1~11}", r(1, 10), r(11));
        checkOfRanges("{1~10}", r(1, 10), r(10));
        checkOfRanges("{1~20}", r(1, 10), r(9, 20));
        checkOfRanges("{1~10}", r(1, 10), r(20, 10));
        checkOfRanges("{}", r(11, 10), r(20, 10));
        checkOfRanges("{1~2}", r(1), r(2));

        checkOfRanges("{1}", r(1));

        checkOfRanges("{1~2}", r(1), r(2));
        checkOfRanges("{1~2}", r(2), r(1));

        checkOfRanges("{2~6}", r(2), r(3, 6));

        checkOfRanges("{-1,1~2}", r(-1), r(1, 2));
        checkOfRanges("{-1,1~2}", r(1, 2), r(-1));
        checkOfRanges("{0~2}", r(0), r(1, 2));
        checkOfRanges("{0~2}", r(1, 2), r(0));
        checkOfRanges("{1~2}", r(1), r(1, 2));
        checkOfRanges("{1~2}", r(1, 2), r(1));
        checkOfRanges("{1~2}", r(2), r(1, 2));
        checkOfRanges("{1~2}", r(1, 2), r(2));
        checkOfRanges("{1~3}", r(3), r(1, 2));
        checkOfRanges("{1~3}", r(1, 2), r(3));
        checkOfRanges("{1~2,4}", r(4), r(1, 2));
        checkOfRanges("{1~2,4}", r(1, 2), r(4));

        checkOfRanges("{0~3}", r(1, 2), r(0, 3));

        checkOfRanges("{1~2,4~5,7~8}", r(4, 5), r(7, 8), r(1, 2));
        checkOfRanges("{1~5,7~8}", r(4, 5), r(7, 8), r(1, 3));
        checkOfRanges("{1~5,7~8}", r(4, 5), r(7, 8), r(1, 4));
        checkOfRanges("{1~5,7~8}", r(4, 5), r(7, 8), r(1, 5));
        checkOfRanges("{1~8}", r(4, 5), r(7, 8), r(1, 6));
        checkOfRanges("{1~8}", r(4, 5), r(7, 8), r(1, 7));
        checkOfRanges("{1~8}", r(4, 5), r(7, 8), r(1, 8));
        checkOfRanges("{1~9}", r(4, 5), r(7, 8), r(1, 9));

        checkOfRanges("{2,4~5,7~8}", r(4, 5), r(7, 8), r(2, 2));
        checkOfRanges("{2~5,7~8}", r(4, 5), r(7, 8), r(2, 3));
        checkOfRanges("{2~5,7~8}", r(4, 5), r(7, 8), r(2, 4));
        checkOfRanges("{2~5,7~8}", r(4, 5), r(7, 8), r(2, 5));
        checkOfRanges("{2~8}", r(4, 5), r(7, 8), r(2, 6));
        checkOfRanges("{2~8}", r(4, 5), r(7, 8), r(2, 7));
        checkOfRanges("{2~8}", r(4, 5), r(7, 8), r(2, 8));
        checkOfRanges("{2~9}", r(4, 5), r(7, 8), r(2, 9));

        checkOfRanges("{3~5,7~8}", r(4, 5), r(7, 8), r(3, 3));
        checkOfRanges("{3~5,7~8}", r(4, 5), r(7, 8), r(3, 4));
        checkOfRanges("{3~5,7~8}", r(4, 5), r(7, 8), r(3, 5));
        checkOfRanges("{3~8}", r(4, 5), r(7, 8), r(3, 6));
        checkOfRanges("{3~8}", r(4, 5), r(7, 8), r(3, 7));
        checkOfRanges("{3~8}", r(4, 5), r(7, 8), r(3, 8));
        checkOfRanges("{3~9}", r(4, 5), r(7, 8), r(3, 9));

        checkOfRanges("{4~5,7~8}", r(4, 5), r(7, 8), r(4, 4));
        checkOfRanges("{4~5,7~8}", r(4, 5), r(7, 8), r(4, 5));
        checkOfRanges("{4~8}", r(4, 5), r(7, 8), r(4, 6));
        checkOfRanges("{4~8}", r(4, 5), r(7, 8), r(4, 7));
        checkOfRanges("{4~8}", r(4, 5), r(7, 8), r(4, 8));
        checkOfRanges("{4~9}", r(4, 5), r(7, 8), r(4, 9));

        checkOfRanges("{4~5,7~8}", r(4, 5), r(7, 8), r(5, 5));
        checkOfRanges("{4~8}", r(4, 5), r(7, 8), r(5, 6));
        checkOfRanges("{4~8}", r(4, 5), r(7, 8), r(5, 7));
        checkOfRanges("{4~8}", r(4, 5), r(7, 8), r(5, 8));
        checkOfRanges("{4~9}", r(4, 5), r(7, 8), r(5, 9));

        checkOfRanges("{4~8}", r(4, 5), r(7, 8), r(6, 6));
        checkOfRanges("{4~8}", r(4, 5), r(7, 8), r(6, 7));
        checkOfRanges("{4~8}", r(4, 5), r(7, 8), r(6, 8));
        checkOfRanges("{4~9}", r(4, 5), r(7, 8), r(6, 9));

        checkOfRanges("{4~5,7~8}", r(4, 5), r(7, 8), r(7, 7));
        checkOfRanges("{4~5,7~8}", r(4, 5), r(7, 8), r(7, 8));
        checkOfRanges("{4~5,7~9}", r(4, 5), r(7, 8), r(7, 9));

        checkOfRanges("{4~5,7~8}", r(4, 5), r(7, 8), r(8, 8));
        checkOfRanges("{4~5,7~9}", r(4, 5), r(7, 8), r(8, 9));

        checkOfRanges("{4~5,7~9}", r(4, 5), r(7, 8), r(9, 9));

        checkOfRanges("{1,4,7}", r(4), r(7), r(1, 1));
        checkOfRanges("{1~2,4,7}", r(4), r(7), r(1, 2));
        checkOfRanges("{1~4,7}", r(4), r(7), r(1, 3));
        checkOfRanges("{1~4,7}", r(4), r(7), r(1, 4));
        checkOfRanges("{1~5,7}", r(4), r(7), r(1, 5));
        checkOfRanges("{1~7}", r(4), r(7), r(1, 6));
        checkOfRanges("{1~7}", r(4), r(7), r(1, 7));
        checkOfRanges("{1~8}", r(4), r(7), r(1, 8));
        checkOfRanges("{1~9}", r(4), r(7), r(1, 9));

        checkOfRanges("{2,4,7}", r(4), r(7), r(2, 2));
        checkOfRanges("{2~4,7}", r(4), r(7), r(2, 3));
        checkOfRanges("{2~4,7}", r(4), r(7), r(2, 4));
        checkOfRanges("{2~5,7}", r(4), r(7), r(2, 5));
        checkOfRanges("{2~7}", r(4), r(7), r(2, 6));
        checkOfRanges("{2~7}", r(4), r(7), r(2, 7));
        checkOfRanges("{2~8}", r(4), r(7), r(2, 8));
        checkOfRanges("{2~9}", r(4), r(7), r(2, 9));

        checkOfRanges("{3~4,7}", r(4), r(7), r(3, 3));
        checkOfRanges("{3~4,7}", r(4), r(7), r(3, 4));
        checkOfRanges("{3~5,7}", r(4), r(7), r(3, 5));
        checkOfRanges("{3~7}", r(4), r(7), r(3, 6));
        checkOfRanges("{3~7}", r(4), r(7), r(3, 7));
        checkOfRanges("{3~8}", r(4), r(7), r(3, 8));
        checkOfRanges("{3~9}", r(4), r(7), r(3, 9));

        checkOfRanges("{4,7}", r(4), r(7), r(4, 4));
        checkOfRanges("{4~5,7}", r(4), r(7), r(4, 5));
        checkOfRanges("{4~7}", r(4), r(7), r(4, 6));
        checkOfRanges("{4~7}", r(4), r(7), r(4, 7));
        checkOfRanges("{4~8}", r(4), r(7), r(4, 8));
        checkOfRanges("{4~9}", r(4), r(7), r(4, 9));

        checkOfRanges("{4~5,7}", r(4), r(7), r(5, 5));
        checkOfRanges("{4~7}", r(4), r(7), r(5, 6));
        checkOfRanges("{4~7}", r(4), r(7), r(5, 7));
        checkOfRanges("{4~8}", r(4), r(7), r(5, 8));
        checkOfRanges("{4~9}", r(4), r(7), r(5, 9));

        checkOfRanges("{4,6~7}", r(4), r(7), r(6, 6));
        checkOfRanges("{4,6~7}", r(4), r(7), r(6, 7));
        checkOfRanges("{4,6~8}", r(4), r(7), r(6, 8));
        checkOfRanges("{4,6~9}", r(4), r(7), r(6, 9));

        checkOfRanges("{4,7}", r(4), r(7), r(7, 7));
        checkOfRanges("{4,7~8}", r(4), r(7), r(7, 8));
        checkOfRanges("{4,7~9}", r(4), r(7), r(7, 9));

        checkOfRanges("{4,7~8}", r(4), r(7), r(8, 8));
        checkOfRanges("{4,7~9}", r(4), r(7), r(8, 9));

        checkOfRanges("{4,7,9}", r(4), r(7), r(9, 9));

        checkOfRanges("{1~2}", r(1), r(1, 2));
        checkOfRanges("{1~2}", r(2), r(1, 2));
        checkOfRanges("{1~2}", r(1, 2), r(1));
        checkOfRanges("{1~2}", r(1, 2), r(2));

        checkOfRanges("{1,3}", r(1), r(3));
        checkOfRanges("{1,3}", r(3), r(1));
    }

    @Test
    void testOfString() {
        checkOfString("{1}", "1");
        checkOfString("{1}", "1, 1");
        checkOfString("{1~2}", "2, 1");
        checkOfString("{1~3}", "2, 1, 3");
    }

    @Test
    void testIsSingleton() {
        assertFalse(s().isSingleton());
        assertFalse(s(r(1, 0)).isSingleton());
        assertTrue(s(r(1)).isSingleton());
        assertTrue(s(r(1, 1)).isSingleton());
        assertFalse(s(r(1), r(2)).isSingleton());
        assertFalse(s(r(1), r(3)).isSingleton());
    }

    @Test
    void testContains() {
        assertFalse(s().contains(1));

        assertFalse(s(r(1, 2)).contains(0));
        assertTrue(s(r(1, 2)).contains(1));
        assertTrue(s(r(1, 2)).contains(2));
        assertFalse(s(r(1, 2)).contains(3));

        assertTrue(s().contains(r(1, 0)));
        assertTrue(s(r(1)).contains(r(1, 0)));

        assertFalse(s(r(1, 2), r(4, 5)).contains(r(0)));
        assertTrue(s(r(1, 2), r(4, 5)).contains(r(1)));
        assertTrue(s(r(1, 2), r(4, 5)).contains(r(2)));
        assertFalse(s(r(1, 2), r(4, 5)).contains(r(3)));
        assertTrue(s(r(1, 2), r(4, 5)).contains(r(4)));
        assertTrue(s(r(1, 2), r(4, 5)).contains(r(5)));
        assertFalse(s(r(1, 2), r(4, 5)).contains(r(6)));
        assertTrue(s(r(1, 2), r(4, 5)).contains(r(1, 2)));
        assertTrue(s(r(1, 2), r(4, 5)).contains(r(4, 5)));

        assertFalse(s(r(1, 2), r(4, 5)).contains(r(0, 1)));

        assertTrue(s(r(1, 2), r(4, 5)).contains((SItem) r(1)));

    }

    @Test
    void testUnion() {
        assertEquals(s(r(1)), s().union(1));
        assertEquals(s(r(1)), s(r(1)).union(1));

        assertEquals(s(r(1)), s().union(r(1)));
        assertEquals(s(r(2)), s().union(r(2)));

        assertEquals(s(r(1)), s(r(1)).union(r(1)));
        assertEquals(s(r(1, 2)), s(r(1)).union(r(2)));

        assertEquals(s(r(1)), s(r(1)).union(s()));
        assertEquals(s(r(1, 2)), s(r(1)).union(s(r(2))));

        assertEquals(s(r(1, 2)), s(r(1)).union((SItemSet) s(r(2))));
        assertEquals(s(r(1, 2)), s(r(1)).union((SItem) r(2)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         s(r(1)).union(BooleanValue.TRUE);
                     });

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         s(r(1)).union(BooleanSet.TRUE);
                     });

    }

    @Test
    void testIntersection() {
        assertEquals(s(), s().intersection(1));
        assertEquals(s(r(1)), s(r(1)).intersection(1));
        assertEquals(s(), s(r(2)).intersection(1));

        assertEquals(s(), s().intersection(r(1)));
        assertEquals(s(r(1)), s(r(1)).intersection(r(1)));
        assertEquals(s(), s(r(2)).intersection(r(1)));
        assertEquals(s(r(2)), s(r(2)).intersection(r(1, 2)));

        assertEquals(s(), s(r(1, 3), r(5, 7)).intersection(r(0, 0)));
        assertEquals(s(r(1)), s(r(1, 3), r(5, 7)).intersection(r(0, 1)));
        assertEquals(s(r(1, 2)), s(r(1, 3), r(5, 7)).intersection(r(0, 2)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).intersection(r(0, 3)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).intersection(r(0, 4)));
        assertEquals(s(r(1, 3), r(5)), s(r(1, 3), r(5, 7)).intersection(r(0, 5)));
        assertEquals(s(r(1, 3), r(5, 6)), s(r(1, 3), r(5, 7)).intersection(r(0, 6)));
        assertEquals(s(r(1, 3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(0, 7)));
        assertEquals(s(r(1, 3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(0, 8)));

        assertEquals(s(r(1)), s(r(1, 3), r(5, 7)).intersection(r(1, 1)));
        assertEquals(s(r(1, 2)), s(r(1, 3), r(5, 7)).intersection(r(1, 2)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).intersection(r(1, 3)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).intersection(r(1, 4)));
        assertEquals(s(r(1, 3), r(5)), s(r(1, 3), r(5, 7)).intersection(r(1, 5)));
        assertEquals(s(r(1, 3), r(5, 6)), s(r(1, 3), r(5, 7)).intersection(r(1, 6)));
        assertEquals(s(r(1, 3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(1, 7)));
        assertEquals(s(r(1, 3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(1, 8)));

        assertEquals(s(r(2)), s(r(1, 3), r(5, 7)).intersection(r(2, 2)));
        assertEquals(s(r(2, 3)), s(r(1, 3), r(5, 7)).intersection(r(2, 3)));
        assertEquals(s(r(2, 3)), s(r(1, 3), r(5, 7)).intersection(r(2, 4)));
        assertEquals(s(r(2, 3), r(5)), s(r(1, 3), r(5, 7)).intersection(r(2, 5)));
        assertEquals(s(r(2, 3), r(5, 6)), s(r(1, 3), r(5, 7)).intersection(r(2, 6)));
        assertEquals(s(r(2, 3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(2, 7)));
        assertEquals(s(r(2, 3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(2, 8)));

        assertEquals(s(r(3)), s(r(1, 3), r(5, 7)).intersection(r(3, 3)));
        assertEquals(s(r(3)), s(r(1, 3), r(5, 7)).intersection(r(3, 4)));
        assertEquals(s(r(3), r(5)), s(r(1, 3), r(5, 7)).intersection(r(3, 5)));
        assertEquals(s(r(3), r(5, 6)), s(r(1, 3), r(5, 7)).intersection(r(3, 6)));
        assertEquals(s(r(3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(3, 7)));
        assertEquals(s(r(3), r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(3, 8)));

        assertEquals(s(), s(r(1, 3), r(5, 7)).intersection(r(4, 4)));
        assertEquals(s(r(5)), s(r(1, 3), r(5, 7)).intersection(r(4, 5)));
        assertEquals(s(r(5, 6)), s(r(1, 3), r(5, 7)).intersection(r(4, 6)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(4, 7)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(4, 8)));

        assertEquals(s(r(5)), s(r(1, 3), r(5, 7)).intersection(r(5, 5)));
        assertEquals(s(r(5, 6)), s(r(1, 3), r(5, 7)).intersection(r(5, 6)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(5, 7)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).intersection(r(5, 8)));

        assertEquals(s(r(6)), s(r(1, 3), r(5, 7)).intersection(r(6, 6)));
        assertEquals(s(r(6, 7)), s(r(1, 3), r(5, 7)).intersection(r(6, 7)));
        assertEquals(s(r(6, 7)), s(r(1, 3), r(5, 7)).intersection(r(6, 8)));

        assertEquals(s(r(7)), s(r(1, 3), r(5, 7)).intersection(r(7, 7)));
        assertEquals(s(r(7)), s(r(1, 3), r(5, 7)).intersection(r(7, 8)));

        assertEquals(s(), s(r(1, 3), r(5, 7)).intersection(r(8, 8)));

        assertEquals(s(r(3), r(5, 7), r(9)), s(r(1, 3), r(5, 7), r(9, 11)).intersection(r(3, 9)));

        assertEquals(s(r(3), r(5, 7), r(9)), s(r(1, 3), r(5, 7), r(9, 11)).intersection(s(r(3, 9))));

        assertEquals(s(), s().intersection(s()));
        assertEquals(s(), s(r(1)).intersection(s()));

        assertEquals(s(r(1)), s(r(1)).intersection((SItem) r(1)));
        assertEquals(s(r(1)), s(r(1)).intersection((SItemSet) s(r(1))));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         assertEquals(s(r(1)), s(r(1)).intersection(BooleanSet.TRUE));
                     });
    }

    @Test
    void testRemove() {
        assertEquals(s(), s().remove(1));
        assertEquals(s(), s(r(1)).remove(1));
        assertEquals(s(r(2, 3)), s(r(1, 3)).remove(1));
        assertEquals(s(r(1), r(3)), s(r(1, 3)).remove(2));

        assertEquals(s(r(1, 3)), s(r(1, 3)).remove(0));
        assertEquals(s(r(2, 3)), s(r(1, 3)).remove(1));
        assertEquals(s(r(1), r(3)), s(r(1, 3)).remove(2));
        assertEquals(s(r(1, 2)), s(r(1, 3)).remove(3));
        assertEquals(s(r(1, 3)), s(r(1, 3)).remove(4));
        assertEquals(s(r(1, 3)), s(r(1, 3)).remove(5));

        assertEquals(s(), s().remove(r(1)));
        assertEquals(s(r(1, 3)), s(r(1, 3)).remove(r(1, 0)));
        assertEquals(s(r(1, 3)), s(r(1, 3)).remove(r(0)));
        assertEquals(s(r(2, 3)), s(r(1, 3)).remove(r(1)));
        assertEquals(s(r(1), r(3)), s(r(1, 3)).remove(r(2)));
        assertEquals(s(r(1, 2)), s(r(1, 3)).remove(r(3)));
        assertEquals(s(r(1, 3)), s(r(1, 3)).remove(r(4)));
        assertEquals(s(r(1, 3)), s(r(1, 3)).remove(r(5)));

        assertEquals(s(r(2, 3)), s(r(1, 3)).remove(r(0, 1)));
        assertEquals(s(r(3)), s(r(1, 3)).remove(r(0, 2)));
        assertEquals(s(), s(r(1, 3)).remove(r(0, 3)));
        assertEquals(s(), s(r(1, 3)).remove(r(0, 4)));

        assertEquals(s(r(2, 3)), s(r(1, 3)).remove(r(1, 1)));
        assertEquals(s(r(3)), s(r(1, 3)).remove(r(1, 2)));
        assertEquals(s(), s(r(1, 3)).remove(r(1, 3)));
        assertEquals(s(), s(r(1, 3)).remove(r(1, 4)));

        assertEquals(s(r(1), r(3)), s(r(1, 3)).remove(r(2, 2)));
        assertEquals(s(r(1)), s(r(1, 3)).remove(r(2, 3)));
        assertEquals(s(r(1)), s(r(1, 3)).remove(r(2, 4)));

        assertEquals(s(r(1, 2)), s(r(1, 3)).remove(r(3, 3)));
        assertEquals(s(r(1, 2)), s(r(1, 3)).remove(r(3, 4)));

        assertEquals(s(r(1, 3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(0, 0)));
        assertEquals(s(r(2, 3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(0, 1)));
        assertEquals(s(r(3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(0, 2)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(0, 3)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(0, 4)));
        assertEquals(s(r(6, 7)), s(r(1, 3), r(5, 7)).remove(r(0, 5)));
        assertEquals(s(r(7)), s(r(1, 3), r(5, 7)).remove(r(0, 6)));
        assertEquals(s(), s(r(1, 3), r(5, 7)).remove(r(0, 7)));
        assertEquals(s(), s(r(1, 3), r(5, 7)).remove(r(0, 8)));

        assertEquals(s(r(2, 3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(1, 1)));
        assertEquals(s(r(3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(1, 2)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(1, 3)));
        assertEquals(s(r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(1, 4)));
        assertEquals(s(r(6, 7)), s(r(1, 3), r(5, 7)).remove(r(1, 5)));
        assertEquals(s(r(7)), s(r(1, 3), r(5, 7)).remove(r(1, 6)));
        assertEquals(s(), s(r(1, 3), r(5, 7)).remove(r(1, 7)));
        assertEquals(s(), s(r(1, 3), r(5, 7)).remove(r(1, 8)));

        assertEquals(s(r(1), r(3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(2, 2)));
        assertEquals(s(r(1), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(2, 3)));
        assertEquals(s(r(1), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(2, 4)));
        assertEquals(s(r(1), r(6, 7)), s(r(1, 3), r(5, 7)).remove(r(2, 5)));
        assertEquals(s(r(1), r(7)), s(r(1, 3), r(5, 7)).remove(r(2, 6)));
        assertEquals(s(r(1)), s(r(1, 3), r(5, 7)).remove(r(2, 7)));
        assertEquals(s(r(1)), s(r(1, 3), r(5, 7)).remove(r(2, 8)));

        assertEquals(s(r(1, 2), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(3, 3)));
        assertEquals(s(r(1, 2), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(3, 4)));
        assertEquals(s(r(1, 2), r(6, 7)), s(r(1, 3), r(5, 7)).remove(r(3, 5)));
        assertEquals(s(r(1, 2), r(7)), s(r(1, 3), r(5, 7)).remove(r(3, 6)));
        assertEquals(s(r(1, 2)), s(r(1, 3), r(5, 7)).remove(r(3, 7)));
        assertEquals(s(r(1, 2)), s(r(1, 3), r(5, 7)).remove(r(3, 8)));

        assertEquals(s(r(1, 3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(4, 4)));
        assertEquals(s(r(1, 3), r(6, 7)), s(r(1, 3), r(5, 7)).remove(r(4, 5)));
        assertEquals(s(r(1, 3), r(7)), s(r(1, 3), r(5, 7)).remove(r(4, 6)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).remove(r(4, 7)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).remove(r(4, 8)));

        assertEquals(s(r(1, 3), r(6, 7)), s(r(1, 3), r(5, 7)).remove(r(5, 5)));
        assertEquals(s(r(1, 3), r(7)), s(r(1, 3), r(5, 7)).remove(r(5, 6)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).remove(r(5, 7)));
        assertEquals(s(r(1, 3)), s(r(1, 3), r(5, 7)).remove(r(5, 8)));

        assertEquals(s(r(1, 3), r(5), r(7)), s(r(1, 3), r(5, 7)).remove(r(6, 6)));
        assertEquals(s(r(1, 3), r(5)), s(r(1, 3), r(5, 7)).remove(r(6, 7)));
        assertEquals(s(r(1, 3), r(5)), s(r(1, 3), r(5, 7)).remove(r(6, 8)));

        assertEquals(s(r(1, 3), r(5, 6)), s(r(1, 3), r(5, 7)).remove(r(7, 7)));
        assertEquals(s(r(1, 3), r(5, 6)), s(r(1, 3), r(5, 7)).remove(r(7, 8)));

        assertEquals(s(r(1, 3), r(5, 7)), s(r(1, 3), r(5, 7)).remove(r(8, 8)));

        assertEquals(s(r(1, 2), r(10, 11)), s(r(1, 3), r(5, 7), r(9, 11)).remove(r(3, 9)));

        assertEquals(s(), s().remove(s(r(1))));
        assertEquals(s(r(1)), s(r(1)).remove(s()));

        assertEquals(s(r(1, 2), r(10, 11)), s(r(1, 3), r(5, 7), r(9, 11)).remove(s(r(3, 9))));
        assertEquals(s(r(1, 3), r(5, 7), r(9, 11)), s(r(1, 3), r(5, 7), r(9, 11)).remove(s()));

        assertEquals(s(r(2, 3)), s(r(1, 3)).remove((SItem) r(1)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         s(r(1, 3)).remove(BooleanSet.TRUE);
                     });

    }

    @Test
    void testConvert() {
        assertEquals(IntegerSet.EMPTY, IntegerSet.convert(BooleanSet.EMPTY));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         IntegerSet.convert(BooleanSet.TRUE);
                     });
    }

    @Test
    void testEquals() {
        assertEquals(s(r(1), r(2)), s(r(2), r(1)));
        assertNotEquals(s(r(1), r(2)), null);
        assertNotEquals(s(r(1), r(2)), 1);
        assertNotEquals(s(), 1);
        assertEquals(s(), s());
        assertEquals(s(), s(r(1, 0)));
        assertNotEquals(s(), s(r(0)));
    }

    @Test
    void testHashCode() {
        assertEquals(s().hashCode(), s().hashCode());
        assertEquals(s().hashCode(), s(r(1, 0)).hashCode());
        assertEquals(s(r(1)).hashCode(), s(r(1)).hashCode());
    }

    @Test
    void testLexicographicComparator() {
        assertSame(0, IntegerSet.LEXICOGRAPHIC_COMPARATOR.compare(IntegerSet.EMPTY, IntegerSet.EMPTY));
        assertSame(0, IntegerSet.LEXICOGRAPHIC_COMPARATOR.compare(s(), s()));
        assertTrue(IntegerSet.LEXICOGRAPHIC_COMPARATOR.compare(s(), null) > 0);
        assertTrue(IntegerSet.LEXICOGRAPHIC_COMPARATOR.compare(null, s()) < 0);

        assertTrue(IntegerSet.LEXICOGRAPHIC_COMPARATOR.compare(s(r(1)), s()) > 0);
        assertTrue(IntegerSet.LEXICOGRAPHIC_COMPARATOR.compare(s(r(1, 2)), s()) > 0);
        assertTrue(IntegerSet.LEXICOGRAPHIC_COMPARATOR.compare(s(r(1)), s(r(2))) < 0);
        assertTrue(IntegerSet.LEXICOGRAPHIC_COMPARATOR.compare(s(r(1, 2)), s(r(2))) < 0);
        assertTrue(IntegerSet.LEXICOGRAPHIC_COMPARATOR.compare(s(r(2)), s(r(1))) > 0);
        assertTrue(IntegerSet.LEXICOGRAPHIC_COMPARATOR.compare(s(r(1, 2)), s(r(1))) > 0);
        assertTrue(IntegerSet.LEXICOGRAPHIC_COMPARATOR.compare(s(r(1, 3)), s(r(1, 2))) > 0);
        assertTrue(IntegerSet.LEXICOGRAPHIC_COMPARATOR.compare(s(r(1, 2)), s(r(1, 3))) < 0);
    }

    @Test
    void testMisc() {
        final IntegerSet s1 = s(r(1));
        assertEquals(v(1), s1.getSingletonValue());
    }

    @Test
    void testBug199() {
        final IntegerSet set = IntegerSet.of("10,9,8");
        assertTrue(set.contains(8));
        checkOfString("{1~2}", "2, 1");
    }
}