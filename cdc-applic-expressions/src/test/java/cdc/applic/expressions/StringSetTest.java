package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.AbstractSItemSet;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.content.UncheckedSet;

class StringSetTest {
    private static final Logger LOGGER = LogManager.getLogger(StringSetTest.class);

    private static StringValue v(String text) {
        return StringValue.of(text, false);
    }

    private static StringSet s(String content) {
        return StringSet.of(content);
    }

    @SafeVarargs
    private static StringSet s(StringValue... values) {
        return StringSet.of(values);
    }

    @Test
    void checkEmpty() {
        assertTrue(StringSet.EMPTY.isEmpty());
        assertEquals(StringSet.EMPTY, IntegerSet.EMPTY);
        assertEquals(StringSet.EMPTY, UncheckedSet.EMPTY);
        assertEquals(StringSet.EMPTY, RealSet.EMPTY);
        assertEquals(StringSet.EMPTY, BooleanSet.EMPTY);
        assertEquals(StringSet.EMPTY, StringSet.EMPTY);
    }

    @Test
    void checkIsSingleton() {
        assertFalse(StringSet.EMPTY.isSingleton());
        assertFalse(s("???").isSingleton());
        assertTrue(s("X").isSingleton());
        assertTrue(s("X,X").isSingleton());
        assertFalse(s("X,Y").isSingleton());
    }

    private static void checkEquals(boolean expected,
                                    String s1,
                                    String s2) {
        final StringSet set1 = s(s1);
        final StringSet set2 = s(s2);
        LOGGER.debug(set1 + " equals " + set2 + ": " + set1.equals(set2));
        if (expected) {
            assertEquals(set1, set2);
        } else {
            assertNotEquals(set1, set2);
        }
    }

    @Test
    void checkEquals() {
        checkEquals(true, "A, B", "A,   B");
        checkEquals(true, "A, B", "A,B");
    }

    private static void checkConstruction(String expectedContent,
                                          String s) {
        final StringSet set = s(s);
        LOGGER.debug("String(" + s + "): " + set);
        assertEquals(expectedContent, set.getContent());

        assertEquals(set, set.getChecked());
        assertEquals(set, set.getBest());
        assertTrue(set.isValid());
        assertTrue(set.isChecked());

        final StringSet set2 = StringSet.of(set.getItems());
        assertEquals(expectedContent, set2.getContent());

        final StringSet set3 = StringSet.of(set.getItems().toArray(new StringValue[0]));
        assertEquals(expectedContent, set3.getContent());

        assertEquals(set.isEmpty(), set.isCompliantWith(BooleanSet.class));
        assertEquals(set.isEmpty(), set.isCompliantWith(IntegerSet.class));
        assertEquals(set.isEmpty(), set.isCompliantWith(RealSet.class));
        assertTrue(set.isCompliantWith(StringSet.class));
        assertTrue(set.isCompliantWith(UncheckedSet.class));
    }

    @Test
    void testConstruction() {
        checkConstruction("", "");
        checkConstruction("A", "A");
        checkConstruction("\"1\"", "\"1\"");
        checkConstruction("A", " A ");
        checkConstruction("A,B", " A , B");
        checkConstruction("A,B", " A,A , B, A");
        checkConstruction("\"A A\",A", "  \"A A\"  , A  ");
        checkConstruction("\"A \"\"A\",A", "  \"A \"\"A\"  , A  ");
        checkConstruction("\"A \",A", "  \"A \",\"A\"  , A  ");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         StringSet.of((StringValue) null);
                     });

        assertEquals(s("A,B"), s(v("A"), v("B"), v("A")));
    }

    @Test
    void testConvert() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         StringSet.convert(BooleanSet.TRUE);
                     });
    }

    @Test
    void testContains() {
        assertTrue(s("A").contains((SItem) v("A")));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         s("A").contains(BooleanValue.TRUE);
                     });
    }

    private static void checkUnion(StringSet set1,
                                   AbstractSItemSet set2,
                                   String content) {
        final StringSet set = set1.union(set2);
        LOGGER.debug(set1 + " enumerated union " + set2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    private static void checkUnion(String s1,
                                   String s2,
                                   String content) {
        final StringSet set1 = s(s1);
        checkUnion(set1, s(s2), content);
        checkUnion(set1, UncheckedSet.of(s2), content);

    }

    @Test
    void testUnion() {
        checkUnion("", "", "");
        checkUnion("A", "", "A");
        checkUnion("", "A", "A");
        checkUnion("A", "A", "A");
        checkUnion("A", "A", "A");
        checkUnion("A", "B", "A,B");
        checkUnion("A,B", "B", "A,B");
        checkUnion("A,B", "A,B", "A,B");
        checkUnion("A,B", "B,A", "A,B");
        checkUnion("A,B", "A,C", "A,B,C");

        assertEquals(s("A,B"), s("A").union((SItem) v("B")));
        assertEquals(s("A,B"), s("A,B").union(v("B")));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         s("A").union(BooleanValue.TRUE);
                     });
    }

    private static void checkIntersection(StringSet set1,
                                          AbstractSItemSet set2,
                                          String content) {
        final StringSet set = set1.intersection(set2);
        LOGGER.debug(set1 + " string intersection " + set2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    private static void checkIntersection(String s1,
                                          String s2,
                                          String content) {
        final StringSet set1 = s(s1);
        checkIntersection(set1, s(s2), content);
        checkIntersection(set1, UncheckedSet.of(s2), content);
    }

    @Test
    void testIntersection() {
        checkIntersection("", "", "");
        checkIntersection("A", "", "");
        checkIntersection("A,B", "", "");
        checkIntersection("", "A", "");
        checkIntersection("", "A,B", "");
        checkIntersection("A", "A", "A");
        checkIntersection("A,B", "A", "A");
        checkIntersection("B,A", "A", "A");
        checkIntersection("B,A", "A,B", "B,A");
        checkIntersection("A", "B", "");

        assertEquals(s("A"), s("A,B").intersection((SItem) v("A")));
        assertEquals(s(), s("A,B").intersection(v("C")));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         s("A").intersection(BooleanValue.TRUE);
                     });
    }

    private static void checkRemove(StringSet set1,
                                    AbstractSItemSet set2,
                                    String content) {
        final StringSet set = set1.remove(set2);
        LOGGER.debug(set1 + " string remove " + set2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    private static void checkRemove(String s1,
                                    String s2,
                                    String content) {
        final StringSet set1 = StringSet.of(s1);
        checkRemove(set1, StringSet.of(s2), content);
        checkRemove(set1, UncheckedSet.of(s2), content);
    }

    @Test
    void testRemove() {
        checkRemove("", "", "");
        checkRemove("A", "", "A");
        checkRemove("A,B", "", "A,B");
        checkRemove("A,B,C", "", "A,B,C");
        checkRemove("", "A", "");
        checkRemove("A", "A", "");
        checkRemove("A,B", "A", "B");
        checkRemove("A,B,C", "A", "B,C");
        checkRemove("", "B", "");
        checkRemove("A", "B", "A");
        checkRemove("A,B", "B", "A");
        checkRemove("A,B,C", "B", "A,C");
        checkRemove("", "C", "");
        checkRemove("A", "C", "A");
        checkRemove("A,B", "C", "A,B");
        checkRemove("A,B,C", "C", "A,B");

        assertEquals(s("A"), s("A,B").remove((SItem) v("B")));
        assertEquals(s("A"), s("A").remove(v("B")));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         s("A").remove(BooleanValue.TRUE);
                     });
    }

    @Test
    void testEquals() {
        assertEquals(StringSet.EMPTY, IntegerSet.EMPTY);
        assertNotEquals(StringSet.EMPTY, BooleanSet.TRUE);
        assertNotEquals(StringSet.EMPTY, null);
    }

    @Test
    void testHashCode() {
        assertEquals(StringSet.EMPTY.hashCode(), StringSet.EMPTY.hashCode());
    }

    @Test
    void testLexicographicCompare() {
        assertSame(0, StringSet.LEXICOGRAPHIC_COMPARATOR.compare(StringSet.EMPTY, StringSet.EMPTY));
        assertSame(0, StringSet.LEXICOGRAPHIC_COMPARATOR.compare(s(""), s("")));
        assertTrue(StringSet.LEXICOGRAPHIC_COMPARATOR.compare(null, s("")) < 0);
        assertTrue(StringSet.LEXICOGRAPHIC_COMPARATOR.compare(s(""), null) > 0);
        assertSame(0, StringSet.LEXICOGRAPHIC_COMPARATOR.compare(null, null));
        assertTrue(StringSet.LEXICOGRAPHIC_COMPARATOR.compare(s("A"), s("")) > 0);
        assertTrue(StringSet.LEXICOGRAPHIC_COMPARATOR.compare(s(""), s("A")) < 0);
        assertTrue(StringSet.LEXICOGRAPHIC_COMPARATOR.compare(s("A,AA"), s("A,AB")) < 0);
        assertTrue(StringSet.LEXICOGRAPHIC_COMPARATOR.compare(s("A,AA"), s("A,AA,B")) < 0);
        assertTrue(StringSet.LEXICOGRAPHIC_COMPARATOR.compare(s("A,AB"), s("A,AA,A")) > 0);

        final Comparator<StringSet> comparator = StringSet.lexicographicComparator(Comparator.naturalOrder());
        assertSame(0, comparator.compare(StringSet.EMPTY, StringSet.EMPTY));
        assertSame(0, comparator.compare(s(""), s("")));
        assertTrue(comparator.compare(null, s("")) < 0);
        assertTrue(comparator.compare(s(""), null) > 0);
        assertSame(0, comparator.compare(null, null));
        assertTrue(comparator.compare(s("A"), s("")) > 0);
        assertTrue(comparator.compare(s(""), s("A")) < 0);
        assertTrue(comparator.compare(s("A,AA"), s("A,AB")) < 0);
        assertTrue(comparator.compare(s("A,AA"), s("A,AA,B")) < 0);
        assertTrue(comparator.compare(s("A,AB"), s("A,AA,A")) > 0);
    }

    @Test
    void testMisc() {
        final StringSet s = s("A");
        assertEquals(v("A"), s.getSingletonValue());
    }

    @Test
    void testPatternUnknown() {
        final StringSet unknown = StringSet.of("???");
        final StringSet a = StringSet.of("A");
        final StringSet aUnknown = StringSet.of("A, ???");
        final StringSet b = StringSet.of("B");
        final StringSet bUnknown = StringSet.of("B, ???");
        final StringSet ab = StringSet.of("A, B");
        final StringSet abUnknown = StringSet.of("A, B, ???");

        assertEquals("???", unknown.getContent());
        assertEquals("A", a.getContent());
        assertEquals("A,???", aUnknown.getContent());
        assertEquals("B", b.getContent());
        assertEquals("B,???", bUnknown.getContent());
        assertEquals("A,B", ab.getContent());
        assertEquals("A,B,???", abUnknown.getContent());

        assertTrue(unknown.contains(unknown));
        assertFalse(unknown.contains(a));
        assertFalse(unknown.contains(aUnknown));
        assertFalse(unknown.contains(b));
        assertFalse(unknown.contains(bUnknown));
        assertFalse(unknown.contains(ab));
        assertFalse(unknown.contains(abUnknown));

        assertTrue(aUnknown.contains(unknown));
        assertTrue(aUnknown.contains(a));
        assertTrue(aUnknown.contains(aUnknown));
        assertFalse(aUnknown.contains(b));
        assertFalse(aUnknown.contains(bUnknown));
        assertFalse(aUnknown.contains(ab));
        assertFalse(aUnknown.contains(abUnknown));

        assertTrue(abUnknown.contains(unknown));
        assertTrue(abUnknown.contains(a));
        assertTrue(abUnknown.contains(aUnknown));
        assertTrue(abUnknown.contains(b));
        assertTrue(abUnknown.contains(bUnknown));
        assertTrue(abUnknown.contains(ab));
        assertTrue(abUnknown.contains(abUnknown));

        assertFalse(a.contains(unknown));
        assertTrue(a.contains(a));
        assertFalse(a.contains(aUnknown));
        assertFalse(a.contains(b));
        assertFalse(a.contains(bUnknown));
        assertFalse(a.contains(ab));
        assertFalse(a.contains(abUnknown));

        assertEquals("???", unknown.union(unknown).getContent());
        assertEquals("???,A", unknown.union(a).getContent());
        assertEquals("???,A", unknown.union(aUnknown).getContent());
        assertEquals("???,B", unknown.union(b).getContent());
        assertEquals("???,B", unknown.union(bUnknown).getContent());
        assertEquals("???,A,B", unknown.union(ab).getContent());
        assertEquals("???,A,B", unknown.union(abUnknown).getContent());

        assertEquals("???", unknown.intersection(unknown).getContent());
        assertEquals("", unknown.intersection(a).getContent());
        assertEquals("???", unknown.intersection(aUnknown).getContent());
        assertEquals("", unknown.intersection(b).getContent());
        assertEquals("???", unknown.intersection(bUnknown).getContent());
        assertEquals("", unknown.intersection(ab).getContent());
        assertEquals("???", unknown.intersection(abUnknown).getContent());

        assertEquals("", unknown.remove(unknown).getContent());
        assertEquals("???", unknown.remove(a).getContent());
        assertEquals("", unknown.remove(aUnknown).getContent());
        assertEquals("???", unknown.remove(b).getContent());
        assertEquals("", unknown.remove(bUnknown).getContent());
        assertEquals("???", unknown.remove(ab).getContent());
        assertEquals("", unknown.remove(abUnknown).getContent());
    }
}