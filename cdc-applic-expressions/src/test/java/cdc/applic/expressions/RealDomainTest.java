package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.Position;
import cdc.applic.expressions.content.RealDomain;
import cdc.applic.expressions.content.RealRange;
import cdc.applic.expressions.content.RealValue;

class RealDomainTest {
    private static final Logger LOGGER = LogManager.getLogger(RealDomainTest.class);

    private static RealValue v(double value) {
        return RealValue.of(value);
    }

    private static double checkUp(int loops,
                                  double from) {
        LOGGER.debug("checkUp(" + from + ")");
        double d = from;
        RealValue v = RealValue.of(from);
        for (int index = 0; index < loops; index++) {
            d = Math.nextUp(d);
            final RealValue pv = v;
            v = RealDomain.INSTANCE.succ(v);
            LOGGER.debug("   >>> " + v);
            assertEquals(0.0, Math.abs(d - v.getNumber()));
            assertEquals(Position.ADJOINT_PRED, RealDomain.INSTANCE.position(pv, v));
            assertTrue(RealDomain.INSTANCE.adjoint(pv, v));
        }
        return d;
    }

    private static double checkDown(int loops,
                                    double from) {
        LOGGER.debug("checkDown(" + from + ")");
        double d = from;
        RealValue v = RealValue.of(from);
        for (int index = 0; index < loops; index++) {
            d = Math.nextDown(d);
            final RealValue pv = v;
            v = RealDomain.INSTANCE.pred(v);
            LOGGER.debug("   <<< " + v + " " + d);
            assertEquals(0.0, Math.abs(d - v.getNumber()));
            assertEquals(Position.ADJOINT_SUCC, RealDomain.INSTANCE.position(pv, v));
            assertTrue(RealDomain.INSTANCE.adjoint(pv, v));
        }
        return d;
    }

    private static void checkUpDown(int loops,
                                    double from) {
        LOGGER.debug("checkUpDown(" + from + ")");
        double d = checkUp(loops, from);
        d = checkDown(loops, d);
        assertEquals(0.0, from - d);
    }

    private static void checkDownUp(int loops,
                                    double from) {
        LOGGER.debug("checkDownUp(" + from + ")");
        double d = checkDown(loops, from);
        d = checkUp(loops, d);
        assertEquals(0.0, from - d);
    }

    @Test
    void test() {
        assertNotEquals(0.0, -0.0);
        checkUpDown(10, 0.0);
        checkDownUp(10, 0.0);

        assertTrue(Math.nextDown(Math.nextUp(0.0)) == 0.0);
        assertTrue(Math.nextUp(Math.nextDown(0.0)) == 0.0);

        assertTrue(Math.nextUp(0.0) > 0.0);
        assertTrue(Math.nextDown(0.0) < 0.0);

        assertTrue(RealDomain.INSTANCE.pred(0.0).getNumber() < 0.0);
        assertTrue(RealDomain.INSTANCE.succ(0.0).getNumber() > 0.0);

        assertEquals(v(Math.nextDown(0.0)), RealDomain.INSTANCE.pred(0.0));
        assertEquals(v(Math.nextUp(0.0)), RealDomain.INSTANCE.succ(0.0));

        assertEquals(RealValue.MIN_VALUE, RealDomain.INSTANCE.min());
        assertEquals(RealValue.MAX_VALUE, RealDomain.INSTANCE.max());

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RealDomain.INSTANCE.succ(RealDomain.INSTANCE.max());
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RealDomain.INSTANCE.pred(RealDomain.INSTANCE.min());
                     });
        assertEquals(RealValue.class, RealDomain.INSTANCE.getValueClass());
        assertEquals(RealRange.class, RealDomain.INSTANCE.getRangeClass());

        assertTrue(RealDomain.INSTANCE.create(v(1)).isSingleton());
        assertTrue(RealDomain.INSTANCE.create(v(1), v(1)).isSingleton());
        assertEquals(RealDomain.INSTANCE.minComparator(), RealRange.MIN_COMPARATOR);
        assertEquals(RealDomain.INSTANCE.maxComparator(), RealRange.MAX_COMPARATOR);
    }

    @Test
    void testAdjoint() {
        assertFalse(RealDomain.INSTANCE.adjoint(0, -2));
        assertTrue(RealDomain.INSTANCE.adjoint(Math.nextDown(0.0), 0.0));
        assertTrue(RealDomain.INSTANCE.adjoint(0.0, Math.nextDown(0.0)));
        assertFalse(RealDomain.INSTANCE.adjoint(0, 0));
        assertTrue(RealDomain.INSTANCE.adjoint(Math.nextUp(0.0), 0.0));
        assertTrue(RealDomain.INSTANCE.adjoint(0.0, Math.nextUp(0.0)));
        assertFalse(RealDomain.INSTANCE.adjoint(0, 2));
    }

    @Test
    void testPosition() {
        assertEquals(Position.DISJOINT_PRED, RealDomain.INSTANCE.position(-2, 0));
        assertEquals(Position.ADJOINT_PRED, RealDomain.INSTANCE.position(Math.nextDown(0.0), 0.0));
        assertEquals(Position.ADJOINT_PRED, RealDomain.INSTANCE.position(0.0, Math.nextUp(0.0)));
        assertEquals(Position.OVERLAPPING, RealDomain.INSTANCE.position(0, 0));
        assertEquals(Position.ADJOINT_SUCC, RealDomain.INSTANCE.position(Math.nextUp(0.0), 0.0));
        assertEquals(Position.ADJOINT_SUCC, RealDomain.INSTANCE.position(0.0, Math.nextDown(0.0)));
        assertEquals(Position.DISJOINT_SUCC, RealDomain.INSTANCE.position(2, 0));
    }
}