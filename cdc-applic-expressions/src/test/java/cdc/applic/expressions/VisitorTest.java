package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.AbstractAnalyzer;
import cdc.applic.expressions.ast.visitors.AbstractCollector;
import cdc.applic.expressions.ast.visitors.AbstractConverter;

class VisitorTest {
    private static class Analyzer extends AbstractAnalyzer {
        int binaryCount = 0;
        int naryCount = 0;
        int unaryCount = 0;
        int leafCount = 0;

        public Analyzer() {
        }

        @Override
        public Void visitBinary(AbstractBinaryNode node) {
            binaryCount++;
            return super.visitBinary(node);
        }

        @Override
        public Void visitNary(AbstractNaryNode node) {
            naryCount++;
            return super.visitNary(node);
        }

        @Override
        public Void visitUnary(AbstractUnaryNode node) {
            unaryCount++;
            return super.visitUnary(node);
        }

        @Override
        public Void visitLeaf(AbstractLeafNode node) {
            leafCount++;
            return super.visitLeaf(node);
        }
    }

    private static class IdConverter extends AbstractConverter {
        public IdConverter() {
        }
    }

    private static class Collector extends AbstractCollector<Node> {
        public Collector(Set<Node> set) {
            super(set);
        }

        public Collector(List<Node> list) {
            super(list);
        }

        @Override
        public Void visitBinary(AbstractBinaryNode node) {
            add(node);
            return super.visitBinary(node);
        }

        @Override
        public Void visitNary(AbstractNaryNode node) {
            add(node);
            return super.visitNary(node);
        }

        @Override
        public Void visitUnary(AbstractUnaryNode node) {
            add(node);
            return super.visitUnary(node);
        }

        @Override
        public Void visitLeaf(AbstractLeafNode node) {
            add(node);
            return super.visitLeaf(node);
        }
    }

    @Test
    void test() {
        final Node n = new NaryAndNode(new AndNode(new NotNode(TrueNode.INSTANCE), TrueNode.INSTANCE));
        final Analyzer analyzer = new Analyzer();
        n.accept(analyzer);
        assertEquals(1, analyzer.naryCount);
        assertEquals(1, analyzer.binaryCount);
        assertEquals(1, analyzer.unaryCount);
        assertEquals(2, analyzer.leafCount);

        final IdConverter converter = new IdConverter();
        final Node n1 = n.accept(converter);
        assertEquals(n, n1);

        final Collector collector1 = new Collector(new HashSet<>());
        n.accept(collector1);
        assertEquals(4, collector1.getSet().size());

        final Collector collector2 = new Collector(new ArrayList<>());
        n.accept(collector2);
        assertEquals(5, collector2.getList().size());
        assertEquals(5, collector2.getCollection().size());
    }
}