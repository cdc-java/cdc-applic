package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.literals.EscapingUtils;

class EscapingUtilsTest {
    private static void testNeedsEscape(String s,
                                        boolean expected) {
        assertEquals(expected, EscapingUtils.needsDoubleQuotesEscape(s), "'" + s + "'");
    }

    @Test
    void testNeedsEscape() {
        // Illegal literals
        testNeedsEscape(null, false);
        testNeedsEscape("", false);

        // Legal literals (but not necessarily valid)
        testNeedsEscape(" ", true);
        testNeedsEscape(".", true);
        testNeedsEscape("$", true);
        testNeedsEscape("{", true);
        testNeedsEscape("}", true);
        testNeedsEscape("=", true);
        testNeedsEscape("<", true);
        testNeedsEscape(">", true);
        testNeedsEscape("-", false);
        testNeedsEscape("(", true);
        testNeedsEscape(")", true);
        testNeedsEscape("!", true);
        testNeedsEscape("&", true);
        testNeedsEscape("\"", true);
        testNeedsEscape("ABCDEFGHIJKLMNOPQRSTUVWXYZ", false);
        testNeedsEscape("abcdefghijklmnopqrstuvwxyz", false);
        testNeedsEscape("0123456789", true);
        testNeedsEscape("+1", true);
        testNeedsEscape("-1", true);
        testNeedsEscape("A1", false);
        testNeedsEscape("1A", true);
        testNeedsEscape("not", true);
        testNeedsEscape("and", true);
        testNeedsEscape("or", true);
        testNeedsEscape("imp", true);
        testNeedsEscape("iff", true);
        testNeedsEscape("to", true);
        testNeedsEscape("10", true);
        testNeedsEscape("10.0", true);

        testNeedsEscape("a+", false);
        testNeedsEscape("7a", true); // We could accept this without escape
    }

    @Test
    void testIsLegalLiteral() {
        assertTrue(EscapingUtils.isLegalLiteral(" "));
        assertTrue(EscapingUtils.isLegalLiteral("1"));
        assertTrue(EscapingUtils.isLegalLiteral("$"));
        assertTrue(EscapingUtils.isLegalLiteral("\""));

        assertFalse(EscapingUtils.isLegalLiteral(null));
        assertFalse(EscapingUtils.isLegalLiteral(""));
        assertFalse(EscapingUtils.isLegalLiteral("|"));
        assertFalse(EscapingUtils.isLegalLiteral("~"));
    }

    @Test
    void testIsValidEscapedText() {
        assertFalse(EscapingUtils.isValidDoubleQuotesEscapedText(null));
        assertFalse(EscapingUtils.isValidDoubleQuotesEscapedText("abc"));
        assertFalse(EscapingUtils.isValidDoubleQuotesEscapedText("$"));
        assertFalse(EscapingUtils.isValidDoubleQuotesEscapedText("\"abc"));
        assertTrue(EscapingUtils.isValidDoubleQuotesEscapedText("\"abc\""));
        assertFalse(EscapingUtils.isValidDoubleQuotesEscapedText("\"\"\""));
        assertTrue(EscapingUtils.isValidDoubleQuotesEscapedText("\"\"\"\""));
        assertFalse(EscapingUtils.isValidDoubleQuotesEscapedText("\"\"\"\"\""));
        assertTrue(EscapingUtils.isValidDoubleQuotesEscapedText("\"\"\"\"\"\""));
        assertFalse(EscapingUtils.isValidDoubleQuotesEscapedText("\"\" \"\"\"\""));
        assertTrue(EscapingUtils.isValidDoubleQuotesEscapedText("\"\"\" \"\"\""));
        assertFalse(EscapingUtils.isValidDoubleQuotesEscapedText("\"\"\"\" \"\""));
        assertFalse(EscapingUtils.isValidDoubleQuotesEscapedText("\"ab\"\""));
        assertFalse(EscapingUtils.isValidDoubleQuotesEscapedText("\"ab\"c\""));
        assertTrue(EscapingUtils.isValidDoubleQuotesEscapedText("\"ab\"\"c\""));
        assertTrue(EscapingUtils.isValidDoubleQuotesEscapedText("\"ab\"\"\""));
        assertFalse(EscapingUtils.isValidDoubleQuotesEscapedText("\"\""));
        assertTrue(EscapingUtils.isValidDoubleQuotesEscapedText("\" \""));
        assertTrue(EscapingUtils.isValidDoubleQuotesEscapedText("\"$\""));
    }

    private static void testIsValidUnescapedText(String s,
                                                 boolean expected) {
        assertEquals(expected, EscapingUtils.isValidUnescapedText(s), "'" + s + "'");
    }

    @Test
    void testIsValidUnescapedText() {
        // Illegal literals
        testIsValidUnescapedText(null, false);
        testIsValidUnescapedText("", false);
        testIsValidUnescapedText("~", false);
        testIsValidUnescapedText("|", false);
        testIsValidUnescapedText("$", false);

        // Legal literals (but not necessarily valid)
        testIsValidUnescapedText(" ", false);
        testIsValidUnescapedText("{", false);
        testIsValidUnescapedText("}", false);
        testIsValidUnescapedText("=", false);
        testIsValidUnescapedText("<", false);
        testIsValidUnescapedText(">", false);
        testIsValidUnescapedText("(", false);
        testIsValidUnescapedText(")", false);
        testIsValidUnescapedText("!", false);
        testIsValidUnescapedText("|", false);
        testIsValidUnescapedText("&", false);
        testIsValidUnescapedText("\"", false);
        testIsValidUnescapedText("abc", true);
        testIsValidUnescapedText("-", true);
        testIsValidUnescapedText("+", true);
        testIsValidUnescapedText("0123", false);
        testIsValidUnescapedText("+1", false);
        testIsValidUnescapedText("-1", false);
    }

    private static void testIsValidText(String s,
                                        boolean expected) {
        assertEquals(expected, EscapingUtils.isValidText(s), "'" + s + "'");
    }

    @Test
    void testIsValidText() {
        // Illegal literals
        testIsValidText(null, false);
        testIsValidText("", false);
        testIsValidText("~", false);
        testIsValidText("|", false);

        // Legal literals (but not necessarily valid)
        testIsValidText(" ", false);
        testIsValidText(".", false);
        testIsValidText("\".\"", true);
        testIsValidText("{", false);
        testIsValidText("}", false);
        testIsValidText("=", false);
        testIsValidText("<", false);
        testIsValidText(">", false);
        testIsValidText("(", false);
        testIsValidText(")", false);
        testIsValidText("!", false);
        testIsValidText("&", false);
        testIsValidText("\"", false);
        testIsValidText("abc", true);
        testIsValidText("-", true);
        testIsValidText("+", true);
        testIsValidText("0123", false);
        testIsValidText("true", false);
        testIsValidText("ab c", false);
        testIsValidText("ab.c", false);
        testIsValidText("$", false);
        testIsValidText("\"$\"", true);

        testIsValidText("\"AAA\"", true);
    }

    private static void checkEscaping(String s,
                                      String escaped) {
        final String e = EscapingUtils.escapeWithDoubleQuotes(s);
        assertEquals(e, escaped);
        final String f = EscapingUtils.unescapeDoubleQuotes(e);
        assertEquals(s, f);
    }

    @Test
    void testEscaping() {
        checkEscaping("", "\"\"");
        checkEscaping("A", "\"A\"");
        checkEscaping("\"", "\"\"\"\"");
        checkEscaping("\"A", "\"\"\"A\"");
    }

    private static void testEscapeIfNeeded(String nameText,
                                           String text) {
        assertEquals(nameText, EscapingUtils.escapeWithDoubleQuotesIfNeeded(text), "NAME Context");
    }

    @Test
    void testEscapeIfNeeded() {
        testEscapeIfNeeded(null, null);
        testEscapeIfNeeded("", "");
        testEscapeIfNeeded("A", "A");
        testEscapeIfNeeded("\"A A\"", "A A");
        testEscapeIfNeeded("\"1\"", "1");
        testEscapeIfNeeded("+", "+");
        testEscapeIfNeeded("-", "-");
    }

    @Test
    void testGetEndIndexOfEscapeSection() {
        assertThrows(IllegalArgumentException.class, () -> {
            EscapingUtils.getEndIndexOfEscapeSection("aa", 0);
        });
        assertSame(1, EscapingUtils.getEndIndexOfEscapeSection("\"\"", 0));
        assertSame(1, EscapingUtils.getEndIndexOfEscapeSection("\"\"a", 0));
        assertSame(2, EscapingUtils.getEndIndexOfEscapeSection("\"a\"", 0));
        assertSame(2, EscapingUtils.getEndIndexOfEscapeSection("\"a\"a", 0));
        assertSame(3, EscapingUtils.getEndIndexOfEscapeSection("\"aa\"", 0));
        assertSame(3, EscapingUtils.getEndIndexOfEscapeSection("\"aa\"a", 0));
        assertSame(5, EscapingUtils.getEndIndexOfEscapeSection("\"a\"\"a\"", 0));
        assertSame(5, EscapingUtils.getEndIndexOfEscapeSection("\"a\"\"a\"a", 0));
        assertSame(-1, EscapingUtils.getEndIndexOfEscapeSection("\"", 0));
        assertSame(-1, EscapingUtils.getEndIndexOfEscapeSection("\"\"\"", 0));
        assertSame(-1, EscapingUtils.getEndIndexOfEscapeSection("\"a\"\"", 0));
        assertSame(-1, EscapingUtils.getEndIndexOfEscapeSection("\"a\"\"a", 0));
    }

    @Test
    void testGetPathSeparatorIndex() {
        assertThrows(IllegalArgumentException.class, () -> {
            EscapingUtils.getPathSeparatorIndex("\"a", 0);
        });

        assertSame(-1, EscapingUtils.getPathSeparatorIndex("a", 0));
        assertSame(0, EscapingUtils.getPathSeparatorIndex(".", 0));
        assertSame(1, EscapingUtils.getPathSeparatorIndex(" .", 0));
        assertSame(0, EscapingUtils.getPathSeparatorIndex(".a", 0));
        assertSame(1, EscapingUtils.getPathSeparatorIndex("a.b", 0));
        assertSame(1, EscapingUtils.getPathSeparatorIndex("a.", 0));
        assertSame(4, EscapingUtils.getPathSeparatorIndex("\"a.\".", 0));
    }
}