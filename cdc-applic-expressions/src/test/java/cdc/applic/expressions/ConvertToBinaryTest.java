package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.ConvertToBinary;
import cdc.applic.expressions.ast.visitors.ConvertToNary;

class ConvertToBinaryTest {

    private static void check(String expression,
                              String expected) {
        final Expression e = new Expression(expression);
        final Node n = ConvertToNary.execute(e.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY);
        final Node n1 = ConvertToBinary.execute(n);
        assertEquals(expected, n1.toString());
    }

    @Test
    void test() {
        check("a", "REF(a)");
        check("a or b or c", "OR(OR(REF(a),REF(b)),REF(c))");
        check("a and b and c", "AND(AND(REF(a),REF(b)),REF(c))");
    }
}