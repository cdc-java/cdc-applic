package cdc.applic.expressions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.UncheckedSet;
import cdc.applic.expressions.content.ValueUtils;
import cdc.applic.expressions.parsing.SItemsParsing;

class UncheckedSetTest {
    private static final Logger LOGGER = LogManager.getLogger(UncheckedSetTest.class);

    private static final Class<? extends SItemSet> B = BooleanSet.class;
    private static final Class<? extends SItemSet> I = IntegerSet.class;
    private static final Class<? extends SItemSet> R = RealSet.class;
    private static final Class<? extends SItemSet> S = StringSet.class;
    private static final Class<? extends SItemSet> U = UncheckedSet.class;

    private static final List<Class<? extends SItemSet>> ALL = List.of(B, I, R, S, U);

    @SafeVarargs
    private static void checkConstruction(String expectedContent,
                                          String s,
                                          Class<? extends SItemSet>... complianceKinds) {
        final UncheckedSet set = UncheckedSet.of(s);
        LOGGER.debug("Unchecked(" + s + "): " + set);
        assertEquals(expectedContent, set.getContent());
        final Set<Class<? extends SItemSet>> accepted = new HashSet<>();
        for (final Class<? extends SItemSet> kind : complianceKinds) {
            accepted.add(kind);
        }
        for (final Class<? extends SItemSet> kind : ALL) {
            assertEquals(accepted.contains(kind), set.isCompliantWith(kind), "Failed for " + kind);
        }
        assertEquals(set.isValid(), set.isEmpty() || complianceKinds.length > 1);
        assertEquals(set.isEmpty(), set.isChecked());

        set.getChecked();
        set.getChecked();

        final UncheckedSet set2 = UncheckedSet.of(set.getItems());
        assertEquals(set, set2);
        final UncheckedSet set3 = UncheckedSet.of(set.getItems().toArray(new SItem[0]));
        assertEquals(set, set3);
    }

    @Test
    void testGetBest() {
        final UncheckedSet set = UncheckedSet.of("1, 1.0");
        assertEquals(set, set.getBest());
    }

    @Test
    void testConstruction() {
        checkConstruction("", "", B, I, R, S, U);
        checkConstruction("1", "1", I, U);
        checkConstruction("1", " 1 ", I, U);
        checkConstruction("1.0", " 1.0 ", R, U);
        checkConstruction("1,2", " 1 , 2", I, U);
        checkConstruction("2,2,1,2", " 2,2 , 1, 2", I, U);
        checkConstruction("A", "A", S, U);
        checkConstruction("A", " A ", S, U);
        checkConstruction("A,B", " A , B", S, U);
        checkConstruction("A,A,B,A", " A,A , B, A", S, U);
        checkConstruction("A,1", " A,1", U);
        checkConstruction("A,\"1\"", " A,\"1\"", S, U);
        checkConstruction("\"A A\",1", " \"A A\" , 1", U);
        checkConstruction("false", "false", B, U);
        checkConstruction("\"false\"", "\"false\"", S, U);
        checkConstruction("\"1\"", "\"1\"", S, U);
        checkConstruction("1,A", " 1, A ", U);
        checkConstruction("1,true", " 1, TRUE  ", U);
        checkConstruction("\"1A\"", "\"1A\"", S, U);

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.of((SItem) null);
                     });
    }

    @Test
    void checkContains() {
        assertFalse(UncheckedSet.EMPTY.contains(ValueUtils.create("1")));
        assertTrue(UncheckedSet.of("1").contains(ValueUtils.create("1")));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.of("1").contains(ValueUtils.create("1.0"));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.of("1,A").contains(ValueUtils.create("1"));
                     });
    }

    private static void checkSetUnion(String s1,
                                      String s2,
                                      String content) {
        final UncheckedSet set1 = UncheckedSet.of(s1);
        final UncheckedSet set2 = UncheckedSet.of(s2);
        final SItemSet set = set1.union(set2);
        LOGGER.debug(set1 + " unchecked union " + set2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    @Test
    void testSetUnion() {
        checkSetUnion("", "", "");
        checkSetUnion("1,10", "2~9", "1~10");
        checkSetUnion("1,2", "2~10", "1~10");
        checkSetUnion("1", "2", "1~2");
        checkSetUnion("1", "3", "1,3");
        checkSetUnion("A", "", "A");
        checkSetUnion("", "A", "A");
        checkSetUnion("A", "A", "A");
        checkSetUnion("A", "A", "A");
        checkSetUnion("A", "B", "A,B");
        checkSetUnion("A,B", "B", "A,B");
        checkSetUnion("A,B", "A,B", "A,B");
        checkSetUnion("A,B", "B,A", "A,B");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.of("A").union(UncheckedSet.of("1"));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.of("A,1").union(UncheckedSet.of("1"));
                     });
    }

    private static void checkItemUnion(String s1,
                                       String i2,
                                       String content) {
        final UncheckedSet set1 = UncheckedSet.of(s1);
        final SItem item2 = SItemsParsing.toSItems(i2).get(0);
        final SItemSet set = set1.union(item2);
        LOGGER.debug(set1 + " unchecked union " + item2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    @Test
    void testItemUnion() {
        checkItemUnion("1", "2", "1~2");
        checkItemUnion("", "2", "2");
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.of("1").union(SItemsParsing.toSItems("A").get(0));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.of("1,A").union(SItemsParsing.toSItems("A").get(0));
                     });
    }

    private static void checkSetIntersection(String s1,
                                             String s2,
                                             String content) {
        final UncheckedSet set1 = UncheckedSet.of(s1);
        final UncheckedSet set2 = UncheckedSet.of(s2);
        final SItemSet set = set1.intersection(set2);
        LOGGER.debug(set1 + " unchecked intersection " + set2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    @Test
    void testSetIntersection() {
        checkSetIntersection("", "", "");
        checkSetIntersection("1", "", "");
        checkSetIntersection("1~2,5", "", "");
        checkSetIntersection("1~10", "5~15", "5~10");
        checkSetIntersection("A", "", "");
        checkSetIntersection("A,B", "", "");
        checkSetIntersection("", "A", "");
        checkSetIntersection("", "A,B", "");
        checkSetIntersection("A", "A", "A");
        checkSetIntersection("A,B", "A", "A");
        checkSetIntersection("B,A", "A", "A");
        checkSetIntersection("B,A", "A,B", "B,A");
        checkSetIntersection("A", "B", "");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.of("A").intersection(UncheckedSet.of("1"));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.of("A,1").intersection(UncheckedSet.of("1"));
                     });
    }

    private static void checkItemIntersection(String s1,
                                              String i2,
                                              String content) {
        final UncheckedSet set1 = UncheckedSet.of(s1);
        final SItem item2 = SItemsParsing.toSItems(i2).get(0);
        final SItemSet set = set1.intersection(item2);
        LOGGER.debug(set1 + " unchecked intersection " + item2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    @Test
    void testItemIntersection() {
        checkItemIntersection("1", "2", "");
        checkItemIntersection("", "2", "");
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.of("1").intersection(SItemsParsing.toSItems("A").get(0));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.of("1,A").intersection(SItemsParsing.toSItems("A").get(0));
                     });
    }

    private static void checkSetRemove(String s1,
                                       String s2,
                                       String content) {
        final UncheckedSet set1 = UncheckedSet.of(s1);
        final UncheckedSet set2 = UncheckedSet.of(s2);
        final SItemSet set = set1.remove(set2);
        LOGGER.debug(set1 + " unchecked remove " + set2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    @Test
    void testSetRemove() {
        checkSetRemove("", "", "");
        checkSetRemove("1,3~10", "", "1,3~10");
        checkSetRemove("1~10", "1", "2~10");
        checkSetRemove("1~10", "2", "1,3~10");
        checkSetRemove("1~10", "9", "1~8,10");
        checkSetRemove("1~10", "10", "1~9");
        checkSetRemove("1~10", "2~8", "1,9~10");
        checkSetRemove("1~10", "3~8", "1~2,9~10");
        checkSetRemove("A", "", "A");
        checkSetRemove("A,B", "", "A,B");
        checkSetRemove("A,B,C", "", "A,B,C");
        checkSetRemove("", "A", "");
        checkSetRemove("A", "A", "");
        checkSetRemove("A,B", "A", "B");
        checkSetRemove("A,B,C", "A", "B,C");
        checkSetRemove("", "B", "");
        checkSetRemove("A", "B", "A");
        checkSetRemove("A,B", "B", "A");
        checkSetRemove("A,B,C", "B", "A,C");
        checkSetRemove("", "C", "");
        checkSetRemove("A", "C", "A");
        checkSetRemove("A,B", "C", "A,B");
        checkSetRemove("A,B,C", "C", "A,B");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.of("A").remove(UncheckedSet.of("1"));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.of("A,1").remove(UncheckedSet.of("1"));
                     });
    }

    private static void checkItemRemove(String s1,
                                        String i2,
                                        String content) {
        final UncheckedSet set1 = UncheckedSet.of(s1);
        final SItem item2 = SItemsParsing.toSItems(i2).get(0);
        final SItemSet set = set1.remove(item2);
        LOGGER.debug(set1 + " unchecked remove " + item2 + ": " + set);
        assertEquals(set.getContent(), content);
    }

    @Test
    void testItemRemove() {
        checkItemRemove("1", "2", "1");
        checkItemRemove("", "2", "");
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.of("1").remove(SItemsParsing.toSItems("A").get(0));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         UncheckedSet.of("1,A").remove(SItemsParsing.toSItems("A").get(0));
                     });
    }

    @Test
    void testEquals() {
        assertEquals(UncheckedSet.EMPTY, UncheckedSet.EMPTY);
        assertEquals(UncheckedSet.EMPTY, IntegerSet.EMPTY);
        assertNotEquals(UncheckedSet.EMPTY, IntegerSet.of("1"));
        assertNotEquals(UncheckedSet.EMPTY, null);
        assertNotEquals(UncheckedSet.EMPTY, "Hello");
    }

    @Test
    void testHashCode() {
        assertEquals(UncheckedSet.EMPTY.hashCode(), UncheckedSet.EMPTY.hashCode());
    }

    private static void checkDuplicate(String content) {
        final UncheckedSet set = UncheckedSet.of(content);
        final SItemSet cset = set.getChecked();

        LOGGER.debug("set: " + set + " " + set.getClass().getSimpleName());
        LOGGER.debug("cset: " + cset + " " + cset.getClass().getSimpleName());
        assertNotEquals(set.getItems().size(), cset.getItems().size());
    }

    @Test
    void testDuplicate() {
        checkDuplicate("1, 1");
        checkDuplicate("1.0, 1.0");
        checkDuplicate("false, false");
        checkDuplicate("A, A");
    }
}