package cdc.applic.demos;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.consistency.ConsistencyChecker;
import cdc.applic.consistency.core.ConsistencyCheckerImpl;
import cdc.applic.consistency.impl.Block;
import cdc.applic.consistency.impl.ConsistencyDataImpl;
import cdc.applic.consistency.impl.Node;
import cdc.applic.consistency.impl.Reference;
import cdc.applic.consistency.impl.io.ConsistencyDataXml;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.io.RepositoryXml;
import cdc.issues.Issue;
import cdc.issues.impl.IssuesAndAnswersImpl;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesWriter;
import cdc.issues.io.OutSettings;
import cdc.util.events.ProgressController;
import cdc.util.lang.FailureReaction;

public class ConsistencyDemo {
    private static final Logger LOGGER = LogManager.getLogger(ConsistencyDemo.class);

    public static void main(String... args) throws IOException {
        final RepositoryXml.StAXLoader repositoryLoader = new RepositoryXml.StAXLoader(FailureReaction.WARN);
        final RepositoryImpl repository = repositoryLoader.load("src/main/resources/consistency-repository.xml");

        final ConsistencyDataXml.StAXLoader dataLoader = new ConsistencyDataXml.StAXLoader(FailureReaction.WARN, repository);
        final ConsistencyDataImpl data = dataLoader.load("src/main/resources/consistency-data.xml");

        final ConsistencyChecker<Node, String, Block, Reference> checker = new ConsistencyCheckerImpl<>();
        final List<Issue> issues = checker.check(data);

        IssuesWriter.save(new IssuesAndAnswersImpl().addIssues(issues),
                          OutSettings.ALL_DATA_NO_ANSWERS,
                          new File("target/consistency-issues.xml"),
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_BEST);
        IssuesWriter.save(new IssuesAndAnswersImpl().addIssues(issues),
                          OutSettings.ALL_DATA_NO_ANSWERS,
                          new File("target/consistency-issues.xlsx"),
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_BEST);

        LOGGER.info("Issues: {}", issues.size());
        for (final Issue issue : issues) {
            LOGGER.info("   {}", issue);
        }
    }
}