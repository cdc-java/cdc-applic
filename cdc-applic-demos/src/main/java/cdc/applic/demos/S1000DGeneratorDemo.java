package cdc.applic.demos;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.charts.DictionaryToGvGenerator;
import cdc.applic.charts.DictionaryToGvGeneratorFeatures;
import cdc.applic.charts.DictionaryToGvGeneratorFeatures.Hint;
import cdc.applic.charts.core.DictionaryToGvGeneratorImpl;
import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.AbstractDictionaryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.io.RepositoryXml;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.literals.Name;
import cdc.applic.s1000d.S1000DAct;
import cdc.applic.s1000d.S1000DActCctPctCollector;
import cdc.applic.s1000d.S1000DCct;
import cdc.applic.s1000d.S1000DPct;
import cdc.applic.s1000d.S1000DPctGenerator;
import cdc.applic.s1000d.S1000DProfile;
import cdc.applic.s1000d.core.S1000DPctGeneratorImpl;
import cdc.gv.tools.GvFormat;
import cdc.issues.Issue;
import cdc.issues.impl.IssuesAndAnswersImpl;
import cdc.issues.io.IssuesIoFactory;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesWriter;
import cdc.issues.io.OutSettings;
import cdc.util.events.ProgressController;
import cdc.util.events.ProgressEvent;
import cdc.util.files.SearchPath;
import cdc.util.lang.FailureReaction;

public class S1000DGeneratorDemo {
    private static final Logger LOGGER = LogManager.getLogger(S1000DGeneratorDemo.class);

    public static void main(String... args) throws IOException {
        final RepositoryXml.StAXLoader loader = new RepositoryXml.StAXLoader(FailureReaction.WARN);
        final RepositoryImpl repository = loader.load("src/main/resources/s1000d-repository.xml");
        final AbstractDictionaryImpl dictionary = repository.getDictionary("/Registry/Policy1");
        final DictionaryHandle handle = new DictionaryHandle(dictionary);

        final S1000DPctGenerator pctComputer = new S1000DPctGeneratorImpl();
        final S1000DActCctPctCollector collector = new S1000DActCctPctCollector();

        final Expression additionalAssertion = new Expression("Rank in {100~120}");
        final S1000DProperty masterProperty = dictionary.getOptionalAllowedProperty(Name.of("Rank")).orElseThrow();

        final ProgressController controller = new ProgressController() {
            @Override
            public void onProgress(ProgressEvent event) {
                LOGGER.debug(event);
            }

            @Override
            public boolean isCancelled() {
                return false;
            }
        };

        final S1000DProfile profile = S1000DProfile.builder()
                                                   .support(S1000DProfile.Feature.BOOLEAN_TYPES, false)
                                                   .support(S1000DProfile.Feature.STRING_TYPES_ENUMS, false)
                                                   .support(S1000DProfile.Feature.STRING_TYPES_PATTERNS, false)
                                                   .support(S1000DProfile.Feature.STRING_RANGES, false)
                                                   .support(S1000DProfile.Feature.REAL_TYPES, false)
                                                   .support(S1000DProfile.Feature.REAL_RANGES, false)
                                                   .support(S1000DProfile.Feature.INTEGER_TYPES, false)
                                                   .support(S1000DProfile.Feature.INTEGER_RANGES, false)
                                                   .support(S1000DProfile.Feature.CONDITION_DEPENDENCIES, false)
                                                   .build();

        pctComputer.generatePct(handle,
                                additionalAssertion,
                                NamingConvention.DEFAULT,
                                masterProperty,
                                collector,
                                controller,
                                profile);

        LOGGER.info("Issues ({})", collector.getIssuesCollector().getIssues().size());
        for (final Issue issue : collector.getIssuesCollector().getIssues()) {
            LOGGER.info(" {}", issue);
        }

        final IssuesIoFactory factory = new IssuesIoFactory(IssuesIoFactoryFeatures.UTC_FASTEST);
        final File issuesFile = new File("target/issues.xlsx");
        final IssuesWriter writer = factory.createIssuesWriter(issuesFile);
        LOGGER.info("Generate {}", issuesFile);
        writer.save(new IssuesAndAnswersImpl().addIssues(collector.getIssuesCollector().getIssues()),
                    OutSettings.ALL_DATA_NO_ANSWERS,
                    ProgressController.VOID);

        LOGGER.info("ACT product attributes ({})", collector.getAct().getProductAttributes().size());
        for (final S1000DAct.ProductAttribute productATtribute : collector.getAct().getProductAttributes()) {
            LOGGER.info("   {}", productATtribute);
        }

        LOGGER.info("CCT condition types ({})", collector.getCct().getConditionTypes().size());
        for (final S1000DCct.ConditionType conditionType : collector.getCct().getConditionTypes()) {
            LOGGER.info("   {}", conditionType);
        }

        LOGGER.info("CCT conditions ({})", collector.getCct().getConditions().size());
        for (final S1000DCct.Condition condition : collector.getCct().getConditions()) {
            LOGGER.info("   {}", condition);
        }

        LOGGER.info("PCT products ({})", collector.getPct().getProducts().size());
        for (final S1000DPct.Product product : collector.getPct().getProducts()) {
            LOGGER.info("   {}", product);
        }

        final DictionaryToGvGenerator generator = new DictionaryToGvGeneratorImpl(SearchPath.EMPTY);

        generator.generate(dictionary,
                           new File("target"),
                           DictionaryToGvGeneratorFeatures.builder()
                                                          .format(GvFormat.PDF)
                                                          .hint(Hint.HORIZONTAL)
                                                          .hint(Hint.SHOW_TYPES)
                                                          .build());
    }
}