package cdc.applic.demos;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.core.constraints.AtLeastOneConstraint;
import cdc.applic.dictionaries.impl.PolicyImpl;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.io.RepositoryOffice;
import cdc.applic.expressions.literals.Name;
import cdc.impex.ImpExFactoryFeatures;

public class RepositoryOfficeDemo {
    private static final Logger LOGGER = LogManager.getLogger(RepositoryOfficeDemo.class);

    public static void main(String... args) throws IOException {
        final File template1 = new File("target/template.xlsx");
        LOGGER.info("Generate template: {}", template1);
        RepositoryOffice.generateTemplate(template1);
        LOGGER.info("Generated template: {}", template1);

        final File template2 = new File("target/template.csv");
        LOGGER.info("Generate template: {}", template2);
        RepositoryOffice.generateTemplate(template2);
        LOGGER.info("Generated template: {}", template2);

        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        registry.booleanType().name("Mod").build();
        registry.integerType().name("Rank").frozen(true).domain("1~999").build();
        registry.realType().name("Length").frozen(false).domain("1.0~1000.0").build();
        registry.patternType().name("Part").frozen(false).pattern(".*").build();
        registry.enumeratedType()
                .name("State")
                .frozen(false)
                .literals("S1", "S2")
                .lessThan("S1", "S2")
                .build();
        registry.alias().name("A1").expression("true").build();
        registry.property().name("P1").type("Mod").ordinal(0).build();
        registry.property().name("Rank").type("Rank").ordinal(1).build();
        registry.createAssertion("true");

        final PolicyImpl policy = registry.policy().name("Policy").build();
        policy.setContextExpression("Rank in {1~100}");
        policy.setItemUsage(Name.of("P1"), DItemUsage.MANDATORY);
        policy.setItemUsage(Name.of("A1"), DItemUsage.OPTIONAL);
        policy.createAssertion("true");
        policy.setWritingRuleEnabled("Rule1", true);
        policy.setWritingRuleEnabled("Rule2", true);
        final AtLeastOneConstraint constraint = new AtLeastOneConstraint(policy);
        constraint.setExpressions("A1", "P1");
        policy.addConstraint(constraint);

        final RegistryImpl registry2 = repository.registry()
                                                 .name("Registry2")
                                                 .prefix("R2")
                                                 .parents(registry)
                                                 .build();
        registry2.setContextExpression("Rank in {1~10}");

        final File repo = new File("target/repository.xlsx");
        LOGGER.info("Generate repository: {}", repo);
        RepositoryOffice.Writer.write(repository, repo, ImpExFactoryFeatures.BEST);
        LOGGER.info("Generated repository: {}", repo);

        LOGGER.info("Load repository: {}", repo);
        final RepositoryImpl repository2 = RepositoryOffice.Loader.load(repo);
        LOGGER.info("Loaded repository: {}", repo);

        final File repo2csv = new File("target/repository2.csv");
        LOGGER.info("Generate repository: {}", repo2csv);
        RepositoryOffice.Writer.write(repository2, repo2csv, ImpExFactoryFeatures.BEST);
        LOGGER.info("Generated repository: {}", repo2csv);

        final File repo2xlsx = new File("target/repository2.xlsx");
        LOGGER.info("Generate repository: {}", repo2xlsx);
        RepositoryOffice.Writer.write(repository2, repo2xlsx, ImpExFactoryFeatures.BEST);
        LOGGER.info("Generated repository: {}", repo2xlsx);
    }
}