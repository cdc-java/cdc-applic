package cdc.applic.demos;

import cdc.applic.tools.RegistryEncoder;

public class RegistryEncoderDemo {
    public static void main(String... args) {
        RegistryEncoder.exec("--args-file", "src/main/resources/registry-encoder-args.txt");
    }
}