package cdc.applic.demos;

import java.util.Objects;

import cdc.applic.expressions.Expression;
import cdc.applic.mountability.Interchangeability;

class TestVariant {
    private final int id;
    private final Interchangeability interchangeability;
    private final Expression applicability;

    public TestVariant(int id,
                       Interchangeability interchangeability,
                       Expression applicability) {
        this.id = id;
        this.interchangeability = interchangeability;
        this.applicability = applicability;
    }

    public int getId() {
        return id;
    }

    public Interchangeability getInterchangeability() {
        return interchangeability;
    }

    public Expression getApplicability() {
        return applicability;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,
                            interchangeability,
                            applicability);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof TestVariant)) {
            return false;
        }
        final TestVariant other = (TestVariant) object;
        return this.id == other.id
                && this.interchangeability == other.interchangeability
                && Objects.equals(this.applicability, other.applicability);
    }

    @Override
    public String toString() {
        return "[" + getId() + " " + getInterchangeability() + " " + getApplicability() + "]";
    }
}