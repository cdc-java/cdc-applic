package cdc.applic.demos;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.core.constraints.AtLeastOneConstraint;
import cdc.applic.dictionaries.impl.PolicyImpl;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.io.RepositoryXml;
import cdc.applic.expressions.literals.Name;
import cdc.io.xml.XmlWriter;
import cdc.util.lang.FailureReaction;

public class RepositoryXmlDemo {
    private static final Logger LOGGER = LogManager.getLogger(RepositoryXmlDemo.class);
    // private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.INFO).buildPrintStream();

    private static void generate(File file) {
        LOGGER.info("Generate {}", file);
    }

    private static void generated(File file) {
        LOGGER.info("Generated {}", file);
    }

    private static void check() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        registry.booleanType().name("Mod").build();
        registry.integerType().name("Rank").frozen(true).domain("1~999").build();
        registry.realType().name("Length").frozen(false).domain("1.0~1000.0").build();
        registry.patternType().name("Part").frozen(false).pattern(".*").build();
        registry.enumeratedType()
                .name("State")
                .frozen(false)
                .literals("S1", "S2")
                .lessThan("S1", "S2")
                .build();
        registry.alias().name("A1").expression("true").build();
        registry.property().name("P1").type("Mod").ordinal(0).build();
        registry.property().name("Rank").type("Rank").ordinal(1).build();
        registry.createAssertion("true");

        final PolicyImpl policy = registry.policy().name("Policy").build();
        policy.setContextExpression("Rank in {1~100}");
        policy.setItemUsage(Name.of("P1"), DItemUsage.MANDATORY);
        policy.setItemUsage(Name.of("A1"), DItemUsage.OPTIONAL);
        policy.createAssertion("true");
        policy.setWritingRuleEnabled("Rule1", true);
        policy.setWritingRuleEnabled("Rule2", true);
        final AtLeastOneConstraint constraint = new AtLeastOneConstraint(policy);
        constraint.setExpressions("A1", "P1");
        policy.addConstraint(constraint);

        final RegistryImpl registry2 = repository.registry()
                                                 .name("Registry2")
                                                 .prefix("R2")
                                                 .parents(registry)
                                                 .build();
        registry2.setContextExpression("Rank in {1~10}");

        final File demo2 = new File("target/demo2.xml");
        generate(demo2);
        try (XmlWriter writer = new XmlWriter(demo2)) {
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT, true);
            writer.setIndentString("   ");
            RepositoryXml.Printer.write(writer, repository);
        }
        generated(demo2);

        final RepositoryXml.StAXLoader loader = new RepositoryXml.StAXLoader(FailureReaction.FAIL);
        final RepositoryImpl repository2 = loader.load("target/demo2.xml");

        final File demo3 = new File("target/demo3.xml");
        generate(demo3);
        try (XmlWriter writer = new XmlWriter(demo3)) {
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT, true);
            writer.setIndentString("   ");
            RepositoryXml.Printer.write(writer, repository2);
        }
        generated(demo3);
    }

    public static void main(String... args) throws IOException {
        final RepositoryXml.StAXLoader loader = new RepositoryXml.StAXLoader(FailureReaction.FAIL);
        final RepositoryImpl repository = loader.load("src/main/resources/example-repository.xml");
        // repository.print(OUT);
        final File demo = new File("target/demo.xml");
        generate(demo);
        try (XmlWriter writer = new XmlWriter(demo)) {
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT, true);
            writer.setIndentString(" ");
            RepositoryXml.Printer.write(writer, repository);
        }
        generated(demo);
        check();
    }
}