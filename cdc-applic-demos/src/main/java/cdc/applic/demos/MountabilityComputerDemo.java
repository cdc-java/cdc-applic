package cdc.applic.demos;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.Registry;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.io.RepositoryXml;
import cdc.applic.dictionaries.types.TypeKind;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.SymbolType;
import cdc.applic.mountability.Interchangeability;
import cdc.applic.mountability.MountabilityComputer;
import cdc.applic.mountability.MountabilityComputerFeatures;
import cdc.applic.mountability.core.MountabilityComputerImpl;
import cdc.applic.mountability.handlers.MountabilityHandler;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.publication.SNameFormatter;
import cdc.applic.publication.Symbol;
import cdc.applic.publication.ValueFormatter;
import cdc.applic.publication.core.formatters.BooleanValueFormatter;
import cdc.applic.publication.core.formatters.FormattersCatalog;
import cdc.applic.publication.core.formatters.InfixFormatter;
import cdc.applic.publication.core.formatters.MapSNameFormatter;
import cdc.applic.publication.core.formatters.SpaceFormatterImpl;
import cdc.applic.publication.core.formatters.SymbolFormatterImpl;
import cdc.applic.simplification.SimplifierFeatures;
import cdc.applic.simplification.SimplifierFeatures.Hint;
import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.tables.TableSection;
import cdc.util.events.ProgressController;
import cdc.util.lang.ExceptionWrapper;
import cdc.util.lang.FailureReaction;

public class MountabilityComputerDemo {
    static final Logger LOGGER = LogManager.getLogger(MountabilityComputerDemo.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.INFO).buildPrintStream();

    private static class Handler implements MountabilityHandler<TestUsePoint, TestVariant> {
        private final WorkbookWriter<?> writer;
        private final InfixFormatter formatter;

        public Handler(Registry registry) throws IOException {
            final WorkbookWriterFactory factory = new WorkbookWriterFactory();
            this.writer = factory.create(new File("target/" + MountabilityComputerDemo.class.getSimpleName() + ".xlsx"));

            final FormattersCatalog catalog = new FormattersCatalog();
            final SymbolFormatterImpl symbolFormatter =
                    SymbolFormatterImpl.builder(SymbolType.SHORT)
                                       .map(Symbol.AND, "&", false)
                                       .map(Symbol.OR, ",", false)
                                       .map(Symbol.TO, "-", false)
                                       .map(Symbol.EQUAL, "", false)
                                       .map(Symbol.NOT_EQUAL, "^", false)
                                       .map(Symbol.CLOSE_SET, ")", false)
                                       .map(Symbol.OPEN_SET, "(", false)
                                       .map(Symbol.IN, "", false)
                                       .map(Symbol.NOT_IN, "^", false)
                                       .map(Symbol.NOT, "^", false)
                                       .build();

            final SNameFormatter prefixFormatter = MapSNameFormatter.builder()
                                                                    .map("R", null)
                                                                    .build();
            final SNameFormatter localNameFormatter = MapSNameFormatter.builder()
                                                                       .map("Rank", "")
                                                                       .map("PartNumber", "")
                                                                       .build();
            final ValueFormatter booleanFormatter = BooleanValueFormatter.builder()
                                                                         .map(false, "Not mountable")
                                                                         .map(true, "Mountable")
                                                                         .build();
            catalog.setPrefixFormatter(prefixFormatter);
            catalog.setLocalNameFormatter(localNameFormatter);
            catalog.setDefaultSpaceFormatter(SpaceFormatterImpl.NARROW_SPACE_FORMATTER);
            catalog.setDefaultSymbolFormatter(symbolFormatter);
            catalog.setDefaultValueFormatter(TypeKind.BOOLEAN, booleanFormatter);

            this.formatter = new InfixFormatter(registry, catalog);
        }

        @Override
        public void processBeginUsePoints() {
            LOGGER.info("processBeginUsePoints()");
            try {
                writer.beginSheet("Mountability");
                writer.beginRow(TableSection.HEADER);
                writer.addCells("Use Point",
                                "Variant",
                                "Interchangeability",
                                "Applicability",
                                "Mountability",
                                "Formatted Mountability");
            } catch (final IOException e) {
                throw new ExceptionWrapper(e);
            }
        }

        @Override
        public void processEndUsePoints() {
            LOGGER.info("processEndUsePoints()");
            try {
                writer.close();
            } catch (final IOException e) {
                throw new ExceptionWrapper(e);
            }
        }

        @Override
        public void processBeginUsePoint(TestUsePoint usePoint) {
            LOGGER.info("processBeginUsePoint({})", usePoint);
        }

        @Override
        public void processEndUsePoint(TestUsePoint usePoint) {
            LOGGER.info("processEndUsePoint({})", usePoint);
        }

        @Override
        public void processVariantMountability(TestUsePoint usePoint,
                                               TestVariant variant,
                                               Expression mountability) {
            LOGGER.info("processVariantMountability({}, {}, {})", usePoint, variant, mountability);
            try {
                writer.beginRow(TableSection.DATA);
                writer.addCell(usePoint.getId());
                writer.addCell(variant.getId());
                writer.addCell(variant.getInterchangeability());
                writer.addCell(variant.getApplicability());
                writer.addCell(mountability);
                writer.addCell(formatter.formatAsString(mountability));
            } catch (final IOException e) {
                throw new ExceptionWrapper(e);
            }
        }

        @Override
        public void processError(RuntimeException e) {
            LOGGER.error(e);
        }
    }

    public static void main(String... args) throws IOException {
        final File file = new File("src/main/resources/mountability-repository.xml");

        final RepositoryXml.StAXLoader loader = new RepositoryXml.StAXLoader(FailureReaction.WARN);
        final RepositoryImpl repository = loader.load(file);
        final Dictionary dictionary = repository.getDictionary("/Registry/Policy");
        final DictionaryHandle handle = new DictionaryHandle(dictionary);

        final MountabilityComputer<TestUsePoint, TestVariant> computer =
                new MountabilityComputerImpl<>(handle,
                                               SimplifierFeatures.builder()
                                                                 .proverFeatures(ProverFeatures.EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)
                                                                 .hints(Hint.CONVERT_TO_DNF,
                                                                        Hint.NORMALIZE_BOOLEAN_PROPERTIES,
                                                                        Hint.REMOVE_ALWAYS_TRUE_OR_FALSE,
                                                                        Hint.REMOVE_NEGATION,
                                                                        Hint.REMOVE_REDUNDANT_SIBLINGS)
                                                                 .noLimits()
                                                                 .build());
        final TestMountabilityData data = new TestMountabilityData();

        LOGGER.info("=============================================");
        data.addUsePoint(new TestUsePoint(1).addVariant(null, "Rank<:{1~10}")
                                            .addVariant(Interchangeability.ONE_WAY, "Rank<:{11~20}")
                                            .addVariant(Interchangeability.NOT_INTERCHANGEABLE,
                                                        "Rank<:{21~30} | Rank<:{1~10}&SB1 | Rank<:{11~20}&SB2")
                                            .addVariant(Interchangeability.TWO_WAYS, "Rank<:{31~999}"));
        data.addUsePoint(new TestUsePoint(2).addVariant(null, "Rank<:{1~10}")
                                            .addVariant(Interchangeability.ONE_WAY, "Rank<:{11~20}")
                                            .addVariant(Interchangeability.NOT_INTERCHANGEABLE, "Rank<:{21~30}")
                                            .addVariant(Interchangeability.TWO_WAYS, "Rank<:{31~999}"));
        data.addUsePoint(new TestUsePoint(3).addVariant(null, "Rank<:{1~10}")
                                            .addVariant(Interchangeability.TWO_WAYS, "Rank<:{11~20}")
                                            .addVariant(Interchangeability.ONE_WAY, "Rank<:{21~30}")
                                            .addVariant(Interchangeability.TWO_WAYS, "Rank<:{31~999}"));
        data.addUsePoint(new TestUsePoint(4).addVariant(null, "Rank<:{1,5,8~10}")
                                            .addVariant(Interchangeability.ONE_WAY, "Rank<:{2~4,6~7,11~20}")
                                            .addVariant(Interchangeability.NOT_INTERCHANGEABLE,
                                                        "Rank<:{21~99} | Rank<:{1~20}&SB3"));
        data.addUsePoint(new TestUsePoint(5).addVariant(null, "false")
                                            .addVariant(Interchangeability.TWO_WAYS, "Rank<:{1~5,7~10}")
                                            .addVariant(Interchangeability.NOT_INTERCHANGEABLE, "false")
                                            .addVariant(Interchangeability.ONE_WAY, "Rank<:{6,11~20}")
                                            .addVariant(Interchangeability.NOT_INTERCHANGEABLE, "Rank<:{21~30} | Rank<:{1~20}&SB4")
                                            .addVariant(Interchangeability.ONE_WAY, "Rank<:{31~999}"));
        data.addUsePoint(new TestUsePoint(6).addVariant(null, "Rank<:{1,5,8~10}")
                                            .addVariant(Interchangeability.TWO_WAYS, "Rank<:{2~4,6~7,11~20}")
                                            .addVariant(Interchangeability.ONE_WAY, "Rank<:{21~999}"));
        data.addUsePoint(new TestUsePoint(7).addVariant(null, "PartNumber<:{PN1,PN2}")
                                            .addVariant(Interchangeability.TWO_WAYS, "PartNumber=PN3")
                                            .addVariant(Interchangeability.ONE_WAY, "PartNumber<:{PN4,PN5}"));
        data.addUsePoint(new TestUsePoint(8).addVariant(null, "PartNumber<:{PN1,PN2}")
                                            .addVariant(Interchangeability.NOT_INTERCHANGEABLE,
                                                        "PartNumber=PN3 | PartNumber<:{PN1,PN2}&SB5")
                                            .addVariant(Interchangeability.ONE_WAY, "PartNumber<:{PN4,PN5}"));
        data.addUsePoint(new TestUsePoint(9).addVariant(null, "PartNumber<:{PN1,PN2}")
                                            .addVariant(Interchangeability.NOT_INTERCHANGEABLE,
                                                        "PartNumber=PN3 | PartNumber<:{PN1,PN2}&SB6")
                                            .addVariant(Interchangeability.NOT_INTERCHANGEABLE,
                                                        "PartNumber<:{PN4,PN5} | PartNumber<:{PN1,PN2}&SB7 | PartNumber=PN3&SB8"));
        data.addUsePoint(new TestUsePoint(10).addVariant(null, "PartNumber<:{PN1,PN2}")
                                             .addVariant(Interchangeability.ONE_WAY, "PartNumber<:{PN3,PN4,PN5}")
                                             .addVariant(Interchangeability.ONE_WAY, "PartNumber<:{PN6,PN7}"));

        data.print(OUT);
        final Handler handler = new Handler(repository.getRegistry("/Registry"));
        computer.compute(data,
                         handler,
                         MountabilityComputerFeatures.builder()
                                                     .hint(MountabilityComputerFeatures.Hint.SIMPLIFY)
                                                     .build(),
                         ProgressController.VOID);
    }
}