--repository
src/main/resources/mountability-repository.xml
--formatters-catalog
src/main/resources/mountability-formatters-catalog.xml
--input
src/main/resources/mountability-data.xml
--output
target/mountabilty.xml