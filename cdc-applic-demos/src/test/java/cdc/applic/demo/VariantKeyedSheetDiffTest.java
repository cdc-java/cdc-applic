package cdc.applic.demo;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.tools.VariantedKeyedSheetDiff;
import cdc.util.cli.MainResult;

class VariantKeyedSheetDiffTest {
    @Test
    void testValid() {
        assertSame(MainResult.SUCCESS,
                   VariantedKeyedSheetDiff.exec("--args-file",
                                                "src/main/resources/vksd-args.txt"));
    }

    @Test
    void testInvalidExpressions() {
        assertThrows(RuntimeException.class,
                     () -> {
                         VariantedKeyedSheetDiff.exec("--args-file",
                                                      "src/main/resources/vksd-invalid-expressions-args.txt");
                     });
    }

    @Test
    void testInvalidPartitions() {
        assertThrows(RuntimeException.class,
                     () -> {
                         VariantedKeyedSheetDiff.exec("--args-file",
                                                      "src/main/resources/vksd-invalid-partitions-args.txt");
                     });
    }
}