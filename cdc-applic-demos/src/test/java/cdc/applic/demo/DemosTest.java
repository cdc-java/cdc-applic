package cdc.applic.demo;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import cdc.applic.demos.ConsistencyDemo;
import cdc.applic.demos.ExpressionLabelDemo;
import cdc.applic.demos.MountabilityComputerDemo;
import cdc.applic.demos.RegistryEncoderDemo;
import cdc.applic.demos.RepositoryOfficeDemo;
import cdc.applic.demos.RepositoryXmlDemo;
import cdc.applic.demos.S1000DGeneratorDemo;

class DemosTest {
    @Test
    void testConsistencyDemo() throws IOException {
        ConsistencyDemo.main();
        assertTrue(true);
    }

    @Test
    void testExpressionLabelDemo() throws IOException {
        ExpressionLabelDemo.main();
        assertTrue(true);
    }

    @Test
    void testMountabilityComputerDemo() throws IOException {
        MountabilityComputerDemo.main();
        assertTrue(true);
    }

    @Test
    void testRepositoryOfficedDemo() throws IOException {
        RepositoryOfficeDemo.main();
        assertTrue(true);
    }

    @Test
    void testRegistryEncoderDemo() throws IOException {
        RegistryEncoderDemo.main();
        assertTrue(true);
    }

    @Test
    void testRepositoryXmlDemo() throws IOException {
        RepositoryXmlDemo.main();
        assertTrue(true);
    }

    @Test
    void testS1000DGeneratorDemo() throws IOException {
        S1000DGeneratorDemo.main();
        assertTrue(true);
    }
}