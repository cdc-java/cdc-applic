package cdc.applic.substitutions;

import cdc.applic.dictionaries.Dictionary;

/**
 * Marking interface of objects used to define a replacement mapping.
 *
 * @author Damien Carbonne
 */
public interface Mapping {
    /**
     * Returns {@code true} if this Mapping is compliant with a dictionary.
     *
     * @param dictionary The Dictionary.
     * @return {@code true} if this Mapping is compliant with {@code dictionary}.
     */
    public boolean isCompliantWith(Dictionary dictionary);
}