package cdc.applic.substitutions;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;

/**
 * Interface of objects that can execute a substitution.
 *
 * @author Damien Carbonne
 */
public interface Replacer {
    /**
     * @return The {@link DictionaryHandle}.
     */
    public DictionaryHandle getDictionaryHandle();

    /**
     * @return The {@link Mapping}.
     */
    public Mapping getMapping();

    /**
     * Replaces parts of a node tree using the associated Mapping.
     *
     * @param node The node.
     * @return The transformation of {@code node} after having executed the replacement.
     */
    public Node replace(Node node);

    /**
     * Replaces parts of an expression using the associated Mapping.
     *
     * @param expression The expression.
     * @return The transformation of {@code expression} after having executed the replacement.
     */
    public default Expression replace(Expression expression) {
        return replace(expression.getRootNode()).toExpression();
    }
}