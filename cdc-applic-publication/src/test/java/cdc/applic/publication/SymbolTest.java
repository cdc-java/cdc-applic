package cdc.applic.publication;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class SymbolTest {
    @Test
    void testIs() {
        for (final Symbol symbol : Symbol.values()) {
            assertTrue(symbol.isDelimiter() || symbol.isOperator() || symbol.isValue());
            assertFalse(symbol.isDelimiter() && symbol.isOperator());
            assertFalse(symbol.isDelimiter() && symbol.isValue());
            assertFalse(symbol.isOperator() && symbol.isValue());
        }
    }
}