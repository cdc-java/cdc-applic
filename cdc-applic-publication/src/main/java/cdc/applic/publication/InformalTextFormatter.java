package cdc.applic.publication;

import cdc.applic.expressions.literals.InformalText;

/**
 * Interface implemented by objects that can format {@link InformalText}s.
 *
 * @author Damien Carbonne
 */
public interface InformalTextFormatter extends PieceFormatter {
    /**
     * The string representation of a {@link InformalText}.
     *
     * @param text The informal text.
     * @return The string representation of {@code text}.
     */
    public String getText(InformalText text);
}