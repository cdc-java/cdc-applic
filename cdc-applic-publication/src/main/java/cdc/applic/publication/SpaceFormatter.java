package cdc.applic.publication;

import cdc.util.lang.Checks;

/**
 * Interface implemented by objects that can format spaces.
 * <p>
 * Spaces may be necessary or optional, depending on context.<br>
 * For example, {@code A=B} can be written {@code A = B}, {@code A= B} or {@code A =B}.<br>
 * And <code>A in {X}</code> can only be written <code>A in{X}</code>.
 *
 * @author Damien Carbonne
 */
public interface SpaceFormatter extends PieceFormatter {
    /**
     * @return The text to use to represent space.
     */
    public String getText();

    /**
     * Returns {@code true} if space is needed around a symbol in a context.
     *
     * @param symbol The symbol close to which a space can be added.
     * @param contextRequiresSpace {@code true} if the context requires space.
     * @param symbolFormatter The symbol formatter to be used.
     * @return {@code true} if space is needed.
     */
    public boolean needsSpace(Symbol symbol,
                              boolean contextRequiresSpace,
                              SymbolFormatter symbolFormatter);

    /**
     * Returns {@code true} if a space is absolutely required around a symbol in a context.
     * <p>
     * This is the case when both the {@code symbol} and context require space.
     *
     * @param symbol The symbol.
     * @param contextRequiresSpace {@code true} if the context requires space.
     * @param symbolFormatter The symbol formatter to be used.
     * @return {@code true} if a space is required between {@code symbol} and another token.
     */
    public static boolean requiresSpace(Symbol symbol,
                                        boolean contextRequiresSpace,
                                        SymbolFormatter symbolFormatter) {
        Checks.isNotNull(symbol, "symbol");

        return contextRequiresSpace && symbolFormatter.requiresSpace(symbol);
    }
}