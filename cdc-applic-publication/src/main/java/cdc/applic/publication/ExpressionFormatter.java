package cdc.applic.publication;

import cdc.applic.expressions.Expression;

@FunctionalInterface
public interface ExpressionFormatter {
    /**
     * Format an {@link Expression}, if possible.
     *
     * @param expression The expression to format.
     * @param handler The handler.
     */
    public void format(Expression expression,
                       FormattingHandler handler);

    public default String formatAsString(Expression expression) {
        final StringFormattingHandler handler = new StringFormattingHandler();
        format(expression, handler);
        return handler.toString();
    }
}