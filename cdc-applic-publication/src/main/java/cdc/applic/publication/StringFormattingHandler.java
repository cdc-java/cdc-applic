package cdc.applic.publication;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.InformalText;
import cdc.applic.expressions.literals.Name;

/**
 * Implementation of {@link FormattingHandler} that stores formatted expression into a {@link StringBuilder}.
 *
 * @author Damien Carbonne
 */
public class StringFormattingHandler implements FormattingHandler {
    private final StringBuilder builder;

    public StringFormattingHandler() {
        this.builder = new StringBuilder();
    }

    public StringFormattingHandler(StringBuilder builder) {
        this.builder = builder;
    }

    public StringBuilder getStringBuilder() {
        return builder;
    }

    public void clear() {
        builder.setLength(0);
    }

    @Override
    public void beginExpression(Expression expression) {
        // Ignore
    }

    @Override
    public void appendSpace(String text) {
        builder.append(text);
    }

    @Override
    public void appendSymbol(Symbol symbol,
                             String text) {
        builder.append(text);
    }

    @Override
    public void appendPrefix(Name name,
                             String text,
                             NameValidity validity) {
        builder.append(text);
    }

    @Override
    public void appendPropertyLocalName(Name name,
                                        String text,
                                        NameValidity validity) {
        builder.append(text);
    }

    @Override
    public void appendAliasLocalName(Name name,
                                     String text) {
        builder.append(text);
    }

    @Override
    public void appendInvalidRefLocalName(Name name,
                                          String text) {
        builder.append(text);
    }

    @Override
    public void appendValue(Value value,
                            String text,
                            ValueValidity validity) {
        builder.append(text);
    }

    @Override
    public void appendInformalText(InformalText informal,
                                   String text) {
        builder.append(text);
    }

    @Override
    public void endExpression() {
        // Ignore
    }

    @Override
    public String toString() {
        return builder.toString();
    }
}