package cdc.applic.publication;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.InformalText;
import cdc.applic.expressions.literals.Name;

/**
 * Interface implemented by objects that can handle formatting events.
 *
 * @author Damien Carbonne
 */
public interface FormattingHandler {
    /**
     * Invoked when the formatting of an expression starts.
     *
     * @param expression The expression.
     */
    public void beginExpression(Expression expression);

    /**
     * Invoked when a space must be appended.
     *
     * @param text The text that should be used to represent space.
     */
    public void appendSpace(String text);

    /**
     * Invoked when a symbol must be appended.
     *
     * @param symbol The symbol.
     * @param text The text that should be used to represent {@code symbol}.
     */
    public void appendSymbol(Symbol symbol,
                             String text);

    /**
     * Invoked when a prefix must be appended.
     *
     * @param name The name whose prefix must be appended.
     * @param text The text that should be used to represent prefix of {@code name}.
     * @param validity The validity of {@code name}.
     */
    public void appendPrefix(Name name,
                             String text,
                             NameValidity validity);

    /**
     * Invoked when a property local name must be appended.
     *
     * @param name The property name.
     * @param text The text that should be used to represent local name of {@code name}.
     * @param validity The validity of {@code name}.
     */
    public void appendPropertyLocalName(Name name,
                                        String text,
                                        NameValidity validity);

    /**
     * Invoked when an alias name must be appended.
     *
     * @param name The alias name.
     * @param text The text that should be used to represent local name of {@code name}.
     */
    public void appendAliasLocalName(Name name,
                                     String text);

    public void appendInvalidRefLocalName(Name name,
                                          String text);

    /**
     * Invoked when a value must be appended.
     *
     * @param value The value.
     * @param text The text that should be used to represent {@code value}.
     * @param validity The validity of {@code value}.
     */
    public void appendValue(Value value,
                            String text,
                            ValueValidity validity);

    /**
     * Invoked when an informal text must be appended.
     *
     * @param informal The informal text.
     * @param text The text that should be used to represent {@code informal}.
     */
    public void appendInformalText(InformalText informal,
                                   String text);

    /**
     * Invoked when the formatting of an expression ends.
     */
    public void endExpression();
}