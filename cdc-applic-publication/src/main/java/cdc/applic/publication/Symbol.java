package cdc.applic.publication;

import cdc.applic.expressions.Spacing;
import cdc.applic.expressions.parsing.ComparisonOperator;

/**
 * Enumeration of possible predefined symbols in Expressions. They are independent of dictionaries and can be:
 * <ul>
 * <li>operators
 * <li>certain values
 * <li>delimiters
 * </ul>
 *
 * @author Damien Carbonne
 */
public enum Symbol {
    AND(false),
    CLOSE_PAREN(false),
    CLOSE_SET(false),
    EMPTY_SET(false),
    EQUAL(true),
    EQUIVALENCE(true),
    FALSE(false),
    GREATER(true),
    GREATER_OR_EQUAL(true),
    IMPLICATION(true),
    IN(true),
    ITEMS_SEPARATOR(true),
    LAST_ITEMS_SEPARATOR(true),
    LESS(true),
    LESS_OR_EQUAL(true),
    NEITHER_GREATER_NOR_EQUAL(true),
    NEITHER_LESS_NOR_EQUAL(true),
    NOT(false),
    NOT_EQUAL(true),
    NOT_IN(true),
    NOT_GREATER(true),
    NOT_LESS(true),
    OPEN_PAREN(false),
    OPEN_SET(false),
    OR(true),
    PATH_SEPARATOR(false),
    TO(false),
    TRUE(false),
    XOR(true);

    private final boolean wideSpace;

    private Symbol(boolean wideSpace) {
        this.wideSpace = wideSpace;
    }

    /**
     * @return {@code true} if space is needed around this symbol when using {@link Spacing#WIDE}.
     */
    public boolean needsWideSpace() {
        return wideSpace;
    }

    public static Symbol fromComparisonOperator(ComparisonOperator operator) {
        if (operator == ComparisonOperator.EQUAL) {
            return EQUAL;
        } else if (operator == ComparisonOperator.GREATER) {
            return GREATER;
        } else if (operator == ComparisonOperator.GREATER_OR_EQUAL) {
            return GREATER_OR_EQUAL;
        } else if (operator == ComparisonOperator.LESS) {
            return LESS;
        } else if (operator == ComparisonOperator.LESS_OR_EQUAL) {
            return LESS_OR_EQUAL;
        } else if (operator == ComparisonOperator.NEITHER_GREATER_NOR_EQUAL) {
            return NEITHER_GREATER_NOR_EQUAL;
        } else if (operator == ComparisonOperator.NEITHER_LESS_NOR_EQUAL) {
            return NEITHER_LESS_NOR_EQUAL;
        } else if (operator == ComparisonOperator.NOT_EQUAL) {
            return NOT_EQUAL;
        } else if (operator == ComparisonOperator.NOT_GREATER) {
            return NOT_GREATER;
        } else if (operator == ComparisonOperator.NOT_LESS) {
            return NOT_LESS;
        } else {
            return null;
        }
    }

    /**
     * @return {@code true} if this symbol is a value.
     */
    public boolean isValue() {
        return this == FALSE
                || this == TRUE
                || this == EMPTY_SET;
    }

    /**
     * @return {@code true} if this symbol is an operator.
     */
    public boolean isOperator() {
        return this == AND || this == OR || this == NOT
                || this == IMPLICATION || this == EQUIVALENCE || this == XOR
                || this == EQUAL || this == NOT_EQUAL
                || this == IN || this == NOT_IN
                || this == LESS || this == LESS_OR_EQUAL
                || this == GREATER || this == GREATER_OR_EQUAL
                || this == NOT_LESS || this == NEITHER_LESS_NOR_EQUAL
                || this == NOT_GREATER || this == NEITHER_GREATER_NOR_EQUAL;
    }

    /**
     * @return {@code true} if this symbol is a delimiter.
     */
    public boolean isDelimiter() {
        return this == CLOSE_PAREN || this == OPEN_PAREN
                || this == CLOSE_SET || this == OPEN_SET
                || this == ITEMS_SEPARATOR || this == LAST_ITEMS_SEPARATOR || this == PATH_SEPARATOR
                || this == TO;
    }
}