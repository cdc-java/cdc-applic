package cdc.applic.publication;

/**
 * Base interface of the different formatters for names, spaces, predefined symbols and values.
 *
 * @author Damien Carbonne
 */
public interface PieceFormatter {
    // Ignore
}