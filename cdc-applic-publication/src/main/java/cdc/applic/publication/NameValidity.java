package cdc.applic.publication;

/**
 * Enumeration of possible name validity.
 *
 * @author Damien Carbonne
 */
public enum NameValidity {
    /** The prefix is valid. */
    VALID,
    /** The prefix is invalid. */
    INVALID
}