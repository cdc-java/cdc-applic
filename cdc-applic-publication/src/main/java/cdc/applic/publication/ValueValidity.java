package cdc.applic.publication;

/**
 * Enumeration of possible value validity.
 *
 * @author Damien Carbonne
 */
public enum ValueValidity {
    /** The value is valid. */
    VALID,
    /** The value is invalid. */
    INVALID,
    /** The value validity is unknown. */
    UNKNOWN
}