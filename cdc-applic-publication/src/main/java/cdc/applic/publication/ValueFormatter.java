package cdc.applic.publication;

import cdc.applic.dictionaries.types.TypeKind;
import cdc.applic.expressions.content.Value;

/**
 * Interface implemented by objects that can format {@link Value}s.
 * <p>
 * Formatting of ranges and sets are base on that.
 *
 * @author Damien Carbonne
 */
public interface ValueFormatter extends PieceFormatter {

    /**
     * Returns {@code true} if a kind of type is supported by this formatter.
     *
     * @param kind The TypeKind.
     * @return {@code true} if {@code kind} is supported by this formatter.
     */
    public boolean isSupported(TypeKind kind);

    /**
     * Returns the text to use to represent a value.
     *
     * @param value The value.
     * @return The text to use to represent {@code value}.
     */
    public String getText(Value value);
}