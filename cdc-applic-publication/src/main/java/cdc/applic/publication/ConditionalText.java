package cdc.applic.publication;

import cdc.applic.expressions.Expression;

public class ConditionalText {
    private final String text;
    private final Expression applicability;

    public ConditionalText(String text,
                           Expression applicability) {
        this.text = text;
        this.applicability = applicability;
    }

    public ConditionalText(String text) {
        this(text, Expression.TRUE);
    }

    public String getText() {
        return text;
    }

    public Expression getApplicability() {
        return applicability;
    }
}