package cdc.applic.publication;

import java.util.List;

import cdc.applic.expressions.Expression;

@FunctionalInterface
public interface ExpressionPublisher {
    /**
     *
     * @param expression The expression to publish.
     * @param context The publication context.
     * @return A list of (text, applicability) pairs.
     */
    public List<ConditionalText> publish(Expression expression,
                                         Expression context);
}