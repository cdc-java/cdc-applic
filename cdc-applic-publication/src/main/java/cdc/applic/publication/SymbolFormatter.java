package cdc.applic.publication;

/**
 * Interface implemented by objects that can format {@link Symbol}s.
 *
 * @author Damien Carbonne
 */
public interface SymbolFormatter extends PieceFormatter {
    /**
     * Returns the String representation of a symbol.
     *
     * @param symbol The Symbol.
     * @return The String representation of {@code symbol}.
     */
    public String getText(Symbol symbol);

    /**
     * Returns {@code true} when a symbol requires space between its text representation and a text literal.
     *
     * @param symbol The Symbol.
     * @return {@code true} when {@code symbol} requires space between its text representation and a text literal.
     */
    public boolean requiresSpace(Symbol symbol);
}