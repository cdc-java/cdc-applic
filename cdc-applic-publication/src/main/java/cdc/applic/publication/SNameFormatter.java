package cdc.applic.publication;

import cdc.applic.expressions.literals.SName;

/**
 * Interface implemented by objects that can format {@link SName}s.
 * <p>
 * Used to format Prefixes, Property local names and Alias local names.
 *
 * @author Damien Carbonne
 */
public interface SNameFormatter extends PieceFormatter {
    /**
     * The string representation of a {@link SName}.
     *
     * @param name The name.
     * @return The string representation of {@code name}.
     */
    public String getText(SName name);
}