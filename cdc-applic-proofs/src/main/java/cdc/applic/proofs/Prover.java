package cdc.applic.proofs;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;

/**
 * Interface of higher level proofs.
 *
 * <h2>Notation</h2>
 * An expression <code><b>X</b></code> can be interpreted as a condition expressed with properties
 * <code><b>P<sub>i</sub></b></code>:<br>
 * <code><b>X</b> = X(P<sub>1</sub>, P<sub>2</sub>, ...)</code><br>
 * <code><b>X</b></code> is satisfiable when it is possible to allocate one value <code><b>v<sub>i</sub></b></code>to
 * each property <code><b>P<sub>i</sub></b></code>, such as <code><b>X</b></code> evaluates to {@code true}.<br>
 * Such an allocation is a tuple <code><b>v = (v<sub>1</sub>, v<sub>2</sub>, ...)</b></code><br>
 * <code><b>v<sub>i</sub> &isin; T<sub>i</sub> = T<sub>i</sub>(P<sub>i</sub>)</b> = {v<sub>i1</sub>, ..., v<sub>iN</sub>}</code>,
 * the type of <code><b>P<sub>i</sub></b></code>.
 * <p>
 * <code>v &isin; <b>V(X)</b> = T<sub>1</sub>&times;T<sub>2</sub>&times;...</code>
 * <p>
 * Let <code><b>V<sub>SAT</sub>(X)</b> = {v|X(v)&equiv;true}</code> and
 * <code><b>V<sub>UNSAT</sub>(X)</b> = {v|X(v)&equiv;false}</code><br>
 * So, <code>V(X) = V<sub>SAT</sub>(X) &cup; V<sub>UNSAT</sub>(X)</code><br>
 * We have <code>V<sub>UNSAT</sub>(X) = V<sub>SAT</sub>(&not;X)</code>
 * <p>
 * These formulas are equivalent:
 * <ul>
 * <li><code>SAT(X) &equiv; V<sub>SAT</sub>(X) &ne; &empty;</code>
 * <li><code>!SAT(X) &equiv; V<sub>SAT</sub>(X) = &empty;</code>
 * <li><code>UNSAT(X) &equiv; V<sub>UNSAT</sub>(X) &ne; &empty; &equiv; SAT(&not;X) &ne; &empty;</code>
 * <li><code>!UNSAT(X) &equiv; V<sub>UNSAT</sub>(X) = &empty; &equiv; SAT(&not;X) = &empty;</code>
 * </ul>
 *
 * <h2>Simple positive formulas</h2>
 * <ul>
 * <li><code>X</code> is <i>sometimes true</i>, <code><b>ST(X)</b></code>, when <code>V<sub>SAT</sub>(X) &ne; &empty;</code>, that
 * is <code>SAT(X)</code>
 * <li><code>X</code> is <i>never true</i>, <code><b>NT(X)</b></code>, when <code>V<sub>SAT</sub>(X) = &empty;</code>, that is
 * <code>!SAT(X)</code><br>
 * <code>V<sub>UNSAT</sub>(X) = V(X)</code>, which can be empty.
 * <li><code>X</code> is <i>always true</i>, <code><b>AT(X)</b></code>, when
 * <code>V<sub>SAT</sub>(X) &ne; &empty; &and; V<sub>UNSAT</sub>(X) = &empty;</code>,<br>
 * which can also expressed as <code>V<sub>SAT</sub>(X) &ne; &empty; &and; V<sub>SAT</sub>(&not;X) = &empty;</code>,<br>
 * or <code>AT(X) = ST(X) &and; NT(&not;X) = SAT(X) &and; &not;SAT(&not;X)</code>,<br>
 * or <code>V<sub>SAT</sub>(X) = V(X) &ne; &empty;</code>
 * </ul>
 *
 * <h2>Simple negative formulas</h2>
 * <ul>
 * <li><code>X</code> is <i>sometimes false</i>, <code><b>SF(X)</b></code>, when <code>V<sub>SAT</sub>(&not;X) &ne; &empty;</code>
 * <li><code>X</code> is <i>never false</i>, <code><b>NF(X)</b></code>, when <code>V<sub>SAT</sub>(&not;X) = &empty;</code>
 * <li><code>X</code> is <i>always false</i>, <code><b>AF(X)</b></code>, when
 * <code>V<sub>SAT</sub>(&not;X) &ne; &empty; &and; V<sub>UNSAT</sub>(&not;X) = &empty;</code>,<br>
 * which can also expressed as <code>V<sub>SAT</sub>(&not;X) &ne; &empty; &and; V<sub>SAT</sub>(X) = &empty;</code>,<br>
 * or <code>AF(X) = SF(X) &and; NF(&not;X)</code>
 * </ul>
 *
 * <h3>Warning</h3>
 * <ul>
 * <li><code>AT(X) &rarr; NF(X)</code>, but the opposite is false.<br>
 * <i>Always true</i> and <i>never false</i> are different.
 * <li><code>AF(X) &rarr; NT(X)</code>, but the opposite is false.<br>
 * <i>Always false</i> and <i>never true</i> are different.
 * </ul>
 * It is generally meaningless to say anything on things that don't exist. All assertions can be proved to be true in that case.<br>
 * Therefore, <code>NT(X)</code> and <code>NF(X)</code> should always be used in conjunction with a formula that proves existence of
 * something.
 *
 * <h2>Elaborated formulas</h2>
 * <ul>
 * <li><code>X &cap; Y &equiv; ST(X &and; Y) &equiv; SAT(X &and; Y)</code>
 * <li><code>X &sub; Y &equiv; X &cap; &not;Y = &empty; &equiv; !SAT(X &and; &not;Y)</code>
 * <li><code>X &sub; Y &and; X &ne; &empty; &equiv; !SAT(X &and; &not;Y) &and; SAT(X)</code>
 * <li>...
 * </ul>
 *
 * @author Damien Carbonne
 */
public interface Prover {
    /**
     * @return The context used by this Prover.
     */
    public DictionaryHandle getDictionaryHandle();

    /**
     * @return The features used by this Prover.
     */
    public ProverFeatures getFeatures();

    /**
     * Returns {@code true} when a Node is never satisfiable.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code !isSometimesTrue(node)}.
     * </ul>
     * <b>Note:</b> result depends on features.<br>
     * <b>Note:</b> this invokes once {@code SatSystemSolver.isSatisfiable(SatSystem)}.
     * <p>
     * <img src="doc-files/is-never-true.svg" alt="is never true" width="110px" height="110px">
     *
     * @param node The Node.
     * @return {@code true} when {@code node} is never satisfiable.
     * @throws IllegalArgumentException When {@code node} is {@code null}.
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean isNeverTrue(Node node);

    /**
     * Returns {@code true} when an Expression is never satisfiable.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code !isSometimesTrue(expression)}.
     * <li>{@code isNeverTrue(expression.getRootNode())}.
     * </ul>
     * <b>Note:</b> result depends on features.<br>
     * <b>Note:</b> this invokes once {@code SatSystemSolver.isSatisfiable(SatSystem)}.
     * <p>
     * <img src="doc-files/is-never-true.svg" alt="is never true" width="110px" height="110px">
     *
     * @param expression The Expression.
     * @return {@code true} when {@code expression} is never satisfiable.
     * @throws IllegalArgumentException When {@code expression} is {@code null}.
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean isNeverTrue(Expression expression);

    /**
     * Returns {@code true} when a Node is sometimes (at least one solution) satisfiable.
     * <p>
     * <b>Note:</b> result depends on features.<br>
     * <b>Note:</b> this invokes once {@code SatSystemSolver.isSatisfiable(SatSystem)}.
     * <p>
     * <b>Warning:</b> this does not imply that the node is sometimes false, it may be always true.
     * <p>
     * <img src="doc-files/is-sometimes-true.svg" alt="is sometimes true" width="110px" height="110px">
     *
     * @param node The Node.
     * @return {@code true} when {@code node} is sometimes satisfiable.
     * @throws IllegalArgumentException When {@code node} is {@code null}.
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean isSometimesTrue(Node node);

    /**
     * Returns {@code true} when an Expression is sometimes (at least one solution) satisfiable.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isSometimesTrue(expression.getRootNode())}.
     * </ul>
     * <b>Note:</b> result depends on features.<br>
     * <b>Note:</b> this invokes once {@code SatSystemSolver.isSatisfiable(SatSystem)}.
     * <p>
     * <b>Warning:</b> this does not imply that the node is sometimes false, it may be always true.
     * <p>
     * <img src="doc-files/is-sometimes-true.svg" alt="is sometimes true" width="110px" height="110px">
     *
     * @param expression The Expression.
     * @return {@code true} when {@code expression} is sometimes satisfiable.
     * @throws IllegalArgumentException When {@code expression} is {@code null}.
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean isSometimesTrue(Expression expression);

    /**
     * Returns {@code true} when a Node is always satisfiable.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isSometimesTrue(node) && isNeverTrue(!node)}.
     * <li>{@code isSometimesTrue(node) && !isSometimesTrue(!node)}.
     * </ul>
     * <p>
     * <b>Note:</b> result depends on features.<br>
     * <b>Note:</b> this invokes once {@code SatSystemSolver.isSatisfiable(SatSystem)}.
     * <p>
     * <b>WARNING:</b> This must <em>NOT</em> be used when the tree contains {@code AtomVarNode}s.
     * <p>
     * <img src="doc-files/is-always-true.svg" alt="is always true" width="110px" height="110px">
     *
     * @param node The Node.
     * @return {@code true} when {@code node} is always satisfiable.
     * @throws IllegalArgumentException When {@code node} is {@code null},
     *             or contains an {@code AtomVarNode}.
     *
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean isAlwaysTrue(Node node);

    /**
     * Returns {@code true} when an Expression is always satisfiable.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isSometimesTrue(expression) && isNeverTrue(!expression)}.
     * <li>{@code isSometimesTrue(expression) && !isSometimesTrue(!expression)}.
     * <li>{@code isAlwaysTrue(expression.getRootNode())}.
     * </ul>
     * <p>
     * <b>Note:</b> result depends on features.<br>
     * <b>Note:</b> this invokes twice {@code SatSystemSolver.isSatisfiable(SatSystem)}.
     * <p>
     * <img src="doc-files/is-always-true.svg" alt="is always true" width="110px" height="110px">
     *
     * @param expression The Expression.
     * @return {@code true} when {@code expression} is always satisfiable.
     * @throws IllegalArgumentException When {@code expression} is {@code null}.
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean isAlwaysTrue(Expression expression);

    /**
     * Returns {@code true} when the negation of a Node is never satisfiable.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code !isSometimesFalse(node)}.
     * <li>{@code !isSometimesTrue(!node)}.
     * </ul>
     * <b>Note:</b> result depends on features.<br>
     * <b>Note:</b> this invokes once {@code SatSystemSolver.isSatisfiable(SatSystem)}.
     * <p>
     * <img src="doc-files/is-never-false.svg" alt="is never false" width="110px" height="110px">
     *
     * @param node The Node.
     * @return {@code true} when {@code !node} is never satisfiable.
     * @throws IllegalArgumentException When {@code node} is {@code null}.
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean isNeverFalse(Node node);

    /**
     * Returns {@code true} when the negation of an Expression is never satisfiable.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isNeverTrue(!node)}.
     * <li>{@code !isSometimesTrue(!node)}.
     * <li>{@code !isSometimesFalse(node)}.
     * </ul>
     * <b>Note:</b> result depends on features.<br>
     * <b>Note:</b> this invokes once {@code SatSystemSolver.isSatisfiable(SatSystem)}.
     * <p>
     * <img src="doc-files/is-never-false.svg" alt="is never false" width="110px" height="110px">
     *
     * @param expression The Expression.
     * @return {@code true} when {@code !expression} is never satisfiable.
     * @throws IllegalArgumentException When {@code expression} is {@code null}.
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean isNeverFalse(Expression expression);

    /**
     * Returns {@code true} when the negation of a Node is sometimes (at least one solution) satisfiable.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isSometimesTrue(!node)}.
     * </ul>
     * <b>Note:</b> result depends on features.<br>
     * <b>Note:</b> this invokes once {@code SatSystemSolver.isSatisfiable(SatSystem)}.
     * <p>
     * <b>Warning:</b> this does not imply that the node is sometimes true, it may be always false.
     * <p>
     * <img src="doc-files/is-sometimes-false.svg" alt="is sometimes false" width="110px" height="110px">
     *
     * @param node The Node.
     * @return {@code true} when {@code !node} is sometimes satisfiable.
     * @throws IllegalArgumentException When {@code node} is {@code null}.
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean isSometimesFalse(Node node);

    /**
     * Returns {@code true} when the negation of an Expression is sometimes (at least one solution) satisfiable.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isSometimesTrue(!expression)}.
     * </ul>
     * <b>Note:</b> result depends on features.<br>
     * <b>Note:</b> this invokes once {@code SatSystemSolver.isSatisfiable(SatSystem)}.
     * <p>
     * <b>Warning:</b> this does not imply that the node is sometimes true, it may be always false.
     * <p>
     * <img src="doc-files/is-sometimes-false.svg" alt="is sometimes false" width="110px" height="110px">
     *
     * @param expression The Expression.
     * @return {@code true} when {@code !expression} is sometimes satisfiable.
     * @throws IllegalArgumentException When {@code expression} is {@code null}.
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean isSometimesFalse(Expression expression);

    /**
     * Returns {@code true} when the negation of a Node is always satisfiable.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isAlwaysTrue(!node)}.
     * <li>{@code isSometimesTrue(!node) && isNeverTrue(node)}.
     * <li>{@code isSometimesTrue(!node) && !isSometimesTrue(node)}.
     * <li>{@code isSometimesFalse(node) && isNeverFalse(!node)}.
     * <li>{@code isSometimesFalse(node) && !isSometimesFalse(!node)}.
     * </ul>
     * <p>
     * <b>Note:</b> result depends on features.<br>
     * <b>Note:</b> this invokes twice {@code SatSystemSolver.isSatisfiable(SatSystem)}.
     * <p>
     * <b>WARNING:</b> This must <em>NOT</em> be used when the tree contains {@code AtomVarNode}s.
     * <p>
     * <img src="doc-files/is-always-false.svg" alt="is always false" width="110px" height="110px">
     *
     * @param node The Node.
     *
     * @return {@code true} when {@code !node} is always satisfiable.
     * @throws IllegalArgumentException When {@code node} is {@code null},
     *             or contains an {@code AtomVarNode}.
     *
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean isAlwaysFalse(Node node);

    /**
     * Returns {@code true} when the negation of an Expression is always satisfiable.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isAlwaysTrue(!expression)}.
     * <li>{@code isSometimesTrue(!expression) && isNeverTrue(expression)}.
     * <li>{@code isSometimesTrue(!expression) && !isSometimesTrue(expression)}.
     * <li>{@code isSometimesFalse(expression) && isNeverFalse(!expression)}.
     * <li>{@code isSometimesFalse(expression) && !isSometimesFalse(!expression)}.
     * </ul>
     * <p>
     * <b>Note:</b> result depends on features.<br>
     * <b>Note:</b> this invokes twice {@code SatSystemSolver.isSatisfiable(SatSystem)}.
     * <p>
     * <b>WARNING:</b> This must <em>NOT</em> be used when the tree contains {@code AtomVarNode}s.
     * <p>
     * <img src="doc-files/is-always-false.svg" alt="is always false" width="110px" height="110px">
     *
     * @param expression The Expression.
     * @return {@code true} when {@code !expression} is always satisfiable.
     * @throws IllegalArgumentException When {@code expression} is {@code null},
     *             or contains an {@code AtomVarNode}.
     *
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean isAlwaysFalse(Expression expression);

    /**
     * Returns {@code true} when 2 Nodes are always equivalent.
     * This is equivalent to:
     * <ul>
     * <li>{@code isAlwaysTrue(alpha <-> beta)}.
     * </ul>
     * <img src="doc-files/are-always-equivalent.svg" alt="are always equivalent" width="150px" height="110px">
     *
     * @param alpha The alpha condition.
     * @param beta The beta condition.
     * @return {@code true} when {@code alpha} and {@code beta} are always equivalent.
     */
    public boolean areAlwaysEquivalent(Node alpha,
                                       Node beta);

    /**
     * Returns {@code true} when 2 Expressions are always equivalent.
     * This is equivalent to:
     * <ul>
     * <li>{@code isAlwaysTrue(alpha <-> beta)}.
     * </ul>
     * <img src="doc-files/are-always-equivalent.svg" alt="are always equivalent" width="150px" height="110px">
     *
     * @param alpha The alpha condition.
     * @param beta The beta condition.
     * @return {@code true} when {@code alpha} and {@code beta} are always equivalent.
     */
    public boolean areAlwaysEquivalent(Expression alpha,
                                       Expression beta);

    /**
     * Returns {@code true} when it is {@code true} that an alpha condition intersects with a beta condition.
     * <p>
     * When {@code alpha} or {@code beta} condition is {@code false}, returns {@code false}.
     * This is equivalent to:
     * <ul>
     * <li>{@code isSometimesTrue(alpha & beta)}.
     * </ul>
     * <img src="doc-files/intersects.svg" alt="intersects" width="150px" height="110px">
     *
     * @param alpha The alpha condition.
     * @param beta The beta condition.
     * @return {@code true} when it is {@code true} that a {@code alpha} condition intersects with {@code beta} condition.
     */
    public boolean intersects(Node alpha,
                              Node beta);

    /**
     * Returns {@code true} when it is {@code true} that an alpha condition intersects with a beta condition.
     * <p>
     * When {@code alpha} or {@code beta} condition is {@code false}, returns {@code false}.
     * This is equivalent to:
     * <ul>
     * <li>{@code isSometimesTrue(alpha & beta)}.
     * </ul>
     * <img src="doc-files/intersects.svg" alt="intersects" width="150px" height="110px">
     *
     * @param alpha The alpha condition.
     * @param beta The beta condition.
     * @return {@code true} when it is {@code true} that a {@code alpha} condition intersects with {@code beta} condition.
     */
    public boolean intersects(Expression alpha,
                              Expression beta);

    /**
     * Returns {@code true} when it is {@code true} that an outer condition contains an inner condition.
     * <p>
     * When {@code inner} condition is false, answer is {@code true}.
     * This is equivalent to:
     * <ul>
     * <li>{@code !isSometimesTrue(!outer & inner)}.
     * <li>{@code isNeverTrue(!outer & inner)}.
     * <li>{@code isContained(inner, outer)}.
     * </ul>
     * <img src="doc-files/contains.svg" alt="contains" width="150px" height="110px">
     *
     * @param outer The outer condition.
     * @param inner The inner condition.
     * @return {@code true} when it is {@code true} that a {@code outer} condition contains {@code inner} condition.
     */
    public boolean contains(Node outer,
                            Node inner);

    /**
     * Returns {@code true} when it is {@code true} that an outer condition contains an inner condition.
     * <p>
     * When {@code inner} condition is false, answer is {@code true}.
     * This is equivalent to:
     * <ul>
     * <li>{@code !isSometimesTrue(!outer & inner)}.
     * <li>{@code isNeverTrue(!inner & outer)}.
     * <li>{@code isContained(inner, outer)}.
     * </ul>
     * <img src="doc-files/contains.svg" alt="contains" width="150px" height="110px">
     *
     * @param outer The outer condition.
     * @param inner The inner condition.
     * @return {@code true} when it is {@code true} that a {@code outer} condition contains {@code inner} condition.
     */
    public boolean contains(Expression outer,
                            Expression inner);

    /**
     * Returns {@code true} when it is {@code always true} that an outer condition contains an inner condition,
     * and the inner condition is not {@code false}.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isNeverTrue(!inner & outer) & isSometimesTrue(inner)}.
     * <li>{@code isNonEmptyContained(inner, outer)}.
     * </ul>
     * <img src="doc-files/contains-non-empty.svg" alt="contains non empty" width="150px" height="110px">
     *
     * @param outer The outer condition.
     * @param inner The inner condition.
     * @return {@code true} when it is {@code always true} that {@code outer} condition contains {@code inner} condition
     *         and {@code inner} is not {@code false}.
     */
    public boolean containsNonEmpty(Node outer,
                                    Node inner);

    /**
     * Returns {@code true} when it is {@code always true} that an outer condition contains an inner condition,
     * and the inner condition is not {@code false}.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isNeverTrue(!inner & outer) & isSometimesTrue(inner)}.
     * <li>{@code isNonEmptyContained(inner, outer)}.
     * </ul>
     * <img src="doc-files/contains-non-empty.svg" alt="contains non empty" width="150px" height="110px">
     *
     * @param outer The outer condition.
     * @param inner The inner condition.
     * @return {@code true} when it is {@code always true} that {@code outer} condition contains {@code inner} condition
     *         and {@code inner} is not {@code false}.
     */
    public boolean containsNonEmpty(Expression outer,
                                    Expression inner);

    /**
     * Returns {@code true} when it is {@code true} that an inner condition is contained in an outer condition.
     * <p>
     * When {@code inner} condition is false, answer is {@code true}.
     * This is equivalent to:
     * <ul>
     * <li>{@code !isSometimesTrue(!outer & inner)}.
     * <li>{@code isNeverTrue(!inner & outer)}.
     * <li>{@code contains(outer, inner)}.
     * </ul>
     *
     * @param inner The inner condition.
     * @param outer The outer condition.
     * @return {@code true} when it is {@code true} that {@code inner} condition is contained in {@code outer} condition.
     */
    public boolean isContained(Node inner,
                               Node outer);

    /**
     * Returns {@code true} when it is {@code true} that an inner condition is contained in an outer condition.
     * <p>
     * When {@code inner} condition is false, answer is {@code true}.
     * This is equivalent to:
     * <ul>
     * <li>{@code !isSometimesTrue(!outer & inner)}.
     * <li>{@code isNeverTrue(!inner & outer)}.
     * <li>{@code contains(outer, inner)}.
     * </ul>
     *
     * @param inner The inner condition.
     * @param outer The outer condition.
     * @return {@code true} when it is {@code true} that {@code inner} condition is contained in {@code outer} condition.
     */
    public boolean isContained(Expression inner,
                               Expression outer);

    /**
     * Returns {@code true} when it is {@code always true} that an inner condition is not {@code false} and is
     * contained in an outer condition.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isNeverTrue(!inner & outer) & isSometimesTrue(inner)}.
     * <li>{@code containsNonEmpy(outer, inner)}.
     * </ul>
     *
     * @param inner The inner condition.
     * @param outer The outer condition.
     * @return {@code true} when it is {@code always true} that {@code inner} condition is not {@code false},
     *         and is contained in {@code outer} condition.
     */
    public boolean isNonEmptyContained(Node inner,
                                       Node outer);

    /**
     * Returns {@code true} when it is {@code always true} that an inner condition is not {@code false} and is
     * contained in an outer condition.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isNeverTrue(!inner & outer) & isSometimesTrue(inner)}.
     * <li>{@code containsNonEmpy(outer, inner)}.
     * </ul>
     *
     * @param inner The inner condition.
     * @param outer The outer condition.
     * @return {@code true} when it is {@code always true} that {@code inner} condition is not {@code false},
     *         and is contained in {@code outer} condition.
     */
    public boolean isNonEmptyContained(Expression inner,
                                       Expression outer);

    /**
     * Returns {@code true} when it is {@code always true} that {@code at least one} condition is {@code true}.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isAlwaysTrue(atLeastOne(nodes))}.
     * </ul>
     *
     * @param nodes The conditions.
     * @return {@code true} when it is {@code always true} that {@code at least one} of {@code nodes} is {@code true}.
     * @throws IllegalArgumentException When {@code nodes} is {@code null} or empty.
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean alwaysAtLeastOne(Node... nodes);

    /**
     * Returns {@code true} when it is {@code always true} that {@code at least one} condition is {@code true}.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isAlwaysTrue(atLeastOne(expressions))}.
     * </ul>
     *
     * @param expressions The conditions.
     * @return {@code true} when it is {@code always true} that {@code at least one} of {@code expressions} is {@code true}.
     * @throws IllegalArgumentException When {@code expressions} is {@code null} or empty.
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean alwaysAtLeastOne(Expression... expressions);

    /**
     * Returns {@code true} when it is {@code always true} that {@code at most one} condition is {@code true}.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isAlwaysTrue(atMostOne(nodes))}.
     * </ul>
     *
     * @param nodes The conditions.
     * @return {@code true} when it is {@code always true} that {@code at most one} of {@code nodes} is {@code true}.
     * @throws IllegalArgumentException When {@code nodes} is {@code null}.
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean alwaysAtMostOne(Node... nodes);

    /**
     * Returns {@code true} when it is {@code always true} that {@code at most one} condition is {@code true}.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isAlwaysTrue(atMostOne(nodes))}.
     * </ul>
     *
     * @param expressions The conditions.
     * @return {@code true} when it is {@code always true} that {@code at most one} of {@code expressions} is {@code true}.
     * @throws IllegalArgumentException When {@code nodes} is {@code null}.
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean alwaysAtMostOne(Expression... expressions);

    /**
     * Returns {@code true} when it is {@code always true} that {@code exactly one} condition is {@code true}.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isAlwaysTrue(exactlyOne(nodes))}.
     * </ul>
     *
     * @param nodes The conditions.
     * @return {@code true} when it is {@code always true} that {@code exactly one} of {@code nodes} is {@code true}.
     * @throws IllegalArgumentException When {@code nodes} is {@code null} or empty.
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean alwaysExactlyOne(Node... nodes);

    /**
     * Returns {@code true} when it is {@code always true} that {@code exactly one} condition is {@code true}.
     * <p>
     * This is equivalent to:
     * <ul>
     * <li>{@code isAlwaysTrue(exactlyOne(expressions))}.
     * </ul>
     *
     * @param expressions The conditions.
     * @return {@code true} when it is {@code always true} that {@code exactly one} of {@code expressions} is {@code true}.
     * @throws IllegalArgumentException When {@code nodes} is {@code null} or empty.
     * @throws ApplicException When there is a semantic or SatSolver problem.
     */
    public boolean alwaysExactlyOne(Expression... expressions);

    public boolean alwaysAtLeastOneInContext(Node context,
                                             Node... nodes);

    public boolean alwaysAtLeastOneInContext(Expression context,
                                             Expression... expressions);

    public boolean alwaysAtMostOneInContext(Node context,
                                            Node... nodes);

    public boolean alwaysAtMostOneInContext(Expression context,
                                            Expression... expressions);

    public boolean alwaysExactlyOneInContext(Node context,
                                             Node... nodes);

    public boolean alwaysExactlyOneInContext(Expression context,
                                             Expression... expressions);

    public ProverMatching getMatching(Node node);

    public ProverMatching getMatching(Expression expression);

    public ProverMatching getProjectedMatching(Node domain,
                                               Node on);

    public ProverMatching getProjectedMatching(Expression domain,
                                               Expression on);
}