package cdc.applic.proofs;

import java.util.Objects;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.ReserveStrategy;

/**
 * Set of features used to configure a prover.
 *
 * @author Damien Carbonne
 */
public final class ProverFeatures {
    public static final ProverFeatures INCLUDE_ASSERTIONS_NO_RESERVES =
            ProverFeatures.builder()
                          .assertionStrategy(AssertionStrategy.INCLUDE_ASSERTIONS)
                          .reserveStrategy(ReserveStrategy.NO_RESERVES)
                          .build();
    public static final ProverFeatures INCLUDE_ASSERTIONS_USER_DEFINED_RESERVES =
            ProverFeatures.builder()
                          .assertionStrategy(AssertionStrategy.INCLUDE_ASSERTIONS)
                          .reserveStrategy(ReserveStrategy.USER_DEFINED_RESERVES)
                          .build();
    public static final ProverFeatures INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES =
            ProverFeatures.builder()
                          .assertionStrategy(AssertionStrategy.INCLUDE_ASSERTIONS)
                          .reserveStrategy(ReserveStrategy.ALL_POSSIBLE_RESERVES)
                          .build();

    public static final ProverFeatures EXCLUDE_ASSERTIONS_NO_RESERVES =
            ProverFeatures.builder()
                          .assertionStrategy(AssertionStrategy.EXCLUDE_ASSERTIONS)
                          .reserveStrategy(ReserveStrategy.NO_RESERVES)
                          .build();
    public static final ProverFeatures EXCLUDE_ASSERTIONS_USER_DEFINED_RESERVES =
            ProverFeatures.builder()
                          .assertionStrategy(AssertionStrategy.EXCLUDE_ASSERTIONS)
                          .reserveStrategy(ReserveStrategy.USER_DEFINED_RESERVES)
                          .build();
    public static final ProverFeatures EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES =
            ProverFeatures.builder()
                          .assertionStrategy(AssertionStrategy.EXCLUDE_ASSERTIONS)
                          .reserveStrategy(ReserveStrategy.ALL_POSSIBLE_RESERVES)
                          .build();

    private final AssertionStrategy assertionStrategy;
    private final ReserveStrategy reserveStrategy;
    private final int timeoutMillis;

    protected ProverFeatures(Builder builder) {
        this.assertionStrategy = builder.assertionStrategy;
        this.reserveStrategy = builder.reserveStrategy;
        this.timeoutMillis = builder.timeoutMillis;
    }

    /**
     * @return The assertion inclusion.
     */
    public AssertionStrategy getAssertionStrategy() {
        return assertionStrategy;
    }

    /**
     * @return The reserve strategy.
     */
    public ReserveStrategy getReserveStrategy() {
        return reserveStrategy;
    }

    /**
     * Returns the maximum time that can be spent in solving problem.
     * <p>
     * This does not include time spent in preparing the problem to be submitted to solver.
     *
     * @return The solver timeout.
     *         A Negative value means no timeout.
     */
    public int getTimeoutMillis() {
        return timeoutMillis;
    }

    @Override
    public int hashCode() {
        return Objects.hash(assertionStrategy,
                            reserveStrategy,
                            timeoutMillis);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof ProverFeatures)) {
            return false;
        }
        final ProverFeatures other = (ProverFeatures) object;
        return assertionStrategy == other.assertionStrategy
                && reserveStrategy == other.reserveStrategy
                && timeoutMillis == other.timeoutMillis;
    }

    @Override
    public String toString() {
        return "[" + getAssertionStrategy()
                + ", " + getReserveStrategy()
                + ", " + (getTimeoutMillis() < 0 ? "No timeout" : getTimeoutMillis() + "ms")
                + "]";
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder of ProverFeatures.
     *
     * @author Damien Carbonne
     */
    public static class Builder {
        private AssertionStrategy assertionStrategy = AssertionStrategy.EXCLUDE_ASSERTIONS;
        private ReserveStrategy reserveStrategy = ReserveStrategy.NO_RESERVES;
        private int timeoutMillis = -1;

        public Builder assertionStrategy(AssertionStrategy assertionStrategy) {
            this.assertionStrategy = assertionStrategy;
            return this;
        }

        public Builder reserveStrategy(ReserveStrategy reserveStrategy) {
            this.reserveStrategy = reserveStrategy;
            return this;
        }

        public Builder timeoutMillis(int timoutMillis) {
            if (timoutMillis < 0) {
                this.timeoutMillis = -1;
            } else {
                this.timeoutMillis = timoutMillis;
            }
            return this;
        }

        public ProverFeatures build() {
            return new ProverFeatures(this);
        }
    }
}