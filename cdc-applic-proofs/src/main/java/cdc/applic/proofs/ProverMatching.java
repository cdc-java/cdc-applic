package cdc.applic.proofs;

public enum ProverMatching {
    NEVER,
    SOMETIMES,
    ALWAYS
}