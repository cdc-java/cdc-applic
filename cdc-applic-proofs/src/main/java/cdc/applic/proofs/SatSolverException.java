package cdc.applic.proofs;

import cdc.applic.expressions.ApplicException;

public class SatSolverException extends ApplicException {
    private static final long serialVersionUID = 1L;

    public SatSolverException(String message,
                              Throwable cause) {
        super(message, cause);
    }
}