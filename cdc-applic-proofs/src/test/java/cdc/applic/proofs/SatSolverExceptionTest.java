package cdc.applic.proofs;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class SatSolverExceptionTest {
    @Test
    void test() {
        assertEquals("Message", new SatSolverException("Message", null).getMessage());
    }
}