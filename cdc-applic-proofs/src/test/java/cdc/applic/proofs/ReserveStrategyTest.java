package cdc.applic.proofs;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.types.Type;

class ReserveStrategyTest {
    private final RepositoryImpl repository = new RepositoryImpl();
    private final RegistryImpl registry = repository.registry().name("Registry").build();
    private final Type tNonModifiable = registry.booleanType().name("NonModifiable").build();
    private final Type tModifiableFrozen =
            registry.integerType().name("ModifiableFrozen").frozen(true).domain("1~10").build();
    private final Type tModifiableNonFrozen =
            registry.integerType().name("ModifiableNonFrozen").frozen(false).domain("1~10").build();

    @Test
    void testHasReserve() {
        assertFalse(ReserveStrategy.ALL_POSSIBLE_RESERVES.hasReserve(tNonModifiable));
        assertFalse(ReserveStrategy.NO_RESERVES.hasReserve(tNonModifiable));
        assertFalse(ReserveStrategy.USER_DEFINED_RESERVES.hasReserve(tNonModifiable));

        assertTrue(ReserveStrategy.ALL_POSSIBLE_RESERVES.hasReserve(tModifiableFrozen));
        assertFalse(ReserveStrategy.NO_RESERVES.hasReserve(tModifiableFrozen));
        assertFalse(ReserveStrategy.USER_DEFINED_RESERVES.hasReserve(tModifiableFrozen));

        assertTrue(ReserveStrategy.ALL_POSSIBLE_RESERVES.hasReserve(tModifiableNonFrozen));
        assertFalse(ReserveStrategy.NO_RESERVES.hasReserve(tModifiableNonFrozen));
        assertTrue(ReserveStrategy.USER_DEFINED_RESERVES.hasReserve(tModifiableNonFrozen));
    }
}