package cdc.applic.proofs;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.ReserveStrategy;

class ProverFeaturesTest {

    @Test
    void testMisc() {
        assertEquals(AssertionStrategy.INCLUDE_ASSERTIONS, ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES.getAssertionStrategy());
        assertEquals(ReserveStrategy.NO_RESERVES, ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES.getReserveStrategy());
        assertNotEquals(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES.hashCode(),
                        ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES.hashCode());
        assertNotEquals(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES.toString(),
                        ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES.toString());
    }

    @Test
    void testEquals() {
        final ProverFeatures f =
                ProverFeatures.builder()
                              .assertionStrategy(AssertionStrategy.INCLUDE_ASSERTIONS)
                              .reserveStrategy(ReserveStrategy.NO_RESERVES)
                              .build();
        assertEquals(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES, ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES);
        assertEquals(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES, f);
        assertNotEquals(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES, ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES);
        assertNotEquals(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES, ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES);
        assertNotEquals(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES, "Hello");
        assertNotEquals(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES, null);

        final ProverFeatures f100 =
                ProverFeatures.builder()
                              .assertionStrategy(AssertionStrategy.INCLUDE_ASSERTIONS)
                              .reserveStrategy(ReserveStrategy.NO_RESERVES)
                              .timeoutMillis(100)
                              .build();
        assertNotEquals(f, f100);
        assertEquals(f100.toString(), f100.toString());

        final ProverFeatures f1 =
                ProverFeatures.builder()
                              .assertionStrategy(AssertionStrategy.INCLUDE_ASSERTIONS)
                              .reserveStrategy(ReserveStrategy.NO_RESERVES)
                              .timeoutMillis(-100)
                              .build();
        assertEquals(f, f1);
    }
}