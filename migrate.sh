#!/bin/sh

# Utility to copy code and rename packages from cdc/applic to com/da/applicabilities
   
input=`pwd`
case "`uname -o`" in
   "Cygwin")
      echo "WIN"
      output=C:/Temp/tmp
      ;;
    *)
      echo "OTHER"
      output=~/tmp
      ;;
esac
root="cdc-applic"
nroot="com-da-applicabilities"

title() {
   echo "============================================================="
   echo $*
   echo "============================================================="
}

init() {
   title "INIT"
   rm -fr $output/$root
   rm -fr $output/$nroot
   mkdir -p $output/$root
   #echo $input
   #echo $output
   cp -Rv $input $output
   rm -frv `find $output -type d -name target`
   rm -frv `find $output -type d -name .settings`
   rm -frv `find $output -type d -name .git`
   rm -fv `find $output -type f -name .project`
   rm -fv `find $output -type f -name .classpath`
   rm -fv `find $output -type f -name .factorypath`
   rm -fv `find $output -type f -path "*benchmarks*csv"`
   rm -fv `find $output -type f -name COPYING.LESSER`
   rm -fv `find $output -type f -name LICENSE`
}

renamedirs() {
   title "RENAME DIRS"
   mv $output/$root $output/$nroot  
    
   for dir in `find  $output/$nroot -maxdepth 1 -type d`
   do
      target=`echo $dir | sed -e "s/$root/$nroot/g"`  
      echo $dir $target
      if [ $dir != $target ]
      then
         mv $dir $target
      fi
   done
   
   for dir in `find $output/$nroot -type d -name applic`
   do
      target=`echo $dir | sed -e "s/cdc\/applic/com\/da\/applicabilities/g"`
      echo $target
      mkdir -p $target
      mv $dir/* $target
      rmdir $dir
   done
   
   for dir in `find $output/$nroot -type d -name cdc`
   do
      rmdir $dir
   done
}

fixjava() {
   title "FIX Java"
   for file in `find $output/$nroot -type f -name "*.java"`
   do
      echo "$file"
      tmp="$file.tmp"
      cat $file | sed -e "s/cdc.applic./com.da.applicabilities./" > $tmp
      mv "$tmp" "$file"
   done
}

fixpom() {
   title "FIX POM"
   for file in `find $output/$nroot -type f -name "pom.xml"`
   do
      echo "$file"
      sed -i \
          -e "s/cdc-applic/com-da-applicabilities/" \
          -e "s/com.gitlab.cdc-java.applic/com.da.applicabilities/" \
          -e "s/CDC - Applic/Applicabilities -/" \
          -e "/<url>https:\/\/www.gitlab.com/d" \
          -e "s/0.13.0-SNAPSHOT/2.2.0-SNAPSHOT/" \
          -e "s/ - <\/name>/<\/name>/" \
          "$file"
   done
   
   # TODO remove developper, scm, license and distributionManagement 
}

fixeol() {
   title "FIX EOL"
   for file in `find $output/$nroot -type f`
   do
      case "$file" in
      *.png|*.xlsx)
        echo "skip $file"
        ;;
      *) 
         unix2dos "$file"
         ;;
      esac
   done
}

#fixjava1() {
#   file=$output/$nroot/com-da-applicabilities-policies-impl/src/main/java/com/da/applicabilities/policies/impl/io/RepositoryXml.java
#   echo $file
#   sed -i \
#       -e "s/https:\/\/www.gitlab.com\/cdc-java/https:\/\/www.dassault-aviation.com\/acs/" \
#       -e "s/https:\/\/www.gitlab.com\/cdc-java\/applic-repository.xsd/https:\/\/www.dassault-aviation.com\/acs\/applic-repository.xsd/" \
#       $file
#}

fixjava2() {
   title "FIX Java 2"
   file=$output/$nroot/com-da-applicabilities-dictionaries-impl/src/test/java/com/da/applicabilities/dictionaries/impl/RepositoryXmlTest.java
   echo $file
   sed -i \
       -e "s/src\/main\/resources\/com.da.applicabilities.applic-repository.xsd/src\/main\/resources\/com\/da\/applicabilities\/applic-repository.xsd/" \
       "$file"
}

fixjava3() {
   title "FIX Java 3"
   file=$output/$nroot/com-da-applicabilities-proofs-core/src/test/java/com/da/applicabilities/proofs/core/EncoderSatSolverTest.java
   echo $file
   sed -i \
       -e "s/UNIVERSE_MAX_SIZE = 3/UNIVERSE_MAX_SIZE = 1/" \
       -e "s/NUMBER_OF_SETS = 6/NUMBER_OF_SETS = 2/" \
       "$file"
}

fixjavaxsd() {
   title "FIX Java XSD"
   files=""
   files="$files $output/$nroot/com-da-applicabilities-dictionaries-impl/src/main/java/com/da/applicabilities/dictionaries/impl/io/RepositoryXml.java"
   files="$files $output/$nroot/com-da-applicabilities-mountability-impl/src/main/java/com/da/applicabilities/mountability/impl/io/DataXml.java"
   files="$files $output/$nroot/com-da-applicabilities-consistency-impl/src/main/java/com/da/applicabilities/consistency/impl/io/ConsistencyDataXml.java"
   files="$files $output/$nroot/com-da-applicabilities-consistency/src/main/java/com/da/applicabilities/consistency/model/io/CyModelXmlIo.java"
   files="$files $output/$nroot/com-da-applicabilities-publication-core/src/main/java/com/da/applicabilities/publication/core/formatters/io/FormattersCatalogXml.java"

   for file in $files
   do   
      echo $file
      sed -i \
          -e "s/https:\/\/www.gitlab.com\/cdc-java/https:\/\/www.dassault-aviation.com\/acs/" \
          "$file"
   done   
}

fixxsd()  {
   title "FIX XSD"
   for file in `find $output/$nroot -type f -name "*.xsd"`
   do
      echo $file
      sed -i \
          -e "s/cdcj:/acs:/g" \
          -e "s/xmlns:cdcj/xmlns:acs/g" \
          -e "s/https:\/\/www.gitlab.com\/cdc-java/https:\/\/www.dassault-aviation.com\/acs/g" \
          "$file"
   done
}

fixxml() {
   title "FIX XML"
   for file in `find $output/$nroot -type f -name "*.xml"`
   do
      echo $file
      sed -i \
          -e "s/https:\/\/www.gitlab.com\/cdc-java/https:\/\/www.dassault-aviation.com\/acs/g" \
          "$file"
   done
}

fixmd() {
   title "FIX MD"
   for file in `find $output/$nroot -type f -name "*.md"`
   do
      echo $file
      sed -i \
          -e "s/cdc-applic/com-da-applicabilities/g" \
          -e "s/cdc\/applic/com\/da\/applicabilities/g" \
          "$file"
   done
}

fixExpression() {
   title "FIX Expression"
   
   file=`find $output/$nroot -type f -name "Expression.java"`
   {
      cat $file | sed "/^}$/d"
      cat << EOF

    // Compatibility
    public static String toInternal(Expression expression) {
        return asString(expression);
    }

    // Compatibility
    public static Expression toExternal(String expression) {
        return fromString(expression);
    }

    // Compatibility
    public String getText() {
        return getContent();
    }

    // Compatibility
    public static Expression create(ParsingNode node) {
        return fromNode(node);
    }
}
EOF
   } > $file.tmp
   mv $file.tmp $file
}

fixName() {
   title "FIX Name"
   
   file=`find $output/$nroot -type f -name "Name.java"`
   {
      cat $file | sed "/^}$/d"
      cat << EOF

    // Compatibility
    public static String toInternal(Name x) {
        return x == null ? null : x.getNonEscapedLiteral();
    }

    // Compatibility
    public static Name toExternal(String s) {
        return s == null ? null : of(s);
    }
}
EOF
   } > $file.tmp
   mv $file.tmp $file
}

fixSName() {
   title "FIX SName"
   
   file=`find $output/$nroot -type f -name "SName.java"`
   {
      cat $file | sed "/^}$/d"
      cat << EOF

    // Compatibility
    public static String toInternal(SName x) {
        return x == null ? null : x.getNonEscapedLiteral();
    }

    // Compatibility
    public static SName toExternal(String s) {
        return s == null ? null : of(s);
    }
}
EOF
   } > $file.tmp
   mv $file.tmp $file
}

fixStringValue() {
   title "FIX StringValue"
   
   file=`find $output/$nroot -type f -name "StringValue.java"`
   {
      cat $file | sed "/^}$/d"
      cat << EOF

    // Compatibility
    public static String toInternal(StringValue x) {
        return x == null ? null : x.getNonEscapedLiteral();
    }

    // Compatibility
    public static StringValue toExternal(String s) {
        return s == null ? null : of(s);
    }
}
EOF
   } > $file.tmp
   mv $file.tmp $file
}


time {
time init
time renamedirs
time fixjava
time fixpom

#time fixjava1
time fixjava2
time fixjava3
time fixjavaxsd
time fixxsd
time fixxml
time fixmd
time fixExpression
time fixName
time fixSName
time fixStringValue

time fixeol
}