package cdc.applic.tools;

import java.io.File;
import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.consistency.checks.CyProfile;
import cdc.applic.consistency.checks.models.CyModelChecker;
import cdc.applic.consistency.model.CyModel;
import cdc.applic.consistency.model.io.CyModelIo;
import cdc.applic.dictionaries.core.writing.checks.DefaultWritingRule;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.io.RepositoryIo;
import cdc.issues.IssuesCollector;
import cdc.issues.checks.CheckStats;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.checks.io.WorkbookCheckStatsIo;
import cdc.issues.impl.SnapshotDataImpl;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesWriter;
import cdc.issues.io.OutSettings;
import cdc.issues.io.ProfileIo;
import cdc.issues.io.ProfileIoFeatures;
import cdc.issues.locations.Location;
import cdc.issues.rules.ProfileConfig;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.events.ProgressController;
import cdc.util.lang.FailureReaction;
import cdc.util.time.Chronometer;

public final class ApplicConsistencyChecker {
    private static final Logger LOGGER = LogManager.getLogger(ApplicConsistencyChecker.class);
    private final MainArgs margs;

    public static final String ISSUES = "-issues.xlsx";
    public static final String STATS = "-check-stats.xlsx";

    private ApplicConsistencyChecker(MainArgs margs) {
        this.margs = margs;
    }

    public static class MainArgs {
        /** Input applic repository file. */
        public File repositoryFile;
        /** Input CY model file. */
        public File modelFile;
        /** Name of the profile config file to load. */
        public File profileConfigFile;
        /** Output directory. */
        public File outputDir;
        /** Base name of read / generated files. */
        public String basename;

        /** Set of enabled features. */
        public final FeatureMask<Feature> features = new FeatureMask<>();

        public enum Feature implements OptionEnum {
            FASTEST("fastest",
                    "Use options that are fast to generate output (default)."),
            BEST("best",
                 "Use options that generate best output."),
            VERBOSE("verbose",
                    "Print messages.");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }
    }

    private void log(String message,
                     Object... params) {
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info(message, params);
        }
    }

    void execute() throws IOException {
        final Chronometer total = new Chronometer();
        final Chronometer chrono = new Chronometer();

        log("Checking applic consistency");
        total.start();

        DefaultWritingRule.elaborate();

        log("Loading applic repository {}", margs.repositoryFile);
        chrono.start();
        final RepositoryImpl repository = RepositoryIo.load(margs.repositoryFile, FailureReaction.FAIL);
        chrono.suspend();
        log("Loaded applic repository ({})", chrono);

        log("Loading applic consistency model {}", margs.modelFile);
        chrono.start();
        final CyModel model = CyModelIo.load(margs.modelFile, repository);
        chrono.suspend();
        log("Loaded applic consistency model ({})", chrono);

        margs.outputDir.mkdirs();

        final String projectName = margs.modelFile.getName(); // FIXME

        if (margs.profileConfigFile != null) {
            log("Loading profile config {}", margs.profileConfigFile);
            chrono.start();
            final ProfileConfig profileConfig = ProfileIo.loadProfileConfig(ProfileIoFeatures.FASTEST, margs.profileConfigFile);
            chrono.suspend();
            log("Loaded profile config ({})", chrono);

            CyProfile.PROFILE.apply(profileConfig);
        }

        // Check model
        log("Checking model");
        chrono.start();
        final SnapshotManager manager = CyModelChecker.check(projectName,
                                                             model,
                                                             new IssuesCollector<>());
        chrono.suspend();
        log("Checked model ({})", chrono);

        // Save issues
        final File issuesFile = new File(margs.outputDir, margs.basename + ISSUES);
        log("Saving issues to {}", issuesFile);
        chrono.start();

        final SnapshotDataImpl synthesis = new SnapshotDataImpl();
        synthesis.setNumberOfIssues(manager.getIssuesCollector().getIssues().size());
        synthesis.setProjectName(projectName);
        synthesis.setProfile(CyProfile.PROFILE);
        IssuesWriter.save(synthesis,
                          manager.getIssuesCollector().getIssues(),
                          OutSettings.builder()
                                     .hint(OutSettings.Hint.NO_ANSWERS)
                                     .hint(OutSettings.Hint.AUTO_LOCATIONS)
                                     .build(),
                          issuesFile,
                          ProgressController.VOID,
                          margs.features.isEnabled(MainArgs.Feature.BEST)
                                  ? IssuesIoFactoryFeatures.UTC_BEST
                                  : IssuesIoFactoryFeatures.UTC_FASTEST);
        chrono.suspend();
        log("Saved issues ({})", chrono);

        // Save stats
        final File statsFile = new File(margs.outputDir, margs.basename + STATS);

        final CheckStats<Location> stats = manager.getStats().orElseThrow();
        log("Saving stats to {}", statsFile);
        chrono.start();
        final WorkbookCheckStatsIo<Location> io = new WorkbookCheckStatsIo<>(x -> Location.toString(x, false));
        io.save(stats,
                statsFile,
                margs.features.isEnabled(MainArgs.Feature.BEST)
                        ? WorkbookWriterFeatures.STANDARD_BEST
                        : WorkbookWriterFeatures.STANDARD_FAST);
        chrono.suspend();
        log("Saved stats ({})", chrono);

        total.suspend();
        log("Checked applic consistency ({})", chrono);
    }

    public static void execute(MainArgs margs) throws IOException {
        final ApplicConsistencyChecker instance = new ApplicConsistencyChecker(margs);
        instance.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String BASENAME = "basename";
        private static final String MODEL_FILE = "model";
        private static final String PROFILE_CONFIG_FILE = "profile-config";
        private static final String REPOSITORY_FILE = "repository";

        protected MainSupport() {
            super(ApplicConsistencyChecker.class,
                  LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected String getHelpHeader() {
            return "Utility that can analyze an applicability model and check its consistency.";
        }

        @Override
        protected boolean addArgsFileOption(Options options) {
            return true;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(BASENAME)
                                    .desc("Mandatory base name of generated files.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(OUTPUT_DIR)
                                    .desc("Mandatory name of the output directory. If this directory does not exist, it is created.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(REPOSITORY_FILE)
                                    .desc("Mandatory name of the Applic Repository file to load.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(MODEL_FILE)
                                    .desc("Mandatory name of the Consistency Model file to load.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(PROFILE_CONFIG_FILE)
                                    .desc("Optional name of the Consistency Profile Config file to load.")
                                    .hasArg()
                                    .build());

            addNoArgOptions(options, MainArgs.Feature.class);
            createGroup(options, MainArgs.Feature.BEST, MainArgs.Feature.FASTEST);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();

            margs.basename = getValueAsString(cl, BASENAME, "xxx");
            margs.repositoryFile = getValueAsResolvedFile(cl, REPOSITORY_FILE, IS_FILE);
            margs.modelFile = getValueAsResolvedFile(cl, MODEL_FILE, IS_FILE);
            margs.profileConfigFile = getValueAsResolvedFile(cl, PROFILE_CONFIG_FILE, IS_NULL_OR_FILE);
            margs.outputDir = getValueAsResolvedFile(cl, OUTPUT_DIR);

            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws Exception {
            ApplicConsistencyChecker.execute(margs);
            return null;
        }
    }
}