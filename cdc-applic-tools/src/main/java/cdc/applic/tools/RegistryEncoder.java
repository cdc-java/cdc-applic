package cdc.applic.tools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.Constraint;
import cdc.applic.dictionaries.Constraints;
import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.bindings.DictionaryConverter;
import cdc.applic.dictionaries.bindings.SItemSetPropertyPair;
import cdc.applic.dictionaries.bindings.TypesBinding;
import cdc.applic.dictionaries.core.constraints.AbstractExpressionsConstraint;
import cdc.applic.dictionaries.core.constraints.PropertyRestrictionConstraint;
import cdc.applic.dictionaries.core.conversions.DictionaryConverterImpl;
import cdc.applic.dictionaries.impl.AbstractDictionaryImpl;
import cdc.applic.dictionaries.impl.AbstractTypeImpl;
import cdc.applic.dictionaries.impl.AliasImpl;
import cdc.applic.dictionaries.impl.BooleanTypeImpl;
import cdc.applic.dictionaries.impl.EnumeratedTypeImpl;
import cdc.applic.dictionaries.impl.EnumeratedValueImpl;
import cdc.applic.dictionaries.impl.IntegerTypeImpl;
import cdc.applic.dictionaries.impl.PatternTypeImpl;
import cdc.applic.dictionaries.impl.PolicyImpl;
import cdc.applic.dictionaries.impl.RealTypeImpl;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.bindings.DictionariesBindingImpl;
import cdc.applic.dictionaries.impl.bindings.EnumeratedEnumeratedBindingImpl;
import cdc.applic.dictionaries.impl.io.RepositoryIo;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.items.UserDefinedAssertion;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.EnumeratedValue;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.RealType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.Named;
import cdc.applic.expressions.literals.SName;
import cdc.tuples.Tuple2;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;
import cdc.util.lang.FailureReaction;

/**
 * Utility used to encode a source registry by:
 * <ul>
 * <li>Creating a target encoded registry
 * <li>Creating a registry binding.
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class RegistryEncoder {
    private static final Logger LOGGER = LogManager.getLogger(RegistryEncoder.class);
    private final MainArgs margs;
    private RepositoryImpl repository;
    private RegistryImpl sourceRegistry;
    private RegistryImpl targetRegistry;
    private DictionariesBindingImpl registriesBinding;
    private DictionaryConverter registryConverter;
    /** (source type, target type) map. */
    private final Map<AbstractTypeImpl, AbstractTypeImpl> mapTypes = new HashMap<>();
    /** (source type, (source value, target value)) map, for enumerated types. */
    private final Map<EnumeratedType, Map<EnumeratedValueImpl, EnumeratedValueImpl>> mapValues = new HashMap<>();
    /** (source item name, target item name) map. */
    private final Map<Name, Name> mapItems = new HashMap<>();

    private RegistryEncoder(MainArgs margs) {
        Constraints.elaborateStandardConstraints();
        this.margs = margs;
    }

    public static class MainArgs {
        public enum Feature implements OptionEnum {
            ENCODE_TYPES("encode-types", "If enabled, all types names are encoded, except the preserved ones."),
            ENCODE_VALUES("encode-values", "If enabled, all types values are encoded, except the preserved ones."),
            ENCODE_PROPERTIES("encode-properties", "If enabled, all properties names are encoded, except the preserved ones."),
            ENCODE_ALIASES("encode-aliases", "If enabled, all aliases names are encoded, except the preserved ones.");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }

        /**
         * Optional File containing the repository to load.
         * <p>
         * If null, then {@link #repository} must be set.<br>
         * Otherwise, the corresponding file will be loaded into {@link #repository}.
         */
        public File inputRepositoryFile;

        /**
         * Optional File containing the repository to save.
         * <p>
         * If null, then repository won't be save.
         */
        public File outputRepositoryFile;

        /**
         * Repository that contains the registry to encode, and will contain the encoded one.
         * <p>
         * If null, then the repository defined by {@link #inputRepositoryFile} will be loaded.
         */
        public RepositoryImpl repository;

        /** Mandatory name of the registry that must be encoded. */
        public String sourceRegistryName;

        /** Mandatory ame of the resulting encoded registry. */
        public String targetRegistryName;

        /** Optional prefix of the resulting encoded registry. */
        public String targetRegistryPrefix;

        /** Set of names of types whose name must be preserved. */
        public final Set<String> preservedTypes = new HashSet<>();

        /** Set of names of types whose values must be preserved. */
        public final Set<String> preservedValues = new HashSet<>();

        /** Set of names of items (Aliases and Properties) whose name must be preserved. */
        public final Set<Name> preservedItems = new HashSet<>();

        public final FeatureMask<Feature> features = new FeatureMask<>();

        public void validate() {
            Checks.assertTrue(repository == null ^ inputRepositoryFile == null,
                              "Exactly one of repository and inputRepositoryFile must be set.");
        }
    }

    void execute() throws IOException {
        margs.validate();
        if (margs.repository == null) {
            LOGGER.info("Load repository from '{}'", margs.inputRepositoryFile);
            this.repository = RepositoryIo.load(margs.inputRepositoryFile, FailureReaction.FAIL);
            LOGGER.info("Loaded repository from '{}'", margs.inputRepositoryFile);
        } else {
            this.repository = margs.repository;
        }
        this.sourceRegistry = this.repository.getRegistry(margs.sourceRegistryName);
        this.targetRegistry = this.repository.registry()
                                             .name(margs.targetRegistryName)
                                             .prefix(margs.targetRegistryPrefix)
                                             .build();

        createTargetTypes();
        createTargetProperties();
        createTargetAliases();
        createBindings();
        this.registryConverter = new DictionaryConverterImpl(this.registriesBinding);
        fixTargetAliases();
        createTargetAssertions(this.sourceRegistry, this.targetRegistry);
        createTargetConstraints(this.sourceRegistry, this.targetRegistry);
        createTargetPolicies(this.sourceRegistry, this.targetRegistry);

        if (margs.outputRepositoryFile != null) {
            LOGGER.info("Save repository to '{}'", margs.outputRepositoryFile);
            RepositoryIo.save(this.repository, margs.outputRepositoryFile);
            LOGGER.info("Saved repository to '{}'", margs.outputRepositoryFile);
        }
    }

    private AbstractTypeImpl getTargetType(Type sourceType) {
        Checks.containKey(mapTypes, sourceType, "sourceType");
        return mapTypes.get(sourceType);
    }

    private Property getTargetProperty(Property sourceProperty) {
        Checks.containKey(mapItems, sourceProperty.getName(), "sourceProperty");
        final Name targetName = mapItems.get(sourceProperty.getName());
        return targetRegistry.getProperty(targetName);
    }

    private AliasImpl getTargetAlias(Alias sourceAlias) {
        Checks.containKey(mapItems, sourceAlias.getName(), "sourceAlias");
        final Name targetName = mapItems.get(sourceAlias.getName());
        return targetRegistry.getAlias(targetName);
    }

    private NamedDItem getTargetItem(NamedDItem sourceItem) {
        Checks.containKey(mapItems, sourceItem.getName(), "sourceItem");
        final Name targetName = mapItems.get(sourceItem.getName());
        return targetRegistry.getItem(targetName);
    }

    /**
     * Duplicate source types.
     */
    private void createTargetTypes() {
        // Collect and sort source types
        final List<AbstractTypeImpl> sourceTypes = new ArrayList<>();
        sourceTypes.addAll(sourceRegistry.getDeclaredTypes());
        Collections.sort(sourceTypes, Type.NAME_COMPARATOR);

        int index = 0;
        for (final AbstractTypeImpl sourceType : sourceTypes) {
            index++;
            final SName sourceTypeName = sourceType.getName().getLocal();
            final SName targetTypeName;
            if (!margs.features.isEnabled(MainArgs.Feature.ENCODE_TYPES)
                    || margs.preservedTypes.contains(sourceTypeName.getNonEscapedLiteral())) {
                targetTypeName = sourceTypeName;
            } else {
                targetTypeName = SName.of("Type" + index);
            }
            createTargetType(sourceType, targetTypeName);
        }
    }

    private void createTargetType(AbstractTypeImpl sourceType,
                                  SName targetTypeName) {
        if (sourceType instanceof BooleanTypeImpl) {
            final BooleanTypeImpl targetType = targetRegistry.booleanType().name(targetTypeName).build();
            setS1000DAttributes(sourceType, targetType);
            mapTypes.put(sourceType, targetType);
        } else if (sourceType instanceof final IntegerTypeImpl stype) {
            final IntegerTypeImpl targetType = targetRegistry.integerType()
                                                             .name(targetTypeName)
                                                             .frozen(stype.isFrozen())
                                                             .domain(stype.getDomain())
                                                             .build();
            setS1000DAttributes(sourceType, targetType);
            mapTypes.put(sourceType, targetType);
        } else if (sourceType instanceof final RealTypeImpl stype) {
            final RealTypeImpl targetType =
                    targetRegistry.realType()
                                  .name(targetTypeName)
                                  .frozen(stype.isFrozen())
                                  .domain(stype.getDomain())
                                  .build();
            setS1000DAttributes(sourceType, targetType);
            mapTypes.put(sourceType, targetType);
        } else if (sourceType instanceof final PatternTypeImpl stype) {
            final PatternTypeImpl targetType =
                    targetRegistry.patternType()
                                  .name(targetTypeName)
                                  .frozen(stype.isFrozen())
                                  .pattern(stype.getPattern().pattern())
                                  .build();
            setS1000DAttributes(sourceType, targetType);
            mapTypes.put(sourceType, targetType);
        } else {
            final EnumeratedTypeImpl stype = (EnumeratedTypeImpl) sourceType;
            // Create the target type
            final EnumeratedTypeImpl targetType =
                    targetRegistry.enumeratedType()
                                  .name(targetTypeName)
                                  .frozen(stype.isFrozen())
                                  .build();
            // Map from source value to target value
            final Map<EnumeratedValueImpl, EnumeratedValueImpl> map = new HashMap<>();

            // Create the target values
            if (!margs.features.isEnabled(MainArgs.Feature.ENCODE_VALUES)
                    || margs.preservedValues.contains(sourceType.getName().getProtectedLiteral())) {
                for (final EnumeratedValueImpl svalue : stype.getValues()) {
                    final EnumeratedValueImpl tvalue = EnumeratedValueImpl.builder()
                                                                          .literal(svalue.getLiteral())
                                                                          .shortLiteral(svalue.getShortLiteral())
                                                                          .ordinal(svalue.getOrdinal())
                                                                          .build();
                    targetType.addValue(tvalue);
                    map.put(svalue, tvalue);
                }
            } else {
                int index = 0;
                for (final EnumeratedValueImpl svalue : stype.getValues()) {
                    index++;
                    final String l = "V" + index;
                    final EnumeratedValueImpl tvalue = EnumeratedValueImpl.builder()
                                                                          .literal(l)
                                                                          .shortLiteral(l)
                                                                          .ordinal(svalue.getOrdinal())
                                                                          .build();
                    targetType.addValue(tvalue);
                    map.put(svalue, tvalue);
                }
            }

            // Create order relationship
            for (final Tuple2<EnumeratedValue, EnumeratedValue> c : stype.getOrderConstraints()) {
                targetType.addLessThan(map.get(c.value0()).getLiteral(), map.get(c.value1()).getLiteral());
            }
            setS1000DAttributes(sourceType, targetType);
            mapTypes.put(sourceType, targetType);
            mapValues.put(stype, map);
        }
    }

    private static void setS1000DAttributes(AbstractTypeImpl sourceType,
                                            AbstractTypeImpl targetType) {
        targetType.setS1000DPropertyType(sourceType.getS1000DPropertyType());
        targetType.setS1000DProductIdentifier(sourceType.getS1000DProductIdentifier());
    }

    /**
     * Duplicate source properties.
     */
    private void createTargetProperties() {
        final List<Property> sourceProperties = new ArrayList<>();
        IterableUtils.addAll(sourceRegistry.getDeclaredProperties(), sourceProperties);
        Collections.sort(sourceProperties, Named.NAME_COMPARATOR);
        int index = 0;
        for (final Property sourceProperty : sourceProperties) {
            index++;
            final Name sourceName = sourceProperty.getName();
            final SName targetName;
            if (!margs.features.isEnabled(MainArgs.Feature.ENCODE_PROPERTIES)
                    || margs.preservedItems.contains(sourceName)) {
                targetName = sourceName.getLocal();
            } else {
                targetName = SName.of("P" + index);
            }
            final Type sourceType = sourceProperty.getType();
            final AbstractTypeImpl targetType = getTargetType(sourceType);
            final Property targetProperty =
                    targetRegistry.property().name(targetName).type(targetType).ordinal(sourceProperty.getOrdinal()).build();
            mapItems.put(sourceName, targetProperty.getName());
        }
    }

    /**
     * Duplicate source aliases.
     * <p>
     * Their definition can not be set.
     */
    private void createTargetAliases() {
        final List<Alias> sourceAliases = new ArrayList<>();
        IterableUtils.addAll(sourceRegistry.getDeclaredAliases(), sourceAliases);
        Collections.sort(sourceAliases, Named.NAME_COMPARATOR);
        int index = 0;
        for (final Alias sourceAlias : sourceAliases) {
            index++;
            final Name sourceName = sourceAlias.getName();
            final SName targetName;
            if (!margs.features.isEnabled(MainArgs.Feature.ENCODE_ALIASES)
                    || margs.preservedItems.contains(sourceName)) {
                targetName = sourceName.getLocal();
            } else {
                targetName = SName.of("A" + index);
            }
            final Alias targetAlias = targetRegistry.alias().name(targetName).expression(Expression.TRUE).ordinal(index).build();
            mapItems.put(sourceName, targetAlias.getName());
        }
    }

    private void createBindings() {
        // Create the registries binding
        this.registriesBinding = repository.binding().source(sourceRegistry).target(targetRegistry).build();

        // Collect and sort types
        final List<AbstractTypeImpl> sourceTypes = new ArrayList<>();
        sourceTypes.addAll(sourceRegistry.getDeclaredTypes());
        Collections.sort(sourceTypes, Type.NAME_COMPARATOR);

        // Create types bindings
        for (final AbstractTypeImpl sourceType : sourceTypes) {
            createTypesBinding(sourceType);
        }

        // Collect and sort properties
        final List<Property> sourceProperties = new ArrayList<>();
        IterableUtils.addAll(sourceRegistry.getDeclaredProperties(), sourceProperties);
        Collections.sort(sourceProperties, Named.NAME_COMPARATOR);

        for (final Property sourceProperty : sourceProperties) {
            createPropertiesBinding(sourceProperty);
        }

        // Collect and sort aliases
        final List<Alias> sourceAliases = new ArrayList<>();
        IterableUtils.addAll(sourceRegistry.getDeclaredAliases(), sourceAliases);
        Collections.sort(sourceAliases, Named.NAME_COMPARATOR);

        for (final Alias sourceAlias : sourceAliases) {
            createAliasesBinding(sourceAlias);
        }
    }

    private void createTypesBinding(AbstractTypeImpl sourceType) {
        final AbstractTypeImpl targetType = getTargetType(sourceType);
        if (sourceType instanceof BooleanTypeImpl) {
            registriesBinding.booleanBooleanBinding()
                             .source((BooleanType) sourceType)
                             .target((BooleanType) targetType)
                             .build();
        } else if (sourceType instanceof IntegerTypeImpl) {
            registriesBinding.integerIntegerBinding()
                             .source((IntegerType) sourceType)
                             .target((IntegerType) targetType)
                             .build();
        } else if (sourceType instanceof RealTypeImpl) {
            registriesBinding.realRealBinding()
                             .source((RealType) sourceType)
                             .target((RealType) targetType)
                             .build();
        } else if (sourceType instanceof PatternTypeImpl) {
            registriesBinding.patternPatternBinding()
                             .source((PatternType) sourceType)
                             .target((PatternType) targetType)
                             .build();
        } else {
            registriesBinding.enumeratedEnumeratedBinding()
                             .source((EnumeratedType) sourceType)
                             .target((EnumeratedType) targetType)
                             .accept(builder -> addValuesBindings(builder,
                                                                  (EnumeratedType) sourceType,
                                                                  (EnumeratedType) targetType))
                             .build();
        }
    }

    private void addValuesBindings(EnumeratedEnumeratedBindingImpl.Builder builder,
                                   EnumeratedType sourceType,
                                   EnumeratedType targetType) {
        final Map<EnumeratedValueImpl, EnumeratedValueImpl> map = mapValues.get(sourceType);
        final List<EnumeratedValue> sourceValues = new ArrayList<>();
        sourceValues.addAll(map.keySet());
        Collections.sort(sourceValues, EnumeratedValue.ORDINAL_LITERAL_COMPARATOR);
        for (final EnumeratedValue sourceValue : sourceValues) {
            final EnumeratedValue targetValue = map.get(sourceValue);
            builder.map(sourceValue.getLiteral(), targetValue.getLiteral());
        }
    }

    private void createPropertiesBinding(Property sourceProperty) {
        final Property targetProperty = getTargetProperty(sourceProperty);
        final TypesBinding typeBinding =
                registriesBinding.getTypesBinding(sourceProperty.getType(),
                                                  targetProperty.getType());
        registriesBinding.propertyPropertyBinding()
                         .source(sourceProperty)
                         .target(targetProperty)
                         .typeBinding(typeBinding)
                         .build();
    }

    private void createAliasesBinding(Alias sourceAlias) {
        final Alias targetAlias = getTargetAlias(sourceAlias);
        registriesBinding.aliasAliasBinding()
                         .source(sourceAlias)
                         .target(targetAlias)
                         .build();
    }

    private void fixTargetAliases() {
        // Collect source aliases
        final List<Alias> sourceAliases = new ArrayList<>();
        IterableUtils.addAll(sourceRegistry.getDeclaredAliases(), sourceAliases);

        // Fix target aliases
        for (final Alias sourceAlias : sourceAliases) {
            final AliasImpl targetAlias = getTargetAlias(sourceAlias);
            final Expression sourceExpression = sourceAlias.getExpression();
            final Expression targetExpression = registryConverter.forward(sourceExpression);
            targetAlias.setExpression(targetExpression);
        }
    }

    private void createTargetAssertions(AbstractDictionaryImpl sourceDictionary,
                                        AbstractDictionaryImpl targetDictionary) {
        for (final UserDefinedAssertion sourceAssertion : sourceDictionary.getAssertions(UserDefinedAssertion.class)) {
            final Expression sourceExpression = sourceAssertion.getExpression();
            final Expression targetExpression = registryConverter.forward(sourceExpression);
            targetDictionary.createAssertion(targetExpression);
        }
    }

    private void createTargetConstraints(AbstractDictionaryImpl sourceDictionary,
                                         AbstractDictionaryImpl targetDictionary) {
        for (final Constraint sourceConstraint : sourceDictionary.getConstraints()) {
            createTargetConstraint(sourceConstraint, targetDictionary);
        }
    }

    private void createTargetConstraint(Constraint constraint,
                                        AbstractDictionaryImpl targetDictionary) {
        if (constraint instanceof AbstractExpressionsConstraint) {
            final AbstractExpressionsConstraint<?> sourceConstraint = (AbstractExpressionsConstraint<?>) constraint;
            final List<Expression> targetExpressions = new ArrayList<>();
            for (final Expression sourceExpression : sourceConstraint.getExpressions()) {
                targetExpressions.add(registryConverter.forward(sourceExpression));
            }
            final AbstractExpressionsConstraint<?> targetConstraint = sourceConstraint.create(targetDictionary);
            targetConstraint.setExpressions(targetExpressions);
            targetDictionary.addConstraint(targetConstraint);
        } else if (constraint instanceof final PropertyRestrictionConstraint sourceConstraint) {
            final Set<SItemSetPropertyPair> pairs =
                    registryConverter.forward(sourceConstraint.getValues(),
                                              sourceConstraint.getProperty());
            for (final SItemSetPropertyPair pair : pairs) {
                final PropertyRestrictionConstraint targetConstraint = new PropertyRestrictionConstraint(targetDictionary);
                targetConstraint.setProperty(pair.property());
                targetConstraint.setValues(pair.set());
                targetDictionary.addConstraint(targetConstraint);
            }
            // } else if (constraint instanceof MatrixConstraint) {
            // final MatrixConstraint sourceConstraint = (MatrixConstraint) constraint;
            // final MatrixConstraint targetConstraint = new MatrixConstraint(targetDictionary,
            // sourceConstraint.getFeatures());
            // // Set context
            // final Expression sourceContext = sourceConstraint.getContext();
            // final Expression targetContext = registryConverter.forward(sourceContext);
            // targetConstraint.setContext(targetContext);
            //
            // // Set keys header
            // // Set column header
            // // Set cells
            // // TODO
        } else {
            LOGGER.warn("Conversion of {} constraints is not supported", constraint.getTypeName());
        }
    }

    private void createTargetPolicies(AbstractDictionaryImpl sourceParent,
                                      AbstractDictionaryImpl targetParent) {
        for (final PolicyImpl sourcePolicy : sourceParent.getChildren(PolicyImpl.class)) {
            final PolicyImpl targetPolicy = targetParent.policy().name(sourcePolicy.getName()).build();

            setAllowedItems(sourcePolicy, targetPolicy);
            setWritingRules(sourcePolicy, targetPolicy);
            createTargetAssertions(sourcePolicy, targetPolicy);
            createTargetConstraints(sourcePolicy, targetPolicy);

            // Recurse
            createTargetPolicies(sourcePolicy, targetPolicy);
        }
    }

    private void setAllowedItems(PolicyImpl sourcePolicy,
                                 PolicyImpl targetPolicy) {
        for (final DItemUsage usage : DItemUsage.values()) {
            if (usage != DItemUsage.FORBIDDEN) {
                for (final NamedDItem sourceItem : sourcePolicy.getAllowedItems(usage)) {
                    final NamedDItem targetItem = getTargetItem(sourceItem);
                    targetPolicy.setItemUsage(targetItem.getName(), usage);
                }
            }
        }
    }

    private static void setWritingRules(PolicyImpl sourcePolicy,
                                        PolicyImpl targetPolicy) {
        for (final String rule : sourcePolicy.getWritingRuleNames()) {
            targetPolicy.setWritingRuleEnabled(rule, true);
        }
    }

    public static void execute(MainArgs margs) throws IOException {
        final RegistryEncoder instance = new RegistryEncoder(margs);
        instance.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String INPUT_REPOSITORY = "input-repository";
        private static final String OUTPUT_REPOSITORY = "output-repository";
        private static final String SOURCE_REGISTRY_NAME = "source-registry";
        private static final String TARGET_REGISTRY_NAME = "target-registry";
        private static final String TARGET_REGISTRY_PREFIX = "target-registry-prefix";
        private static final String PRESERVE_TYPE = "preserve-type";
        private static final String PRESERVE_VALUE = "preserve-value";
        private static final String PRESERVE_ITEM = "preserve-item";

        protected MainSupport() {
            super(RegistryEncoder.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(INPUT_REPOSITORY)
                                    .desc("Name of the file containing the repository to load.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(OUTPUT_REPOSITORY)
                                    .desc("Name of the file containing the resulting repository.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(SOURCE_REGISTRY_NAME)
                                    .desc("Name of the source registry to encode.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(TARGET_REGISTRY_NAME)
                                    .desc("Name of the target encoded registry.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(TARGET_REGISTRY_PREFIX)
                                    .desc("Prefix of the target encoded registry.")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(PRESERVE_TYPE)
                                    .desc("Name(s) of type(s) that must be preserved.")
                                    .hasArgs()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(PRESERVE_VALUE)
                                    .desc("Name(s) of type(s) whose values must be preserved.")
                                    .hasArgs()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(PRESERVE_ITEM)
                                    .desc("Name(s) of item(s) that must be preserved.")
                                    .hasArgs()
                                    .build());

            AbstractMainSupport.addNoArgOptions(options, MainArgs.Feature.class);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            margs.inputRepositoryFile = getValueAsFile(cl, INPUT_REPOSITORY, IS_FILE);
            margs.outputRepositoryFile = getValueAsFile(cl, OUTPUT_REPOSITORY);
            margs.sourceRegistryName = getValueAsString(cl, SOURCE_REGISTRY_NAME, null);
            margs.targetRegistryName = getValueAsString(cl, TARGET_REGISTRY_NAME, null);
            margs.targetRegistryPrefix = getValueAsString(cl, TARGET_REGISTRY_PREFIX, null);
            fillValues(cl, PRESERVE_TYPE, margs.preservedTypes);
            fillValues(cl, PRESERVE_VALUE, margs.preservedValues);
            fillValues(cl, PRESERVE_ITEM, margs.preservedItems, Name::of);
            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);
            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws Exception {
            RegistryEncoder.execute(margs);
            return null;
        }
    }
}