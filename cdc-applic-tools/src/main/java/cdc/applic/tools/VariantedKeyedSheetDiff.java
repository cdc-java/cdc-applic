package cdc.applic.tools;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.io.RepositoryIo;
import cdc.applic.simplification.SimplifierFeatures;
import cdc.applic.tools.VariantedKeyedSheetDiff.MainArgs.Feature;
import cdc.office.ss.SheetLoader;
import cdc.office.ss.SheetParserFactory;
import cdc.office.ss.WorkbookKind;
import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.office.tables.Header;
import cdc.office.tables.HeaderCell;
import cdc.office.tables.HeaderMapper;
import cdc.office.tables.Row;
import cdc.office.tables.Rows;
import cdc.office.tables.TableSection;
import cdc.office.tables.diff.KeyedTableDiff;
import cdc.office.tables.diff.Side;
import cdc.office.tools.KeyedSheetDiff;
import cdc.office.tools.KeyedTableDiffExporter;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.lang.FailureReaction;
import cdc.util.lang.InvalidDataException;
import cdc.util.time.Chronometer;

public final class VariantedKeyedSheetDiff {
    static final Logger LOGGER = LogManager.getLogger(VariantedKeyedSheetDiff.class);
    static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.INFO).buildPrintStream();

    private static final String HEADER1 = "   Header 1: ";
    private static final String HEADER2 = "   Header 1: ";

    final MainArgs margs;

    private VariantedKeyedSheetDiff(MainArgs margs) {
        this.margs = margs;
    }

    private void info(String message) {
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info(message);
        }
    }

    public static class MainArgs {
        public static final String DEFAULT_ADDED_MARK = "<A>";
        public static final String DEFAULT_REMOVED_MARK = "<R>";
        public static final String DEFAULT_CHANGED_MARK = "<C>";
        public static final String DEFAULT_UNCHANGED_MARK = "";
        public static final String DEFAULT_FILE1_MARK = "1";
        public static final String DEFAULT_FILE2_MARK = "2";
        public static final String DEFAULT_DIFF_MARK = "Diff";
        public static final String DEFAULT_DELTA_SHEET_NAME = "Delta";

        public enum Feature implements OptionEnum {
            UNCHANGED_LINES(KeyedSheetDiff.MainArgs.Feature.UNCHANGED_LINES),
            NO_UNCHANGED_LINES(KeyedSheetDiff.MainArgs.Feature.NO_UNCHANGED_LINES),
            ADDED_OR_REMOVED_MARKS(KeyedSheetDiff.MainArgs.Feature.ADDED_OR_REMOVED_MARKS),
            NO_ADDED_OR_REMOVED_MARKS(KeyedSheetDiff.MainArgs.Feature.NO_ADDED_OR_REMOVED_MARKS),
            COLORS(KeyedSheetDiff.MainArgs.Feature.COLORS),
            NO_COLORS(KeyedSheetDiff.MainArgs.Feature.NO_COLORS),
            MARKS(KeyedSheetDiff.MainArgs.Feature.MARKS),
            NO_MARKS(KeyedSheetDiff.MainArgs.Feature.NO_MARKS),
            LINE_DIFF_COLUMN(KeyedSheetDiff.MainArgs.Feature.LINE_DIFF_COLUMN),
            NO_LINE_DIFF_COLUMN(KeyedSheetDiff.MainArgs.Feature.NO_LINE_DIFF_COLUMN),
            CELL_DIFF_COLUMNS(KeyedSheetDiff.MainArgs.Feature.CELL_DIFF_COLUMNS),
            NO_CELL_DIFF_COLUMNS(KeyedSheetDiff.MainArgs.Feature.NO_CELL_DIFF_COLUMNS),
            VULNERABILITY_PROTECTIONS(KeyedSheetDiff.MainArgs.Feature.VULNERABILITY_PROTECTIONS),
            NO_VULNERABILITY_PROTECTIONS(KeyedSheetDiff.MainArgs.Feature.NO_VULNERABILITY_PROTECTIONS),
            SORT_LINES("sort-lines", "Sort lines using keys and applic columns. Order of key columns declaration matters."),
            AUTO_SIZE_COLUMNS(KeyedSheetDiff.MainArgs.Feature.AUTO_SIZE_COLUMNS),
            AUTO_SIZE_ROWS(KeyedSheetDiff.MainArgs.Feature.AUTO_SIZE_ROWS),
            SHOW_CHANGE_DETAILS(KeyedSheetDiff.MainArgs.Feature.SHOW_CHANGE_DETAILS),
            SHOW_ORIGINAL_NAMES(KeyedSheetDiff.MainArgs.Feature.SHOW_ORIGINAL_NAMES),
            DUMP_PARTITIONS("dump-partitions", "Dump generated partitions that are then compared."),
            SPLIT_COMPARISONS(KeyedSheetDiff.MainArgs.Feature.SPLIT_COMPARISONS),
            SYNTHESIS(KeyedSheetDiff.MainArgs.Feature.SYNTHESIS),
            SAVE_SYNTHESIS(KeyedSheetDiff.MainArgs.Feature.SAVE_SYNTHESIS),
            VERBOSE(KeyedSheetDiff.MainArgs.Feature.VERBOSE);

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            private Feature(OptionEnum model) {
                this.name = model.getName();
                this.description = model.getDescription();
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }

        /** First file. */
        public File file1;
        /** First sheet. */
        public String sheet1;
        /** First map. */
        public final Map<String, String> map1 = new HashMap<>();
        /** Second file. */
        public File file2;
        /** Second sheet. */
        public String sheet2;
        /** Second map. */
        public final Map<String, String> map2 = new HashMap<>();
        /** Output file. */
        public File output;
        /** Output sheet. */
        public String sheet = DEFAULT_DELTA_SHEET_NAME;
        /** Key names (after mapping of names). */
        public final List<String> keys = new ArrayList<>();
        /** Names of attributes to keep (after mapping of names). */
        public final Set<String> attributes = new HashSet<>();
        /** charset. */
        public Charset charset;
        /** separator. */
        public char separator = ';';
        public String addedMark = DEFAULT_ADDED_MARK;
        public String removedMark = DEFAULT_REMOVED_MARK;
        public String changedMark = DEFAULT_CHANGED_MARK;
        public String unchangedMark = DEFAULT_UNCHANGED_MARK;
        public String diffMark = DEFAULT_DIFF_MARK;
        public String file1Mark = DEFAULT_FILE1_MARK;
        public String file2Mark = DEFAULT_FILE2_MARK;
        public String applicColumn = null;
        public File repository;
        public String dictionary;

        public final FeatureMask<Feature> features = new FeatureMask<>();

        public final void setEnabled(Feature feature,
                                     boolean enabled) {
            features.setEnabled(feature, enabled);
        }

        public final boolean isEnabled(Feature feature) {
            return features.isEnabled(feature);
        }
    }

    void execute() throws IOException {
        final Chronometer chrono = new Chronometer();

        // Load the applic repository
        info("Load repository " + margs.repository);
        final RepositoryImpl repository = RepositoryIo.load(margs.repository,
                                                            FailureReaction.FAIL);

        // Retrieve the applic dictionary
        info("Retrieve dictionary " + margs.dictionary);
        final Dictionary dictionary = repository.getDictionary(margs.dictionary);
        if (dictionary == null) {
            throw new InvalidDataException("Invalid dictionary: " + margs.dictionary);
        }

        final DictionaryHandle handle = new DictionaryHandle(dictionary);

        // TODO create simplifier from margs
        final SimplifierFeatures simplifierFeatures =
                SimplifierFeatures.ALL_HINTS_INCLUDE_ASSERTIONS_USER_DEFINED_RESERVES;

        // Load input files as rows
        final SheetLoader loader = new SheetLoader();
        loader.getFactory().setCharset(margs.charset);
        loader.getFactory().setSeparator(margs.separator);
        loader.getFactory()
              .setEnabled(SheetParserFactory.Feature.DISABLE_VULNERABILITY_PROTECTIONS,
                          margs.isEnabled(MainArgs.Feature.NO_VULNERABILITY_PROTECTIONS));

        chrono.start();
        info("Load sheet 1 " + margs.file1 + ":" + margs.sheet1);
        final List<Row> r1 =
                margs.sheet1 == null
                        ? loader.load(margs.file1, null, 0)
                        : loader.load(margs.file1, null, margs.sheet1);
        chrono.suspend();
        info("Done (" + r1.size() + " rows) " + chrono);

        chrono.start();
        info("Load sheet 2 " + margs.file2 + ":" + margs.sheet2);
        final List<Row> r2 =
                margs.sheet2 == null
                        ? loader.load(margs.file2, null, 0)
                        : loader.load(margs.file2, null, margs.sheet2);
        chrono.suspend();
        info("Done (" + r2.size() + " rows) " + chrono);

        if (r1.isEmpty()) {
            throw new IllegalArgumentException("No data in file1 sheet.");
        } else if (r2.isEmpty()) {
            throw new IllegalArgumentException("No data in file2 sheet.");
        }

        if (r1.get(0).hasNullValues()) {
            throw new IllegalArgumentException("Header of file1 sheet contains empty intermediate cell(s): " + r1.get(0));
        } else if (r2.get(0).hasNullValues()) {
            throw new IllegalArgumentException("Header of file2 sheet contains empty intermediate cell(s): " + r2.get(0));
        }

        info(HEADER1 + Header.builder().names(r1.get(0)).build());
        info(HEADER2 + Header.builder().names(r2.get(0)).build());

        // Rename headers
        if (!margs.map1.isEmpty()) {
            info("Rename Header 1 using: " + margs.map1);
            Rows.renameHeader(r1, margs.map1);
            info(HEADER1 + Header.builder().names(r1.get(0)).build());
        }
        if (!margs.map2.isEmpty()) {
            info("Rename Header 2 using: " + margs.map2);
            Rows.renameHeader(r2, margs.map2);
            info(HEADER2 + Header.builder().names(r2.get(0)).build());
        }

        // Remove unwanted columns
        final List<Row> rows1;
        final List<Row> rows2;
        if (margs.attributes.isEmpty()) {
            rows1 = r1;
            rows2 = r2;
        } else {
            // Keep only columns that are keys or listed in data
            info("Filter columns");
            chrono.start();
            final Set<String> keep = new HashSet<>();
            keep.addAll(margs.keys);
            keep.addAll(margs.attributes);
            keep.add(margs.applicColumn);
            final Header h1 = Header.builder().names(r1.get(0)).build();
            final Header h2 = Header.builder().names(r2.get(0)).build();
            rows1 = Rows.filter(r1, h1, keep);
            rows2 = Rows.filter(r2, h2, keep);
            chrono.suspend();
            info("Done " + chrono);
            info(HEADER1 + Header.builder().names(rows1.get(0)).build());
            info(HEADER2 + Header.builder().names(rows2.get(0)).build());
        }

        // Here only desired columns are present

        // Retrieve headers of both files
        final Header header1 = Header.builder().names(rows1.get(0)).build();
        final Header header2 = Header.builder().names(rows2.get(0)).build();

        info("Header 1 " + header1);
        info("Header 2 " + header2);

        final List<String> keysApplic = new ArrayList<>(margs.keys);
        keysApplic.add(margs.applicColumn);

        // Check that both headers contain the expected keys, including applic
        final Header expected = Header.builder()
                                      .names(keysApplic)
                                      .build();
        info("Expected header " + expected);

        final HeaderMapper mapper1 = HeaderMapper.builder()
                                                 .mandatory(expected)
                                                 .actual(header1)
                                                 .build();

        final HeaderMapper mapper2 = HeaderMapper.builder()
                                                 .mandatory(expected)
                                                 .actual(header2)
                                                 .build();

        if (!mapper1.hasAllMandatoryCells()) {
            throw new IllegalArgumentException("Missing keys: "
                    + mapper1.getMissingMandatoryCells().stream().map(HeaderCell::toString).sorted()
                             .collect(Collectors.joining(",", "[", "]"))
                    + " in file1 header: " + header1);
        }

        if (!mapper2.hasAllMandatoryCells()) {
            throw new IllegalArgumentException("Missing keys: "
                    + mapper2.getMissingMandatoryCells().stream().map(HeaderCell::toString).sorted()
                             .collect(Collectors.joining(",", "[", "]"))
                    + " in file2 header: " + header2);
        }

        // Remove header in both input rows
        rows1.remove(0);
        rows2.remove(0);

        // Partition input data
        chrono.start();
        info("Partition data");
        final KeyedTablePartitioner partitioner =
                new KeyedTablePartitioner(handle,
                                          simplifierFeatures,
                                          header1,
                                          rows1,
                                          header2,
                                          rows2,
                                          margs.applicColumn,
                                          margs.keys.toArray(new String[margs.keys.size()]));
        chrono.suspend();
        info("Done " + chrono);

        if (margs.features.isEnabled(MainArgs.Feature.DUMP_PARTITIONS)) {
            final File dir = margs.output.getParentFile();
            dump(new File(dir, "left.xlsx"), "Data", header1, partitioner.getLeftRows());
            dump(new File(dir, "right.xlsx"), "Data", header2, partitioner.getRightRows());
        }

        // Compare prepared data
        chrono.start();
        info("Compare rows");
        final KeyedTableDiff diff = KeyedTableDiff.builder()
                                                  .leftSystemId(margs.file1.getName()
                                                          + (margs.sheet1 == null ? "" : ":" + margs.sheet1))
                                                  .leftHeader(header1)
                                                  .leftRows(partitioner.getLeftRows())
                                                  .rightSystemId(margs.file2.getName()
                                                          + (margs.sheet2 == null ? "" : ":" + margs.sheet2))
                                                  .rightHeader(header2)
                                                  .rightRows(partitioner.getRightRows())
                                                  .keyNames(keysApplic)
                                                  .build();
        chrono.suspend();
        info("Done" + (diff.getNumberOfIgnoredRows() == 0
                ? ""
                : " (" + diff.getNumberOfIgnoredRows(Side.LEFT) + "/" + rows1.size() + " "
                        + diff.getNumberOfIgnoredRows(Side.RIGHT) + "/" + rows2.size() + " ignored)")
                + " " + chrono);

        if (margs.isEnabled(MainArgs.Feature.SYNTHESIS)) {
            diff.getSynthesis().print(OUT);
        }

        final WorkbookKind outputKind = WorkbookKind.from(margs.output);
        final boolean supportsColors =
                outputKind == WorkbookKind.XLS
                        || outputKind == WorkbookKind.XLSM
                        || outputKind == WorkbookKind.XLSX;
        final boolean showColors;
        if (margs.features.isEnabled(MainArgs.Feature.COLORS)) {
            showColors = true;
        } else if (margs.features.isEnabled(MainArgs.Feature.NO_COLORS)) {
            showColors = false;
        } else {
            showColors = supportsColors;
        }

        final boolean showMarks;
        if (margs.features.isEnabled(MainArgs.Feature.MARKS)) {
            showMarks = true;
        } else if (margs.features.isEnabled(MainArgs.Feature.NO_MARKS)) {
            showMarks = false;
        } else {
            showMarks = !supportsColors;
        }

        // Export result
        final KeyedTableDiffExporter exporter =
                KeyedTableDiffExporter.builder()
                                      .addedMark(margs.isEnabled(Feature.NO_ADDED_OR_REMOVED_MARKS) ? "" : margs.addedMark)
                                      .changedMark(margs.changedMark)
                                      .unchangedMark(margs.unchangedMark)
                                      .removedMark(margs.isEnabled(Feature.NO_ADDED_OR_REMOVED_MARKS) ? "" : margs.removedMark)
                                      .diffMark(margs.diffMark)
                                      .file1Mark(margs.file1Mark)
                                      .file2Mark(margs.file2Mark)
                                      .header1(header1)
                                      .header2(header2)
                                      .map1(margs.map1)
                                      .map2(margs.map2)
                                      .hint(KeyedTableDiffExporter.Hint.ADD_LINE_DIFF_COLUMN,
                                            !margs.isEnabled(Feature.NO_LINE_DIFF_COLUMN))
                                      .hint(KeyedTableDiffExporter.Hint.ADD_CELL_DIFF_COLUMNS,
                                            margs.isEnabled(Feature.CELL_DIFF_COLUMNS))
                                      .hint(KeyedTableDiffExporter.Hint.SHOW_COLORS, showColors)
                                      .hint(KeyedTableDiffExporter.Hint.SHOW_MARKS, showMarks)
                                      .hint(KeyedTableDiffExporter.Hint.SHOW_CHANGE_DETAILS,
                                            margs.isEnabled(Feature.SHOW_CHANGE_DETAILS))
                                      .hint(KeyedTableDiffExporter.Hint.SHOW_UNCHANGED_LINES,
                                            !margs.isEnabled(Feature.NO_UNCHANGED_LINES))
                                      .hint(KeyedTableDiffExporter.Hint.SHOW_ORIGINAL_NAMES,
                                            margs.isEnabled(Feature.SHOW_ORIGINAL_NAMES))
                                      .hint(KeyedTableDiffExporter.Hint.SORT_LINES,
                                            margs.isEnabled(Feature.SORT_LINES))
                                      .diffSheetName(margs.sheet)
                                      .hint(KeyedTableDiffExporter.Hint.SAVE_SYNTHESIS,
                                            margs.isEnabled(Feature.SAVE_SYNTHESIS))
                                      .hint(KeyedTableDiffExporter.Hint.SPLIT_COMPARISONS,
                                            margs.isEnabled(Feature.SPLIT_COMPARISONS))
                                      .features(WorkbookWriterFeatures.builder()
                                                                      .separator(margs.separator)
                                                                      .charset(margs.charset)
                                                                      .maxLineLength(-1)
                                                                      .enable(WorkbookWriterFeatures.Feature.AUTO_FILTER_COLUMNS)
                                                                      .setEnabled(WorkbookWriterFeatures.Feature.AUTO_SIZE_COLUMNS,
                                                                                  margs.features.contains(Feature.AUTO_SIZE_COLUMNS))
                                                                      .setEnabled(WorkbookWriterFeatures.Feature.AUTO_SIZE_ROWS,
                                                                                  margs.features.contains(Feature.AUTO_SIZE_ROWS))
                                                                      .setEnabled(WorkbookWriterFeatures.Feature.RICH_TEXT,
                                                                                  !margs.features.contains(Feature.NO_COLORS))
                                                                      .build())
                                      .build();

        chrono.start();
        info("Generate " + margs.output);
        exporter.save(diff, margs.output);
        chrono.suspend();
        info("Done " + chrono);
    }

    private void dump(File file,
                      String sheetName,
                      Header header,
                      List<Row> rows) throws IOException {
        info("Generate " + file);
        final WorkbookWriterFactory factory = new WorkbookWriterFactory();
        try (final WorkbookWriter<?> writer = factory.create(file)) {
            writer.beginSheet(sheetName);
            writer.addRow(TableSection.HEADER, header.getSortedNames());
            for (final Row row : rows) {
                writer.addRow(TableSection.DATA, row);
            }
            writer.flush();
        }
    }

    public static void execute(MainArgs margs) throws IOException {
        final VariantedKeyedSheetDiff instance = new VariantedKeyedSheetDiff(margs);
        instance.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String KEY_VALUE_SEPARATOR = "::";
        private static final String FILE1 = "file1";
        private static final String FILE2 = "file2";
        private static final String MAP1 = "map1";
        private static final String MAP2 = "map2";
        private static final String SHEET1 = "sheet1";
        private static final String SHEET2 = "sheet2";
        private static final String SHEET = "sheet";
        private static final String KEY = "key";
        private static final String ATTRIBUTE = "attribute";
        private static final String CHARSET = "charset";
        private static final String SEPARATOR = "separator";
        private static final String ADDED_MARK = "added-mark";
        private static final String REMOVED_MARK = "removed-mark";
        private static final String CHANGED_MARK = "changed-mark";
        private static final String UNCHANGED_MARK = "unchanged-mark";
        private static final String DIFF_MARK = "diff-mark";
        private static final String FILE1_MARK = "file1-mark";
        private static final String FILE2_MARK = "file2-mark";
        private static final String APPLIC_COLUMN = "applic-column";
        private static final String APPLIC_REPOSITORY = "applic-repository";
        private static final String APPLIC_DICTIONARY = "applic-dictionary";

        public MainSupport() {
            super(VariantedKeyedSheetDiff.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected String getHelpHeader() {
            return VariantedKeyedSheetDiff.class.getSimpleName()
                    + " is used to compare two sheets (csv, xls, xlsx or ods).\n"
                    + "Lines in sheets are matched by a set of key columns and an applic column.\n"
                    + "Input and output files can use different formats.\n"
                    + "Differences are indicated with textual marks or colors (if output format supports it).\n"
                    + "Several lines with the same keys can exist as long as their applicabilities are disjoint. "
                    + "If this is not the case, this tool will not work.";
        }

        @Override
        protected String getHelpFooter() {
            return "\nKNOWN LIMITATIONS\n"
                    + "All csv files (input and output) must use the same charset and separator.\n"
                    + "When mixing input file formats with CSV, if a key column contains numbers, comparison will fail.\n"
                    + "Ods handling is experimental. Ods output does not support coloring.";
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(FILE1)
                                    .desc("Mandatory name of the first input file.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(FILE2)
                                    .desc("Mandatory name of the second input file.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(SHEET1)
                                    .desc("Optional name of the sheet in the first input file. If omitted, the first sheet is loaded.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(SHEET2)
                                    .desc("Optional name of the sheet in the second input file. If omitted, the first sheet is loaded.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(SHEET)
                                    .desc("Optional name of the delta sheet in the output file. (default: \""
                                            + MainArgs.DEFAULT_DELTA_SHEET_NAME + "\").")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(MAP1)
                                    .desc("Optional mapping of column names (keys and attributes) of first file."
                                            + "\nEach mapping has the form <old name>::<new name>.")
                                    .hasArgs()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(MAP2)
                                    .desc("Optional mapping of column names (keys and attributes) of second file."
                                            + "\nEach mapping has the form <old name>::<new name>.")
                                    .hasArgs()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(OUTPUT)
                                    .desc("Mandatory name of the output file.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(KEY)
                                    .desc("Mandatory name(s) of key column(s)."
                                            + "\nIf mapping is used, use new name(s).")
                                    .hasArgs()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(ATTRIBUTE)
                                    .desc("Optional name(s) of attribute column(s) to compare."
                                            + "\nIf omitted, all attribute columns are compared."
                                            + "\nIf mapping is used, use new name(s).")
                                    .hasArgs()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(CHARSET)
                                    .desc("Optional name of the charset for csv files (default: platform default charset).")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(SEPARATOR)
                                    .desc("Optional char separator for csv files (default: ';').")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(ADDED_MARK)
                                    .desc("Optional mark for added cells (default: \"" + MainArgs.DEFAULT_ADDED_MARK + "\").")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(REMOVED_MARK)
                                    .desc("Optional mark for removed cells (default: \"" + MainArgs.DEFAULT_REMOVED_MARK + "\").")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(CHANGED_MARK)
                                    .desc("Optional mark for changed cells (default: \"" + MainArgs.DEFAULT_CHANGED_MARK + "\").")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(UNCHANGED_MARK)
                                    .desc("Optional mark for unchanged cells (default: \"" + MainArgs.DEFAULT_UNCHANGED_MARK
                                            + "\").")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(DIFF_MARK)
                                    .desc("Optional mark for diff columns (default: \"" + MainArgs.DEFAULT_DIFF_MARK + "\").")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(FILE1_MARK)
                                    .desc("Optional mark for file1 columns (default: \"" + MainArgs.DEFAULT_FILE1_MARK + "\")."
                                            + "\nRelated to " + MainArgs.Feature.SHOW_ORIGINAL_NAMES.getName() + "option.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(FILE2_MARK)
                                    .desc("Optional mark for file2 columns (default: \"" + MainArgs.DEFAULT_FILE2_MARK + "\")."
                                            + "\nRelated to " + MainArgs.Feature.SHOW_ORIGINAL_NAMES.getName() + "option.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(APPLIC_COLUMN)
                                    .desc("Mandatory name of the applic column.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(APPLIC_REPOSITORY)
                                    .desc("Mandatory path to the applic repository.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(APPLIC_DICTIONARY)
                                    .desc("Mandatory path of the applic dictionary to use.")
                                    .hasArg()
                                    .required()
                                    .build());

            AbstractMainSupport.addNoArgOptions(options, MainArgs.Feature.class);
            AbstractMainSupport.createGroup(options,
                                            MainArgs.Feature.ADDED_OR_REMOVED_MARKS,
                                            MainArgs.Feature.NO_ADDED_OR_REMOVED_MARKS);
            AbstractMainSupport.createGroup(options,
                                            MainArgs.Feature.CELL_DIFF_COLUMNS,
                                            MainArgs.Feature.NO_CELL_DIFF_COLUMNS);
            AbstractMainSupport.createGroup(options,
                                            MainArgs.Feature.COLORS,
                                            MainArgs.Feature.NO_COLORS);
            AbstractMainSupport.createGroup(options,
                                            MainArgs.Feature.LINE_DIFF_COLUMN,
                                            MainArgs.Feature.NO_LINE_DIFF_COLUMN);
            AbstractMainSupport.createGroup(options,
                                            MainArgs.Feature.MARKS,
                                            MainArgs.Feature.NO_MARKS);
            AbstractMainSupport.createGroup(options,
                                            MainArgs.Feature.UNCHANGED_LINES,
                                            MainArgs.Feature.NO_UNCHANGED_LINES);
            AbstractMainSupport.createGroup(options,
                                            MainArgs.Feature.VULNERABILITY_PROTECTIONS,
                                            MainArgs.Feature.NO_VULNERABILITY_PROTECTIONS);
        }

        private static String getKey(String s) {
            return getPart(s, KEY_VALUE_SEPARATOR, 0);
        }

        private static String getValue(String s) {
            return getPart(s, KEY_VALUE_SEPARATOR, 1);
        }

        private static void analyzeMap(String s,
                                       Map<String, String> map) {
            final String key = getKey(s);
            final String value = getValue(s);
            map.put(key, value);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();

            margs.file1 = getValueAsResolvedFile(cl, FILE1, IS_FILE);
            margs.sheet1 = getValueAsString(cl, SHEET1, null);
            margs.file2 = getValueAsResolvedFile(cl, FILE2, IS_FILE);
            margs.sheet2 = getValueAsString(cl, SHEET2, null);
            margs.output = getValueAsResolvedFile(cl, OUTPUT);
            margs.sheet = getValueAsString(cl, SHEET, MainArgs.DEFAULT_DELTA_SHEET_NAME);
            margs.charset = getValueAsCharset(cl, CHARSET);
            margs.separator = getValueAsChar(cl, SEPARATOR, ';');
            margs.addedMark = getValueAsString(cl, ADDED_MARK, MainArgs.DEFAULT_ADDED_MARK);
            margs.removedMark = getValueAsString(cl, REMOVED_MARK, MainArgs.DEFAULT_REMOVED_MARK);
            margs.changedMark = getValueAsString(cl, CHANGED_MARK, MainArgs.DEFAULT_CHANGED_MARK);
            margs.unchangedMark = getValueAsString(cl, UNCHANGED_MARK, MainArgs.DEFAULT_UNCHANGED_MARK);
            margs.diffMark = getValueAsString(cl, DIFF_MARK, MainArgs.DEFAULT_DIFF_MARK);
            margs.file1Mark = getValueAsString(cl, FILE1_MARK, MainArgs.DEFAULT_FILE1_MARK);
            margs.file2Mark = getValueAsString(cl, FILE2_MARK, MainArgs.DEFAULT_FILE2_MARK);
            margs.repository = getValueAsResolvedFile(cl, APPLIC_REPOSITORY, IS_FILE);
            margs.dictionary = getValueAsString(cl, APPLIC_DICTIONARY, null);
            margs.applicColumn = getValueAsString(cl, APPLIC_COLUMN, null);

            for (final String s : cl.getOptionValues(KEY)) {
                margs.keys.add(s);
            }
            if (cl.hasOption(ATTRIBUTE)) {
                for (final String s : cl.getOptionValues(ATTRIBUTE)) {
                    margs.attributes.add(s);
                }
            }
            if (cl.hasOption(MAP1)) {
                for (final String s : cl.getOptionValues(MAP1)) {
                    analyzeMap(s, margs.map1);
                }
            }
            if (cl.hasOption(MAP2)) {
                for (final String s : cl.getOptionValues(MAP2)) {
                    analyzeMap(s, margs.map2);
                }
            }
            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);
            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws Exception {
            VariantedKeyedSheetDiff.execute(margs);
            return null;
        }
    }
}