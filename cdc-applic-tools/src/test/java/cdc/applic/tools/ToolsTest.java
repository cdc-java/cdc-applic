package cdc.applic.tools;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

import cdc.util.cli.MainResult;

class ToolsTest {
    @Test
    void testApplicConsistencyChecker() {
        assertSame(MainResult.SUCCESS,
                   ApplicConsistencyChecker.exec("--args-file", "src/test/resources/applic-consistency-checker-args.txt"));
    }

    @Test
    void testS1000DApplicToRepository() {
        assertSame(MainResult.SUCCESS,
                   S1000DApplicToRepository.exec("--args-file", "src/test/resources/s1000d-applic-to-repository-args.txt"));
    }

    @Test
    void testExtractS1000DDmApplicFromRepository() {
        assertSame(MainResult.SUCCESS,
                   ExtractS1000DDmApplic.exec("--args-file", "src/test/resources/extract-s1000d-dm-applic-repository-args.txt"));
    }

    @Test
    void testExtractS1000DDmApplicFromAct() {
        assertSame(MainResult.SUCCESS,
                   ExtractS1000DDmApplic.exec("--args-file", "src/test/resources/extract-s1000d-dm-applic-act-args.txt"));
    }
}