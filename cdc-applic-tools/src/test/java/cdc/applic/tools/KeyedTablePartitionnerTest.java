package cdc.applic.tools;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.simplification.SimplifierFeatures;
import cdc.office.tables.Header;
import cdc.office.tables.Row;
import cdc.util.lang.CollectionUtils;

class KeyedTablePartitionnerTest {
    private static final Logger LOGGER = LogManager.getLogger(KeyedTablePartitionnerTest.class);

    private static final String APPLIC = "A";
    private static final String KEY1 = "I";
    private static final String[] KEYS = { KEY1 };

    private static void print(String title,
                              List<Row> rows) {
        LOGGER.debug(title);
        for (final Row row : CollectionUtils.toSortedList(rows, Row.LEXICOGRAPHIC_COMPARATOR)) {
            LOGGER.debug("   {}", row);
        }
    }

    private static Row r(String key,
                         String applic,
                         String value) {
        return Row.builder().addValues(key, applic, value).build();
    }

    @SafeVarargs
    private static <T> List<T> list(T... values) {
        return CollectionUtils.toList(values);
    }

    private static void check(List<Row> leftData,
                              List<Row> rightData,
                              List<Row> leftExpected,
                              List<Row> rightExpected) {
        LOGGER.debug("========================");
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        registry.enumeratedType().name("Version").frozen(true).literals("V1", "V2", "V3", "V4").build();
        registry.property().name("Version").type("Version").ordinal(0).build();

        final DictionaryHandle handle = new DictionaryHandle(registry);
        final SimplifierFeatures simplifierFeatures = SimplifierFeatures.ALL_HINTS_INCLUDE_ASSERTIONS_USER_DEFINED_RESERVES;
        final Header leftHeader = Header.builder().names(KEY1, APPLIC, "V").build();
        final Header rightHeader = Header.builder().names(KEY1, APPLIC, "V").build();

        print("Left In", leftData);
        print("Right In", rightData);

        final KeyedTablePartitioner partitioner = new KeyedTablePartitioner(handle,
                                                                            simplifierFeatures,
                                                                            leftHeader,
                                                                            leftData,
                                                                            rightHeader,
                                                                            rightData,
                                                                            APPLIC,
                                                                            KEYS);

        print("Left Out", partitioner.getLeftRows());
        print("Right Out", partitioner.getRightRows());
        final List<Row> leftResult = CollectionUtils.toSortedList(partitioner.getLeftRows(), Row.LEXICOGRAPHIC_COMPARATOR);
        final List<Row> rightResult = CollectionUtils.toSortedList(partitioner.getRightRows(), Row.LEXICOGRAPHIC_COMPARATOR);
        assertSame(leftExpected.size(), leftResult.size());
        assertSame(rightExpected.size(), rightResult.size());
        for (int index = 0; index < leftExpected.size(); index++) {
            assertEquals(leftExpected.get(index), leftResult.get(index));
        }

        for (int index = 0; index < rightExpected.size(); index++) {
            assertEquals(rightExpected.get(index), rightResult.get(index));
        }
    }

    @Test
    void test1() {
        check(list(r("I1", "Version = V1", "Value1"),
                   r("I1", "Version = V2", "Value2")),
              list(r("I1", "Version = V1", "Value1"),
                   r("I1", "Version = V2", "Value2")),
              list(r("I1", "Version=V1", "Value1"),
                   r("I1", "Version=V2", "Value2")),
              list(r("I1", "Version=V1", "Value1"),
                   r("I1", "Version=V2", "Value2")));
    }

    @Test
    void test2() {
        check(list(r("I1", "Version = V1", "Value1"),
                   r("I1", "Version = V2", "Value2")),
              list(r("I1", "Version <: {V1,V2}", "Value1")),
              list(r("I1", "Version=V1", "Value1"),
                   r("I1", "Version=V2", "Value2")),
              list(r("I1", "Version=V1", "Value1"),
                   r("I1", "Version=V2", "Value1")));
    }

    @Test
    void test3() {
        check(list(r("I1", "Version = V1", "Value1"),
                   r("I1", "Version = V2", "Value2")),
              list(r("I1", "Version <: {V1,V2,V3}", "Value1")),
              list(r("I1", "Version=V1", "Value1"),
                   r("I1", "Version=V2", "Value2")),
              list(r("I1", "Version=V1", "Value1"),
                   r("I1", "Version=V2", "Value1"),
                   r("I1", "Version=V3", "Value1")));
    }

    @Test
    void test4() {
        check(list(r("I1", "Version <: {V1,V2}", "Value1")),
              list(r("I1", "Version <: {V1,V2,V3}", "Value1")),
              list(r("I1", "Version<:{V1,V2}", "Value1")),
              list(r("I1", "Version<:{V1,V2}", "Value1"),
                   r("I1", "Version=V3", "Value1")));
    }
}