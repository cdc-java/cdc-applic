package cdc.applic.dictionaries.core.conversions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.bindings.BindingDirection;
import cdc.applic.dictionaries.bindings.DictionaryConverter;
import cdc.applic.dictionaries.core.visitors.MoveNotInwards;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.bindings.DictionariesBindingImpl;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.Node;

class DictionaryConverterImplTest {
    private static final Logger LOGGER = LogManager.getLogger(DictionaryConverterImplTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    private static Expression simplify(Expression expression) {
        return simplify(expression.getRootNode()).toExpression();
    }

    private static Node simplify(Node node) {
        return MoveNotInwards.execute(node, MoveNotInwards.Variant.USE_NEGATIVE_LEAVES);
    }

    private static void checkOne(String source,
                                 String target,
                                 DictionaryConverter converter) {
        final Expression sourceX = new Expression(source);
        final Expression targetX = new Expression(target);

        final Expression forwardTargetX =
                simplify(converter.convert(sourceX, BindingDirection.FORWARD));
        final Node forwardTargetN =
                simplify(converter.convert(sourceX.getRootNode(), BindingDirection.FORWARD));

        final Expression backwardSourceX =
                simplify(converter.convert(targetX, BindingDirection.BACKWARD));
        final Node backwardSourceN =
                simplify(converter.convert(targetX.getRootNode(), BindingDirection.BACKWARD));

        assertEquals(target, forwardTargetX.toInfix(Formatting.SHORT_NARROW));
        assertEquals(target, forwardTargetN.toInfix(Formatting.SHORT_NARROW));

        assertEquals(source, backwardSourceX.toInfix(Formatting.SHORT_NARROW));
        assertEquals(source, backwardSourceN.toInfix(Formatting.SHORT_NARROW));
    }

    private static void check(String source,
                              String target,
                              DictionaryConverter converter) {
        checkOne(source, target, converter);
        checkOne(target, source, converter.revert());
        checkOne(source, source, converter.andThen(converter.revert()));
        checkOne(target, target, converter.revert().andThen(converter));
    }

    @Test
    void testConversions() {
        final RepositoryImpl repository = new RepositoryImpl();
        // Source registry
        final RegistryImpl sourceRegistry = repository.registry().name("Source Registry").build();
        sourceRegistry.integerType().name("SourceRankType").frozen(true).domain("0~9999").build();
        sourceRegistry.booleanType().name("SourceBoolType").build();
        sourceRegistry.property().name("SourceRank").type("SourceRankType").build();
        sourceRegistry.property().name("SourceBool").type("SourceBoolType").build();
        sourceRegistry.property().name("SourceRBool").type("SourceBoolType").build();

        // Target registry
        final RegistryImpl targetRegistry = repository.registry().name("Target Registry").build();
        targetRegistry.integerType().name("TargetRankType").frozen(true).domain("0~999").build();
        targetRegistry.booleanType().name("TargetBoolType").build();
        targetRegistry.booleanType().name("TargetRBoolType").build();
        targetRegistry.property().name("TargetRank").type("TargetRankType").build();
        targetRegistry.property().name("TargetBool").type("TargetBoolType").build();
        targetRegistry.property().name("TargetRBool").type("TargetRBoolType").build();

        // Binding
        final DictionariesBindingImpl binding = repository.binding().source("/Source Registry").target("/Target Registry").build();
        binding.integerIntegerBinding()
               .source("SourceRankType")
               .target("TargetRankType")
               .build();
        binding.booleanBooleanBinding()
               .source("SourceBoolType")
               .target("TargetBoolType")
               .build();

        binding.booleanBooleanBinding()
               .source("SourceBoolType")
               .target("TargetRBoolType")
               .revert(true)
               .build();

        binding.propertyPropertyBinding().source("SourceRank").target("TargetRank").build();
        binding.propertyPropertyBinding().source("SourceBool").target("TargetBool").build();
        binding.propertyPropertyBinding().source("SourceRBool").target("TargetRBool").build();

        repository.print(OUT);

        final DictionaryConverterImpl converter = new DictionaryConverterImpl(binding);

        assertSame(binding, converter.getDictionariesBinding());

        check("true", "true", converter);
        check("SourceRank=10", "TargetRank=10", converter);
        check("SourceRank<:{10,12}", "TargetRank<:{10,12}", converter);
        check("SourceRank!<:{10,12}", "TargetRank!<:{10,12}", converter);
        check("SourceBool", "TargetBool", converter);
        check("SourceBool=true", "TargetBool=true", converter);
        check("SourceRBool=false", "TargetRBool=true", converter);
        check("SourceRBool=true", "TargetRBool=false", converter);
        check("SourceRBool!=false", "TargetRBool!=true", converter);
        check("SourceRBool!=true", "TargetRBool!=false", converter);
        check("!SourceRBool", "TargetRBool", converter);
    }

    @Test
    void testComposition() {
        final RepositoryImpl repository = new RepositoryImpl();
        repository.registry().name("Source Registry").build();
        repository.registry().name("Target Registry").build();
        final DictionariesBindingImpl binding = repository.binding().source("/Source Registry").target("/Target Registry").build();
        final DictionaryConverterImpl converter = new DictionaryConverterImpl(binding);

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         converter.andThen(converter);
                     });
    }
}