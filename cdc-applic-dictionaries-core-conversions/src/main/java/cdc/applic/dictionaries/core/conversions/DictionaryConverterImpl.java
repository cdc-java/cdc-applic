package cdc.applic.dictionaries.core.conversions;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cdc.applic.dictionaries.ConversionException;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.bindings.AliasAliasBinding;
import cdc.applic.dictionaries.bindings.BindingDirection;
import cdc.applic.dictionaries.bindings.BindingRole;
import cdc.applic.dictionaries.bindings.BooleanBooleanBinding;
import cdc.applic.dictionaries.bindings.DItemsBinding;
import cdc.applic.dictionaries.bindings.DictionariesBinding;
import cdc.applic.dictionaries.bindings.DictionaryConverter;
import cdc.applic.dictionaries.bindings.PropertyPropertyBinding;
import cdc.applic.dictionaries.bindings.SItemSetPropertyPair;
import cdc.applic.dictionaries.bindings.TypesBinding;
import cdc.applic.dictionaries.bindings.ValuePropertyPair;
import cdc.applic.dictionaries.checks.SemanticChecker;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractPropertyNode;
import cdc.applic.expressions.ast.AbstractSetNode;
import cdc.applic.expressions.ast.AbstractValueNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.UncheckedSet;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;
import cdc.util.lang.NotFoundException;

public class DictionaryConverterImpl implements DictionaryConverter {
    private final DictionariesBinding registriesBinding;
    private final Converter forwardConverter = new Converter(BindingDirection.FORWARD);
    private final Converter backwardConverter = new Converter(BindingDirection.BACKWARD);

    public DictionaryConverterImpl(DictionariesBinding binding) {
        Checks.isNotNull(binding, "binding");

        this.registriesBinding = binding;
    }

    /**
     * @return The used DictionariesBinding.
     */
    public DictionariesBinding getDictionariesBinding() {
        return registriesBinding;
    }

    @Override
    public Dictionary getDictionary(BindingRole role) {
        return registriesBinding.getDictionary(role);
    }

    @Override
    public Set<Property> forward(Property property) {
        return forwardConverter.convertProperty(property);
    }

    @Override
    public Alias forward(Alias alias) {
        return forwardConverter.convertAlias(alias);
    }

    @Override
    public ValuePropertyPair forward(Value value,
                                     Property property) {
        return forwardConverter.convertValue(value, property);
    }

    @Override
    public Set<SItemSetPropertyPair> forward(SItemSet set,
                                             Property property) {
        return forwardConverter.convertSet(set, property);
    }

    @Override
    public Node forward(Node node) {
        SemanticChecker.checkCompliance(getDictionary(BindingRole.SOURCE), node);

        return node.accept(forwardConverter);
    }

    @Override
    public Set<Property> backward(Property property) {
        return backwardConverter.convertProperty(property);
    }

    @Override
    public Alias backward(Alias alias) {
        return backwardConverter.convertAlias(alias);
    }

    @Override
    public ValuePropertyPair backward(Value value,
                                      Property property) {
        return backwardConverter.convertValue(value, property);
    }

    @Override
    public Set<SItemSetPropertyPair> backward(SItemSet set,
                                              Property property) {
        return backwardConverter.convertSet(set, property);
    }

    @Override
    public Node backward(Node node) {
        SemanticChecker.checkCompliance(getDictionary(BindingRole.TARGET), node);

        return node.accept(backwardConverter);
    }

    private class Converter extends AbstractConverter {
        /** Conversion direction. */
        private final BindingDirection direction;
        /** Conversion source role. */
        private final BindingRole srcRole;
        private final BindingRole tgtRole;

        public Converter(BindingDirection direction) {
            this.direction = direction;
            this.srcRole = direction.getSourceRole();
            this.tgtRole = this.srcRole.opposite();
        }

        public Set<Property> convertProperty(Property property) {
            final Set<Property> set = new HashSet<>();

            // Set of all bindings usable to convert property
            final Set<PropertyPropertyBinding> bindings =
                    registriesBinding.getPropertyPropertyBindings(property.getName(), srcRole);
            for (final PropertyPropertyBinding binding : bindings) {
                final Property tgtProperty = binding.getProperty(tgtRole);
                set.add(tgtProperty);
            }

            if (set.isEmpty()) {
                throw new ConversionException("Can not convert property '" + property.getName() + "'");
            } else {
                return set;
            }
        }

        public Alias convertAlias(Alias alias) {
            Checks.isNotNull(alias, "alias");

            try {
                // This will raise an exception of no matching bindings exist
                final AliasAliasBinding binding = registriesBinding.getAliasAliasBinding(alias.getName(), srcRole);
                return binding.getAlias(tgtRole);
            } catch (final NotFoundException e) {
                throw new ConversionException("Can not convert alias '" + alias.getName() + "'");
            }
        }

        public ValuePropertyPair convertValue(Value value,
                                              Property property) {
            // Set of all bindings usable to convert property
            final Set<PropertyPropertyBinding> bindings =
                    registriesBinding.getPropertyPropertyBindings(property.getName(), srcRole);

            // Iterate on available type bindings till one is found
            // The domain of the found binding must include the value to convert
            for (final PropertyPropertyBinding binding : bindings) {
                if (binding.getTypesBinding().getDomain(srcRole).contains(value)) {
                    final Property tgtProperty = binding.getProperty(tgtRole);
                    final Value tgtValue = (Value) binding.getTypesBinding().convert(value, direction);
                    return new ValuePropertyPair(tgtValue, tgtProperty);
                }
            }
            // We did not find any usable type binding
            throw new ConversionException("Can not convert value '" + value + "' for propery '" + property.getName() + "'");
        }

        public Set<SItemSetPropertyPair> convertSet(SItemSet set,
                                                    Property property) {
            // Set of all bindings usable to convert property
            final Set<PropertyPropertyBinding> bindings =
                    registriesBinding.getPropertyPropertyBindings(property.getName(), srcRole);
            // List of all converted pieces
            final Set<SItemSetPropertyPair> tgtSets = new HashSet<>();
            // The part of src domain that is converted
            SItemSet convertedSet = UncheckedSet.EMPTY;

            // Iterate on all usable type bindings.
            for (final PropertyPropertyBinding binding : bindings) {
                // Compute the intersection of the src set to convert with the domain of the type binding
                final SItemSet srcInter = binding.getTypesBinding().getDomain(srcRole).intersection(set);
                if (!srcInter.isEmpty()) {
                    final Property tgtProperty = binding.getProperty(tgtRole);
                    final SItemSet tgtInter = binding.getTypesBinding().convert(srcInter, direction);
                    tgtSets.add(new SItemSetPropertyPair(tgtInter, tgtProperty));
                    convertedSet = convertedSet.union(srcInter);
                }
            }
            if (convertedSet.hasSameContentAs(set)) {
                // src set was totally converted
                return tgtSets;
            } else {
                // src set was not or partially converted
                throw new ConversionException("Can not convert set '" + set + "' for property '" + property.getName() + "'");
            }
        }

        /**
         * Converts an AbstractValueNode.
         *
         * @param node The node to convert.
         * @return The conversion of {@code node}.
         * @throws ConversionException When conversion fails.
         */
        private AbstractValueNode convertValueNode(AbstractValueNode node) {
            final Value srcValue = node.getValue();
            final Name srcPropertyName = node.getName();
            final Property srcProperty = registriesBinding.getDictionary(srcRole).getProperty(srcPropertyName);
            final ValuePropertyPair pair = convertValue(srcValue, srcProperty);
            return node.create(pair.property().getName(), pair.value());
        }

        /**
         * Converts an AbstractSetNode.
         *
         * @param node The node to convert.
         * @return The conversion of {@code node}.
         * @throws ConversionException When conversion fails.
         */
        private Node convertSetNode(AbstractSetNode node) {
            final SItemSet srcSet = node.getSet();
            final Name srcPropertyName = node.getName();
            final Property srcProperty = registriesBinding.getDictionary(srcRole).getProperty(srcPropertyName);

            final Set<SItemSetPropertyPair> pairs = convertSet(srcSet, srcProperty);
            // List of all converted pieces
            final List<AbstractSetNode> tgtSets = new ArrayList<>();
            for (final SItemSetPropertyPair pair : pairs) {
                tgtSets.add(node.create(pair.property().getName(), pair.set()));
            }
            return NaryAndNode.createSimplestAnd(tgtSets);
        }

        /**
         * Converts a RefNode.
         *
         * @param node The node to convert.
         * @return The conversion of {@code node}.
         * @throws ConversionException When conversion fails.
         */
        private Node convertRefNode(RefNode node) {
            // Name of the src ref (Alias or Boolean property)
            final Name srcName = node.getName();
            // Set of all bindings usable to convert src Alias / Boolean property
            final Set<DItemsBinding> bindings = registriesBinding.getDItemBindings(srcName, srcRole);

            // There should be one and only one binding in that case
            if (bindings.size() == 1) {
                final DItemsBinding binding = bindings.iterator().next();
                final Name tgtName;
                final boolean revert;
                if (binding instanceof PropertyPropertyBinding) {
                    tgtName = ((PropertyPropertyBinding) binding).getProperty(tgtRole).getName();
                    final TypesBinding typeBinding = ((PropertyPropertyBinding) binding).getTypesBinding();
                    if (typeBinding instanceof BooleanBooleanBinding) {
                        revert = ((BooleanBooleanBinding) typeBinding).revert();
                    } else {
                        revert = false;
                    }
                } else {
                    tgtName = ((AliasAliasBinding) binding).getAlias(tgtRole).getName();
                    revert = false;
                }
                if (revert) {
                    return new NotNode(new RefNode(tgtName));
                } else {
                    return new RefNode(tgtName);
                }
            } else {
                throw new ConversionException("Can not convert node " + node + " (" + bindings.size() + " bindings associated to "
                        + srcName + " as " + srcRole + ").");
            }
        }

        @Override
        public Node visitLeaf(AbstractLeafNode node) {
            if (node instanceof AbstractPropertyNode) {
                if (node instanceof AbstractValueNode) {
                    return convertValueNode((AbstractValueNode) node);
                } else {
                    return convertSetNode((AbstractSetNode) node);
                }
            } else if (node instanceof RefNode) {
                return convertRefNode((RefNode) node);
            } else {
                return node;
            }
        }
    }
}