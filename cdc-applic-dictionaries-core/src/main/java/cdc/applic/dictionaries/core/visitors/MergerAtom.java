package cdc.applic.dictionaries.core.visitors;

import java.util.Comparator;
import java.util.Objects;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.DictionaryUtils;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.ast.AbstractSetNode;
import cdc.applic.expressions.ast.AbstractValueNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotEqualNode;
import cdc.applic.expressions.ast.NotInNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.content.Value;
import cdc.util.lang.Checks;

/**
 * Atomic item for merger. It is a triplet (negated, Alias or Property, Set).
 * <p>
 * Set is {@code null} for aliases.
 *
 * @author Damien Carbonne
 */
public final class MergerAtom {
    public static final Comparator<MergerAtom> COMPARATOR = (ma1,
                                                             ma2) -> {
        if (ma1 == ma2) {
            return 0;
        }
        // Check null
        if (ma1 == null || ma2 == null) {
            return ma1 == null ? -1 : 1;
        }

        // Compare negated
        if (ma1.negated != ma2.negated) {
            return ma1.negated ? -1 : 1;
        }

        // Compare items
        final int cmDItem = NamedDItem.ORDINAL_NAME_COMPARATOR.compare(ma1.item, ma2.item);
        if (cmDItem == 0
                && ma1.item instanceof final Property p1) {
            // Then compare sets
            final SItemSet set1 = ma1.set;
            final SItemSet set2 = ma2.set;
            return Property.compare(p1, set1.getChecked(), set2.getChecked());
        } else {
            return cmDItem;
        }
    };

    private final boolean negated;
    private final NamedDItem item;
    private final SItemSet set;

    private MergerAtom(boolean negated,
                       Property property,
                       SItemSet set) {
        Checks.isNotNull(property, "property");
        Checks.isNotNull(set, "set");

        this.negated = negated;
        this.item = property;
        this.set = set.getChecked();
    }

    private MergerAtom(boolean negated,
                       Property property,
                       Value value) {
        Checks.isNotNull(property, "property");
        Checks.isNotNull(value, "value");

        this.negated = negated;
        this.item = property;
        this.set = SItemSetUtils.createBest(value);
    }

    private MergerAtom(Alias alias) {
        Checks.isNotNull(alias, "alias");

        this.negated = false;
        this.item = alias;
        this.set = null;
    }

    public boolean isNegated() {
        return negated;
    }

    public NamedDItem getItem() {
        return item;
    }

    public SItemSet getSet() {
        return set;
    }

    /**
     * Merge this MergerAtom with another one, if possible.
     * <p>
     * This is possible when both MergerAtoms have the same negated and item.
     *
     * @param atom The other MergerAtom.
     * @return The merger of the MergerAtom with {@code atom} or {@code null}.
     */
    public MergerAtom mergeWith(MergerAtom atom) {
        if (negated == atom.negated && item == atom.item) {
            if (set == null) {
                // Must be an alias
                return this;
            } else {
                // Must be a property: sets are not null
                return new MergerAtom(negated, (Property) item, set.union(atom.set));
            }
        } else {
            return null;
        }
    }

    /**
     * @return The conversion of this MergerAtom to a Node.<br>
     *         If item is a property, an {@link AbstractSetNode} is returned.
     */
    public Node toNode() {
        if (item instanceof Property) {
            if (negated) {
                return new NotInNode(item.getName(), set);
            } else {
                return new InNode(item.getName(), set);
            }
        } else {
            // Alias (not negated)
            return new RefNode(item.getName());
        }
    }

    /**
     * Builds a MergerAtom from a {@link AbstractValueNode}, {@link AbstractSetNode} or {@link RefNode}.
     * <p>
     * In other cases, returns {@code null}.
     *
     * @param node The node.
     * @param dictionary The dictionary (used to retrieve Property and ALias).
     * @return A MergerAtom or {@code null}.
     */
    public static MergerAtom build(Node node,
                                   Dictionary dictionary) {
        if (node instanceof final AbstractValueNode n) {
            return new MergerAtom(node instanceof NotEqualNode,
                                  dictionary.getRegistry().getProperty(n.getName()),
                                  n.getValue());
        } else if (node instanceof final AbstractSetNode n) {
            return new MergerAtom(node instanceof NotInNode,
                                  dictionary.getRegistry().getProperty(n.getName()),
                                  n.getSet());
        } else if (node instanceof final RefNode n) {
            if (dictionary.getRegistry().hasAlias(n.getName())) {
                // Alias
                return new MergerAtom(dictionary.getRegistry().getAlias(n.getName()));
            } else {
                // Boolean property
                return new MergerAtom(false,
                                      DictionaryUtils.getAvailableBooleanProperty(n.getName(), dictionary),
                                      BooleanValue.TRUE);
            }
        } else {
            return null;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(negated,
                            item,
                            set);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof MergerAtom)) {
            return false;
        }
        final MergerAtom other = (MergerAtom) object;
        return this.negated == other.negated
                && this.item.equals(other.item)
                && Objects.equals(this.set, other.set);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append('[');
        if (isNegated()) {
            builder.append("NOT ");
        }
        builder.append(getItem().getName());
        if (getSet() != null) {
            builder.append(' ')
                   .append(getSet());
        }
        builder.append(']');
        return builder.toString();
    }
}