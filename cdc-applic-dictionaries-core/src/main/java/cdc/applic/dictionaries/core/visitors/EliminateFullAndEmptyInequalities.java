package cdc.applic.dictionaries.core.visitors;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.core.utils.SItemSetOperations;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.InequalityNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.parsing.ComparisonOperator;
import cdc.util.lang.Checks;

/**
 * Converter that applies the following rewriting rules:
 * <ul>
 * <li>&pi; &lt; &upsilon; &equiv; &bottom; if &Omega;(&pi;&lt;&upsilon;) = &empty;
 * <li>&pi; &gt; &upsilon; &equiv; &bottom; if &Omega;(&pi;&gt;&upsilon;) = &empty;
 * <li>&pi; &nle; &upsilon; &equiv; &bottom; if &Omega;(&pi;&nle;&upsilon;) = &empty;
 * <li>&pi; &nge; &upsilon; &equiv; &bottom; if &Omega;(&pi;&nge;&upsilon;) = &empty;
 * <li>&pi; &le; &upsilon; &equiv; &top; if &Omega;(&pi;&le;&upsilon;) = &Omega;(&pi;)
 * <li>&pi; &ge; &upsilon; &equiv; &top; if &Omega;(&pi;&ge;&upsilon;) = &Omega;(&pi;)
 * <li>&pi; &nlt; &upsilon; &equiv; &top; if &Omega;(&pi;&nlt;&upsilon;) = &Omega;(&pi;)
 * <li>&pi; &ngt; &upsilon; &equiv; &top; if &Omega;(&pi;&ngt;&upsilon;) = &Omega;(&pi;)
 * </ul>
 * <b>Note:</b>If a reserve is applied on a type, no elimination is possible on related inequalities.
 *
 * @author Damien Carbonne
 */
public final class EliminateFullAndEmptyInequalities extends AbstractConverter {
    private final Dictionary dictionary;
    private final ReserveStrategy reserveStrategy;

    private EliminateFullAndEmptyInequalities(Dictionary dictionary,
                                              ReserveStrategy reserveStrategy) {
        Checks.isNotNull(dictionary, "dictionary");
        Checks.isNotNull(reserveStrategy, "reserveStrategy");
        this.dictionary = dictionary;
        this.reserveStrategy = reserveStrategy;
    }

    public static Node execute(Node node,
                               Dictionary dictionary,
                               ReserveStrategy reserveStrategy) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(dictionary, "dictionary");
        Checks.isNotNull(reserveStrategy, "reserveStrategy");
        final EliminateFullAndEmptyInequalities visitor = new EliminateFullAndEmptyInequalities(dictionary, reserveStrategy);
        return node.accept(visitor);
    }

    private boolean isFullSet(Value value,
                              Type type,
                              ComparisonOperator operator) {
        if (reserveStrategy.hasReserve(type)) {
            // We cannot do anything in that case. We don't know where reserves will be added.
            return false;
        } else {
            return SItemSetOperations.isFullSet(value, type, operator);
        }
    }

    private boolean isEmptySet(Value value,
                               Type type,
                               ComparisonOperator operator) {
        if (reserveStrategy.hasReserve(type)) {
            // We cannot do anything in that case. We don't know where reserves will be added.
            return false;
        } else {
            return SItemSetOperations.isEmptySet(value, type, operator);
        }
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (node instanceof final InequalityNode n) {
            final Name name = n.getName();
            final Property property = dictionary.getRegistry().getProperty(name);
            final Type type = property.getType();
            if (isFullSet(n.getValue(), type, n.getComparisonOperator())) {
                return TrueNode.INSTANCE;
            } else if (isEmptySet(n.getValue(), type, n.getComparisonOperator())) {
                return FalseNode.INSTANCE;
            } else {
                return node;
            }
        } else {
            return node;
        }
    }
}