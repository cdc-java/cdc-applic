package cdc.applic.dictionaries.core.visitors;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AbstractPropertyNode;
import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.util.lang.Checks;

/**
 * Utility class used to move not nodes (&not;) inwards by recursively applying
 * those rewriting rules:
 * <ul>
 * <li>&not;(&not;&alpha;) &equiv; &alpha; (double negation elimination)
 * <li>&not;(&alpha; &and; &beta;) &equiv; &not;&alpha; &or; &not;&beta;
 * (de Morgan law)
 * <li>&not;(&alpha; &or; &beta;) &equiv; &not;&alpha; &and; &not;&beta;
 * (de Morgan law)
 * <li>&not;(&pi; = &upsilon;) &equiv; &pi; &ne; &upsilon;
 * <li>&not;(&pi; &ne; &upsilon;) &equiv; &pi; = &upsilon;
 * <li>&not;(&pi; = &upsilon;) &equiv; &pi; &ne; &upsilon;
 * <li>&not;(&pi; &lt; &upsilon;) &equiv; &pi; &nlt; &upsilon;
 * <li>&not;(&pi; &nlt; &omega;) &equiv; &pi; &lt; &omega;
 * <li>&not;(&pi; &gt; &upsilon;) &equiv; &pi; &ngt; &upsilon;
 * <li>&not;(&pi; &ngt; &omega;) &equiv; &pi; &gt; &omega;
 * <li>&not;(&pi; &le; &upsilon;) &equiv; &pi; &nle; &upsilon;
 * <li>&not;(&pi; &nle; &omega;) &equiv; &pi; &le; &omega;
 * <li>&not;(&pi; &ge; &upsilon;) &equiv; &pi; &nge; &upsilon;
 * <li>&not;(&pi; &nge; &omega;) &equiv; &pi; &ge; &omega;
 * <li>&not;(&pi; &isin; &omega;) &equiv; &pi; &notin; &omega;
 * <li>&not; &perp; &equiv; &top;
 * <li>&not; &top; &equiv; &perp;
 * </ul>
 * Optionally, negative leaves can be replaced by the negation of their
 * positive counterpart.
 * <p>
 * <b>Warning</b>: This must be called:
 * <ul>
 * <li>after equivalence nodes (&harr;) have been removed,
 * <li>after xor nodes (&nharr;) have been removed,
 * <li>and after implication nodes (&rarr;) have been removed.
 * </ul>
 *
 * <b>Note</b>: This is an adaptation of AIMA Java implementation.
 *
 * @author Damien Carbonne
 */
public final class MoveNotInwards extends AbstractConverter {
    private final Variant variant;

    private MoveNotInwards(Variant variant) {
        Checks.isNotNull(variant, "variant");
        this.variant = variant;
    }

    /**
     * Enumeration of possible policies for handling of negative leaves.
     *
     * @author Damien Carbonne
     */
    public enum Variant {
        /**
         * Use negative leaves when possible, by applying those rules:
         * <ul>
         * <li>&not;(&pi; = &upsilon;) &equiv; &pi; &ne; &upsilon;
         * <li>&not;(&pi; &isin; &omega;) &equiv; &pi; &notin; &omega;
         * <li>&not;(&pi; &lt; &upsilon;) &equiv; &pi; &nlt; &upsilon;
         * <li>&not;(&pi; &gt; &upsilon;) &equiv; &pi; &ngt; &upsilon;
         * <li>&not;(&pi; &le; &upsilon;) &equiv; &pi; &nle; &upsilon;
         * <li>&not;(&pi; &ge; &upsilon;) &equiv; &pi; &nge; &upsilon;
         * <li>&not;&top; &equiv; &perp;
         * </ul>
         */
        USE_NEGATIVE_LEAVES,

        /**
         * Never use negative leaves, by applying those rules:
         * <ul>
         * <li>(&pi; &ne; &upsilon;) &equiv; &not; (&pi; = &upsilon;)
         * <li>(&pi; &notin; &omega;) &equiv; &not; (&pi; &isin; &omega;)
         * <li>&pi; &nlt; &upsilon; &equiv; &not;(&pi; &lt; &upsilon;)
         * <li>&pi; &ngt; &upsilon; &equiv; &not;(&pi; &gt; &upsilon;)
         * <li>&pi; &nle; &upsilon; &equiv; &not;(&pi; &le; &upsilon;)
         * <li>&pi; &nge; &upsilon; &equiv; &not;(&pi; &ge; &upsilon;)
         * </ul>
         */
        DONT_USE_NEGATIVE_LEAVES,

        /**
         * Never use negation, whether on a leaf or not.
         * <p>
         * This is similar to use negative leaves followed by a negative
         * leaves removal (replaced by the positive equivalent).<br>
         * <b>WARNING:</b> The necessitates a dictionary and full set computation.
         */
        DONT_USE_NEGATION_IGNORE_WHEN_IMPOSSIBLE,

        DONT_USE_NEGATION_THROW_WHEN_IMPOSSIBLE;

        /**
         * @return {@code true} when this variant requires a dictionary.
         */
        public boolean requiresDictionary() {
            return this == DONT_USE_NEGATION_IGNORE_WHEN_IMPOSSIBLE
                    || this == DONT_USE_NEGATION_THROW_WHEN_IMPOSSIBLE;
        }
    }

    public static Node execute(Node node,
                               Variant variant,
                               Dictionary dictionary,
                               ReserveStrategy reserveStrategy) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(variant, "variant");

        final MoveNotInwards converter = new MoveNotInwards(variant);
        if (variant == Variant.DONT_USE_NEGATION_IGNORE_WHEN_IMPOSSIBLE) {
            Checks.isNotNull(dictionary, "dictionary");
            return ConvertToPositiveLeaves.execute(node.accept(converter),
                                                   ConvertToPositiveLeaves.Variant.IGNORE_WHEN_IMPOSSIBLE,
                                                   dictionary,
                                                   reserveStrategy);
        } else if (variant == Variant.DONT_USE_NEGATION_THROW_WHEN_IMPOSSIBLE) {
            Checks.isNotNull(dictionary, "dictionary");
            return ConvertToPositiveLeaves.execute(node.accept(converter),
                                                   ConvertToPositiveLeaves.Variant.THROW_WHEN_IMPOSSIBLE,
                                                   dictionary,
                                                   reserveStrategy);
        } else {
            return node.accept(converter);
        }
    }

    public static Node execute(Node node,
                               Variant variant) {
        return execute(node, variant, null, null);
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (variant == Variant.DONT_USE_NEGATIVE_LEAVES) {
            if (node instanceof final AbstractPropertyNode n) {
                if (n.isNegative()) {
                    // Optionally:
                    // - p != v <-> !(p = v)
                    // - p !< s <-> !(p < s)
                    // - p !>= v <-> !(p >= v)
                    // - p !<= v <-> !(p <= v)
                    // - p !> v <-> !(p > v)
                    // - p !< v <-> !(p < v)
                    // - p !<: s <-> !(p <: s)
                    return new NotNode(n.negate());
                } else {
                    return super.visitLeaf(node);
                }
            } else {
                return super.visitLeaf(node);
            }
        } else {
            return super.visitLeaf(node);
        }
    }

    @Override
    public Node visitUnary(AbstractUnaryNode node) {
        // Expecting NotNode
        final Node negated = node.getAlpha();

        if (negated instanceof AbstractLeafNode) {
            if (negated instanceof final AbstractPropertyNode n) {
                if (n.isNegative() || variant == Variant.USE_NEGATIVE_LEAVES) {
                    // Systematically:
                    // - !(p != v) <-> p = v
                    // - !(p !< v) <-> p < v
                    // - !(p !> v) <-> p > v
                    // - !(p !<= v) <-> p <= v
                    // - !(p !>= v) <-> p >= v
                    // - !(p !<: s) <-> p <: s
                    // Optionally:
                    // - !(p = v) <-> p != v
                    // - !(p < v) <-> p !< v
                    // - !(p > v) <-> p !> v
                    // - !(p <= v) <-> p !<= v
                    // - !(p >= v) <-> p !>= v
                    // - !(p <: s) <-> p !<: s
                    return n.negate();
                } else {
                    // !n.isNegative() && variant != Variant.USE_NEGATIVE_LEAVES
                    return node;
                }
            } else if (negated instanceof FalseNode) {
                // Systematically: !false <-> true
                return TrueNode.INSTANCE;
            } else if (negated instanceof TrueNode) {
                // Systematically: !true <-> false
                return FalseNode.INSTANCE;
            } else {
                // Can not do more
                return node;
            }
        } else if (negated instanceof final NotNode n) {
            // Double negation
            // !(!alpha) <==> alpha
            final Node alpha = n.getAlpha();
            return alpha.accept(this);
        } else if (negated instanceof AndNode || negated instanceof OrNode) {
            final Node alpha = ((AbstractBinaryNode) negated).getAlpha();
            final Node beta = ((AbstractBinaryNode) negated).getBeta();

            // This ensures double-negation elimination happens
            final Node notAlpha = new NotNode(alpha).accept(this);
            final Node notBeta = new NotNode(beta).accept(this);

            if (negated instanceof AndNode) {
                return new OrNode(notAlpha, notBeta);
            } else {
                return new AndNode(notAlpha, notBeta);
            }
        } else if (negated instanceof NaryAndNode || negated instanceof NaryOrNode) {
            final Node[] nodes = ((AbstractNaryNode) negated).getChildren().clone();

            for (int index = 0; index < nodes.length; index++) {
                // This ensures double-negation elimination happens
                nodes[index] = new NotNode(nodes[index]).accept(this);
            }

            if (negated instanceof NaryAndNode) {
                return new NaryOrNode(nodes);
            } else {
                return new NaryAndNode(nodes);
            }
        } else {
            throw new IllegalArgumentException("Equivalence, Xor and Implication should not exist in input: " + node);
        }
    }
}