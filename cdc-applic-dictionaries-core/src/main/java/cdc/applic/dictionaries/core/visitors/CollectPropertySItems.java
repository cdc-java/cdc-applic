package cdc.applic.dictionaries.core.visitors;

import java.util.Set;

import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractPropertyNode;
import cdc.applic.expressions.ast.AbstractSetNode;
import cdc.applic.expressions.ast.AbstractValueNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.AbstractCollector;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

/**
 * A Collector that collects {@link SItem}s used in association with a {@link Property}.
 *
 * @author Damien Carbonne
 */
public final class CollectPropertySItems extends AbstractCollector<SItem> {
    private final Name propertyName;

    private CollectPropertySItems(Name propertyName,
                                  Set<SItem> set) {
        super(set);
        this.propertyName = propertyName;
    }

    public static void collect(Node node,
                               Name propertyName,
                               Set<SItem> items) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(propertyName, "propertyName");
        Checks.isNotNull(items, "items");

        final CollectPropertySItems collector = new CollectPropertySItems(propertyName, items);
        node.accept(collector);
    }

    public static void collect(Node node,
                               Property property,
                               Set<SItem> items) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(property, "property");
        Checks.isNotNull(items, "items");

        collect(node, property.getName(), items);
    }

    @Override
    public Void visitLeaf(AbstractLeafNode node) {
        if (node instanceof final AbstractPropertyNode n && n.getName().equals(propertyName)) {
            if (n instanceof AbstractValueNode) {
                getSet().add(((AbstractValueNode) node).getValue());
            } else {
                getSet().addAll(((AbstractSetNode) node).getCheckedSet().getItems());
            }
        }
        return null;
    }
}