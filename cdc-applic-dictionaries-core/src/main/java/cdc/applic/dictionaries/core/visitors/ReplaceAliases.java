package cdc.applic.dictionaries.core.visitors;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

/**
 * Converter that recursively replaces Aliases by their definition.
 * <p>
 * The returned Node does not contain any Alias, only Properties.
 *
 * @author Damien Carbonne
 */
public final class ReplaceAliases extends AbstractConverter {
    private final Dictionary dictionary;
    /** Set to true if an alias was replaced during a loop. */
    private boolean replacedAlias = false;

    private ReplaceAliases(Dictionary dictionary) {
        Checks.isNotNull(dictionary, "dictionary");
        this.dictionary = dictionary;
    }

    public static Node execute(Node node,
                               Dictionary dictionary) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(dictionary, "dictionary");
        final ReplaceAliases visitor = new ReplaceAliases(dictionary);
        Node n = node;
        boolean loop = true;
        while (loop) {
            n = n.accept(visitor);
            loop = visitor.replacedAlias;
            visitor.replacedAlias = false;
        }
        return n;
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (node instanceof final RefNode n) {
            final Name name = n.getName();
            if (dictionary.getRegistry().hasAlias(name)) {
                final Alias alias = dictionary.getRegistry().getAlias(name);
                replacedAlias = true;
                return alias.getQualifiedExpression().getRootNode();
            } else {
                return node;
            }
        } else {
            return node;
        }
    }
}