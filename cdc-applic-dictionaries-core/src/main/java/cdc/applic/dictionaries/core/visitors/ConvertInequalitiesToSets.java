package cdc.applic.dictionaries.core.visitors;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.core.utils.SItemSetOperations;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.InequalityNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

/**
 * Converter that replaces Inequality nodes by set nodes, by applying those rewriting rule:
 * <ul>
 * <li>&pi; &lt; &upsilon; &equiv; &pi; &isin; &Omega;(&pi; &lt; &upsilon;)</li>
 * <li>&pi; &le; &upsilon; &equiv; &pi; &isin; &Omega;(&pi; &le; &upsilon;)</li>
 * <li>&pi; &gt; &upsilon; &equiv; &pi; &isin; &Omega;(&pi; &gt; &upsilon;)</li>
 * <li>&pi; &ge; &upsilon; &equiv; &pi; &isin; &Omega;(&pi; &ge; &upsilon;)</li>
 * <li>&pi; &nlt; &upsilon; &equiv; &pi; &isin; &Omega;(&pi; &nlt; &upsilon;)</li>
 * <li>&pi; &nle; &upsilon; &equiv; &pi; &isin; &Omega;(&pi; &nle; &upsilon;)</li>
 * <li>&pi; &ngt; &upsilon; &equiv; &pi; &isin; &Omega;(&pi; &ngt; &upsilon;)</li>
 * <li>&pi; &nge; &upsilon; &equiv; &pi; &isin; &Omega;(&pi; &nge; &upsilon;)</li>
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class ConvertInequalitiesToSets extends AbstractConverter {
    private final Dictionary dictionary;

    private ConvertInequalitiesToSets(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public static Node execute(Node node,
                               Dictionary dictionary) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(dictionary, "dictionary");

        final ConvertInequalitiesToSets converter = new ConvertInequalitiesToSets(dictionary);
        return node.accept(converter);
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (node instanceof final InequalityNode n) {
            final Name name = n.getName();
            final Property property = dictionary.getRegistry().getProperty(name);
            final Type type = property.getType();
            final SItemSet set = SItemSetOperations.toSet(n.getValue(), type, n.getComparisonOperator());
            return new InNode(name, set);
        } else {
            return node;
        }
    }
}