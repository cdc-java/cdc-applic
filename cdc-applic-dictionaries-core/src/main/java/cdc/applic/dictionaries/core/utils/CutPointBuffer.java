package cdc.applic.dictionaries.core.utils;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cdc.applic.expressions.content.AbstractRange;
import cdc.applic.expressions.content.Domain;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.Value;
import cdc.util.debug.Printable;
import cdc.util.lang.Checks;

/**
 * Class used to compute the minimum set of intervals that covers a set of intervals and values belonging to a {@link Domain}.
 * <p>
 * Domain is discrete.<br>
 * For example, with {@code 1~3}, {@code 2} and {@code 2~5} in the integer domain,
 * expected result is {@code 1}, {@code 2}, {@code 3} and {@code 4~5}.
 * <p>
 * A value {@code v} has a predecessor, {@code pred(v)} and a successor, {@code succ(v)}.<br>
 * The virtual value between {@code pred(v)} and {@code v} is named {@code v-} or {@code pred(v)+}.<br>
 * The virtual value between {@code v} and {@code succ(v)} is named {@code v+} or {@code succ(v)-}.<br>
 * <p>
 * With an interval min~max, cut points min- and max+ are virtual values: they don't belong to domain.<br>
 * Each time a cut point is associated to an opening bound, its count is incremented by 1.<br>
 * Each time a cut point is associated to a closing bound, its count is decremented by 1.
 * <p>
 * In order to manipulate values in the domain, cut point values are shifted towards minus infinity.<br>
 * So {@code v-} is replaced by {@code pred(v)} and {@code v+} by {@code v}.<br>
 * This excludes the domain minimum value from usable values as interval bounds. Practically, this should not be a problem.
 * <p>
 * With example data, we obtain:
 * <ul>
 * <li>{@code 1~3: 1- 3+} &equiv; {@code 1- 4-}
 * <li>{@code 2~2: 2- 2+} &equiv; {@code 2- 3-}
 * <li>{@code 2~5: 2- 5+} &equiv; {@code 2- 6-}
 * </ul>
 * <pre>
 * 1-  2-  3-  4-  5-  6-
 * |   +===============-
 * |   +===-   |   |   |
 * +===========-   |   |
 * | 1 | 2 | 3 | 4 | 5 |
 * </pre>
 *
 * In the end:
 * <ul>
 * <li>1-: &Delta; = +1, &Sigma; = 1
 * <li>2-: &Delta; = +2, &Sigma; = 3
 * <li>3-: &Delta; = -1, &Sigma; = 2
 * <li>4-: &Delta; = -1, &Sigma; = 1
 * <li>6-: &Delta; = -1, &Sigma; = 0
 * </ul>
 *
 * @author Damien Carbonne
 *
 * @param <D> The domain type.
 * @param <V> The value type.
 * @param <R> The range type.
 */
public final class CutPointBuffer<D extends Domain<V, R>, V extends Value & Comparable<? super V>, R extends AbstractRange<V, R>> implements Printable {
    /** The associated Domain. */
    private final D domain;

    /**
     * A cut point has an x coordinate and an associated delta.
     * <p>
     * The associated delta must be added to obtain a meaningful value.
     *
     * @author Damien Carbonne
     * @param <V> The value type.
     */
    private static class Point<V extends Comparable<? super V>> {
        /** Coordinate. */
        protected final V x;

        /**
         * The delta value.
         * <p>
         * The is the difference (open / close) at x with previous point.
         */
        protected int delta = 0;

        protected Point(V x) {
            this.x = x;
        }

        @Override
        public String toString() {
            return x + " " + delta;
        }
    }

    /**
     * Comparator of points using x coordinates.
     */
    private final Comparator<Point<V>> pointXComparator = (o1,
                                                           o2) -> o1.x.compareTo(o2.x);

    /**
     * Sorted list (using x coordinate) of points.
     */
    private final List<Point<V>> points = new ArrayList<>();

    private boolean valid = false;

    /**
     * Ordered list of non overlapping ranges.<br>
     * This is cleared each time cut points are created.
     */
    private final List<R> ranges = new ArrayList<>();

    /**
     * Indices of the previous disjoint interval.
     * <p>
     * This list has the same size as {@link #ranges}.
     * First value is -1.
     */
    private final List<Integer> gaps = new ArrayList<>();

    public CutPointBuffer(D domain) {
        this.domain = domain;
    }

    private void resetCutRanges() {
        if (valid) {
            ranges.clear();
            gaps.clear();
            valid = false;
        }
    }

    private void buildCutRanges() {
        if (!valid) {
            int total = 0;
            int gapIndex = -1;
            for (int index = 0; index < points.size() - 1; index++) {
                final Point<V> p1 = points.get(index);
                total += p1.delta;
                // Checks.assertTrue(total >= 0, "Implementation error");
                // When total = 0, we are outside valid values.
                if (total > 0) {
                    final Point<V> p2 = points.get(index + 1);
                    ranges.add(domain.create(domain.succ(p1.x), p2.x));
                    gaps.add(gapIndex);
                } else {
                    // total == 0
                    gapIndex = ranges.size();
                }
            }
            valid = true;
        }
    }

    /**
     * Creates if necessary a point at a given coordinate.
     * <p>
     * The created point is inserted a the right place, so that points is sorted.
     *
     * @param x The x coordinate.
     * @return The point whose coordinate is {@code x}.
     */
    private Point<V> getOrCreatePoint(V x) {
        resetCutRanges();

        final Point<V> key = new Point<>(x);
        final int index = Collections.binarySearch(points, key, pointXComparator);
        final Point<V> result;
        if (index >= 0) {
            result = points.get(index);
        } else {
            result = key;
            points.add(-(index + 1), key);
        }
        return result;
    }

    private void addCutPoint(V x,
                             int delta) {
        final Point<V> point = getOrCreatePoint(x);
        point.delta += delta;
    }

    /**
     * Adds an interval known by its bounds.
     * <p>
     * If {@code min > max} nothing is done.
     *
     * @param min The minimum bound.
     * @param max The maximum bound.
     * @throws IllegalArgumentException When {@code min} equals domain min value.
     */
    public void add(V min,
                    V max) {
        Checks.isNotNull(min, "min");
        Checks.isNotNull(max, "max");
        Checks.isTrue(!domain.min().equals(min), "min is too small");

        if (max.compareTo(min) >= 0) {
            addCutPoint(domain.pred(min), 1);
            addCutPoint(max, -1);
        }
    }

    /**
     * Adds a singleton.
     *
     * @param value The singleton value.
     * @throws IllegalArgumentException When {@code value} equals domain min value.
     */
    public void add(V value) {
        Checks.isNotNull(value, "value");

        add(value, value);
    }

    /**
     * Adds a range.
     *
     * @param range The range.
     * @throws IllegalArgumentException When range is {@code null}
     *             or when {@code range} min value equals domain min value.
     */
    public void add(R range) {
        Checks.isNotNull(range, "range");

        add(range.getMin(), range.getMax());
    }

    public void add(SItem item) {
        if (domain.getValueClass().isInstance(item)) {
            add(domain.getValueClass().cast(item));
        } else if (domain.getRangeClass().isInstance(item)) {
            add(domain.getRangeClass().cast(item));
        } else {
            throw new IllegalArgumentException("Can not add " + item);
        }
    }

    public void add(CutPointBuffer<D, V, R> other) {
        Checks.isNotNull(other, "other");

        for (final Point<V> op : other.points) {
            final Point<V> p = getOrCreatePoint(op.x);
            p.delta += op.delta;
        }
    }

    /**
     * @return A list of ranges that have the expected properties.
     */
    public List<R> getCutRanges() {
        buildCutRanges();

        return ranges;
    }

    /**
     * Returns the list of ranges that overlaps a range and is equivalent to that range.
     *
     * @param range The range.
     * @return A list of ranges equivalent to {@code range}.
     * @throws IllegalArgumentException When {@code range} is {@code null}
     *             or can not be mapped as an exact set of contiguous equivalent ranges.
     */
    public List<R> getEquivalence(R range) {
        Checks.isNotNull(range, "range");

        buildCutRanges();

        final int lowIndex = Collections.binarySearch(ranges, range, domain.minComparator());
        final int highIndex = Collections.binarySearch(ranges, range, domain.maxComparator());

        if (lowIndex < 0 || highIndex < 0) {
            // lowIndex < 0 means that no interval with a minimum bound corresponding to range min bound has been found
            // highIndex < 0 means that no interval with a maximum bound corresponding to range max bound has been found
            throw new IllegalArgumentException("Can not map this range: " + range + " on exact ranges");
        }

        // Check that obtained ranges are contiguous
        final int lowGapIndex = gaps.get(lowIndex);
        final int highGapIndex = gaps.get(highIndex);

        if (lowGapIndex != highGapIndex) {
            throw new IllegalArgumentException("Can not map this range: " + range + " on contiguous ranges");
        }

        return new ArrayList<>(ranges.subList(lowIndex, highIndex + 1));
    }

    public List<R> getEquivalence(V value) {
        return getEquivalence(domain.create(value));
    }

    public List<R> getEquivalence(SItem item) {
        if (domain.getValueClass().isInstance(item)) {
            return getEquivalence(domain.getValueClass().cast(item));
        } else if (domain.getRangeClass().isInstance(item)) {
            return getEquivalence(domain.getRangeClass().cast(item));
        } else {
            throw new IllegalArgumentException("Can not get equivalence of " + item + " for domain " + domain);
        }
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        domain.print(out, level);

        indent(out, level);
        out.println("Points: " + points);

        indent(out, level);
        out.println(valid ? "Valid" : "Invalid");
        if (valid) {
            indent(out, level);
            out.println("Ranges and Gaps");
            for (int index = 0; index < ranges.size(); index++) {
                indent(out, level + 1);
                out.println(index + " " + ranges.get(index) + " " + gaps.get(index));
            }
        }
    }
}