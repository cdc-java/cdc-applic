package cdc.applic.dictionaries.core.visitors;

import java.util.Set;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.DictionaryUtils;
import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.PropertyNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.SetNode;
import cdc.applic.expressions.ast.ValueNode;
import cdc.applic.expressions.ast.visitors.AbstractAnalyzer;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

public final class IsCompliantWithNamingConvention extends AbstractAnalyzer {
    private final Dictionary dictionary;
    private final NamingConvention convention;
    private boolean success = true;

    private IsCompliantWithNamingConvention(Dictionary dictionary,
                                            NamingConvention convention) {
        this.dictionary = dictionary;
        this.convention = convention;
    }

    public static boolean test(Node node,
                               Dictionary dictionary,
                               NamingConvention convention) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(dictionary, "dictionary");
        Checks.isNotNull(convention, "convention");

        final IsCompliantWithNamingConvention analyzer = new IsCompliantWithNamingConvention(dictionary, convention);
        node.accept(analyzer);
        return analyzer.success;
    }

    private void check(Set<NamingConvention> conventions) {
        if (!conventions.contains(convention)) {
            success = false;
        }
    }

    @Override
    public Void visitLeaf(AbstractLeafNode node) {
        if (!success) {
            return null;
        }
        if (node instanceof final RefNode refNode) {
            // Alias name and boolean property name synonyms
            final Name name = refNode.getName();
            if (dictionary.getRegistry().hasAlias(name)
                    || DictionaryUtils.isAvailableBooleanProperty(name, dictionary)) {
                check(dictionary.getItemNameNamingConventions(name));
                return null;
            } else {
                success = false;
                return null;
            }
        } else if (node instanceof final PropertyNode propertyNode) {
            // Property name synonyms
            final Name name = propertyNode.getName();
            if (dictionary.getRegistry().hasProperty(name)) {
                check(dictionary.getItemNameNamingConventions(name));
                if (!success) {
                    return null;
                }
            } else {
                success = false;
                return null;
            }

            // If the property type is enumerated, its values could have synonyms.
            final Property property = dictionary.getProperty(name);
            final Type type = property.getType();
            if (type instanceof final EnumeratedType enumeratedType) {
                // Property values synonyms
                if (propertyNode instanceof final ValueNode valueNode) {
                    check(enumeratedType.getNamingConventions(valueNode.getValue().getNonEscapedLiteral()));
                } else if (propertyNode instanceof final SetNode setNode) {
                    for (final SItem item : setNode.getSet().getItems()) {
                        if (item instanceof final Value value) {
                            check(enumeratedType.getNamingConventions(value.getNonEscapedLiteral()));
                            if (!success) {
                                return null;
                            }
                        }
                        // No enum range at the moment
                    }
                }
            }
        }
        return null;
    }
}