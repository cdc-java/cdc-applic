package cdc.applic.dictionaries.core.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import cdc.applic.dictionaries.core.DictionaryDependencyCache;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.TrueNode;
import cdc.util.function.IterableUtils;

/**
 * Utility that can analyze an expression and complement it by adding expressions using default values.
 * <p>
 * The generated complement is an expression like:
 * &pi;<sub>1</sub>=&delta;<sub>1</sub> &and; &pi;<sub>2</sub>=&delta;<sub>2</sub> &and; ...
 * where &delta;<sub>i</sub> is the default value of the type of property &pi;<sub>i</sub>.<br>
 * This should be used with expressions of the form:
 * &pi;<sub>1</sub>=&nu;<sub>1</sub> &and; &pi;<sub>2</sub>=&nu;<sub>2</sub> &and; ...
 * that typically define the applied configuration of a product.
 * <p>
 * <b>WARNING:</b> all properties whose type has a default value are selected.
 * That may include too many properties if the dictionary contains properties that are not related to the product.
 * <p>
 * <b>WARNING:</b> no check is done on the passed expression.
 */
public class Expliciter {
    private final DictionaryHandle handle;

    public Expliciter(DictionaryHandle handle) {
        this.handle = handle;
    }

    /**
     * @param expression The expression.
     * @return A set of properties that are not directly or indirectly used by {@code expression}
     *         and are present in the dictionary.<br>
     *         <b>WARNING:</b> may contain properties that haev no default value.
     */
    public Set<Property> getImplicitProperties(Expression expression) {
        return getImplicitProperties(expression.getRootNode());
    }

    /**
     * @param node The node.
     * @return A set of properties that are not directly or indirectly used by {@code node}
     *         and are present in the dictionary.<br>
     *         <b>WARNING:</b> may contain properties that haev no default value.
     */
    public Set<Property> getImplicitProperties(Node node) {
        final DictionaryDependencyCache cache = DictionaryDependencyCache.get(handle);
        final Set<Property> used = cache.getDependencies(node)
                                        .stream()
                                        .filter(Property.class::isInstance)
                                        .map(Property.class::cast)
                                        .collect(Collectors.toSet());
        final Set<Property> all = IterableUtils.toSet(handle.getDictionary().getAllProperties());
        final Set<Property> set = new HashSet<>(all);
        set.removeAll(used);
        return set;
    }

    /**
     * @param expression The expression.
     * @return An expression that sets default values all of properties that are not
     *         directly or indirectly used by {@code expression} and whose type has a default value.
     */
    public Expression getExplicitComplement(Expression expression) {
        return getExplicitComplement(expression.getRootNode()).toExpression();
    }

    /**
     * @param node The node.
     * @return A node that sets default values all of properties that are not
     *         directly or indirectly used by {@code node} and whose type has a default value.
     */
    public Node getExplicitComplement(Node node) {
        final List<Property> properties = new ArrayList<>(getImplicitProperties(node));
        if (properties.isEmpty()) {
            return TrueNode.INSTANCE;
        } else {
            Collections.sort(properties, NamedDItem.ORDINAL_NAME_COMPARATOR);
            final List<Node> list = new ArrayList<>();
            for (final Property property : properties) {
                final Type type = property.getType();
                if (type.hasDefaultValue()) {
                    list.add(new EqualNode(property.getName(), type.getDefaultValue().orElseThrow()));
                }
            }
            return NaryAndNode.createSimplestAnd(list);
        }
    }

    /**
     * @param expression The expression.
     * @return A new expression that combines {@code expression} with its explicit complement.
     */
    public Expression makeExplicit(Expression expression) {
        return makeExplicit(expression.getRootNode()).toExpression();
    }

    /**
     * @param node The node.
     * @return A new node that combines {@code node} with its explicit complement.
     */
    public Node makeExplicit(Node node) {
        final Node complement = getExplicitComplement(node);
        return new AndNode(node, complement);
    }
}