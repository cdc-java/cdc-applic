package cdc.applic.dictionaries.core.utils;

import java.io.PrintStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.applic.dictionaries.items.DItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.content.AbstractRange;
import cdc.applic.expressions.content.Domain;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.Value;
import cdc.util.debug.Printable;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;

/**
 * Class used to associate a CutPointBuffer to properties.
 *
 * @author Damien Carbonne
 *
 * @param <D> The domain type.
 * @param <V> The value type.
 * @param <R> The range type.
 */
public class CutPointBuffers<D extends Domain<V, R>, V extends Value & Comparable<? super V>, R extends AbstractRange<V, R>> implements Printable {
    private final D domain;

    /** The map from properties to buffers. */
    private final Map<Property, CutPointBuffer<D, V, R>> buffers = new HashMap<>();

    public CutPointBuffers(D domain) {
        Checks.isNotNull(domain, "domain");
        this.domain = domain;
    }

    public void clear() {
        buffers.clear();
    }

    private CutPointBuffer<D, V, R> getOrCreateBuffer(Property property) {
        Checks.isNotNull(property, "property");
        return buffers.computeIfAbsent(property, p -> new CutPointBuffer<>(domain));
    }

    public void add(Property property,
                    SItem item) {
        final CutPointBuffer<D, V, R> buffer = getOrCreateBuffer(property);
        buffer.add(item);
    }

    /**
     * @return The set of known properties.
     */
    public Set<Property> getProperties() {
        return buffers.keySet();
    }

    /**
     * Returns the list of ranges associated to a property.
     * <p>
     * If {@code property} is not known, returns an empty list.
     *
     * @param property The property.
     * @return The list of ranges associated to {@code property}.
     */
    public List<R> getCutRanges(Property property) {
        Checks.isNotNull(property, "property");
        final CutPointBuffer<D, V, R> buffer = buffers.get(property);
        if (buffer == null) {
            return Collections.emptyList();
        } else {
            return buffer.getCutRanges();
        }
    }

    public List<R> getEquivalence(Property property,
                                  SItem item) {
        final CutPointBuffer<D, V, R> buffer = buffers.get(property);
        if (buffer == null) {
            throw new IllegalArgumentException("Unknown property " + property);
        } else {
            return buffer.getEquivalence(item);
        }
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        domain.print(out, level);

        indent(out, level);
        out.println("Buffers " + buffers.size());

        for (final Property property : IterableUtils.toSortedList(getProperties(), DItem.COMPARATOR)) {
            indent(out, level + 1);
            out.println(property);
            buffers.get(property).print(out, level + 2);
        }
    }
}