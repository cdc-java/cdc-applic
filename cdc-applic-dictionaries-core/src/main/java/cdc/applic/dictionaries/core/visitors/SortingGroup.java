package cdc.applic.dictionaries.core.visitors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.Ordered;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractSetNode;
import cdc.applic.expressions.ast.AbstractValueNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.AbstractAnalyzer;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.Named;
import cdc.util.lang.Checks;
import cdc.util.lang.CollectionUtils;

/**
 * A SortGroup is a node representation that can be used to sort nodes.
 * <p>
 * TODO: handle negation
 *
 * @author Damien Carbonne
 */
public class SortingGroup {
    private static final Comparator<Object> ORDINAL_NAME_COMPARATOR =
            Comparator.comparing(SortingGroup::getOrdinal)
                      .thenComparing(SortingGroup::getName);

    /**
     * A Comparator of SortGroups that compares lexicographically NamedDItems (using Ordinal then Name),
     * and then compares associated sets.
     */
    public static final Comparator<SortingGroup> COMPARATOR = (sg1,
                                                               sg2) -> {
        if (sg1 == sg2) {
            return 0;
        }
        if (sg1 == null || sg2 == null) {
            return sg1 == null ? -1 : 1;
        }
        // Comparator of 2 NamedDItems (ordinal, name, set)
        final Comparator<Object> cmp = (i1,
                                        i2) -> {
            // First compare ordinal and name
            final int cmpOrdinalName = ORDINAL_NAME_COMPARATOR.compare(i1, i2);
            if (cmpOrdinalName == 0
                    && i1 instanceof final Property p1) {
                // Then compare sets
                final SItemSet set1 = sg1.map.get(i1);
                final SItemSet set2 = sg2.map.get(i2);
                return Property.compare(p1, set1.getChecked(), set2.getChecked());
            } else {
                return cmpOrdinalName;
            }
        };
        // Lexicographic comparison of NamedDItems (ordinal + name + set)
        return CollectionUtils.compareLexicographic(sg1.items, sg2.items, cmp);
    };

    /** Items that make the group: {@link Property}, {@link Alias} or {@link Name}. */
    private final List<Object> items = new ArrayList<>();

    /** Sets associated to Properties. */
    private final Map<Property, SItemSet> map = new HashMap<>();

    protected void add(Property property,
                       SItemSet set) {
        Checks.isNotNull(property, "property");
        Checks.isNotNull(set, "set");

        if (map.containsKey(property)) {
            map.put(property, map.get(property).union(set));
        } else {
            map.put(property, set);
            items.add(property);
            Collections.sort(items, ORDINAL_NAME_COMPARATOR);
        }
    }

    protected void add(Property property,
                       Value value) {
        Checks.isNotNull(property, "property");
        Checks.isNotNull(value, "value");

        add(property, SItemSetUtils.createBest(value));
    }

    protected void add(Alias alias) {
        Checks.isNotNull(alias, "alias");

        if (!items.contains(alias)) {
            items.add(alias);
            Collections.sort(items, ORDINAL_NAME_COMPARATOR);
        }
    }

    protected void add(Name name) {
        Checks.isNotNull(name, "name");

        if (!items.contains(name)) {
            items.add(name);
            Collections.sort(items, ORDINAL_NAME_COMPARATOR);
        }
    }

    private static int getOrdinal(Object object) {
        if (object instanceof final Ordered o) {
            return o.getOrdinal();
        } else {
            return Integer.MIN_VALUE;
        }
    }

    private static Name getName(Object object) {
        if (object instanceof final Named n) {
            return n.getName();
        } else {
            return (Name) object;
        }
    }

    /**
     * Creates a SortGroup corresponding to a {@link Node}.
     *
     * @param node The node.
     * @param dictionary The dictionary.
     * @return A SortGroup corresponding to {@code node}.
     */
    public static SortingGroup build(Node node,
                                     Dictionary dictionary) {
        final Builder builder = new Builder(dictionary);
        node.accept(builder);
        return builder.group;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append('[');
        for (final Object item : items) {
            builder.append('[');
            if (item instanceof final Property property) {
                builder.append("P ");
                builder.append(property.getName());
                builder.append(' ');
                builder.append(map.get(property));
            } else if (item instanceof final Alias alias) {
                builder.append("A ");
                builder.append(alias.getName());
            } else {
                builder.append("N ");
                final Name name = (Name) item;
                builder.append(name);
            }
            builder.append(']');
        }
        builder.append(']');
        return builder.toString();
    }

    private static class Builder extends AbstractAnalyzer {
        private final Dictionary dictionary;
        protected final SortingGroup group = new SortingGroup();

        public Builder(Dictionary dictionary) {
            this.dictionary = dictionary;
        }

        @Override
        public Void visitLeaf(AbstractLeafNode node) {
            if (node instanceof final AbstractSetNode n) {
                group.add(dictionary.getRegistry().getProperty(n.getName()), n.getSet());
            } else if (node instanceof final AbstractValueNode n) {
                group.add(dictionary.getRegistry().getProperty(n.getName()), n.getValue());
            } else if (node instanceof final RefNode n) {
                if (dictionary.getRegistry().hasAlias(n.getName())) {
                    group.add(dictionary.getRegistry().getAlias(n.getName()));
                } else {
                    // Boolean property
                    group.add(dictionary.getRegistry().getProperty(n.getName()), BooleanValue.TRUE);
                }
            } else if (node instanceof FalseNode) {
                group.add(FalseNode.FALSE);
            } else if (node instanceof TrueNode) {
                group.add(TrueNode.TRUE);
            }
            return null;
        }
    }
}