package cdc.applic.dictionaries.core.visitors;

import java.util.HashSet;
import java.util.Set;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.visitors.AbstractCollector;
import cdc.applic.expressions.literals.Name;

/**
 * A Collector that collects {@link Name}s of all {@link Alias}es that are directly used in a node tree.
 * <ul>
 * <li>{@link RefNode}s that reference an {@link Alias}: Name of the RefNode must be declared as an Alias name in the Registry.
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class CollectAliasNames extends AbstractCollector<Name> {
    private final Dictionary dictionary;

    private CollectAliasNames(Dictionary dictionary) {
        super(new HashSet<>());
        this.dictionary = dictionary;
    }

    public static Set<Name> collect(Node node,
                                    Dictionary dictionary) {
        final CollectAliasNames collector = new CollectAliasNames(dictionary);
        node.accept(collector);
        return collector.getSet();
    }

    @Override
    public Void visitLeaf(AbstractLeafNode node) {
        if (node instanceof final RefNode n && dictionary.getRegistry().hasAlias(n.getName())) {
            add(n.getName());
        }
        return null;
    }
}