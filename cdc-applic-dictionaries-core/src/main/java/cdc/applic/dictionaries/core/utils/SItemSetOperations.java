package cdc.applic.dictionaries.core.utils;

import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.RealType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.IllegalOperationException;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.CheckedSet;
import cdc.applic.expressions.content.IntegerSItem;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.RealSItem;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.parsing.ComparisonOperator;
import cdc.graphs.PartialOrderPosition;
import cdc.util.lang.Checks;

/**
 * Utilities related to {@link SItemSet}s.
 * <p>
 * This includes:
 * <ul>
 * <li>Full sets of a type.
 * <li>Complement of an item (simple value or range).
 * <li>Complement of a set.
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class SItemSetOperations {
    private SItemSetOperations() {
    }

    public static boolean supportsAllOperations(Type type) {
        Checks.isNotNull(type, "type");
        return !(type instanceof PatternType);
    }

    /**
     * @return The boolean full set.
     */
    public static BooleanSet booleanFullSet() {
        return BooleanSet.FALSE_TRUE;
    }

    /**
     * Returns the full set of an enumerated type.
     *
     * @param type The type.
     * @return The full set of {@code type}.
     */
    public static StringSet enumeratedFullSet(EnumeratedType type) {
        return type.getDomain();
    }

    /**
     * Returns the full set of an integer type.
     *
     * @param type The type.
     * @return The full set of {@code type}.
     */
    public static IntegerSet integerFullSet(IntegerType type) {
        return type.getDomain();
    }

    /**
     * Returns the full set of a real type.
     *
     * @param type The type.
     * @return The full set of {@code type}.
     */
    public static RealSet realFullSet(RealType type) {
        return type.getDomain();
    }

    /**
     * Returns the full set of a type.
     *
     * @param type The type.
     * @return The full set of {@code type}.
     * @throws IllegalOperationException When this operation is not supported.
     */
    public static SItemSet fullSet(Type type) {
        if (type instanceof BooleanType) {
            return booleanFullSet();
        } else if (type instanceof final EnumeratedType t) {
            return enumeratedFullSet(t);
        } else if (type instanceof final IntegerType t) {
            return integerFullSet(t);
        } else if (type instanceof final RealType t) {
            return realFullSet(t);
        } else {
            throw new IllegalOperationException("Can not compute full set of " + type);
        }
    }

    /**
     * Returns the complement of a boolean value.
     *
     * @param value The value.
     * @return The complement of {@code value}.
     */
    public static BooleanSet booleanComplement(BooleanValue value) {
        return BooleanSet.of(value.negate());
    }

    /**
     * Returns the complement of an enumerated value.
     *
     * @param value The value.
     * @param type The enumerated type.
     * @return The complement of {@code value} in {@code type}.
     */
    public static StringSet enumeratedComplement(StringValue value,
                                                 EnumeratedType type) {
        return enumeratedFullSet(type).remove(value);
    }

    /**
     * Returns the complement of an integer item (value or range).
     *
     * @param item The item.
     * @param type The integer type.
     * @return The complement of {@code item} in {@code type}.
     */
    public static IntegerSet integerComplement(IntegerSItem item,
                                               IntegerType type) {
        return integerFullSet(type).remove(item);
    }

    /**
     * Returns the complement of a real item (value or range).
     *
     * @param item The item.
     * @param type The real type.
     * @return The complement of {@code item} in {@code type}.
     */
    public static RealSet realComplement(RealSItem item,
                                         RealType type) {
        return realFullSet(type).remove(item);
    }

    /**
     * Returns the complement of a set item (simple value or range).
     *
     * @param item The item.
     * @param type The type.
     * @return The complement of {@code item} in {@code type}.
     * @throws IllegalOperationException When this operation is not supported.
     */
    public static SItemSet complement(SItem item,
                                      Type type) {
        final Class<? extends CheckedSet<?, ?>> cls = item.getCheckedSetClass();
        if (StringSet.class.equals(cls)) {
            if (type instanceof final EnumeratedType t) {
                return enumeratedComplement((StringValue) item, t);
            } else {
                throw new IllegalOperationException("Complement of patterns not supported for " + type);
            }
        } else if (IntegerSet.class.equals(cls)) {
            return integerComplement((IntegerSItem) item, (IntegerType) type);
        } else if (BooleanSet.class.equals(cls)) {
            return booleanComplement((BooleanValue) item);
        } else {
            // It must be a real
            return realComplement((RealSItem) item, (RealType) type);
        }
    }

    /**
     * Returns the complement of a boolean set.
     *
     * @param set The set.
     * @return The complement of {@code set}.
     */
    public static BooleanSet booleanComplement(BooleanSet set) {
        return booleanFullSet().remove(set);
    }

    /**
     * Returns the complement of an enumerated set.
     *
     * @param set The set.
     * @param type The enumerated type.
     * @return The complement of {@code set} in {@code type}.
     */
    public static StringSet enumeratedComplement(StringSet set,
                                                 EnumeratedType type) {
        return enumeratedFullSet(type).remove(set);
    }

    /**
     * Returns the complement of an integer set.
     *
     * @param set The set.
     * @param type The integer type.
     * @return The complement of {@code set} in {@code type}.
     */
    public static IntegerSet integerComplement(IntegerSet set,
                                               IntegerType type) {
        return integerFullSet(type).remove(set);
    }

    /**
     * Returns the complement of a real set.
     *
     * @param set The set.
     * @param type The real type.
     * @return The complement of {@code set} in {@code type}.
     */
    public static RealSet realComplement(RealSet set,
                                         RealType type) {
        return realFullSet(type).remove(set);
    }

    /**
     * Returns the complement of a set.
     *
     * @param set The set.
     * @param type The type. It must be compliant with {@code set}.
     * @return The complement of {@code set} in {@code type}.
     * @throws IllegalOperationException When this operation is not supported.
     */
    public static SItemSet complement(SItemSet set,
                                      Type type) {
        Checks.isNotNull(set, "set");
        Checks.isNotNull(type, "type");

        if (set.isEmpty()) {
            return fullSet(type);
        } else {
            final SItemSet checked = set.getChecked();
            final Class<? extends SItemSet> cls = checked.getClass();

            if (StringSet.class.equals(cls)) {
                if (type instanceof final EnumeratedType t) {
                    return enumeratedComplement((StringSet) checked, t);
                } else {
                    throw new IllegalOperationException("Complement of patterns not supported");
                }
            } else if (IntegerSet.class.equals(cls)) {
                return integerComplement((IntegerSet) checked, (IntegerType) type);
            } else if (BooleanSet.class.equals(cls)) {
                return booleanComplement((BooleanSet) checked);
            } else {
                return realComplement((RealSet) checked, (RealType) type);
            }
        }
    }

    /**
     * Returns {@code true} if a boolean set if full.
     *
     * @param set The set.
     * @return {@code true} if {@code set} if full.
     */
    public static boolean isFullSet(BooleanSet set) {
        return set.isFull();
    }

    /**
     * Returns {@code true} if an enumerated set if full.
     *
     * @param set The set.
     * @param type The type.
     * @return {@code true} if {@code set} if full.
     */
    public static boolean isFullSet(StringSet set,
                                    EnumeratedType type) {
        return set.getItems().size() == type.getValues().size();
    }

    /**
     * Returns {@code true} if an integer set if full.
     *
     * @param set The set.
     * @param type The type.
     * @return {@code true} if {@code set} if full.
     */
    public static boolean isFullSet(IntegerSet set,
                                    IntegerType type) {
        return complement(set, type).isEmpty();
    }

    /**
     * Returns {@code true} if a real set if full.
     *
     * @param set The set.
     * @param type The type.
     * @return {@code true} if {@code set} if full.
     */
    public static boolean isFullSet(RealSet set,
                                    RealType type) {
        return complement(set, type).isEmpty();
    }

    /**
     * Returns {@code true} if a set if full.
     *
     * @param set The set.
     * @param type The type. It must be compliant with {@code set}.
     * @return {@code true} if {@code set} if full.
     * @throws IllegalOperationException When this operation is not supported.
     */
    public static boolean isFullSet(SItemSet set,
                                    Type type) {
        Checks.isNotNull(set, "set");
        Checks.isNotNull(type, "type");

        final Class<? extends SItemSet> cls = set.getClass();
        if (StringSet.class.equals(cls)) {
            if (type instanceof final EnumeratedType t) {
                return isFullSet((StringSet) set, t);
            } else {
                throw new IllegalOperationException("isFullSet of patterns not supported");
            }
        } else if (IntegerSet.class.equals(cls)) {
            return isFullSet((IntegerSet) set, (IntegerType) type);
        } else if (BooleanSet.class.equals(cls)) {
            return isFullSet((BooleanSet) set);
        } else {
            return isFullSet((RealSet) set, (RealType) type);
        }
    }

    public static boolean isFullSet(StringValue value,
                                    EnumeratedType type) {
        return isFullSet(StringSet.of(value), type);
    }

    public static boolean isFullSet(IntegerValue value,
                                    IntegerType type) {
        return isFullSet(IntegerSet.of(value), type);
    }

    public static boolean isFullSet(RealValue value,
                                    RealType type) {
        return isFullSet(RealSet.of(value), type);
    }

    /**
     * Returns {@code true} if a singleton is a full set.
     *
     * @param value The singleton value.
     * @param type The type. It must be compliant with {@code value}.
     * @return {@code true} if {{@code value}} singleton is a full set.
     * @throws IllegalOperationException When this operation is not supported.
     */
    public static boolean isFullSet(Value value,
                                    Type type) {
        if (value instanceof final StringValue v) {
            if (type instanceof final EnumeratedType t) {
                return isFullSet(v, t);
            } else {
                throw new IllegalOperationException("isFullSet of patterns is not supported");
            }
        } else if (value instanceof final IntegerValue v) {
            return isFullSet(v, (IntegerType) type);
        } else if (value instanceof BooleanValue) {
            return false;
        } else {
            // It must be a real set
            return isFullSet((RealValue) value, (RealType) type);
        }
    }

    /**
     * Converts a &pi; {@code operator} &upsilon; into a set.
     *
     * @param value The value.
     * @param type The type.
     * @param operator The comparison operator.
     * @return The set corresponding to {@code operator value}.
     * @throws IllegalArgumentException When {@code value}, {@code type} or {@code operator} is {@code null},<br>
     *             or when {@code value} is not compliant with {@code type},<br>
     *             or when such computation is not supported.
     */
    public static SItemSet toSet(Value value,
                                 Type type,
                                 ComparisonOperator operator) {
        Checks.isNotNull(value, "value");
        Checks.isNotNull(type, "type");
        Checks.isNotNull(operator, "operator");
        Checks.isTrue(type.isCompliant(value), "Value '{}' is not compliant with {}", value, type.getName());

        if (type instanceof final BooleanType t && value instanceof final BooleanValue v) {
            return toSet(v, t, operator);
        } else if (type instanceof final IntegerType t && value instanceof final IntegerValue v) {
            return toSet(v, t, operator);
        } else if (type instanceof final RealType t && value instanceof final RealValue v) {
            return toSet(v, t, operator);
        } else if (type instanceof final EnumeratedType t && value instanceof final StringValue v) {
            return toSet(v, t, operator);
        } else {
            throw new IllegalArgumentException("Cannot convert comparison to set for type " + type.getName());
        }
    }

    /**
     * Converts a &pi; {@code operator} &upsilon; into a set.
     *
     * @param value The boolean value.
     * @param type The boolean type.
     * @param operator The comparison operator.
     * @return The set corresponding to {@code operator value}.
     * @throws IllegalArgumentException When {@code value}, {@code type} or {@code operator} is {@code null}.
     */
    public static BooleanSet toSet(BooleanValue value,
                                   BooleanType type,
                                   ComparisonOperator operator) {
        Checks.isNotNull(value, "value");
        Checks.isNotNull(type, "type");
        Checks.isNotNull(operator, "operator");

        BooleanSet set = BooleanSet.EMPTY;

        if (operator.includesEqual()) {
            set = set.union(type.toSet(value, PartialOrderPosition.EQUAL));
        }
        if (operator.includesLess()) {
            set = set.union(type.toSet(value, PartialOrderPosition.LESS_THAN));
        }
        if (operator.includesGreater()) {
            set = set.union(type.toSet(value, PartialOrderPosition.GREATER_THAN));
        }
        if (operator.includesUnrelated()) {
            set = set.union(type.toSet(value, PartialOrderPosition.UNRELATED));
        }
        return set;
    }

    /**
     * Converts a &pi; {@code operator} &upsilon; into a set.
     *
     * @param value The integer value.
     * @param type The integer type.
     * @param operator The comparison operator.
     * @return The set corresponding to {@code operator value}.
     * @throws IllegalArgumentException When {@code value}, {@code type} or {@code operator} is {@code null}.
     */
    public static IntegerSet toSet(IntegerValue value,
                                   IntegerType type,
                                   ComparisonOperator operator) {
        Checks.isNotNull(value, "value");
        Checks.isNotNull(type, "type");
        Checks.isNotNull(operator, "operator");
        IntegerSet set = IntegerSet.EMPTY;

        if (operator.includesEqual()) {
            set = set.union(type.toSet(value, PartialOrderPosition.EQUAL));
        }
        if (operator.includesLess()) {
            set = set.union(type.toSet(value, PartialOrderPosition.LESS_THAN));
        }
        if (operator.includesGreater()) {
            set = set.union(type.toSet(value, PartialOrderPosition.GREATER_THAN));
        }
        if (operator.includesUnrelated()) {
            set = set.union(type.toSet(value, PartialOrderPosition.UNRELATED));
        }
        return set;
    }

    /**
     * Converts a &pi; {@code operator} &upsilon; into a set.
     *
     * @param value The real value.
     * @param type The real type.
     * @param operator The comparison operator.
     * @return The set corresponding to {@code operator value}.
     * @throws IllegalArgumentException When {@code value}, {@code type} or {@code operator} is {@code null}.
     */
    public static RealSet toSet(RealValue value,
                                RealType type,
                                ComparisonOperator operator) {
        Checks.isNotNull(value, "value");
        Checks.isNotNull(type, "type");
        Checks.isNotNull(operator, "operator");
        RealSet set = RealSet.EMPTY;

        if (operator.includesEqual()) {
            set = set.union(type.toSet(value, PartialOrderPosition.EQUAL));
        }
        if (operator.includesLess()) {
            set = set.union(type.toSet(value, PartialOrderPosition.LESS_THAN));
        }
        if (operator.includesGreater()) {
            set = set.union(type.toSet(value, PartialOrderPosition.GREATER_THAN));
        }
        if (operator.includesUnrelated()) {
            set = set.union(type.toSet(value, PartialOrderPosition.UNRELATED));
        }
        return set;
    }

    /**
     * Converts a &pi; {@code operator} &upsilon; into a set.
     *
     * @param value The string value.
     * @param type The enumerated type.
     * @param operator The comparison operator.
     * @return The set corresponding to {@code operator value}.
     * @throws IllegalArgumentException When {@code value}, {@code type} or {@code operator} is {@code null}.
     */
    public static StringSet toSet(StringValue value,
                                  EnumeratedType type,
                                  ComparisonOperator operator) {
        Checks.isNotNull(value, "value");
        Checks.isNotNull(type, "type");
        Checks.isNotNull(operator, "operator");
        StringSet set = StringSet.EMPTY;

        if (operator.includesEqual()) {
            set = set.union(type.toSet(value, PartialOrderPosition.EQUAL));
        }
        if (operator.includesLess()) {
            set = set.union(type.toSet(value, PartialOrderPosition.LESS_THAN));
        }
        if (operator.includesGreater()) {
            set = set.union(type.toSet(value, PartialOrderPosition.GREATER_THAN));
        }
        if (operator.includesUnrelated()) {
            set = set.union(type.toSet(value, PartialOrderPosition.UNRELATED));
        }
        return set;
    }

    /**
     * Returns {@code true} if the set designated by &pi; {@code operator} &upsilon; is full.
     *
     * @param value The value.
     * @param type The type.
     * @param operator The comparison operator.
     * @return {@code true} if the set designated by &pi; {@code operator} &upsilon; is full.
     * @throws IllegalArgumentException When {@code value}, {@code type} or {@code operator} is {@code null},<br>
     *             or when {@code value} is not compliant with {@code type},<br>
     *             or when such computation is not supported.
     */
    public static boolean isFullSet(Value value,
                                    Type type,
                                    ComparisonOperator operator) {
        Checks.isNotNull(value, "value");
        Checks.isNotNull(type, "type");
        Checks.isNotNull(operator, "operator");
        Checks.isTrue(type.isCompliant(value), "Value '{}' is not compliant with {}", value, type.getName());

        if (operator.includesEqual()) {
            // Reference value is included, do more computations
            final SItemSet set = toSet(value, type, operator);
            return isFullSet(set, type);
        } else {
            // Reference value is excluded, so it can not be full
            return false;
        }
    }

    /**
     * Returns {@code true} if the set designated by &pi; {@code operator} &upsilon; is empty.
     *
     * @param value The value.
     * @param type The type.
     * @param operator The comparison operator.
     * @return {@code true} if the set designated by &pi; {@code operator} &upsilon; is empty.
     * @throws IllegalArgumentException When {@code value}, {@code type} or {@code operator} is {@code null},<br>
     *             or when {@code value} is not compliant with {@code type},<br>
     *             or when such computation is not supported.
     */
    public static boolean isEmptySet(Value value,
                                     Type type,
                                     ComparisonOperator operator) {
        Checks.isNotNull(value, "value");
        Checks.isNotNull(type, "type");
        Checks.isNotNull(operator, "operator");
        Checks.isTrue(type.isCompliant(value), "Value '{}' is not compliant with {}", value, type.getName());

        if (operator.includesEqual()) {
            // Reference value is included, so it can not be empty.
            return false;
        } else {
            // Reference value is excluded, do more computations
            final SItemSet set = toSet(value, type, operator);
            return set.isEmpty();
        }
    }
}