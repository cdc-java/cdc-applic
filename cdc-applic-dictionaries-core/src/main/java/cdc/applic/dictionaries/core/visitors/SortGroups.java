package cdc.applic.dictionaries.core.visitors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.util.lang.Checks;

public final class SortGroups extends AbstractConverter {
    private final Dictionary dictionary;

    private SortGroups(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public static Node execute(Node node,
                               Dictionary dictionary) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(dictionary, "dictionary");
        final SortGroups visitor = new SortGroups(dictionary);
        return node.accept(visitor);
    }

    @Override
    public Node visitBinary(AbstractBinaryNode node) {
        final Node alpha = node.getAlpha().accept(this);
        final Node beta = node.getBeta().accept(this);
        final SortingGroup alphaSortGroup = SortingGroup.build(alpha, dictionary);
        final SortingGroup betaSortGroup = SortingGroup.build(beta, dictionary);
        final int cmp = SortingGroup.COMPARATOR.compare(alphaSortGroup, betaSortGroup);

        if (cmp <= 0) {
            return node.create(alpha, beta);
        } else {
            return node.create(beta, alpha);
        }
    }

    @Override
    public Node visitNary(AbstractNaryNode node) {
        final List<Node> processedChildren = new ArrayList<>();
        final Map<Node, SortingGroup> sortGroups = new HashMap<>();
        for (final Node child : node.getChildren()) {
            final Node processedChild = child.accept(this);
            processedChildren.add(processedChild);
            sortGroups.put(processedChild, SortingGroup.build(processedChild, dictionary));
        }

        // Sort processed children
        final Comparator<Node> comparator = (n1,
                                             n2) -> {
            final SortingGroup sg1 = sortGroups.get(n1);
            final SortingGroup sg2 = sortGroups.get(n2);
            return SortingGroup.COMPARATOR.compare(sg1, sg2);
        };
        Collections.sort(processedChildren, comparator);

        // Create a new NaryNode using sorted children
        return node.create(processedChildren);
    }
}