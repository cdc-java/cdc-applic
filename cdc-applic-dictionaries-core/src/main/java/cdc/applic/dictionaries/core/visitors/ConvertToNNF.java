package cdc.applic.dictionaries.core.visitors;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.ConvertEquivalence;
import cdc.applic.expressions.ast.visitors.ConvertImplication;
import cdc.applic.expressions.ast.visitors.ConvertXor;
import cdc.util.lang.Checks;

/**
 * Converts an AST (node) to an equivalent Negative Normal Form.
 * <p>
 * The resulting tree has only <em>or</em>, <em>and</em>, <em>not</em> and terminal positive nodes.
 * Nots are moved inwards (if present, then just over terminal nodes).
 * There are no more <em>implication</em>, <em>equivalence</em>, <em>xor</em> and <em>negative</em> terminal nodes.
 * <p>
 * <b>Note</b>: This is an adaptation of AIMA Java implementation.
 *
 * @author Damien Carbonne
 */
public final class ConvertToNNF {
    private ConvertToNNF() {
    }

    public static Node execute(Node node,
                               MoveNotInwards.Variant variant,
                               Dictionary dictionary,
                               ReserveStrategy reserveStrategy) {
        Checks.isTrue(dictionary != null || !variant.requiresDictionary(),
                      "Can not use " + variant + " without dictionary");

        Node n = node;
        n = ConvertEquivalence.execute(n);
        n = ConvertImplication.execute(n);
        n = ConvertXor.execute(n);
        n = MoveNotInwards.execute(n, variant, dictionary, reserveStrategy);
        return n;
    }

    public static Node execute(Node node,
                               MoveNotInwards.Variant variant) {
        return execute(node, variant, null, null);
    }
}