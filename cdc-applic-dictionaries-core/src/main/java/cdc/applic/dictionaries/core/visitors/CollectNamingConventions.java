package cdc.applic.dictionaries.core.visitors;

import java.util.HashSet;
import java.util.Set;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.DictionaryUtils;
import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.PropertyNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.SetNode;
import cdc.applic.expressions.ast.ValueNode;
import cdc.applic.expressions.ast.visitors.AbstractCollector;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;

/**
 * A collector that collects NamingConventions potentially used in an expression.
 * <p>
 * If a name or value is valid for several conventions, result may be imprecise.
 * <p>
 * One could imagine a more precise algorithm that tries to reduce ambiguities.
 * Is it worth the effort?<br>
 * It would probably be much expensive.
 *
 * @author Damien Carbonne
 */
public final class CollectNamingConventions extends AbstractCollector<NamingConvention> {
    private final Dictionary dictionary;

    private CollectNamingConventions(Dictionary dictionary) {
        super(new HashSet<>());
        this.dictionary = dictionary;
    }

    public static Set<NamingConvention> collect(Node node,
                                                Dictionary dictionary) {
        final CollectNamingConventions collector = new CollectNamingConventions(dictionary);
        node.accept(collector);
        return collector.getSet();
    }

    @Override
    public Void visitLeaf(AbstractLeafNode node) {
        if (node instanceof final RefNode refNode) {
            // Alias name and boolean property name synonyms
            final Name name = refNode.getName();
            if (dictionary.getRegistry().hasAlias(name)
                    || DictionaryUtils.isAvailableBooleanProperty(name, dictionary)) {
                addAll(dictionary.getRegistry().getItemNameNamingConventions(name));
            }
        } else if (node instanceof final PropertyNode propertyNode) {
            // Property name synonyms
            final Name name = propertyNode.getName();
            if (dictionary.getRegistry().hasProperty(name)) {
                addAll(dictionary.getRegistry().getItemNameNamingConventions(name));
            }

            // If the property type is enumerated, its values could have synonyms.
            final Property property = dictionary.getProperty(name);
            final Type type = property.getType();
            if (type instanceof final EnumeratedType enumeratedType) {
                // Property values synonyms
                if (propertyNode instanceof final ValueNode valueNode) {
                    addAll(enumeratedType.getNamingConventions(valueNode.getValue().getNonEscapedLiteral()));
                } else if (propertyNode instanceof final SetNode setNode) {
                    for (final SItem item : setNode.getSet().getItems()) {
                        if (item instanceof final Value value) {
                            addAll(enumeratedType.getNamingConventions(value.getNonEscapedLiteral()));
                        }
                        // No enum range at the moment
                    }
                }
            }
        }
        return null;
    }
}