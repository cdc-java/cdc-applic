package cdc.applic.dictionaries.core.visitors;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.DictionaryUtils;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractValueNode;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.content.BooleanValue;
import cdc.util.lang.Checks;

public final class NormalizeBooleanProperties extends AbstractConverter {
    private final Dictionary dictionary;

    private NormalizeBooleanProperties(Dictionary dictionary) {
        Checks.isNotNull(dictionary, "dictionary");
        this.dictionary = dictionary;
    }

    public static Node execute(Node node,
                               Dictionary dictionary) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(dictionary, "dictionary");
        final NormalizeBooleanProperties visitor = new NormalizeBooleanProperties(dictionary);
        return node.accept(visitor);
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (node instanceof final AbstractValueNode n) {
            if (DictionaryUtils.isAvailableBooleanProperty(n.getName(), dictionary)) {
                final RefNode ref = new RefNode(n.getName());
                final boolean isTrue = BooleanValue.TRUE.equals(n.getValue());
                if ((node instanceof EqualNode) == isTrue) {
                    return ref;
                } else {
                    return new NotNode(ref);
                }
            } else {
                return node;
            }
        } else {
            return node;
        }
    }
}