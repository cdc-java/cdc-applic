package cdc.applic.dictionaries.core.visitors;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.DictionaryUtils;
import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.core.utils.SItemSetOperations;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.IllegalOperationException;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractPropertyNode;
import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.AbstractValueNode;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.LessNode;
import cdc.applic.expressions.ast.NeitherGreaterNorEqualNode;
import cdc.applic.expressions.ast.NeitherLessNorEqualNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotEqualNode;
import cdc.applic.expressions.ast.NotGreaterNode;
import cdc.applic.expressions.ast.NotInNode;
import cdc.applic.expressions.ast.NotLessNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.content.BooleanValue;
import cdc.util.lang.Checks;

/**
 * Utility class used to replace negative leaves, if possible, by a positive equivalent, using those rewriting rules:
 * <ul>
 * <li>&not;(&pi; &ne; &upsilon;) &equiv; &pi; = &upsilon;
 * <li>&not;(&pi; &notin; &omega;) &equiv; &pi; &isin; &omega;
 * <li>&not;(&pi; &nlt; &upsilon;) &equiv; &pi; &lt; &upsilon;
 * <li>&not;(&pi; &nle; &upsilon;) &equiv; &pi; &le; &upsilon;
 * <li>&not;(&pi; &ngt; &upsilon;) &equiv; &pi; &gt; &upsilon;
 * <li>&not;(&pi; &nge; &upsilon;) &equiv; &pi; &ge; &upsilon;
 * <li>&not; &perp; &equiv; &top;
 * <li>&not; &top; &equiv; &perp;
 * <li>&not;(&pi; = &upsilon;) &equiv; &pi; &isin; &Omega;(&pi;) - &upsilon;
 * <li>&not;(&pi; &isin; &omega;) &equiv; &pi; &isin; &Omega;(&pi;) - &omega;
 * <li>&pi; &ne; &upsilon; &equiv; &pi; &isin; &Omega;(&pi;) - &upsilon;
 * <li>&pi; &notin; &omega; &equiv; &pi; &isin; &Omega;(&pi;) - &omega;
 * <li>&not;&pi; &equiv; &pi; = &perp; (When &pi; is a boolean property)
 * <li>&pi; &nlt; &upsilon; &equiv; &pi; &isin; &Omega;(&pi; &ge; &upsilon;) &cup; &Omega;(&pi; &lt;&gt; &upsilon;)
 * <li>&pi; &nle; &upsilon; &equiv; &pi; &isin; &Omega;(&pi; &gt; &upsilon;) &cup; &Omega;(&pi; &lt;&gt; &upsilon;)
 * <li>&pi; &ngt; &upsilon; &equiv; &pi; &isin; &Omega;(&pi; &le; &upsilon;) &cup; &Omega;(&pi; &lt;&gt; &upsilon;)
 * <li>&pi; &nge; &upsilon; &equiv; &pi; &isin; &Omega;(&pi; &lt; &upsilon;) &cup; &Omega;(&pi; &lt;&gt; &upsilon;)
 *
 * </ul>
 * <b>WARNING:</b> It does not eliminate all negations.
 * Using {@link MoveNotInwards} may help for this.<br>
 * <b>Note:</b> It does not eliminate nodes that can not be negated because of reserves
 * or impossibility to compute complement.
 *
 * @author Damien Carbonne
 */
public final class ConvertToPositiveLeaves extends AbstractConverter {
    private final Dictionary dictionary;
    private final Variant variant;
    private final ReserveStrategy reserveStrategy;

    private ConvertToPositiveLeaves(Dictionary dictionary,
                                    Variant variant,
                                    ReserveStrategy reserveStrategy) {
        Checks.isNotNull(dictionary, "dictionary");
        Checks.isNotNull(variant, "variant");
        Checks.isNotNull(reserveStrategy, "reserveStrategy");

        this.dictionary = dictionary;
        this.variant = variant;
        this.reserveStrategy = reserveStrategy;
    }

    public enum Variant {
        /**
         * When conversion to positive leaves is impossible, ignore conversion.
         */
        IGNORE_WHEN_IMPOSSIBLE,

        /**
         * When conversion to positive leaves is impossible, throw an exception.
         */
        THROW_WHEN_IMPOSSIBLE
    }

    public static Node execute(Node node,
                               Variant variant,
                               Dictionary dictionary,
                               ReserveStrategy reserveStrategy) {
        final ConvertToPositiveLeaves converter =
                new ConvertToPositiveLeaves(dictionary, variant, reserveStrategy);
        return node.accept(converter);
    }

    private boolean supportsComplement(Type type) {
        return SItemSetOperations.supportsAllOperations(type)
                && !reserveStrategy.hasReserve(type);
    }

    private Node visitComparisonUnary(AbstractUnaryNode node,
                                      AbstractValueNode negated) {
        final Property property = dictionary.getRegistry().getProperty(negated.getName());
        final Type type = property.getType();
        if (supportsComplement(type)) {
            return new InNode(negated.getName(),
                              SItemSetOperations.toSet(negated.getValue(), type, negated.getComparisonOperator().negate()));
        } else if (variant == Variant.THROW_WHEN_IMPOSSIBLE) {
            throw new IllegalOperationException("Can not replace comparison by set for" + type.getName());
        } else {
            return super.visitUnary(node);
        }
    }

    @Override
    public Node visitUnary(AbstractUnaryNode node) {
        final Node negated = node.getAlpha();
        if (negated instanceof AbstractLeafNode) {
            if (negated instanceof final AbstractPropertyNode n) {
                if (n.isNegative()) {
                    // !(p != v) <-> p = v
                    // !(p !< s) <-> p < s
                    // !(p !<= s) <-> p <= s
                    // !(p !> s) <-> p > s
                    // !(p !>= s) <-> p >= s
                    // !(p !<: s) <-> p <: s
                    return n.negate();
                } else if (negated instanceof EqualNode) {
                    // !(p = v) <-> p <: {v1, v2, ...} - v
                    final Property property = dictionary.getRegistry().getProperty(n.getName());
                    final Type type = property.getType();
                    if (supportsComplement(type)) {
                        return new InNode(n.getName(), SItemSetOperations.complement(((EqualNode) n).getValue(), type));
                    } else if (variant == Variant.THROW_WHEN_IMPOSSIBLE) {
                        throw new IllegalOperationException("Can not compute complement of " + type.getName());
                    } else {
                        return super.visitUnary(node);
                    }
                } else if (negated instanceof InNode) {
                    // !(p <: s) <-> p <: S - s
                    final Property property = dictionary.getRegistry().getProperty(n.getName());
                    final Type type = property.getType();
                    if (supportsComplement(type)) {
                        return new InNode(n.getName(), SItemSetOperations.complement(((InNode) n).getCheckedSet(), type));
                    } else if (variant == Variant.THROW_WHEN_IMPOSSIBLE) {
                        throw new IllegalOperationException("Can not compute complement of " + type.getName());
                    } else {
                        return super.visitUnary(node);
                    }
                } else {
                    return visitComparisonUnary(node, (LessNode) negated);
                }
            } else if (negated instanceof FalseNode) {
                // !false <-> true
                return TrueNode.INSTANCE;
            } else if (negated instanceof TrueNode) {
                // !true <-> false
                return FalseNode.INSTANCE;
            } else if (negated instanceof final RefNode n) {
                if (DictionaryUtils.isAvailableBooleanProperty(n.getName(), dictionary)) {
                    return new EqualNode(n.getName(), BooleanValue.FALSE);
                } else {
                    return super.visitUnary(node);
                }
            } else {
                // Other atom nodes can not be converted
                return super.visitUnary(node);
            }
        } else {
            return super.visitUnary(node);
        }
    }

    private Node visitComparisonLeaf(AbstractValueNode node) {
        final Property property = dictionary.getRegistry().getProperty(node.getName());
        final Type type = property.getType();
        if (supportsComplement(type)) {
            return new InNode(node.getName(), SItemSetOperations.toSet(node.getValue(), type, node.getComparisonOperator()));
        } else if (variant == Variant.THROW_WHEN_IMPOSSIBLE) {
            throw new IllegalOperationException("Can not replace comparison by set for" + type.getName());
        } else {
            return super.visitLeaf(node);
        }
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (node instanceof final NotEqualNode n) {
            // p != v <-> p <: {v1, v2, ...} - v
            final Property property = dictionary.getRegistry().getProperty(n.getName());
            final Type type = property.getType();
            if (supportsComplement(type)) {
                return new InNode(n.getName(), SItemSetOperations.complement(n.getValue(), type));
            } else if (variant == Variant.THROW_WHEN_IMPOSSIBLE) {
                throw new IllegalOperationException("Can not compute complement of " + type.getName());
            } else {
                return node;
            }
        } else if (node instanceof final NotInNode n) {
            // p !<: s <-> p <: S - s
            final Property property = dictionary.getRegistry().getProperty(n.getName());
            final Type type = property.getType();
            if (supportsComplement(type)) {
                return new InNode(n.getName(), SItemSetOperations.complement(n.getSet(), type));
            } else if (variant == Variant.THROW_WHEN_IMPOSSIBLE) {
                throw new IllegalOperationException("Can not compute complement of " + type.getName());
            } else {
                return node;
            }
        } else if (node instanceof NotLessNode
                || node instanceof NotGreaterNode
                || node instanceof NeitherLessNorEqualNode
                || node instanceof NeitherGreaterNorEqualNode) {
            return visitComparisonLeaf((AbstractValueNode) node);
        } else {
            return node;
        }
    }
}