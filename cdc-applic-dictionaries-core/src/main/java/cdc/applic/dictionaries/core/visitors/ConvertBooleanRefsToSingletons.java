package cdc.applic.dictionaries.core.visitors;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.DictionaryUtils;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.content.BooleanSet;
import cdc.util.lang.Checks;

public final class ConvertBooleanRefsToSingletons extends AbstractConverter {
    private final Dictionary dictionary;

    private ConvertBooleanRefsToSingletons(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public static Node execute(Node node,
                               Dictionary dictionary) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(dictionary, "dictionary");

        final ConvertBooleanRefsToSingletons converter = new ConvertBooleanRefsToSingletons(dictionary);
        return node.accept(converter);
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (node instanceof final RefNode n) {
            if (DictionaryUtils.isAvailableBooleanProperty(n.getName(), dictionary)) {
                return new InNode(n.getName(), BooleanSet.TRUE);
            } else {
                return node;
            }
        } else {
            return node;
        }
    }
}