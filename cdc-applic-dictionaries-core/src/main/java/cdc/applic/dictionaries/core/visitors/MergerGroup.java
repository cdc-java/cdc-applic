package cdc.applic.dictionaries.core.visitors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.Node;
import cdc.util.lang.Checks;
import cdc.util.lang.CollectionUtils;

/**
 * A Merger group is an ordered list of {@link MergerAtom}s.
 * <p>
 * It is a normalized representation of an {@link AndNode},
 * {@link NaryAndNode} or {@link AbstractLeafNode}.<br>
 * It can not contain 2 MergerAtoms that match the same Property or Alias.
 *
 * @author Damien Carbonne
 */
public final class MergerGroup {
    public static final MergerGroup EMPTY = new MergerGroup(Collections.emptySet());

    public static final Comparator<MergerGroup> COMPARATOR = (mg1,
                                                              mg2) -> {
        if (mg1 == mg2) {
            return 0;
        }
        // Check null
        if (mg1 == null || mg2 == null) {
            return mg1 == null ? -1 : 1;
        }
        return CollectionUtils.compareLexicographic(mg1.atoms, mg2.atoms, MergerAtom.COMPARATOR);
    };

    private final List<MergerAtom> atoms;

    private MergerGroup(Set<MergerAtom> atoms) {
        Checks.isNotNull(atoms, "atoms");

        final List<MergerAtom> sorted = new ArrayList<>(atoms);
        Collections.sort(sorted, MergerAtom.COMPARATOR);
        this.atoms = Collections.unmodifiableList(sorted);

        final Set<NamedDItem> items = new HashSet<>();
        for (final MergerAtom atom : atoms) {
            items.add(atom.getItem());
        }
        if (atoms.size() != items.size()) {
            throw new IllegalArgumentException("Duplicate items in " + atoms);
        }
    }

    private MergerGroup(MergerGroup group,
                        MergerAtom atom) {
        Checks.isNotNull(group, "group");
        Checks.isNotNull(atom, "atom");

        final List<MergerAtom> sorted = new ArrayList<>(group.atoms);
        sorted.add(atom);
        Collections.sort(sorted, MergerAtom.COMPARATOR);
        this.atoms = Collections.unmodifiableList(sorted);

        // if (atoms.size() != (group.atoms.size() + 1)) {
        // throw new IllegalArgumentException("Duplicate items in " + group + " " + atom);
        // }
    }

    /**
     * @return The number of MergerAtoms.
     */
    public int size() {
        return atoms.size();
    }

    /**
     * @return The ordered list of MergerAtoms.
     */
    public List<MergerAtom> getAtoms() {
        return atoms;
    }

    /**
     * Returns the intersection of this MergerGroup with another one.
     *
     * @param group The other MergerGroup.
     * @return The intersection of this MergerGroup with {@code group}.
     */
    public MergerGroup intersectionWith(MergerGroup group) {
        Checks.isNotNull(group, "group");
        final Set<MergerAtom> set = new HashSet<>(this.atoms);
        set.retainAll(group.atoms);
        if (set.isEmpty()) {
            return EMPTY;
        } else {
            return new MergerGroup(set);
        }
    }

    /**
     * Merges this MergerGroup with another one, if possible.
     * <ul>
     * <li>The 2 MergerGroups must have the same size.
     * <li>The 2 MergerGroups must share all their MergerAtoms except one.
     * <li>The 2 different MergerAtoms must match the same Property or Alias.
     * </ul>
     *
     * @param group The MergerGroup to merge.
     * @param bridge The intersection of this MergerGroup with {@code group}.
     * @return The merger of the MergerGroup with {@code group} or {@code null}.
     */
    public MergerGroup mergeWith(MergerGroup group,
                                 MergerGroup bridge) {
        final MergerAtom alpha = remove(bridge);
        final MergerAtom beta = group.remove(bridge);
        final MergerAtom merged = alpha == null ? null : alpha.mergeWith(beta);
        if (merged == null) {
            return null;
        } else {
            return new MergerGroup(bridge, merged);
        }
    }

    private MergerAtom remove(MergerGroup bridge) {
        final Set<MergerAtom> set = new HashSet<>(atoms);
        set.removeAll(bridge.atoms);
        if (set.size() != size() - bridge.size()) {
            throw new IllegalArgumentException("Invalid bridge " + bridge + " for " + this);
        } else if (set.size() == 1) {
            return set.iterator().next();
        } else {
            return null;
        }
    }

    /**
     * @return The conversion of this MergerGroup to a Node.
     */
    public Node toNode() {
        final List<Node> nodes = new ArrayList<>();
        for (final MergerAtom atom : atoms) {
            nodes.add(atom.toNode());
        }
        return NaryAndNode.createSimplestAnd(nodes);
    }

    /**
     * Creates a MergerGroup from a {@link Node}.
     * <p>
     * If {@code node} is an {@link AndNode} or {@link NaryAndNode}
     * and its children can all be converted to {@link MergerAtom}s,
     * a valid MergerGroup is returned.<br>
     * If {@code node} is an {@link AbstractLeafNode} that can be converted
     * to {@link MergerAtom}s, a valid MergerGroup is returned.<br>
     * Otherwise {@code null} is returned.
     *
     * @param node The node to convert.
     * @param dictionary The dictionary (used to retrieve Property and ALias).
     * @return A MergerGroup corresponding to {@code node} or {@code null}.
     * @throws IllegalArgumentException When {@code node} fulfills all requirements,
     *             but 2 atoms match the same Property or Alias.
     */
    public static MergerGroup build(Node node,
                                    Dictionary dictionary) {
        if (node instanceof final AndNode n) {
            final MergerAtom alphaAtom = MergerAtom.build(n.getAlpha(), dictionary);
            final MergerAtom betaAtom = MergerAtom.build(n.getBeta(), dictionary);
            if (alphaAtom != null && betaAtom != null) {
                final Set<MergerAtom> atoms = new HashSet<>();
                atoms.add(alphaAtom);
                atoms.add(betaAtom);
                return new MergerGroup(atoms);
            } else {
                return null;
            }
        } else if (node instanceof final NaryAndNode n) {
            final Set<MergerAtom> atoms = new HashSet<>();
            for (final Node child : n.getChildren()) {
                final MergerAtom atom = MergerAtom.build(child, dictionary);
                if (atom == null) {
                    return null;
                } else {
                    atoms.add(atom);
                }
            }
            return new MergerGroup(atoms);
        } else if (node instanceof AbstractLeafNode) {
            final MergerAtom atom = MergerAtom.build(node, dictionary);
            if (atom == null) {
                return null;
            } else {
                final Set<MergerAtom> atoms = new HashSet<>();
                atoms.add(atom);
                return new MergerGroup(atoms);
            }
        } else {
            return null;
        }
    }

    @Override
    public int hashCode() {
        return atoms.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof MergerGroup)) {
            return false;
        }
        final MergerGroup other = (MergerGroup) object;
        return this.atoms.equals(other.atoms);
    }

    @Override
    public String toString() {
        return atoms.toString();
    }
}