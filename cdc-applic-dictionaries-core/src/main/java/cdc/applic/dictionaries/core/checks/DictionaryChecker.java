package cdc.applic.dictionaries.core.checks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.DictionaryException;
import cdc.applic.dictionaries.Registry;
import cdc.applic.dictionaries.checks.DependencyCycleDictionaryIssue;
import cdc.applic.dictionaries.checks.DictionaryIssue;
import cdc.applic.dictionaries.checks.DuplicateNamesDictionaryIssue;
import cdc.applic.dictionaries.checks.EmptyDictionaryIssue;
import cdc.applic.dictionaries.checks.InvalidExpressionDictionaryIssue;
import cdc.applic.dictionaries.checks.InvalidTypeDictionaryIssue;
import cdc.applic.dictionaries.checks.NullExpressionDictionaryIssue;
import cdc.applic.dictionaries.checks.NullPropertyTypeDictionaryIssue;
import cdc.applic.dictionaries.checks.SemanticChecker;
import cdc.applic.dictionaries.core.utils.DictionaryGraph;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.DExpressed;
import cdc.applic.dictionaries.items.DItem;
import cdc.applic.dictionaries.items.LocalAssertion;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.Checker;
import cdc.applic.expressions.literals.Name;
import cdc.issues.Diagnosis;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;

/**
 * Class used to check the consistency of a {@link Dictionary}.
 * <p>
 * The following errors are detected:
 * <ul>
 * <li>Duplicate names of {@link Alias} or {@link Property}.
 * <li>{@link Property} without type.
 * <li>{@link Type} with empty definition.
 * <li>{@link DExpressed} with invalid {@link Expression}.
 * <li>Cycles in dependency graph of {@link DItem}s.
 * </ul>
 * Those errors are fatal.
 *
 * @author Damien Carbonne
 */
public class DictionaryChecker {
    private static final Logger LOGGER = LogManager.getLogger(DictionaryChecker.class);
    private final Dictionary dictionary;
    private final DictionaryGraph graph;

    public DictionaryChecker(Dictionary dictionary,
                             DictionaryGraph graph) {
        Checks.isNotNull(dictionary, "dictionary");

        this.dictionary = dictionary;
        this.graph = graph;
    }

    public DictionaryChecker(Dictionary dictionary) {
        this(dictionary, null);
    }

    private static String toString(Dictionary dictionary,
                                   boolean capital) {
        final StringBuilder builder = new StringBuilder();
        if (dictionary instanceof Registry) {
            builder.append(capital ? 'R' : 'r');
            builder.append("egistry ");
        } else {
            builder.append(capital ? 'P' : 'p');
            builder.append("olicy ");
        }
        builder.append(Checker.wrap(dictionary.getName()));
        return builder.toString();
    }

    /**
     * Detect issues in dictionary and adds them to a list of issues.
     *
     * @param issues The filled issues.
     */
    public void check(List<? super DictionaryIssue> issues) {
        checkNonEmpty(issues);
        checkDuplicateNames(issues);
        checkTypes(issues);
        // Check local assertion, not inherited ones
        checkPExpressed(dictionary.getAssertions(LocalAssertion.class), dictionary, issues);
        // Check aliases if dictionary is a registry.
        if (dictionary.getRegistry() == dictionary) {
            checkPExpressed(dictionary.getRegistry().getDeclaredAliases(), dictionary, issues);
        }
        checkGraph(issues);
    }

    /**
     * Detects issues and raises an exception if one is detected.
     *
     * @throws DictionaryException When a {@link DictionaryIssue} is detected.
     */
    public void check() {
        final List<ApplicIssue> issues = new ArrayList<>();
        check(issues);
        if (!issues.isEmpty()) {
            final Diagnosis<ApplicIssue> diagnosis = Diagnosis.of(issues);
            LOGGER.fatal("There are issues.");
            for (final ApplicIssue issue : diagnosis.getIssues()) {
                LOGGER.fatal("  {}", issue);
            }
            throw new DictionaryException("Invalid " + toString(dictionary, false) + ": " + diagnosis, diagnosis);
        }
    }

    private void checkDuplicateNames(List<? super DictionaryIssue> issues) {
        // Map from name to first item with that name
        final Map<Name, NamedDItem> first = new HashMap<>();
        // set of duplicate items
        final Set<NamedDItem> duplicateItems = new HashSet<>();
        final Set<Name> duplicateNames = new HashSet<>();
        for (final NamedDItem item : dictionary.getRegistry().getDeclaredItems()) {
            final Name name = item.getName();

            if (first.containsKey(name)) {
                // We may add the first item several time, it does not matter
                duplicateItems.add(first.get(name));
                duplicateItems.add(item);
                duplicateNames.add(name);
            } else {
                // First time we meet that name
                first.put(name, item);
            }
        }
        if (!duplicateItems.isEmpty()) {
            final List<Name> sorted = new ArrayList<>(duplicateNames);
            Collections.sort(sorted);
            issues.add(new DuplicateNamesDictionaryIssue(dictionary,
                                                         toString(dictionary, true)
                                                                 + " contains duplicate names: "
                                                                 + sorted,
                                                         duplicateItems));
        }
    }

    private void checkNonEmpty(List<? super DictionaryIssue> issues) {
        if (IterableUtils.isEmpty(dictionary.getAllowedItems())) {
            issues.add(new EmptyDictionaryIssue(dictionary,
                                                toString(dictionary, true) + " is empty"));
        }
    }

    private void checkTypes(List<? super DictionaryIssue> issues) {
        final Set<Type> types = new HashSet<>();
        for (final Property property : dictionary.getRegistry().getDeclaredProperties()) {
            final Type type = property.getType();
            if (type == null) {
                issues.add(new NullPropertyTypeDictionaryIssue(dictionary,
                                                               "Property " + Checker.wrap(property.getName()) + " has no type.",
                                                               property));
            } else {
                types.add(type);
            }
        }

        for (final Type type : types) {
            if (Type.isEmpty(type)) {
                issues.add(new InvalidTypeDictionaryIssue(dictionary,
                                                          "Type " + Checker.wrap(type.getName()) + " has an empty definition.",
                                                          type));
            }
        }
    }

    private static String toString(DExpressed expressed) {
        if (expressed instanceof final Alias alias) {
            return "Alias '" + alias.getName() + "'";
        } else {
            return "Assertion";
        }
    }

    private static void checkPExpressed(Iterable<? extends DExpressed> expresseds,
                                        Dictionary dictionary,
                                        List<? super DictionaryIssue> issues) {
        final SemanticChecker semanticChecker;
        semanticChecker = new SemanticChecker(dictionary);

        for (final DExpressed expressed : expresseds) {
            final Expression expression = expressed.getExpression();
            if (expression == null) {
                issues.add(new NullExpressionDictionaryIssue(dictionary,
                                                             toString(expressed) + " with null expression.",
                                                             expressed));
            } else {
                final Diagnosis<ApplicIssue> diagnosis = semanticChecker.check(expression);
                if (!diagnosis.isOk()) {
                    for (final ApplicIssue issue : diagnosis.getIssues()) {
                        issues.add(new InvalidExpressionDictionaryIssue(dictionary,
                                                                        toString(expressed) + " " + issue.getDescription(),
                                                                        expressed,
                                                                        diagnosis));
                    }
                }
            }
        }
    }

    private void checkGraph(List<? super DictionaryIssue> issues) {
        final DictionaryGraph g;
        if (graph == null) {
            g = new DictionaryGraph();
            g.build(dictionary);
        } else {
            g = graph;
        }
        if (g.hasCycles()) {
            issues.add(new DependencyCycleDictionaryIssue(dictionary,
                                                          toString(dictionary, true)
                                                                  + " contains dependency cycles.",
                                                          g.getCyclesMembers()));
        }
    }
}
