package cdc.applic.dictionaries.core;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.DictionaryException;
import cdc.applic.dictionaries.DictionaryUtils;
import cdc.applic.dictionaries.core.checks.DictionaryChecker;
import cdc.applic.dictionaries.core.utils.DictionaryGraph;
import cdc.applic.dictionaries.core.visitors.CollectNamedDItemNames;
import cdc.applic.dictionaries.handles.ApplicCache;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.Assertion;
import cdc.applic.dictionaries.items.DExpressed;
import cdc.applic.dictionaries.items.DItem;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.literals.Name;
import cdc.graphs.EdgeDirection;
import cdc.graphs.GraphAdapter;
import cdc.graphs.core.GraphConnectedComponents;
import cdc.graphs.impl.BasicGraphEdge;
import cdc.graphs.impl.RestrictionSubGraph;
import cdc.util.debug.Verbosity;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;

/**
 * A DictionaryDependencyCache computes and stores dependency data that only depend on a Dictionary.
 *
 * <h2>Dependencies</h2>
 * All {@link DExpressed}s have <b>dependencies</b> on {@link NamedDItem}s:
 * <ul>
 * <li>An {@link Assertion} may depend on {@link Alias}es and {@link Property Properties}.
 * <li>An {@link Alias} may depend on other {@link Alias}es and {@link Property Properties}.
 * </ul>
 * There shall not exist any cycle in the dependency graph.
 *
 * <h2>Related items</h2>
 * All {@link NamedDItem}s have <b>related {@link DItem}s</b> that should be taken into account.<br>
 * An Item {@code Y} is related to a NamedItem {@code X}:
 * <ul>
 * <li>if {@code Y = X},
 * <li>or if there exists a set of Assertions that link together {@code X} and {@code Y}.
 * </ul>
 * When there are no Assertions (or they are ignored), the set of related Items of a NamedItem {@code X} is the singleton
 * {{@code X}}.
 *
 * @author Damien Carbonne
 */
public class DictionaryDependencyCache extends ApplicCache {
    private boolean valid = false;
    private final DictionaryGraph g = new DictionaryGraph();
    private boolean hasDependencyCycles = false;

    /** Map from each Expressed to the NamedItems it depends on (excluding itself). */
    private final Map<DExpressed, Set<NamedDItem>> expressedDependencies = new HashMap<>();

    /** Map from each NamedItem to its related Items (including itself) when Assertions are included. */
    private final Map<NamedDItem, Set<DItem>> relatedItemsIncludeAssertions = new HashMap<>();

    /** Map from each NamedItem to its related Items (including itself) when Assertions are excluded. */
    private final Map<NamedDItem, Set<DItem>> relatedItemsExcludeAssertions = new HashMap<>();

    public DictionaryDependencyCache(DictionaryHandle handle) {
        super(handle, null);
    }

    public static DictionaryDependencyCache get(DictionaryHandle handle) {
        return handle.computeIfAbsent(DictionaryDependencyCache.class, DictionaryDependencyCache::new);
    }

    /**
     * Validate and fills this cache if it is invalid.
     *
     * @throws DictionaryException When the dictionary is invalid.
     */
    public void validate() {
        checkSerial();
        if (!valid) {
            clear();
            g.build(getDictionary());

            final DictionaryChecker checker = new DictionaryChecker(getDictionary(), g);
            checker.check();

            hasDependencyCycles = g.hasCycles();

            // Compute all NamedItems each alias depends on
            for (final Alias alias : getDictionary().getRegistry().getAllAliases()) {
                fillDependencies(alias);
            }

            // Compute all NamedItems each assertion depends on
            for (final Assertion assertion : getDictionary().getAllAssertions()) {
                fillDependencies(assertion);
            }

            // Compute the related graph (for assertions)
            // It is the initial graph from which some edges have been removed
            final RestrictionSubGraph<DItem, BasicGraphEdge<DItem>> rg = buildAssertionRelatedGraph();

            // Compute connected components in the related graph
            final GraphConnectedComponents<DItem, BasicGraphEdge<DItem>> cg = new GraphConnectedComponents<>(rg);
            final List<GraphAdapter<DItem, BasicGraphEdge<DItem>>> components = cg.computeConnectedComponents();

            for (final GraphAdapter<DItem, BasicGraphEdge<DItem>> component : components) {
                // Collect NamedDItems of each component
                final Set<DItem> items = new HashSet<>();
                for (final DItem item : component.getNodes()) {
                    items.add(item);
                }
                // Add all DItems to related of each NamedDItem
                for (final DItem item : items) {
                    if (item instanceof final NamedDItem x) {
                        final Set<DItem> set =
                                relatedItemsIncludeAssertions.computeIfAbsent(x, k -> new HashSet<>());
                        set.addAll(items);
                    }
                }
            }

            // Make sure all NamedItems are related to themselves
            for (final NamedDItem item : getDictionary().getRegistry().getAllItems()) {
                final Set<DItem> setia = relatedItemsIncludeAssertions.computeIfAbsent(item, k -> new HashSet<>());
                setia.add(item);
                final Set<DItem> setea = relatedItemsExcludeAssertions.computeIfAbsent(item, k -> new HashSet<>());
                setea.add(item);
            }

            valid = true;
        }
    }

    @Override
    public void invalidate() {
        valid = false;
    }

    @Override
    public void clear() {
        valid = false;
        g.clear();
        hasDependencyCycles = false;
        expressedDependencies.clear();
        relatedItemsIncludeAssertions.clear();
        relatedItemsExcludeAssertions.clear();
    }

    /**
     * Computes all dependencies of an expressed (using {@link #g}) and adds them to {@link #expressedDependencies}.
     * <p>
     * <b>Note:</b> {@code expressed} is excluded from its dependencies.
     *
     * @param expressed The expressed.
     */
    private void fillDependencies(DExpressed expressed) {
        final Set<NamedDItem> dependencies = expressedDependencies.computeIfAbsent(expressed, k -> new HashSet<>());

        // Compute all items 'expressed' depends on
        final Set<DItem> items = g.getClosure(expressed, EdgeDirection.OUTGOING);

        for (final DItem item : items) {
            if (item instanceof final NamedDItem x && item != expressed) {
                dependencies.add(x);
            }
        }
    }

    /**
     * Computes the assertion related graph.
     * <p>
     * It is the DictionaryGraph from which certain edges are removed.<br>
     * Removed edges are dependencies of Aliases that are not used by any Assertion.
     *
     * @return The assertion related graph.
     */
    private RestrictionSubGraph<DItem, BasicGraphEdge<DItem>> buildAssertionRelatedGraph() {
        final RestrictionSubGraph<DItem, BasicGraphEdge<DItem>> rg = new RestrictionSubGraph<>(g);
        // Remove all edges that define Aliases that have no dependent Assertion

        // Set of aliases that are used by at least one assertion
        final Set<Alias> usedAliases = new HashSet<>();

        for (final Assertion assertion : getDictionary().getAllAssertions()) {
            for (final NamedDItem item : expressedDependencies.get(assertion)) {
                if (item instanceof final Alias alias) {
                    usedAliases.add(alias);
                }
            }
        }

        // Now remove all outgoing edges of useless Aliases
        for (final Alias alias : getDictionary().getRegistry().getDeclaredAliases()) {
            if (!usedAliases.contains(alias)) {
                final Set<BasicGraphEdge<DItem>> toRemove = new HashSet<>();
                for (final BasicGraphEdge<DItem> edge : g.getEdges(alias, EdgeDirection.OUTGOING)) {
                    toRemove.add(edge);
                }
                for (final BasicGraphEdge<DItem> edge : toRemove) {
                    rg.removeEdge(edge);
                }
            }
        }

        return rg;
    }

    /**
     * Returns the set of {@link NamedDItem}s an {@link DExpressed} is depending on.
     *
     * @param expressed The expressed.
     * @return The set of {@link NamedDItem}s {@code expressed} is depending on.
     * @throws IllegalArgumentException When {@code expressed} is {@code null} or unknown.
     */
    public Set<NamedDItem> getDependencies(DExpressed expressed) {
        Checks.isNotNull(expressed, "expressed");

        validate();
        Checks.isTrue(expressedDependencies.containsKey(expressed),
                      "This expressed {} is unknown in {}",
                      expressed,
                      getDictionary().getPath());

        return expressedDependencies.get(expressed);
    }

    /**
     * Returns the set of {@link DItem}s a {@link NamedDItem} is related to.
     * <p>
     * <b>WARNING:</b> This includes Assertions.
     *
     * @param item The named item.
     * @param assertionStrategy If {@code TRUE}, Assertions are used to define related items.
     * @return the set of {@link DItem}s {@code item} is related to.
     */
    public Set<DItem> getRelatedItems(NamedDItem item,
                                      AssertionStrategy assertionStrategy) {
        Checks.isNotNull(item, "item");
        Checks.isNotNull(assertionStrategy, "assertionStrategy");

        validate();
        if (assertionStrategy == AssertionStrategy.INCLUDE_ASSERTIONS) {
            Checks.isTrue(relatedItemsIncludeAssertions.containsKey(item),
                          "This item {} is unknown in {}",
                          item,
                          getDictionary().getPath());
            return relatedItemsIncludeAssertions.get(item);
        } else {
            Checks.isTrue(relatedItemsExcludeAssertions.containsKey(item),
                          "This item {} is unknown in {}",
                          item,
                          getDictionary().getPath());
            return relatedItemsExcludeAssertions.get(item);
        }
    }

    /**
     * Calculates and returns the set of {@link NamedDItem}s a {@link Node} is depending on.
     *
     * @param node The node.
     * @return The set of {@link NamedDItem}s {@code node} is depending on.
     */
    public Set<NamedDItem> getDependencies(Node node) {
        Checks.isNotNull(node, "node");

        validate();
        // Names of NamedItems that are directly used by the node tree
        final Set<Name> directNames = CollectNamedDItemNames.collect(node);

        // NamedItems that are directly used by the node tree
        final List<NamedDItem> directNamedItems = new ArrayList<>();
        DictionaryUtils.fillNamedItems(directNames, directNamedItems, getDictionaryHandle().getDictionary());

        final Set<NamedDItem> result = new HashSet<>();
        for (final NamedDItem item : directNamedItems) {
            if (item instanceof Property) {
                result.add(item);
            } else {
                result.addAll(getDependencies((DExpressed) item));
                if (item instanceof Alias) {
                    result.add(item);
                }
            }
        }
        return result;
    }

    public boolean hasDependecyCycles() {
        validate();
        return hasDependencyCycles;
    }

    @Override
    public void print(PrintStream out,
                      int level,
                      Verbosity verbosity) {
        indent(out, level);
        out.println(valid ? "Valid" : "Invalid");

        if (valid) {
            indent(out, level);
            out.println("Dependencies (" + expressedDependencies.size() + ")");
            if (verbosity != Verbosity.ESSENTIAL) {
                for (final DExpressed expressed : IterableUtils.toSortedList(expressedDependencies.keySet(), DItem.COMPARATOR)) {
                    indent(out, level + 1);
                    out.println(expressed);
                    for (final NamedDItem dependency : IterableUtils.toSortedList(getDependencies(expressed), DItem.COMPARATOR)) {
                        indent(out, level + 2);
                        out.println(dependency);
                    }
                }
            }

            indent(out, level);
            out.println("Related Items Include Assertions (" + relatedItemsIncludeAssertions.size() + ")");
            if (verbosity != Verbosity.ESSENTIAL) {
                for (final NamedDItem item : IterableUtils.toSortedList(relatedItemsIncludeAssertions.keySet(), DItem.COMPARATOR)) {
                    indent(out, level + 1);
                    out.println(item);
                    for (final DItem related : IterableUtils.toSortedList(getRelatedItems(item,
                                                                                          AssertionStrategy.INCLUDE_ASSERTIONS),
                                                                          DItem.COMPARATOR)) {
                        indent(out, level + 2);
                        out.println(related);
                    }
                }
            }

            indent(out, level);
            out.println("Related Items Exclude Assertions (" + relatedItemsExcludeAssertions.size() + ")");
            if (verbosity != Verbosity.ESSENTIAL) {
                for (final NamedDItem item : IterableUtils.toSortedList(relatedItemsExcludeAssertions.keySet(), DItem.COMPARATOR)) {
                    indent(out, level + 1);
                    out.println(item);
                    for (final DItem related : IterableUtils.toSortedList(getRelatedItems(item,
                                                                                          AssertionStrategy.EXCLUDE_ASSERTIONS),
                                                                          DItem.COMPARATOR)) {
                        indent(out, level + 2);
                        out.println(related);
                    }
                }
            }
        }
    }
}