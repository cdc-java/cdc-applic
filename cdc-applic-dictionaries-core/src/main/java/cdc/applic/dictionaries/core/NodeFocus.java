package cdc.applic.dictionaries.core;

import java.io.PrintStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.core.visitors.CollectPropertySItems;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.DExpressed;
import cdc.applic.dictionaries.items.DItem;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.DomainedType;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.literals.Named;
import cdc.util.debug.Printable;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;

/**
 * Utility class that computes data related to a {@link Node}, for a given {@link AssertionStrategy}.
 *
 * @author Damien Carbonne
 */
public class NodeFocus implements Printable {
    private final DictionaryHandle handle;
    private final DictionaryDependencyCache cache;
    private final Node mainNode;
    private final AssertionStrategy assertionStrategy;
    private final Set<NamedDItem> dependencies;

    /**
     * Set of all {@link DItem}s related to {@link #mainNode}.
     * <p>
     * They are the DItems that matter for this SatSystem.
     */
    private final Set<DItem> relatedDItems;

    public NodeFocus(DictionaryHandle handle,
                     Node mainNode,
                     AssertionStrategy assertionStrategy) {
        Checks.isNotNull(handle, "handle");
        Checks.isNotNull(mainNode, "mainNode");
        Checks.isNotNull(assertionStrategy, "assertionStrategy");

        this.handle = handle;
        this.cache = DictionaryDependencyCache.get(handle);
        this.mainNode = mainNode;
        this.assertionStrategy = assertionStrategy;
        this.dependencies = Collections.unmodifiableSet(cache.getDependencies(mainNode));

        // Build related DItems
        final Set<DItem> tmp = new HashSet<>();
        for (final NamedDItem dependency : dependencies) {
            tmp.addAll(cache.getRelatedItems(dependency, assertionStrategy));
        }
        this.relatedDItems = Collections.unmodifiableSet(tmp);
    }

    /**
     * @return The DictionaryHandle.
     */
    public DictionaryHandle getDictionaryHandle() {
        return handle;
    }

    /**
     * @return The main node, under focus.
     */
    public Node getMainNode() {
        return mainNode;
    }

    /**
     * @return AssertionStrategy.
     */
    public AssertionStrategy getAssertionStrategy() {
        return assertionStrategy;
    }

    /**
     * @return The set of dependencies of main node.
     */
    public Set<NamedDItem> getDependencies() {
        return dependencies;
    }

    /**
     * @return The set of DItems related to main node.
     */
    public Set<DItem> getRelatedDItems() {
        return relatedDItems;
    }

    public Set<SItem> collectRelatedSItems(Property property) {
        final Set<SItem> result = new HashSet<>();
        CollectPropertySItems.collect(mainNode, property, result);

        for (final DItem item : relatedDItems) {
            if (item instanceof final DExpressed expressed) {
                CollectPropertySItems.collect(expressed.getQualifiedExpression().getRootNode(), property, result);
            }
        }
        if (property.getType() instanceof final DomainedType type) {
            result.addAll(type.getDomain().getItems());
        } else {
            result.add(StringValue.PATTERN_UNKNOWN);
        }
        return result;
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        out.println("Main Node: " + getMainNode());
        getMainNode().print(out, level + 1);

        indent(out, level);
        out.println("Assertion Strategy: " + getAssertionStrategy());

        indent(out, level);
        out.println("Dependencies " + getDependencies().size());
        for (final NamedDItem item : IterableUtils.toSortedList(getDependencies(), Named.NAME_COMPARATOR)) {
            indent(out, level + 1);
            out.println(item);
        }

        indent(out, level);
        out.println("Related DItems " + getRelatedDItems().size());
        for (final DItem item : IterableUtils.toSortedList(getRelatedDItems(), DItem.COMPARATOR)) {
            indent(out, level + 1);
            out.println(item);
        }
    }
}