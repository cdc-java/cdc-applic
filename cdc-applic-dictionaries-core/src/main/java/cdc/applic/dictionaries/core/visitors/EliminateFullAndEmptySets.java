package cdc.applic.dictionaries.core.visitors;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.core.utils.SItemSetOperations;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractPropertyNode;
import cdc.applic.expressions.ast.AbstractSetNode;
import cdc.applic.expressions.ast.AbstractValueNode;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.Value;
import cdc.util.lang.Checks;

/**
 * Converter that applies the following rewriting rules:
 * <ul>
 * <li>&pi;&isin;&empty; &equiv; &perp;
 * <li>&pi;&notin;&empty; &equiv; &top;
 * <li>&pi;&isin;&Omega; &equiv; &top;
 * <li>&pi;&notin;&Omega; &equiv; &perp;
 * <li>&pi;=&omega; &equiv; &top;
 * <li>&pi;&ne;&omega; &equiv; &perp;
 * </ul>
 * <b>Notations:</b><br>
 * &Omega; is the full set of type(&pi;)<br>
 * When &Omega; is a singleton, &Omega;={&omega;}
 * <p>
 * <b>WARNING:</b> these rewriting rules hold when type(&pi;) is not empty.
 *
 * @author Damien Carbonne
 */
public final class EliminateFullAndEmptySets extends AbstractConverter {
    private final Dictionary dictionary;
    private final ReserveStrategy reserveStrategy;

    private EliminateFullAndEmptySets(Dictionary dictionary,
                                      ReserveStrategy reserveStrategy) {
        Checks.isNotNull(dictionary, "dictionary");
        Checks.isNotNull(reserveStrategy, "reserveStrategy");
        this.dictionary = dictionary;
        this.reserveStrategy = reserveStrategy;
    }

    public static Node execute(Node node,
                               Dictionary dictionary,
                               ReserveStrategy reserveStrategy) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(dictionary, "dictionary");
        Checks.isNotNull(reserveStrategy, "reserveStrategy");
        final EliminateFullAndEmptySets visitor = new EliminateFullAndEmptySets(dictionary, reserveStrategy);
        return node.accept(visitor);
    }

    private boolean isFullSet(AbstractSetNode node) {
        final Property property = dictionary.getRegistry().getProperty(node.getName());
        final Type type = property.getType();
        if (reserveStrategy.hasReserve(type)) {
            // Set is never full when the associated type has reserve
            return false;
        } else if (SItemSetOperations.supportsAllOperations(type)) {
            final SItemSet set = node.getCheckedSet();
            return SItemSetOperations.isFullSet(set, type);
        } else {
            return false;
        }
    }

    private boolean isFullSet(AbstractValueNode node) {
        final Property property = dictionary.getRegistry().getProperty(node.getName());
        final Type type = property.getType();
        if (reserveStrategy.hasReserve(type)) {
            // Set is never full when the associated type has reserve
            return false;
        } else if (SItemSetOperations.supportsAllOperations(type)) {
            final Value value = node.getValue();
            return SItemSetOperations.isFullSet(value, type);
        } else {
            return false;
        }
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (node instanceof AbstractPropertyNode) {
            if (node instanceof final AbstractSetNode setNode) {
                final SItemSet set = setNode.getSet();
                if (set.isEmpty()) {
                    return node instanceof InNode
                            ? FalseNode.INSTANCE // node in empty set
                            : TrueNode.INSTANCE; // node not in empty set. This holds if type is not empty
                } else if (isFullSet(setNode)) {
                    return node instanceof InNode
                            ? TrueNode.INSTANCE // node in full set
                            : FalseNode.INSTANCE; // node not in full set
                } else {
                    return setNode.create(setNode.getName(), setNode.getCheckedSet());
                }
            } else {
                final AbstractValueNode valueNode = (AbstractValueNode) node;
                if (isFullSet(valueNode)) {
                    return node instanceof EqualNode
                            ? TrueNode.INSTANCE // node = singleton value
                            : FalseNode.INSTANCE; // node != singleton value
                } else {
                    return super.visitLeaf(node);
                }
            }
        } else {
            return super.visitLeaf(node);
        }
    }
}