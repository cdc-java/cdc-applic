package cdc.applic.dictionaries.core.visitors;

import java.util.HashSet;
import java.util.Set;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.DictionaryUtils;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractPropertyNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.visitors.AbstractCollector;
import cdc.applic.expressions.literals.Name;

/**
 * A Collector that collects {@link Name}s of all {@link Property Properties} that are directly used in a node tree.
 * <ul>
 * <li>{@link AbstractPropertyNode}s
 * <li>{@link RefNode}s that reference a {@link BooleanType} properties: Name of the RefNode must be declared as a Property typed by
 * a BooleanType.
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class CollectPropertyNames extends AbstractCollector<Name> {
    private final Dictionary dictionary;

    private CollectPropertyNames(Dictionary dictionary) {
        super(new HashSet<>());
        this.dictionary = dictionary;
    }

    public static Set<Name> collect(Node node,
                                    Dictionary dictionary) {
        final CollectPropertyNames collector = new CollectPropertyNames(dictionary);
        node.accept(collector);
        return collector.getSet();
    }

    @Override
    public Void visitLeaf(AbstractLeafNode node) {
        if (node instanceof final AbstractPropertyNode n) {
            add(n.getName());
        } else if (node instanceof final RefNode n) {
            if (DictionaryUtils.isAvailableBooleanProperty(n.getName(), dictionary)) {
                add(n.getName());
            }
        }
        return null;
    }
}