package cdc.applic.dictionaries.core.visitors;

import java.util.HashSet;
import java.util.Set;

import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractPropertyNode;
import cdc.applic.expressions.ast.NamedNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.visitors.AbstractCollector;
import cdc.applic.expressions.literals.Name;

/**
 * A Collector that collects {@link Name}s of all {@link NamedDItem}s that are directly used in a node tree.
 * <ul>
 * <li>{@link AbstractPropertyNode}s
 * <li>{@link RefNode}s
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class CollectNamedDItemNames extends AbstractCollector<Name> {

    private CollectNamedDItemNames() {
        super(new HashSet<>());
    }

    public static Set<Name> collect(Node node) {
        final CollectNamedDItemNames collector = new CollectNamedDItemNames();
        node.accept(collector);
        return collector.getSet();
    }

    @Override
    public Void visitLeaf(AbstractLeafNode node) {
        if (node instanceof AbstractPropertyNode || node instanceof RefNode) {
            add(((NamedNode) node).getName());
        }
        return null;
    }
}