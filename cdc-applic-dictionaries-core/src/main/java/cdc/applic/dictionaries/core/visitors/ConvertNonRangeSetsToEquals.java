package cdc.applic.dictionaries.core.visitors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.StringType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractSetNode;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

/**
 * Utility class used to convert set nodes to equal nodes for types that don't support ranges:
 * <ul>
 * <li>{@link BooleanType}s
 * <li>{@link EnumeratedType}s
 * <li>{@link PatternType}s
 * </ul>
 * The following transformations are applied for properties that don't support ranges:
 * <ul>
 * <li>&pi; &isin; {} &equiv; &perp;
 * <li>&pi; &notin; {} &equiv; &top;
 * <li>&pi; &isin; {&upsilon;<sub>1</sub>, &upsilon;<sub>2</sub>, ...} &equiv; &pi; = &upsilon;<sub>1</sub> &or; &pi; =
 * &upsilon;<sub>2</sub> &or; ...
 * <li>&pi; &notin; {&upsilon;<sub>1</sub>, &upsilon;<sub>2</sub>, ...} &equiv; &not; (&pi; = &upsilon;<sub>1</sub> &or; &pi; =
 * &upsilon;<sub>2</sub> &or; ...)
 * </ul>
 * <b>Note:</b> Properties that support ranges are not transformed, even if they don't use them.
 *
 * @author Damien Carbonne
 */
public final class ConvertNonRangeSetsToEquals extends AbstractConverter {
    private final Dictionary dictionary;

    private ConvertNonRangeSetsToEquals(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public static Node execute(Node node,
                               Dictionary dictionary) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(dictionary, "dictionary");

        final ConvertNonRangeSetsToEquals converter = new ConvertNonRangeSetsToEquals(dictionary);
        return node.accept(converter);
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        final Node result;
        if (node instanceof final AbstractSetNode setNode) {
            final SItemSet set = setNode.getSet();
            if (set.isEmpty()) {
                if (setNode instanceof InNode) {
                    result = FalseNode.INSTANCE;
                } else {
                    result = TrueNode.INSTANCE;
                }
            } else {
                final Collection<? extends SItem> items = set.getItems();
                final Name name = setNode.getName();
                final Property property = dictionary.getRegistry().getProperty(name);
                final Type type = property.getType();

                if (type instanceof StringType || type instanceof BooleanType) {
                    // We suppose there is no range for String types.
                    // This may be wrong in the future.
                    final List<Node> nodes = new ArrayList<>();
                    for (final SItem item : items) {
                        final Value value = (Value) item;
                        nodes.add(new EqualNode(name, value));
                    }
                    if (setNode instanceof InNode) {
                        result = NaryOrNode.createSimplestOr(nodes);
                    } else {
                        result = new NotNode(NaryOrNode.createSimplestOr(nodes));
                    }
                } else {
                    result = super.visitLeaf(setNode);
                }
            }
        } else {
            result = super.visitLeaf(node);
        }
        return result;
    }
}