package cdc.applic.dictionaries.core.visitors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.DictionaryUtils;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AbstractPropertyNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.SortableNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.util.lang.Checks;

/**
 * Converter that sorts children nodes.
 * <p>
 * <b>Note:</b> {@link ConvertToNary} should be called first.
 * <p>
 * {@link SortableNode}s are sorted using ordinal then name, and come first.<br>
 * Other children nodes come after, keeping their relative order.<br>
 * Nodes associated to a {@link NamedDItem} ({@link AbstractPropertyNode}
 * and {@link RefNode}) use their user defined ordinal.<br>
 * {@link TrueNode} and {@link FalseNode} have a -&infin; ordinal.
 *
 * @author Damien Carbonne
 */
public final class SortLeaves extends AbstractConverter {
    private final Comparator<SortableNode> comparator;

    private SortLeaves(Dictionary dictionary) {
        Checks.isNotNull(dictionary, "dictionary");
        this.comparator = DictionaryUtils.createOrdinaNameComparator(dictionary);
    }

    public static Node execute(Node node,
                               Dictionary dictionary) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(dictionary, "dictionary");
        final SortLeaves visitor = new SortLeaves(dictionary);
        return node.accept(visitor);
    }

    private static SortableNode toSortable(Node node) {
        if (node instanceof final SortableNode n) {
            return n;
        } else {
            return null;
        }
    }

    @Override
    public Node visitBinary(AbstractBinaryNode node) {
        final Node alpha = node.getAlpha().accept(this);
        final Node beta = node.getBeta().accept(this);
        final SortableNode sortableAlpha = toSortable(alpha);
        final SortableNode sortableBeta = toSortable(beta);

        if (sortableAlpha != null && sortableBeta != null) {
            if (comparator.compare(sortableAlpha, sortableBeta) <= 0) {
                return node.create(sortableAlpha, sortableBeta);
            } else {
                return node.create(sortableBeta, sortableAlpha);
            }
        } else {
            return node.create(alpha, beta);
        }
    }

    @Override
    public Node visitNary(AbstractNaryNode node) {
        final List<Node> processedChildren = new ArrayList<>();
        final List<SortableNode> sortableChildren = new ArrayList<>();
        for (final Node child : node.getChildren()) {
            final Node processedChild = child.accept(this);
            processedChildren.add(processedChild);
            final SortableNode sortableChild = toSortable(processedChild);
            if (sortableChild != null) {
                sortableChildren.add(sortableChild);
            }
        }
        if (sortableChildren.size() >= 2) {
            // Sort sortable children
            Collections.sort(sortableChildren, comparator);
            // Add sortable children first, and others after
            final List<Node> children = new ArrayList<>(sortableChildren);
            processedChildren.removeAll(sortableChildren);
            children.addAll(processedChildren);
            return node.create(children);
        } else {
            return node.create(processedChildren);
        }
    }
}