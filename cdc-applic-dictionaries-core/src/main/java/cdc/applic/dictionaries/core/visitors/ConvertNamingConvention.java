package cdc.applic.dictionaries.core.visitors;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.DictionaryUtils;
import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.NamingConventionUtils;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.PropertyNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.SetNode;
import cdc.applic.expressions.ast.ValueNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

/**
 * Converter that converts the {@link NamingConvention} of an {@link Expression}.
 *
 * @author Damien Carbonne
 */
public final class ConvertNamingConvention extends AbstractConverter {
    private final Dictionary dictionary;
    private final NamingConvention convention;

    private ConvertNamingConvention(Dictionary dictionary,
                                    NamingConvention convention) {
        this.dictionary = dictionary;
        this.convention = convention;
    }

    /**
     * Converts an expression to a target convention.
     *
     * @param node The node to convert.
     * @param dictionary The dictionary.
     * @param convention The target convention.
     * @return The conversion of {@code node} with the same semantic and compliant with {@code convention}.
     */
    public static Node execute(Node node,
                               Dictionary dictionary,
                               NamingConvention convention) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(dictionary, "dictionary");
        Checks.isNotNull(convention, "convention");

        final ConvertNamingConvention converter = new ConvertNamingConvention(dictionary, convention);
        return node.accept(converter);
    }

    /**
     * Converts an expression to the {@link NamingConvention#DEFAULT} convention.
     *
     * @param node The node to convert.
     * @param dictionary The dictionary.
     * @return The conversion of {@code node} with the same semantic and compliant with {@link NamingConvention#DEFAULT} convention.
     */
    public static Node convertToDefault(Node node,
                                        Dictionary dictionary) {
        return execute(node, dictionary, NamingConvention.DEFAULT);
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (node instanceof final RefNode refNode) {
            // Alias name and boolean property name synonyms
            final Name name = refNode.getName();
            if (dictionary.getRegistry().hasAlias(name)
                    || DictionaryUtils.isAvailableBooleanProperty(name, dictionary)) {
                final Name newName = dictionary.getRegistry().convertItemName(name, convention);
                if (name.equals(newName)) {
                    return node;
                } else {
                    return refNode.setName(newName);
                }
            } else {
                return node;
            }
        } else if (node instanceof final PropertyNode propertyNode) {
            // Property name synonyms
            final Name name = propertyNode.getName();
            final Name newName = dictionary.getRegistry().convertItemName(name, convention);

            // If the property type is enumerated, its values could have synonyms.
            final Property property = dictionary.getProperty(name);
            final Type type = property.getType();
            if (type instanceof EnumeratedType) {

                // Property values synonyms
                if (propertyNode instanceof final ValueNode valueNode) {
                    final Value value = valueNode.getValue();
                    final Value newValue =
                            NamingConventionUtils.convert(value, type, convention);
                    if (name.equals(newName) && value.equals(newValue)) {
                        return node;
                    } else {
                        return valueNode.create(newName, newValue);
                    }
                } else {
                    final SetNode setNode = (SetNode) propertyNode;
                    final SItemSet set = setNode.getSet();
                    final SItemSet newSet =
                            NamingConventionUtils.convert(set, type, convention);
                    if (name.equals(newName) && set.equals(newSet)) {
                        return node;
                    } else {
                        return setNode.create(newName, newSet);
                    }
                }
            } else {
                if (name.equals(newName)) {
                    return node;
                } else {
                    return propertyNode.setName(newName);
                }
            }
        } else {
            return node;
        }
    }
}