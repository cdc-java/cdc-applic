package cdc.applic.dictionaries.core.utils;

import java.util.Collection;
import java.util.Set;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.Registry;
import cdc.applic.dictionaries.core.visitors.CollectAliasNames;
import cdc.applic.dictionaries.core.visitors.CollectPropertyNames;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.Assertion;
import cdc.applic.dictionaries.items.DExpressed;
import cdc.applic.dictionaries.items.DItem;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.literals.Name;
import cdc.graphs.EdgeDirection;
import cdc.graphs.core.GraphCycles;
import cdc.graphs.core.GraphTransitiveClosure;
import cdc.graphs.impl.BasicGraphEdge;
import cdc.graphs.impl.BasicSuperLightGraph;
import cdc.graphs.impl.ExplicitSubGraph;
import cdc.util.lang.Checks;

/**
 * Graph associated to a Dictionary.
 * <ul>
 * <li>Nodes are all declared and inherited {@link NamedDItem}s in the registry
 * and all declared and inherited {@link Assertion}s of the dictionary
 * <li>Edges are all dependencies of {@link DExpressed}s.
 * </ul>
 *
 * @author Damien Carbonne
 */
public class DictionaryGraph extends BasicSuperLightGraph<DItem> {
    public DictionaryGraph() {
        super();
    }

    public void build(Dictionary dictionary) {
        clear();

        final Registry registry = dictionary.getRegistry();

        // 1) Add all items (Properties and ALiases) of the registry
        for (final NamedDItem item : registry.getAllItems()) {
            addNode(item);
        }

        // 2) Add aliases edges
        for (final Alias alias : registry.getAllAliases()) {
            addEdges(registry, alias);
        }

        // Add all assertions and their edges
        for (final Assertion assertion : dictionary.getAllAssertions()) {
            addNode(assertion);
            addEdges(registry, assertion);
        }
    }

    private void addEdges(Registry registry,
                          DExpressed expressed) {
        final Node node = expressed.getQualifiedExpression().getRootNode();

        // Link expressed to aliases it directly uses
        final Set<Name> anames = CollectAliasNames.collect(node, registry);
        for (final Name name : anames) {
            final Alias alias = registry.getAlias(name);
            addEdge(expressed, alias);
        }

        // Link expressed to properties it directly uses
        final Set<Name> pnames = CollectPropertyNames.collect(node, registry);
        for (final Name name : pnames) {
            final Property property = registry.getProperty(name);
            addEdge(expressed, property);
        }
    }

    public Set<DItem> getClosure(Collection<DItem> items,
                                 EdgeDirection direction) {
        Checks.isNotNull(items, "items");

        final GraphTransitiveClosure<DItem, BasicGraphEdge<DItem>> closure = new GraphTransitiveClosure<>(this);
        return closure.computeTransitiveClosureNodes(items, direction);
    }

    public Set<DItem> getClosure(DItem item,
                                 EdgeDirection direction) {
        Checks.isNotNull(item, "item");

        final GraphTransitiveClosure<DItem, BasicGraphEdge<DItem>> closure = new GraphTransitiveClosure<>(this);
        return closure.computeTransitiveClosureNodes(item, direction);
    }

    public boolean hasCycles() {
        final GraphCycles<DItem, BasicGraphEdge<DItem>> cycles = new GraphCycles<>(this);
        return cycles.containsCycles();
    }

    public ExplicitSubGraph<DItem, BasicGraphEdge<DItem>> getCyclesMembers() {
        final GraphCycles<DItem, BasicGraphEdge<DItem>> cycles = new GraphCycles<>(this);
        return cycles.computeCyclesMembers();
    }
}