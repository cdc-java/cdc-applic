package cdc.applic.dictionaries.core.visitors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.EnumeratedValue;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractSetNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.content.UncheckedSet;
import cdc.util.lang.Checks;

/**
 * Converter that normalizes writing of sets to be compliant with dictionary constraints.
 * <p>
 * {@link BooleanSet}, {@link IntegerSet}s and {RealSet}s are natively normalized.<br>
 * {@link StringSet} needs special handling and {@link UncheckedSet} is ignored.
 * <p>
 * {@link PatternType} sets are normalized using alphabetical order.<br>
 * {@link EnumeratedType} sets are normalized using ordinal then literal.
 *
 * @author Damien Carbonne
 */
public final class NormalizeSets extends AbstractConverter {
    private final Dictionary dictionary;

    private NormalizeSets(Dictionary dictionary) {
        Checks.isNotNull(dictionary, "dictionary");
        this.dictionary = dictionary;
    }

    public static Node execute(Node node,
                               Dictionary dictionary) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(dictionary, "dictionary");
        final NormalizeSets visitor = new NormalizeSets(dictionary);
        return node.accept(visitor);
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (node instanceof final AbstractSetNode n) {
            return normalize(n);
        } else {
            return node;
        }
    }

    private AbstractSetNode normalize(AbstractSetNode node) {
        final SItemSet set = node.getSet();
        if (set.isEmpty() || set.isSingleton()) {
            return node;
        } else {
            // Set is non empty, so checked is subclass of CheckedSet
            final SItemSet checked = node.getCheckedSet();
            if (checked instanceof final StringSet ss) {
                final Property property = dictionary.getRegistry().getProperty(node.getName());
                final Type type = property.getType();
                if (type instanceof final EnumeratedType e) {
                    return node.create(node.getName(), normalizeEnumeratedSet(ss, e));
                } else {
                    return node.create(node.getName(), normalizePatternSet(ss));
                }
            } else {
                // BooleanSet, IntegerSet and RealSet
                // Parsing generated UncheckedSets, that may contain duplicate or be in wrong order.
                // Creating the node with the checked set is sufficient.
                return node.create(node.getName(), checked);
            }
        }
    }

    public static SItemSet normalizeSet(SItemSet set,
                                        Type type) {
        if (set.isEmpty() || set.isSingleton()) {
            return set;
        } else {
            final SItemSet checked = set.getChecked();
            if (checked == null) {
                throw new IllegalArgumentException();
            } else if (checked instanceof StringSet) {
                return normalizeStringSet((StringSet) set, type);
            } else {
                return checked;
            }
        }
    }

    public static StringSet normalizeStringSet(StringSet set,
                                               Type type) {
        if (set.isEmpty() || set.isSingleton()) {
            return set;
        } else {
            if (type instanceof final EnumeratedType t) {
                return normalizeEnumeratedSet(set, t);
            } else if (type instanceof PatternType) {
                return normalizePatternSet(set);
            } else {
                throw new IllegalArgumentException();
            }
        }
    }

    private static StringSet normalizeEnumeratedSet(StringSet set,
                                                    EnumeratedType type) {
        final List<EnumeratedValue> evalues = new ArrayList<>();
        for (final StringValue value : set.getItems()) {
            evalues.add(type.getValue(value));
        }
        Collections.sort(evalues, EnumeratedValue.ORDINAL_LITERAL_COMPARATOR);
        final List<StringValue> values = new ArrayList<>();
        for (final EnumeratedValue evalue : evalues) {
            values.add(evalue.getLiteral());
        }
        return StringSet.of(values);
    }

    private static StringSet normalizePatternSet(StringSet set) {
        final List<StringValue> values = new ArrayList<>(set.getItems());
        Collections.sort(values);
        return StringSet.of(values);
    }
}