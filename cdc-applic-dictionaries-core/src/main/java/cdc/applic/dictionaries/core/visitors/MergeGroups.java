package cdc.applic.dictionaries.core.visitors;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.util.lang.Checks;

public final class MergeGroups extends AbstractConverter {
    private static final Logger LOGGER = LogManager.getLogger(MergeGroups.class);
    private final Dictionary dictionary;

    private MergeGroups(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public static Node execute(Node node,
                               Dictionary dictionary) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(dictionary, "dictionary");
        final MergeGroups visitor = new MergeGroups(dictionary);
        return node.accept(visitor);
    }

    @Override
    public Node visitBinary(AbstractBinaryNode node) {
        if (node instanceof OrNode) {
            final Node alpha = node.getAlpha().accept(this);
            final Node beta = node.getBeta().accept(this);
            final MergerGroup alphaGroup = MergerGroup.build(alpha, dictionary);
            final MergerGroup betaGroup = MergerGroup.build(beta, dictionary);
            LOGGER.trace("Visit Or({}, {}): {} {}", alpha, beta, alphaGroup, betaGroup);
            final Node merged = merge(alphaGroup, betaGroup);
            if (merged != null) {
                return merged;
            } else {
                return node.create(alpha, beta);
            }
        } else {
            return super.visitBinary(node);
        }
    }

    private static Node merge(MergerGroup alpha,
                              MergerGroup beta) {
        LOGGER.trace("merge({}, {})", alpha, beta);
        if (alpha == null
                || beta == null
                || alpha.size() != beta.size()) {
            return null;
        } else {
            // alpha.size() == beta.size()
            final MergerGroup bridge = alpha.intersectionWith(beta);
            LOGGER.trace("bridge: {}", bridge);
            if (bridge.size() == alpha.size()) {
                return alpha.toNode();
            } else if (bridge.size() == alpha.size() - 1) {
                // alpha and beta differ by one MergeAtom
                // Try to merge them
                final MergerGroup merger = alpha.mergeWith(beta, bridge);
                LOGGER.trace("merger: {}", merger);
                if (merger == null) {
                    // The 2 differing MergertAtoms could not be merged
                    return null;
                } else {
                    return merger.toNode();
                }
            } else {
                return null;
            }
        }
    }

    @Override
    public Node visitNary(AbstractNaryNode node) {
        if (node instanceof NaryOrNode) {
            final List<Node> processedChildren = new ArrayList<>();
            final List<MergerGroup> groups = new ArrayList<>();
            for (final Node child : node.getChildren()) {
                final Node processedChild = child.accept(this);
                processedChildren.add(processedChild);
                groups.add(MergerGroup.build(processedChild, dictionary));
            }
            LOGGER.trace("Visit Nary Or({}): {}", processedChildren, groups);
            final List<Node> merged = merge(groups, processedChildren);
            return node.create(merged);

        } else {
            return super.visitNary(node);
        }
    }

    private static List<Node> merge(List<MergerGroup> groups,
                                    List<Node> nodes) {
        LOGGER.trace("===============================================");
        LOGGER.trace("merge({}, {})", groups, nodes);

        // Resulting nodes
        final List<Node> result = new ArrayList<>();
        final int size = groups.size();
        // Indices of nodes that have been merged and should be skipped
        final Set<Integer> done = new HashSet<>();

        // Alpha iteration
        for (int i = 0; i < size - 1; i++) {
            LOGGER.trace("------------------------");
            LOGGER.trace("loop {}", i);
            LOGGER.trace("result: {}", result);
            LOGGER.trace("done: {}", done);
            final MergerGroup alpha = groups.get(i);
            LOGGER.trace("alpha: {} {}", i, alpha);
            if (alpha == null) {
                result.add(nodes.get(i));
            } else if (!done.contains(i)) {
                // List of all possible mergers between alpha and other nodes
                final List<MergerGroup> mergers = new ArrayList<>();
                // List of indices of beta nodes corresponding to possible mergers
                final List<Integer> mergersIndices = new ArrayList<>();
                // Beta iteration
                for (int j = i + 1; j < size; j++) {
                    final MergerGroup beta = groups.get(j);
                    LOGGER.trace("   beta: {} {}", j, beta);
                    if (beta != null
                            && alpha.size() == beta.size()
                            && !done.contains(j)) {
                        // Compute bridge between alpha and beta
                        final MergerGroup bridge = groups.get(i).intersectionWith(groups.get(j));
                        LOGGER.trace("      bridge: {}", bridge);
                        if (bridge.size() == alpha.size()) {
                            mergers.add(bridge);
                            mergersIndices.add(j);
                        } else if (bridge.size() == alpha.size() - 1) {
                            // Compute merger of alpha and beta
                            final MergerGroup merger = alpha.mergeWith(beta, bridge);
                            LOGGER.trace("         merger: {} {}", j, merger);
                            if (merger != null) {
                                mergers.add(merger);
                                mergersIndices.add(j);
                            }
                        }
                    }
                }
                LOGGER.trace("   mergers: {}", mergers);
                LOGGER.trace("   mergers indices: {}", mergersIndices);
                if (mergers.isEmpty()) {
                    // No merger of alpha was found: keep node corresponding to alpha
                    result.add(nodes.get(i));
                    // No need to add i to done
                } else {
                    // Select the best merger for alpha among possible ones
                    final int bestIndex = getIndexOfBestMerger(mergers);
                    final MergerGroup best = mergers.get(bestIndex);
                    LOGGER.trace("      best {} {}", bestIndex, best);
                    result.add(best.toNode());
                    done.add(mergersIndices.get(bestIndex));
                    // No need to add i to done
                }
            }
        }
        LOGGER.trace("------------------------");
        if (!done.contains(size - 1)) {
            LOGGER.trace("add last");
            result.add(nodes.get(size - 1));
        }
        LOGGER.trace("   result: {}", result);
        return result;
    }

    private static int getIndexOfBestMerger(List<MergerGroup> mergers) {
        if (mergers.size() == 1) {
            return 0;
        } else {
            MergerGroup max = mergers.get(0);
            int maxIndex = 0;
            for (int index = 2; index < mergers.size(); index++) {
                if (MergerGroup.COMPARATOR.compare(max, mergers.get(index)) < 0) {
                    max = mergers.get(index);
                    maxIndex = index;
                }
            }
            return maxIndex;
        }
    }
}