package cdc.applic.dictionaries.core.visitors;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.ConvertToBinary;
import cdc.applic.expressions.ast.visitors.DistributeAndOverOr;
import cdc.util.lang.Checks;

/**
 * Convert an AST (node) to an equivalent Disjunctive Normal Form.
 * <p>
 * <b>Note</b>: This is an adaptation of AIMA Java implementation.
 *
 * @author Damien Carbonne
 */
public final class ConvertToDNF {
    private ConvertToDNF() {
    }

    public static Node execute(Node node,
                               MoveNotInwards.Variant variant,
                               Dictionary dictionary,
                               ReserveStrategy reserveStrategy) {
        Checks.isTrue(dictionary != null || !variant.requiresDictionary(), "Can not use {} without a dictionary", variant);

        Node n = node;
        n = ConvertToBinary.execute(n);
        n = ConvertToNNF.execute(n, variant, dictionary, reserveStrategy);
        n = DistributeAndOverOr.execute(n);
        return n;
    }

    public static Node execute(Node node,
                               MoveNotInwards.Variant variant) {
        return execute(node, variant, null, null);
    }
}