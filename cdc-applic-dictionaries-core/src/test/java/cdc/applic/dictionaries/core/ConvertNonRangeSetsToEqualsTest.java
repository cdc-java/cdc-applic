package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.core.visitors.ConvertNonRangeSetsToEquals;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;

class ConvertNonRangeSetsToEqualsTest {
    private final RepositorySupport support = new RepositorySupport();

    private void check(String expression,
                       String expected) {
        final Expression x1 = new Expression(expression);
        final Expression x2 = ConvertNonRangeSetsToEquals.execute(x1.getRootNode(), support.registry)
                                                         .toExpression(Formatting.SHORT_NARROW);
        assertEquals(expected, x2.toString());
    }

    @Test
    void test() {
        check("", "true");
        check("E = E1", "E=E1");
        check("E in {}", "false");
        check("E in {E1}", "E=E1");
        check("E in {E1,E2}", "E=E1|E=E2");

        check("I = 10", "I=10");
        check("I in {10}", "I<:{10}");

        check("E not in {}", "true");
        check("B", "B");
        check("B in {true}", "B=true");
        check("B in {false}", "B=false");
        check("B not in {true}", "!(B=true)");
        check("B not in {false}", "!(B=false)");
    }
}