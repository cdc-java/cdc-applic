package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.checks.DictionaryIssue;
import cdc.applic.dictionaries.checks.EmptyDictionaryIssue;
import cdc.applic.dictionaries.impl.RepositorySupport;

class DictionaryIssueTest {
    private final RepositorySupport support = new RepositorySupport();

    @Test
    void testEmptyDictionaryIssue() {
        final DictionaryIssue issue = new EmptyDictionaryIssue(support.registry, "description");
        assertEquals(support.registry, issue.getDictionary());
        assertEquals("description", issue.getDescription());
    }

    // TODO add other tests
}