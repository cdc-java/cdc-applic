package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.core.visitors.ConvertToNNF;
import cdc.applic.dictionaries.core.visitors.MoveNotInwards;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.IllegalOperationException;
import cdc.applic.expressions.ast.visitors.ConvertToNary;

class ConvertToNNFTest {
    private final RepositorySupport support = new RepositorySupport();

    private void check(String expression,
                       MoveNotInwards.Variant variant,
                       ReserveStrategy reserveStrategy,
                       String expected) {
        final Expression x1 = new Expression(expression);
        final Expression x2 = ConvertToNNF.execute(ConvertToNary.execute(x1.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY),
                                                   variant,
                                                   support.registry,
                                                   reserveStrategy)
                                          .toExpression(Formatting.SHORT_NARROW);
        assertEquals(expected, x2.toString());
        if (!variant.requiresDictionary()) {
            final Expression x3 = ConvertToNNF.execute(ConvertToNary.execute(x1.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY),
                                                       variant)
                                              .toExpression(Formatting.SHORT_NARROW);
            assertEquals(expected, x3.toString());
        } else {
            assertThrows(IllegalArgumentException.class,
                         () -> {
                             ConvertToNNF.execute(x1.getRootNode(), variant, null, null);
                         });
        }
    }

    private void checkUseNegativeLeaves(String expression,
                                        String expected) {
        check(expression,
              MoveNotInwards.Variant.USE_NEGATIVE_LEAVES,
              ReserveStrategy.NO_RESERVES,
              expected);
    }

    private void checkDontUseNegativeLeaves(String expression,
                                            String expected) {
        check(expression,
              MoveNotInwards.Variant.DONT_USE_NEGATIVE_LEAVES,
              ReserveStrategy.NO_RESERVES,
              expected);
    }

    private void checkDontUseNegationIgnore(String expression,
                                            String expected) {
        check(expression,
              MoveNotInwards.Variant.DONT_USE_NEGATION_IGNORE_WHEN_IMPOSSIBLE,
              ReserveStrategy.NO_RESERVES,
              expected);
    }

    private void checkDontUseNegationThrow(String expression,
                                           String expected) {
        check(expression,
              MoveNotInwards.Variant.DONT_USE_NEGATION_THROW_WHEN_IMPOSSIBLE,
              ReserveStrategy.NO_RESERVES,
              expected);
    }

    @Test
    void testUseNegativeLeaves() {
        checkUseNegativeLeaves("true", "true");
        checkUseNegativeLeaves("not true", "false");

        checkUseNegativeLeaves("B", "B");
        checkUseNegativeLeaves("not B", "!B");

        checkUseNegativeLeaves("E = E1", "E=E1");
        checkUseNegativeLeaves("E != E1", "E!=E1");
        checkUseNegativeLeaves("!(E = E1)", "E!=E1");
        checkUseNegativeLeaves("!(E != E1)", "E=E1");

        checkUseNegativeLeaves("E in {E1}", "E<:{E1}");
        checkUseNegativeLeaves("not (E in {E1})", "E!<:{E1}");
        checkUseNegativeLeaves("E not in {E1}", "E!<:{E1}");
        checkUseNegativeLeaves("not (E not in {E1})", "E<:{E1}");

        checkUseNegativeLeaves("P = AA", "P=AA");
        checkUseNegativeLeaves("P != AA", "P!=AA");
        checkUseNegativeLeaves("!(P = AA)", "P!=AA");
        checkUseNegativeLeaves("!(P != AA)", "P=AA");
    }

    @Test
    void testDontUseNegativeLeaves() {
        checkDontUseNegativeLeaves("true", "true");
        checkDontUseNegativeLeaves("not true", "false");

        checkDontUseNegativeLeaves("B", "B");
        checkDontUseNegativeLeaves("not B", "!B");

        checkDontUseNegativeLeaves("E = E1", "E=E1");
        checkDontUseNegativeLeaves("E != E1", "!(E=E1)");
        checkDontUseNegativeLeaves("!(E = E1)", "!(E=E1)");
        checkDontUseNegativeLeaves("!(E != E1)", "E=E1");

        checkDontUseNegativeLeaves("E in {E1}", "E<:{E1}");
        checkDontUseNegativeLeaves("not (E in {E1})", "!(E<:{E1})");
        checkDontUseNegativeLeaves("E not in {E1}", "!(E<:{E1})");
        checkDontUseNegativeLeaves("not (E not in {E1})", "E<:{E1}");

        checkDontUseNegativeLeaves("P = AA", "P=AA");
        checkDontUseNegativeLeaves("P != AA", "!(P=AA)");
        checkDontUseNegativeLeaves("!(P = AA)", "!(P=AA)");
        checkDontUseNegativeLeaves("!(P != AA)", "P=AA");
    }

    @Test
    void testDontUseNegationIgnore() {
        checkDontUseNegationIgnore("true", "true");
        checkDontUseNegationIgnore("not true", "false");

        checkDontUseNegationIgnore("B", "B");
        checkDontUseNegationIgnore("not B", "B=false");

        checkDontUseNegationIgnore("E = E1", "E=E1");
        checkDontUseNegationIgnore("E != E1", "E<:{E2,E3}");
        checkDontUseNegationIgnore("!(E = E1)", "E<:{E2,E3}");
        checkDontUseNegationIgnore("!(E != E1)", "E=E1");

        checkDontUseNegationIgnore("E in {E1}", "E<:{E1}");
        checkDontUseNegationIgnore("not (E in {E1})", "E<:{E2,E3}");
        checkDontUseNegationIgnore("E not in {E1}", "E<:{E2,E3}");
        checkDontUseNegationIgnore("not (E not in {E1})", "E<:{E1}");

        checkDontUseNegationIgnore("P = AA", "P=AA");
        checkDontUseNegationIgnore("P != AA", "P!=AA");
        checkDontUseNegationIgnore("!(P = AA)", "!(P=AA)");
        checkDontUseNegationIgnore("!(P != AA)", "P=AA");
    }

    @Test
    void testDontUseNegationThrow() {
        checkDontUseNegationThrow("true", "true");
        checkDontUseNegationThrow("not true", "false");

        checkDontUseNegationThrow("B", "B");
        checkDontUseNegationThrow("not B", "B=false");

        checkDontUseNegationThrow("E = E1", "E=E1");
        checkDontUseNegationThrow("E != E1", "E<:{E2,E3}");
        checkDontUseNegationThrow("!(E = E1)", "E<:{E2,E3}");
        checkDontUseNegationThrow("!(E != E1)", "E=E1");

        checkDontUseNegationThrow("E in {E1}", "E<:{E1}");
        checkDontUseNegationThrow("not (E in {E1})", "E<:{E2,E3}");
        checkDontUseNegationThrow("E not in {E1}", "E<:{E2,E3}");
        checkDontUseNegationThrow("not (E not in {E1})", "E<:{E1}");

        checkDontUseNegationThrow("P = AA", "P=AA");
        assertThrows(IllegalOperationException.class,
                     () -> {
                         checkDontUseNegationThrow("P != AA", null);
                     });
        assertThrows(IllegalOperationException.class,
                     () -> {
                         checkDontUseNegationThrow("!(P = AA)", null);
                     });
        checkDontUseNegationThrow("!(P != AA)", "P=AA");
    }
}