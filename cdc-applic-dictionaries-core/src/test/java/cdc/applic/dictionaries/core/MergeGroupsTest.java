package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.core.visitors.MergeGroups;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.ConvertToNary;

class MergeGroupsTest {
    private final RepositorySupport support = new RepositorySupport();

    public MergeGroupsTest() {
        support.registry.alias().name("A").expression("B").build();
    }

    private void check(String expression,
                       String expected) {
        final Expression x = new Expression(expression);
        final Node node =
                MergeGroups.execute(ConvertToNary.execute(x.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY), support.registry);
        assertEquals(expected, node.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testSimple() {
        check("E=E1", "E=E1");
        check("E=E1 & E=E2", "E=E1&E=E2");
    }

    @Test
    void testBinaryOr() {
        check("E=E1 | E=E1", "E<:{E1}");
        check("E=E1 | E=E2", "E<:{E1,E2}");
        check("E=E1 | true", "E=E1|true");
        check("E<:{E1,E2} | E=E3", "E<:{E1,E2,E3}");
        check("E<:{E1,E2} | E=E2", "E<:{E1,E2}");
        check("true | E=E1", "true|E=E1");
        check("E=E1 | E=E1 & I=10", "E=E1|E=E1&I=10");
        check("E=E1&I=10&R=10.0 | E=E1", "E=E1&I=10&R=10.0|E=E1");
        check("E=E1&I=10&R=10.0 | E=E1&I=11&R=11.0", "E=E1&I=10&R=10.0|E=E1&I=11&R=11.0");
        check("E=E1&I=10 | E=E1&R=10.0", "E=E1&I=10|E=E1&R=10.0");
        check("E=E1&I=10 | E=E1&I=11", "I<:{10~11}&E<:{E1}");
    }

    @Test
    void testNaryOr() {
        check("E=E1 & E=E2 & E=E3", "E=E1&E=E2&E=E3");
        check("E=E1 | true | E=E3", "E<:{E1,E3}|true");
        check("E=E1 | E=E2 | E=E3", "E<:{E1,E2}|E=E3");
        check("E=E1 | E=E1 | E=E3", "E<:{E1}|E=E3");

    }
}