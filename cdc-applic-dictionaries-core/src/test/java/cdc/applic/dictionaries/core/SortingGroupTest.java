package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.core.visitors.SortingGroup;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;

class SortingGroupTest {
    private static final Logger LOGGER = LogManager.getLogger(SortingGroupTest.class);
    private final RepositorySupport support = new RepositorySupport();

    public SortingGroupTest() {
        support.registry.alias().name("A").expression("B").ordinal(20).build();
        support.registry.alias().name("X").expression("B").ordinal(20).build();
    }

    private void checkConstruction(String expression,
                                   String expected) {
        final Expression x = new Expression(expression);
        final SortingGroup sg = SortingGroup.build(x.getRootNode(), support.registry);
        assertEquals(expected, sg.toString());
    }

    private void checkComparator(String expression1,
                                 String expression2,
                                 int expected) {
        LOGGER.debug("checkComparator(" + expression1 + ", " + expression2 + ")");
        final Expression x1 = new Expression(expression1);
        final SortingGroup sg1 = SortingGroup.build(x1.getRootNode(), support.registry);
        final Expression x2 = new Expression(expression2);
        final SortingGroup sg2 = SortingGroup.build(x2.getRootNode(), support.registry);

        LOGGER.debug("   sg1: " + sg1);
        LOGGER.debug("   sg2: " + sg2);

        assertEquals(0, SortingGroup.COMPARATOR.compare(sg1, sg1));
        assertEquals(1, Integer.signum(SortingGroup.COMPARATOR.compare(sg1, null)));
        assertEquals(-1, Integer.signum(SortingGroup.COMPARATOR.compare(null, sg1)));

        final int actual = SortingGroup.COMPARATOR.compare(sg1, sg2);
        assertEquals(Integer.signum(expected), Integer.signum(actual));
    }

    @Test
    void testConstruction() {
        checkConstruction("true", "[[N \"true\"]]");
        checkConstruction("true & true", "[[N \"true\"]]");
        checkConstruction("false", "[[N \"false\"]]");
        checkConstruction("B", "[[P B {true}]]");
        checkConstruction("E=E1", "[[P E {E1}]]");
        checkConstruction("E in {E1,E2}", "[[P E {E1,E2}]]");
        checkConstruction("E in {E2,E1}", "[[P E {E2,E1}]]");
        checkConstruction("I=10", "[[P I {10}]]");
        checkConstruction("I=10 | I=11", "[[P I {10~11}]]");
        checkConstruction("R=10.0", "[[P R {10.0}]]");
        checkConstruction("P=AA", "[[P P {AA}]]");
        checkConstruction("A", "[[A A]]");
        checkConstruction("A & A", "[[A A]]");
        checkConstruction("E=E1&I=10&B", "[[P B {true}][P I {10}][P E {E1}]]");
        checkConstruction("E=E1&I=10&B|true", "[[N \"true\"][P B {true}][P I {10}][P E {E1}]]");
        checkConstruction("E=E1&I=10&B|true|false", "[[N \"false\"][N \"true\"][P B {true}][P I {10}][P E {E1}]]");
    }

    @Test
    void testComparator() {
        checkComparator("true", "true", 0);

        checkComparator("B", "B", 0);

        checkComparator("E=E1", "E=E1", 0);
        checkComparator("E=E1", "E=E2", -1);
        checkComparator("E=E2", "E=E1", 1);

        checkComparator("I=10", "I=10", 0);
        checkComparator("I=10", "I=11", -1);
        checkComparator("I=10", "I=9", 1);

        checkComparator("R=10.0", "R=10.0", 0);
        checkComparator("R=10.0", "R=11.0", -1);
        checkComparator("R=10.0", "R=9.0", 1);

        checkComparator("P=AA", "P=AA", 0);
        checkComparator("P=AA", "P=BB", -1);
        checkComparator("P=BB", "P=AA", 1);

        checkComparator("A", "A", 0);
        checkComparator("A", "X", -1);
    }
}