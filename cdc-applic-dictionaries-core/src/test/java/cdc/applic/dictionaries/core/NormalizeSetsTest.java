package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.core.visitors.NormalizeSets;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.Node;

class NormalizeSetsTest {
    private final RepositorySupport support = new RepositorySupport();

    private void check(String expression,
                       String expected) {
        final Expression x = new Expression(expression);
        final Node node = NormalizeSets.execute(x.getRootNode(), support.registry);
        assertEquals(expected, node.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void test() {
        check("E = E1", "E=E1");
        check("E in {}", "E<:{}");
        check("E in {E1}", "E<:{E1}");
        check("E in {E1,E2,E3}", "E<:{E1,E2,E3}");
        check("E in {E3,E2,E1,E3,E2,E1}", "E<:{E1,E2,E3}");
        check("B in {false,true,false,true}", "B<:{false,true}");
        check("I in {1,10,3~5,5~4}", "I<:{1,3~5,10}");
        check("R in {1.0,10.0,3.0~5.0,5.0~4.0}", "R<:{1.0,3.0~5.0,10.0}");
        check("P in {AC,AB,AA,AC,AB,AA}", "P<:{AA,AB,AC}");
    }
}