package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.core.visitors.ConvertBooleanRefsToSingletons;
import cdc.applic.dictionaries.core.visitors.MergerAtom;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.ConvertEqualitiesToSingletons;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.literals.Name;

class MergerAtomTest {
    private final RepositorySupport support = new RepositorySupport();

    public MergerAtomTest() {
        support.registry.alias().name("A").expression("B").build();
    }

    private MergerAtom ma(String expression) {
        final Expression x = new Expression(expression);
        final Node node = x.getRootNode();
        return MergerAtom.build(node, support.registry);
    }

    private void checkBuild(String expression,
                            boolean valid,
                            boolean negated,
                            String itemName,
                            String content) {
        final Expression x = new Expression(expression);
        final Node node = x.getRootNode();
        final MergerAtom matom = MergerAtom.build(node, support.registry);
        if (valid) {
            final NamedDItem item = support.registry.getItem(Name.of(itemName));
            assertEquals(negated, matom.isNegated());
            assertEquals(item, matom.getItem());
            if (content != null) {
                assertEquals(content, matom.getSet().getContent());
                final SItemSet set = SItemSetUtils.createBest(content);
                assertEquals(set, matom.getSet().getChecked());
            } else {
                assertEquals(null, matom.getSet());
            }
            final Node n = matom.toNode();
            assertEquals(ConvertBooleanRefsToSingletons.execute(ConvertEqualitiesToSingletons.execute(node), support.registry),
                         n);
        } else {
            assertEquals(null, matom);
        }
    }

    @Test
    void testBuild() {
        checkBuild("true", false, false, null, null);
        checkBuild("E=E1", true, false, "E", "E1");
        checkBuild("E!=E1", true, true, "E", "E1");
        checkBuild("E<:{E1}", true, false, "E", "E1");
        checkBuild("E!<:{E1}", true, true, "E", "E1");
        checkBuild("I=10", true, false, "I", "10");

        checkBuild("A", true, false, "A", null);
        checkBuild("B", true, false, "B", "true");
    }

    @Test
    void testMergeWith() {
        assertEquals(ma("E<:{E1,E2}"), ma("E=E1").mergeWith(ma("E=E2")));
        assertEquals(null, ma("E=E1").mergeWith(ma("E!=E2")));
        assertEquals(null, ma("E=E1").mergeWith(ma("I=10")));
        assertEquals(ma("B"), ma("B").mergeWith(ma("B")));
        assertEquals(ma("A"), ma("A").mergeWith(ma("A")));
    }

    @Test
    void testComparator() {
        assertEquals(0, MergerAtom.COMPARATOR.compare(null, null));
        assertTrue(MergerAtom.COMPARATOR.compare(null, ma("E=E1")) < 0);
        assertTrue(MergerAtom.COMPARATOR.compare(ma("E=E1"), null) > 0);
        assertEquals(0, MergerAtom.COMPARATOR.compare(ma("E=E1"), ma("E=E1")));
        assertEquals(0, MergerAtom.COMPARATOR.compare(ma("E=E1"), ma("E<:{E1}")));
        assertTrue(MergerAtom.COMPARATOR.compare(ma("E!=E1"), ma("E=E1")) < 0);
        assertTrue(MergerAtom.COMPARATOR.compare(ma("E=E1"), ma("E!=E1")) > 0);
        assertTrue(MergerAtom.COMPARATOR.compare(ma("E=E1"), ma("I=10")) > 0);
        assertTrue(MergerAtom.COMPARATOR.compare(ma("A"), ma("B")) < 0);
        assertEquals(0, MergerAtom.COMPARATOR.compare(ma("A"), ma("A")));
    }

    @Test
    void testHashCode() {
        assertEquals(ma("E=E1").hashCode(), ma("E=E1").hashCode());
        assertNotEquals(ma("E=E1").hashCode(), ma("E=E2").hashCode());
    }

    @Test
    void testEquals() {
        final MergerAtom ma = ma("E=E1");
        assertEquals(ma, ma);
        assertEquals(ma("E=E1"), ma("E=E1"));
        assertNotEquals(ma, null);
        assertNotEquals(ma, "Hello");
        assertNotEquals(ma("E=E1"), ma("E!=E1"));
        assertNotEquals(ma("E=E1"), ma("E=E2"));
        assertNotEquals(ma("E=E1"), ma("I=10"));
        assertEquals(ma("E=E1"), ma("E<:{E1}"));
    }

    @Test
    void testToString() {
        assertEquals(ma("E=E1").toString(), ma("E=E1").toString());
        assertEquals(ma("E=E1").toString(), ma("E<:{E1}").toString());
        assertEquals(ma("E!=E1").toString(), ma("E!<:{E1}").toString());
        assertNotEquals(ma("E=E1").toString(), ma("E!=E1").toString());
        assertEquals(ma("B").toString(), ma("B").toString());
        assertEquals(ma("A").toString(), ma("A").toString());
    }
}