package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.core.visitors.MergerAtom;
import cdc.applic.dictionaries.core.visitors.MergerGroup;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.ConvertToNary;

class MergerGroupTest {
    private final RepositorySupport support = new RepositorySupport();

    public MergerGroupTest() {
        support.registry.alias().name("A").expression("B").build();
    }

    private MergerAtom ma(String expression) {
        final Expression x = new Expression(expression);
        final Node node = x.getRootNode();
        return MergerAtom.build(node, support.registry);
    }

    private MergerGroup mg(String expression) {
        final Expression x = new Expression(expression);
        final Node node = x.getRootNode();
        return MergerGroup.build(ConvertToNary.execute(node, ConvertToNary.Variant.WHEN_NECESSARY), support.registry);
    }

    private void checkBuild(String expression,
                            boolean valid,
                            MergerAtom... atoms) {
        final Expression x = new Expression(expression);
        final Node node = x.getRootNode();
        final MergerGroup mgroup =
                MergerGroup.build(ConvertToNary.execute(node, ConvertToNary.Variant.WHEN_NECESSARY), support.registry);
        if (valid) {
            assertEquals(Arrays.asList(atoms), mgroup.getAtoms());
            final Node n = mgroup.toNode();
            assertTrue(n instanceof AndNode || n instanceof NaryAndNode || n instanceof AbstractLeafNode);
            if (n instanceof AndNode || n instanceof NaryAndNode) {
                assertEquals(atoms.length, n.getChildrenCount());
            } else {
                assertEquals(atoms.length, 1);
            }
            assertEquals(atoms.length, mgroup.size());
        } else {
            assertEquals(null, mgroup);
        }
    }

    @Test
    void testBuild() {
        checkBuild("true", false);
        checkBuild("E=E1", true, ma("E=E1"));
        checkBuild("E=E1 & I=10", true, ma("I=10"), ma("E=E1"));
        checkBuild("E=E1 | I=10", false);
        checkBuild("E=E1 & true", false);
        checkBuild("true & E=E1", false);
        checkBuild("E=E1 & I=10 & R=10.0", true, ma("I=10"), ma("R=10.0"), ma("E=E1"));
        checkBuild("E=E1 & true & R=10.0", false);
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         mg("E=E1 & E=E2");
                     });
    }

    @Test
    void testIntersectionWith() {
        assertEquals(mg("E=E1"), mg("E=E1 & I=10").intersectionWith(mg("E=E1 & I=11")));
        assertEquals(mg("E=E1"), mg("E=E1 & B").intersectionWith(mg("E=E1 & I=11")));
        assertEquals(mg("E=E1 & I=10"), mg("E=E1 & I=10").intersectionWith(mg("E=E1 & I=10 & R=10.0")));
        assertEquals(MergerGroup.EMPTY, mg("E=E1").intersectionWith(mg("E=E2")));
    }

    @Test
    void testMergeWith() {
        assertEquals(mg("E<:{E1,E2}"), mg("E=E1").mergeWith(mg("E=E2"), MergerGroup.EMPTY));
        assertEquals(mg("I<:{10}&E<:{E1,E2}"), mg("E=E1 & I=10").mergeWith(mg("E=E2 & I=10"), mg("I=10")));
        assertEquals(null, mg("I=10").mergeWith(mg("E=E2 & I=10"), mg("I=10")));
        assertEquals(null, mg("E=E1 & I=10").mergeWith(mg("B & I=10"), mg("I=10")));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         mg("E=E1 & I=10").mergeWith(mg("E=E2 & I=11"), mg("E=E1"));
                     });
    }

    @Test
    void testEquals() {
        final MergerGroup mg = mg("E=E1 & I=10");
        assertEquals(mg, mg);
        assertEquals(mg("E=E1 & I=10"), mg("E=E1 & I=10"));
        assertEquals(mg("E=E1 & I=10"), mg("I=10 & E=E1"));
        assertEquals(mg("E=E1 & I=10"), mg("I<:{10} & E<:{E1}"));
        assertNotEquals(mg, null);
        assertNotEquals(mg, "Hello");
    }

    @Test
    void testComparator() {
        assertEquals(0, MergerGroup.COMPARATOR.compare(null, null));
        assertTrue(MergerGroup.COMPARATOR.compare(null, mg("E=E1")) < 0);
        assertTrue(MergerGroup.COMPARATOR.compare(mg("E=E1"), null) > 0);
        assertEquals(0, MergerGroup.COMPARATOR.compare(mg("E=E1"), mg("E=E1")));
        assertTrue(MergerGroup.COMPARATOR.compare(mg("E=E1"), mg("E=E2")) < 0);
        assertTrue(MergerGroup.COMPARATOR.compare(mg("E=E2"), mg("E=E1")) > 0);
        assertTrue(MergerGroup.COMPARATOR.compare(mg("E=E1"), mg("I=10")) > 0);
        assertTrue(MergerGroup.COMPARATOR.compare(mg("E=E1&I=10"), mg("I=10")) > 0);
        assertTrue(MergerGroup.COMPARATOR.compare(mg("E=E1&I=9"), mg("I=10")) < 0);
    }

    @Test
    void testHashCode() {
        assertEquals(mg("E=E1 & I=10").hashCode(), mg("E=E1 & I=10").hashCode());
        assertEquals(mg("E=E1 & I=10").hashCode(), mg("I<:{10} & E<:{E1}").hashCode());
    }

    @Test
    void testToString() {
        assertEquals(mg("E=E1 & I=10").toString(), mg("E=E1 & I=10").toString());
        assertEquals(mg("E=E1 & I=10").toString(), mg("I<:{10} & E<:{E1}").toString());
    }
}