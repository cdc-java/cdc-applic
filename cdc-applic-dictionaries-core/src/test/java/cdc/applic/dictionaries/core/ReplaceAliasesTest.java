package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.core.visitors.ReplaceAliases;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.Node;

class ReplaceAliasesTest {
    private static final Logger LOGGER = LogManager.getLogger(ReplaceAliasesTest.class);
    private final RepositorySupport support = new RepositorySupport();

    ReplaceAliasesTest() {
        support.registry.alias().name("A1").expression("I=10").ordinal(0).build();
        support.registry.alias().name("A2").expression("A1").ordinal(1).build();
    }

    private void check(String expression,
                       String expected) {
        LOGGER.debug("expression: {}", expression);
        final Expression x = new Expression(expression);
        final Node node = ReplaceAliases.execute(x.getRootNode(), support.registry);
        assertEquals(expected, node.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void test() {
        check("true", "true");
        check("A1", "I=10");
        check("A2", "I=10");
        check("A1 or I=11", "I=10|I=11");
        check("A2 or I=11", "I=10|I=11");
    }
}