package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.core.visitors.SortLeaves;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.ConvertToNary;

class SortLeavesTest {
    private final RepositorySupport support = new RepositorySupport();

    private void check(String expression,
                       String expected) {
        final Expression x = new Expression(expression);
        final Node node = SortLeaves.execute(ConvertToNary.execute(x.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY),
                                             support.registry);
        assertEquals(expected, node.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testBinary() {
        // False < True < B < I < R < P < E

        check("true & false", "false&true");
        check("false & true", "false&true");

        check("true & B", "true&B");
        check("false & B", "false&B");

        check("B & I=10", "B&I=10");
        check("I=10 & B", "B&I=10");
        check("B & R=10.0", "B&R=10.0");
        check("R=10.0 & B", "B&R=10.0");
        check("B & P=AA", "B&P=AA");
        check("P=AA & B", "B&P=AA");
        check("B & E=E1", "B&E=E1");
        check("E=E1&B", "B&E=E1");

        check("I=10&R=10.0", "I=10&R=10.0");
        check("R=10.0&I=10", "I=10&R=10.0");
        check("I=10&P=AA", "I=10&P=AA");
        check("P=AA&I=10", "I=10&P=AA");
        check("I=10&E=E1", "I=10&E=E1");
        check("E=E1&I=10", "I=10&E=E1");

        check("R=10.0&P=AA", "R=10.0&P=AA");
        check("P=AA&R=10.0", "R=10.0&P=AA");
        check("R=10.0&E=E1", "R=10.0&E=E1");
        check("E=E1&R=10.0", "R=10.0&E=E1");

        check("P=AA&E=E1", "P=AA&E=E1");
        check("E=E1&P=AA", "P=AA&E=E1");

        check("(true or false) and (true or false)", "(false|true)&(false|true)");
        check("false and (true or false)", "false&(false|true)");
        check("(true or false) and false", "(false|true)&false");
    }

    @Test
    void testNary() {
        // True < False < B < I < R < P < E

        check("false & true & B", "false&true&B");
        check("B & true & false", "false&true&B");

        check("false & true & B & I=10 & R=10.0 & P=AA & E=E1", "false&true&B&I=10&R=10.0&P=AA&E=E1");
        check("E=E1 & P=AA & R=10.0 & I=10 & B & true & false", "false&true&B&I=10&R=10.0&P=AA&E=E1");

        check("(true or false) and (true or false) and (true or false)", "(false|true)&(false|true)&(false|true)");
    }
}