package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.core.utils.CutPointBuffers;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerDomain;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.literals.Name;

class CutPointBuffersTest {
    RepositorySupport support = new RepositorySupport();

    @Test
    void test() {
        final CutPointBuffers<IntegerDomain, IntegerValue, IntegerRange> cpb = new CutPointBuffers<>(IntegerDomain.INSTANCE);
        final Property i = support.registry.getProperty(Name.of("I"));
        final Property r = support.registry.getProperty(Name.of("R"));
        assertTrue(cpb.getCutRanges(i).isEmpty());
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         cpb.getEquivalence(i, BooleanValue.TRUE);
                     });
        cpb.add(i, IntegerValue.of(1));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         cpb.add(i, BooleanValue.TRUE);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         cpb.getEquivalence(r, RealValue.of(0.0));
                     });
        cpb.clear();
    }
}