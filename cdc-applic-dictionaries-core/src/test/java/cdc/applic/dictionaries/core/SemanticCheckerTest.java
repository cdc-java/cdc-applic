package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.SemanticException;
import cdc.applic.dictionaries.checks.SemanticChecker;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.checks.CheckedData;

class SemanticCheckerTest {
    private final RepositorySupport support = new RepositorySupport();

    SemanticCheckerTest() {
        support.registry.policy().name("Policy").build();
    }

    @Test
    void testRegistry() {
        final SemanticChecker checker = new SemanticChecker(support.registry);
        assertEquals(SemanticChecker.RULE_NAME, checker.getRuleName());
        assertFalse(checker.isCompliant(new CheckedData(new Expression("E=E4"))));
        assertTrue(checker.isCompliant(new CheckedData(new Expression("E=E1"))));

        checker.checkCompliance(new CheckedData(new Expression("E=E1")));
        checker.checkCompliance(new CheckedData(new Expression("true")));
        assertThrows(SemanticException.class,
                     () -> {
                         checker.checkCompliance(new CheckedData(new Expression("E=E4")));
                     });
        assertThrows(SemanticException.class,
                     () -> {
                         checker.checkCompliance(new CheckedData(new Expression("E in {E4}")));
                     });
        assertThrows(SemanticException.class,
                     () -> {
                         checker.checkCompliance(new CheckedData(new Expression("F = F1")));
                     });
        assertThrows(SemanticException.class,
                     () -> {
                         checker.checkCompliance(new CheckedData(new Expression("E")));
                     });
        assertThrows(SemanticException.class,
                     () -> {
                         checker.checkCompliance(new CheckedData(new Expression("F")));
                     });
    }

    @Test
    void testDictionary() {
        final SemanticChecker checker = new SemanticChecker(support.repository.getDictionary("/Registry/Policy"));
        assertFalse(checker.isCompliant(new CheckedData(new Expression("E=E1"))));
    }
}