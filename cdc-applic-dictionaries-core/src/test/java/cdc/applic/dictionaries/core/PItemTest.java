package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.Assertion;
import cdc.applic.dictionaries.items.DItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.literals.Name;

class DItemTest {
    RepositorySupport support = new RepositorySupport();

    public DItemTest() {
        support.registry.alias().name("A").expression("true").build();
    }

    @Test
    void testComparator() {
        final Property e = support.registry.getProperty(Name.of("E"));
        final Assertion x = support.registry.createAssertion("true");
        final Alias a = support.registry.getAlias(Name.of("A"));
        assertEquals(0, DItem.COMPARATOR.compare(e, e));
        assertEquals(0, DItem.COMPARATOR.compare(x, x));
        assertEquals(0, DItem.COMPARATOR.compare(a, a));
        assertTrue(DItem.COMPARATOR.compare(e, x) < 0);
        assertTrue(DItem.COMPARATOR.compare(e, a) < 0);
        assertTrue(DItem.COMPARATOR.compare(a, x) < 0);
    }
}
