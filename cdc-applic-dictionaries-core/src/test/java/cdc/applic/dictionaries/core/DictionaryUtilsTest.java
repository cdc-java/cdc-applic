package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Comparator;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.DictionaryUtils;
import cdc.applic.dictionaries.SemanticException;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.SortableNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.literals.Name;

class DictionaryUtilsTest {
    private final RepositorySupport support = new RepositorySupport();
    private final SortableNode t = TrueNode.INSTANCE;
    private final SortableNode f = FalseNode.INSTANCE;
    private final SortableNode bRef = new RefNode(Name.of("B"));
    private final SortableNode iEqual = new EqualNode(Name.of("I"), IntegerValue.of(10));
    private final SortableNode rEqual = new EqualNode(Name.of("R"), RealValue.of(10.0));
    private final SortableNode pEqual = new EqualNode(Name.of("P"), StringValue.of("AA"));
    private final SortableNode eEqual = new EqualNode(Name.of("E"), StringValue.of("E1"));

    public DictionaryUtilsTest() {
        support.registry.alias().name("A").expression("true").build();
    }

    @Test
    void testGetOrdinal() {
        // B < I < R < P < E
        assertEquals(Integer.MIN_VALUE, DictionaryUtils.getOrdinal(f, support.registry));
        assertEquals(Integer.MIN_VALUE, DictionaryUtils.getOrdinal(t, support.registry));
        assertEquals(0, DictionaryUtils.getOrdinal(bRef, support.registry));
        assertEquals(1, DictionaryUtils.getOrdinal(iEqual, support.registry));
        assertEquals(2, DictionaryUtils.getOrdinal(rEqual, support.registry));
        assertEquals(3, DictionaryUtils.getOrdinal(pEqual, support.registry));
        assertEquals(4, DictionaryUtils.getOrdinal(eEqual, support.registry));
    }

    @Test
    void testOrdinalNameComparator() {
        final Comparator<SortableNode> comparator = DictionaryUtils.createOrdinaNameComparator(support.registry);
        assertEquals(0, comparator.compare(f, f));
        assertEquals(0, comparator.compare(t, t));
        assertEquals(0, comparator.compare(bRef, bRef));
        assertEquals(0, comparator.compare(iEqual, iEqual));
        assertEquals(0, comparator.compare(rEqual, rEqual));
        assertEquals(0, comparator.compare(pEqual, pEqual));
        assertEquals(0, comparator.compare(eEqual, eEqual));

        assertTrue(comparator.compare(f, t) < 0);
        assertTrue(comparator.compare(t, f) > 0);
    }

    @Test
    void testMisc() {
        assertNotNull(support.registry.getProperty(Name.of("B")));
        assertNotNull(support.registry.getItem(Name.of("B")));
        assertTrue(support.registry.hasItem(Name.of("B")));
        assertFalse(support.registry.hasItem(Name.of("X")));
        assertThrows(SemanticException.class,
                     () -> {
                         support.registry.getAlias(Name.of("X"));
                     });
        assertThrows(SemanticException.class,
                     () -> {
                         support.registry.getAlias(Name.of("I"));
                     });
        assertThrows(SemanticException.class,
                     () -> {
                         support.registry.getProperty(Name.of("X"));
                     });
        assertThrows(SemanticException.class,
                     () -> {
                         support.registry.getProperty(Name.of("A"));
                     });
        assertThrows(SemanticException.class,
                     () -> {
                         support.registry.getItem(Name.of("X"));
                     });
        assertThrows(SemanticException.class,
                     () -> {
                         DictionaryUtils.getAvailableBooleanProperty(Name.of("X"), support.registry);
                     });
        assertThrows(SemanticException.class,
                     () -> {
                         DictionaryUtils.getAvailableBooleanProperty(Name.of("I"), support.registry);
                     });
    }
}