package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.core.visitors.EliminateFullAndEmptySets;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.Node;

class EliminateFullAndEmptySetsTest {
    private final RepositorySupport support = new RepositorySupport();

    public EliminateFullAndEmptySetsTest() {
        support.registry.enumeratedType().name("S").frozen(false).literals("S1").build();
        support.registry.property().name("S").type("S").build();
    }

    private void check(String expression,
                       ReserveStrategy reserveStrategy,
                       String expected) {
        final Expression x = new Expression(expression);
        final Node node = EliminateFullAndEmptySets.execute(x.getRootNode(), support.registry, reserveStrategy);
        assertEquals(expected, node.toInfix(Formatting.SHORT_NARROW));
    }

    private void checkNoReserves(String expression,
                                 String expected) {
        check(expression, ReserveStrategy.NO_RESERVES, expected);
    }

    private void checkAllPossibleReserves(String expression,
                                          String expected) {
        check(expression, ReserveStrategy.ALL_POSSIBLE_RESERVES, expected);
    }

    @Test
    void testNoReserves() {
        checkNoReserves("B", "B");
        checkNoReserves("E = E1", "E=E1");
        checkNoReserves("E in {}", "false");
        checkNoReserves("E not in {}", "true");
        checkNoReserves("not (E in {})", "!false");
        checkNoReserves("E in {E1}", "E<:{E1}");
        checkNoReserves("E in {E1,E2,E3}", "true");
        checkNoReserves("E not in {E1,E2,E3}", "false");
        checkNoReserves("not (E in {E1,E2,E3})", "!true");
        checkNoReserves("E in {E3,E2,E1,E3,E2,E1}", "true");
        checkNoReserves("B in {false,true,false,true}", "true");
        checkNoReserves("I in {1,10,3~5,5~4}", "I<:{1,3~5,10}");
        checkNoReserves("R in {1.0,10.0,3.0~5.0,5.0~4.0}", "R<:{1.0,3.0~5.0,10.0}");
        checkNoReserves("P in {}", "false");
        checkNoReserves("P not in {}", "true");
        checkNoReserves("P in {AC,AB,AA,AC,AB,AA}", "P<:{AC,AB,AA}");
        checkNoReserves("P = AA", "P=AA");
        checkNoReserves("S = S1", "true");
        checkNoReserves("S != S1", "false");
    }

    @Test
    void testAllPossibleReserves() {
        checkAllPossibleReserves("E = E1", "E=E1");
        checkAllPossibleReserves("E in {}", "false");
        checkAllPossibleReserves("E in {E1}", "E<:{E1}");
        checkAllPossibleReserves("E in {E1,E2,E3}", "E<:{E1,E2,E3}");
        checkAllPossibleReserves("E in {E3,E2,E1,E3,E2,E1}", "E<:{E3,E2,E1}");
        checkAllPossibleReserves("B in {false,true,false,true}", "true");
        checkAllPossibleReserves("I in {1,10,3~5,5~4}", "I<:{1,3~5,10}");
        checkAllPossibleReserves("R in {1.0,10.0,3.0~5.0,5.0~4.0}", "R<:{1.0,3.0~5.0,10.0}");
        checkAllPossibleReserves("P in {AC,AB,AA,AC,AB,AA}", "P<:{AC,AB,AA}");
        checkAllPossibleReserves("S = S1", "S=S1");
        checkAllPossibleReserves("S != S1", "S!=S1");
    }
}