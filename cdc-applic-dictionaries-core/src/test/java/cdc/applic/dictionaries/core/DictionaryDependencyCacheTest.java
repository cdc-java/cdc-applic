package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.Assertion;
import cdc.applic.dictionaries.items.Property;

class DictionaryDependencyCacheTest {
    private static final Logger LOGGER = LogManager.getLogger(DictionaryDependencyCacheTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    @SafeVarargs
    private static <X> Set<X> of(X... values) {
        final HashSet<X> result = new HashSet<>();
        for (final X x : values) {
            result.add(x);
        }
        return result;
    }

    @Test
    void test1() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        registry.booleanType().name("B").build();
        registry.enumeratedType().name("E").frozen(false).literals("E1", "E2").build();
        registry.integerType().name("I").frozen(false).domain("1~99").build();
        registry.realType().name("R").frozen(false).domain("1.0~99.0").build();
        registry.patternType().name("P").frozen(false).pattern("[A-Z]{2}").build();

        final DictionaryHandle handle = new DictionaryHandle(registry);
        handle.print(OUT);
        assertTrue(handle.getIds().isEmpty());
        final DictionaryDependencyCache cache = DictionaryDependencyCache.get(handle);
        handle.print(OUT);
        assertSame(1, handle.getIds().size());

        registry.property().name("B").type("B").ordinal(0).build();
        registry.property().name("E").type("E").ordinal(1).build();
        registry.property().name("I").type("I").ordinal(2).build();
        registry.property().name("R").type("R").ordinal(3).build();
        registry.property().name("P").type("P").ordinal(4).build();

        registry.alias().name("X").expression("E = E1 or B").ordinal(0).build();
        registry.alias().name("Y").expression("I in {1~10}").ordinal(0).build();
        registry.createAssertion("X");

        handle.invalidate();
        cache.validate();
        handle.print(OUT);
    }

    @Test
    void test2() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        registry.enumeratedType().name("S_Type").frozen(false).literals("S1", "S2").build();
        registry.enumeratedType().name("F_Type").frozen(false).literals("F1", "F2").build();

        final DictionaryHandle handle = new DictionaryHandle(registry);
        handle.print(OUT);
        assertTrue(handle.getIds().isEmpty());
        final DictionaryDependencyCache cache = DictionaryDependencyCache.get(handle);
        handle.print(OUT);
        assertSame(1, handle.getIds().size());

        final Property s = registry.property().name("S").type("S_Type").ordinal(0).build();
        final Property f = registry.property().name("F").type("F_Type").ordinal(1).build();

        final Assertion x1 = registry.createAssertion("F=F1 -> S=S1");
        final Assertion x2 = registry.createAssertion("F=F1");

        handle.invalidate();
        cache.validate();
        handle.print(OUT);

        assertEquals(of(s), cache.getRelatedItems(s, AssertionStrategy.EXCLUDE_ASSERTIONS));
        assertEquals(of(f), cache.getRelatedItems(f, AssertionStrategy.EXCLUDE_ASSERTIONS));

        assertEquals(of(s, x1, x2, f), cache.getRelatedItems(s, AssertionStrategy.INCLUDE_ASSERTIONS));
        assertEquals(of(s, x1, x2, f), cache.getRelatedItems(f, AssertionStrategy.INCLUDE_ASSERTIONS));
    }

    @Test
    void test3() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        registry.booleanType().name("B_Type").build();

        final DictionaryHandle handle = new DictionaryHandle(registry);
        handle.print(OUT);
        assertTrue(handle.getIds().isEmpty());
        final DictionaryDependencyCache cache = DictionaryDependencyCache.get(handle);
        handle.print(OUT);
        assertSame(1, handle.getIds().size());

        final Property p1 = registry.property().name("P1").type("B_Type").build();
        final Property p2 = registry.property().name("P2").type("B_Type").build();
        final Property p3 = registry.property().name("P3").type("B_Type").build();
        final Property p4 = registry.property().name("P4").type("B_Type").build();
        final Property p5 = registry.property().name("P5").type("B_Type").build();

        final Alias a1 = registry.alias().name("A1").expression("P3 or P4").build();
        final Alias a2 = registry.alias().name("A2").expression("P4 or P5").build();
        final Alias a3 = registry.alias().name("A3").expression("A1 or A2").build();

        final Assertion x1 = registry.createAssertion("P1 or P3");
        final Assertion x2 = registry.createAssertion("P2 or A1");

        handle.invalidate();
        cache.validate();
        handle.print(OUT);

        assertEquals(of(p1), cache.getRelatedItems(p1, AssertionStrategy.EXCLUDE_ASSERTIONS));
        assertEquals(of(p2), cache.getRelatedItems(p2, AssertionStrategy.EXCLUDE_ASSERTIONS));
        assertEquals(of(p3), cache.getRelatedItems(p3, AssertionStrategy.EXCLUDE_ASSERTIONS));
        assertEquals(of(p4), cache.getRelatedItems(p4, AssertionStrategy.EXCLUDE_ASSERTIONS));
        assertEquals(of(p5), cache.getRelatedItems(p5, AssertionStrategy.EXCLUDE_ASSERTIONS));

        assertEquals(of(a1), cache.getRelatedItems(a1, AssertionStrategy.EXCLUDE_ASSERTIONS));
        assertEquals(of(a2), cache.getRelatedItems(a2, AssertionStrategy.EXCLUDE_ASSERTIONS));
        assertEquals(of(a3), cache.getRelatedItems(a3, AssertionStrategy.EXCLUDE_ASSERTIONS));

        assertEquals(of(p1, p2, p3, p4, a1, x1, x2), cache.getRelatedItems(p1, AssertionStrategy.INCLUDE_ASSERTIONS));
        assertEquals(of(p1, p2, p3, p4, a1, x1, x2), cache.getRelatedItems(p2, AssertionStrategy.INCLUDE_ASSERTIONS));
        assertEquals(of(p1, p2, p3, p4, a1, x1, x2), cache.getRelatedItems(p3, AssertionStrategy.INCLUDE_ASSERTIONS));
        assertEquals(of(p1, p2, p3, p4, a1, x1, x2), cache.getRelatedItems(p4, AssertionStrategy.INCLUDE_ASSERTIONS));
        assertEquals(of(p5), cache.getRelatedItems(p5, AssertionStrategy.INCLUDE_ASSERTIONS));

        assertEquals(of(p1, p2, p3, p4, a1, x1, x2), cache.getRelatedItems(a1, AssertionStrategy.INCLUDE_ASSERTIONS));
        assertEquals(of(a2), cache.getRelatedItems(a2, AssertionStrategy.INCLUDE_ASSERTIONS));
        assertEquals(of(a3), cache.getRelatedItems(a3, AssertionStrategy.INCLUDE_ASSERTIONS));
    }
}