package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.core.utils.SItemSetOperations;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.RealType;
import cdc.applic.expressions.IllegalOperationException;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.content.UncheckedSet;
import cdc.applic.expressions.content.Value;

class SItemSetOperationsTest {
    @Test
    void testBooleanOperations() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final BooleanType type = registry.booleanType().name("B").build();
        assertEquals(BooleanSet.FALSE_TRUE, SItemSetOperations.booleanFullSet());
        assertEquals(BooleanSet.TRUE, SItemSetOperations.booleanComplement(BooleanValue.FALSE));
        assertEquals(BooleanSet.FALSE, SItemSetOperations.booleanComplement(BooleanValue.TRUE));
        assertEquals(BooleanSet.EMPTY, SItemSetOperations.booleanComplement(BooleanSet.FALSE_TRUE));
        assertEquals(BooleanSet.TRUE, SItemSetOperations.complement(BooleanValue.FALSE, type));
        assertEquals(BooleanSet.EMPTY, SItemSetOperations.complement(BooleanSet.FALSE_TRUE, type));

        assertEquals(BooleanSet.FALSE_TRUE, SItemSetOperations.fullSet(type));
        assertTrue(SItemSetOperations.isFullSet(BooleanSet.FALSE_TRUE, type));
        assertFalse(SItemSetOperations.isFullSet(BooleanSet.FALSE, type));
        assertFalse(SItemSetOperations.isFullSet(BooleanValue.FALSE, type));
        assertEquals(BooleanSet.FALSE_TRUE, SItemSetOperations.complement(BooleanSet.EMPTY, type));
    }

    @Test
    void testEnumeratedOperations() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final EnumeratedType type = registry.enumeratedType().name("E").frozen(false).literals("E1", "E2", "E3").build();
        assertEquals(StringSet.of("E1, E2, E3"), SItemSetOperations.enumeratedFullSet(type));
        assertEquals(StringSet.of("E2, E3"), SItemSetOperations.enumeratedComplement(StringValue.of("E1"), type));
        assertEquals(StringSet.of("E2, E3"), SItemSetOperations.complement(StringValue.of("E1"), type));
        assertEquals(StringSet.of("E2, E3"), SItemSetOperations.enumeratedComplement(StringSet.of("E1"), type));
        assertEquals(StringSet.of("E2, E3"), SItemSetOperations.complement(StringSet.of("E1"), type));

        assertEquals(StringSet.of("E1, E2, E3"), SItemSetOperations.fullSet(type));
        assertTrue(SItemSetOperations.isFullSet((SItemSet) StringSet.of("E1, E2, E3"), type));
        assertFalse(SItemSetOperations.isFullSet((SItemSet) StringSet.of("E1, E2"), type));
        assertFalse(SItemSetOperations.isFullSet((Value) StringValue.of("E1"), type));
    }

    @Test
    void testIntegerOperations() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final IntegerType type = registry.integerType().name("I").frozen(false).domain("1~100").build();
        assertEquals(IntegerSet.of("1~100"), SItemSetOperations.integerFullSet(type));
        assertEquals(IntegerSet.of("1~10,12~100"), SItemSetOperations.integerComplement(IntegerValue.of(11), type));
        assertEquals(IntegerSet.of("1~10,12~100"), SItemSetOperations.complement(IntegerValue.of(11), type));
        assertEquals(IntegerSet.of("1~10,21~100"), SItemSetOperations.integerComplement(IntegerSet.of("11~20"), type));
        assertEquals(IntegerSet.of("1~10,21~100"), SItemSetOperations.complement(IntegerSet.of("11~20"), type));

        assertEquals(IntegerSet.of("1~100"), SItemSetOperations.fullSet(type));
        assertTrue(SItemSetOperations.isFullSet((SItemSet) IntegerSet.of("1~100"), type));
        assertFalse(SItemSetOperations.isFullSet((SItemSet) IntegerSet.of("1~10"), type));
        assertFalse(SItemSetOperations.isFullSet((Value) IntegerValue.of("10"), type));
    }

    @Test
    void testRealOperations() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final RealType type = registry.realType().name("R").frozen(false).domain("1.0~100.0").build();
        assertEquals(RealSet.of("1.0~100.0"), SItemSetOperations.realFullSet(type));
        assertEquals(RealSet.of("1.0~10.999999999999998,11.000000000000002~100.0"),
                     SItemSetOperations.realComplement(RealValue.of(11.0), type));
        assertEquals(RealSet.of("1.0~10.999999999999998,11.000000000000002~100.0"),
                     SItemSetOperations.complement(RealValue.of(11.0), type));
        assertEquals(RealSet.of("1.0~10.999999999999998,20.000000000000004~100.0"),
                     SItemSetOperations.realComplement(RealSet.of("11.0~20.0"), type));
        assertEquals(RealSet.of("1.0~10.999999999999998,20.000000000000004~100.0"),
                     SItemSetOperations.complement(RealSet.of("11.0~20.0"), type));

        assertEquals(RealSet.of("1.0~100.0"), SItemSetOperations.fullSet(type));
        assertTrue(SItemSetOperations.isFullSet((SItemSet) RealSet.of("1.0~100.0"), type));
        assertFalse(SItemSetOperations.isFullSet((SItemSet) RealSet.of("1.0~10.0"), type));
        assertFalse(SItemSetOperations.isFullSet((Value) RealValue.of("10.0"), type));
    }

    @Test
    void testPatternOperations() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final PatternType type = registry.patternType().name("P").frozen(false).pattern("[A-Z]{2}").build();
        assertThrows(IllegalOperationException.class,
                     () -> {
                         SItemSetOperations.fullSet(type);
                     });
        assertThrows(IllegalOperationException.class,
                     () -> {
                         SItemSetOperations.complement(StringSet.of("AA"), type);
                     });
        assertThrows(IllegalOperationException.class,
                     () -> {
                         SItemSetOperations.complement(StringSet.EMPTY, type);
                     });
        assertThrows(IllegalOperationException.class,
                     () -> {
                         SItemSetOperations.isFullSet(StringSet.of("AA"), type);
                     });
        assertThrows(IllegalOperationException.class,
                     () -> {
                         SItemSetOperations.isFullSet(StringValue.of("AA"), type);
                     });
        assertThrows(IllegalOperationException.class,
                     () -> {
                         SItemSetOperations.complement(StringValue.of("AA"), type);
                     });
    }

    @Test
    void testUnchekedOperations() {
        final UncheckedSet set = UncheckedSet.of("A,1");
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         SItemSetOperations.complement(set, null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         SItemSetOperations.isFullSet(set, null);
                     });
    }
}