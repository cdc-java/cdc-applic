package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.core.visitors.ConvertToPositiveLeaves;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.IllegalOperationException;

class ConvertToPositiveLeavesTest {
    private final RepositorySupport support = new RepositorySupport();

    private void check(String expression,
                       ConvertToPositiveLeaves.Variant variant,
                       ReserveStrategy reserveStrategy,
                       String expected) {
        final Expression x1 = new Expression(expression);
        final Expression x2 = ConvertToPositiveLeaves.execute(x1.getRootNode(),
                                                              variant,
                                                              support.registry,
                                                              reserveStrategy)
                                                     .toExpression(Formatting.SHORT_NARROW);
        assertEquals(expected, x2.toString());
    }

    private void checkNoReservesIgnore(String expression,
                                       String expected) {
        check(expression,
              ConvertToPositiveLeaves.Variant.IGNORE_WHEN_IMPOSSIBLE,
              ReserveStrategy.NO_RESERVES,
              expected);
    }

    private void checkNoReservesThrow(String expression,
                                      String expected) {
        check(expression,
              ConvertToPositiveLeaves.Variant.THROW_WHEN_IMPOSSIBLE,
              ReserveStrategy.NO_RESERVES,
              expected);
    }

    private void checkAllPossibleReservesIgnore(String expression,
                                                String expected) {
        check(expression,
              ConvertToPositiveLeaves.Variant.IGNORE_WHEN_IMPOSSIBLE,
              ReserveStrategy.ALL_POSSIBLE_RESERVES,
              expected);
    }

    private void checkAllPossibleReservesThrow(String expression,
                                               String expected) {
        check(expression,
              ConvertToPositiveLeaves.Variant.THROW_WHEN_IMPOSSIBLE,
              ReserveStrategy.ALL_POSSIBLE_RESERVES,
              expected);
    }

    @Test
    void testUncheckedNoReserves() {
        // This converter does not check semantic when no conversion is done
        checkNoReservesIgnore("I = A", "I=A");
        checkNoReservesIgnore("P = 10", "P=10");
        checkNoReservesIgnore("R = A", "R=A");

        checkNoReservesThrow("I = A", "I=A");
        checkNoReservesThrow("P = 10", "P=10");
        checkNoReservesThrow("R = A", "R=A");
    }

    @Test
    void testUncheckedAllPossibleReserves() {
        // This converter does not check semantic when no conversion is done
        checkAllPossibleReservesIgnore("I = A", "I=A");
        checkAllPossibleReservesIgnore("P = 10", "P=10");
        checkAllPossibleReservesIgnore("R = A", "R=A");

        checkAllPossibleReservesThrow("I = A", "I=A");
        checkAllPossibleReservesThrow("P = 10", "P=10");
        checkAllPossibleReservesThrow("R = A", "R=A");
    }

    @Test
    void testBooleanNoReserves() {
        checkNoReservesIgnore("true", "true");
        checkNoReservesIgnore("false", "false");
        checkNoReservesIgnore("not true", "false");
        checkNoReservesIgnore("not false", "true");
        checkNoReservesIgnore("not (true or false)", "!(true|false)");

        checkNoReservesIgnore("B", "B");
        checkNoReservesIgnore("not B", "B=false");
        checkNoReservesIgnore("B = false", "B=false");
        checkNoReservesIgnore("B = true", "B=true");
    }

    @Test
    void testBooleanAllPossibleReserves() {
        checkAllPossibleReservesIgnore("true", "true");
        checkAllPossibleReservesIgnore("false", "false");
        checkAllPossibleReservesIgnore("not true", "false");
        checkAllPossibleReservesIgnore("not false", "true");
        checkAllPossibleReservesIgnore("not (true or false)", "!(true|false)");

        checkAllPossibleReservesIgnore("B", "B");
        checkAllPossibleReservesIgnore("not B", "B=false");
        checkAllPossibleReservesIgnore("B = false", "B=false");
        checkAllPossibleReservesIgnore("B = true", "B=true");
    }

    @Test
    void testEnumeratedNoReserves() {
        checkNoReservesIgnore("E = E1", "E=E1");
        checkNoReservesIgnore("E in {E1}", "E<:{E1}");
        checkNoReservesIgnore("E != E1", "E<:{E2,E3}");
        checkNoReservesIgnore("!(E = E1)", "E<:{E2,E3}");
        checkNoReservesIgnore("!(E != E1)", "E=E1");
        checkNoReservesIgnore("E !<: {E1}", "E<:{E2,E3}");
        checkNoReservesIgnore("!(E !<: {E1})", "E<:{E1}");
        checkNoReservesIgnore("!(E <: {E1})", "E<:{E2,E3}");

        checkNoReservesThrow("!(E <: {E1})", "E<:{E2,E3}");
    }

    @Test
    void testEnumeratedAllPossibleReserves() {
        checkAllPossibleReservesIgnore("E = E1", "E=E1");
        checkAllPossibleReservesIgnore("E in {E1}", "E<:{E1}");
        checkAllPossibleReservesIgnore("E != E1", "E!=E1");
        checkAllPossibleReservesIgnore("!(E = E1)", "!(E=E1)");
        checkAllPossibleReservesIgnore("!(E != E1)", "E=E1");
        checkAllPossibleReservesIgnore("E !<: {E1}", "E!<:{E1}");
        checkAllPossibleReservesIgnore("!(E !<: {E1})", "E<:{E1}");
        checkAllPossibleReservesIgnore("!(E <: {E1})", "!(E<:{E1})");

        assertThrows(IllegalOperationException.class,
                     () -> {
                         checkAllPossibleReservesThrow("!(E <: {E1})", null);
                     });
    }

    @Test
    void testIntegerNoReserves() {
        checkNoReservesIgnore("I = 10", "I=10");
        checkNoReservesIgnore("I != 10", "I<:{1~9,11~999}");
        checkNoReservesIgnore("!(I = 10)", "I<:{1~9,11~999}");
        checkNoReservesIgnore("!(I != 10)", "I=10");
        checkNoReservesIgnore("I <: {10}", "I<:{10}");
        checkNoReservesIgnore("I !<: {10}", "I<:{1~9,11~999}");
        checkNoReservesIgnore("!(I <: {10})", "I<:{1~9,11~999}");
        checkNoReservesIgnore("!(I !<: {10})", "I<:{10}");
        checkNoReservesIgnore("!(I = 10 or I = 11)", "!(I=10|I=11)");

        checkNoReservesThrow("!(I = 10 or I = 11)", "!(I=10|I=11)");
    }

    @Test
    void testIntegerAllPossibleReserves() {
        checkAllPossibleReservesIgnore("I = 10", "I=10");
        checkAllPossibleReservesIgnore("I != 10", "I!=10");
        checkAllPossibleReservesIgnore("!(I = 10)", "!(I=10)");
        checkAllPossibleReservesIgnore("!(I != 10)", "I=10");
        checkAllPossibleReservesIgnore("I <: {10}", "I<:{10}");
        checkAllPossibleReservesIgnore("I !<: {10}", "I!<:{10}");
        checkAllPossibleReservesIgnore("!(I <: {10})", "!(I<:{10})");
        checkAllPossibleReservesIgnore("!(I !<: {10})", "I<:{10}");
        checkAllPossibleReservesIgnore("!(I = 10 or I = 11)", "!(I=10|I=11)");

        checkAllPossibleReservesThrow("!(I = 10 or I = 11)", "!(I=10|I=11)");
        assertThrows(IllegalOperationException.class,
                     () -> {
                         checkAllPossibleReservesThrow("I !<: {10}", null);
                     });
    }

    @Test
    void testRealNoReserves() {
        checkNoReservesIgnore("R = 10.0", "R=10.0");
        checkNoReservesIgnore("R != 10.0", "R<:{1.0~9.999999999999998,10.000000000000002~999.0}");
        checkNoReservesIgnore("!(R = 10.0)", "R<:{1.0~9.999999999999998,10.000000000000002~999.0}");
        checkNoReservesIgnore("!(R != 10.0)", "R=10.0");
        checkNoReservesIgnore("!(R = 10.0 or R = 11.0)", "!(R=10.0|R=11.0)");
    }

    @Test
    void testRealAllPossibleReserves() {
        checkAllPossibleReservesIgnore("R = 10.0", "R=10.0");
        checkAllPossibleReservesIgnore("R != 10.0", "R!=10.0");
        checkAllPossibleReservesIgnore("!(R = 10.0)", "!(R=10.0)");
        checkAllPossibleReservesIgnore("!(R != 10.0)", "R=10.0");
        checkAllPossibleReservesIgnore("!(R = 10.0 or R = 11.0)", "!(R=10.0|R=11.0)");
    }

    @Test
    public void testPatternNoReserves() {
        checkNoReservesIgnore("P = AA", "P=AA");
        checkNoReservesIgnore("!(P != AA)", "P=AA");
        checkAllPossibleReservesIgnore("P != AA", "P!=AA");
        checkAllPossibleReservesIgnore("!(P = AA)", "!(P=AA)");

        assertThrows(IllegalOperationException.class,
                     () -> {
                         checkNoReservesThrow("!(P = AA)", null);
                     });
    }

    @Test
    void testPatternAllPossibleReserves() {
        checkAllPossibleReservesIgnore("P = AA", "P=AA");
        checkAllPossibleReservesIgnore("!(P != AA)", "P=AA");
        checkAllPossibleReservesIgnore("P != AA", "P!=AA");
        checkAllPossibleReservesIgnore("!(P = AA)", "!(P=AA)");

        assertThrows(IllegalOperationException.class,
                     () -> {
                         checkAllPossibleReservesThrow("!(P = AA)", null);
                     });
    }
}