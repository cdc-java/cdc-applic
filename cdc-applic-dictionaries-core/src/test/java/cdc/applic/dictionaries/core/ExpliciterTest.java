package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Set;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.core.utils.Expliciter;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.BooleanTypeImpl;
import cdc.applic.dictionaries.impl.PropertyImpl;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.Expression;

class ExpliciterTest {
    @Test
    void test() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("R").build();
        final BooleanTypeImpl type1 = registry.booleanType().name("BooleanType1").defaultValue(false).build();
        final BooleanTypeImpl type2 = registry.booleanType().name("BooleanType2").build();
        final PropertyImpl b1a = registry.property().name("B1A").type(type1).build();
        final PropertyImpl b1b = registry.property().name("B1B").type(type1).build();
        final PropertyImpl b1c = registry.property().name("B1C").type(type1).build();
        final PropertyImpl b1d = registry.property().name("B1D").type(type1).build();

        final PropertyImpl b2a = registry.property().name("B2A").type(type2).build();
        final PropertyImpl b2b = registry.property().name("B2B").type(type2).build();
        final PropertyImpl b2c = registry.property().name("B2C").type(type2).build();
        final PropertyImpl b2d = registry.property().name("B2D").type(type2).build();

        registry.alias().name("A1").expression("B1A or B1B or B2A or B2B").build();

        final DictionaryHandle handle = new DictionaryHandle(registry);
        final Expliciter expliciter = new Expliciter(handle);

        assertEquals(Set.of(b1a, b1b, b1c, b1d, b2a, b2b, b2c, b2d),
                     expliciter.getImplicitProperties(Expression.TRUE));

        assertEquals(Set.of(b1a, b1b, b1c, b1d, b2a, b2b, b2c, b2d),
                     expliciter.getImplicitProperties(Expression.FALSE));

        assertEquals(Set.of(b1b, b1c, b1d, b2a, b2b, b2c, b2d),
                     expliciter.getImplicitProperties(Expression.fromString("B1A")));

        assertEquals(Set.of(b1c, b1d, b2c, b2d),
                     expliciter.getImplicitProperties(Expression.fromString("A1")));

        assertEquals(Set.of(b1c, b1d, b2c, b2d),
                     expliciter.getImplicitProperties(Expression.fromString("!A1")));

        assertEquals(Expression.fromString("B1C=false&B1D=false"),
                     expliciter.getExplicitComplement(Expression.fromString("A1")));

        assertEquals(Expression.fromString("A1&B1C=false&B1D=false"),
                     expliciter.makeExplicit(Expression.fromString("A1")));
    }
}