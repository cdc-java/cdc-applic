package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.core.visitors.ConvertNamingConvention;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;

class ConvertNamingConventionsTest {
    private static final Logger LOGGER = LogManager.getLogger(ConvertNamingConventionsTest.class);

    static void check(Dictionary dictionary,
                      String expression,
                      String convention,
                      String expected) {
        LOGGER.debug("check({}, {})", expression, convention);
        final NamingConvention nc = dictionary.getRegistry().getNamingConvention(convention);
        final Node node = ConvertNamingConvention.execute(Expression.fromString(expression).getRootNode(),
                                                          dictionary,
                                                          nc);
        final Expression actual = node.toExpression();

        assertEquals(expected, actual.getContent(), "expression: " + expression);
    }

    @Test
    void test1() {
        final String def = "Default";
        final String conv1 = "Convention1";
        final String conv2 = "Convention2";
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        r1.namingConvention().name(conv1).build();
        r1.namingConvention().name(conv2).build();

        r1.booleanType().name("BooleanType").build();
        r1.integerType().name("IntegerType").domain("1~999").build();
        r1.realType().name("RealType").domain("0.0~1000.0").build();
        r1.enumeratedType()
          .name("EnumType")
          .value()
          .literal("A")
          .synonym(conv1, "a")
          .back()
          .value()
          .literal("B")
          .synonym(conv1, "b")
          .back()
          .build();

        r1.property().name("B1").synonym(conv1, "b1").type("BooleanType").build();
        r1.property().name("I1").synonym(conv1, "i1").type("IntegerType").build();
        r1.property().name("R1").synonym(conv1, "r1").type("RealType").build();
        r1.property().name("E1").synonym(conv1, "e1").type("EnumType").build();
        r1.alias().name("A").synonym(conv1, "a").expression("B1").build();

        check(r1, "true", def, "true");
        check(r1, "A", def, "A");
        check(r1, "a", conv1, "a");
        check(r1, "a", def, "A");
        check(r1, "a or A", def, "A|A");
        check(r1, "a or A", conv1, "a|a");
        check(r1, "B1", def, "B1");
        check(r1, "B1", conv1, "b1");
        check(r1, "b1", def, "B1");
        check(r1, "b1", conv1, "b1");

        check(r1, "E1 = A", def, "E1=A");
        check(r1, "E1 = A", conv1, "e1=a");
        check(r1, "E1 != A", def, "E1!=A");
        check(r1, "E1 != A", conv1, "e1!=a");

        check(r1, "E1 < A", def, "E1<A");
        check(r1, "E1 < A", conv1, "e1<a");
        check(r1, "E1 !< A", def, "E1!<A");
        check(r1, "E1 !< A", conv1, "e1!<a");
        check(r1, "E1 <= A", def, "E1<=A");
        check(r1, "E1 <= A", conv1, "e1<=a");
        check(r1, "E1 !<= A", def, "E1!<=A");
        check(r1, "E1 !<= A", conv1, "e1!<=a");

        check(r1, "E1 > A", def, "E1>A");
        check(r1, "E1 > A", conv1, "e1>a");
        check(r1, "E1 !> A", def, "E1!>A");
        check(r1, "E1 !> A", conv1, "e1!>a");
        check(r1, "E1 >= A", def, "E1>=A");
        check(r1, "E1 >= A", conv1, "e1>=a");
        check(r1, "E1 !>= A", def, "E1!>=A");
        check(r1, "E1 !>= A", conv1, "e1!>=a");

        check(r1, "E1 = a", def, "E1=A");
        check(r1, "E1 = a", conv1, "e1=a");
        check(r1, "E1 != a", def, "E1!=A");
        check(r1, "E1 != a", conv1, "e1!=a");

        check(r1, "E1 < a", def, "E1<A");
        check(r1, "E1 < a", conv1, "e1<a");
        check(r1, "E1 !< a", def, "E1!<A");
        check(r1, "E1 !< a", conv1, "e1!<a");
        check(r1, "E1 <= a", def, "E1<=A");
        check(r1, "E1 <= a", conv1, "e1<=a");
        check(r1, "E1 !<= a", def, "E1!<=A");
        check(r1, "E1 !<= a", conv1, "e1!<=a");

        check(r1, "E1 > a", def, "E1>A");
        check(r1, "E1 > a", conv1, "e1>a");
        check(r1, "E1 !> a", def, "E1!>A");
        check(r1, "E1 !> a", conv1, "e1!>a");
        check(r1, "E1 >= a", def, "E1>=A");
        check(r1, "E1 >= a", conv1, "e1>=a");
        check(r1, "E1 !>= a", def, "E1!>=A");
        check(r1, "E1 !>= a", conv1, "e1!>=a");

        check(r1, "E1 <: {A}", def, "E1<:{A}");
        check(r1, "E1 <: {A}", conv1, "e1<:{a}");
        check(r1, "E1 !<: {A}", def, "E1!<:{A}");
        check(r1, "E1 !<: {A}", conv1, "e1!<:{a}");

        check(r1, "E1 <: {}", def, "E1<:{}");
        check(r1, "E1 <: {}", conv1, "e1<:{}");
        check(r1, "E1 !<: {}", def, "E1!<:{}");
        check(r1, "E1 !<: {}", conv1, "e1!<:{}");
    }
}