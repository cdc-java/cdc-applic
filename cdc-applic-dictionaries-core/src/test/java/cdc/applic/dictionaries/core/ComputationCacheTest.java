package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.handles.ApplicCacheId;
import cdc.applic.dictionaries.handles.ComputationCache;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.ast.TrueNode;

class ComputationCacheTest {
    RepositorySupport support = new RepositorySupport();

    private static class CC extends ComputationCache<String> {
        private CC(DictionaryHandle handle) {
            super(handle, null);
        }

        public static CC get(DictionaryHandle handle) {
            return handle.computeIfAbsent(CC.class, (c) -> new CC(c));
        }

    }

    @Test
    void test() {
        final CC cc = CC.get(support.registryHandle);
        assertEquals(new ApplicCacheId(CC.class, null), cc.getId());
        assertEquals(null, cc.get(TrueNode.INSTANCE));
        assertFalse(cc.containsKey(TrueNode.INSTANCE));

        cc.put(TrueNode.INSTANCE, "Hello");
        assertEquals("Hello", cc.get(TrueNode.INSTANCE));
        assertTrue(cc.containsKey(TrueNode.INSTANCE));

        support.registryHandle.invalidate();
        assertFalse(cc.containsKey(TrueNode.INSTANCE));
    }
}