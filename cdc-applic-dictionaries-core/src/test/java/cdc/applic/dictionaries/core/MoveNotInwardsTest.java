package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.core.visitors.MoveNotInwards;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.visitors.ConvertToNary;

class MoveNotInwardsTest {
    private final RepositorySupport support = new RepositorySupport();

    private void check(String expression,
                       MoveNotInwards.Variant variant,
                       ReserveStrategy reserveStrategy,
                       String expected) {
        final Expression x1 = new Expression(expression);
        final Expression x2 = MoveNotInwards.execute(ConvertToNary.execute(x1.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY),
                                                     variant,
                                                     support.registry,
                                                     reserveStrategy)
                                            .toExpression(Formatting.SHORT_NARROW);
        assertEquals(expected, x2.toString());
        if (!variant.requiresDictionary()) {
            final Expression x3 =
                    MoveNotInwards.execute(ConvertToNary.execute(x1.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY),
                                           variant)
                                  .toExpression(Formatting.SHORT_NARROW);
            assertEquals(expected, x3.toString());
        }
    }

    private void checkUseNegativeLeaves(String expression,
                                        String expected) {
        check(expression,
              MoveNotInwards.Variant.USE_NEGATIVE_LEAVES,
              ReserveStrategy.NO_RESERVES,
              expected);
    }

    private void checkDontUseNegativeLeaves(String expression,
                                            String expected) {
        check(expression,
              MoveNotInwards.Variant.DONT_USE_NEGATIVE_LEAVES,
              ReserveStrategy.NO_RESERVES,
              expected);
    }

    private void checkDontUseNegationIgnore(String expression,
                                            String expected) {
        check(expression,
              MoveNotInwards.Variant.DONT_USE_NEGATION_IGNORE_WHEN_IMPOSSIBLE,
              ReserveStrategy.NO_RESERVES,
              expected);
    }

    private void checkDontUseNegationThrow(String expression,
                                           String expected) {
        check(expression,
              MoveNotInwards.Variant.DONT_USE_NEGATION_THROW_WHEN_IMPOSSIBLE,
              ReserveStrategy.NO_RESERVES,
              expected);
    }

    @Test
    void testUseNegativeLeaves() {
        checkUseNegativeLeaves("", "true");
        checkUseNegativeLeaves("true", "true");
        checkUseNegativeLeaves("false", "false");
        checkUseNegativeLeaves("!false", "true");
        checkUseNegativeLeaves("!true", "false");

        checkUseNegativeLeaves("B", "B");
        checkUseNegativeLeaves("!B", "!B");
        checkUseNegativeLeaves("!B", "!B");
        checkUseNegativeLeaves("!!B", "B");

        checkUseNegativeLeaves("!(A = B)", "A!=B");
        checkUseNegativeLeaves("!(A != B)", "A=B");

        checkUseNegativeLeaves("!(A in {B})", "A!<:{B}");
        checkUseNegativeLeaves("!(A not in {B})", "A<:{B}");

        checkUseNegativeLeaves("!(A and B)", "!A|!B");
        checkUseNegativeLeaves("!(A or B)", "!A&!B");

        checkUseNegativeLeaves("!(A and B and C)", "!A|!B|!C");
        checkUseNegativeLeaves("!(A or B or C)", "!A&!B&!C");

        checkUseNegativeLeaves("!(A > B)", "A!>B");
        checkUseNegativeLeaves("!(A >= B)", "A!>=B");
        checkUseNegativeLeaves("!(A < B)", "A!<B");
        checkUseNegativeLeaves("!(A <= B)", "A!<=B");

        checkUseNegativeLeaves("!(A !> B)", "A>B");
        checkUseNegativeLeaves("!(A !>= B)", "A>=B");
        checkUseNegativeLeaves("!(A !< B)", "A<B");
        checkDontUseNegativeLeaves("!(A !<= B)", "A<=B");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checkUseNegativeLeaves("!(A -> B)", null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checkUseNegativeLeaves("!(A <-> B)", null);
                     });
    }

    @Test
    void testDontUseNegativeLeaves() {
        checkDontUseNegativeLeaves("", "true");
        checkDontUseNegativeLeaves("true", "true");
        checkDontUseNegativeLeaves("false", "false"); // ???
        checkDontUseNegativeLeaves("!false", "true");
        checkDontUseNegativeLeaves("!true", "false"); // ???

        checkDontUseNegativeLeaves("B", "B");
        checkDontUseNegativeLeaves("!B", "!B");
        checkDontUseNegativeLeaves("!B", "!B");
        checkDontUseNegativeLeaves("!!B", "B");

        checkDontUseNegativeLeaves("!(A = B)", "!(A=B)");
        checkDontUseNegativeLeaves("!(A != B)", "A=B");
        checkDontUseNegativeLeaves("(A != B)", "!(A=B)");

        checkDontUseNegativeLeaves("!(A in {B})", "!(A<:{B})");
        checkDontUseNegativeLeaves("!(A not in {B})", "A<:{B}");
        checkDontUseNegativeLeaves("A not in {B}", "!(A<:{B})");

        checkDontUseNegativeLeaves("!(A and B)", "!A|!B");
        checkDontUseNegativeLeaves("!(A or B)", "!A&!B");

        checkDontUseNegativeLeaves("!(A and B and C)", "!A|!B|!C");
        checkDontUseNegativeLeaves("!(A or B or C)", "!A&!B&!C");

        checkDontUseNegativeLeaves("!(A > B)", "!(A>B)");
        checkDontUseNegativeLeaves("!(A >= B)", "!(A>=B)");
        checkDontUseNegativeLeaves("!(A < B)", "!(A<B)");
        checkDontUseNegativeLeaves("!(A <= B)", "!(A<=B)");

        checkDontUseNegativeLeaves("!(A !> B)", "A>B");
        checkDontUseNegativeLeaves("!(A !>= B)", "A>=B");
        checkDontUseNegativeLeaves("!(A !< B)", "A<B");
        checkUseNegativeLeaves("!(A !<= B)", "A<=B");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checkDontUseNegativeLeaves("!(A -> B)", null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checkDontUseNegativeLeaves("!(A <-> B)", null);
                     });
    }

    @Test
    void testDontUseNegationIgnore() {
        checkDontUseNegationIgnore("", "true");
        checkDontUseNegationIgnore("true", "true");
        checkDontUseNegationIgnore("false", "false");
        checkDontUseNegationIgnore("!false", "true");
        checkDontUseNegationIgnore("!true", "false");

        checkDontUseNegationIgnore("B", "B");
        checkDontUseNegationIgnore("!B", "B=false");
        checkDontUseNegationIgnore("!B", "B=false");
        checkDontUseNegationIgnore("!!B", "B");

        checkDontUseNegationIgnore("!(E = E1)", "E<:{E2,E3}");
        checkDontUseNegationIgnore("!(E != E1)", "E=E1");
        checkDontUseNegationIgnore("(E != E1)", "E<:{E2,E3}");

        checkDontUseNegationIgnore("!(E in {E1})", "E<:{E2,E3}");
        checkDontUseNegationIgnore("!(E not in {E1})", "E<:{E1}");
        checkDontUseNegationIgnore("E not in {E1}", "E<:{E2,E3}");

        checkDontUseNegationIgnore("!(A and B)", "!A|B=false");
        checkDontUseNegationIgnore("!(A or B)", "!A&B=false");

        checkDontUseNegationIgnore("!(A and B and C)", "!A|B=false|!C");
        checkDontUseNegationIgnore("!(A or B or C)", "!A&B=false&!C");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checkDontUseNegationIgnore("!(A -> B)", null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checkDontUseNegationIgnore("!(A <-> B)", null);
                     });
    }

    @Test
    void testDontUseNegationThrow() {
        checkDontUseNegationThrow("", "true");
        checkDontUseNegationThrow("true", "true");
        checkDontUseNegationThrow("false", "false");
        checkDontUseNegationThrow("!false", "true");
        checkDontUseNegationThrow("!true", "false");

        checkDontUseNegationThrow("B", "B");
        checkDontUseNegationThrow("!B", "B=false");
        checkDontUseNegationThrow("!B", "B=false");
        checkDontUseNegationThrow("!!B", "B");

        checkDontUseNegationThrow("!(E = E1)", "E<:{E2,E3}");
        checkDontUseNegationThrow("!(E != E1)", "E=E1");
        checkDontUseNegationThrow("(E != E1)", "E<:{E2,E3}");

        checkDontUseNegationThrow("!(E in {E1})", "E<:{E2,E3}");
        checkDontUseNegationThrow("!(E not in {E1})", "E<:{E1}");
        checkDontUseNegationThrow("E not in {E1}", "E<:{E2,E3}");

        checkDontUseNegationThrow("!(A and B)", "!A|B=false");
        checkDontUseNegationThrow("!(A or B)", "!A&B=false");

        checkDontUseNegationThrow("!(A and B and C)", "!A|B=false|!C");
        checkDontUseNegationThrow("!(A or B or C)", "!A&B=false&!C");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checkDontUseNegationThrow("!(A -> B)", null);
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checkDontUseNegationThrow("!(A <-> B)", null);
                     });
    }
}