package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.core.visitors.IsCompliantWithNamingConvention;
import cdc.applic.dictionaries.impl.PolicyImpl;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.Expression;

class IsCompliantWithNamingConventionTest {
    private static final Logger LOGGER = LogManager.getLogger(IsCompliantWithNamingConventionTest.class);

    private static void check(Dictionary dictionary,
                              String expression,
                              String convention,
                              boolean expected) {
        LOGGER.debug("check({}, {}, {})", expression, convention, expected);
        final NamingConvention nc = dictionary.getRegistry().getNamingConvention(convention);
        final boolean actual = IsCompliantWithNamingConvention.test(Expression.fromString(expression).getRootNode(),
                                                                    dictionary,
                                                                    nc);

        assertEquals(expected, actual, "expression: " + expression);
    }

    @Test
    void test() {
        final String def = "Default";
        final String conv1 = "Convention1";
        final String conv2 = "Convention2";
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        r1.namingConvention().name(conv1).build();
        r1.namingConvention().name(conv2).build();

        r1.booleanType().name("BooleanType").build();
        r1.integerType().name("IntegerType").domain("1~999").build();
        r1.realType().name("RealType").domain("0.0~1000.0").build();
        r1.enumeratedType()
          .name("EnumType")
          .value()
          .literal("A")
          .synonym(conv1, "a")
          .back()
          .value()
          .literal("B")
          .synonym(conv1, "b")
          .back()
          .build();

        r1.property().name("B1").synonym(conv1, "b1").type("BooleanType").build();
        r1.property().name("I1").synonym(conv1, "i1").type("IntegerType").build();
        r1.property().name("R1").synonym(conv1, "r1").type("RealType").build();
        r1.property().name("E1").synonym(conv1, "e1").type("EnumType").build();
        r1.alias().name("A").synonym(conv1, "a").expression("B1").build();

        final PolicyImpl p1 = r1.policy().name("p1").build();

        check(r1, "true", def, true);
        check(p1, "true", def, true);
        check(r1, "true", conv1, true);
        check(p1, "true", conv1, true);

        check(r1, "Foo", def, false);
        check(p1, "Foo", def, false);
        check(r1, "Foo", conv1, false);
        check(p1, "Foo", conv1, false);

        check(r1, "A", def, true);
        check(r1, "a", def, false);
        check(p1, "A", def, true);
        check(p1, "a", def, false);

        check(r1, "A", conv1, false);
        check(r1, "a", conv1, true);
        check(p1, "A", conv1, false);
        check(p1, "a", conv1, true);

        check(r1, "E1=A", def, true);
        check(r1, "E1=a", def, false);
        check(r1, "e1=A", def, false);
        check(r1, "e1=a", def, false);
        check(p1, "E1=A", def, true);
        check(p1, "E1=a", def, false);
        check(p1, "e1=A", def, false);
        check(p1, "e1=a", def, false);

        check(r1, "E1=A", conv1, false);
        check(r1, "E1=a", conv1, false);
        check(r1, "e1=A", conv1, false);
        check(r1, "e1=a", conv1, true);
        check(p1, "E1=A", conv1, false);
        check(p1, "E1=a", conv1, false);
        check(p1, "e1=A", conv1, false);
        check(p1, "e1=a", conv1, true);

        check(r1, "E1<:{A}", def, true);
        check(r1, "E1<:{a}", def, false);
        check(r1, "e1<:{A}", def, false);
        check(r1, "e1<:{a}", def, false);
        check(p1, "E1<:{A}", def, true);
        check(p1, "E1<:{a}", def, false);
        check(p1, "e1<:{A}", def, false);
        check(p1, "e1<:{a}", def, false);

        check(r1, "E1<:{A}", conv1, false);
        check(r1, "E1<:{a}", conv1, false);
        check(r1, "e1<:{A}", conv1, false);
        check(r1, "e1<:{a}", conv1, true);
        check(p1, "E1<:{A}", conv1, false);
        check(p1, "E1<:{a}", conv1, false);
        check(p1, "e1<:{A}", conv1, false);
        check(p1, "e1<:{a}", conv1, true);

        check(r1, "B1", def, true);
        check(r1, "b1", def, false);
        check(r1, "B1", conv1, false);
        check(r1, "b1", conv1, true);
        check(p1, "B1", def, true);
        check(p1, "b1", def, false);
        check(p1, "B1", conv1, false);
        check(p1, "b1", conv1, true);
    }
}