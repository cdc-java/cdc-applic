package cdc.applic.dictionaries.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.PrintStream;
import java.util.Arrays;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.core.utils.CutPointBuffer;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerDomain;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerValue;

class CutPointBufferTest {
    private static final Logger LOGGER = LogManager.getLogger(CutPointBufferTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    private static IntegerValue v(int value) {
        return IntegerValue.of(value);
    }

    private static IntegerRange r(int value) {
        return IntegerRange.of(value);
    }

    private static IntegerRange r(int min,
                                  int max) {
        return IntegerRange.of(min, max);
    }

    @Test
    void test1() {
        final CutPointBuffer<IntegerDomain, IntegerValue, IntegerRange> cpb = new CutPointBuffer<>(IntegerDomain.INSTANCE);
        assertEquals(Arrays.asList(), cpb.getCutRanges());
        cpb.add(v(1));
        assertEquals(Arrays.asList(r(1)), cpb.getCutRanges());
        cpb.add(r(1, 3));
        assertEquals(Arrays.asList(r(1), r(2, 3)), cpb.getCutRanges());
        cpb.add(r(0, 1));
        assertEquals(Arrays.asList(r(0), r(1), r(2, 3)), cpb.getCutRanges());
        // Do this twice for branch coverage
        assertEquals(Arrays.asList(r(0), r(1), r(2, 3)), cpb.getCutRanges());
    }

    @Test
    void test2() {
        final CutPointBuffer<IntegerDomain, IntegerValue, IntegerRange> cpb = new CutPointBuffer<>(IntegerDomain.INSTANCE);
        cpb.add(r(1, 3));
        cpb.add(r(6, 8));
        assertEquals(Arrays.asList(r(1, 3), r(6, 8)), cpb.getCutRanges());
    }

    @Test
    public void testEmpty() {
        final CutPointBuffer<IntegerDomain, IntegerValue, IntegerRange> cpb = new CutPointBuffer<>(IntegerDomain.INSTANCE);
        cpb.add(r(3, 2));
        assertEquals(Arrays.asList(), cpb.getCutRanges());
    }

    @Test
    void testAddOther() {
        final CutPointBuffer<IntegerDomain, IntegerValue, IntegerRange> cpb1 = new CutPointBuffer<>(IntegerDomain.INSTANCE);
        final CutPointBuffer<IntegerDomain, IntegerValue, IntegerRange> cpb2 = new CutPointBuffer<>(IntegerDomain.INSTANCE);
        cpb1.add(cpb2);
        assertEquals(Arrays.asList(), cpb1.getCutRanges());
        cpb2.add(r(1, 2));
        cpb1.add(cpb2);
        assertEquals(Arrays.asList(r(1, 2)), cpb1.getCutRanges());
    }

    @Test
    void testRejectMin() {
        final CutPointBuffer<IntegerDomain, IntegerValue, IntegerRange> cpb = new CutPointBuffer<>(IntegerDomain.INSTANCE);
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         cpb.add(IntegerDomain.INSTANCE.min(), v(0));
                     });
    }

    @Test
    void testGetEquivalence() {
        final CutPointBuffer<IntegerDomain, IntegerValue, IntegerRange> cpb = new CutPointBuffer<>(IntegerDomain.INSTANCE);
        cpb.add(r(1, 4));
        cpb.add(r(3, 5));
        cpb.add(r(7, 8));
        assertEquals(Arrays.asList(r(1, 2), r(3, 4), r(5), r(7, 8)), cpb.getCutRanges());

        assertEquals(Arrays.asList(r(1, 2)), cpb.getEquivalence(r(1, 2)));
        assertEquals(Arrays.asList(r(1, 2), r(3, 4)), cpb.getEquivalence(r(1, 4)));
        assertEquals(Arrays.asList(r(1, 2), r(3, 4), r(5)), cpb.getEquivalence(r(1, 5)));
        assertEquals(Arrays.asList(r(3, 4)), cpb.getEquivalence(r(3, 4)));
        assertEquals(Arrays.asList(r(3, 4), r(5)), cpb.getEquivalence(r(3, 5)));
        assertEquals(Arrays.asList(r(5)), cpb.getEquivalence(r(5)));
        assertEquals(Arrays.asList(r(7, 8)), cpb.getEquivalence(r(7, 8)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         cpb.getEquivalence(r(1));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         cpb.getEquivalence(r(0));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         cpb.getEquivalence(r(0, 1));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         cpb.getEquivalence(r(0, 2));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         cpb.getEquivalence(r(2, 3));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         cpb.getEquivalence(r(2, 4));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         cpb.getEquivalence(r(1, 3));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         cpb.getEquivalence(r(5, 8));
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         cpb.getEquivalence(BooleanValue.TRUE);
                     });
    }

    @Test
    void testPrint() {
        final CutPointBuffer<IntegerDomain, IntegerValue, IntegerRange> cpb = new CutPointBuffer<>(IntegerDomain.INSTANCE);
        cpb.print(OUT, 0);
    }
}