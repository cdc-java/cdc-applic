package cdc.applic.dictionaries.edit.events;

import cdc.applic.dictionaries.edit.EnAbstractElement;

public class EnCreateEvent extends EnEvent {
    protected EnCreateEvent(EnAbstractElement target) {
        super(target);
    }

    public static EnCreateEvent of(EnAbstractElement target) {
        return new EnCreateEvent(target);
    }
}