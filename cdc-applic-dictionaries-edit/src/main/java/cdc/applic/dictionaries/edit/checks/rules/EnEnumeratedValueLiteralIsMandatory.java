package cdc.applic.dictionaries.edit.checks.rules;

import cdc.applic.dictionaries.edit.EnEnumeratedValue;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.util.strings.StringUtils;

public class EnEnumeratedValueLiteralIsMandatory extends EnAbstractRuleChecker<EnEnumeratedValue> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final String NAME = "ENUMERATED_VALUE_LITERAL_IS_MANDATORY";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Enumerated values must have a literal.")
                                                         .appliesTo("Enumerated values"),
                               SEVERITY);

    public EnEnumeratedValueLiteralIsMandatory(SnapshotManager manager) {
        super(manager,
              EnEnumeratedValue.class,
              RULE);
    }

    public static boolean isDefined(String literal) {
        return !StringUtils.isNullOrEmpty(literal);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnEnumeratedValue object,
                             Location location) {
        final String literal = object.getLiteral();
        if (!isDefined(literal)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has no literal");

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}