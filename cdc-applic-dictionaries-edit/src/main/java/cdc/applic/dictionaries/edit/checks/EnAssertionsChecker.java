package cdc.applic.dictionaries.edit.checks;

import java.util.List;
import java.util.stream.Collectors;

import cdc.applic.dictionaries.edit.EnDictionary;
import cdc.applic.dictionaries.edit.EnLocation;
import cdc.applic.dictionaries.edit.EnUserDefinedAssertion;
import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;
import cdc.util.lang.Introspection;

public class EnAssertionsChecker extends AbstractPartsChecker<EnDictionary<?>, EnUserDefinedAssertion, EnAssertionsChecker> {
    public EnAssertionsChecker(SnapshotManager manager) {
        super(manager,
              Introspection.uncheckedCast(EnDictionary.class),
              EnUserDefinedAssertion.class,
              new EnAssertionChecker(manager));
    }

    @Override
    protected List<LocatedObject<? extends EnUserDefinedAssertion>> getParts(EnDictionary<?> object) {
        final List<EnUserDefinedAssertion> parts = object.getUserDefinedAssertions();
        return parts.stream()
                    .map(x -> LocatedObject.of(x, EnLocation.builder().element(x).build()))
                    .collect(Collectors.toList());
    }
}