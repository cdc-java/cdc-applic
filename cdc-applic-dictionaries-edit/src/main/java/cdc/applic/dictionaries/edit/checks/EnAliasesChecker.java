package cdc.applic.dictionaries.edit.checks;

import java.util.List;
import java.util.stream.Collectors;

import cdc.applic.dictionaries.edit.EnAlias;
import cdc.applic.dictionaries.edit.EnLocation;
import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;

public class EnAliasesChecker extends AbstractPartsChecker<EnRegistry, EnAlias, EnAliasesChecker> {
    public EnAliasesChecker(SnapshotManager manager) {
        super(manager,
              EnRegistry.class,
              EnAlias.class,
              new EnAliasChecker(manager));
    }

    @Override
    protected List<LocatedObject<? extends EnAlias>> getParts(EnRegistry object) {
        final List<EnAlias> parts = object.getAliases();
        return parts.stream()
                    .map(x -> LocatedObject.of(x, EnLocation.builder().element(x).build()))
                    .collect(Collectors.toList());
    }
}