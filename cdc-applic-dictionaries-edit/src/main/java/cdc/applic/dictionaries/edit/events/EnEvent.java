package cdc.applic.dictionaries.edit.events;

import java.time.format.DateTimeFormatter;

import cdc.applic.dictionaries.edit.EnAbstractElement;
import cdc.util.events.Event;

public abstract class EnEvent extends Event {
    private final EnAbstractElement target;

    protected EnEvent(EnAbstractElement target) {
        this.target = target;
    }

    public final EnAbstractElement getTarget() {
        return target;
    }

    @Override
    public String toString() {
        return "[" + getLocalDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
                + " " + getClass().getSimpleName()
                + " " + getTarget().getId() + "]";
    }
}