package cdc.applic.dictionaries.edit.checks.rules;

import java.util.List;

import cdc.applic.dictionaries.edit.EnNamedDItem;
import cdc.applic.dictionaries.edit.EnQName;
import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.applic.dictionaries.edit.checks.utils.AmbiguityHelper;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class EnItemNamesAndSynonymsMustNotBeAmbiguous extends EnAbstractRuleChecker<EnRegistry> {
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    public static final String NAME = "ITEM_NAMES_AND_SYNONYMS_MUST_NOT_BE_AMBIGUOUS";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("In a registry, names and synonyms of items must not be ambiguous."
                                       + "\nThey must not be identical to names or synonyms of inherited items.")
                                                         .appliesTo("Registries"),
                               SEVERITY);

    public static AmbiguityHelper<EnNamedDItem, EnQName> helper(EnRegistry registry) {
        return AmbiguityHelper.ofN((EnNamedDItem t) -> t.getOwner() == registry
                ? EnQName.removePrefix(t.getQNameAndQSynonyms())
                : t.getQNameAndQSynonyms(),
                                   EnQName::isValid);

    }

    public EnItemNamesAndSynonymsMustNotBeAmbiguous(SnapshotManager manager) {
        super(manager,
              EnRegistry.class,
              RULE);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnRegistry object,
                             Location location) {
        object.getAllAncestors(false);
        final List<? extends EnNamedDItem> items = object.getNamedDItems();
        final List<? extends EnNamedDItem> others = object.getAllNamedDItems(false);
        others.removeAll(items);
        final AmbiguityHelper<EnNamedDItem, EnQName> helper = helper(object);
        if (helper.hasAmbiguities(items, others)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has ambiguous item names or synonyms: " + helper.explain(items, others));

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}