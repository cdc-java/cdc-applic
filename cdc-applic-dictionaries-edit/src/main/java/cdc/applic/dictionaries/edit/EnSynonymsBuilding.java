package cdc.applic.dictionaries.edit;

public interface EnSynonymsBuilding<B> {

    public default B synonym(EnNamingConvention convention,
                             String synonym) {
        return synonym(convention.getId(), synonym);
    }

    public B synonym(String conventionId,
                     String synonym);
}