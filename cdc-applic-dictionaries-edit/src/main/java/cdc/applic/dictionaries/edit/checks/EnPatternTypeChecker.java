package cdc.applic.dictionaries.edit.checks;

import cdc.applic.dictionaries.edit.EnPatternType;
import cdc.applic.dictionaries.edit.checks.rules.EnPatternIsMandatory;
import cdc.applic.dictionaries.edit.checks.rules.EnPatternMustBeValid;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.SnapshotManager;

public class EnPatternTypeChecker extends CompositeChecker<EnPatternType> {
    public EnPatternTypeChecker(SnapshotManager manager) {
        super(manager,
              EnPatternType.class,
              new EnPatternIsMandatory(manager),
              new EnPatternMustBeValid(manager));
    }
}