package cdc.applic.dictionaries.edit.checks.utils;

import cdc.applic.dictionaries.edit.EnElement;
import cdc.issues.checks.AbstractRuleChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;

public abstract class EnAbstractRuleChecker<O extends EnElement> extends AbstractRuleChecker<O> {
    protected EnAbstractRuleChecker(SnapshotManager manager,
                                    Class<O> objectClass,
                                    Rule rule) {
        super(manager, objectClass, rule);
    }

    @Override
    protected final String getHeader(O object) {
        return "The " + object.getKind() + " (" + object.getId() + ")";
    }
}