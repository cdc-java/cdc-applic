package cdc.applic.dictionaries.edit;

import cdc.applic.expressions.content.IntegerSet;

public final class EnIntegerType extends EnType
        implements EnFrozenItem, EnDomainItem {
    public static final String KIND = "IntegerType";
    private String domain;
    private boolean frozen;

    protected EnIntegerType(Builder builder) {
        super(builder);
        this.domain = builder.domain;
        this.frozen = builder.frozen;
        addToOwner();
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public EnRef<EnIntegerType> getRef() {
        return getRef(EnIntegerType.class);
    }

    @Override
    public String getDomain() {
        return domain;
    }

    @Override
    public void setDomain(String domain) {
        this.domain = domain;

        fireSemanticChange(EnNames.DOMAIN);
    }

    public void setDomain(IntegerSet domain) {
        setDomain(domain == null ? null : domain.getContent());
    }

    @Override
    public boolean isFrozen() {
        return frozen;
    }

    @Override
    public void setFrozen(boolean frozen) {
        this.frozen = frozen;

        fireSemanticChange(EnNames.FROZEN);
    }

    static Builder builder(EnRegistry owner) {
        return new Builder(owner);
    }

    public static final class Builder
            extends EnType.Builder<Builder, EnIntegerType>
            implements EnFrozenBuilding<Builder>, EnDomainBuilding<Builder> {
        private String domain;
        private boolean frozen = false;

        protected Builder(EnRegistry owner) {
            super(owner);
        }

        @Override
        public Class<EnIntegerType> getBuiltClass() {
            return EnIntegerType.class;
        }

        @Override
        public Builder domain(String domain) {
            this.domain = domain;
            return self();
        }

        public Builder domain(IntegerSet domain) {
            this.domain = domain == null ? null : domain.getContent();
            return self();
        }

        @Override
        public Builder frozen(boolean frozen) {
            this.frozen = frozen;
            return self();
        }

        @Override
        public EnIntegerType build() {
            return wrap(new EnIntegerType(this));
        }
    }
}