package cdc.applic.dictionaries.edit.checks.rules;

import java.util.Set;

import cdc.applic.dictionaries.edit.EnDictionary;
import cdc.applic.dictionaries.edit.EnNamedDItem;
import cdc.applic.dictionaries.edit.EnRef;
import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.util.lang.Introspection;

public class EnDictionaryUsageItemsRefsMustBeValid extends EnAbstractRefMustBeValid<EnDictionary<?>, EnNamedDItem> {
    public static final String NAME = "DICTIONARY_USAGE_ITEMS_REFS_MUST_BE_VALID";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Referenced items in usage must exist and be usable.")
                                                         .appliesTo("Policies",
                                                                    "Registries"),
                               SEVERITY);

    public EnDictionaryUsageItemsRefsMustBeValid(SnapshotManager manager) {
        super(manager,
              Introspection.uncheckedCast(EnDictionary.class),
              EnNamedDItem.class,
              RULE,
              "item ref");
    }

    @Override
    protected boolean isValid(EnRef<EnNamedDItem> ref) {
        if (ref.canResolve()) {
            final EnRegistry registry = ref.getOwner(EnDictionary.class).getRegistry();
            final EnNamedDItem item = ref.resolve();
            return registry.isKnownItem(item);
        } else {
            return false;
        }
    }

    @Override
    protected Set<EnRef<EnNamedDItem>> getRefs(EnDictionary<?> object) {
        return object.getItemToUsageKeys();
    }
}