package cdc.applic.dictionaries.edit.checks.rules;

import java.util.List;

import cdc.applic.dictionaries.edit.EnEnumeratedType;
import cdc.applic.dictionaries.edit.EnEnumeratedValue;
import cdc.applic.dictionaries.edit.checks.utils.DuplicateHelper;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class EnEnumeratedTypeLiteralsAndSynonymsMustBeUnique extends EnAbstractRuleChecker<EnEnumeratedType> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final String NAME = "ENUMERATED_TYPE_LITERALS_AND_SYNONYMS_MUST_BE_UNIQUE";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Literals and synonyms of an enumerated type must be unique."
                                       + "\nIn the context of an enumerated type, literals and synonyms can not designate several enumerated values.")
                                                         .appliesTo("Enumerated types"),
                               SEVERITY);

    private static final DuplicateHelper<EnEnumeratedValue, String> HELPER =
            DuplicateHelper.ofN(EnEnumeratedValue::getLiteralAndSynonyms,
                                EnEnumeratedValueLiteralIsMandatory::isDefined);

    public EnEnumeratedTypeLiteralsAndSynonymsMustBeUnique(SnapshotManager manager) {
        super(manager,
              EnEnumeratedType.class,
              RULE);
    }

    public static boolean isCompliant(List<EnEnumeratedValue> values) {
        return !HELPER.hasDuplicates(values);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnEnumeratedType object,
                             Location location) {
        final List<EnEnumeratedValue> values = object.getValues();
        if (HELPER.hasDuplicates(values)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has duplicate literals or synonyms: " + HELPER.explain(values));

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}