package cdc.applic.dictionaries.edit.checks.rules;

import java.util.Collection;
import java.util.List;

import cdc.applic.dictionaries.edit.EnElement;
import cdc.applic.dictionaries.edit.EnOwnedElement;
import cdc.applic.dictionaries.edit.EnRef;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public abstract class EnAbstractRefMustBeValid<O extends EnElement, X extends EnOwnedElement<?>>
        extends EnAbstractRuleChecker<O> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;

    protected final Class<X> refClass;
    private final String refLabel;

    protected EnAbstractRefMustBeValid(SnapshotManager manager,
                                       Class<O> objectClass,
                                       Class<X> refClass,
                                       Rule rule,
                                       String refLabel) {
        super(manager,
              objectClass,
              rule);

        this.refClass = refClass;
        this.refLabel = refLabel;
    }

    protected abstract Collection<EnRef<X>> getRefs(O object);

    protected boolean isValid(EnRef<X> ref) {
        return ref.canResolve();
    }

    @Override
    public CheckResult check(CheckContext context,
                             O object,
                             Location location) {
        final Collection<EnRef<X>> refs = getRefs(object);
        final List<EnRef<X>> invalidRefs = refs.stream()
                                               .filter(r -> !isValid(r))
                                               .toList();

        if (!invalidRefs.isEmpty()) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has invalid " + refLabel + ": " + invalidRefs);

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}