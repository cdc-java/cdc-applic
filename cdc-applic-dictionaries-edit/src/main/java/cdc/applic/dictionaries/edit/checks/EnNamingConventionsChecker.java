package cdc.applic.dictionaries.edit.checks;

import java.util.List;
import java.util.stream.Collectors;

import cdc.applic.dictionaries.edit.EnLocation;
import cdc.applic.dictionaries.edit.EnNamingConvention;
import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;

public class EnNamingConventionsChecker extends AbstractPartsChecker<EnRegistry, EnNamingConvention, EnNamingConventionsChecker> {
    public EnNamingConventionsChecker(SnapshotManager manager) {
        super(manager,
              EnRegistry.class,
              EnNamingConvention.class,
              new EnNamingConventionChecker(manager));
    }

    @Override
    protected List<LocatedObject<? extends EnNamingConvention>> getParts(EnRegistry object) {
        final List<EnNamingConvention> parts = object.getNamingConventions();
        return parts.stream()
                    .map(x -> LocatedObject.of(x, EnLocation.builder().element(x).build()))
                    .collect(Collectors.toList());
    }
}