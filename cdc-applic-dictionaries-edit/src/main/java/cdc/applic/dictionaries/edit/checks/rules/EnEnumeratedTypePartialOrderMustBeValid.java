package cdc.applic.dictionaries.edit.checks.rules;

import cdc.applic.dictionaries.edit.EnEnumeratedType;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class EnEnumeratedTypePartialOrderMustBeValid extends EnAbstractRuleChecker<EnEnumeratedType> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final String NAME = "ENUMERATED_TYPE_PARTIAL_ORDER_MUST_BE_VALID";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Partial order of an enumerated type must be valid."
                                       + "\nIt shall not contain cycles.")
                                                         .appliesTo("Enumerated types"),
                               SEVERITY);

    public EnEnumeratedTypePartialOrderMustBeValid(SnapshotManager manager) {
        super(manager,
              EnEnumeratedType.class,
              RULE);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnEnumeratedType object,
                             Location location) {
        if (!object.hasValidPartialOrder()) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has invalid partial order.");

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}