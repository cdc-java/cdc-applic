package cdc.applic.dictionaries.edit.checks.rules;

import java.util.List;

import cdc.applic.dictionaries.edit.EnDictionary;
import cdc.applic.dictionaries.edit.EnPolicy;
import cdc.applic.dictionaries.edit.checks.utils.DuplicateHelper;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.util.lang.Introspection;

public class EnPolicyNamesMustBeUnique extends EnAbstractRuleChecker<EnDictionary<?>> {
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    public static final String NAME = "POLICY_NAMES_MUST_BE_UNIQUE";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Names of sibling policies in a dictionary must be unique.")
                                                         .appliesTo("Policies",
                                                                    "Registries"),
                               SEVERITY);

    private static final DuplicateHelper<EnPolicy, String> HELPER =
            DuplicateHelper.of1(EnPolicy::getName,
                                EnNameIsMandatory::isDefined);

    public EnPolicyNamesMustBeUnique(SnapshotManager manager) {
        super(manager,
              Introspection.uncheckedCast(EnDictionary.class),
              RULE);
    }

    public static boolean isCompliant(List<EnPolicy> policies) {
        return !HELPER.hasDuplicates(policies);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnDictionary<?> object,
                             Location location) {
        final List<EnPolicy> policies = object.getPolicies();
        if (HELPER.hasDuplicates(policies)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has duplicate policy names: " + HELPER.explain(policies));

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}