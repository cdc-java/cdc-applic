package cdc.applic.dictionaries.edit.checks.rules;

import java.util.List;

import cdc.applic.dictionaries.edit.EnDictionary;
import cdc.applic.dictionaries.edit.EnRef;
import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.util.lang.Introspection;

public class EnRegistryParentsRefsMustBeValid extends EnAbstractRefMustBeValid<EnRegistry, EnDictionary<?>> {
    public static final String NAME = "REGISTRY_PARENTS_REFS_MUST_BE_VALID";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Parent of a registry must exist.")
                                                         .appliesTo("Registries"),
                               SEVERITY);

    public EnRegistryParentsRefsMustBeValid(SnapshotManager manager) {
        super(manager,
              EnRegistry.class,
              Introspection.uncheckedCast(EnDictionary.class),
              RULE,
              "parent refs");
    }

    @Override
    protected List<EnRef<EnDictionary<?>>> getRefs(EnRegistry object) {
        return object.getParentRefs();
    }
}