package cdc.applic.dictionaries.edit.checks;

import cdc.applic.dictionaries.edit.EnLocation;
import cdc.applic.dictionaries.edit.EnRepository;
import cdc.applic.dictionaries.edit.checks.rules.EnProfile;
import cdc.applic.dictionaries.edit.checks.rules.EnRegistryNamesMustBeUnique;
import cdc.issues.checks.RootChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;

public class EnRepositoryChecker extends RootChecker<EnRepository> {
    public EnRepositoryChecker(String projectName,
                               String snapshotName,
                               EnRepository root) {
        super(SnapshotManager.builder()
                             .projectName(projectName)
                             .snapshotName(snapshotName)
                             .profile(EnProfile.PROFILE)
                             .stats(true)
                             .build(),
              EnRepository.class,
              LocatedObject.of(root,
                               EnLocation.builder()
                                         .element(root)
                                         .build()));

        add(new EnDictionariesChecker(getManager()));
        add(new EnRegistryNamesMustBeUnique(getManager()));
    }
}