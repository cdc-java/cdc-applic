package cdc.applic.dictionaries.edit.checks.rules;

import cdc.issues.impl.ProfileImpl;
import cdc.issues.rules.Rule;

public final class EnProfile {
    private EnProfile() {
    }

    public static final String DOMAIN = "EnApplic";

    private static void add(Rule rule) {
        PROFILE.add(rule);
    }

    public static final ProfileImpl PROFILE = new ProfileImpl("EnProfile");
    static {
        add(EnAliasExpressionMustBeSyntacticallyValid.RULE);
        add(EnAssertionExpressionMustBeSyntacticallyValid.RULE);
        add(EnDictionaryContextExpressionMustBeSyntacticallyValid.RULE);
        add(EnDictionaryUsageItemsRefsMustBeValid.RULE);
        add(EnDictionaryUsageTypesRefsMustBeValid.RULE);
        add(EnItemNamesAndSynonymsMustBeUnique.RULE);
        add(EnItemNamesAndSynonymsMustNotBeAmbiguous.RULE);
        add(EnEnumeratedTypeLiteralsAndSynonymsMustBeUnique.RULE);
        add(EnEnumeratedTypePartialOrderMustBeValid.RULE);
        add(EnEnumeratedTypeValuesRefsMustBeValid.RULE);
        add(EnEnumeratedValueLiteralIsMandatory.RULE);
        add(EnIntegerDomainMustBeValid.RULE);
        add(EnNameIsMandatory.RULE);
        add(EnNamingConventionNamesMustBeUnique.RULE);
        add(EnPatternIsMandatory.RULE);
        add(EnPatternMustBeValid.RULE);
        add(EnPolicyNamesMustBeUnique.RULE);
        add(EnPropertyTypeRefMustBeValid.RULE);
        add(EnRealDomainMustBeValid.RULE);
        add(EnRegistryDependencyCyclesAreForbidden.RULE);
        add(EnRegistryNamesMustBeUnique.RULE);
        add(EnRegistryParentsRefsMustBeValid.RULE);
        add(EnSynonymsMustBeValid.RULE);
        add(EnSynonymsNamingConventionsRefsMustBeValid.RULE);
        add(EnTypeDomainIsMandatory.RULE);
        add(EnTypeNamesAndSynonymsMustBeUnique.RULE);
        add(EnTypeNamesAndSynonymsMustNotBeAmbiguous.RULE);
        add(EnTypeS1000DAttributesMustBeCompliant.RULE);
    }
}