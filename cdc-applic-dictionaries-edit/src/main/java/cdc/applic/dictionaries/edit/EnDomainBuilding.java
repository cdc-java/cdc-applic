package cdc.applic.dictionaries.edit;

public interface EnDomainBuilding<B> {
    public B domain(String domain);
}