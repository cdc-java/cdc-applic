package cdc.applic.dictionaries.edit.checks;

import java.util.List;
import java.util.stream.Collectors;

import cdc.applic.dictionaries.edit.EnDictionary;
import cdc.applic.dictionaries.edit.EnDictionaryOwner;
import cdc.applic.dictionaries.edit.EnLocation;
import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;
import cdc.util.lang.Introspection;

public class EnDictionariesChecker extends AbstractPartsChecker<EnDictionaryOwner, EnDictionary<?>, EnDictionariesChecker> {
    public static final String KEY = "DictionariesChecker";

    public EnDictionariesChecker(SnapshotManager manager) {
        super(manager,
              EnDictionaryOwner.class,
              Introspection.uncheckedCast(EnDictionary.class),
              new EnDictionaryChecker(manager));
        manager.register(KEY, this);
    }

    @Override
    protected List<LocatedObject<? extends EnDictionary<?>>> getParts(EnDictionaryOwner object) {
        final List<? extends EnDictionary<?>> parts = object.getDictionaries();
        return parts.stream()
                    .map(x -> LocatedObject.of(x, EnLocation.builder().element(x).build()))
                    .collect(Collectors.toList());
    }
}