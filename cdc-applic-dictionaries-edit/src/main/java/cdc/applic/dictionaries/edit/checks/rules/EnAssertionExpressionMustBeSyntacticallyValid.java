package cdc.applic.dictionaries.edit.checks.rules;

import cdc.applic.dictionaries.edit.EnUserDefinedAssertion;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.applic.expressions.Expression;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;

public class EnAssertionExpressionMustBeSyntacticallyValid
        extends EnAbstractExpressionMustBeSyntacticallyValid<EnUserDefinedAssertion> {
    public static final String NAME = "ASSERTION_EXPRESSION_MUST_BE_SYNTACTICALLY_VALID";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Expression of a user defined assertion must be syntactically valid.")
                                                         .appliesTo("User defined assertions"),
                               SEVERITY);

    public EnAssertionExpressionMustBeSyntacticallyValid(SnapshotManager manager) {
        super(manager,
              EnUserDefinedAssertion.class,
              RULE);
    }

    @Override
    protected Expression getExpression(EnUserDefinedAssertion object) {
        return object.getExpression();
    }
}