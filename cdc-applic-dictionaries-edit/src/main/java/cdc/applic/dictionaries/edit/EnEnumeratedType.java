package cdc.applic.dictionaries.edit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import cdc.graphs.core.SlimGraph;
import cdc.graphs.impl.BasicGraphEdge;

public final class EnEnumeratedType extends EnType
        implements EnFrozenItem {
    public static final String KIND = "EnumeratedType";
    public static final Comparator<BasicGraphEdge<EnRef<EnEnumeratedValue>>> SOURCE_COMPARATOR =
            Comparator.comparing(e -> e.getSource().getId());
    public static final Comparator<BasicGraphEdge<EnRef<EnEnumeratedValue>>> TARGET_COMPARATOR =
            Comparator.comparing(e -> e.getTarget().getId());
    public static final Comparator<BasicGraphEdge<EnRef<EnEnumeratedValue>>> SOURCE_TARGET_COMPARATOR =
            SOURCE_COMPARATOR.thenComparing(TARGET_COMPARATOR);

    private boolean frozen;

    /**
     * [source -> {(source, target)} map.
     */
    private final Map<EnRef<EnEnumeratedValue>, Set<BasicGraphEdge<EnRef<EnEnumeratedValue>>>> lessThan = new HashMap<>();

    protected EnEnumeratedType(Builder builder) {
        super(builder);
        this.frozen = builder.frozen;
        for (final LessThanPair lt : builder.lessThanPairs) {
            addLessThan(lt.value1Id(), lt.value2Id());
        }

        addToOwner();
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public EnRef<EnEnumeratedType> getRef() {
        return getRef(EnEnumeratedType.class);
    }

    @Override
    public boolean isFrozen() {
        return frozen;
    }

    @Override
    public void setFrozen(boolean frozen) {
        this.frozen = frozen;

        fireSemanticChange(EnNames.FROZEN);
    }

    public List<EnEnumeratedValue> getValues() {
        return getChildren(EnEnumeratedValue.class);
    }

    public Set<BasicGraphEdge<EnRef<EnEnumeratedValue>>> getLessThanEdges() {
        final Set<BasicGraphEdge<EnRef<EnEnumeratedValue>>> set = new HashSet<>();
        for (final Set<BasicGraphEdge<EnRef<EnEnumeratedValue>>> edges : lessThan.values()) {
            set.addAll(edges);
        }
        return set;
    }

    public void addLessThan(String value1Id,
                            String value2Id) {
        // We can reference values that don't not exist or don't belong to this type
        final EnRef<EnEnumeratedValue> ref1 = EnRef.of(this, EnEnumeratedValue.class, value1Id);
        final EnRef<EnEnumeratedValue> ref2 = EnRef.of(this, EnEnumeratedValue.class, value2Id);

        final BasicGraphEdge<EnRef<EnEnumeratedValue>> edge = new BasicGraphEdge<>(ref1, ref2);
        final Set<BasicGraphEdge<EnRef<EnEnumeratedValue>>> edges = lessThan.computeIfAbsent(ref1, k -> new HashSet<>());
        edges.add(edge);

        fireSemanticChange(EnNames.LESS_THAN);
    }

    public void addLessThan(EnEnumeratedValue value1,
                            EnEnumeratedValue value2) {
        addLessThan(value1.getId(), value2.getId());
    }

    public void removeLessThan(String value1Id,
                               String value2Id) {
        final EnRef<EnEnumeratedValue> ref1 = EnRef.of(this, EnEnumeratedValue.class, value1Id);
        if (lessThan.containsKey(ref1)) {
            final EnRef<EnEnumeratedValue> ref2 = EnRef.of(this, EnEnumeratedValue.class, value2Id);
            final BasicGraphEdge<EnRef<EnEnumeratedValue>> edge = new BasicGraphEdge<>(ref1, ref2);
            final Set<BasicGraphEdge<EnRef<EnEnumeratedValue>>> edges = lessThan.get(ref1);
            edges.remove(edge);
            if (edges.isEmpty()) {
                lessThan.remove(ref1);
            }

            fireSemanticChange(EnNames.LESS_THAN);
        }
    }

    public void removeLessThan(EnEnumeratedValue value1,
                               EnEnumeratedValue value2) {
        removeLessThan(value1.getId(), value2.getId());
    }

    public EnEnumeratedValue.Builder enumeratedValue() {
        return EnEnumeratedValue.builder(this);
    }

    public Set<EnRef<EnEnumeratedValue>> getLessThan(EnRef<EnEnumeratedValue> ref) {
        return lessThan.getOrDefault(ref, Collections.emptySet())
                       .stream()
                       .map(BasicGraphEdge::getTarget)
                       .collect(Collectors.toSet());
    }

    /**
     * Returns {@code true} if partial order is valid.
     * <p>
     * This does not mean <em>less than</em> edges are valid: they may reference an invalid (unknown or foreign) value.
     *
     * @return {@code true} if partial order is valid.
     */
    public boolean hasValidPartialOrder() {
        for (final EnRef<EnEnumeratedValue> ref1 : lessThan.keySet()) {
            final boolean inCycle = SlimGraph.nodeIsCycleMember(ref1,
                                                                this::getLessThan);
            if (inCycle) {
                return false;
            }
        }
        return true;
    }

    static Builder builder(EnRegistry owner) {
        return new Builder(owner);
    }

    private static record LessThanPair(String value1Id,
                                       String value2Id) {
    }

    public static final class Builder
            extends EnType.Builder<Builder, EnEnumeratedType>
            implements EnFrozenBuilding<Builder> {
        private boolean frozen = false;
        private final List<LessThanPair> lessThanPairs = new ArrayList<>();

        protected Builder(EnRegistry owner) {
            super(owner);
        }

        @Override
        public Class<EnEnumeratedType> getBuiltClass() {
            return EnEnumeratedType.class;
        }

        @Override
        public Builder frozen(boolean frozen) {
            this.frozen = frozen;
            return self();
        }

        public Builder lessThan(String value1Id,
                                String value2Id) {
            this.lessThanPairs.add(new LessThanPair(value1Id, value2Id));
            return self();
        }

        public Builder lessThan(EnEnumeratedValue value1,
                                EnEnumeratedValue value2) {
            return lessThan(value1.getId(), value2.getId());
        }

        @Override
        public EnEnumeratedType build() {
            return wrap(new EnEnumeratedType(this));
        }
    }
}