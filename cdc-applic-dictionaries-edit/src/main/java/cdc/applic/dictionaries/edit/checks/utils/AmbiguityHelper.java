package cdc.applic.dictionaries.edit.checks.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class AmbiguityHelper<E, K> {
    private final Function<E, Set<K>> keysExtractor;
    private final Predicate<K> keyValidator;

    private AmbiguityHelper(Function<E, Set<K>> keysExtractor,
                            Predicate<K> keyValidator) {
        this.keysExtractor = keysExtractor;
        this.keyValidator = keyValidator;
    }

    public static <E, K> AmbiguityHelper<E, K> ofN(Function<E, Set<K>> keysExtractor,
                                                   Predicate<K> keyValidator) {
        return new AmbiguityHelper<>(keysExtractor,
                                     keyValidator);
    }

    public static <E, K> AmbiguityHelper<E, K> of1(Function<E, K> keyExtractor,
                                                   Predicate<K> keyValidator) {
        final Function<E, Set<K>> keysExtractor =
                e -> {
                    final K k = keyExtractor.apply(e);
                    return keyValidator.test(k) ? Set.of(k) : Set.of();
                };
        return new AmbiguityHelper<>(keysExtractor,
                                     keyValidator);
    }

    public List<K> getAmbiguities(List<? extends E> elements,
                                  List<? extends E> with) {
        final List<K> list = elements.stream()
                                     .flatMap(e -> keysExtractor.apply(e).stream())
                                     .filter(keyValidator)
                                     .toList();
        final Set<K> set = with.stream()
                               .flatMap(e -> keysExtractor.apply(e).stream())
                               .filter(keyValidator)
                               .collect(Collectors.toSet());

        final List<K> ambiguities = new ArrayList<>();
        for (final K k : list) {
            if (set.contains(k)) {
                ambiguities.add(k);
            }
        }

        return ambiguities;
    }

    public boolean hasAmbiguities(List<? extends E> elements,
                                  List<? extends E> with) {
        return !getAmbiguities(elements, with).isEmpty();
    }

    public String explain(List<? extends E> elements,
                          List<? extends E> with) {
        final List<K> ambiguities = getAmbiguities(elements, with);

        return ambiguities.stream()
                          .map(Object::toString)
                          .collect(Collectors.joining(", "));
    }
}