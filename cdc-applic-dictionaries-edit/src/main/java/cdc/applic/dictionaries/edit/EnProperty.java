package cdc.applic.dictionaries.edit;

import java.util.HashMap;
import java.util.Map;

public final class EnProperty extends EnAbstractOwnedElement<EnRegistry>
        implements EnNamedDItem {
    public static final String KIND = "Property";
    private String name;
    private final EnSynonyms synonyms;
    private int ordinal;
    private EnRef<EnType> typeRef;

    protected EnProperty(Builder builder) {
        super(builder);
        this.name = builder.name;
        this.synonyms = new EnSynonyms(this, builder.synonyms);
        this.ordinal = builder.ordinal;
        this.typeRef = EnRef.of(this, EnType.class, builder.typeId);
        addToOwner();
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public EnRef<EnProperty> getRef() {
        return getRef(EnProperty.class);
    }

    @Override
    public final void setName(String name) {
        this.name = name;

        fireSemanticChange(EnNames.NAME);
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public final EnSynonyms getSynonyms() {
        return synonyms;
    }

    @Override
    public int getOrdinal() {
        return ordinal;
    }

    @Override
    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;

        fireWritingChange(EnNames.ORDINAL);
    }

    public EnRef<EnType> getTypeRef() {
        return typeRef;
    }

    public void setTypeId(String typeId) {
        this.typeRef = EnRef.of(this, EnType.class, typeId);

        fireSemanticChange(EnNames.TYPE_ID);
    }

    public void setType(EnType type) {
        this.typeRef = EnRef.of(this, EnType.class, type);

        fireSemanticChange(EnNames.TYPE);
    }

    static Builder builder(EnRegistry owner) {
        return new Builder(owner);
    }

    public static final class Builder
            extends EnAbstractOwnedElement.Builder<Builder, EnProperty, EnRegistry>
            implements EnNameBuilding<Builder>, EnSynonymsBuilding<Builder>, EnOrdinalBuilding<Builder> {
        private String name;
        private final Map<String, String> synonyms = new HashMap<>();
        private int ordinal = 0;
        private String typeId = null;

        protected Builder(EnRegistry owner) {
            super(owner);
        }

        @Override
        public Class<EnProperty> getBuiltClass() {
            return EnProperty.class;
        }

        @Override
        public Builder name(String name) {
            this.name = name;
            return self();
        }

        @Override
        public Builder synonym(String conventionId,
                               String synonym) {
            this.synonyms.put(conventionId, synonym);
            return self();
        }

        @Override
        public Builder ordinal(int ordinal) {
            this.ordinal = ordinal;
            return self();
        }

        public Builder typeId(String typeId) {
            this.typeId = typeId;
            return self();
        }

        public Builder type(EnType type) {
            return typeId(type == null ? null : type.getId());
        }

        @Override
        public EnProperty build() {
            return wrap(new EnProperty(this));
        }
    }
}