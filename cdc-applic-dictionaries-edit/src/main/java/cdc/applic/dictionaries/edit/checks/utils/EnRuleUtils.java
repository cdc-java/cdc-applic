package cdc.applic.dictionaries.edit.checks.utils;

import java.util.function.Consumer;

import cdc.applic.dictionaries.edit.checks.rules.EnProfile;
import cdc.issues.IssueSeverity;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;
import cdc.issues.rules.RuleUtils;

public final class EnRuleUtils {
    private EnRuleUtils() {
    }

    public static Rule define(String name,
                              Consumer<RuleDescription.Builder<?>> descriptionBuilder,
                              IssueSeverity severity) {
        return RuleUtils.define(EnProfile.DOMAIN,
                                name,
                                descriptionBuilder,
                                severity);
    }
}