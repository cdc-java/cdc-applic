package cdc.applic.dictionaries.edit.checks.rules;

import cdc.applic.dictionaries.edit.EnElement;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.expressions.Expression;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public abstract class EnAbstractExpressionMustBeSyntacticallyValid<O extends EnElement> extends EnAbstractRuleChecker<O> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;

    protected EnAbstractExpressionMustBeSyntacticallyValid(SnapshotManager manager,
                                                           Class<O> objectClass,
                                                           Rule rule) {
        super(manager,
              objectClass,
              rule);
    }

    protected abstract Expression getExpression(O object);

    public static boolean isValid(Expression expression) {
        return expression.isValid();
    }

    @Override
    public CheckResult check(CheckContext context,
                             O object,
                             Location location) {
        final Expression expression = getExpression(object);
        if (!isValid(expression)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has syntactically invalid expression: " + expression);

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}