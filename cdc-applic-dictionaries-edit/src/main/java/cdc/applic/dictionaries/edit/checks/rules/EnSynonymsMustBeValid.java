package cdc.applic.dictionaries.edit.checks.rules;

import java.util.List;
import java.util.Set;

import cdc.applic.dictionaries.edit.EnSynonymsItem;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.util.strings.StringUtils;

public class EnSynonymsMustBeValid extends EnAbstractRuleChecker<EnSynonymsItem> {
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    public static final String NAME = "SYNONYMS_MUST_BE_VALID";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Synonyms must be valid.")
                                                         .appliesTo("Aliases",
                                                                    "Enumerated values",
                                                                    "Properties",
                                                                    "Types"),
                               SEVERITY);

    public EnSynonymsMustBeValid(SnapshotManager manager) {
        super(manager,
              EnSynonymsItem.class,
              RULE);
    }

    private static boolean isValid(String synonym) {
        return !StringUtils.isNullOrEmpty(synonym);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnSynonymsItem object,
                             Location location) {
        final Set<String> synonyms = object.getSynonyms().getSynonyms();
        final List<String> invalidSynonyms = synonyms.stream()
                                                     .filter(x -> !isValid(x))
                                                     .sorted()
                                                     .toList();
        if (!invalidSynonyms.isEmpty()) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has invalid synonyms: " + invalidSynonyms);

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}