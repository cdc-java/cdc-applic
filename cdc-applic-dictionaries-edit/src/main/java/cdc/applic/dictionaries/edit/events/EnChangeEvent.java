package cdc.applic.dictionaries.edit.events;

import java.time.format.DateTimeFormatter;

import cdc.applic.dictionaries.edit.EnAbstractElement;

public class EnChangeEvent extends EnEvent {
    private final Impact impact;
    private final String message;

    protected EnChangeEvent(EnAbstractElement target,
                            Impact impact,
                            String message) {
        super(target);
        this.impact = impact;
        this.message = message;
    }

    public enum Impact {
        SEMANTIC,
        WRITING,
        DESCRIPTIONS
    }

    public Impact getImpact() {
        return impact;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "[" + getLocalDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
                + " " + getClass().getSimpleName()
                + " " + getTarget().getId()
                + " " + getImpact()
                + " " + getMessage()
                + "]";
    }

    public static EnChangeEvent of(EnAbstractElement target,
                                   Impact impact,
                                   String message) {
        return new EnChangeEvent(target, impact, message);
    }
}