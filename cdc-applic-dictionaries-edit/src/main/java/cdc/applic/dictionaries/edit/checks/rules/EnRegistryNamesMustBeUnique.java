package cdc.applic.dictionaries.edit.checks.rules;

import java.util.List;

import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.applic.dictionaries.edit.EnRepository;
import cdc.applic.dictionaries.edit.checks.utils.DuplicateHelper;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class EnRegistryNamesMustBeUnique extends EnAbstractRuleChecker<EnRepository> {
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    public static final String NAME = "REGISTRY_NAMES_MUST_BE_UNIQUE";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Names of registries in a repository must be unique.")
                                                         .appliesTo("Repositories"),
                               SEVERITY);

    private static final DuplicateHelper<EnRegistry, String> HELPER =
            DuplicateHelper.of1(EnRegistry::getName,
                                EnNameIsMandatory::isDefined);

    public EnRegistryNamesMustBeUnique(SnapshotManager manager) {
        super(manager,
              EnRepository.class,
              RULE);
    }

    public static boolean isCompliant(List<EnRegistry> registries) {
        return !HELPER.hasDuplicates(registries);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnRepository object,
                             Location location) {
        final List<EnRegistry> registries = object.getRegistries();
        if (HELPER.hasDuplicates(registries)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has duplicate registry names: " + HELPER.explain(registries));

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}