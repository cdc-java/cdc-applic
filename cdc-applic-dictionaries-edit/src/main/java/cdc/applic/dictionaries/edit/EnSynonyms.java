package cdc.applic.dictionaries.edit;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import cdc.util.lang.Checks;

public final class EnSynonyms {
    private final EnAbstractElement owner;
    private final Map<EnRef<EnNamingConvention>, String> map = new HashMap<>();

    protected EnSynonyms(EnAbstractElement owner,
                         Map<String, String> synonyms) {
        this.owner = Checks.isNotNull(owner, EnNames.OWNER);
        for (final Map.Entry<String, String> entry : synonyms.entrySet()) {
            // Do not call set() to avoid firing change events
            map.put(EnRef.of(owner, EnNamingConvention.class, entry.getKey()),
                    entry.getValue());
        }
    }

    public EnAbstractElement getOwner() {
        return owner;
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    public void set(EnNamingConvention convention,
                    String synonym) {
        map.put(EnRef.of(owner, EnNamingConvention.class, convention),
                synonym);
        owner.fireSemanticChange(EnNames.SYNONYMS);
    }

    public void set(String conventionId,
                    String synonym) {
        map.put(EnRef.of(owner, EnNamingConvention.class, conventionId),
                synonym);
        owner.fireSemanticChange(EnNames.SYNONYMS);
    }

    public void remove(EnRef<EnNamingConvention> conventionRef) {
        map.remove(conventionRef);
        owner.fireSemanticChange(EnNames.SYNONYMS);
    }

    public Set<String> getSynonyms() {
        return new HashSet<>(map.values());
    }

    public Set<String> getRefAndSynonyms(String ref) {
        final Set<String> set = new HashSet<>(map.values());
        set.add(ref);
        return set;
    }

    public Set<EnRef<EnNamingConvention>> getNamingConventionRefs() {
        return map.keySet();
    }

    public String getSynonym(EnRef<EnNamingConvention> conventionRef) {
        return map.get(conventionRef);
    }
}