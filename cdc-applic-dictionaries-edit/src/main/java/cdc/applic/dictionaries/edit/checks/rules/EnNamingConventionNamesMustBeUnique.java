package cdc.applic.dictionaries.edit.checks.rules;

import java.util.List;

import cdc.applic.dictionaries.edit.EnNamingConvention;
import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.applic.dictionaries.edit.checks.utils.DuplicateHelper;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class EnNamingConventionNamesMustBeUnique extends EnAbstractRuleChecker<EnRegistry> {
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    public static final String NAME = "NAMING_CONVENTION_NAMES_MUST_BE_UNIQUE";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Names of naming conventions of a registry must be unique.")
                                                         .appliesTo("Registries"),
                               SEVERITY);

    private static final DuplicateHelper<EnNamingConvention, String> HELPER =
            DuplicateHelper.of1(EnNamingConvention::getName,
                                EnNameIsMandatory::isDefined);

    public EnNamingConventionNamesMustBeUnique(SnapshotManager manager) {
        super(manager,
              EnRegistry.class,
              RULE);
    }

    public static boolean isCompliant(List<EnNamingConvention> conventions) {
        return !HELPER.hasDuplicates(conventions);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnRegistry object,
                             Location location) {
        final List<EnNamingConvention> conventions = object.getNamingConventions();
        if (HELPER.hasDuplicates(conventions)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has duplicate naming convention names: " + HELPER.explain(conventions));

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}