package cdc.applic.dictionaries.edit;

import java.util.List;

public interface EnRegistryOwner extends EnDictionaryOwner {
    public default List<EnRegistry> getRegistries() {
        return getChildren(EnRegistry.class);
    }

    public EnRegistry.Builder registry();
}