package cdc.applic.dictionaries.edit;

/**
 * Interface implemented by builders of items that have a name.
 *
 * @param <B> The builder type.
 */
public interface EnNameBuilding<B> {
    /**
     * Sets the name (possibly {@code null}).
     *
     * @param name The name.
     * @return The builder.
     */
    public B name(String name);
}