package cdc.applic.dictionaries.edit.checks.rules;

import cdc.applic.dictionaries.edit.EnPatternType;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.util.strings.StringUtils;

public class EnPatternIsMandatory extends EnAbstractRuleChecker<EnPatternType> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final String NAME = "PATTERN_IS_MANDATORY";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Pattern types must have a pattern.")
                                                         .appliesTo("Pattern types"),
                               SEVERITY);

    public EnPatternIsMandatory(SnapshotManager manager) {
        super(manager,
              EnPatternType.class,
              RULE);
    }

    public static boolean isDefined(String pattern) {
        return !StringUtils.isNullOrEmpty(pattern);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnPatternType object,
                             Location location) {
        final String pattern = object.getPattern();
        if (!isDefined(pattern)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has no pattern");

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}