package cdc.applic.dictionaries.edit.checks.rules;

import java.util.List;

import cdc.applic.dictionaries.edit.EnNamedDItem;
import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.applic.dictionaries.edit.checks.utils.DuplicateHelper;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class EnItemNamesAndSynonymsMustBeUnique extends EnAbstractRuleChecker<EnRegistry> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final String NAME = "ITEM_NAMES_AND_SYNONYMS_MUST_BE_UNIQUE";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("In a registry, items (properties and aliases) names and synonyms must be unique."
                                       + "\nIn the context of a registry, a name or synonym can not designate several properties or aliases.")
                                                         .appliesTo("Registries"),
                               SEVERITY);

    private static final DuplicateHelper<EnNamedDItem, String> HELPER =
            DuplicateHelper.ofN(EnNamedDItem::getNameAndSynonyms,
                                EnNameIsMandatory::isDefined);

    public EnItemNamesAndSynonymsMustBeUnique(SnapshotManager manager) {
        super(manager,
              EnRegistry.class,
              RULE);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnRegistry object,
                             Location location) {
        final List<EnNamedDItem> items = object.getNamedDItems();
        if (HELPER.hasDuplicates(items)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has duplicate items names or synonyms: " + HELPER.explain(items));

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}