package cdc.applic.dictionaries.edit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import cdc.applic.dictionaries.edit.events.EnChangeEvent;
import cdc.applic.dictionaries.edit.events.EnCreateEvent;
import cdc.applic.dictionaries.edit.events.EnEvent;
import cdc.applic.dictionaries.edit.events.EnRemoveEvent;
import cdc.util.lang.Checks;
import cdc.util.lang.NotFoundException;

public final class EnRepository extends EnAbstractElement implements EnRegistryOwner {
    public static final String KIND = "Repository";
    private final Map<String, EnElement> idToElement = new HashMap<>();
    private final List<Consumer<EnEvent>> handlers = new ArrayList<>();
    private long eventSerial = 0L;

    private void fireEvent(EnEvent event) {
        eventSerial++;
        for (final Consumer<EnEvent> handler : handlers) {
            handler.accept(event);
        }
    }

    <X extends EnAbstractElement> X addToRepository(X element) {
        Checks.assertFalse(idToElement.containsKey(element.getId()), "Duplicate element id {}", element.getId());

        idToElement.put(element.getId(), element);

        final EnCreateEvent event = EnCreateEvent.of(element);
        fireEvent(event);
        return element;
    }

    void fireChange(EnAbstractElement element,
                    EnChangeEvent.Impact impact,
                    String message) {
        final EnChangeEvent event = EnChangeEvent.of(element, impact, message);
        fireEvent(event);
    }

    void removeFromRepository(EnAbstractOwnedElement<?> element) {
        idToElement.remove(element.getId());
        final EnRemoveEvent event = EnRemoveEvent.of(element);
        fireEvent(event);
    }

    private int nextIdNumber = 0;

    static String newId(Class<? extends EnElement> cls,
                        int index) {
        return cls.getSimpleName() + "@" + index;
    }

    protected EnRepository(Builder builder) {
        super(builder);

        addToRepository(this);
    }

    public long getEventSerial() {
        return eventSerial;
    }

    @Override
    public String getKind() {
        return KIND;
    }

    String newId(Class<? extends EnElement> cls) {
        nextIdNumber++;
        return newId(cls, nextIdNumber);
    }

    @Override
    public EnElement getOwner() {
        return null;
    }

    @Override
    public int getIndex() {
        return -1;
    }

    @Override
    public EnRepository getRepository() {
        return this;
    }

    public void addHandler(Consumer<EnEvent> handler) {
        handlers.add(handler);
    }

    public void removeHandler(Consumer<EnEvent> handler) {
        handlers.remove(handler);
    }

    /**
     * @param id The element id.
     * @return {@code true} if an element identified by {@code id} exists.
     */
    public boolean hasElementWithId(String id) {
        return idToElement.containsKey(id);
    }

    /**
     * @param <X> The element type.
     * @param id The element id.
     * @param elementClass The element class.
     * @return {@code true} if an element identified by {@code id} exists and is an instance of {@code elementClass}.
     */
    public <X extends EnElement> boolean hasElementWithId(String id,
                                                          Class<X> elementClass) {
        final EnElement e = idToElement.get(id);
        return e != null && elementClass.isInstance(e);
    }

    /**
     * @param id The element id.
     * @return The element identified by {@code id}.
     * @throws NotFoundException When no element is identified by {@code id}.
     */
    public EnElement getElementWithId(String id) {
        final EnElement result = idToElement.get(id);
        if (result == null) {
            throw new NotFoundException("No element with id '" + id + "' found in " + this);
        }
        return result;
    }

    /**
     * @param <X> The element type.
     * @param id The element id.
     * @param elementClass The element class.
     * @return The element identified by {@code id}.
     * @throws NotFoundException When no element is identified by {@code id}.
     * @throws ClassCastException When an element identified by {@code id} exists,
     *             but can not be cast to {@code elementClass}.
     */
    public <X extends EnElement> X getElementWithId(String id,
                                                    Class<X> elementClass) {
        return elementClass.cast(getElementWithId(id));
    }

    @Override
    public EnRegistry.Builder registry() {
        return EnRegistry.builder(this);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder
            extends EnAbstractElement.Builder<Builder, EnRepository> {

        protected Builder() {
            super();
            id(newId(EnRepository.class, 0));
        }

        @Override
        public Class<EnRepository> getBuiltClass() {
            return EnRepository.class;
        }

        @Override
        public EnRepository build() {
            return new EnRepository(this);
        }
    }
}