package cdc.applic.dictionaries.edit;

import java.util.Comparator;

/**
 * Interface implemented by items that have a name.
 */
public interface EnNameItem extends EnElement {
    public static final Comparator<EnNameItem> NAME_COMPARATOR =
            Comparator.comparing(EnNameItem::getName);

    /**
     * @return The item name, possibly {@code null}.
     */
    public String getName();

    /**
     * Set the item name ({@code null} is valid).
     *
     * @param name The name.
     */
    public void setName(String name);
}