package cdc.applic.dictionaries.edit;

public interface EnFrozenItem extends EnElement {
    public boolean isFrozen();

    public void setFrozen(boolean frozen);
}