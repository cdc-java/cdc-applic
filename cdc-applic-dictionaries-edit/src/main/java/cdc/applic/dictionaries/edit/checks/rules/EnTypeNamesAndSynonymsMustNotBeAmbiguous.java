package cdc.applic.dictionaries.edit.checks.rules;

import java.util.List;

import cdc.applic.dictionaries.edit.EnType;
import cdc.applic.dictionaries.edit.EnQName;
import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.applic.dictionaries.edit.checks.utils.AmbiguityHelper;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class EnTypeNamesAndSynonymsMustNotBeAmbiguous extends EnAbstractRuleChecker<EnRegistry> {
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    public static final String NAME = "TYPE_NAMES_AND_SYNONYMS_MUST_NOT_BE_AMBIGUOUS";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("In a registry, names and synonyms of types must not be ambiguous."
                                       + "\nThey must not be identical to names or synonyms of inherited types.")
                                                         .appliesTo("Registries"),
                               SEVERITY);

    public static AmbiguityHelper<EnType, EnQName> helper(EnRegistry registry) {
        return AmbiguityHelper.ofN((EnType t) -> t.getOwner() == registry
                ? EnQName.removePrefix(t.getQNameAndQSynonyms())
                : t.getQNameAndQSynonyms(),
                                   EnQName::isValid);

    }

    public EnTypeNamesAndSynonymsMustNotBeAmbiguous(SnapshotManager manager) {
        super(manager,
              EnRegistry.class,
              RULE);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnRegistry object,
                             Location location) {
        object.getAllAncestors(false);
        final List<? extends EnType> types = object.getTypes();
        final List<? extends EnType> others = object.getAllTypes(false);
        others.removeAll(types);
        final AmbiguityHelper<EnType, EnQName> helper = helper(object);
        if (helper.hasAmbiguities(types, others)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has ambiguous type names or synonyms: " + helper.explain(types, others));

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}