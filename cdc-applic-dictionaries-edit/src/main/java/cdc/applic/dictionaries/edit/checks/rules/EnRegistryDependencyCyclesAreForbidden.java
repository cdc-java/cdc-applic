package cdc.applic.dictionaries.edit.checks.rules;

import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class EnRegistryDependencyCyclesAreForbidden extends EnAbstractRuleChecker<EnRegistry> {
    public static final IssueSeverity SEVERITY = IssueSeverity.BLOCKER;
    public static final String NAME = "REGISTRY_DEPENDENCY_CYCLES_ARE_FORBIDDEN";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Registries must not have dependency cycles.")
                                                         .appliesTo("Registries"),
                               SEVERITY);

    public EnRegistryDependencyCyclesAreForbidden(SnapshotManager manager) {
        super(manager,
              EnRegistry.class,
              RULE);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnRegistry object,
                             Location location) {
        if (object.isCycleMember(false)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has a dependency cycle");

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}