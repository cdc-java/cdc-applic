package cdc.applic.dictionaries.edit.checks;

import cdc.applic.dictionaries.edit.EnRealType;
import cdc.applic.dictionaries.edit.checks.rules.EnTypeDomainIsMandatory;
import cdc.applic.dictionaries.edit.checks.rules.EnRealDomainMustBeValid;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.SnapshotManager;

public class EnRealTypeChecker extends CompositeChecker<EnRealType> {
    public EnRealTypeChecker(SnapshotManager manager) {
        super(manager,
              EnRealType.class,
              new EnTypeDomainIsMandatory(manager),
              new EnRealDomainMustBeValid(manager));
    }
}