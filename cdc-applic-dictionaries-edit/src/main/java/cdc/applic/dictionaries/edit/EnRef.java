package cdc.applic.dictionaries.edit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import cdc.util.lang.Checks;
import cdc.util.lang.NotFoundException;

public final class EnRef<X extends EnElement> {
    private final EnElement owner;
    private final Class<X> refClass;
    private final String id;

    public static final Comparator<EnRef<?>> ID_COMPARATOR =
            Comparator.comparing(EnRef::getId);

    private EnRef(EnElement owner,
                  Class<X> refClass,
                  String id) {
        this.owner = Checks.isNotNull(owner, EnNames.OWNER);
        this.refClass = Checks.isNotNull(refClass, EnNames.REF_CLASS);
        this.id = id;
    }

    public static <X extends EnElement> EnRef<X> of(EnElement owner,
                                                    Class<X> refClass,
                                                    String id) {
        return new EnRef<>(owner, refClass, id);
    }

    public static <X extends EnElement> EnRef<X> of(EnElement owner,
                                                    Class<X> refClass,
                                                    X element) {
        return new EnRef<>(owner, refClass, element == null ? null : element.getId());
    }

    public EnElement getOwner() {
        return owner;
    }

    public <O extends EnElement> O getOwner(Class<O> ownerClass) {
        return ownerClass.cast(owner);
    }

    public Class<X> getRefClass() {
        return refClass;
    }

    public String getId() {
        return id;
    }

    /**
     * @return {@code true} if the reference can be resolved.
     */
    public boolean canResolve() {
        return owner.getRepository().hasElementWithId(id, refClass);
    }

    /**
     * @return The referenced element.
     * @throws NotFoundException When this reference can not be resolved.
     * @throws ClassCastException When this reference can be resolved,
     *             but the corresponding element can not be cast to {@code X}.
     */
    public X resolve() {
        return owner.getRepository().getElementWithId(id, refClass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner,
                            refClass,
                            id);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof EnRef)) {
            return false;
        }
        final EnRef<?> other = (EnRef<?>) object;
        return owner == other.owner
                && Objects.equals(refClass, other.refClass)
                && Objects.equals(id, other.id);
    }

    @Override
    public String toString() {
        return "[" + owner.getId() + " " + refClass.getSimpleName() + " " + id + "]";
    }

    /**
     * @param refs The references.
     * @return {@code true} if all references can be resolved.
     */
    public static boolean canResolve(Collection<EnRef<?>> refs) {
        for (final EnRef<?> ref : refs) {
            if (!ref.canResolve()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Resolves a list of references.
     *
     * @param <X> The referenced type.
     * @param refs The references.
     * @param fail If {@code true}, and a reference can not be resolved, an exception is thrown.
     * @return The list of resolved references.
     * @throws NotFoundException When a reference can not be resolved and fail is {@code true}.
     * @throws ClassCastException When a reference can be resolved,
     *             but the corresponding element can not be cast to {@code X}.
     */
    public static <X extends EnElement> List<X> resolve(List<EnRef<X>> refs,
                                                        boolean fail) {
        final List<X> list = new ArrayList<>();
        for (final EnRef<X> ref : refs) {
            if (fail || ref.canResolve()) {
                list.add(ref.resolve());
            }
        }
        return list;
    }

    /**
     * Resolves a set of references.
     *
     * @param <X> The referenced type.
     * @param refs The references.
     * @param fail If {@code true}, and a reference can not be resolved, an exception is thrown.
     * @return The set of resolved references.
     * @throws NotFoundException When a reference can not be resolved and fail is {@code true}.
     * @throws ClassCastException When a reference can be resolved,
     *             but the corresponding element can not be cast to {@code X}.
     */
    public static <X extends EnElement> Set<X> resolve(Set<EnRef<X>> refs,
                                                       boolean fail) {
        final Set<X> set = new HashSet<>();
        for (final EnRef<X> ref : refs) {
            if (fail || ref.canResolve()) {
                set.add(ref.resolve());
            }
        }
        return set;
    }
}