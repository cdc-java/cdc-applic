package cdc.applic.dictionaries.edit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cdc.applic.expressions.literals.SName;
import cdc.util.lang.Introspection;

public final class EnRegistry extends EnDictionary<EnRepository> {
    public static final String KIND = "Registry";
    private SName prefix;
    private final List<EnRef<EnDictionary<?>>> parentRefs = new ArrayList<>();

    protected EnRegistry(Builder builder) {
        super(builder);
        this.prefix = builder.prefix;
        for (final String parentId : builder.parentIds) {
            this.parentRefs.add(EnRef.of(this, Introspection.uncheckedCast(EnDictionary.class), parentId));
        }
        addToOwner();
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public EnRef<EnRegistry> getRef() {
        return getRef(EnRegistry.class);
    }

    @Override
    public EnRegistry getRegistry() {
        return this;
    }

    @Override
    public List<EnDictionary<?>> getParents(boolean fail) {
        return EnRef.resolve(getParentRefs(), fail);
    }

    public final void setPrefix(SName prefix) {
        this.prefix = prefix;

        fireSemanticChange(EnNames.PREFIX);
    }

    public final void setPrefix(String prefix) {
        setPrefix(SName.of(prefix));
    }

    public final SName getPrefix() {
        return prefix;
    }

    public void setParents(List<EnDictionary<?>> parents) {
        this.parentRefs.clear();
        for (final EnDictionary<?> parent : parents) {
            this.parentRefs.add(EnRef.of(this, Introspection.uncheckedCast(EnDictionary.class), parent));
        }

        fireSemanticChange(EnNames.PARENTS);
    }

    public void setParents(EnDictionary<?>... parents) {
        setParents(List.of(parents));
    }

    public List<EnRef<EnDictionary<?>>> getParentRefs() {
        return Collections.unmodifiableList(parentRefs);
    }

    public List<? extends EnType> getTypes() {
        return getChildren(EnType.class);
    }

    public List<? extends EnType> getAllTypes(boolean fail) {
        final List<EnType> list = new ArrayList<>();
        for (final EnDictionary<?> dictionary : getAllAncestors(fail)) {
            if (dictionary instanceof EnRegistry) {
                list.addAll(((EnRegistry) dictionary).getTypes());
            }
        }
        return list;
    }

    public boolean isKnownType(EnType type) {
        // FIXME O(n) complexity
        final List<? extends EnType> types = getAllTypes(false);
        return types.contains(type);
    }

    public List<EnNamingConvention> getNamingConventions() {
        return getChildren(EnNamingConvention.class);
    }

    public EnNamingConvention.Builder namingConvention() {
        return EnNamingConvention.builder(this);
    }

    public List<EnNamedDItem> getNamedDItems() {
        return getChildren(EnNamedDItem.class);
    }

    public List<? extends EnNamedDItem> getAllNamedDItems(boolean fail) {
        final List<EnNamedDItem> list = new ArrayList<>();
        for (final EnDictionary<?> dictionary : getAllAncestors(fail)) {
            if (dictionary instanceof EnRegistry) {
                list.addAll(((EnRegistry) dictionary).getNamedDItems());
            }
        }
        return list;
    }

    public boolean isKnownItem(EnNamedDItem item) {
        // FIXME O(n) complexity
        final List<? extends EnNamedDItem> items = getAllNamedDItems(false);
        return items.contains(item);
    }

    public List<EnProperty> getProperties() {
        return getChildren(EnProperty.class);
    }

    public List<EnAlias> getAliases() {
        return getChildren(EnAlias.class);
    }

    public EnBooleanType.Builder booleanType() {
        return EnBooleanType.builder(this);
    }

    public EnIntegerType.Builder integerType() {
        return EnIntegerType.builder(this);
    }

    public EnRealType.Builder realType() {
        return EnRealType.builder(this);
    }

    public EnEnumeratedType.Builder enumeratedType() {
        return EnEnumeratedType.builder(this);
    }

    public EnPatternType.Builder patternType() {
        return EnPatternType.builder(this);
    }

    public EnProperty.Builder property() {
        return EnProperty.builder(this);
    }

    public EnAlias.Builder alias() {
        return EnAlias.builder(this);
    }

    static Builder builder(EnRepository owner) {
        return new Builder(owner);
    }

    public static final class Builder
            extends EnDictionary.Builder<Builder, EnRegistry, EnRepository> {
        private SName prefix;
        private final List<String> parentIds = new ArrayList<>();

        protected Builder(EnRepository owner) {
            super(owner);
        }

        @Override
        public Class<EnRegistry> getBuiltClass() {
            return EnRegistry.class;
        }

        public Builder prefix(SName prefix) {
            this.prefix = prefix;
            return self();
        }

        public Builder prefix(String prefix) {
            return prefix(SName.of(prefix));
        }

        public Builder parentIds(List<String> parentIds) {
            this.parentIds.clear();
            this.parentIds.addAll(parentIds);
            return self();
        }

        public Builder parentIds(String... parentIds) {
            return parentIds(List.of(parentIds));
        }

        public Builder parents(List<EnDictionary<?>> parents) {
            this.parentIds.clear();
            for (final EnDictionary<?> parent : parents) {
                this.parentIds.add(parent.getId());
            }
            return self();
        }

        public Builder parents(EnDictionary<?>... parents) {
            return parents(List.of(parents));
        }

        @Override
        public EnRegistry build() {
            return wrap(new EnRegistry(this));
        }
    }
}