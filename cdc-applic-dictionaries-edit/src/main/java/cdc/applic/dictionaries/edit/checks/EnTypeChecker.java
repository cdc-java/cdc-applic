package cdc.applic.dictionaries.edit.checks;

import cdc.applic.dictionaries.edit.EnType;
import cdc.applic.dictionaries.edit.checks.rules.EnNameIsMandatory;
import cdc.applic.dictionaries.edit.checks.rules.EnSynonymsMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnSynonymsNamingConventionsRefsMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnTypeS1000DAttributesMustBeCompliant;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.checks.SpecializationChecker;

public class EnTypeChecker extends CompositeChecker<EnType> {
    public EnTypeChecker(SnapshotManager manager) {
        super(manager,
              EnType.class,
              new EnNameIsMandatory(manager),
              new EnTypeS1000DAttributesMustBeCompliant(manager),
              new EnSynonymsMustBeValid(manager),
              new EnSynonymsNamingConventionsRefsMustBeValid(manager),
              new SpecializationChecker<>(manager, EnType.class, new EnEnumeratedTypeChecker(manager)),
              new SpecializationChecker<>(manager, EnType.class, new EnIntegerTypeChecker(manager)),
              new SpecializationChecker<>(manager, EnType.class, new EnRealTypeChecker(manager)),
              new SpecializationChecker<>(manager, EnType.class, new EnPatternTypeChecker(manager)));
    }
}