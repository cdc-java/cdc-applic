package cdc.applic.dictionaries.edit;

public interface EnOrdinalBuilding<B> {
    public B ordinal(int ordinal);
}