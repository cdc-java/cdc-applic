package cdc.applic.dictionaries.edit;

public interface EnDomainItem extends EnElement {
    public String getDomain();

    public void setDomain(String domain);
}