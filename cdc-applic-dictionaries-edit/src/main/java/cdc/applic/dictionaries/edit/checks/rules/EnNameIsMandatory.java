package cdc.applic.dictionaries.edit.checks.rules;

import cdc.applic.dictionaries.edit.EnNameItem;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.util.strings.StringUtils;

public class EnNameIsMandatory extends EnAbstractRuleChecker<EnNameItem> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final String NAME = "NAME_IS_MANDATORY";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Named elements must have a name.")
                                                         .appliesTo("Aliases",
                                                                    "Naming Conventions",
                                                                    "Policies",
                                                                    "Properties",
                                                                    "Registries",
                                                                    "Types"),
                               SEVERITY);

    public EnNameIsMandatory(SnapshotManager manager) {
        super(manager,
              EnNameItem.class,
              RULE);
    }

    public static boolean isDefined(String name) {
        return !StringUtils.isNullOrEmpty(name);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnNameItem object,
                             Location location) {
        final String name = object.getName();
        if (!isDefined(name)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has no name");

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}