package cdc.applic.dictionaries.edit.checks.rules;

import java.util.List;

import cdc.applic.dictionaries.edit.EnProperty;
import cdc.applic.dictionaries.edit.EnRef;
import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.applic.dictionaries.edit.EnType;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;

public class EnPropertyTypeRefMustBeValid extends EnAbstractRefMustBeValid<EnProperty, EnType> {
    public static final String NAME = "PROPERTY_TYPE_REF_MUST_BE_VALID";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Type of a property must exist and be usable.")
                                                         .appliesTo("Properties"),
                               SEVERITY);

    public EnPropertyTypeRefMustBeValid(SnapshotManager manager) {
        super(manager,
              EnProperty.class,
              EnType.class,
              RULE,
              "type ref");
    }

    @Override
    protected boolean isValid(EnRef<EnType> ref) {
        if (ref.canResolve()) {
            final EnRegistry registry = ref.getOwner(EnProperty.class).getOwner();
            final EnType type = ref.resolve();
            return registry.isKnownType(type);
        } else {
            return false;
        }
    }

    @Override
    protected List<EnRef<EnType>> getRefs(EnProperty object) {
        return List.of(object.getTypeRef());
    }
}