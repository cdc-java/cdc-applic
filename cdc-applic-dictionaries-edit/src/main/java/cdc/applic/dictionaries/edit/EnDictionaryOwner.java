package cdc.applic.dictionaries.edit;

import java.util.List;

import cdc.util.lang.Introspection;

public interface EnDictionaryOwner extends EnElement {
    public default List<EnDictionary<?>> getDictionaries() {
        return getChildren(Introspection.uncheckedCast(EnDictionary.class));
    }
}