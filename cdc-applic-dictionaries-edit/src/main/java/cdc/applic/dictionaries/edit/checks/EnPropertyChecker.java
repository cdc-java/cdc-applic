package cdc.applic.dictionaries.edit.checks;

import cdc.applic.dictionaries.edit.EnProperty;
import cdc.applic.dictionaries.edit.checks.rules.EnNameIsMandatory;
import cdc.applic.dictionaries.edit.checks.rules.EnPropertyTypeRefMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnSynonymsMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnSynonymsNamingConventionsRefsMustBeValid;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.SnapshotManager;

public class EnPropertyChecker extends CompositeChecker<EnProperty> {
    public EnPropertyChecker(SnapshotManager manager) {
        super(manager,
              EnProperty.class,
              new EnNameIsMandatory(manager),
              new EnSynonymsMustBeValid(manager),
              new EnSynonymsNamingConventionsRefsMustBeValid(manager),
              new EnPropertyTypeRefMustBeValid(manager));
    }
}