package cdc.applic.dictionaries.edit.events;

import cdc.applic.dictionaries.edit.EnAbstractElement;

public class EnRemoveEvent extends EnEvent {
    protected EnRemoveEvent(EnAbstractElement target) {
        super(target);
    }

    public static EnRemoveEvent of(EnAbstractElement target) {
        return new EnRemoveEvent(target);
    }
}