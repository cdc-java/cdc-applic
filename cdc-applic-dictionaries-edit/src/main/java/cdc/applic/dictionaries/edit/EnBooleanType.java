package cdc.applic.dictionaries.edit;

public final class EnBooleanType extends EnType {
    public static final String KIND = "BooleanType";

    protected EnBooleanType(Builder builder) {
        super(builder);
        addToOwner();
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public EnRef<EnBooleanType> getRef() {
        return getRef(EnBooleanType.class);
    }

    static Builder builder(EnRegistry owner) {
        return new Builder(owner);
    }

    public static final class Builder
            extends EnType.Builder<Builder, EnBooleanType> {
        protected Builder(EnRegistry owner) {
            super(owner);
        }

        @Override
        public Class<EnBooleanType> getBuiltClass() {
            return EnBooleanType.class;
        }

        @Override
        public EnBooleanType build() {
            return wrap(new EnBooleanType(this));
        }
    }
}