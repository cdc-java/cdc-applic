package cdc.applic.dictionaries.edit.checks.rules;

import cdc.applic.dictionaries.edit.EnRealType;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.applic.expressions.SyntacticException;
import cdc.applic.expressions.content.RealSet;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class EnRealDomainMustBeValid extends EnAbstractRuleChecker<EnRealType> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final String NAME = "REAL_DOMAIN_MUST_BE_VALID";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Real types must have a valid domain.")
                                                         .appliesTo("Real types"),
                               SEVERITY);

    public EnRealDomainMustBeValid(SnapshotManager manager) {
        super(manager,
              EnRealType.class,
              RULE);
    }

    public static boolean isValid(String domain) {
        try {
            RealSet.of(domain);
            return true;
        } catch (final SyntacticException e) {
            return false;
        }
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnRealType object,
                             Location location) {
        final String domain = object.getDomain();
        if (EnTypeDomainIsMandatory.isDefined(domain)
                && !isValid(domain)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has an invalid domain: '" + domain + "'");

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}