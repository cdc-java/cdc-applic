package cdc.applic.dictionaries.edit;

public interface EnSynonymsItem extends EnElement {
    public EnSynonyms getSynonyms();
}