package cdc.applic.dictionaries.edit.checks;

import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.applic.dictionaries.edit.checks.rules.EnItemNamesAndSynonymsMustBeUnique;
import cdc.applic.dictionaries.edit.checks.rules.EnItemNamesAndSynonymsMustNotBeAmbiguous;
import cdc.applic.dictionaries.edit.checks.rules.EnNamingConventionNamesMustBeUnique;
import cdc.applic.dictionaries.edit.checks.rules.EnRegistryDependencyCyclesAreForbidden;
import cdc.applic.dictionaries.edit.checks.rules.EnRegistryParentsRefsMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnTypeNamesAndSynonymsMustBeUnique;
import cdc.applic.dictionaries.edit.checks.rules.EnTypeNamesAndSynonymsMustNotBeAmbiguous;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.SnapshotManager;

public class EnRegistryChecker extends CompositeChecker<EnRegistry> {
    public EnRegistryChecker(SnapshotManager manager) {
        super(manager,
              EnRegistry.class,
              new EnNamingConventionsChecker(manager),
              new EnNamingConventionNamesMustBeUnique(manager),
              new EnRegistryParentsRefsMustBeValid(manager),
              new EnRegistryDependencyCyclesAreForbidden(manager),
              new EnTypesChecker(manager),
              new EnTypeNamesAndSynonymsMustBeUnique(manager),
              new EnTypeNamesAndSynonymsMustNotBeAmbiguous(manager),
              new EnItemNamesAndSynonymsMustBeUnique(manager),
              new EnItemNamesAndSynonymsMustNotBeAmbiguous(manager),
              new EnAliasesChecker(manager),
              new EnPropertiesChecker(manager));
    }
}