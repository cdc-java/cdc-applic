package cdc.applic.dictionaries.edit;

public final class EnNamingConvention extends EnAbstractOwnedElement<EnRegistry>
        implements EnNameItem {
    public static final String KIND = "NamingConvention";
    private String name;

    protected EnNamingConvention(Builder builder) {
        super(builder);
        this.name = builder.name;
        addToOwner();
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public EnRef<EnNamingConvention> getRef() {
        return getRef(EnNamingConvention.class);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;

        fireSemanticChange(EnNames.NAME);
    }

    static Builder builder(EnRegistry owner) {
        return new Builder(owner);
    }

    public static final class Builder
            extends EnAbstractOwnedElement.Builder<Builder, EnNamingConvention, EnRegistry>
            implements EnNameBuilding<Builder> {
        private String name;

        protected Builder(EnRegistry owner) {
            super(owner);
        }

        @Override
        public Class<EnNamingConvention> getBuiltClass() {
            return EnNamingConvention.class;
        }

        @Override
        public Builder name(String name) {
            this.name = name;
            return self();
        }

        @Override
        public EnNamingConvention build() {
            return wrap(new EnNamingConvention(this));
        }
    }
}