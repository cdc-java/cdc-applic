package cdc.applic.dictionaries.edit;

import java.util.ArrayList;
import java.util.List;

import cdc.util.lang.Checks;

/**
 * Base class of owned elements (they have a owner).
 *
 * @author Damien Carbonne
 *
 * @param <O> The owner type. The concrete owner may derive from it.
 */
public abstract class EnAbstractOwnedElement<O extends EnElement> extends EnAbstractElement implements EnOwnedElement<O> {
    private O owner;

    protected EnAbstractOwnedElement(Builder<?, ?, ? extends O> builder) {
        super(builder.fixId());
        this.owner = Checks.isNotNull(builder.owner, EnNames.OWNER);
    }

    protected final void addToOwner() {
        ((EnAbstractElement) getOwner()).addChild(this);
    }

    private final void removeFromOwner() {
        ((EnAbstractElement) getOwner()).removeChild(this);
    }

    @Override
    public O getOwner() {
        return owner;
    }

    protected <E extends EnAbstractOwnedElement<O>> EnRef<E> getRef(Class<E> elementClass) {
        return EnRef.of(getOwner(), elementClass, getId());
    }

    public abstract EnRef<? extends EnAbstractOwnedElement<O>> getRef();

    /**
     * Recursively removes this element and its children.
     * <p>
     * Children are removed first.
     */
    public void remove() {
        final EnRepository repository = getRepository();
        final List<EnAbstractOwnedElement<?>> todo = new ArrayList<>(getChildren());
        for (final EnAbstractOwnedElement<?> child : todo) {
            child.remove();
        }
        removeFromOwner();
        this.owner = null;
        repository.removeFromRepository(this);
    }

    public boolean isAlive() {
        return this.owner != null;
    }

    public boolean isDead() {
        return this.owner == null;
    }

    @Override
    public int getIndex() {
        return ((EnAbstractElement) owner).getChildIndex(this);
    }

    @Override
    public EnRepository getRepository() {
        return getOwner().getRepository();
    }

    /**
     * Base builder of children elements.
     *
     * @author Damien Carbonne
     *
     * @param <B> The builder type.
     * @param <E> The built element type.
     * @param <O> The concrete parent type.
     */
    public abstract static class Builder<B extends Builder<B, E, O>,
                                         E extends EnAbstractOwnedElement<? super O>,
                                         O extends EnElement>
            extends EnAbstractElement.Builder<B, E> {
        protected final O owner;

        protected Builder(O owner) {
            this.owner = owner;
        }

        protected B fixId() {
            if (!hasId()) {
                id(getRepository().newId(getBuiltClass()));
            }
            return self();
        }

        protected E wrap(E element) {
            return getRepository().addToRepository(element);
        }

        /**
         * @return The owner element.
         */
        public O getOwner() {
            return owner;
        }

        /**
         * @return The root model.
         */
        public EnRepository getRepository() {
            return owner.getRepository();
        }

        protected <X extends EnElement> X getItemWithId(String id,
                                                        Class<X> itemClass) {
            return getRepository().getElementWithId(id, itemClass);
        }

        /**
         * Build the element and returns its parent.
         *
         * @return The concrete parent of the built element.
         */
        public O back() {
            build();
            return owner;
        }
    }
}