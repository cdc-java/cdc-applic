package cdc.applic.dictionaries.edit.checks;

import cdc.applic.dictionaries.edit.EnUserDefinedAssertion;
import cdc.applic.dictionaries.edit.checks.rules.EnAssertionExpressionMustBeSyntacticallyValid;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.SnapshotManager;

public class EnAssertionChecker extends CompositeChecker<EnUserDefinedAssertion> {
    public EnAssertionChecker(SnapshotManager manager) {
        super(manager,
              EnUserDefinedAssertion.class,
              new EnAssertionExpressionMustBeSyntacticallyValid(manager));
    }
}