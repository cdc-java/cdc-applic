package cdc.applic.dictionaries.edit.checks.rules;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import cdc.applic.dictionaries.edit.EnEnumeratedType;
import cdc.applic.dictionaries.edit.EnEnumeratedValue;
import cdc.applic.dictionaries.edit.EnRef;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.graphs.impl.BasicGraphEdge;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;

public class EnEnumeratedTypeValuesRefsMustBeValid extends EnAbstractRefMustBeValid<EnEnumeratedType, EnEnumeratedValue> {
    public static final String NAME = "ENUMERATED_TYPE_VALUES_REFS_MUST_BE_VALID";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("In an enumerated type, referebces values must exist and belong to the type.")
                                                         .appliesTo("Enumerated type"),
                               SEVERITY);

    public EnEnumeratedTypeValuesRefsMustBeValid(SnapshotManager manager) {
        super(manager,
              EnEnumeratedType.class,
              EnEnumeratedValue.class,
              RULE,
              "enumerated values refs");
    }

    @Override
    protected boolean isValid(EnRef<EnEnumeratedValue> ref) {
        if (ref.canResolve()) {
            final EnEnumeratedValue value = ref.resolve();
            final EnEnumeratedType type = ref.getOwner(EnEnumeratedType.class);
            // FIXME O(n) complexity
            return type.getValues().contains(value);
        } else {
            return false;
        }
    }

    @Override
    protected Collection<EnRef<EnEnumeratedValue>> getRefs(EnEnumeratedType object) {
        final Set<EnRef<EnEnumeratedValue>> set = new HashSet<>();
        for (final BasicGraphEdge<EnRef<EnEnumeratedValue>> edge : object.getLessThanEdges()) {
            set.add(edge.getSource());
            set.add(edge.getTarget());
        }
        return set;
    }
}