package cdc.applic.dictionaries.edit.checks.rules;

import cdc.applic.dictionaries.edit.EnDictionary;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.applic.expressions.Expression;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.util.lang.Introspection;

public class EnDictionaryContextExpressionMustBeSyntacticallyValid
        extends EnAbstractExpressionMustBeSyntacticallyValid<EnDictionary<?>> {
    public static final String NAME = "DICTIONARY_CONTEXT_EXPRESSION_MUST_BE_SYNTACTICALLY_VALID";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Context expression of a dictionary must be syntactically valid.")
                                                         .appliesTo("Policies",
                                                                    "Registries"),
                               SEVERITY);

    public EnDictionaryContextExpressionMustBeSyntacticallyValid(SnapshotManager manager) {
        super(manager,
              Introspection.uncheckedCast(EnDictionary.class),
              RULE);
    }

    @Override
    protected Expression getExpression(EnDictionary<?> object) {
        return object.getContextExpression();
    }
}