package cdc.applic.dictionaries.edit.io;

import java.io.IOException;
import java.util.Locale;
import java.util.Objects;

import cdc.applic.dictionaries.edit.EnAbstractElement;
import cdc.applic.dictionaries.edit.EnAlias;
import cdc.applic.dictionaries.edit.EnBooleanType;
import cdc.applic.dictionaries.edit.EnConstraint;
import cdc.applic.dictionaries.edit.EnDescription;
import cdc.applic.dictionaries.edit.EnDescriptionItem;
import cdc.applic.dictionaries.edit.EnDictionary;
import cdc.applic.dictionaries.edit.EnDomainItem;
import cdc.applic.dictionaries.edit.EnElement;
import cdc.applic.dictionaries.edit.EnEnumeratedType;
import cdc.applic.dictionaries.edit.EnEnumeratedValue;
import cdc.applic.dictionaries.edit.EnExpressionItem;
import cdc.applic.dictionaries.edit.EnFrozenItem;
import cdc.applic.dictionaries.edit.EnIntegerType;
import cdc.applic.dictionaries.edit.EnNameItem;
import cdc.applic.dictionaries.edit.EnNamedDItem;
import cdc.applic.dictionaries.edit.EnNamingConvention;
import cdc.applic.dictionaries.edit.EnOrdinalItem;
import cdc.applic.dictionaries.edit.EnPatternType;
import cdc.applic.dictionaries.edit.EnPolicy;
import cdc.applic.dictionaries.edit.EnProperty;
import cdc.applic.dictionaries.edit.EnRealType;
import cdc.applic.dictionaries.edit.EnRef;
import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.applic.dictionaries.edit.EnRepository;
import cdc.applic.dictionaries.edit.EnSynonyms;
import cdc.applic.dictionaries.edit.EnSynonymsItem;
import cdc.applic.dictionaries.edit.EnType;
import cdc.applic.dictionaries.edit.EnUserDefinedAssertion;
import cdc.applic.dictionaries.s1000d.S1000DProductIdentifier;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.graphs.impl.BasicGraphEdge;
import cdc.io.xml.XmlWriter;
import cdc.util.function.IterableUtils;
import cdc.util.lang.CollectionUtils;
import cdc.util.strings.StringUtils;

/**
 * Utilities to save / load a repository to / from an XML stream.
 *
 * @author Damien Carbonne
 */
public final class EnRepositoryXml {
    private static final String ALIAS = "alias";
    private static final String ALIASES = "aliases";
    private static final String ASSERTION = "assertion";
    private static final String ASSERTIONS = "assertions";
    private static final String BOOLEAN_TYPE = "boolean-type";
    private static final String CONSTRAINT = "constraint";
    private static final String CONSTRAINTS = "constraints";
    private static final String CONTEXT_EXPRESSION = "context-expression";
    private static final String DESCRIPTION = "description";
    private static final String DESCRIPTIONS = "descriptions";
    private static final String DOMAIN = "domain";
    private static final String ENUMERATED_TYPE = "enumerated-type";
    private static final String EXPRESSION = "expression";
    private static final String FROZEN = "frozen";
    private static final String ID = "id";
    private static final String IDREF = "idref";
    private static final String INTEGER_TYPE = "integer-type";
    private static final String ITEM = "item";
    private static final String ITEM_USAGE = "item-usage";
    private static final String ITEM_USAGES = "item-usages";
    private static final String LANGUAGE = "lang";
    private static final String LESS_THAN = "less-than";
    private static final String LITERAL = "literal";
    private static final String NAME = "name";
    private static final String NAMING_CONVENTION = "naming-convention";
    private static final String NAMING_CONVENTIONS = "naming-conventions";
    private static final String ORDER = "order";
    private static final String ORDINAL = "ordinal";
    private static final String PARAMS = "params";
    private static final String PARENT = "parent";
    private static final String PARENTS = "parents";
    private static final String PATTERN = "pattern";
    private static final String PATTERN_TYPE = "pattern-type";
    private static final String POLICIES = "policies";
    private static final String POLICY = "policy";
    private static final String PREFIX = "prefix";
    private static final String PROPERTIES = "properties";
    private static final String PROPERTY = "property";
    private static final String REAL_TYPE = "real-type";
    private static final String REGISTRIES = "registries";
    private static final String REGISTRY = "registry";
    private static final String REPOSITORY = "applic-edit-repository";
    private static final String SHORT_LITERAL = "short-literal";
    private static final String SYNONYM = "synonym";
    private static final String SYNONYMS = "synonyms";
    private static final String S1000D_PROPERTY_TYPE = "s1000d-property-type";
    private static final String S1000D_PRODUCT_IDENTIFIER = "s1000d-product-identifier";
    private static final String TYPE = "type";
    private static final String TYPES = "types";
    private static final String TYPE_USAGE = "type-usage";
    private static final String TYPE_USAGES = "type-usages";
    private static final String USAGE = "usage";
    private static final String VALUE = "value";
    private static final String WRITING_RULE = "writing-rule";
    private static final String WRITING_RULES = "writing-rules";

    private EnRepositoryXml() {
    }

    /**
     * Utility class used to write an {@link EnRepository} as an XML stream.
     *
     * @author Damien Carbonne
     */
    public static final class Printer {
        private final XmlWriter writer;
        boolean debug = true;

        private Printer(XmlWriter writer) {
            this.writer = writer;
        }

        public static void write(XmlWriter writer,
                                 EnRepository repository,
                                 boolean debug) throws IOException {
            final Printer printer = new Printer(writer);
            printer.debug = debug;
            printer.write(repository);
        }

        public static void write(XmlWriter writer,
                                 EnRepository repository) throws IOException {
            write(writer, repository, false);
        }

        private void write(EnRepository repository) throws IOException {
            final String namespace = "https://www.gitlab.com/cdc-java";
            final String schema = "https://www.gitlab.com/cdc-java/applic-edit-repository.xsd";
            writer.beginDocument();
            writer.beginElement(REPOSITORY);
            writeId(repository);
            writer.addDefaultNamespace(namespace);
            writer.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            writer.addAttribute("xsi:schemaLocation", namespace + " " + schema);

            writeDescription(repository);
            writeRegistries(repository);

            // TODO bindings

            writer.endElement();
            writer.endDocument();
        }

        private void writeId(EnAbstractElement item) throws IOException {
            writer.addAttribute(ID, item.getId());
        }

        private void writeIdRef(String name,
                                EnRef<?> ref) throws IOException {
            writer.beginElement(name);
            writer.addAttribute(IDREF, ref.getId());
            writer.endElement();
        }

        private void writeExpression(EnExpressionItem item) throws IOException {
            if (item.getExpression() != null) {
                writer.beginElement(EXPRESSION);
                writer.addElementContent(item.getExpression().getContent());
                writer.endElement();
            }
        }

        private void writeName(EnNameItem item) throws IOException {
            if (!StringUtils.isNullOrEmpty(item.getName())) {
                writer.addAttribute(NAME, item.getName());
            }
        }

        private void writeOrdinal(EnOrdinalItem item) throws IOException {
            if (item.getOrdinal() != 0) {
                writer.addAttribute(ORDINAL, item.getOrdinal());
            }
        }

        private void writeFrozen(EnFrozenItem item) throws IOException {
            if (item.isFrozen()) {
                writer.addAttribute(FROZEN, item.isFrozen());
            }
        }

        private void writeDomain(EnDomainItem item) throws IOException {
            if (!StringUtils.isNullOrEmpty(item.getDomain())) {
                writer.addAttribute(DOMAIN, item.getDomain());
            }
        }

        private void writeDescription(EnDescriptionItem item) throws IOException {
            final EnDescription description = item.getDescription();
            if (!description.getLocales().isEmpty()) {
                writer.beginElement(DESCRIPTIONS);
                for (final Locale locale : IterableUtils.toSortedList(description.getLocales(),
                                                                      EnDescription.LOCALE_LANGUAGE_COMPARATOR)) {
                    writer.beginElement(DESCRIPTION);
                    if (locale != null) {
                        writer.addAttribute(LANGUAGE, locale);
                    }
                    if (description.getContent(locale) != null && !description.getContent(locale).isEmpty()) {
                        writer.addElementContent(description.getContent(locale));
                    }
                    writer.endElement();
                }
                writer.endElement();
            }
        }

        private void writeS1000DAttributes(EnType type) throws IOException {
            if (type.getS1000DPropertyType() != S1000DPropertyType.UNDEFINED) {
                writer.addAttribute(S1000D_PROPERTY_TYPE, type.getS1000DPropertyType());
            }
            if (type.getS1000DProductIdentifier() != S1000DProductIdentifier.NONE
                    && type.getS1000DProductIdentifier() != S1000DProductIdentifier.NOT_APPLICABLE) {
                writer.addAttribute(S1000D_PRODUCT_IDENTIFIER, type.getS1000DProductIdentifier());
            }
        }

        private void writeSynonyms(EnSynonymsItem item) throws IOException {
            final EnSynonyms synonyms = item.getSynonyms();

            if (!synonyms.isEmpty()) {
                writer.beginElement(SYNONYMS);
                for (final EnRef<EnNamingConvention> conventionRef : IterableUtils.toSortedList(synonyms.getNamingConventionRefs(),
                                                                                                EnRef.ID_COMPARATOR)) {
                    final String synonym = synonyms.getSynonym(conventionRef);
                    writer.beginElement(SYNONYM);
                    writer.addAttribute(NAME, synonym);
                    writeIdRef(NAMING_CONVENTION, conventionRef);
                    writer.endElement();
                }
                writer.endElement();
            }
        }

        private void writeTypes(EnRegistry registry) throws IOException {
            if (!registry.getTypes().isEmpty()) {
                writer.beginElement(TYPES);
                for (final EnType type : CollectionUtils.toSortedList(registry.getTypes(),
                                                                      EnElement.ID_COMPARATOR)) {
                    writeType(type);
                }
                writer.endElement();
            }
        }

        private void writeType(EnType type) throws IOException {
            if (type instanceof EnBooleanType) {
                writer.beginElement(BOOLEAN_TYPE);
                writeId(type);
                writeName(type);
                writeS1000DAttributes(type);
                writeDescription(type);
                writeSynonyms(type);
                writer.endElement();
            } else if (type instanceof EnIntegerType) {
                final EnIntegerType t = (EnIntegerType) type;
                writer.beginElement(INTEGER_TYPE);
                writeId(type);
                writeName(type);
                writeDomain(t);
                writeS1000DAttributes(type);
                writeFrozen((EnFrozenItem) type);
                writeDescription(type);
                writeSynonyms(type);
                writer.endElement();
            } else if (type instanceof EnRealType) {
                final EnRealType t = (EnRealType) type;
                writer.beginElement(REAL_TYPE);
                writeId(type);
                writeName(type);
                writeDomain(t);
                writeS1000DAttributes(type);
                writeFrozen((EnFrozenItem) type);
                writeDescription(type);
                writeSynonyms(type);
                writer.endElement();
            } else if (type instanceof EnEnumeratedType) {
                final EnEnumeratedType t = (EnEnumeratedType) type;
                writer.beginElement(ENUMERATED_TYPE);
                writeId(type);
                writeName(type);
                writeS1000DAttributes(type);
                writeFrozen((EnFrozenItem) type);
                writeDescription(type);
                writeSynonyms(type);
                for (final EnEnumeratedValue value : t.getValues()) {
                    writer.beginElement(VALUE);
                    writeId(value);
                    if (!StringUtils.isNullOrEmpty(value.getLiteral())) {
                        writer.addAttribute(LITERAL, value.getLiteral());
                    }
                    if (!StringUtils.isNullOrEmpty(value.getShortLiteral())
                            && !Objects.equals(value.getLiteral(), value.getShortLiteral())) {
                        writer.addAttribute(SHORT_LITERAL, value.getShortLiteral());
                    }
                    writeOrdinal(value);
                    writeDescription(value);
                    writeSynonyms(value);
                    writer.endElement();
                }
                if (!t.getLessThanEdges().isEmpty()) {
                    writer.beginElement(ORDER);
                    for (final BasicGraphEdge<EnRef<EnEnumeratedValue>> edge : IterableUtils.toSortedList(t.getLessThanEdges(),
                                                                                                          EnEnumeratedType.SOURCE_TARGET_COMPARATOR)) {
                        writer.beginElement(LESS_THAN);
                        writeIdRef(VALUE, edge.getSource());
                        writeIdRef(VALUE, edge.getTarget());
                        writer.endElement();
                    }
                    writer.endElement();
                }
                writer.endElement();
            } else {
                final EnPatternType t = (EnPatternType) type;
                writer.beginElement(PATTERN_TYPE);
                writeId(type);
                writeName(type);
                writer.addAttribute(PATTERN, t.getPattern());
                writeS1000DAttributes(type);
                writeFrozen((EnFrozenItem) type);
                writeDescription(type);
                writeSynonyms(type);
                writer.endElement();
            }
        }

        private void writeRegistries(EnRepository repository) throws IOException {
            writer.beginElement(REGISTRIES);
            for (final EnRegistry registry : repository.getRegistries()) {
                writeRegistry(registry);
            }
            writer.endElement();
        }

        private void writeRegistry(EnRegistry registry) throws IOException {
            writer.beginElement(REGISTRY);
            writeId(registry);
            if (!StringUtils.isNullOrEmpty(registry.getName())) {
                writer.addAttribute(NAME, registry.getName());
            }
            if (registry.getPrefix() != null) {
                writer.addAttribute(PREFIX, registry.getPrefix());
            }
            writeDescription(registry);
            writeParents(registry);
            writeContext(registry);
            writeNamingConventions(registry);
            writeTypes(registry);
            writeProperties(registry);
            writeAliases(registry);
            writeTypeUsages(registry);
            writeItemUsages(registry);
            writeConstraints(registry);
            writeAssertions(registry);
            writeWritingRuleNames(registry);
            writePolicies(registry);

            writer.endElement();
        }

        private void writeContext(EnDictionary<?> dictionary) throws IOException {
            if (!dictionary.getContextExpression().isValid() || !dictionary.getContextExpression().isTrue()) {
                writer.beginElement(CONTEXT_EXPRESSION);
                writer.addElementContent(dictionary.getContextExpression().getContent());
                writer.endElement();
            }
        }

        private void writeNamingConventions(EnRegistry registry) throws IOException {
            if (!registry.getNamingConventions().isEmpty()) {
                writer.beginElement(NAMING_CONVENTIONS);
                for (final EnNamingConvention convention : IterableUtils.toSortedList(registry.getNamingConventions(),
                                                                                      EnElement.ID_COMPARATOR)) {
                    writer.beginElement(NAMING_CONVENTION);
                    writeId(convention);
                    writeName(convention);
                    writeDescription(convention);
                    writer.endElement();
                }
                writer.endElement();
            }
        }

        private void writeParents(EnRegistry registry) throws IOException {
            if (!registry.getParentRefs().isEmpty()) {
                writer.beginElement(PARENTS);
                for (final EnRef<EnDictionary<?>> parentRef : registry.getParentRefs()) {
                    writeIdRef(PARENT, parentRef);
                }
                writer.endElement();
            }
        }

        private void writeProperties(EnRegistry registry) throws IOException {
            if (!registry.getProperties().isEmpty()) {
                writer.beginElement(PROPERTIES);
                for (final EnProperty property : IterableUtils.toSortedList(registry.getProperties(),
                                                                            EnElement.ID_COMPARATOR)) {
                    writeProperty(property/* , registry.getPrefix() */);
                }
                writer.endElement();
            }
        }

        private void writeProperty(EnProperty property) throws IOException {
            writer.beginElement(PROPERTY);
            writeId(property);
            writeName(property);
            writer.addAttribute(ORDINAL, property.getOrdinal());
            writeDescription(property);
            writeSynonyms(property);
            writeIdRef(TYPE, property.getTypeRef());
            writer.endElement();
        }

        private void writeAliases(EnRegistry registry) throws IOException {
            if (!registry.getAliases().isEmpty()) {
                writer.beginElement(ALIASES);
                for (final EnAlias alias : IterableUtils.toSortedList(registry.getAliases(),
                                                                      EnElement.ID_COMPARATOR)) {
                    writeAlias(alias);
                }
                writer.endElement();
            }
        }

        private void writeAlias(EnAlias alias) throws IOException {
            writer.beginElement(ALIAS);
            writeId(alias);
            writeName(alias);
            writeOrdinal(alias);
            writeDescription(alias);
            writeSynonyms(alias);
            writeExpression(alias);
            writer.endElement();
        }

        private void writeConstraints(EnDictionary<?> dictionary) throws IOException {
            if (!dictionary.getConstraints().isEmpty()) {
                writer.beginElement(CONSTRAINTS);
                for (final EnConstraint constraint : IterableUtils.toSortedList(dictionary.getConstraints(),
                                                                                EnElement.ID_COMPARATOR)) {
                    writeConstraint(constraint);
                }
                writer.endElement();
            }
        }

        private void writeConstraint(EnConstraint constraint) throws IOException {
            writer.beginElement(CONSTRAINT);
            writer.addAttribute(ID, constraint.getId());
            if (!StringUtils.isNullOrEmpty(constraint.getTypeName())) {
                writer.addAttribute(TYPE, constraint.getTypeName());
            }
            writeDescription(constraint);
            writer.addElementIfNonEmpty(PARAMS, constraint.getParams());
            writer.endElement();
        }

        private void writeAssertions(EnDictionary<?> dictionary) throws IOException {
            if (!dictionary.getUserDefinedAssertions().isEmpty()) {
                writer.beginElement(ASSERTIONS);
                for (final EnUserDefinedAssertion assertion : CollectionUtils.toSortedList(dictionary.getUserDefinedAssertions(),
                                                                                           EnElement.ID_COMPARATOR)) {
                    writeAssertion(assertion);
                }
                writer.endElement();
            }
        }

        private void writeAssertion(EnUserDefinedAssertion assertion) throws IOException {
            writer.beginElement(ASSERTION);
            writeId(assertion);
            writeDescription(assertion);
            writeExpression(assertion);
            writer.endElement();
        }

        private void writePolicies(EnDictionary<?> dictionary) throws IOException {
            if (!dictionary.getPolicies().isEmpty()) {
                writer.beginElement(POLICIES);
                for (final EnPolicy policy : dictionary.getPolicies()) {
                    writePolicy(policy);
                }
                writer.endElement();
            }
        }

        private void writePolicy(EnPolicy policy) throws IOException {
            writer.beginElement(POLICY);
            writeId(policy);
            if (!StringUtils.isNullOrEmpty(policy.getName())) {
                writer.addAttribute(NAME, policy.getName());
            }

            writeDescription(policy);
            writeContext(policy);
            writeTypeUsages(policy);
            writeItemUsages(policy);
            writeConstraints(policy);
            writeAssertions(policy);
            writeWritingRuleNames(policy);
            writePolicies(policy);

            writer.endElement();
        }

        private void writeTypeUsages(EnDictionary<?> dictionary) throws IOException {
            if (!dictionary.getTypeToUsageKeys().isEmpty()) {
                writer.beginElement(TYPE_USAGES);
                for (final EnRef<EnType> typeRef : IterableUtils.toSortedList(dictionary.getTypeToUsageKeys(),
                                                                              EnRef.ID_COMPARATOR)) {
                    writeTypeUsage(dictionary, typeRef);
                }
                writer.endElement();
            }
        }

        private void writeTypeUsage(EnDictionary<?> dictionary,
                                    EnRef<EnType> typeRef) throws IOException {
            writer.beginElement(TYPE_USAGE);
            writer.addAttribute(USAGE, dictionary.getTypeUsage(typeRef));
            writeIdRef(TYPE, typeRef);
            writer.endElement();
        }

        private void writeItemUsages(EnDictionary<?> dictionary) throws IOException {
            if (!dictionary.getItemToUsageKeys().isEmpty()) {
                writer.beginElement(ITEM_USAGES);
                for (final EnRef<EnNamedDItem> itemRef : IterableUtils.toSortedList(dictionary.getItemToUsageKeys(),
                                                                                    EnRef.ID_COMPARATOR)) {
                    writeItemUsage(dictionary, itemRef);
                }
                writer.endElement();
            }
        }

        private void writeItemUsage(EnDictionary<?> dictionary,
                                    EnRef<EnNamedDItem> itemRef) throws IOException {
            writer.beginElement(ITEM_USAGE);
            writer.addAttribute(USAGE, dictionary.getItemUsage(itemRef));
            writeIdRef(ITEM, itemRef);
            writer.endElement();
        }

        private void writeWritingRuleNames(EnDictionary<?> dictionary) throws IOException {
            if (!dictionary.getEnabledWritingRules().isEmpty()) {
                writer.beginElement(WRITING_RULES);
                for (final String name : IterableUtils.toSortedList(dictionary.getEnabledWritingRules())) {
                    writer.beginElement(WRITING_RULE);
                    writer.addAttribute(NAME, name);
                    writer.endElement();
                }
                writer.endElement();
            }
        }
    }
}