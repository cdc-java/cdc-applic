package cdc.applic.dictionaries.edit;

public interface EnOwnedElement<O extends EnElement> extends EnElement {
    @Override
    public O getOwner();
}