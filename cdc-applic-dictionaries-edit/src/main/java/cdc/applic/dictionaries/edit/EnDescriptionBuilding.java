package cdc.applic.dictionaries.edit;

import java.util.Locale;

public interface EnDescriptionBuilding<B> {
    public B description(Locale locale,
                         String content);

    public default B description(String locale,
                                 String content) {
        return description(locale == null ? null : Locale.forLanguageTag(locale),
                           content);
    }

    public default B description(String content) {
        return description((Locale) null,
                           content);
    }
}