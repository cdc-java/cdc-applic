package cdc.applic.dictionaries.edit;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import cdc.applic.expressions.literals.SName;
import cdc.util.strings.StringUtils;

public final class EnQName {
    private final SName prefix;
    private final String name;

    private EnQName(SName prefix,
                    String name) {
        this.prefix = prefix;
        this.name = name;
    }

    public static EnQName of(SName prefix,
                             String name) {
        return new EnQName(prefix,
                           name);
    }

    public SName getPrefix() {
        return prefix;
    }

    public String getName() {
        return name;
    }

    public boolean isValid() {
        return !StringUtils.isNullOrEmpty(name);
    }

    public EnQName removePrefix() {
        return of(null, name);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof EnQName)) {
            return false;
        }
        final EnQName o = (EnQName) other;
        return Objects.equals(prefix, o.prefix)
                && Objects.equals(name, o.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(prefix,
                            name);
    }

    @Override
    public String toString() {
        return "[" + Objects.toString(prefix) + ", "
                + Objects.toString(name) + "]";
    }

    public static List<EnQName> of(SName prefix,
                                   List<String> names) {
        return names.stream()
                    .map(n -> EnQName.of(prefix, n))
                    .toList();
    }

    public static Set<EnQName> of(SName prefix,
                                  Set<String> names) {
        return names.stream()
                    .map(n -> EnQName.of(prefix, n))
                    .collect(Collectors.toSet());
    }

    public static List<EnQName> removePrefix(List<EnQName> qnames) {
        return qnames.stream()
                     .map(EnQName::removePrefix)
                     .toList();
    }

    public static Set<EnQName> removePrefix(Set<EnQName> qnames) {
        return qnames.stream()
                     .map(EnQName::removePrefix)
                     .collect(Collectors.toSet());
    }
}