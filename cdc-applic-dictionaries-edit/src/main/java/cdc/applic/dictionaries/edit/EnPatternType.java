package cdc.applic.dictionaries.edit;

public final class EnPatternType extends EnType
        implements EnFrozenItem {
    public static final String KIND = "PatternType";
    private String pattern;
    private boolean frozen;

    protected EnPatternType(Builder builder) {
        super(builder);
        this.pattern = builder.pattern;
        this.frozen = builder.frozen;
        addToOwner();
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public EnRef<EnPatternType> getRef() {
        return getRef(EnPatternType.class);
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;

        fireSemanticChange(EnNames.PATTERN);
    }

    @Override
    public boolean isFrozen() {
        return frozen;
    }

    @Override
    public void setFrozen(boolean frozen) {
        this.frozen = frozen;

        fireSemanticChange(EnNames.FROZEN);
    }

    static Builder builder(EnRegistry owner) {
        return new Builder(owner);
    }

    public static final class Builder
            extends EnType.Builder<Builder, EnPatternType>
            implements EnFrozenBuilding<Builder> {
        private String pattern;
        private boolean frozen = false;

        protected Builder(EnRegistry owner) {
            super(owner);
        }

        @Override
        public Class<EnPatternType> getBuiltClass() {
            return EnPatternType.class;
        }

        public Builder pattern(String pattern) {
            this.pattern = pattern;
            return self();
        }

        @Override
        public Builder frozen(boolean frozen) {
            this.frozen = frozen;
            return self();
        }

        @Override
        public EnPatternType build() {
            return wrap(new EnPatternType(this));
        }
    }
}