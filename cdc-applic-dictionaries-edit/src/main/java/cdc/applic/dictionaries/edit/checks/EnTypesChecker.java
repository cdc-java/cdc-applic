package cdc.applic.dictionaries.edit.checks;

import java.util.List;
import java.util.stream.Collectors;

import cdc.applic.dictionaries.edit.EnLocation;
import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.applic.dictionaries.edit.EnType;
import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;

public class EnTypesChecker extends AbstractPartsChecker<EnRegistry, EnType, EnTypesChecker> {
    public static final String KEY = "TypesChecker";

    public EnTypesChecker(SnapshotManager manager) {
        super(manager,
              EnRegistry.class,
              EnType.class,
              new EnTypeChecker(manager));
        manager.register(KEY, this);
    }

    @Override
    protected List<LocatedObject<? extends EnType>> getParts(EnRegistry object) {
        final List<? extends EnType> parts = object.getTypes();
        return parts.stream()
                    .map(x -> LocatedObject.of(x, EnLocation.builder().element(x).build()))
                    .collect(Collectors.toList());
    }
}