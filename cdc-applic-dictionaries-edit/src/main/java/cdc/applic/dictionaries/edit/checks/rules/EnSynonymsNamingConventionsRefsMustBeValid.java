package cdc.applic.dictionaries.edit.checks.rules;

import java.util.Set;

import cdc.applic.dictionaries.edit.EnNamingConvention;
import cdc.applic.dictionaries.edit.EnRef;
import cdc.applic.dictionaries.edit.EnSynonymsItem;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;

public class EnSynonymsNamingConventionsRefsMustBeValid extends EnAbstractRefMustBeValid<EnSynonymsItem, EnNamingConvention> {
    public static final String NAME = "SYNONYMS_NAMING_CONVENTIONS_REFS_MUST_BE_VALID";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Referenced naming conventions of synonyms must exist and be usable.")
                                                         .appliesTo("Aliases",
                                                                    "Enumerated values",
                                                                    "Properties",
                                                                    "Types"),
                               SEVERITY);

    public EnSynonymsNamingConventionsRefsMustBeValid(SnapshotManager manager) {
        super(manager,
              EnSynonymsItem.class,
              EnNamingConvention.class,
              RULE,
              "naming convention refs");
    }

    @Override
    protected Set<EnRef<EnNamingConvention>> getRefs(EnSynonymsItem object) {
        return object.getSynonyms().getNamingConventionRefs();
    }
}