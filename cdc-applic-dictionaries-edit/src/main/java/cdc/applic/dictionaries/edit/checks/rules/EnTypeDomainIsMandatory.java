package cdc.applic.dictionaries.edit.checks.rules;

import cdc.applic.dictionaries.edit.EnDomainItem;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.util.strings.StringUtils;

public class EnTypeDomainIsMandatory extends EnAbstractRuleChecker<EnDomainItem> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final String NAME = "TYPE_DOMAIN_IS_MANDATORY";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Domained types must have a domain.")
                                                         .appliesTo("Integer types",
                                                                    "Real types"),
                               SEVERITY);

    public EnTypeDomainIsMandatory(SnapshotManager manager) {
        super(manager,
              EnDomainItem.class,
              RULE);
    }

    public static boolean isDefined(String domain) {
        return !StringUtils.isNullOrEmpty(domain);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnDomainItem object,
                             Location location) {
        final String domain = object.getDomain();
        if (!isDefined(domain)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has no domain");

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}