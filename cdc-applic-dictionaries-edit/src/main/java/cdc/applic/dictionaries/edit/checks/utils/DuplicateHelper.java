package cdc.applic.dictionaries.edit.checks.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class DuplicateHelper<E, K> {
    private final Function<E, Set<K>> keysExtractor;
    private final Predicate<K> keyValidator;

    private DuplicateHelper(Function<E, Set<K>> keysExtractor,
                            Predicate<K> keyValidator) {
        this.keysExtractor = keysExtractor;
        this.keyValidator = keyValidator;
    }

    public static <E, K> DuplicateHelper<E, K> ofN(Function<E, Set<K>> keysExtractor,
                                                   Predicate<K> keyValidator) {
        return new DuplicateHelper<>(keysExtractor,
                                     keyValidator);
    }

    public static <E, K> DuplicateHelper<E, K> of1(Function<E, K> keyExtractor,
                                                   Predicate<K> keyValidator) {
        final Function<E, Set<K>> keysExtractor =
                e -> {
                    final K k = keyExtractor.apply(e);
                    return keyValidator.test(k) ? Set.of(k) : Set.of();
                };
        return new DuplicateHelper<>(keysExtractor,
                                     keyValidator);
    }

    public boolean hasDuplicates(List<? extends E> elements) {
        final List<K> list = elements.stream()
                                     .flatMap(e -> keysExtractor.apply(e).stream())
                                     .filter(keyValidator)
                                     .toList();
        final Set<K> set = new HashSet<>(list);
        return list.size() != set.size();
    }

    private Map<K, List<E>> counts(List<? extends E> elements) {
        final Map<K, List<E>> map = new HashMap<>();
        for (final E element : elements) {
            final Set<K> keys = keysExtractor.apply(element);
            for (final K key : keys) {
                if (keyValidator.test(key)) {
                    final List<E> list = map.computeIfAbsent(key, k -> new ArrayList<>());
                    list.add(element);
                }
            }
        }
        return map;
    }

    public String explain(List<? extends E> elements) {
        final Map<K, List<E>> counts = counts(elements);
        return counts.entrySet()
                     .stream()
                     .filter(e -> e.getValue().size() > 1)
                     .map(e -> e.getKey() + " (x" + e.getValue().size() + ")")
                     .sorted()
                     .collect(Collectors.joining(", "));
    }
}