package cdc.applic.dictionaries.edit;

public interface EnFrozenBuilding<B> {
    public B frozen(boolean frozen);
}