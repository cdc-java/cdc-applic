package cdc.applic.dictionaries.edit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public interface EnElement {
    public static final Comparator<EnElement> ID_COMPARATOR =
            Comparator.comparing(EnElement::getId);

    public String getId();

    public String getKind();

    /**
     * @return The owner element of this element, possibly {@code null}.
     */
    public EnElement getOwner();

    public default boolean hasOwner() {
        return getOwner() != null;
    }

    /**
     * @return The depth (1 for root element) of the element.
     */
    public default int getDepth() {
        int depth = 0;
        EnElement index = this;
        while (index != null) {
            index = index.getOwner();
            depth++;
        }
        return depth;
    }

    /**
     * @return A list of ancestors.
     */
    public default List<EnElement> getHierarchy() {
        final List<EnElement> list = new ArrayList<>();
        EnElement iter = this;
        while (iter != null) {
            list.add(iter);
            iter = iter.getOwner();
        }
        Collections.reverse(list);
        return list;
    }

    /**
     * @return The index of this element among the children of its parent.
     *         {@code -1} if this element has no parent.
     */
    public int getIndex();

    public default List<Integer> getIndices() {
        final List<Integer> list = new ArrayList<>();
        EnElement iter = this;
        while (iter != null && iter.getIndex() >= 0) {
            list.add(iter.getIndex());
            iter = iter.getOwner();
        }
        Collections.reverse(list);
        return list;
    }

    /**
     * @param <O> The owner type.
     * @param ownerClass The owner class.
     * @return The owner element of this element,
     *         possibly {@code null} or converted to {@code ownerClass}.
     * @throws ClassCastException When the owner can not be converted to {@code ownerClass}.
     */
    public default <O extends EnElement> O getOwner(Class<O> ownerClass) {
        return ownerClass.cast(getOwner());
    }

    /**
     * @return All children of this element.
     */
    public List<? extends EnElement> getChildren();

    /**
     * @param <C> The child type.
     * @param childClass The child class.
     * @return All children elements that are instances of {@code childClass}.
     */
    public default <C extends EnElement> List<C> getChildren(Class<C> childClass) {
        return getChildren().stream()
                            .filter(childClass::isInstance)
                            .map(childClass::cast)
                            .toList();
    }

    /**
     * @param <C> The child type.
     * @param childClass The child class.
     * @param predicate The predicate.
     * @return All children elements that are instances of {@code childClass} and match {@code predicate}.
     */
    public default <C extends EnElement> List<C> getChildren(Class<C> childClass,
                                                             Predicate<? super C> predicate) {
        return getChildren().stream()
                            .filter(childClass::isInstance)
                            .map(childClass::cast)
                            .filter(predicate)
                            .toList();
    }

    /**
     * @param <C> The child type.
     * @param childClass The child class.
     * @return {@code true} if this element has {@code childClass} children.
     */
    public default <C extends EnElement> boolean hasChildren(Class<C> childClass) {
        return getChildren().stream()
                            .anyMatch(childClass::isInstance);
    }

    /**
     * @return The {@link EnRepository} of this element.
     */
    public EnRepository getRepository();

    /**
     * Traverses all elements and invokes a consumer on elements that match a class and predicate.
     *
     * @param <X> The element type.
     * @param cls The element class.
     * @param consumer The element consumer.
     * @param predicate The element predicate.
     */
    public default <X extends EnElement> void traverse(Class<X> cls,
                                                       Consumer<? super X> consumer,
                                                       Predicate<? super X> predicate) {
        if (cls.isInstance(this)) {
            final X x = cls.cast(this);
            if (predicate.test(x)) {
                consumer.accept(x);
            }
        }
        for (final EnElement child : getChildren()) {
            child.traverse(cls, consumer, predicate);
        }
    }

    /**
     * Traverses all elements and invokes a consumer on elements that match a class.
     *
     * @param <X> The element type.
     * @param cls The element class.
     * @param consumer The element consumer.
     */
    public default <X extends EnElement> void traverse(Class<X> cls,
                                                       Consumer<? super X> consumer) {
        traverse(cls, consumer, x -> true);
    }

    /**
     * Traverses all elements and invokes a consumer on each traversed element.
     *
     * @param consumer The element consumer.
     */
    public default void traverse(Consumer<? super EnElement> consumer) {
        traverse(EnElement.class, consumer);
        consumer.accept(this);
    }

    /**
     * @param <X> The element type.
     * @param cls The element class.
     * @param predicate The element predicate.
     * @return A List of all elements that are instances of {@code cls} and match {@code predicate}.
     */
    public default <X extends EnElement> List<X> collect(Class<X> cls,
                                                         Predicate<? super X> predicate) {
        final List<X> list = new ArrayList<>();
        traverse(cls, list::add, predicate);
        return list;
    }

    /**
     * @param <X> The element type.
     * @param cls The element class.
     * @return A List of all elements that are instances of {@code cls}.
     */
    public default <X extends EnElement> List<X> collect(Class<X> cls) {
        return collect(cls, x -> true);
    }

    public static interface Builder<B extends Builder<B>> {
        public Class<? extends EnElement> getBuiltClass();

        public B self();

        public EnElement build();
    }
}