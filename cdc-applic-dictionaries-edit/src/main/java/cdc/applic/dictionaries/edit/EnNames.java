package cdc.applic.dictionaries.edit;

public final class EnNames {
    private EnNames() {
    }

    public static final String CHILD = "child";
    public static final String CONTEXT = "context";
    public static final String DESCRIPTION = "description";
    public static final String DOMAIN = "domain";
    public static final String EXPRESSION = "expression";
    public static final String FROZEN = "frozen";
    public static final String ID = "id";
    public static final String ITEM = "item";
    public static final String ITEM_ID = "itemId";
    public static final String ITEM_USAGE = "itemUsage";
    public static final String LESS_THAN = "lessThan";
    public static final String LITERAL = "literal";
    public static final String LOCALE = "locale";
    public static final String NAME = "name";
    public static final String ORDINAL = "ordinal";
    public static final String OWNER = "owner";
    public static final String PARAMS = "params";
    public static final String PARENTS = "parents";
    public static final String PATTERN = "pattern";
    public static final String PREFIX = "prefix";
    public static final String REF_CLASS = "refClass";
    public static final String SHORT_LITERAL = "shortLiteral";
    public static final String SUPPLIER = "supplier";
    public static final String SYNONYMS = "synonyms";
    public static final String S1000D_PROPERTY_TYPE = "s1000dPropertyType";
    public static final String S1000D_PRODUCT_IDENTIFIER = "s1000dProductIdentifier";
    public static final String TYPE = "type";
    public static final String TYPE_ID = "typeId";
    public static final String TYPE_NAME = "typeName";
    public static final String TYPE_USAGE = "typeUsage";
    public static final String WRITING_RULE = "writingRule";
}