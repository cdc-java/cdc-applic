package cdc.applic.dictionaries.edit;

import java.util.HashMap;
import java.util.Map;

import cdc.applic.expressions.Expression;
import cdc.util.lang.Checks;

public final class EnAlias extends EnAbstractOwnedElement<EnRegistry>
        implements EnNamedDItem, EnExpressionItem {
    public static final String KIND = "Alias";
    private String name;
    private final EnSynonyms synonyms;
    private int ordinal;
    private Expression expression;

    protected EnAlias(Builder builder) {
        super(builder);
        this.name = builder.name;
        this.synonyms = new EnSynonyms(this, builder.synonyms);
        this.ordinal = builder.ordinal;
        this.expression = builder.expression;
        addToOwner();
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public EnRef<EnAlias> getRef() {
        return getRef(EnAlias.class);
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public final void setName(String name) {
        this.name = name;

        fireSemanticChange(EnNames.NAME);
    }

    @Override
    public final EnSynonyms getSynonyms() {
        return synonyms;
    }

    @Override
    public int getOrdinal() {
        return ordinal;
    }

    @Override
    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;

        fireSemanticChange(EnNames.ORDINAL);
    }

    @Override
    public Expression getExpression() {
        return expression;
    }

    @Override
    public void setExpression(Expression expression) {
        this.expression = Checks.isNotNull(expression, EnNames.EXPRESSION);

        fireSemanticChange(EnNames.EXPRESSION);
    }

    static Builder builder(EnRegistry owner) {
        return new Builder(owner);
    }

    public static final class Builder
            extends EnAbstractOwnedElement.Builder<Builder, EnAlias, EnRegistry>
            implements EnNameBuilding<Builder>, EnSynonymsBuilding<Builder>, EnOrdinalBuilding<Builder>,
            EnExpressionBuilding<Builder> {
        private String name;
        private final Map<String, String> synonyms = new HashMap<>();
        private int ordinal = 0;
        private Expression expression = Expression.TRUE;

        protected Builder(EnRegistry owner) {
            super(owner);
        }

        @Override
        public Class<EnAlias> getBuiltClass() {
            return EnAlias.class;
        }

        @Override
        public Builder name(String name) {
            this.name = name;
            return self();
        }

        @Override
        public Builder synonym(String conventionId,
                               String synonym) {
            this.synonyms.put(conventionId, synonym);
            return self();
        }

        @Override
        public Builder ordinal(int ordinal) {
            this.ordinal = ordinal;
            return self();
        }

        @Override
        public Builder expression(Expression expression) {
            Checks.isNotNull(expression, EnNames.EXPRESSION);
            this.expression = expression;
            return self();
        }

        @Override
        public EnAlias build() {
            return wrap(new EnAlias(this));
        }
    }
}