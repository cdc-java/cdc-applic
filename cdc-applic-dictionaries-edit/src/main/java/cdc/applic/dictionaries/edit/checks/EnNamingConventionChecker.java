package cdc.applic.dictionaries.edit.checks;

import cdc.applic.dictionaries.edit.EnNamingConvention;
import cdc.applic.dictionaries.edit.checks.rules.EnNameIsMandatory;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.SnapshotManager;

public class EnNamingConventionChecker extends CompositeChecker<EnNamingConvention> {
    public EnNamingConventionChecker(SnapshotManager manager) {
        super(manager,
              EnNamingConvention.class,
              new EnNameIsMandatory(manager));
    }
}