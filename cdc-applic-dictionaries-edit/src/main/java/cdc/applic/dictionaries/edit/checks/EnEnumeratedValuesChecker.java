package cdc.applic.dictionaries.edit.checks;

import java.util.List;
import java.util.stream.Collectors;

import cdc.applic.dictionaries.edit.EnEnumeratedType;
import cdc.applic.dictionaries.edit.EnEnumeratedValue;
import cdc.applic.dictionaries.edit.EnLocation;
import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;

public class EnEnumeratedValuesChecker
        extends AbstractPartsChecker<EnEnumeratedType, EnEnumeratedValue, EnEnumeratedValuesChecker> {

    public EnEnumeratedValuesChecker(SnapshotManager manager) {
        super(manager,
              EnEnumeratedType.class,
              EnEnumeratedValue.class,
              new EnEnumeratedValueChecker(manager));
    }

    @Override
    protected List<LocatedObject<? extends EnEnumeratedValue>> getParts(EnEnumeratedType object) {
        final List<EnEnumeratedValue> parts = object.getValues();
        return parts.stream()
                    .map(x -> LocatedObject.of(x, EnLocation.builder().element(x).build()))
                    .collect(Collectors.toList());
    }
}