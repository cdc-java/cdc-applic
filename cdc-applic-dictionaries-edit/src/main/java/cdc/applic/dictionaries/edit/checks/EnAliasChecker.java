package cdc.applic.dictionaries.edit.checks;

import cdc.applic.dictionaries.edit.EnAlias;
import cdc.applic.dictionaries.edit.checks.rules.EnAliasExpressionMustBeSyntacticallyValid;
import cdc.applic.dictionaries.edit.checks.rules.EnNameIsMandatory;
import cdc.applic.dictionaries.edit.checks.rules.EnSynonymsMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnSynonymsNamingConventionsRefsMustBeValid;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.SnapshotManager;

public class EnAliasChecker extends CompositeChecker<EnAlias> {
    public EnAliasChecker(SnapshotManager manager) {
        super(manager,
              EnAlias.class,
              new EnNameIsMandatory(manager),
              new EnSynonymsMustBeValid(manager),
              new EnSynonymsNamingConventionsRefsMustBeValid(manager),
              new EnAliasExpressionMustBeSyntacticallyValid(manager));
    }
}