package cdc.applic.dictionaries.edit;

import cdc.applic.expressions.Expression;

public interface EnExpressionItem extends EnElement {
    public Expression getExpression();

    public void setExpression(Expression expression);

    public default void setExpression(String expression) {
        setExpression(Expression.of(expression, false));
    }
}