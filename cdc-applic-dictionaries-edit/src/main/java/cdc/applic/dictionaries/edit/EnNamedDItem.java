package cdc.applic.dictionaries.edit;

import java.util.Set;

public interface EnNamedDItem extends EnOwnedElement<EnRegistry>, EnNameItem, EnSynonymsItem, EnOrdinalItem {
    public default Set<String> getNameAndSynonyms() {
        return getSynonyms().getRefAndSynonyms(getName());
    }

    public default Set<EnQName> getQNameAndQSynonyms() {
        return EnQName.of(getOwner().getPrefix(), getNameAndSynonyms());
    }
}