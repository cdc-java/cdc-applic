package cdc.applic.dictionaries.edit.checks.rules;

import java.util.List;

import cdc.applic.dictionaries.edit.EnType;
import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.applic.dictionaries.edit.checks.utils.DuplicateHelper;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class EnTypeNamesAndSynonymsMustBeUnique extends EnAbstractRuleChecker<EnRegistry> {
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    public static final String NAME = "TYPE_NAMES_AND_SYNONYMS_MUST_BE_UNIQUE";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Names and synonyms of types in a registry must be unique."
                                       + "\nIn the context of a registry, type names and synonyms can not designate several types.")
                                                         .appliesTo("Registries"),
                               SEVERITY);

    private static final DuplicateHelper<EnType, String> HELPER =
            DuplicateHelper.ofN(EnType::getNameAndSynonyms,
                                EnNameIsMandatory::isDefined);

    public EnTypeNamesAndSynonymsMustBeUnique(SnapshotManager manager) {
        super(manager,
              EnRegistry.class,
              RULE);
    }

    public static boolean isCompliant(List<? extends EnType> types) {
        return !HELPER.hasDuplicates(types);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnRegistry object,
                             Location location) {
        final List<? extends EnType> types = object.getTypes();
        if (HELPER.hasDuplicates(types)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has duplicate type names or synonyms: " + HELPER.explain(types));

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}