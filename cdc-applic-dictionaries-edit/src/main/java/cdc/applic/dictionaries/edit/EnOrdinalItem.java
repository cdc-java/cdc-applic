package cdc.applic.dictionaries.edit;

public interface EnOrdinalItem extends EnElement {
    public int getOrdinal();

    public void setOrdinal(int ordinal);
}