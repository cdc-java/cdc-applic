package cdc.applic.dictionaries.edit.checks.rules;

import cdc.applic.dictionaries.edit.EnType;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.applic.dictionaries.s1000d.S1000DProductIdentifier;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class EnTypeS1000DAttributesMustBeCompliant extends EnAbstractRuleChecker<EnType> {
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    public static final String NAME = "TYPE_S1000D_ATTRIBUTES_MUST_BE_COMPLIANT";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("S1000D attributes of types must be compliant.")
                                                         .appliesTo("Types"),
                               SEVERITY);

    public EnTypeS1000DAttributesMustBeCompliant(SnapshotManager manager) {
        super(manager,
              EnType.class,
              RULE);
    }

    public static boolean isCompliant(EnType type) {
        final S1000DProductIdentifier pi = type.getS1000DProductIdentifier();
        final S1000DPropertyType pt = type.getS1000DPropertyType();
        return pi.isCompliantWith(pt);
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnType object,
                             Location location) {
        if (!isCompliant(object)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("S1000D property type: " + object.getS1000DPropertyType()
                               + " is not compliant with S1000D product identifier: "
                               + object.getS1000DProductIdentifier());

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}