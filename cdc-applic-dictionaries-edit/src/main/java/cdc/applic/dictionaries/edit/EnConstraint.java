package cdc.applic.dictionaries.edit;

public final class EnConstraint extends EnAbstractOwnedElement<EnDictionary<?>> {
    public static final String KIND = "Constraint";
    private String typeName;
    private String params;

    protected EnConstraint(Builder builder) {
        super(builder);
        this.typeName = builder.typeName;
        this.params = builder.params;
        addToOwner();
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public EnRef<EnConstraint> getRef() {
        return getRef(EnConstraint.class);
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;

        fireSemanticChange(EnNames.TYPE_NAME);
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;

        fireSemanticChange(EnNames.PARAMS);
    }

    static Builder builder(EnDictionary<?> owner) {
        return new Builder(owner);
    }

    public static final class Builder
            extends EnAbstractOwnedElement.Builder<Builder, EnConstraint, EnDictionary<?>> {
        private String typeName;
        private String params;

        protected Builder(EnDictionary<?> owner) {
            super(owner);
        }

        @Override
        public Class<EnConstraint> getBuiltClass() {
            return EnConstraint.class;
        }

        public Builder typeName(String typeName) {
            this.typeName = typeName;
            return self();
        }

        public Builder params(String params) {
            this.params = params;
            return self();
        }

        @Override
        public EnConstraint build() {
            return wrap(new EnConstraint(this));
        }
    }
}