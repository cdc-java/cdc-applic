package cdc.applic.dictionaries.edit;

import cdc.applic.expressions.content.RealSet;

public final class EnRealType extends EnType
        implements EnFrozenItem, EnDomainItem {
    public static final String KIND = "RealType";
    private String domain;
    private boolean frozen;

    protected EnRealType(Builder builder) {
        super(builder);
        this.domain = builder.domain;
        this.frozen = builder.frozen;
        addToOwner();
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public EnRef<EnRealType> getRef() {
        return getRef(EnRealType.class);
    }

    @Override
    public String getDomain() {
        return domain;
    }

    @Override
    public void setDomain(String domain) {
        this.domain = domain;
        fireSemanticChange(EnNames.DOMAIN);
    }

    public void setDomain(RealSet domain) {
        setDomain(domain == null ? null : domain.getContent());
    }

    @Override
    public boolean isFrozen() {
        return frozen;
    }

    @Override
    public void setFrozen(boolean frozen) {
        this.frozen = frozen;

        fireSemanticChange(EnNames.FROZEN);
    }

    static Builder builder(EnRegistry parent) {
        return new Builder(parent);
    }

    public static final class Builder
            extends EnType.Builder<Builder, EnRealType>
            implements EnFrozenBuilding<Builder>, EnDomainBuilding<Builder> {
        private String domain;
        private boolean frozen = false;

        protected Builder(EnRegistry parent) {
            super(parent);
        }

        @Override
        public Class<EnRealType> getBuiltClass() {
            return EnRealType.class;
        }

        @Override
        public Builder domain(String domain) {
            this.domain = domain;
            return self();
        }

        public Builder domain(RealSet domain) {
            this.domain = domain == null ? null : domain.getContent();
            return self();
        }

        @Override
        public Builder frozen(boolean frozen) {
            this.frozen = frozen;
            return self();
        }

        @Override
        public EnRealType build() {
            return wrap(new EnRealType(this));
        }
    }
}