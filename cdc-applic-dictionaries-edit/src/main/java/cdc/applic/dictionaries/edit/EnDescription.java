package cdc.applic.dictionaries.edit;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;
import cdc.util.strings.StringUtils;

/**
 * Description, using different languages, of an element.
 * <p>
 * A text content can be associated to any number of {@link Locale}s, including a {@code null} one.
 */
public final class EnDescription {
    public static final Comparator<Locale> LOCALE_LANGUAGE_COMPARATOR =
            Comparator.comparing(Locale::getLanguage);

    /** The description owner. */
    private final EnAbstractElement owner;
    /** The (locale, content) map. */
    private final Map<Locale, String> localeToContent = new HashMap<>();

    EnDescription(EnAbstractElement owner,
                  Map<Locale, String> map) {
        this.owner = Checks.isNotNull(owner, EnNames.OWNER);

        for (final Map.Entry<Locale, String> entry : map.entrySet()) {
            final Locale locale = entry.getKey();
            final String content = entry.getValue();
            set(locale, content);
        }
    }

    /**
     * @return The owner.
     */
    public EnAbstractElement getOwner() {
        return owner;
    }

    /**
     * @return The set of {@link Locale}s for which a text content is associated.
     *         May contain {@code null}.
     */
    public Set<Locale> getLocales() {
        return localeToContent.keySet();
    }

    /**
     * @param locale The {@link Locale}, possibly {@code null}.
     * @return The associated content or {@code null}.
     */
    public String getContent(Locale locale) {
        return localeToContent.get(locale);
    }

    /**
     * Clears all contents.
     */
    public void clear() {
        localeToContent.clear();
        getOwner().fireDescriptionsChange(EnNames.DESCRIPTION);
    }

    private void set(Locale locale,
                     String content) {
        if (content == null) {
            localeToContent.remove(locale);
        } else {
            localeToContent.put(locale, content);
        }
    }

    /**
     * Sets or removes the content associated to a {@link Locale}.
     * <p>
     * If {@code content} is {@code null}, the description is removed.
     *
     * @param locale The {@link Locale}, possibly {@code null}.
     * @param content The content.
     */
    public void setContent(Locale locale,
                           String content) {
        set(locale, content);
        getOwner().fireDescriptionsChange(EnNames.DESCRIPTION);
    }

    /**
     * Sets or removes the content associated to a {@link Locale}.
     * <p>
     * If {@code content} is {@code null}, the description is removed.
     * <p>
     * <b>WARNING:</b> if {@code locale} is not a recognized language tag, this may however succeed.
     *
     * @param locale The {@link Locale}, possibly {@code null}.
     * @param content The content.
     */
    public void setContent(String locale,
                           String content) {
        setContent(locale == null ? null : Locale.forLanguageTag(locale),
                   content);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append('[');
        for (final Locale locale : IterableUtils.toSortedList(getLocales(), LOCALE_LANGUAGE_COMPARATOR)) {
            builder.append('[')
                   .append(locale.toLanguageTag())
                   .append(' ')
                   .append(StringUtils.extract(getContent(locale), 10))
                   .append(']');
        }
        builder.append(']');
        return builder.toString();
    }
}