package cdc.applic.dictionaries.edit.checks;

import cdc.applic.dictionaries.edit.EnDictionary;
import cdc.applic.dictionaries.edit.EnDictionaryOwner;
import cdc.applic.dictionaries.edit.checks.rules.EnDictionaryContextExpressionMustBeSyntacticallyValid;
import cdc.applic.dictionaries.edit.checks.rules.EnDictionaryUsageItemsRefsMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnDictionaryUsageTypesRefsMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnNameIsMandatory;
import cdc.applic.dictionaries.edit.checks.rules.EnPolicyNamesMustBeUnique;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.LazyChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.checks.SpecializationChecker;
import cdc.util.lang.Introspection;

public class EnDictionaryChecker extends CompositeChecker<EnDictionary<?>> {
    public EnDictionaryChecker(SnapshotManager manager) {
        super(manager,
              Introspection.uncheckedCast(EnDictionary.class),
              new EnNameIsMandatory(manager),
              new EnPolicyNamesMustBeUnique(manager),
              new EnDictionaryContextExpressionMustBeSyntacticallyValid(manager),
              new EnAssertionsChecker(manager),
              new EnDictionaryUsageItemsRefsMustBeValid(manager),
              new EnDictionaryUsageTypesRefsMustBeValid(manager),
              new SpecializationChecker<>(manager, EnDictionary.class, new EnRegistryChecker(manager)),
              new LazyChecker<>(manager, EnDictionaryOwner.class, EnDictionariesChecker.KEY));
    }
}