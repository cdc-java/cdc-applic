package cdc.applic.dictionaries.edit.checks;

import cdc.applic.dictionaries.edit.EnEnumeratedValue;
import cdc.applic.dictionaries.edit.checks.rules.EnEnumeratedValueLiteralIsMandatory;
import cdc.applic.dictionaries.edit.checks.rules.EnSynonymsMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnSynonymsNamingConventionsRefsMustBeValid;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.SnapshotManager;

public class EnEnumeratedValueChecker extends CompositeChecker<EnEnumeratedValue> {
    public EnEnumeratedValueChecker(SnapshotManager manager) {
        super(manager,
              EnEnumeratedValue.class,
              new EnEnumeratedValueLiteralIsMandatory(manager),
              new EnSynonymsMustBeValid(manager),
              new EnSynonymsNamingConventionsRefsMustBeValid(manager));
    }
}