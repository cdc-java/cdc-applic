package cdc.applic.dictionaries.edit.checks.utils;

import cdc.issues.StructuredDescription;

public class EnIssueDescription extends StructuredDescription {
    protected EnIssueDescription(Builder builder) {
        super(builder);
    }

    public static EnIssueDescription.Builder builder() {
        return new EnIssueDescription.Builder();
    }

    public static class Builder extends StructuredDescription.Builder<Builder> {
        protected Builder() {
            super();
        }

        @Override
        public EnIssueDescription build() {
            return new EnIssueDescription(this);
        }
    }
}