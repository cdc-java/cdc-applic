package cdc.applic.dictionaries.edit.checks;

import java.util.List;
import java.util.stream.Collectors;

import cdc.applic.dictionaries.edit.EnLocation;
import cdc.applic.dictionaries.edit.EnProperty;
import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;

public class EnPropertiesChecker extends AbstractPartsChecker<EnRegistry, EnProperty, EnPropertiesChecker> {
    public EnPropertiesChecker(SnapshotManager manager) {
        super(manager,
              EnRegistry.class,
              EnProperty.class,
              new EnPropertyChecker(manager));
    }

    @Override
    protected List<LocatedObject<? extends EnProperty>> getParts(EnRegistry object) {
        final List<EnProperty> parts = object.getProperties();
        return parts.stream()
                    .map(x -> LocatedObject.of(x, EnLocation.builder().element(x).build()))
                    .collect(Collectors.toList());
    }
}