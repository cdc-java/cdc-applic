package cdc.applic.dictionaries.edit.checks.rules;

import cdc.applic.dictionaries.edit.EnAlias;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.applic.expressions.Expression;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;

public class EnAliasExpressionMustBeSyntacticallyValid extends EnAbstractExpressionMustBeSyntacticallyValid<EnAlias> {
    public static final String NAME = "ALIAS_EXPRESSION_MUST_BE_SYNTACTICALLY_VALID";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Expression of an alias must be syntactically valid.")
                                                         .appliesTo("Aliases"),
                               SEVERITY);

    public EnAliasExpressionMustBeSyntacticallyValid(SnapshotManager manager) {
        super(manager,
              EnAlias.class,
              RULE);
    }

    @Override
    protected Expression getExpression(EnAlias object) {
        return object.getExpression();
    }
}