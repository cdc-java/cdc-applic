package cdc.applic.dictionaries.edit;

import cdc.applic.expressions.Expression;

public interface EnExpressionBuilding<B> {
    public B expression(Expression expression);

    public default B expression(String expression) {
        return expression(Expression.of(expression, false));
    }
}