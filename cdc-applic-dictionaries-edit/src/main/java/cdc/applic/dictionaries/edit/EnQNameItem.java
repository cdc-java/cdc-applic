package cdc.applic.dictionaries.edit;

import java.util.HashSet;
import java.util.Set;

public interface EnQNameItem extends EnNameItem {
    public EnQName getQName();

    public default Set<EnQName> getQNames() {
        final Set<EnQName> set = new HashSet<>();
        set.add(getQName());
        set.add(EnQName.of(null, getName()));
        return set;
    }
}