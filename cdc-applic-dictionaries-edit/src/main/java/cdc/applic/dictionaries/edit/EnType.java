package cdc.applic.dictionaries.edit;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cdc.applic.dictionaries.s1000d.S1000DProductIdentifier;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;

public abstract class EnType extends EnAbstractOwnedElement<EnRegistry>
        implements EnQNameItem, EnSynonymsItem {
    private String name;
    private final EnSynonyms synonyms;
    private S1000DPropertyType s1000DPropertyType;
    private S1000DProductIdentifier s1000DProductIdentifier;

    protected EnType(Builder<?, ?> builder) {
        super(builder);
        this.name = builder.name;
        this.synonyms = new EnSynonyms(this, builder.synonyms);
        this.s1000DPropertyType = builder.s1000DPropertyType;
        this.s1000DProductIdentifier = builder.s1000DProductIdentifier;
    }

    @Override
    public final EnRegistry getOwner() {
        return super.getOwner();
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public final EnQName getQName() {
        return EnQName.of(getOwner().getPrefix(), name);
    }

    @Override
    public final void setName(String name) {
        this.name = name;

        fireSemanticChange(EnNames.NAME);
    }

    @Override
    public final EnSynonyms getSynonyms() {
        return synonyms;
    }

    public final Set<String> getNameAndSynonyms() {
        return synonyms.getRefAndSynonyms(name);
    }

    public final Set<EnQName> getQNameAndQSynonyms() {
        return EnQName.of(getOwner().getPrefix(), getNameAndSynonyms());
    }

    public final S1000DProductIdentifier getS1000DProductIdentifier() {
        return s1000DProductIdentifier;
    }

    public final void setS1000DProductIdentifier(S1000DProductIdentifier s1000dProductIdentifier) {
        s1000DProductIdentifier = s1000dProductIdentifier;

        fireSemanticChange(EnNames.S1000D_PRODUCT_IDENTIFIER);
    }

    public final S1000DPropertyType getS1000DPropertyType() {
        return s1000DPropertyType;
    }

    public final void setS1000DPropertyType(S1000DPropertyType s1000dPropertyType) {
        s1000DPropertyType = s1000dPropertyType;

        fireSemanticChange(EnNames.S1000D_PROPERTY_TYPE);
    }

    public abstract static class Builder<B extends Builder<B, E>,
                                         E extends EnType>
            extends EnAbstractOwnedElement.Builder<B, E, EnRegistry>
            implements EnNameBuilding<B>, EnSynonymsBuilding<B> {
        private String name;
        private final Map<String, String> synonyms = new HashMap<>();
        private S1000DPropertyType s1000DPropertyType = S1000DPropertyType.UNDEFINED;
        private S1000DProductIdentifier s1000DProductIdentifier = S1000DProductIdentifier.NOT_APPLICABLE;

        protected Builder(EnRegistry owner) {
            super(owner);
        }

        @Override
        public final B name(String name) {
            this.name = name;
            return self();
        }

        public final B s1000DPropertyType(S1000DPropertyType type) {
            this.s1000DPropertyType = type;
            return self();
        }

        public final B s1000DProductIdentifier(S1000DProductIdentifier identifier) {
            this.s1000DProductIdentifier = identifier;
            return self();
        }

        @Override
        public final B synonym(String conventionId,
                               String synonym) {
            this.synonyms.put(conventionId, synonym);
            return self();
        }
    }
}