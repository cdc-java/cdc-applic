package cdc.applic.dictionaries.edit.checks;

import cdc.applic.dictionaries.edit.EnEnumeratedType;
import cdc.applic.dictionaries.edit.checks.rules.EnEnumeratedTypeLiteralsAndSynonymsMustBeUnique;
import cdc.applic.dictionaries.edit.checks.rules.EnEnumeratedTypePartialOrderMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnEnumeratedTypeValuesRefsMustBeValid;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.SnapshotManager;

public class EnEnumeratedTypeChecker extends CompositeChecker<EnEnumeratedType> {
    public EnEnumeratedTypeChecker(SnapshotManager manager) {
        super(manager,
              EnEnumeratedType.class,
              new EnEnumeratedValuesChecker(manager),
              new EnEnumeratedTypeValuesRefsMustBeValid(manager),
              new EnEnumeratedTypePartialOrderMustBeValid(manager),
              new EnEnumeratedTypeLiteralsAndSynonymsMustBeUnique(manager));
    }
}