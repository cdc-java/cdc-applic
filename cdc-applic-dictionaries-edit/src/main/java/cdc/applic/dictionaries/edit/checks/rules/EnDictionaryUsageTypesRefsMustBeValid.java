package cdc.applic.dictionaries.edit.checks.rules;

import java.util.Set;

import cdc.applic.dictionaries.edit.EnDictionary;
import cdc.applic.dictionaries.edit.EnRef;
import cdc.applic.dictionaries.edit.EnRegistry;
import cdc.applic.dictionaries.edit.EnType;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.util.lang.Introspection;

public class EnDictionaryUsageTypesRefsMustBeValid extends EnAbstractRefMustBeValid<EnDictionary<?>, EnType> {
    public static final String NAME = "DICTIONARY_USAGE_TYPES_REFS_MUST_BE_VALID";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Referenced types in usage must exist and be usable.")
                                                         .appliesTo("Policies",
                                                                    "Registries"),
                               SEVERITY);

    public EnDictionaryUsageTypesRefsMustBeValid(SnapshotManager manager) {
        super(manager,
              Introspection.uncheckedCast(EnDictionary.class),
              EnType.class,
              RULE,
              "type ref");
    }

    @Override
    protected boolean isValid(EnRef<EnType> ref) {
        if (ref.canResolve()) {
            final EnRegistry registry = ref.getOwner(EnDictionary.class).getRegistry();
            final EnType type = ref.resolve();
            return registry.isKnownType(type);
        } else {
            return false;
        }
    }

    @Override
    protected Set<EnRef<EnType>> getRefs(EnDictionary<?> object) {
        return object.getTypeToUsageKeys();
    }
}