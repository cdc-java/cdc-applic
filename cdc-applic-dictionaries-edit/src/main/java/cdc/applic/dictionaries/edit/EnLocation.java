package cdc.applic.dictionaries.edit;

import cdc.issues.locations.AbstractLocation;
import cdc.issues.locations.Locations;

/**
 * Implementation of Location dedicated to repository edition.
 * <p>
 * It can be used to designate:
 * <ul>
 * <li>A repository.
 * <li>An element in a repository.
 * <li>An element in a repository and a detail into it.
 * </ul>
 */
public class EnLocation extends AbstractLocation {
    public static final String TAG = "EnLocation";
    private static final String SEP = ":";

    static {
        Locations.register(TAG, EnLocation::of);
    }

    private final String path;
    private final String elementId;
    private final String detail;

    protected EnLocation(Builder builder) {
        this.path = builder.path;
        this.elementId = builder.elementId;
        this.detail = builder.detail;
    }

    public static void elaborate() {
        // Ignore
    }

    public static EnLocation of(String path,
                                String anchor) {
        return builder().path(path)
                        .anchor(anchor)
                        .build();
    }

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    public String getPath() {
        return path;
    }

    public String getElementId() {
        return elementId;
    }

    public boolean hasElementId() {
        return elementId != null;
    }

    public String getDetail() {
        return detail;
    }

    public boolean hasDetail() {
        return detail != null;
    }

    @Override
    public String getAnchor() {
        if (hasElementId()) {
            if (hasDetail()) {
                return elementId + SEP + detail;
            } else {
                return elementId;
            }
        } else {
            if (hasDetail()) {
                return SEP + detail;
            } else {
                return null;
            }
        }
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return super.equals(object);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String path;
        private String elementId;
        private String detail;

        protected Builder() {
            super();
        }

        public Builder path(String path) {
            this.path = path;
            return this;
        }

        public Builder elementId(String elementId) {
            this.elementId = elementId;
            return this;
        }

        public Builder element(EnElement element) {
            return elementId(element == null ? null : element.getId());
        }

        public Builder detail(String detail) {
            this.detail = detail;
            return this;
        }

        public Builder anchor(String anchor) {
            if (anchor == null) {
                this.elementId = null;
                this.detail = null;
            } else {
                final int posSep = anchor.indexOf(SEP);
                if (posSep >= 0) {
                    this.elementId = anchor.substring(0, posSep);
                    this.detail = anchor.substring(posSep + 1);
                } else {
                    this.elementId = anchor;
                    this.detail = null;
                }
            }
            return this;
        }

        public EnLocation build() {
            return new EnLocation(this);
        }
    }
}