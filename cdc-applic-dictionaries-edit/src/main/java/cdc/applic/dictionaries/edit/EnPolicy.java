package cdc.applic.dictionaries.edit;

import java.util.Collections;
import java.util.List;

public final class EnPolicy extends EnDictionary<EnDictionary<?>> {
    public static final String KIND = "Policy";

    protected EnPolicy(Builder builder) {
        super(builder);
        addToOwner();
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public EnRef<EnPolicy> getRef() {
        return getRef(EnPolicy.class);
    }

    @Override
    public EnRegistry getRegistry() {
        return getOwner().getRegistry();
    }

    @Override
    public List<EnDictionary<?>> getParents(boolean fail) {
        if (getOwner() instanceof EnDictionary) {
            return List.of((EnDictionary<?>) getOwner());
        } else {
            return Collections.emptyList();
        }
    }

    static Builder builder(EnDictionary<?> owner) {
        return new Builder(owner);
    }

    public static final class Builder
            extends EnDictionary.Builder<Builder, EnPolicy, EnDictionary<?>> {
        protected Builder(EnDictionary<?> owner) {
            super(owner);
        }

        @Override
        public Class<EnPolicy> getBuiltClass() {
            return EnPolicy.class;
        }

        @Override
        public EnPolicy build() {
            return wrap(new EnPolicy(this));
        }
    }
}