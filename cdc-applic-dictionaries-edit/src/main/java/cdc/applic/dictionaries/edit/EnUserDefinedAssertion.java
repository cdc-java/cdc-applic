package cdc.applic.dictionaries.edit;

import cdc.applic.expressions.Expression;
import cdc.util.lang.Checks;

public final class EnUserDefinedAssertion extends EnAbstractOwnedElement<EnDictionary<?>>
        implements EnExpressionItem {
    public static final String KIND = "UserDefinedAssertion";
    private Expression expression;

    protected EnUserDefinedAssertion(Builder builder) {
        super(builder);
        this.expression = builder.expression;
        addToOwner();
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public EnRef<EnUserDefinedAssertion> getRef() {
        return getRef(EnUserDefinedAssertion.class);
    }

    @Override
    public Expression getExpression() {
        return expression;
    }

    @Override
    public void setExpression(Expression expression) {
        this.expression = Checks.isNotNull(expression, EnNames.EXPRESSION);

        fireSemanticChange(EnNames.EXPRESSION);
    }

    static Builder builder(EnDictionary<?> owner) {
        return new Builder(owner);
    }

    public static final class Builder
            extends EnAbstractOwnedElement.Builder<Builder, EnUserDefinedAssertion, EnDictionary<?>>
            implements EnExpressionBuilding<Builder> {
        private Expression expression = Expression.TRUE;

        protected Builder(EnDictionary<?> owner) {
            super(owner);
        }

        @Override
        public Class<EnUserDefinedAssertion> getBuiltClass() {
            return EnUserDefinedAssertion.class;
        }

        @Override
        public Builder expression(Expression expression) {
            Checks.isNotNull(expression, EnNames.EXPRESSION);
            this.expression = expression;
            return self();
        }

        @Override
        public EnUserDefinedAssertion build() {
            return wrap(new EnUserDefinedAssertion(this));
        }
    }
}