package cdc.applic.dictionaries.edit.checks;

import cdc.applic.dictionaries.edit.EnIntegerType;
import cdc.applic.dictionaries.edit.checks.rules.EnTypeDomainIsMandatory;
import cdc.applic.dictionaries.edit.checks.rules.EnIntegerDomainMustBeValid;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.SnapshotManager;

public class EnIntegerTypeChecker extends CompositeChecker<EnIntegerType> {
    public EnIntegerTypeChecker(SnapshotManager manager) {
        super(manager,
              EnIntegerType.class,
              new EnTypeDomainIsMandatory(manager),
              new EnIntegerDomainMustBeValid(manager));
    }
}