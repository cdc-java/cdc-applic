package cdc.applic.dictionaries.edit;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public final class EnEnumeratedValue extends EnAbstractOwnedElement<EnEnumeratedType>
        implements EnSynonymsItem, EnOrdinalItem {
    public static final String KIND = "EnumeratedValue";
    private String literal;
    private String shortLiteral;
    private final EnSynonyms synonyms;
    private int ordinal;

    protected EnEnumeratedValue(Builder builder) {
        super(builder);
        this.literal = builder.literal;
        this.shortLiteral = builder.shortLiteral;
        this.synonyms = new EnSynonyms(this, builder.synonyms);
        this.ordinal = builder.ordinal;
        addToOwner();
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public EnRef<EnEnumeratedValue> getRef() {
        return getRef(EnEnumeratedValue.class);
    }

    public String getLiteral() {
        return literal;
    }

    public Set<String> getLiteralAndSynonyms() {
        return synonyms.getRefAndSynonyms(literal);
    }

    public void setLiteral(String literal) {
        this.literal = literal;

        fireSemanticChange(EnNames.LITERAL);
    }

    public String getShortLiteral() {
        return shortLiteral;
    }

    public void setShortLiteral(String shortLiteral) {
        this.shortLiteral = shortLiteral;

        fireDescriptionsChange(EnNames.SHORT_LITERAL);
    }

    @Override
    public EnSynonyms getSynonyms() {
        return synonyms;
    }

    @Override
    public int getOrdinal() {
        return ordinal;
    }

    @Override
    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;

        fireWritingChange(EnNames.ORDINAL);
    }

    static Builder builder(EnEnumeratedType owner) {
        return new Builder(owner);
    }

    public static final class Builder
            extends EnAbstractOwnedElement.Builder<Builder, EnEnumeratedValue, EnEnumeratedType>
            implements EnSynonymsBuilding<Builder>, EnOrdinalBuilding<Builder> {
        private String literal;
        private String shortLiteral;
        private final Map<String, String> synonyms = new HashMap<>();
        private int ordinal = 0;

        protected Builder(EnEnumeratedType owner) {
            super(owner);
        }

        @Override
        public Class<EnEnumeratedValue> getBuiltClass() {
            return EnEnumeratedValue.class;
        }

        public Builder literal(String literal) {
            this.literal = literal;
            return self();
        }

        public Builder shortLiteral(String shortLiteral) {
            this.shortLiteral = shortLiteral;
            return self();
        }

        @Override
        public Builder synonym(String conventionId,
                               String synonym) {
            this.synonyms.put(conventionId, synonym);
            return self();
        }

        @Override
        public Builder ordinal(int ordinal) {
            this.ordinal = ordinal;
            return self();
        }

        @Override
        public EnEnumeratedValue build() {
            return wrap(new EnEnumeratedValue(this));
        }
    }
}