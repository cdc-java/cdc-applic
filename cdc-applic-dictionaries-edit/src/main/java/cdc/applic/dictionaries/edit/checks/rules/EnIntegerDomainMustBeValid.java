package cdc.applic.dictionaries.edit.checks.rules;

import cdc.applic.dictionaries.edit.EnIntegerType;
import cdc.applic.dictionaries.edit.checks.utils.EnAbstractRuleChecker;
import cdc.applic.dictionaries.edit.checks.utils.EnIssueDescription;
import cdc.applic.dictionaries.edit.checks.utils.EnRuleUtils;
import cdc.applic.expressions.SyntacticException;
import cdc.applic.expressions.content.IntegerSet;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;

public class EnIntegerDomainMustBeValid extends EnAbstractRuleChecker<EnIntegerType> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final String NAME = "INTEGER_DOMAIN_MUST_BE_VALID";
    public static final Rule RULE =
            EnRuleUtils.define(NAME,
                               description -> description.text("Integer types must have a valid domain.")
                                                         .appliesTo("Integer types"),
                               SEVERITY);

    public EnIntegerDomainMustBeValid(SnapshotManager manager) {
        super(manager,
              EnIntegerType.class,
              RULE);
    }

    public static boolean isValid(String domain) {
        try {
            IntegerSet.of(domain);
            return true;
        } catch (final SyntacticException e) {
            return false;
        }
    }

    @Override
    public CheckResult check(CheckContext context,
                             EnIntegerType object,
                             Location location) {
        final String domain = object.getDomain();
        if (EnTypeDomainIsMandatory.isDefined(domain)
                && !isValid(domain)) {
            final EnIssueDescription.Builder description = EnIssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has an invalid domain: '" + domain + "'");

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;

        } else {
            return CheckResult.SUCCESS;
        }
    }
}