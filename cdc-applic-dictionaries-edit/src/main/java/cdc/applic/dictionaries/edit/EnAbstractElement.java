package cdc.applic.dictionaries.edit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cdc.applic.dictionaries.edit.events.EnChangeEvent;
import cdc.util.lang.Checks;

public abstract class EnAbstractElement implements EnElement, EnDescriptionItem {
    private final String id;
    private final EnDescription description;
    private final List<EnAbstractOwnedElement<?>> children = new ArrayList<>();

    protected EnAbstractElement(Builder<?, ?> builder) {
        this.id = Checks.isNotNull(builder.id, EnNames.ID);
        this.description = new EnDescription(this, builder.descriptions);
    }

    protected final void addChild(EnAbstractOwnedElement<?> child) {
        Checks.isNotNull(child, EnNames.CHILD);

        children.add(child);
    }

    protected final void removeChild(EnElement child) {
        Checks.isNotNull(child, EnNames.CHILD);

        children.remove(child);
    }

    private void fireChange(EnChangeEvent.Impact impact,
                            String message) {
        getRepository().fireChange(this, impact, message);
    }

    protected void fireSemanticChange(String message) {
        fireChange(EnChangeEvent.Impact.SEMANTIC, message);
    }

    protected void fireWritingChange(String message) {
        fireChange(EnChangeEvent.Impact.WRITING, message);
    }

    protected void fireDescriptionsChange(String message) {
        fireChange(EnChangeEvent.Impact.DESCRIPTIONS, message);
    }

    @Override
    public final String getId() {
        return id;
    }

    @Override
    public final EnDescription getDescription() {
        return description;
    }

    /**
     * @param child The child element.
     * @return The index of {@code child} in among the children of the element.
     */
    protected final int getChildIndex(EnElement child) {
        return children.indexOf(child);
    }

    @Override
    public List<EnAbstractOwnedElement<?>> getChildren() {
        return children;
    }

    @Override
    public String toString() {
        return getId();
    }

    public abstract static class Builder<B extends Builder<B, E>, E extends EnAbstractElement>
            implements EnElement.Builder<B>, EnDescriptionBuilding<B> {
        private String id;
        private final Map<Locale, String> descriptions = new HashMap<>();

        protected Builder() {
        }

        @Override
        public B self() {
            @SuppressWarnings("unchecked")
            final B tmp = (B) this;
            return tmp;
        }

        public final boolean hasId() {
            return id != null;
        }

        /**
         * Sets the identifier.
         *
         * @param id The identifier.
         * @return This builder.
         */
        public final B id(String id) {
            this.id = id;
            return self();
        }

        @Override
        public final B description(Locale locale,
                                   String content) {
            descriptions.put(locale, content);
            return self();
        }

        /**
         * @return The built element.
         */
        @Override
        public abstract E build();
    }
}