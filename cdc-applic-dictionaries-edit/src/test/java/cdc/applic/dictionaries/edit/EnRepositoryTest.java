package cdc.applic.dictionaries.edit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.function.Consumer;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.edit.checks.EnRepositoryChecker;
import cdc.applic.dictionaries.edit.events.EnEvent;
import cdc.applic.dictionaries.edit.io.EnRepositoryXml;
import cdc.applic.dictionaries.s1000d.S1000DProductIdentifier;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.io.utils.NonCloseableOutputStream;
import cdc.io.xml.XmlWriter;
import cdc.issues.checks.io.WorkbookCheckStatsIo;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesWriter;
import cdc.issues.io.OutSettings;
import cdc.issues.locations.Location;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.util.events.ProgressController;

class EnRepositoryTest {
    private static final Logger LOGGER = LogManager.getLogger(EnRepositoryTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.INFO).buildPrintStream();

    private static void print(EnRepository repository) throws IOException {
        OUT.println("===========================================");
        final XmlWriter writer = new XmlWriter(new NonCloseableOutputStream(OUT));
        writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
        writer.setEnabled(XmlWriter.Feature.APPEND_FINAL_EOL);
        EnRepositoryXml.Printer.write(writer, repository);
    }

    @Test
    void test() throws IOException {
        final Consumer<EnEvent> handler = e -> {
            LOGGER.info("accept: {}", e);
        };

        final String NC1 = "NC1";

        final EnRepository repository =
                EnRepository.builder()
                            .description("fr", "xxx")
                            .build();

        print(repository);

        repository.addHandler(handler);

        assertNotNull(repository.getId());
        assertSame(1, repository.getDescription().getLocales().size());
        assertSame(0, repository.getRegistries().size());
        assertSame(1, repository.getDepth());
        assertFalse(repository.hasOwner());

        final EnRegistry registry1 =
                repository.registry()
                          .name("Reg1")
                          .prefix("R1")
                          .description("fr", "xxx")
                          .contextExpression("???")
                          .build();
        assertNotNull(registry1.getId());
        assertSame(1, registry1.getDescription().getLocales().size());
        assertSame(1, repository.getRegistries().size());
        assertSame(2, registry1.getDepth());
        assertTrue(registry1.hasOwner());

        final EnPolicy policy1 =
                registry1.policy()
                         .name("Pol1")
                         .typeIdUsage("T1", DItemUsage.MANDATORY)
                         .itemIdUsage("IT1", DItemUsage.OPTIONAL)
                         .writingRuleNames("Rule1", "Rule2")
                         .build();
        assertNotNull(policy1.getId());
        assertSame(0, policy1.getDescription().getLocales().size());
        assertSame(1, registry1.getPolicies().size());
        assertSame(3, policy1.getDepth());

        final EnBooleanType bool1 =
                registry1.booleanType()
                         .build();

        assertNotNull(bool1.getId());
        assertSame(1, registry1.getTypes().size());
        assertSame(3, bool1.getDepth());

        final EnEnumeratedType enum1 =
                registry1.enumeratedType()
                         .description("INVALID", "xxx")
                         .build();

        assertNotNull(enum1.getId());
        assertSame(2, registry1.getTypes().size());
        assertSame(3, enum1.getDepth());

        assertSame(0, enum1.getValues().size());

        final EnEnumeratedValue v1 =
                enum1.enumeratedValue()
                     .description("fr", "xxx")
                     .literal("AAA")
                     .ordinal(1)
                     .build();

        final EnEnumeratedValue v2 =
                enum1.enumeratedValue()
                     .description("fr", "xxx")
                     .shortLiteral("B")
                     .synonym(NC1, "b")
                     .ordinal(2)
                     .build();

        final EnEnumeratedValue v3 =
                enum1.enumeratedValue()
                     .description("fr", "xxx")
                     .literal("CCC")
                     .ordinal(3)
                     .build();

        // final EnEnumeratedValue v4 =
        enum1.enumeratedValue()
             .literal("CCC")
             .ordinal(4)
             .build();

        assertSame(4, enum1.getValues().size());

        enum1.addLessThan(v1, v2);
        enum1.addLessThan(v1, v3);

        final EnIntegerType int1 =
                registry1.integerType()
                         .domain("TODO")
                         .frozen(true)
                         .s1000DProductIdentifier(S1000DProductIdentifier.PRIMARY)
                         .s1000DPropertyType(S1000DPropertyType.PRODUCT_ATTRIBUTE)
                         .build();

        assertTrue(int1.isFrozen());

        final EnRealType real1 =
                registry1.realType()
                         .frozen(true)
                         .s1000DProductIdentifier(S1000DProductIdentifier.PRIMARY)
                         .s1000DPropertyType(S1000DPropertyType.PRODUCT_ATTRIBUTE)
                         .build();
        assertTrue(real1.isFrozen());

        final EnPatternType pattern1 =
                registry1.patternType()
                         .frozen(true)
                         .pattern("TODO")
                         .build();
        assertTrue(pattern1.isFrozen());

        final EnProperty property1 =
                registry1.property()
                         .description("fr", "xxx")
                         .type(enum1)
                         .ordinal(1)
                         .build();
        assertSame(1, registry1.getProperties().size());
        assertSame(3, property1.getDepth());
        assertEquals(enum1.getId(), property1.getTypeRef().getId());
        assertSame(property1, property1.getTypeRef().getOwner());
        assertSame(repository, property1.getTypeRef().getOwner().getRepository());
        assertTrue(property1.getTypeRef().canResolve());
        assertSame(1, property1.getOrdinal());

        final EnAlias alias1 =
                registry1.alias()
                         .name("A1")
                         .description("fr", "xxx")
                         .expression("??? ???")
                         .ordinal(10)
                         .synonym(NC1, "a1")
                         .build();
        assertSame(1, registry1.getAliases().size());
        assertSame(3, alias1.getDepth());

        final EnUserDefinedAssertion uda1 =
                policy1.assertion()
                       .description("fr", "xxx")
                       .expression("??? ???")
                       .build();
        assertSame(1, policy1.getUserDefinedAssertions().size());
        assertSame(4, uda1.getDepth());

        final EnConstraint constraint1 =
                policy1.constraint()
                       .description("fr", "xxx")
                       .typeName("XXX")
                       .params("YYY")
                       .build();
        assertSame(1, policy1.getConstraints().size());
        assertSame(4, constraint1.getDepth());

        final EnNamingConvention convention1 =
                registry1.namingConvention()
                         .description("xxx")
                         .build();
        assertSame(1, registry1.getNamingConventions().size());
        assertSame(3, convention1.getDepth());

        alias1.getSynonyms().set(convention1, "XXX");
        registry1.setParents(policy1);

        print(repository);

        assertSame(1, registry1.getParentRefs().size());

        assertTrue(policy1.isAlive());
        policy1.remove();
        assertSame(0, registry1.getPolicies().size());
        assertFalse(policy1.isAlive());

        repository.getDescription().clear();
        registry1.setName("Rep1");
        print(repository);

        registry1.namingConvention()
                 .name(NC1)
                 .build();
        registry1.namingConvention()
                 .name(NC1)
                 .build();

        registry1.realType()
                 .name("Real1")
                 .build();
        registry1.realType()
                 .name("Real1")
                 .build();
        registry1.realType()
                 .name("Real2")
                 .synonym(NC1, "Real1")
                 .build();

        final EnRepositoryChecker checker =
                new EnRepositoryChecker("Test",
                                        "Snapshot",
                                        repository);

        final File issuesFile = new File("target/issues.xlsx");
        checker.check();
        checker.print(OUT);

        LOGGER.info("Generate {}", issuesFile);
        IssuesWriter.save(checker.getManager(),
                          checker.getManager().getIssuesCollector().getIssues(),
                          OutSettings.builder()
                                     .hint(OutSettings.Hint.NO_ANSWERS)
                                     .hint(OutSettings.Hint.AUTO_LOCATIONS)
                                     .build(),
                          issuesFile,
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_BEST);

        final WorkbookCheckStatsIo<Location> io = new WorkbookCheckStatsIo<>(i -> i.toString());
        final File statsFile = new File("target/stats.xlsx");
        LOGGER.info("Generate {}", statsFile);
        io.save(checker.getManager().getStats().orElseThrow(),
                statsFile,
                WorkbookWriterFeatures.STANDARD_BEST);
    }
}