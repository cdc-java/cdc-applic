package cdc.applic.dictionaries.edit;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.edit.checks.EnRepositoryChecker;
import cdc.applic.dictionaries.edit.checks.rules.EnAliasExpressionMustBeSyntacticallyValid;
import cdc.applic.dictionaries.edit.checks.rules.EnAssertionExpressionMustBeSyntacticallyValid;
import cdc.applic.dictionaries.edit.checks.rules.EnDictionaryContextExpressionMustBeSyntacticallyValid;
import cdc.applic.dictionaries.edit.checks.rules.EnDictionaryUsageItemsRefsMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnDictionaryUsageTypesRefsMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnEnumeratedTypePartialOrderMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnEnumeratedTypeValuesRefsMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnItemNamesAndSynonymsMustBeUnique;
import cdc.applic.dictionaries.edit.checks.rules.EnItemNamesAndSynonymsMustNotBeAmbiguous;
import cdc.applic.dictionaries.edit.checks.rules.EnNameIsMandatory;
import cdc.applic.dictionaries.edit.checks.rules.EnPolicyNamesMustBeUnique;
import cdc.applic.dictionaries.edit.checks.rules.EnPropertyTypeRefMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnRegistryDependencyCyclesAreForbidden;
import cdc.applic.dictionaries.edit.checks.rules.EnRegistryNamesMustBeUnique;
import cdc.applic.dictionaries.edit.checks.rules.EnRegistryParentsRefsMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnSynonymsMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnSynonymsNamingConventionsRefsMustBeValid;
import cdc.applic.dictionaries.edit.checks.rules.EnTypeNamesAndSynonymsMustBeUnique;
import cdc.applic.dictionaries.edit.checks.rules.EnTypeNamesAndSynonymsMustNotBeAmbiguous;
import cdc.applic.dictionaries.edit.checks.rules.EnTypeS1000DAttributesMustBeCompliant;
import cdc.applic.dictionaries.edit.events.EnEvent;
import cdc.applic.dictionaries.edit.io.EnRepositoryXml;
import cdc.applic.dictionaries.s1000d.S1000DProductIdentifier;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.io.utils.NonCloseableOutputStream;
import cdc.io.xml.XmlWriter;
import cdc.issues.Issue;
import cdc.issues.checks.io.WorkbookCheckStatsIo;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesWriter;
import cdc.issues.io.OutSettings;
import cdc.issues.locations.Location;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.util.events.ProgressController;

class EnRulesTest {
    private static final Logger LOGGER = LogManager.getLogger(EnRulesTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.INFO).buildPrintStream();
    static final Consumer<EnEvent> HANDLER = e -> {
        LOGGER.info("accept: {}", e);
    };

    static void print(EnRepository repository) throws IOException {
        OUT.println("===========================================");
        final XmlWriter writer = new XmlWriter(new NonCloseableOutputStream(OUT));
        writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
        writer.setEnabled(XmlWriter.Feature.APPEND_FINAL_EOL);
        EnRepositoryXml.Printer.write(writer, repository);
    }

    private static void check(String basename,
                              EnRepository repository,
                              String... expectedRules) throws IOException {
        // print(repository);

        final EnRepositoryChecker checker =
                new EnRepositoryChecker("Test",
                                        "Snapshot",
                                        repository);

        final File repositoryFile = new File("target/rule-" + basename + "-repository.xml");
        LOGGER.info("Generate {}", repositoryFile);
        final XmlWriter writer = new XmlWriter(repositoryFile);
        writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
        writer.setEnabled(XmlWriter.Feature.APPEND_FINAL_EOL);
        EnRepositoryXml.Printer.write(writer, repository);

        final File issuesFile = new File("target/rule-" + basename + "-issues.xlsx");
        checker.check();
        // checker.print(OUT);

        LOGGER.info("Generate {}", issuesFile);
        IssuesWriter.save(checker.getManager(),
                          checker.getManager().getIssuesCollector().getIssues(),
                          OutSettings.builder()
                                     .hint(OutSettings.Hint.NO_ANSWERS)
                                     .hint(OutSettings.Hint.AUTO_LOCATIONS)
                                     .build(),
                          issuesFile,
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_BEST);

        final WorkbookCheckStatsIo<Location> io = new WorkbookCheckStatsIo<>(i -> i.toString());
        final File statsFile = new File("target/rule-" + basename + "-stats.xlsx");
        LOGGER.info("Generate {}", statsFile);
        io.save(checker.getManager().getStats().orElseThrow(),
                statsFile,
                WorkbookWriterFeatures.STANDARD_BEST);

        final Set<String> expected = Set.of(expectedRules);

        final Set<String> found = checker.getManager()
                                         .getIssuesCollector()
                                         .getIssues()
                                         .stream()
                                         .map(Issue::getName)
                                         .collect(Collectors.toSet());

        final Set<String> missing = new HashSet<>(expected);
        missing.removeAll(found);

        final Set<String> unexpected = new HashSet<>(found);
        unexpected.removeAll(expected);

        assertTrue(missing.isEmpty(), basename + " has " + missing.size() + " missing issues: " + missing);
        assertTrue(unexpected.isEmpty(), basename + " has " + unexpected.size() + " unexpected issues: " + unexpected);
    }

    private static EnRepository newRepository() {
        OUT.println("===========================================");
        final EnRepository repository =
                EnRepository.builder()
                            .build();
        repository.addHandler(HANDLER);

        return repository;
    }

    @Test
    void testRegistryName() throws IOException {
        final EnRepository repository = newRepository();

        repository.registry()
                  .build();

        check("registry-name",
              repository,
              EnNameIsMandatory.NAME);
    }

    @Test
    void testRegistryParentsRefs() throws IOException {
        final EnRepository repository = newRepository();

        repository.registry()
                  .name("R1")
                  .parentIds("???")
                  .build();

        check("registry-parents-refs",
              repository,
              EnRegistryParentsRefsMustBeValid.NAME);
    }

    @Test
    void testRegistryNamesDuplicate() throws IOException {
        final EnRepository repository = newRepository();

        repository.registry()
                  .name("R")
                  .build();
        repository.registry()
                  .name("R")
                  .build();

        check("registry-names-duplicate",
              repository,
              EnRegistryNamesMustBeUnique.NAME);
    }

    @Test
    void testRegistryContextSyntax() throws IOException {
        final EnRepository repository = newRepository();

        repository.registry()
                  .name("R")
                  .contextExpression("? ? ?")
                  .build();

        check("registry-context-syntax",
              repository,
              EnDictionaryContextExpressionMustBeSyntacticallyValid.NAME);
    }

    @Test
    void testRegistryDependencyCycleSelf() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .id("ID")
                          .name("R")
                          .parentIds("ID")
                          .build();

        assertTrue(registry.getParents(false).contains(registry));
        assertTrue(registry.getAllAncestors(false).contains(registry));
        assertTrue(registry.isCycleMember(false));

        check("registry-dependency-cycles-self",
              repository,
              EnRegistryDependencyCyclesAreForbidden.NAME);
    }

    @Test
    void testRegistryDependencyCycle() throws IOException {
        final EnRepository repository = newRepository();

        repository.registry()
                  .id("ID1")
                  .name("R1")
                  .parentIds("ID2")
                  .build();

        repository.registry()
                  .id("ID2")
                  .name("R2")
                  .parentIds("ID1")
                  .build();

        check("registry-dependency-cycles",
              repository,
              EnRegistryDependencyCyclesAreForbidden.NAME);
    }

    @Test
    void testRegistryUsageTypesRefs() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R1")
                          .build();

        registry.setTypeUsage("???", DItemUsage.FORBIDDEN);

        check("registry-usage-types-refs",
              repository,
              EnDictionaryUsageTypesRefsMustBeValid.NAME);
    }

    @Test
    void testRegistryUsageItemsRefs() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R1")
                          .build();

        registry.setItemUsage("???", DItemUsage.FORBIDDEN);

        check("registry-usage-items-refs",
              repository,
              EnDictionaryUsageItemsRefsMustBeValid.NAME);
    }

    @Test
    void testAliasName() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();

        registry.alias()
                .build();

        check("alias-name",
              repository,
              EnNameIsMandatory.NAME);
    }

    @Test
    void testAliasSynonym() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();
        final EnNamingConvention nc1 =
                registry.namingConvention()
                        .name("NC1")
                        .build();

        registry.alias()
                .name("A")
                .synonym(nc1, null)
                .build();

        check("alias-synonym",
              repository,
              EnSynonymsMustBeValid.NAME);
    }

    @Test
    void testAliasSynonymsNamingConventionRefs() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();

        registry.alias()
                .name("A")
                .synonym("???", "a")
                .build();

        check("alias-synonyms-naming-conventions-refs",
              repository,
              EnSynonymsNamingConventionsRefsMustBeValid.NAME);
    }

    @Test
    void testAliasExpressionSyntax() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();

        registry.alias()
                .name("A")
                .expression("? ? ?")
                .build();

        check("alias-expression-syntax",
              repository,
              EnAliasExpressionMustBeSyntacticallyValid.NAME);
    }

    @Test
    void testPropertyName() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();

        final EnBooleanType b =
                registry.booleanType()
                        .name("B")
                        .build();

        registry.property()
                .type(b)
                .build();

        check("property-name",
              repository,
              EnNameIsMandatory.NAME);
    }

    @Test
    void testPropertySynonym() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();
        final EnNamingConvention nc1 =
                registry.namingConvention()
                        .name("NC1")
                        .build();
        final EnBooleanType b =
                registry.booleanType()
                        .name("B")
                        .build();

        registry.property()
                .name("P")
                .type(b)
                .synonym(nc1, null)
                .build();

        check("property-synonym",
              repository,
              EnSynonymsMustBeValid.NAME);
    }

    @Test
    void testPropertyTypeRef() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();

        registry.property()
                .name("P")
                .typeId("???")
                .build();

        check("property-type-ref",
              repository,
              EnPropertyTypeRefMustBeValid.NAME);
    }

    @Test
    void testItemDuplicatNameSynonym() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();

        registry.alias()
                .name("A")
                .build();
        registry.alias()
                .name("A")
                .build();

        check("item-names-synonyms-duplicate",
              repository,
              EnItemNamesAndSynonymsMustBeUnique.NAME);
    }

    @Test
    void testItemAmbiguousNamesSynonyms() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry1 =
                repository.registry()
                          .name("R1")
                          .build();

        registry1.alias()
                 .name("A")
                 .build();

        final EnRegistry registry2 =
                repository.registry()
                          .name("R2")
                          .parentIds(registry1.getId())
                          .build();

        registry2.alias()
                 .name("A")
                 .build();

        check("item-names-synonyms-ambiguous",
              repository,
              EnItemNamesAndSynonymsMustNotBeAmbiguous.NAME);
    }

    @Test
    void testNamingConventionName() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();

        registry.namingConvention()
                .build();

        check("naming-convention-name",
              repository,
              EnNameIsMandatory.NAME);
    }

    @Test
    void testPolicyName() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();

        registry.policy()
                .build();

        check("policy-name",
              repository,
              EnNameIsMandatory.NAME);
    }

    @Test
    void testPolicyNamesDuplicate() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();

        registry.policy()
                .name("P")
                .build();
        registry.policy()
                .name("P")
                .build();

        check("policy-names-duplicate",
              repository,
              EnPolicyNamesMustBeUnique.NAME);
    }

    @Test
    void testPolicyContextSyntax() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();
        registry.policy()
                .name("P")
                .contextExpression("? ? ?")
                .build();

        check("policy-context-syntax",
              repository,
              EnDictionaryContextExpressionMustBeSyntacticallyValid.NAME);
    }

    @Test
    void testPolicyUsageTypesRefs() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R1")
                          .build();
        final EnPolicy policy =
                registry.policy()
                        .name("P")
                        .build();

        policy.setTypeUsage("???", DItemUsage.FORBIDDEN);

        check("policy-usage-types-refs",
              repository,
              EnDictionaryUsageTypesRefsMustBeValid.NAME);
    }

    @Test
    void testPolicyUsageItemsRefs() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R1")
                          .build();
        final EnPolicy policy =
                registry.policy()
                        .name("P")
                        .build();

        policy.setItemUsage("???", DItemUsage.FORBIDDEN);

        check("policy-usage-items-refs",
              repository,
              EnDictionaryUsageItemsRefsMustBeValid.NAME);
    }

    @Test
    void testTypeName() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();
        registry.booleanType()
                .build();

        check("type-name",
              repository,
              EnNameIsMandatory.NAME);
    }

    @Test
    void testTypeNamesDuplicate() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();
        registry.booleanType()
                .name("B")
                .build();
        registry.booleanType()
                .name("B")
                .build();

        check("type-names-duplicate",
              repository,
              EnTypeNamesAndSynonymsMustBeUnique.NAME);
    }

    @Test
    void testTypeNamesSynonymsDuplicate() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();
        final EnNamingConvention nc1 =
                registry.namingConvention()
                        .name("NC1")
                        .build();
        registry.booleanType()
                .name("B")
                .build();
        registry.booleanType()
                .name("B2")
                .synonym(nc1, "B")
                .build();

        check("type-names-synonyms-duplicate",
              repository,
              EnTypeNamesAndSynonymsMustBeUnique.NAME);
    }

    @Test
    void testTypeS1000DCompliance() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();
        registry.booleanType()
                .name("B")
                .s1000DPropertyType(S1000DPropertyType.PRODUCT_ATTRIBUTE)
                .s1000DProductIdentifier(S1000DProductIdentifier.NOT_APPLICABLE)
                .build();

        check("type-s1000d-compliance",
              repository,
              EnTypeS1000DAttributesMustBeCompliant.NAME);
    }

    @Test
    void testTypeNamesAmbiguous() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry1 =
                repository.registry()
                          .name("R1")
                          .build();
        registry1.booleanType()
                 .name("B")
                 .build();

        final EnRegistry registry2 =
                repository.registry()
                          .name("R2")
                          .parentIds(registry1.getId())
                          .build();
        registry2.booleanType()
                 .name("B")
                 .build();

        check("type-names-ambiguous",
              repository,
              EnTypeNamesAndSynonymsMustNotBeAmbiguous.NAME);
    }

    @Test
    void testTypeEnumeratedPartialOrder() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();
        final EnEnumeratedType type =
                registry.enumeratedType()
                        .name("E")
                        .build();
        final EnEnumeratedValue v1 =
                type.enumeratedValue()
                    .literal("V1")
                    .build();

        type.addLessThan(v1, v1);

        assertSame(1, type.getLessThanEdges().size());
        assertTrue(type.getLessThan(v1.getRef()).contains(v1.getRef()));

        check("type-enumerated-partial-order",
              repository,
              EnEnumeratedTypePartialOrderMustBeValid.NAME);
    }

    @Test
    void testTypeEnumeratedValuesRefs() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();
        final EnEnumeratedType type =
                registry.enumeratedType()
                        .name("E")
                        .build();

        type.addLessThan("xxx", "yyy");

        check("type-enumerated-values-refs",
              repository,
              EnEnumeratedTypeValuesRefsMustBeValid.NAME);
    }

    @Test
    void testAssertionExpressionSyntax() throws IOException {
        final EnRepository repository = newRepository();

        final EnRegistry registry =
                repository.registry()
                          .name("R")
                          .build();

        registry.assertion()
                .expression("? ? ?")
                .build();

        check("assertion-expression-syntax",
              repository,
              EnAssertionExpressionMustBeSyntacticallyValid.NAME);
    }
}