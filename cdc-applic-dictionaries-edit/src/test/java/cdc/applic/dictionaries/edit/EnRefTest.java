package cdc.applic.dictionaries.edit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

class EnRefTest {
    @Test
    void test() {
        final EnRepository repository = EnRepository.builder().build();
        final EnRef<EnRegistry> ref1a = EnRef.of(repository, EnRegistry.class, "R1");
        final EnRef<EnRegistry> ref1b = EnRef.of(repository, EnRegistry.class, "R1");
        final EnRef<EnRegistry> ref2 = EnRef.of(repository, EnRegistry.class, "R2");

        assertEquals(ref1a, ref1b);
        assertNotEquals(ref1a, ref2);
    }
}