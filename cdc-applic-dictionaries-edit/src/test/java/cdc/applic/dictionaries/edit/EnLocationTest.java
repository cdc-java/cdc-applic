package cdc.applic.dictionaries.edit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.issues.locations.Location;
import cdc.issues.locations.Locations;

class EnLocationTest {
    static {
        EnLocation.elaborate();
    }

    @Test
    void testPath() {
        final EnLocation location =
                EnLocation.builder()
                          .path("PATH")
                          .build();

        assertEquals("PATH", location.getPath());
        assertEquals(null, location.getElementId());
        assertEquals(null, location.getDetail());
        assertEquals(null, location.getAnchor());

        final Location l = Locations.build(location.toString());
        assertEquals(location, l);
    }

    @Test
    void testPathElementId() {
        final EnLocation location =
                EnLocation.builder()
                          .path("PATH")
                          .elementId("ID")
                          .build();

        assertEquals("PATH", location.getPath());
        assertEquals("ID", location.getElementId());
        assertEquals(null, location.getDetail());
        assertEquals("ID", location.getAnchor());

        final Location l = Locations.build(location.toString());
        assertEquals(location, l);
    }

    @Test
    void testPathElementIdDetail() {
        final EnLocation location =
                EnLocation.builder()
                          .path("PATH")
                          .elementId("ID")
                          .detail("D")
                          .build();

        assertEquals("PATH", location.getPath());
        assertEquals("ID", location.getElementId());
        assertEquals("D", location.getDetail());
        assertEquals("ID:D", location.getAnchor());

        final Location l = Locations.build(location.toString());
        assertEquals(location, l);
    }

    @Test
    void testPathDetail() {
        final EnLocation location =
                EnLocation.builder()
                          .path("PATH")
                          .detail("D")
                          .build();

        assertEquals("PATH", location.getPath());
        assertEquals(null, location.getElementId());
        assertEquals("D", location.getDetail());
        assertEquals(":D", location.getAnchor());

        final Location l = Locations.build(location.toString());
        assertEquals(location, l);
    }

    @Test
    void testPathAnchor1() {
        final EnLocation location =
                EnLocation.builder()
                          .path("PATH")
                          .anchor("ID:D")
                          .build();

        assertEquals("PATH", location.getPath());
        assertEquals("ID", location.getElementId());
        assertEquals("D", location.getDetail());
        assertEquals("ID:D", location.getAnchor());

        final Location l = Locations.build(location.toString());
        assertEquals(location, l);
    }

    @Test
    void testPathAnchor2() {
        final EnLocation location =
                EnLocation.builder()
                          .path("PATH")
                          .anchor("ID")
                          .build();

        assertEquals("PATH", location.getPath());
        assertEquals("ID", location.getElementId());
        assertEquals(null, location.getDetail());
        assertEquals("ID", location.getAnchor());

        final Location l = Locations.build(location.toString());
        assertEquals(location, l);
    }

    @Test
    void testPathAnchor3() {
        final EnLocation location =
                EnLocation.builder()
                          .path("PATH")
                          .anchor(null)
                          .build();

        assertEquals("PATH", location.getPath());
        assertEquals(null, location.getElementId());
        assertEquals(null, location.getDetail());
        assertEquals(null, location.getAnchor());

        final Location l = Locations.build(location.toString());
        assertEquals(location, l);
    }

    @Test
    void testElement() {
        final EnRepository repository =
                EnRepository.builder()
                            .build();

        final EnLocation location =
                EnLocation.builder()
                          .path("PATH")
                          .element(repository)
                          .build();

        assertEquals("PATH", location.getPath());
        assertEquals(repository.getId(), location.getElementId());
        assertEquals(null, location.getDetail());
        assertEquals(repository.getId(), location.getAnchor());

        final Location l = Locations.build(location.toString());
        assertEquals(location, l);
    }

    @Test
    void testNullElement() {

        final EnLocation location =
                EnLocation.builder()
                          .path("PATH")
                          .element(null)
                          .build();

        assertEquals("PATH", location.getPath());
        assertEquals(null, location.getElementId());
        assertEquals(null, location.getDetail());
        assertEquals(null, location.getAnchor());

        final Location l = Locations.build(location.toString());
        assertEquals(location, l);
    }
}