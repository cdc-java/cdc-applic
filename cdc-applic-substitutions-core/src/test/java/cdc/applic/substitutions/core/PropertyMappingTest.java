package cdc.applic.substitutions.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.SemanticException;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.literals.Name;

class PropertyMappingTest {
    private static final Logger LOGGER = LogManager.getLogger(PropertyMappingTest.class);

    public final RepositoryImpl repository = new RepositoryImpl();
    public final RegistryImpl registry = repository.registry().name("Registry").build();
    public final DictionaryHandle registryHandle = new DictionaryHandle(registry);

    PropertyMappingTest() {
        registry.enumeratedType().name("StandardType").frozen(false).literals("S1", "S2", "S3", "S4", "S5").build();
        registry.integerType().name("RankType").frozen(true).domain("1~999").build();

        registry.property().name("Standard").type("StandardType").ordinal(0).build();
        registry.property().name("Rank").type("RankType").ordinal(1).build();

        registry.createAssertion("Standard = S1 -> Rank in {1~100}");
        registry.createAssertion("Standard = S2 -> Rank in {101~200}");
        registry.createAssertion("Standard = S3 -> Rank in {201~300}");
        registry.createAssertion("Standard = S4 -> Rank in {301~400}");
        registry.createAssertion("Standard = S5 -> Rank in {401~500}");
    }

    @Test
    void testInvalidMapping() {
        assertThrows(SemanticException.class,
                     () -> {
                         PropertyMapping.builder("St", "Rank", registry);
                     });
        assertThrows(SemanticException.class,
                     () -> {
                         PropertyMapping.builder("Standard", "R", registry);
                     });

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         PropertyMapping.builder("Standard", "Rank", registry)
                                        .map("S", "10");
                     });

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         PropertyMapping.builder("Standard", "Rank", registry)
                                        .map("S1", "X");
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         PropertyMapping.builder("Standard", "Rank", registry)
                                        .map("S1", "10")
                                        .map("S1", "20");
                     });
    }

    @Test
    void testBuilderEmptyMapping() {
        final PropertyMapping mapping =
                PropertyMapping.builder("Standard", "Rank", registry)
                               .build();
        LOGGER.debug("mapping: " + mapping);
        LOGGER.debug(mapping.getSourceSet() + " on " + mapping.getTargetSet());
        assertEquals(registry.getProperty(Name.of("Standard")), mapping.getSource());
        assertEquals(registry.getProperty(Name.of("Rank")), mapping.getTarget());
        assertTrue(mapping.getSourceSet().isEmpty());
        assertTrue(mapping.getTargetSet().isEmpty());
        assertTrue(mapping.isCompliantWith(registry));
    }

    @Test
    void testBuilderMapping() {
        final PropertyMapping mapping =
                PropertyMapping.builder("Standard", "Rank", registry)
                               .map("S1", "1~100")
                               .map("S2", "101~200")
                               .map("S3", "201~300")
                               .map("S4", "301~400")
                               .map("S5", "401~500")
                               .build();
        LOGGER.debug("mapping: " + mapping);
        LOGGER.debug(mapping.getSourceSet() + " on " + mapping.getTargetSet());
        assertEquals(SItemSetUtils.createBest("S1, S2, S3, S4, S5"), mapping.getSourceSet());
        assertEquals(SItemSetUtils.createBest("1~500"), mapping.getTargetSet());
        assertTrue(mapping.isCompliantWith(registry));
    }

    private void testAutoCreate(ReserveStrategy reserveStrategy) {
        final PropertyMapping mapping =
                PropertyMapping.create("Standard", "Rank", registryHandle, reserveStrategy);
        LOGGER.debug("mapping: " + mapping);
        LOGGER.debug(mapping.getSourceSet() + " on " + mapping.getTargetSet());
        assertEquals(SItemSetUtils.createBest("S1, S2, S3, S4, S5"), mapping.getSourceSet());
        assertEquals(SItemSetUtils.createBest("1~500"), mapping.getTargetSet());
        assertTrue(mapping.isCompliantWith(registry));
    }

    @Test
    void testAutoCreateMapAllPossibleReserves() {
        testAutoCreate(ReserveStrategy.ALL_POSSIBLE_RESERVES);
    }

    @Test
    void testAutoCreateMapUserDefinedReserves() {
        testAutoCreate(ReserveStrategy.USER_DEFINED_RESERVES);
    }

    @Test
    void testAutoCreateMapNoReserves() {
        testAutoCreate(ReserveStrategy.NO_RESERVES);
    }

}