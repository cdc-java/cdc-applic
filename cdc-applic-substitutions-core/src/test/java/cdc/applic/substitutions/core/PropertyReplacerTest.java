package cdc.applic.substitutions.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;

class PropertyReplacerTest {
    private static final Logger LOGGER = LogManager.getLogger(PropertyReplacerTest.class);

    public final RepositoryImpl repository = new RepositoryImpl();
    public final RegistryImpl registry = repository.registry().name("Registry").build();
    public final DictionaryHandle registryHandle = new DictionaryHandle(registry);

    PropertyReplacerTest() {
        registry.enumeratedType().name("StandardType").frozen(false).literals("S1", "S2", "S3", "S4", "S5", "S6").build();
        registry.integerType().name("RankType").frozen(true).domain("1~999").build();
        registry.enumeratedType().name("VersionType").frozen(false).literals("V1", "V2").build();

        registry.property().name("Standard").type("StandardType").ordinal(0).build();
        registry.property().name("Rank").type("RankType").ordinal(1).build();
        registry.property().name("Version").type("VersionType").ordinal(2).build();

        registry.createAssertion("Standard = S1 -> Rank in {1~100}");
        registry.createAssertion("Standard = S2 -> Rank in {101~200}");
        registry.createAssertion("Standard = S3 -> Rank in {201~300}");
        registry.createAssertion("Standard = S4 -> Rank in {301~400}");
        registry.createAssertion("Standard = S5 -> Rank in {401~500}");
        registry.createAssertion("Standard = S6 -> Rank = 600");
    }

    private static void check(String expected,
                              String expression,
                              PropertyReplacer replacer) {
        LOGGER.debug("replace: " + expression);
        final Expression x = new Expression(expression);
        final Expression y = replacer.replace(x);
        LOGGER.debug("   " + y);
        assertEquals(expected, y.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testEmptyMapping() {
        final PropertyMapping mapping =
                PropertyMapping.create("Standard", "Rank", registryHandle, ReserveStrategy.ALL_POSSIBLE_RESERVES);
        LOGGER.debug("mapping: " + mapping);
        LOGGER.debug(mapping.getSourceSet() + " on " + mapping.getTargetSet());

        final PropertyReplacer replacer = new PropertyReplacer(registryHandle, mapping);

        assertSame(mapping, replacer.getMapping());
        assertSame(registryHandle, replacer.getDictionaryHandle());

        check("true", "true", replacer);
        check("false", "false", replacer);
        check("Version=V1", "Version = V1", replacer);
        check("Rank=100", "Rank = 100", replacer);
        check("Rank!=100", "Rank != 100", replacer);
        check("Rank<:{1~100}", "Standard = S1", replacer);
        check("Rank<:{1~100}|Rank<:{101~200}", "Standard = S1 or Standard = S2", replacer);
        check("Rank<:{1~100}|Rank!<:{101~200}", "Standard = S1 or Standard != S2", replacer);
        check("Rank<:{1~100,201~300}", "Standard in {S1, S3}", replacer);
        check("Rank!<:{1~100,201~300}", "Standard not in {S1, S3}", replacer);
        check("Rank=600", "Standard = S6", replacer);
        check("Rank!=600", "Standard != S6", replacer);
    }
}