package cdc.applic.substitutions.core;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.DomainedType;
import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.projections.Axis;
import cdc.applic.projections.Shadow;
import cdc.applic.projections.core.SingleAxisExpressionProjector;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.ProverMatching;
import cdc.applic.substitutions.Mapping;
import cdc.util.lang.Checks;

/**
 * Mapping between 2 properties.
 *
 * @author Damien Carbonne
 */
public class PropertyMapping implements Mapping {
    private static final String DICTIONARY = "dictionary";
    private static final String HANDLE = "handle";
    private static final String SOURCE = "source";
    private static final String SOURCE_NAME = "sourceName";
    private static final String TARGET = "target";
    private static final String TARGET_NAME = "targetName";

    private final Property source;
    private final Property target;

    /**
     * Map between items and sets.
     */
    private final Map<SItem, SItemSet> itemMapping;

    /**
     * Same map, but keys are replaced by sets.
     */
    private final Map<SItemSet, SItemSet> setMapping = new LinkedHashMap<>();

    /**
     * Set of all mapped source data.
     */
    private final SItemSet sourceSet;

    /**
     * Set of all mapped target data.
     */
    private final SItemSet targetSet;

    protected PropertyMapping(Property source,
                              Property target,
                              Map<SItem, SItemSet> map) {
        Checks.isNotNull(source, SOURCE);
        Checks.isNotNull(target, TARGET);
        Checks.isNotNull(map, "map");

        // map is supposed to be compliant with source and target

        this.source = source;
        this.target = target;
        this.itemMapping = Collections.unmodifiableMap(new LinkedHashMap<>(map));
        SItemSet srcSet = this.source.getType().getEmptySet();
        SItemSet tgtSet = this.target.getType().getEmptySet();
        // Use map that preserved order
        for (final Map.Entry<SItem, SItemSet> entry : map.entrySet()) {
            this.setMapping.put(SItemSetUtils.createBest(entry.getKey()), entry.getValue());
            srcSet = srcSet.union(entry.getKey());
            tgtSet = tgtSet.union(entry.getValue());
        }
        this.sourceSet = srcSet;
        this.targetSet = tgtSet;
    }

    @Override
    public boolean isCompliantWith(Dictionary dictionary) {
        Checks.isNotNull(dictionary, DICTIONARY);
        return dictionary.isAllowed(getSource()) && dictionary.isAllowed(getTarget());
    }

    /**
     * @return The source Property.
     */
    public Property getSource() {
        return source;
    }

    /**
     * @return The set of mapped (source) items.
     */
    public SItemSet getSourceSet() {
        return sourceSet;
    }

    /**
     * @return The target Property.
     */
    public Property getTarget() {
        return target;
    }

    /**
     * @return The set of mapping (target) items.
     */
    public SItemSet getTargetSet() {
        return targetSet;
    }

    /**
     * Returns the mapping of an item.
     *
     * @param item The item.
     * @return The mapping for {@code item}.
     * @throws IllegalArgumentException When {@code item} is {@code null},<br>
     *             or is not compliant with {@code source},<br>
     *             or can not be mapped.
     */
    public SItemSet map(SItem item) {
        Checks.isNotNull(item, "item");
        Checks.isTrue(getSource().getType().isCompliant(item), "Item {} is not compliant with: {}", item, source.getName());
        Checks.isTrue(sourceSet.contains(item), "Item {} is not a subset of: {}", item, sourceSet);

        // Try to find a direct mapping
        SItemSet result = itemMapping.get(item);
        if (result == null) {
            // Failed to find a mapping
            result = getTarget().getType().getEmptySet();
            // Iterate on all keys and append associated out to the result
            for (final Map.Entry<SItemSet, SItemSet> entry : setMapping.entrySet()) {
                if (entry.getKey().contains(item)) {
                    result = result.union(entry.getValue());
                }
            }
        }
        return result;
    }

    public SItemSet map(SItemSet set) {
        Checks.isNotNull(set, "set");

        SItemSet result = getTarget().getType().getEmptySet();

        for (final SItem item : set.getItems()) {
            result = result.union(map(item));
        }
        return result;
    }

    /**
     * Automatically creates a PropertyyMapping from assertions defined in a Dictionary.
     * <p>
     * This is possible when the source type is {@link DomainedType}.
     *
     * @param source The source Property.
     * @param target The target Property.
     * @param handle The DictionaryHandle.
     * @param reserveStrategy The ReserveStrategy to adopt.
     * @return An automatically created PropertyyMapping.
     * @throws IllegalArgumentException When {@code source}, {@code target},
     *             {@code handle} or {@code reserveStrategy} is {@code null},<br>
     *             or when automatic creation is not possible.
     */
    public static PropertyMapping create(Property source,
                                         Property target,
                                         DictionaryHandle handle,
                                         ReserveStrategy reserveStrategy) {
        Checks.isNotNull(source, SOURCE);
        Checks.isNotNull(target, TARGET);
        Checks.isNotNull(handle, HANDLE);
        Checks.isNotNull(reserveStrategy, "reserveStrategy");
        Checks.isTrue(source.getType() instanceof DomainedType, "Non supported source type: {}", source.getType().getName());

        final Builder builder = builder(source, target);

        final Axis axis = new Axis(target);
        final ProverFeatures features = ProverFeatures.builder()
                                                      .assertionStrategy(AssertionStrategy.INCLUDE_ASSERTIONS)
                                                      .reserveStrategy(reserveStrategy)
                                                      .build();
        final SingleAxisExpressionProjector projector = new SingleAxisExpressionProjector(handle, features, axis);
        final DomainedType domained = (DomainedType) source.getType();
        // Iterate on all SItems of the source domain
        for (final SItem item : domained.getDomain().getItems()) {
            // Create the node corresponding to item
            final Node node = buildNode(source, item);
            // Project that node on axis
            final Shadow shadow = projector.project(node);
            final SItemSet set = shadow.getAxisSet(axis, ProverMatching.ALWAYS, ProverMatching.SOMETIMES);
            builder.map(item, set);
        }

        return builder.build();
    }

    public static PropertyMapping create(String sourceName,
                                         String targetName,
                                         DictionaryHandle handle,
                                         ReserveStrategy reserveStrategy) {
        Checks.isNotNull(sourceName, SOURCE_NAME);
        Checks.isNotNull(targetName, TARGET_NAME);
        Checks.isNotNull(handle, HANDLE);
        Checks.isNotNull(reserveStrategy, "reserveStrategy");
        final Property source = handle.getDictionary().getOptionalAllowedProperty(Name.of(sourceName)).orElse(null);
        final Property target = handle.getDictionary().getOptionalAllowedProperty(Name.of(targetName)).orElse(null);

        return create(source, target, handle, reserveStrategy);
    }

    private static Node buildNode(Property property,
                                  SItem item) {
        if (item instanceof final Value v) {
            return new EqualNode(property.getName(), v);
        } else {
            return new InNode(property.getName(), SItemSetUtils.createBest(item));
        }
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("['")
               .append(getSource().getName())
               .append("' on '")
               .append(getTarget().getName())
               .append("'");

        // TODO sort
        for (final SItem in : itemMapping.keySet()) {
            builder.append(" (")
                   .append(in)
                   .append("->")
                   .append(itemMapping.get(in))
                   .append(")");
        }
        builder.append(']');
        return builder.toString();
    }

    public static Builder builder(Property source,
                                  Property target) {
        return new Builder(source, target);
    }

    public static Builder builder(Name sourceName,
                                  Name targetName,
                                  Dictionary dictionary) {
        Checks.isNotNull(sourceName, SOURCE_NAME);
        Checks.isNotNull(targetName, TARGET_NAME);
        Checks.isNotNull(dictionary, DICTIONARY);

        final Property source = dictionary.getProperty(sourceName);
        final Property target = dictionary.getProperty(targetName);
        return builder(source, target);
    }

    public static Builder builder(String sourceName,
                                  String targetName,
                                  Dictionary dictionary) {
        Checks.isNotNull(sourceName, SOURCE_NAME);
        Checks.isNotNull(targetName, TARGET_NAME);
        Checks.isNotNull(dictionary, DICTIONARY);

        return builder(Name.of(sourceName),
                       Name.of(targetName),
                       dictionary);
    }

    /**
     * Builder of PropertyMapping.
     *
     * @author Damien Carbonne
     */
    public static class Builder {
        private final Property source;
        private final Property target;
        /** Use a LinkedHashMap to preserve order. */
        private final Map<SItem, SItemSet> map = new LinkedHashMap<>();
        private SItemSet sourceSet;

        Builder(Property source,
                Property target) {
            Checks.isNotNull(source, SOURCE);
            Checks.isNotNull(target, TARGET);

            this.source = source;
            this.target = target;
            this.sourceSet = source.getType().getEmptySet();
        }

        private void checkIn(SItem in) {
            Checks.isNotNull(in, "in");
            Checks.isTrue(source.getType().isCompliant(in),
                          "Input {} is not compliant with {}: {}",
                          in,
                          source.getName(),
                          source.getType().getDefinition());
        }

        private void checkIn(SItemSet in) {
            Checks.isNotNull(in, "in");
            Checks.isTrue(source.getType().isCompliant(in),
                          "Input {} is not compliant with {}: {}",
                          in,
                          source.getName(),
                          source.getType().getDefinition());
        }

        private void checkOut(SItem out) {
            Checks.isNotNull(out, "out");
            Checks.isTrue(target.getType().isCompliant(out),
                          "Output {} is not compliant with {}: {}",
                          out,
                          target.getName(),
                          target.getType().getDefinition());
        }

        private void checkOut(SItemSet out) {
            Checks.isNotNull(out, "out");
            Checks.isTrue(target.getType().isCompliant(out),
                          "Output {} is not compliant with {}: {}",
                          out,
                          target.getName(),
                          target.getType().getDefinition());
        }

        /**
         * Maps an SItem onto an SItemSet.
         *
         * @param in The input data.
         * @param out The output data .
         * @return This Builder.
         * @throws IllegalArgumentException When {@code in} or {@code out} is {@code null},<br>
         *             or when they are not compliant with {@code source} or {@code target},<br>
         *             or when {@code in} is already mapped.
         */
        public Builder map(SItem in,
                           SItemSet out) {
            checkIn(in);
            checkOut(out);
            Checks.isTrue(sourceSet.intersection(in).isEmpty(), "Input {} is already mapped: {}", in, sourceSet);

            // Accept that target is already mapped
            sourceSet = sourceSet.union(in);

            map.put(in, out);
            return this;
        }

        /**
         * Maps an SItemSet onto an SItemSet.
         *
         * @param in The input data.
         * @param out The output data .
         * @return This Builder.
         * @throws IllegalArgumentException When {@code in} or {@code out} is {@code null},<br>
         *             or when they are not compliant with {@code source} or {@code target},<br>
         *             or when {@code in} is already mapped.
         */
        public Builder map(SItemSet in,
                           SItemSet out) {
            checkIn(in);
            checkOut(out);

            for (final SItem item : in.getItems()) {
                map(item, out);
            }
            return this;
        }

        /**
         * Maps an SItemSet onto an SItem.
         *
         * @param in The input data.
         * @param out The output data .
         * @return This Builder.
         * @throws IllegalArgumentException When {@code in} or {@code out} is {@code null},<br>
         *             or when they are not compliant with {@code source} or {@code target},<br>
         *             or when {@code in} is already mapped.
         */
        public Builder map(SItemSet in,
                           SItem out) {
            checkIn(in);
            checkOut(out);

            return map(in,
                       SItemSetUtils.createBest(out));
        }

        /**
         * Maps an SItem onto an SItem.
         *
         * @param in The input data.
         * @param out The output data .
         * @return This Builder.
         * @throws IllegalArgumentException When {@code in} or {@code out} is {@code null},<br>
         *             or when they are not compliant with {@code source} or {@code target},<br>
         *             or when {@code in} is already mapped.
         */
        public Builder map(SItem in,
                           SItem out) {
            checkIn(in);
            checkOut(out);

            return map(SItemSetUtils.createBest(in),
                       SItemSetUtils.createBest(out));
        }

        /**
         * Maps an input data to an output data.
         *
         * @param in The input data.
         * @param out The output data.
         * @return This Builder.
         * @throws IllegalArgumentException When {@code in} or {@code out} is {@code null},<br>
         *             or when they are not compliant with {@code source} or {@code target},<br>
         *             or when {@code in} is already mapped.
         * @throws ApplicException When {@code in} or {@code out} can not be parsed correctly.
         */
        public Builder map(String in,
                           String out) {
            final SItemSet ins = SItemSetUtils.createBest(in);
            final SItemSet outs = SItemSetUtils.createBest(out);

            return map(ins, outs);
        }

        public PropertyMapping build() {
            return new PropertyMapping(source, target, map);
        }
    }
}