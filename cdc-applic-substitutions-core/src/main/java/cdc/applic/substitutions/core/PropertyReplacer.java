package cdc.applic.substitutions.core;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractPropertyNode;
import cdc.applic.expressions.ast.AbstractSetNode;
import cdc.applic.expressions.ast.AbstractValueNode;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotEqualNode;
import cdc.applic.expressions.ast.NotInNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.substitutions.Mapping;
import cdc.applic.substitutions.Replacer;
import cdc.util.lang.Checks;

/**
 * Implementation of Replacer that can use a PropertyMapping.
 *
 * @author Damien Carbonne
 */
public class PropertyReplacer implements Replacer {
    private final PropertyMapping mapping;
    private final DictionaryHandle handle;

    public PropertyReplacer(DictionaryHandle handle,
                            PropertyMapping mapping) {
        Checks.isNotNull(handle, "handle");
        Checks.isNotNull(mapping, "mapping");
        Checks.isTrue(mapping.isCompliantWith(handle.getDictionary()),
                      "Matching is not compliant with " + handle.getDictionary().getName());

        this.handle = handle;
        this.mapping = mapping;
    }

    @Override
    public DictionaryHandle getDictionaryHandle() {
        return handle;
    }

    @Override
    public final Mapping getMapping() {
        return mapping;
    }

    @Override
    public Node replace(Node node) {
        final Converter converter = new Converter(mapping);
        return node.accept(converter);
    }

    private static class Converter extends AbstractConverter {
        private final PropertyMapping mapping;

        public Converter(PropertyMapping mapping) {
            this.mapping = mapping;
        }

        @Override
        public Node visitLeaf(AbstractLeafNode node) {
            if (node instanceof AbstractPropertyNode
                    && ((AbstractPropertyNode) node).getName().equals(mapping.getSource().getName())) {
                final boolean negative = ((AbstractPropertyNode) node).isNegative();
                final SItemSet targetSet;
                if (node instanceof AbstractValueNode) {
                    final AbstractValueNode n = (AbstractValueNode) node;
                    final Value sourceValue = n.getValue();
                    targetSet = mapping.map(sourceValue);

                } else {
                    final AbstractSetNode n = (AbstractSetNode) node;
                    final SItemSet sourceSet = n.getSet();
                    targetSet = mapping.map(sourceSet);
                }
                final Name targetName = mapping.getTarget().getName();
                if (targetSet.isSingleton()) {
                    final Value targetValue = targetSet.getSingletonValue();
                    if (negative) {
                        return new NotEqualNode(targetName, targetValue);
                    } else {
                        return new EqualNode(targetName, targetValue);
                    }
                } else {
                    if (negative) {
                        return new NotInNode(targetName, targetSet);
                    } else {
                        return new InNode(targetName, targetSet);
                    }
                }
            } else {
                return node;
            }
        }
    }
}