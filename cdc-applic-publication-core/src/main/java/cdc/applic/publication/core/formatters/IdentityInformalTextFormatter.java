package cdc.applic.publication.core.formatters;

import cdc.applic.expressions.literals.EscapingMode;
import cdc.applic.expressions.literals.InformalText;
import cdc.applic.publication.InformalTextFormatter;
import cdc.util.lang.Checks;

/**
 * Formatter of InformalTexts that returns the original text.
 *
 * @author Damien Carbonne
 */
public class IdentityInformalTextFormatter implements InformalTextFormatter {
    public static final IdentityInformalTextFormatter NON_ESCAPED =
            builder().escapingMode(EscapingMode.NON_ESCAPED)
                     .build();

    public static final IdentityInformalTextFormatter ESCAPED =
            builder().escapingMode(EscapingMode.ESCAPED)
                     .build();

    public static final IdentityInformalTextFormatter DEFAULT = ESCAPED;

    private final EscapingMode escapingMode;

    IdentityInformalTextFormatter(Builder builder) {
        this.escapingMode = builder.escapingMode;
    }

    public EscapingMode getEscapingMode() {
        return escapingMode;
    }

    @Override
    public String getText(InformalText text) {
        return escapingMode == EscapingMode.NON_ESCAPED
                ? text.getNonEscapedText()
                : text.getEscapedText();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private EscapingMode escapingMode = EscapingMode.ESCAPED;

        Builder() {
        }

        public Builder escapingMode(EscapingMode escapingMode) {
            Checks.isNotNull(escapingMode, "escapingMode");

            this.escapingMode = escapingMode;
            return this;
        }

        public IdentityInformalTextFormatter build() {
            return new IdentityInformalTextFormatter(this);
        }
    }
}