package cdc.applic.publication.core.formatters.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import cdc.applic.dictionaries.types.TypeKind;
import cdc.applic.expressions.Spacing;
import cdc.applic.expressions.SymbolType;
import cdc.applic.expressions.literals.EscapingMode;
import cdc.applic.expressions.literals.Name;
import cdc.applic.publication.InformalTextFormatter;
import cdc.applic.publication.PieceFormatter;
import cdc.applic.publication.SNameFormatter;
import cdc.applic.publication.SpaceFormatter;
import cdc.applic.publication.Symbol;
import cdc.applic.publication.SymbolFormatter;
import cdc.applic.publication.ValueFormatter;
import cdc.applic.publication.core.formatters.BooleanValueFormatter;
import cdc.applic.publication.core.formatters.FormattersCatalog;
import cdc.applic.publication.core.formatters.IdentityInformalTextFormatter;
import cdc.applic.publication.core.formatters.IdentitySNameFormatter;
import cdc.applic.publication.core.formatters.IdentityStringValueFormatter;
import cdc.applic.publication.core.formatters.IntegerValueFormatter;
import cdc.applic.publication.core.formatters.MapInformalTextFormatter;
import cdc.applic.publication.core.formatters.MapSNameFormatter;
import cdc.applic.publication.core.formatters.MapStringValueFormatter;
import cdc.applic.publication.core.formatters.RealValueFormatter;
import cdc.applic.publication.core.formatters.SpaceFormatterImpl;
import cdc.applic.publication.core.formatters.SymbolFormatterImpl;
import cdc.io.xml.AbstractStAXLoader;
import cdc.io.xml.AbstractStAXParser;
import cdc.io.xml.XmlWriter;
import cdc.util.lang.CollectionUtils;
import cdc.util.lang.FailureReaction;

public final class FormattersCatalogXml {
    private static final String BOOLEAN = "boolean";
    private static final String CONTENT = "content";
    private static final String ESCAPING_MODE = "escaping-mode";
    private static final String FALSE = "false";
    private static final String FORMAT = "format";
    private static final String FORMATTERS = "formatters";
    private static final String FORMATTERS_CATALOG = "formatters-catalog";
    private static final String ID = "id";
    private static final String INFORMAL = "informal";
    private static final String INFORMAL_IDENTITY = "informal-identity";
    private static final String INFORMAL_MAP = "informal-map";
    private static final String INTEGER = "integer";
    private static final String ITEM = "item";
    private static final String ITEMS = "items";
    private static final String LITERAL = "literal";
    private static final String LOCALE = "locale";
    private static final String LOCAL_NAME = "local-name";
    private static final String MAP = "map";
    private static final String NAME = "name";
    private static final String NAME_IDENTITY = "name-identity";
    private static final String NAME_MAP = "name-map";
    private static final String PREFIX = "prefix";
    private static final String REAL = "real";
    private static final String REQUIRES_SPACE = "requires-space";
    private static final String SETTINGS = "settings";
    private static final String SPACE = "space";
    private static final String SPACING = "spacing";
    private static final String SYMBOL = "symbol";
    private static final String SYMBOL_TYPE = "symbol-type";
    private static final String STRING = "string";
    private static final String STRING_IDENTITY = "string-identity";
    private static final String STRING_MAP = "string-map";
    private static final String TEXT = "text";
    private static final String TO = "to";
    private static final String TRUE = "true";
    private static final String TYPE = "type";
    private static final String TYPES = "types";

    private FormattersCatalogXml() {
    }

    static String toXml(Symbol symbol) {
        return symbol.name().toLowerCase().replace('_', '-');
    }

    static Symbol toSymbol(String s) {
        return Symbol.valueOf(s.toUpperCase().replace('-', '_'));
    }

    public static class Printer {
        private final Map<PieceFormatter, String> formatterToId = new HashMap<>();
        private final Map<String, PieceFormatter> idToFormatter = new HashMap<>();

        public Printer() {
            super();
        }

        private void init(FormattersCatalog catalog) {
            formatterToId.clear();
            idToFormatter.clear();

            // Collect all formatters
            final Set<PieceFormatter> formatters = new HashSet<>();
            formatters.add(catalog.getDefaultSpaceFormatter());
            formatters.add(catalog.getPrefixFormatter());
            formatters.add(catalog.getInformalTextFormatter());
            formatters.add(catalog.getLocalNameFormatter());
            formatters.add(catalog.getDefaultSymbolFormatter());
            for (final TypeKind kind : TypeKind.values()) {
                formatters.add(catalog.getDefaultValueFormatter(kind));
            }
            for (final Name name : catalog.getTypeNames()) {
                formatters.add(catalog.getTypeSpaceFormatterOrNull(name));
                formatters.add(catalog.getTypeSymbolFormatterOrNull(name));
                formatters.add(catalog.getTypeValueFormatterOrNull(name));
            }
            for (final Name name : catalog.getItemNames()) {
                formatters.add(catalog.getItemSpaceFormatterOrNull(name));
                formatters.add(catalog.getItemSymbolFormatterOrNull(name));
                formatters.add(catalog.getItemValueFormatterOrNull(name));
            }
            formatters.remove(null);

            // Collect all defined ids
            for (final PieceFormatter formatter : formatters) {
                final Object id = catalog.getIdOrNull(formatter);
                if (id != null) {
                    formatterToId.put(formatter, id.toString());
                    idToFormatter.put(id.toString(), formatter);
                }
            }

            // Create all missing ids
            // Do this after previous step to avoid creating a duplicate id
            int next = 1;
            for (final PieceFormatter formatter : formatters) {
                if (catalog.getIdOrNull(formatter) == null) {
                    while (idToFormatter.containsKey(Integer.toString(next))) {
                        next++;
                    }
                    final String id = Integer.toString(next);
                    formatterToId.put(formatter, id);
                    idToFormatter.put(id, formatter);
                }
            }
        }

        private String getId(PieceFormatter formatter) {
            return formatterToId.get(formatter);
        }

        public void write(XmlWriter writer,
                          FormattersCatalog catalog) throws IOException {
            init(catalog);
            final String namespace = "https://www.gitlab.com/cdc-java";
            final String schema = "https://www.gitlab.com/cdc-java/applic-publication.xsd";
            writer.beginDocument();
            writer.beginElement(FORMATTERS_CATALOG);
            writer.addDefaultNamespace(namespace);
            writer.addNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            writer.addAttribute("xsi:schemaLocation", namespace + " " + schema);

            writeFormatters(writer);
            writeSettings(writer, catalog);

            writer.endElement();
            writer.endDocument();
        }

        private void writeFormatters(XmlWriter writer) throws IOException {
            final List<String> ids = new ArrayList<>(idToFormatter.keySet());
            Collections.sort(ids);

            writer.beginElement(FORMATTERS);
            for (final String id : ids) {
                writeFormatter(writer, idToFormatter.get(id));
            }
            writer.endElement();
        }

        private void writeFormatter(XmlWriter writer,
                                    PieceFormatter formatter) throws IOException {
            if (formatter instanceof final BooleanValueFormatter f) {
                writeFormatter(writer, f);
            } else if (formatter instanceof final IdentityInformalTextFormatter f) {
                writeFormatter(writer, f);
            } else if (formatter instanceof final IdentitySNameFormatter f) {
                writeFormatter(writer, f);
            } else if (formatter instanceof final IdentityStringValueFormatter f) {
                writeFormatter(writer, f);
            } else if (formatter instanceof final IntegerValueFormatter f) {
                writeFormatter(writer, f);
            } else if (formatter instanceof final MapInformalTextFormatter f) {
                writeFormatter(writer, f);
            } else if (formatter instanceof final MapSNameFormatter f) {
                writeFormatter(writer, f);
            } else if (formatter instanceof final MapStringValueFormatter f) {
                writeFormatter(writer, f);
            } else if (formatter instanceof final RealValueFormatter f) {
                writeFormatter(writer, f);
            } else if (formatter instanceof final SpaceFormatterImpl f) {
                writeFormatter(writer, f);
            } else if (formatter instanceof final SymbolFormatterImpl f) {
                writeFormatter(writer, f);
            } else {
                throw new IllegalArgumentException("Non supporter formatter");
            }
        }

        private void writeFormatter(XmlWriter writer,
                                    BooleanValueFormatter formatter) throws IOException {
            writer.beginElement(BOOLEAN);
            writer.addAttribute(ID, getId(formatter));
            writer.beginElement(FALSE);
            writer.addAttribute(LITERAL, formatter.format(false));
            writer.endElement();
            writer.beginElement(TRUE);
            writer.addAttribute(LITERAL, formatter.format(true));
            writer.endElement();
            writer.endElement();
        }

        private void writeFormatter(XmlWriter writer,
                                    IdentityInformalTextFormatter formatter) throws IOException {
            writer.beginElement(INFORMAL_IDENTITY);
            writer.addAttribute(ID, getId(formatter));
            writer.addAttribute(ESCAPING_MODE, formatter.getEscapingMode());
            writer.endElement();
        }

        private void writeFormatter(XmlWriter writer,
                                    IdentitySNameFormatter formatter) throws IOException {
            writer.beginElement(NAME_IDENTITY);
            writer.addAttribute(ID, getId(formatter));
            writer.addAttribute(ESCAPING_MODE, formatter.getEscapingMode());
            writer.endElement();
        }

        private void writeFormatter(XmlWriter writer,
                                    IdentityStringValueFormatter formatter) throws IOException {
            writer.beginElement(STRING_IDENTITY);
            writer.addAttribute(ID, getId(formatter));
            writer.addAttribute(ESCAPING_MODE, formatter.getEscapingMode());
            writer.endElement();
        }

        private void writeFormatter(XmlWriter writer,
                                    IntegerValueFormatter formatter) throws IOException {
            writer.beginElement(INTEGER);
            writer.addAttribute(ID, getId(formatter));
            writer.addAttribute(FORMAT, formatter.getFormat());
            if (formatter.getLocale() != null) {
                writer.addAttribute(LOCALE, formatter.getLocale().toLanguageTag());
            }
            writer.endElement();
        }

        private void writeFormatter(XmlWriter writer,
                                    MapInformalTextFormatter formatter) throws IOException {
            writer.beginElement(INFORMAL_MAP);
            writer.addAttribute(ID, getId(formatter));
            for (final String key : CollectionUtils.toSortedList(formatter.getMap().keySet())) {
                writer.beginElement(MAP);
                writer.addAttribute(TEXT, key);
                writer.addAttribute(TO, formatter.getMap().get(key));
                writer.endElement();
            }
            writer.endElement();
        }

        private void writeFormatter(XmlWriter writer,
                                    MapSNameFormatter formatter) throws IOException {
            writer.beginElement(NAME_MAP);
            writer.addAttribute(ID, getId(formatter));
            for (final String key : CollectionUtils.toSortedList(formatter.getMap().keySet())) {
                writer.beginElement(MAP);
                writer.addAttribute(NAME, key);
                writer.addAttribute(TO, formatter.getMap().get(key));
                writer.endElement();
            }
            writer.endElement();
        }

        private void writeFormatter(XmlWriter writer,
                                    MapStringValueFormatter formatter) throws IOException {
            writer.beginElement(STRING_MAP);
            writer.addAttribute(ID, getId(formatter));
            for (final String key : CollectionUtils.toSortedList(formatter.getMap().keySet())) {
                writer.beginElement(MAP);
                writer.addAttribute(TEXT, key);
                writer.addAttribute(TO, formatter.getMap().get(key));
                writer.endElement();
            }
            writer.endElement();
        }

        private void writeFormatter(XmlWriter writer,
                                    RealValueFormatter formatter) throws IOException {
            writer.beginElement(REAL);
            writer.addAttribute(ID, getId(formatter));
            writer.addAttribute(FORMAT, formatter.getFormat());
            if (formatter.getLocale() != null) {
                writer.addAttribute(LOCALE, formatter.getLocale().toString());
            }
            writer.endElement();
        }

        private void writeFormatter(XmlWriter writer,
                                    SpaceFormatterImpl formatter) throws IOException {
            writer.beginElement(SPACE);
            writer.addAttribute(ID, getId(formatter));
            writer.addAttribute(SPACING, formatter.getSpacing());
            writer.addAttribute(TEXT, formatter.getText());
            writer.endElement();
        }

        private void writeFormatter(XmlWriter writer,
                                    SymbolFormatterImpl formatter) throws IOException {
            writer.beginElement(SYMBOL);
            writer.addAttribute(ID, getId(formatter));
            writer.addAttribute(SYMBOL_TYPE, formatter.getDefaultSymbolType());
            for (final Symbol symbol : Symbol.values()) {
                if (!formatter.isDefault(symbol)) {
                    writer.beginElement(toXml(symbol));
                    writer.addAttribute(LITERAL, formatter.getText(symbol));
                    writer.addAttribute(REQUIRES_SPACE, formatter.requiresSpace(symbol));
                    writer.endElement();
                }
            }
            writer.endElement();
        }

        private void writeSettings(XmlWriter writer,
                                   FormattersCatalog catalog) throws IOException {
            writer.beginElement(SETTINGS);
            addFormatterRef(writer, SPACE, catalog.getDefaultSpaceFormatter());
            addFormatterRef(writer, SYMBOL, catalog.getDefaultSymbolFormatter());
            addFormatterRef(writer, PREFIX, catalog.getPrefixFormatter());
            addFormatterRef(writer, INFORMAL, catalog.getInformalTextFormatter());
            addFormatterRef(writer, LOCAL_NAME, catalog.getLocalNameFormatter());
            addFormatterRef(writer, BOOLEAN, catalog.getDefaultValueFormatter(TypeKind.BOOLEAN));
            addFormatterRef(writer, INTEGER, catalog.getDefaultValueFormatter(TypeKind.INTEGER));
            addFormatterRef(writer, REAL, catalog.getDefaultValueFormatter(TypeKind.REAL));
            addFormatterRef(writer, STRING, catalog.getDefaultValueFormatter(TypeKind.STRING));
            writeTypesSettings(writer, catalog);
            writeItemsSettings(writer, catalog);
            writer.endElement();
        }

        private void addFormatterRef(XmlWriter writer,
                                     String name,
                                     PieceFormatter formatter) throws IOException {
            if (formatter != null) {
                writer.addAttribute(name, getId(formatter));
            }
        }

        private void writeTypesSettings(XmlWriter writer,
                                        FormattersCatalog catalog) throws IOException {
            writer.beginElement(TYPES);
            for (final Name name : CollectionUtils.toSortedList(catalog.getTypeNames())) {
                writeTypeSettings(writer, catalog, name);
            }
            writer.endElement();
        }

        private void writeTypeSettings(XmlWriter writer,
                                       FormattersCatalog catalog,
                                       Name name) throws IOException {
            writer.beginElement(TYPE);
            writer.addAttribute(NAME, name);
            addFormatterRef(writer, SPACE, catalog.getTypeSpaceFormatterOrNull(name));
            addFormatterRef(writer, SYMBOL, catalog.getTypeSymbolFormatterOrNull(name));
            addFormatterRef(writer, CONTENT, catalog.getTypeValueFormatterOrNull(name));
            writer.endElement();
        }

        private void writeItemsSettings(XmlWriter writer,
                                        FormattersCatalog catalog) throws IOException {
            writer.beginElement(ITEMS);
            for (final Name name : CollectionUtils.toSortedList(catalog.getItemNames())) {
                writeItemSettings(writer, catalog, name);
            }
            writer.endElement();
        }

        private void writeItemSettings(XmlWriter writer,
                                       FormattersCatalog catalog,
                                       Name name) throws IOException {
            writer.beginElement(ITEM);
            writer.addAttribute(NAME, name.getNonEscapedLiteral());
            addFormatterRef(writer, SPACE, catalog.getItemSpaceFormatterOrNull(name));
            addFormatterRef(writer, SYMBOL, catalog.getItemSymbolFormatterOrNull(name));
            addFormatterRef(writer, CONTENT, catalog.getItemValueFormatterOrNull(name));
            writer.endElement();
        }
    }

    public static class StAXLoader extends AbstractStAXLoader<FormattersCatalog> {
        public StAXLoader(FailureReaction reaction) {
            super((reader,
                   systemId) -> new Parser(reader, systemId, reaction));
        }

        private static class Parser extends AbstractStAXParser<FormattersCatalog> {
            protected Parser(XMLStreamReader reader,
                             String systemId,
                             FailureReaction reaction) {
                super(reader, systemId, reaction);
            }

            @Override
            protected FormattersCatalog parse() throws XMLStreamException {
                final String ctx = "parse()";
                trace(ctx);
                // Move to root start tag
                nextTag();

                if (isStartElement(FORMATTERS_CATALOG)) {
                    final FormattersCatalog result = parseCatalog();
                    next();
                    return result;
                } else {
                    throw unexpectedEvent();
                }
            }

            private FormattersCatalog parseCatalog() throws XMLStreamException {
                final String ctx = "parseCatalog()";
                trace(ctx);
                final FormattersCatalog catalog = new FormattersCatalog();
                nextTag();
                parseFormatters(catalog);
                nextTag();
                parseSettings(catalog);
                nextTag();
                return catalog;
            }

            private void parseFormatters(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseFormatters()";
                trace(ctx);
                expectStartElement(ctx, FORMATTERS);
                nextTag();
                while (reader.isStartElement()) {
                    if (isStartElement(BOOLEAN)) {
                        parseBooleanFormatter(catalog);
                    } else if (isStartElement(INFORMAL_IDENTITY)) {
                        parseInformalTextIdentityFormatter(catalog);
                    } else if (isStartElement(NAME_IDENTITY)) {
                        parseNameIdentityFormatter(catalog);
                    } else if (isStartElement(STRING_IDENTITY)) {
                        parseStringIdentityFormatter(catalog);
                    } else if (isStartElement(INTEGER)) {
                        parseIntegerFormatter(catalog);
                    } else if (isStartElement(INFORMAL_MAP)) {
                        parseInformatTextMapFormatter(catalog);
                    } else if (isStartElement(NAME_MAP)) {
                        parseNameMapFormatter(catalog);
                    } else if (isStartElement(STRING_MAP)) {
                        parseStringMapFormatter(catalog);
                    } else if (isStartElement(REAL)) {
                        parseRealFormatter(catalog);
                    } else if (isStartElement(SPACE)) {
                        parseSpaceFormatter(catalog);
                    } else if (isStartElement(SYMBOL)) {
                        parseSymbolFormatter(catalog);
                    } else {
                        unexpectedEvent();
                    }
                    nextTag();
                }
            }

            private void parseBooleanFormatter(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseBooleanFormatter()";
                trace(ctx);
                expectStartElement(ctx, BOOLEAN);
                final String id = getAttributeValue(ID, null);
                nextTag();
                expectStartElement(ctx, FALSE);
                final String falseLiteral = getAttributeValue(LITERAL, null);
                nextTag();
                expectEndElement(ctx, FALSE);
                nextTag();
                expectStartElement(ctx, TRUE);
                final String trueLiteral = getAttributeValue(LITERAL, null);
                nextTag();
                expectEndElement(ctx, TRUE);
                nextTag();
                expectEndElement(ctx, BOOLEAN);

                final BooleanValueFormatter formatter =
                        BooleanValueFormatter.builder()
                                             .map(false, falseLiteral)
                                             .map(true, trueLiteral)
                                             .build();
                catalog.setId(formatter, id);
            }

            private void parseInformalTextIdentityFormatter(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseInformalTextIdentityFormatter()";
                trace(ctx);
                expectStartElement(ctx, INFORMAL_IDENTITY);
                final String id = getAttributeValue(ID, null);
                final EscapingMode escapingMode = getAttributeAsEnum(ESCAPING_MODE, EscapingMode.class, null);
                nextTag();
                expectEndElement(ctx, INFORMAL_IDENTITY);

                final IdentityInformalTextFormatter formatter =
                        IdentityInformalTextFormatter.builder()
                                                     .escapingMode(escapingMode)
                                                     .build();
                catalog.setId(formatter, id);
            }

            private void parseNameIdentityFormatter(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseNameIdentityFormatter()";
                trace(ctx);
                expectStartElement(ctx, NAME_IDENTITY);
                final String id = getAttributeValue(ID, null);
                final EscapingMode escapingMode = getAttributeAsEnum(ESCAPING_MODE, EscapingMode.class, null);
                nextTag();
                expectEndElement(ctx, NAME_IDENTITY);

                final IdentitySNameFormatter formatter =
                        IdentitySNameFormatter.builder()
                                              .escapingMode(escapingMode)
                                              .build();
                catalog.setId(formatter, id);
            }

            private void parseStringIdentityFormatter(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseStringIdentityFormatter()";
                trace(ctx);
                expectStartElement(ctx, STRING_IDENTITY);
                final String id = getAttributeValue(ID, null);
                final EscapingMode escapingMode = getAttributeAsEnum(ESCAPING_MODE, EscapingMode.class, null);
                nextTag();
                expectEndElement(ctx, STRING_IDENTITY);

                final IdentityStringValueFormatter formatter =
                        IdentityStringValueFormatter.builder()
                                                    .escapingMode(escapingMode)
                                                    .build();
                catalog.setId(formatter, id);
            }

            private void parseIntegerFormatter(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseIntegerFormatter()";
                trace(ctx);
                expectStartElement(ctx, INTEGER);
                final String id = getAttributeValue(ID, null);
                final String format = getAttributeValue(FORMAT, null);
                final String locale = getAttributeValue(LOCALE, null);
                nextTag();
                expectEndElement(ctx, INTEGER);

                final IntegerValueFormatter formatter =
                        IntegerValueFormatter.builder()
                                             .format(format)
                                             .locale(locale == null ? null : Locale.forLanguageTag(locale))
                                             .build();
                catalog.setId(formatter, id);
            }

            private void parseInformatTextMapFormatter(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseInformatTextMapFormatter()";
                trace(ctx);
                expectStartElement(ctx, INFORMAL_MAP);
                final String id = getAttributeValue(ID, null);

                final MapSNameFormatter.Builder builder = MapSNameFormatter.builder();

                nextTag();
                while (isStartElement(MAP)) {
                    final String name = getAttributeValue(TEXT, null);
                    final String literal = getAttributeValue(TO, null);
                    builder.map(name, literal);
                    nextTag();
                    nextTag();
                }
                catalog.setId(builder.build(), id);
            }

            private void parseNameMapFormatter(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseNameMapFormatter()";
                trace(ctx);
                expectStartElement(ctx, NAME_MAP);
                final String id = getAttributeValue(ID, null);

                final MapSNameFormatter.Builder builder = MapSNameFormatter.builder();

                nextTag();
                while (isStartElement(MAP)) {
                    final String name = getAttributeValue(NAME, null);
                    final String literal = getAttributeValue(TO, null);
                    builder.map(name, literal);
                    nextTag();
                    nextTag();
                }
                catalog.setId(builder.build(), id);
            }

            private void parseStringMapFormatter(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseStringMapFormatter()";
                trace(ctx);
                expectStartElement(ctx, STRING_MAP);
                final String id = getAttributeValue(ID, null);

                final MapStringValueFormatter.Builder builder = MapStringValueFormatter.builder();

                nextTag();
                while (isStartElement(MAP)) {
                    final String text = getAttributeValue(TEXT, null);
                    final String literal = getAttributeValue(TO, null);
                    builder.map(text, literal);
                    nextTag();
                    nextTag();
                }
                catalog.setId(builder.build(), id);
            }

            private void parseRealFormatter(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseRealFormatter()";
                trace(ctx);
                expectStartElement(ctx, REAL);
                final String id = getAttributeValue(ID, null);
                final String format = getAttributeValue(FORMAT, null);
                final String locale = getAttributeValue(LOCALE, null);
                nextTag();
                expectEndElement(ctx, REAL);

                final RealValueFormatter formatter =
                        RealValueFormatter.builder()
                                          .format(format)
                                          .locale(locale == null ? null : Locale.forLanguageTag(locale))
                                          .build();
                catalog.setId(formatter, id);
            }

            private void parseSpaceFormatter(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseSpaceFormatter()";
                trace(ctx);
                expectStartElement(ctx, SPACE);
                final String id = getAttributeValue(ID, null);
                final Spacing spacing = getAttributeAsEnum(SPACING, Spacing.class, null);
                final String text = getAttributeValue(TEXT, " ");
                nextTag();
                expectEndElement(ctx, SPACE);

                final SpaceFormatterImpl formatter = SpaceFormatterImpl.builder().spacing(spacing).text(text).build();
                catalog.setId(formatter, id);
            }

            private void parseSymbolFormatter(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseSymbolFormatter()";
                trace(ctx);
                expectStartElement(ctx, SYMBOL);
                final String id = getAttributeValue(ID, null);
                final SymbolType symbolType = getAttributeAsEnum(SYMBOL_TYPE, SymbolType.class, null);
                final SymbolFormatterImpl.Builder builder =
                        SymbolFormatterImpl.builder(symbolType);

                nextTag();
                while (reader.isStartElement()) {
                    final String tag = reader.getLocalName();
                    final Symbol symbol = toSymbol(tag);
                    final String literal = getAttributeValue(LITERAL, null);
                    final boolean requiresSpace = getAttributeAsBoolean(REQUIRES_SPACE, false);
                    builder.map(symbol, literal, requiresSpace);
                    nextTag();
                    nextTag();
                }

                catalog.setId(builder.build(), id);
            }

            private void parseSettings(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseSettings()";
                trace(ctx);
                expectStartElement(ctx, SETTINGS);
                final String spaceId = getAttributeValue(SPACE, null);
                final String symbolId = getAttributeValue(SYMBOL, null);
                final String prefixId = getAttributeValue(PREFIX, null);
                final String informalId = getAttributeValue(INFORMAL, null);
                final String localNameId = getAttributeValue(LOCAL_NAME, null);
                final String booleanId = getAttributeValue(BOOLEAN, null);
                final String integerId = getAttributeValue(INTEGER, null);
                final String realId = getAttributeValue(REAL, null);
                final String stringId = getAttributeValue(STRING, null);

                if (catalog.hasFormatter(spaceId)) {
                    catalog.setDefaultSpaceFormatter((SpaceFormatter) catalog.getFormatter(spaceId));
                }
                if (catalog.hasFormatter(symbolId)) {
                    catalog.setDefaultSymbolFormatter((SymbolFormatter) catalog.getFormatter(symbolId));
                }
                if (catalog.hasFormatter(informalId)) {
                    catalog.setInformalTextFormatter((InformalTextFormatter) catalog.getFormatter(informalId));
                }
                if (catalog.hasFormatter(prefixId)) {
                    catalog.setPrefixFormatter((SNameFormatter) catalog.getFormatter(prefixId));
                }
                if (catalog.hasFormatter(localNameId)) {
                    catalog.setLocalNameFormatter((SNameFormatter) catalog.getFormatter(localNameId));
                }
                if (catalog.hasFormatter(booleanId)) {
                    catalog.setDefaultValueFormatter(TypeKind.BOOLEAN, (ValueFormatter) catalog.getFormatter(booleanId));
                }
                if (catalog.hasFormatter(integerId)) {
                    catalog.setDefaultValueFormatter(TypeKind.INTEGER, (ValueFormatter) catalog.getFormatter(integerId));
                }
                if (catalog.hasFormatter(realId)) {
                    catalog.setDefaultValueFormatter(TypeKind.REAL, (ValueFormatter) catalog.getFormatter(realId));
                }
                if (catalog.hasFormatter(stringId)) {
                    catalog.setDefaultValueFormatter(TypeKind.STRING, (ValueFormatter) catalog.getFormatter(stringId));
                }

                nextTag();
                parseTypesSettings(catalog);
                nextTag();
                parseItemsSettings(catalog);
                nextTag();
            }

            private void parseTypesSettings(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseTypesSettings()";
                trace(ctx);
                expectStartElement(ctx, TYPES);
                nextTag();
                while (isStartElement(TYPE)) {
                    parseTypeSettings(catalog);
                    nextTag();
                }
            }

            private void parseTypeSettings(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseTypeSettings()";
                trace(ctx);
                expectStartElement(ctx, TYPE);

                final Name name = Name.of(getAttributeValue(NAME, null));
                final String spaceId = getAttributeValue(SPACE, null);
                final String symbolId = getAttributeValue(SYMBOL, null);
                final String contentId = getAttributeValue(CONTENT, null);

                catalog.setTypeSpaceFormatter(name, (SpaceFormatter) catalog.getFormatterOrNull(spaceId));
                catalog.setTypeSymbolFormatter(name, (SymbolFormatter) catalog.getFormatterOrNull(symbolId));
                catalog.setTypeValueFormatter(name, (ValueFormatter) catalog.getFormatterOrNull(contentId));

                nextTag();
            }

            private void parseItemsSettings(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseTypesSettings()";
                trace(ctx);
                expectStartElement(ctx, ITEMS);
                nextTag();
                while (isStartElement(ITEM)) {
                    parseItemSettings(catalog);
                    nextTag();
                }
            }

            private void parseItemSettings(FormattersCatalog catalog) throws XMLStreamException {
                final String ctx = "parseItemSettings()";
                trace(ctx);
                expectStartElement(ctx, ITEM);

                final Name name = Name.of(getAttributeValue(NAME, null));
                final String spaceId = getAttributeValue(SPACE, null);
                final String symbolId = getAttributeValue(SYMBOL, null);
                final String contentId = getAttributeValue(CONTENT, null);

                catalog.setItemSpaceFormatter(name, (SpaceFormatter) catalog.getFormatterOrNull(spaceId));
                catalog.setItemSymbolFormatter(name, (SymbolFormatter) catalog.getFormatterOrNull(symbolId));
                catalog.setItemContentFormatter(name, (ValueFormatter) catalog.getFormatterOrNull(contentId));

                nextTag();
            }
        }
    }
}