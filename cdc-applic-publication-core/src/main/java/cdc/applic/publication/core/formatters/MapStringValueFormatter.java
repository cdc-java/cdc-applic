package cdc.applic.publication.core.formatters;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import cdc.applic.dictionaries.types.TypeKind;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.content.Value;

/**
 * Formatter of string values based on a map.
 *
 * @author Damien Carbonne
 */
public class MapStringValueFormatter extends AbstractValueFormatter {
    private final Map<String, String> map;

    MapStringValueFormatter(Builder builder) {
        this.map = Collections.unmodifiableMap(new HashMap<>(builder.map));
    }

    public Map<String, String> getMap() {
        return map;
    }

    @Override
    public boolean isSupported(TypeKind kind) {
        return kind == TypeKind.STRING;
    }

    @Override
    public String getText(Value value) {
        if (value instanceof final StringValue v) {
            return map.getOrDefault(v.getNonEscapedLiteral(), v.getProtectedLiteral());
        } else {
            throw nonSupportedValue(value);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private final Map<String, String> map = new HashMap<>();

        Builder() {
        }

        public Builder map(String value,
                           String to) {
            this.map.put(value, to);
            return this;
        }

        public MapStringValueFormatter build() {
            return new MapStringValueFormatter(this);
        }
    }
}