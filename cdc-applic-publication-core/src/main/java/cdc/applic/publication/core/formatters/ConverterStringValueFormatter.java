package cdc.applic.publication.core.formatters;

import java.util.function.UnaryOperator;

import cdc.applic.dictionaries.types.TypeKind;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.content.Value;
import cdc.util.lang.Checks;

/**
 * Formatter of string values base on a converting function.
 *
 * @author Damien Carbonne
 */
public class ConverterStringValueFormatter extends AbstractValueFormatter {
    private final UnaryOperator<String> converter;

    ConverterStringValueFormatter(Builder builder) {
        this.converter = builder.converter;
    }

    @Override
    public boolean isSupported(TypeKind kind) {
        return kind == TypeKind.STRING;
    }

    @Override
    public String getText(Value value) {
        if (value instanceof final StringValue v) {
            return converter.apply(v.getNonEscapedLiteral());
        } else {
            throw nonSupportedValue(value);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private UnaryOperator<String> converter = UnaryOperator.identity();

        Builder() {
        }

        public Builder converter(UnaryOperator<String> converter) {
            Checks.isNotNull(converter, "converter");

            this.converter = converter;
            return this;
        }

        public ConverterStringValueFormatter build() {
            return new ConverterStringValueFormatter(this);
        }
    }
}