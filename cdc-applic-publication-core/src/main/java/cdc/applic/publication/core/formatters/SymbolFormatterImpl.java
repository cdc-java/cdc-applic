package cdc.applic.publication.core.formatters;

import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import cdc.applic.expressions.SymbolType;
import cdc.applic.expressions.ast.NodeKerning;
import cdc.applic.publication.Symbol;
import cdc.applic.publication.SymbolFormatter;
import cdc.util.lang.Checks;

/**
 * Formatter of symbols.
 *
 * @author Damien Carbonne
 */
public class SymbolFormatterImpl implements SymbolFormatter {
    public static final SymbolFormatterImpl SHORT_SYMBOL_FORMATTER =
            builder(SymbolType.SHORT).build();

    public static final SymbolFormatterImpl LONG_SYMBOL_FORMATTER =
            builder(SymbolType.LONG).build();

    public static final SymbolFormatterImpl MATH_SYMBOL_FORMATTER =
            builder(SymbolType.MATH).build();

    private final SymbolType defaultSymbolType;

    /** Map from Symbol to associated text. */
    private final Map<Symbol, String> literals;

    /**
     * Map from Symbol to boolean indicating if spaces
     * are necessary between symbol and a text literal.
     */
    private final Map<Symbol, Boolean> spaces;

    /**
     * Map from Symbol to boolean indicating whether the symbol formating
     * is the default one, inherited from {@link #defaultSymbolType},
     * or was overridden.
     */
    private final Map<Symbol, Boolean> defaults;

    SymbolFormatterImpl(SymbolType defaultSymbolType,
                        Map<Symbol, String> literals,
                        Map<Symbol, Boolean> spaces,
                        Map<Symbol, Boolean> defaults) {
        this.defaultSymbolType = defaultSymbolType;
        this.literals = Collections.unmodifiableMap(new HashMap<>(literals));
        this.spaces = Collections.unmodifiableMap(new HashMap<>(spaces));
        this.defaults = Collections.unmodifiableMap(new HashMap<>(defaults));
    }

    /**
     * @return The {@link SymbolType} that was used to initialize this formatter.
     */
    public SymbolType getDefaultSymbolType() {
        return defaultSymbolType;
    }

    @Override
    public String getText(Symbol symbol) {
        return literals.get(symbol);
    }

    @Override
    public boolean requiresSpace(Symbol symbol) {
        return spaces.get(symbol);
    }

    /**
     * @param symbol The symbol.
     * @return {@code true} if the formatting of {@code symbol} is the default one,
     *         as defined by {@link #getDefaultSymbolType()},
     *         or {@code false} if the default formatting was overridden for {@code symbol}.
     */
    public boolean isDefault(Symbol symbol) {
        return defaults.get(symbol);
    }

    public static Builder builder(SymbolType type) {
        return new Builder(type);
    }

    public static class Builder {
        private SymbolType defaultSymbolType;
        private final Map<Symbol, String> literals = new EnumMap<>(Symbol.class);
        private final Map<Symbol, Boolean> spaces = new EnumMap<>(Symbol.class);
        private final Map<Symbol, Boolean> defaults = new EnumMap<>(Symbol.class);

        Builder(SymbolType type) {
            symbolType(type);
        }

        public Builder symbolType(SymbolType defaultSymbolType) {
            Checks.isNotNull(defaultSymbolType, "defaultSymbolType");

            this.defaultSymbolType = defaultSymbolType;

            mapDefault(Symbol.CLOSE_PAREN,
                       ")",
                       false);
            mapDefault(Symbol.CLOSE_SET,
                       "}",
                       false);
            mapDefault(Symbol.OPEN_PAREN,
                       "(",
                       false);
            mapDefault(Symbol.OPEN_SET,
                       "{",
                       false);
            mapDefault(Symbol.ITEMS_SEPARATOR,
                       ",",
                       false);
            mapDefault(Symbol.LAST_ITEMS_SEPARATOR,
                       ",",
                       false);
            mapDefault(Symbol.TO,
                       "~",
                       false);
            mapDefault(Symbol.PATH_SEPARATOR,
                       ".",
                       false);

            mapDefault(Symbol.AND,
                       NodeKerning.KERNING_AND.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_AND.needsSpace(defaultSymbolType));
            mapDefault(Symbol.EMPTY_SET,
                       NodeKerning.KERNING_EMPTY_SPACE.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_EMPTY_SPACE.needsSpace(defaultSymbolType));
            mapDefault(Symbol.EQUAL,
                       NodeKerning.KERNING_EQUAL.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_EQUAL.needsSpace(defaultSymbolType));
            mapDefault(Symbol.EQUIVALENCE,
                       NodeKerning.KERNING_EQUIV.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_EQUIV.needsSpace(defaultSymbolType));
            mapDefault(Symbol.FALSE,
                       NodeKerning.KERNING_FALSE.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_FALSE.needsSpace(defaultSymbolType));
            mapDefault(Symbol.GREATER,
                       NodeKerning.KERNING_GREATER.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_GREATER.needsSpace(defaultSymbolType));
            mapDefault(Symbol.GREATER_OR_EQUAL,
                       NodeKerning.KERNING_GREATER_OR_EQUAL.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_GREATER_OR_EQUAL.needsSpace(defaultSymbolType));
            mapDefault(Symbol.IMPLICATION,
                       NodeKerning.KERNING_IMPL.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_IMPL.needsSpace(defaultSymbolType));
            mapDefault(Symbol.IN,
                       NodeKerning.KERNING_IN.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_IN.needsSpace(defaultSymbolType));
            mapDefault(Symbol.LESS,
                       NodeKerning.KERNING_LESS.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_LESS.needsSpace(defaultSymbolType));
            mapDefault(Symbol.LESS_OR_EQUAL,
                       NodeKerning.KERNING_LESS_OR_EQUAL.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_LESS_OR_EQUAL.needsSpace(defaultSymbolType));
            mapDefault(Symbol.NEITHER_GREATER_NOR_EQUAL,
                       NodeKerning.KERNING_NEITHER_GREATER_NOR_EQUAL.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_NEITHER_GREATER_NOR_EQUAL.needsSpace(defaultSymbolType));
            mapDefault(Symbol.NEITHER_LESS_NOR_EQUAL,
                       NodeKerning.KERNING_NEITHER_LESS_NOR_EQUAL.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_NEITHER_LESS_NOR_EQUAL.needsSpace(defaultSymbolType));
            mapDefault(Symbol.NOT,
                       NodeKerning.KERNING_NOT.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_NOT.needsSpace(defaultSymbolType));
            mapDefault(Symbol.NOT_EQUAL,
                       NodeKerning.KERNING_NOT_EQUAL.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_NOT_EQUAL.needsSpace(defaultSymbolType));
            mapDefault(Symbol.NOT_GREATER,
                       NodeKerning.KERNING_NOT_GREATER.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_NOT_GREATER.needsSpace(defaultSymbolType));
            mapDefault(Symbol.NOT_IN,
                       NodeKerning.KERNING_NOT_IN.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_NOT_IN.needsSpace(defaultSymbolType));
            mapDefault(Symbol.NOT_LESS,
                       NodeKerning.KERNING_NOT_LESS.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_NOT_LESS.needsSpace(defaultSymbolType));
            mapDefault(Symbol.OR,
                       NodeKerning.KERNING_OR.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_OR.needsSpace(defaultSymbolType));
            mapDefault(Symbol.TRUE,
                       NodeKerning.KERNING_TRUE.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_TRUE.needsSpace(defaultSymbolType));
            mapDefault(Symbol.XOR,
                       NodeKerning.KERNING_XOR.getSymbol(defaultSymbolType),
                       NodeKerning.KERNING_XOR.needsSpace(defaultSymbolType));
            return this;
        }

        private Builder map(Symbol symbol,
                            String literal,
                            boolean requiresSpace,
                            boolean def) {
            Checks.isNotNull(symbol, "kind");
            Checks.isNotNull(literal, "symbol");

            this.literals.put(symbol, literal);
            this.spaces.put(symbol, requiresSpace);
            this.defaults.put(symbol, def);
            return this;
        }

        private Builder mapDefault(Symbol symbol,
                                   String literal,
                                   boolean requiresSpace) {
            return map(symbol, literal, requiresSpace, true);
        }

        public Builder map(Symbol symbol,
                           String literal,
                           boolean requiresSpace) {
            return map(symbol, literal, requiresSpace, false);
        }

        public SymbolFormatterImpl build() {
            return new SymbolFormatterImpl(defaultSymbolType, literals, spaces, defaults);
        }
    }
}