package cdc.applic.publication.core.formatters;

import cdc.applic.dictionaries.types.TypeKind;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.EscapingMode;
import cdc.util.lang.Checks;

/**
 * Formatter of string values that returns the original value.
 *
 * @author Damien Carbonne
 */
public class IdentityStringValueFormatter extends AbstractValueFormatter {
    public static final IdentityStringValueFormatter PROTECTED =
            builder().escapingMode(EscapingMode.PROTECTED)
                     .build();

    public static final IdentityStringValueFormatter NON_ESCAPED =
            builder().escapingMode(EscapingMode.NON_ESCAPED)
                     .build();

    public static final IdentityStringValueFormatter ESCAPED =
            builder().escapingMode(EscapingMode.ESCAPED)
                     .build();

    public static final IdentityStringValueFormatter DEFAULT = PROTECTED;

    private final EscapingMode escapingMode;

    IdentityStringValueFormatter(Builder builder) {
        this.escapingMode = builder.escapingMode;
    }

    public EscapingMode getEscapingMode() {
        return escapingMode;
    }

    @Override
    public boolean isSupported(TypeKind kind) {
        return kind == TypeKind.STRING;
    }

    @Override
    public String getText(Value value) {
        if (value instanceof final StringValue v) {
            return v.getLiteral(escapingMode);
        } else {
            throw nonSupportedValue(value);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private EscapingMode escapingMode = EscapingMode.PROTECTED;

        Builder() {
        }

        public Builder escapingMode(EscapingMode escapingMode) {
            Checks.isNotNull(escapingMode, "escapingMode");

            this.escapingMode = escapingMode;
            return this;
        }

        public IdentityStringValueFormatter build() {
            return new IdentityStringValueFormatter(this);
        }
    }
}