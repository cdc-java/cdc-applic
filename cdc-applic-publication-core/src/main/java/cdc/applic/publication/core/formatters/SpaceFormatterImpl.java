package cdc.applic.publication.core.formatters;

import cdc.applic.expressions.Spacing;
import cdc.applic.publication.SpaceFormatter;
import cdc.applic.publication.Symbol;
import cdc.applic.publication.SymbolFormatter;
import cdc.util.lang.Checks;

/**
 * Formatter of spaces.
 *
 * @author Damien Carbonne
 */
public class SpaceFormatterImpl implements SpaceFormatter {
    private final Spacing spacing;
    private final String text;

    public static final SpaceFormatterImpl NARROW_SPACE_FORMATTER = builder().spacing(Spacing.NARROW).text(" ").build();
    public static final SpaceFormatterImpl WIDE_SPACE_FORMATTER = builder().spacing(Spacing.WIDE).text(" ").build();

    protected SpaceFormatterImpl(Builder builder) {
        this.spacing = builder.spacing;
        this.text = builder.text;
    }

    public Spacing getSpacing() {
        return spacing;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public boolean needsSpace(Symbol symbol,
                              boolean contextRequiresSpace,
                              SymbolFormatter symbolFormatter) {
        Checks.isNotNull(symbol, "symbol");

        if (SpaceFormatter.requiresSpace(symbol, contextRequiresSpace, symbolFormatter)) {
            return true;
        } else if (spacing == Spacing.NARROW) {
            return false;
        } else { // WIDE
            return symbol.needsWideSpace();
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Spacing spacing = Spacing.NARROW;
        private String text = " ";

        public Builder spacing(Spacing spacing) {
            this.spacing = spacing;
            return this;
        }

        public Builder text(String text) {
            this.text = text;
            return this;
        }

        public SpaceFormatterImpl build() {
            return new SpaceFormatterImpl(this);
        }
    }
}