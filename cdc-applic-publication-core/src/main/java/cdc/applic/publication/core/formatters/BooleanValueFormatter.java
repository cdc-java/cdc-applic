package cdc.applic.publication.core.formatters;

import cdc.applic.dictionaries.types.TypeKind;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.Value;
import cdc.applic.publication.Symbol;
import cdc.applic.publication.SymbolFormatter;
import cdc.util.lang.Checks;

/**
 * Formatter of boolean values.
 * <p>
 * It can associate a text to each value.
 *
 * @author Damien Carbonne
 */
public class BooleanValueFormatter extends AbstractValueFormatter {
    private final String[] literals = new String[2];

    public static final BooleanValueFormatter SHORT_BOOLEAN_FORMATTER =
            builder().symbolFormatter(SymbolFormatterImpl.SHORT_SYMBOL_FORMATTER).build();

    public static final BooleanValueFormatter LONG_BOOLEAN_FORMATTER =
            builder().symbolFormatter(SymbolFormatterImpl.LONG_SYMBOL_FORMATTER).build();

    public static final BooleanValueFormatter MATH_BOOLEAN_FORMATTER =
            builder().symbolFormatter(SymbolFormatterImpl.MATH_SYMBOL_FORMATTER).build();

    public static final BooleanValueFormatter DEFAULT = SHORT_BOOLEAN_FORMATTER;

    BooleanValueFormatter(Builder builder) {
        literals[0] = Checks.isNotNull(builder.falseLiteral, "falseLiteral");
        literals[1] = Checks.isNotNull(builder.trueLiteral, "falseLiteral");
    }

    @Override
    public String getText(Value value) {
        if (value instanceof final BooleanValue v) {
            return format(v.getValue());
        } else {
            throw nonSupportedValue(value);
        }
    }

    public String format(boolean value) {
        return value ? literals[1] : literals[0];
    }

    @Override
    public boolean isSupported(TypeKind kind) {
        return kind == TypeKind.BOOLEAN;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String falseLiteral = "false";
        private String trueLiteral = "true";

        Builder() {
        }

        public Builder symbolFormatter(SymbolFormatter symbolFormatter) {
            this.falseLiteral = symbolFormatter.getText(Symbol.FALSE);
            this.trueLiteral = symbolFormatter.getText(Symbol.TRUE);
            return this;
        }

        public Builder map(boolean value,
                           String to) {
            if (value) {
                this.trueLiteral = to;
            } else {
                this.falseLiteral = to;
            }
            return this;
        }

        public BooleanValueFormatter build() {
            return new BooleanValueFormatter(this);
        }
    }
}