package cdc.applic.publication.core.formatters;

import cdc.applic.dictionaries.DictionaryUtils;
import cdc.applic.dictionaries.Registry;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AbstractOperatorNode;
import cdc.applic.expressions.ast.AbstractPropertyNode;
import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.AbstractValueNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.ImplicationNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.InformalNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotInNode;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.XorNode;
import cdc.applic.expressions.ast.visitors.AbstractAnalyzer;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.SName;
import cdc.applic.publication.ExpressionFormatter;
import cdc.applic.publication.FormattingHandler;
import cdc.applic.publication.NameValidity;
import cdc.applic.publication.Symbol;
import cdc.util.lang.Checks;
import cdc.util.lang.Procedure;

/**
 * Infix formatter of expressions.
 *
 * @author Damien Carbonne
 */
public class InfixFormatter implements ExpressionFormatter {
    protected final Registry registry;
    protected final FormattersCatalog catalog;

    public InfixFormatter(Registry registry,
                          FormattersCatalog catalog) {
        Checks.isNotNull(registry, "registry");
        Checks.isNotNull(catalog, "catalog");

        this.registry = registry;
        this.catalog = catalog;
    }

    public Registry getRegistry() {
        return registry;
    }

    public FormattersCatalog getCatalog() {
        return catalog;
    }

    @Override
    public void format(Expression expression,
                       FormattingHandler handler) {
        final Node ast = ConvertToNary.execute(expression.getRootNode(),
                                               ConvertToNary.Variant.WHEN_NECESSARY);
        final Visitor visitor = new Visitor(handler);
        handler.beginExpression(expression);
        ast.accept(visitor);
        handler.endExpression();
    }

    private class Visitor extends AbstractAnalyzer {
        private final FormattingHandler handler;

        public Visitor(FormattingHandler handler) {
            this.handler = handler;
        }

        private void addParentheses(AbstractOperatorNode node,
                                    Node child,
                                    Procedure acceptor) {
            final boolean paren = node.needParentheses(child);
            addParentheses(paren, acceptor);
        }

        private void addParentheses(boolean paren,
                                    Procedure acceptor) {
            if (paren) {
                catalog.appendSymbol(handler, Symbol.OPEN_PAREN);
            }
            acceptor.invoke();
            if (paren) {
                catalog.appendSymbol(handler, Symbol.CLOSE_PAREN);
            }
        }

        private NameValidity getPrefixValidity(Name name) {
            final SName prefix = name.getPrefix();
            if (prefix == null) {
                return NameValidity.VALID;
            } else {
                return registry.isDeclaredPrefix(prefix) ? NameValidity.VALID : NameValidity.INVALID;
            }
        }

        @Override
        public Void visitLeaf(AbstractLeafNode node) {
            if (node instanceof final RefNode n) {
                if (registry.hasAlias(n.getName())) {
                    catalog.appendValidAlias(handler, n.getName());
                } else if (DictionaryUtils.isAvailableBooleanProperty(n.getName(), registry)) {
                    catalog.appendValidProperty(handler, n.getName());
                } else {
                    catalog.appendInvalidRef(handler, n.getName(), getPrefixValidity(n.getName()));
                }
            } else if (node instanceof TrueNode) {
                catalog.appendSymbol(handler, Symbol.TRUE);
            } else if (node instanceof FalseNode) {
                catalog.appendSymbol(handler, Symbol.FALSE);
            } else if (node instanceof final InformalNode n) {
                catalog.appendInformalText(handler, n.getText());
            } else {
                final AbstractPropertyNode p = (AbstractPropertyNode) node;
                final Type type;
                if (registry.hasProperty(p.getName())) {
                    final Property property = registry.getProperty(p.getName());
                    type = property.getType();
                    catalog.appendValidProperty(handler, p.getName());
                } else {
                    type = null;
                    catalog.appendInvalidProperty(handler, p.getName(), getPrefixValidity(p.getName()));
                }
                if (node instanceof final AbstractValueNode n) {
                    final Symbol symbol = Symbol.fromComparisonOperator(n.getComparisonOperator());
                    catalog.appendSpace(handler, symbol, true);
                    catalog.appendSymbol(handler, symbol);
                    catalog.appendSpace(handler, symbol, true);
                    catalog.appendValue(handler, n.getValue(), type);
                } else if (node instanceof final InNode n) {
                    catalog.appendSpace(handler, Symbol.IN, true);
                    catalog.appendSymbol(handler, Symbol.IN);
                    catalog.appendSpace(handler, Symbol.IN, true);
                    catalog.appendSet(handler, n.getSet(), type);
                } else {
                    final NotInNode n = (NotInNode) node;
                    catalog.appendSpace(handler, Symbol.NOT_IN, true);
                    catalog.appendSymbol(handler, Symbol.NOT_IN);
                    catalog.appendSpace(handler, Symbol.NOT_IN, true);
                    catalog.appendSet(handler, n.getSet(), type);
                }
            }
            return null;
        }

        @Override
        public Void visitUnary(AbstractUnaryNode node) {
            final boolean paren = node.needParentheses(node.getAlpha());
            catalog.appendSymbol(handler, Symbol.NOT);
            catalog.appendSpace(handler, Symbol.NOT, false);
            addParentheses(paren,
                           () -> node.getAlpha().accept(this));
            return null;
        }

        @Override
        public Void visitBinary(AbstractBinaryNode node) {
            final Symbol symbol;
            if (node instanceof AndNode) {
                symbol = Symbol.AND;
            } else if (node instanceof OrNode) {
                symbol = Symbol.OR;
            } else if (node instanceof ImplicationNode) {
                symbol = Symbol.IMPLICATION;
            } else if (node instanceof XorNode) {
                symbol = Symbol.XOR;
            } else {
                symbol = Symbol.EQUIVALENCE;
            }

            addParentheses(node,
                           node.getAlpha(),
                           () -> node.getAlpha().accept(this));
            catalog.appendSpace(handler, symbol, true);
            catalog.appendSymbol(handler, symbol);
            catalog.appendSpace(handler, symbol, true);
            addParentheses(node,
                           node.getBeta(),
                           () -> node.getBeta().accept(this));
            return null;
        }

        @Override
        public Void visitNary(AbstractNaryNode node) {
            final Symbol symbol = node instanceof NaryAndNode
                    ? Symbol.AND
                    : Symbol.OR;

            for (int index = 0; index < node.getChildrenCount(); index++) {
                if (index > 0) {
                    catalog.appendSpace(handler, symbol, true);
                    catalog.appendSymbol(handler, symbol);
                    catalog.appendSpace(handler, symbol, true);
                }
                final Node sub = node.getChildAt(index);
                addParentheses(node,
                               sub,
                               () -> sub.accept(this));
            }
            return null;
        }
    }
}