package cdc.applic.publication.core.formatters;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.dictionaries.types.TypeKind;
import cdc.applic.expressions.content.Range;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.InformalText;
import cdc.applic.expressions.literals.Name;
import cdc.applic.publication.FormattingHandler;
import cdc.applic.publication.InformalTextFormatter;
import cdc.applic.publication.NameValidity;
import cdc.applic.publication.PieceFormatter;
import cdc.applic.publication.SNameFormatter;
import cdc.applic.publication.SpaceFormatter;
import cdc.applic.publication.Symbol;
import cdc.applic.publication.SymbolFormatter;
import cdc.applic.publication.ValueFormatter;
import cdc.applic.publication.ValueValidity;
import cdc.util.lang.Checks;
import cdc.util.lang.NotFoundException;
import cdc.util.strings.StringUtils;

/**
 * Catalog of Formatters organized hierarchically.
 * <p>
 * Prefixes, local names and informal texts have global formatters.<br>
 * Spaces, symbols and values have default global formatters that can be overridden
 * for a type or a name dictionary item (alias and property).
 *
 * @author Damien Carbonne
 */
public class FormattersCatalog {
    private static final String FORMATTER = "formatter";
    private static final String ID = "id";
    private static final String INFORMAL = "informal";
    private static final String ITEM = "item";
    private static final String KIND = "kind";
    private static final String NAME = "name";
    private static final String RANGE = "range";
    private static final String SET = "set";
    private static final String SYMBOL = "symbol";
    private static final String TYPE = "type";
    private static final String VALUE = "value";

    /** Global prefix formatter. */
    private SNameFormatter prefixFormatter = IdentitySNameFormatter.PROTECTED;
    /** Global item local name formatter. */
    private SNameFormatter localNameFormatter = IdentitySNameFormatter.PROTECTED;
    /** Global informal text formatter. */
    private InformalTextFormatter informalTextFormatter = IdentityInformalTextFormatter.ESCAPED;

    /** Default Space formatter. Can be overridden for a type or item. */
    private SpaceFormatter defaultSpaceFormatter = SpaceFormatterImpl.NARROW_SPACE_FORMATTER;
    /** Default symbol formatter. Can be overridden for a type or item. */
    private SymbolFormatter defaultSymbolFormatter = SymbolFormatterImpl.SHORT_SYMBOL_FORMATTER;
    /** Default content formatters. Can be overridden for a type or named dictionary item. */
    private final Map<TypeKind, ValueFormatter> defaultValueFormatters = new EnumMap<>(TypeKind.class);

    /** Specific types formatters. */
    private final Map<Name, Bucket> typeFormatters = new HashMap<>();
    /** Specific NamedDItems formatters. */
    private final Map<Name, Bucket> itemFormatters = new HashMap<>();

    private final Map<String, PieceFormatter> idToFormatter = new HashMap<>();
    private final Map<PieceFormatter, String> formatterToId = new HashMap<>();

    /**
     * Set of specific formatters.
     *
     * @author Damien Carbonne
     */
    private static class Bucket {
        SpaceFormatter spaceFormatter;
        SymbolFormatter symbolFormatter;
        ValueFormatter valueFormatter;

        public Bucket() {
            super();
        }
    }

    private static final Bucket EMPTY = new Bucket();

    /**
     * Returns the Bucket associated to a Type name. If none exists, one Bucket is created.
     *
     * @param name The Type name.
     * @return The Bucket associated to Type named {@code name}.
     */
    private Bucket getTypeBucket(Name name) {
        return typeFormatters.computeIfAbsent(name, t -> new Bucket());
    }

    /**
     * Returns the Bucket associated to a Type name, if it exists, {@link #EMPTY} otherwise.
     *
     * @param name The Type name.
     * @return The Bucket associated to {@code name} or {@link #EMPTY}.
     */
    private Bucket getTypeBucketOrDefault(Name name) {
        return typeFormatters.getOrDefault(name, EMPTY);
    }

    private Bucket getItemBucket(Name name) {
        return itemFormatters.computeIfAbsent(name, t -> new Bucket());
    }

    private Bucket getItemBucketOrDefault(Name name) {
        return itemFormatters.getOrDefault(name, EMPTY);
    }

    public FormattersCatalog() {
        setDefaultValueFormatter(TypeKind.BOOLEAN, BooleanValueFormatter.DEFAULT);
        setDefaultValueFormatter(TypeKind.INTEGER, IntegerValueFormatter.DEFAULT);
        setDefaultValueFormatter(TypeKind.REAL, RealValueFormatter.DEFAULT);
        setDefaultValueFormatter(TypeKind.STRING, IdentityStringValueFormatter.DEFAULT);
    }

    public void setId(PieceFormatter formatter,
                      String id) {
        Checks.isNotNull(formatter, FORMATTER);
        Checks.isNotNull(id, ID);

        if (formatterToId.containsKey(formatter)) {
            throw new IllegalArgumentException("'" + formatter + "' already mapped to '" + formatterToId.get(formatter) + "'");
        }
        if (idToFormatter.containsKey(id)) {
            throw new IllegalArgumentException("'" + id + "' is already mapped from '" + formatter + "'");
        }

        formatterToId.put(formatter, id);
        idToFormatter.put(id, formatter);
    }

    public Object getIdOrNull(PieceFormatter formatter) {
        return formatterToId.get(formatter);
    }

    public String getId(PieceFormatter formatter) {
        final String result = formatterToId.get(formatter);
        if (result == null) {
            throw new NotFoundException("'" + formatter + "' has no associated id");
        }
        return result;
    }

    public PieceFormatter getFormatterOrNull(String id) {
        return idToFormatter.get(id);
    }

    public boolean hasFormatter(String id) {
        return idToFormatter.containsKey(id);
    }

    public PieceFormatter getFormatter(String id) {
        Checks.isNotNull(id, "id");

        final PieceFormatter result = idToFormatter.get(id);
        if (result == null) {
            throw new NotFoundException("'" + id + "' has no associated formatter");
        }
        return result;
    }

    /**
     * Sets the default SpaceFormatter.
     *
     * @param formatter The SpaceFormatter.
     */
    public void setDefaultSpaceFormatter(SpaceFormatter formatter) {
        Checks.isNotNull(formatter, FORMATTER);

        this.defaultSpaceFormatter = formatter;
    }

    /**
     * @return The default space formatter.
     */
    public SpaceFormatter getDefaultSpaceFormatter() {
        return defaultSpaceFormatter;
    }

    /**
     * Sets the default SymbolFormatter.
     *
     * @param formatter The {@link SymbolFormatter}.
     */
    public void setDefaultSymbolFormatter(SymbolFormatter formatter) {
        Checks.isNotNull(formatter, FORMATTER);

        this.defaultSymbolFormatter = formatter;
    }

    /**
     * @return The default symbol formatter.
     */
    public SymbolFormatter getDefaultSymbolFormatter() {
        return defaultSymbolFormatter;
    }

    /**
     * Sets the prefix formatter.
     *
     * @param formatter The {@link SNameFormatter} for prefixes.
     */
    public void setPrefixFormatter(SNameFormatter formatter) {
        Checks.isNotNull(formatter, FORMATTER);

        this.prefixFormatter = formatter;
    }

    /**
     * @return The prefix formatter.
     */
    public SNameFormatter getPrefixFormatter() {
        return prefixFormatter;
    }

    /**
     * Sets the item local name formatter.
     *
     * @param formatter The {@link SNameFormatter} for local names.
     */
    public void setLocalNameFormatter(SNameFormatter formatter) {
        Checks.isNotNull(formatter, FORMATTER);

        this.localNameFormatter = formatter;
    }

    /**
     * @return The item local name formatter.
     */
    public SNameFormatter getLocalNameFormatter() {
        return localNameFormatter;
    }

    /**
     * Sets the informal text formatter.
     *
     * @param formatter The {@link InformalTextFormatter}.
     */
    public void setInformalTextFormatter(InformalTextFormatter formatter) {
        Checks.isNotNull(formatter, FORMATTER);

        this.informalTextFormatter = formatter;
    }

    /**
     * @return The informal text formatter.
     */
    public InformalTextFormatter getInformalTextFormatter() {
        return informalTextFormatter;
    }

    public void setDefaultValueFormatter(TypeKind kind,
                                         ValueFormatter formatter) {
        Checks.isNotNull(kind, KIND);
        Checks.isNotNull(formatter, FORMATTER);
        Checks.isTrue(formatter.isSupported(kind), "Formatter not compliant with type kind");

        this.defaultValueFormatters.put(kind, formatter);
    }

    public ValueFormatter getDefaultValueFormatter(TypeKind kind) {
        Checks.isNotNull(kind, KIND);

        return defaultValueFormatters.get(kind);
    }

    public Set<Name> getTypeNames() {
        return typeFormatters.keySet();
    }

    /**
     * Sets the {@link SpaceFormatter} dedicated to a {@link Type}.
     *
     * @param type The Type.
     * @param formatter The SpaceFormatter (may be {@code null}).
     */
    public void setTypeSpaceFormatter(Type type,
                                      SpaceFormatter formatter) {
        Checks.isNotNull(type, TYPE);

        getTypeBucket(type.getName()).spaceFormatter = formatter;
    }

    /**
     * Sets the {@link SpaceFormatter} dedicated to a {@link Type}.
     *
     * @param name The Type name.
     * @param formatter The SpaceFormatter (may be {@code null}).
     */
    public void setTypeSpaceFormatter(Name name,
                                      SpaceFormatter formatter) {
        Checks.isNotNull(name, NAME);

        getTypeBucket(name).spaceFormatter = formatter;
    }

    /**
     * @param name The Type name.
     * @return The {@link SpaceFormatter} dedicated to the Type named {@code name} or {@code null}.
     */
    public SpaceFormatter getTypeSpaceFormatterOrNull(Name name) {
        Checks.isNotNull(name, NAME);

        return getTypeBucketOrDefault(name).spaceFormatter;
    }

    public SpaceFormatter getEffectiveTypeSpaceFormatter(Type type) {
        Checks.isNotNull(type, TYPE);

        SpaceFormatter result = getTypeSpaceFormatterOrNull(type.getName());
        if (result == null) {
            result = getDefaultSpaceFormatter();
        }
        return result;
    }

    /**
     * Sets the {@link SymbolFormatter} dedicated to a {@link Type}.
     *
     * @param type The Type.
     * @param formatter The SymbolFormatter (may be {@code null}).
     */
    public void setTypeSymbolFormatter(Type type,
                                       SymbolFormatter formatter) {
        Checks.isNotNull(type, TYPE);

        getItemBucket(type.getName()).symbolFormatter = formatter;
    }

    /**
     * Sets the {@link SymbolFormatter} dedicated to a {@link Type}.
     *
     * @param name The Type name.
     * @param formatter The SymbolFormatter (may be {@code null}).
     */
    public void setTypeSymbolFormatter(Name name,
                                       SymbolFormatter formatter) {
        Checks.isNotNull(name, NAME);

        getTypeBucket(name).symbolFormatter = formatter;
    }

    /**
     * @param name The Type name.
     * @return The {@link SymbolFormatter} dedicated to the Type named {@code name} or {@code null}.
     */
    public SymbolFormatter getTypeSymbolFormatterOrNull(Name name) {
        return getTypeBucketOrDefault(name).symbolFormatter;
    }

    public SymbolFormatter getEffectiveTypeSymbolFormatter(Type type) {
        SymbolFormatter result = getTypeSymbolFormatterOrNull(type.getName());
        if (result == null) {
            result = getDefaultSymbolFormatter();
        }
        return result;
    }

    /**
     * Sets the {@link ValueFormatter} dedicated to a {@link Type}.
     *
     * @param type The Type.
     * @param formatter The ValueFormatter (may be {@code null}).
     */
    public void setTypeValueFormatter(Type type,
                                      ValueFormatter formatter) {
        Checks.isNotNull(type, TYPE);

        getTypeBucket(type.getName()).valueFormatter = formatter;
    }

    /**
     * Sets the {@link ValueFormatter} dedicated to a {@link Type}.
     *
     * @param name The Type name.
     * @param formatter The ValueFormatter (may be {@code null}).
     */
    public void setTypeValueFormatter(Name name,
                                      ValueFormatter formatter) {
        Checks.isNotNull(name, NAME);

        getTypeBucket(name).valueFormatter = formatter;
    }

    /**
     * @param name The Type name.
     * @return The {@link ValueFormatter} dedicated to the Type named {@code name} or {@code null}.
     */
    public ValueFormatter getTypeValueFormatterOrNull(Name name) {
        return getTypeBucketOrDefault(name).valueFormatter;
    }

    public ValueFormatter getEffectiveTypeValueFormatter(Type type) {
        Checks.isNotNull(type, TYPE);

        ValueFormatter result = getTypeValueFormatterOrNull(type.getName());
        if (result == null) {
            result = getDefaultValueFormatter(Type.getTypeKind(type));
        }
        return result;
    }

    public Set<Name> getItemNames() {
        return itemFormatters.keySet();
    }

    /**
     * Sets the {@link SpaceFormatter} dedicated to a {@link NamedDItem}
     *
     * @param item The NamedDItem.
     * @param formatter The SpaceFormatter (may be {@code null}).
     */
    public void setItemSpaceFormatter(NamedDItem item,
                                      SpaceFormatter formatter) {
        Checks.isNotNull(item, ITEM);

        getItemBucket(item.getName()).spaceFormatter = formatter;
    }

    /**
     * Sets the {@link SpaceFormatter} dedicated to a {@link NamedDItem}
     *
     * @param name The NamedDItem name.
     * @param formatter The SpaceFormatter (may be {@code null}).
     */
    public void setItemSpaceFormatter(Name name,
                                      SpaceFormatter formatter) {
        Checks.isNotNull(name, NAME);

        getItemBucket(name).spaceFormatter = formatter;
    }

    /**
     * @param name The NamedDItem name.
     * @return The {@link SpaceFormatter} dedicated to the NamedDItem named {@code name} or {@code null}.
     */
    public SpaceFormatter getItemSpaceFormatterOrNull(Name name) {
        return getItemBucketOrDefault(name).spaceFormatter;
    }

    public SpaceFormatter getEffectiveItemSpaceFormatter(NamedDItem item) {
        SpaceFormatter result = getItemSpaceFormatterOrNull(item.getName());
        if (result == null) {
            if (item instanceof final Property property) {
                final Type type = property.getType();
                result = getEffectiveTypeSpaceFormatter(type);
            } else {
                result = getDefaultSpaceFormatter();
            }
        }
        return result;
    }

    /**
     * Sets the {@link SymbolFormatter} dedicated to a {@link NamedDItem}.
     *
     * @param item The NamedDItem.
     * @param formatter The SymbolFormatter (may be {@code null}).
     */
    public void setItemSymbolFormatter(NamedDItem item,
                                       SymbolFormatter formatter) {
        Checks.isNotNull(item, ITEM);

        getItemBucket(item.getName()).symbolFormatter = formatter;
    }

    /**
     * Sets the {@link SymbolFormatter} dedicated to a {@link NamedDItem}.
     *
     * @param name The NamedDItem name.
     * @param formatter The SymbolFormatter (may be {@code null}).
     */
    public void setItemSymbolFormatter(Name name,
                                       SymbolFormatter formatter) {
        Checks.isNotNull(name, NAME);

        getItemBucket(name).symbolFormatter = formatter;
    }

    /**
     * @param name The NamedDItem name.
     * @return The {@link SymbolFormatter} dedicated to the NamedDItem named {@code name} or {@code null}.
     */
    public SymbolFormatter getItemSymbolFormatterOrNull(Name name) {
        return getItemBucketOrDefault(name).symbolFormatter;
    }

    public SymbolFormatter getEffectiveItemSymbolFormatter(NamedDItem item) {
        if (item instanceof final Property property) {
            final Type type = property.getType();
            return getEffectiveTypeSymbolFormatter(type);
        } else {
            return getDefaultSymbolFormatter();
        }
    }

    /**
     * Sets the {@link ValueFormatter} dedicated to a {@link NamedDItem}.
     *
     * @param item The NamedDItem.
     * @param formatter The ValueFormatter (may be {@code null}).
     */
    public void setItemValueFormatter(NamedDItem item,
                                      ValueFormatter formatter) {
        Checks.isNotNull(item, ITEM);

        getItemBucket(item.getName()).valueFormatter = formatter;
    }

    /**
     * Sets the {@link ValueFormatter} dedicated to a {@link NamedDItem}.
     *
     * @param name The NamedDItem name.
     * @param formatter The ValueFormatter (may be {@code null}).
     */
    public void setItemContentFormatter(Name name,
                                        ValueFormatter formatter) {
        Checks.isNotNull(name, NAME);

        getItemBucket(name).valueFormatter = formatter;
    }

    /**
     * @param name The NamedDItem name.
     * @return The {@link ValueFormatter} dedicated to the NamedDItem named {@code name} or {@code null}.
     */
    public ValueFormatter getItemValueFormatterOrNull(Name name) {
        return getItemBucketOrDefault(name).valueFormatter;
    }

    public ValueFormatter getEffectiveItemValueFormatter(NamedDItem item) {
        Checks.isNotNull(item, ITEM);

        ValueFormatter result = getItemValueFormatterOrNull(item.getName());
        if (result == null) {
            if (item instanceof final Property property) {
                final Type type = property.getType();
                result = getEffectiveTypeValueFormatter(type);
            } else {
                result = IdentityStringValueFormatter.DEFAULT;
            }
        }
        return result;
    }

    public FormattersCatalog appendSpace(FormattingHandler handler,
                                         Symbol symbol,
                                         boolean contextRequiresSpace) {
        Checks.isNotNull(symbol, SYMBOL);

        final SymbolFormatter symbolFormatter = getDefaultSymbolFormatter();
        final SpaceFormatter spaceFormatter = getDefaultSpaceFormatter();
        if (spaceFormatter.needsSpace(symbol, contextRequiresSpace, symbolFormatter)) {
            handler.appendSpace(spaceFormatter.getText());
        }
        return this;
    }

    public FormattersCatalog appendSymbol(FormattingHandler handler,
                                          Symbol symbol) {
        Checks.isNotNull(symbol, SYMBOL);

        final SymbolFormatter symbolFormatter = getDefaultSymbolFormatter();
        handler.appendSymbol(symbol, symbolFormatter.getText(symbol));
        return this;
    }

    private void appendPrefix(FormattingHandler handler,
                              Name name,
                              NameValidity validity) {
        Checks.isNotNull(name, NAME);

        if (name.hasPrefix()) {
            final SNameFormatter formatter = getPrefixFormatter();
            final String text = formatter.getText(name.getPrefix());
            if (!StringUtils.isNullOrEmpty(text)) {
                handler.appendPrefix(name, text, validity);
                appendSymbol(handler, Symbol.PATH_SEPARATOR);
            }
        }
    }

    public FormattersCatalog appendValidProperty(FormattingHandler handler,
                                                 Name name) {
        Checks.isNotNull(name, NAME);

        appendPrefix(handler, name, NameValidity.VALID);
        final SNameFormatter formatter = getLocalNameFormatter();
        handler.appendPropertyLocalName(name, formatter.getText(name.getLocal()), NameValidity.VALID);
        return this;
    }

    public FormattersCatalog appendInvalidProperty(FormattingHandler handler,
                                                   Name name,
                                                   NameValidity prefixValidity) {
        Checks.isNotNull(name, NAME);

        appendPrefix(handler, name, prefixValidity);
        final SNameFormatter formatter = getLocalNameFormatter();
        handler.appendPropertyLocalName(name, formatter.getText(name.getLocal()), NameValidity.INVALID);
        return this;
    }

    public FormattersCatalog appendInvalidRef(FormattingHandler handler,
                                              Name name,
                                              NameValidity prefixValidity) {
        Checks.isNotNull(name, NAME);

        appendPrefix(handler, name, prefixValidity);
        final SNameFormatter formatter = getLocalNameFormatter();
        handler.appendInvalidRefLocalName(name, formatter.getText(name.getLocal()));
        return this;
    }

    public FormattersCatalog appendValidAlias(FormattingHandler handler,
                                              Name name) {
        Checks.isNotNull(name, NAME);

        appendPrefix(handler, name, NameValidity.VALID);
        final SNameFormatter formatter = getLocalNameFormatter();
        handler.appendAliasLocalName(name, formatter.getText(name.getLocal()));
        return this;
    }

    public FormattersCatalog appendValue(FormattingHandler handler,
                                         Value value,
                                         Type type) {
        Checks.isNotNull(value, VALUE);

        if (type == null) {
            handler.appendValue(value, value.getNonEscapedLiteral(), ValueValidity.UNKNOWN);
        } else {
            final ValueFormatter valueFormatter = getEffectiveTypeValueFormatter(type);
            if (type.isCompliant(value)) {
                handler.appendValue(value, valueFormatter.getText(value), ValueValidity.VALID);
            } else {
                handler.appendValue(value, value.getNonEscapedLiteral(), ValueValidity.INVALID);
            }
        }
        return this;
    }

    public FormattersCatalog appendRange(FormattingHandler handler,
                                         Range range,
                                         Type type) {
        Checks.isNotNull(range, RANGE);

        appendValue(handler, range.getMin(), type);
        appendSymbol(handler, Symbol.TO);
        appendValue(handler, range.getMax(), type);
        return this;
    }

    public FormattersCatalog appendItem(FormattingHandler handler,
                                        SItem item,
                                        Type type) {
        Checks.isNotNull(item, ITEM);

        if (item instanceof final Value value) {
            appendValue(handler, value, type);
        } else {
            appendRange(handler, (Range) item, type);
        }
        return this;
    }

    public FormattersCatalog appendSetContent(FormattingHandler handler,
                                              SItemSet set,
                                              Type type) {
        Checks.isNotNull(set, SET);

        final List<SItem> items = new ArrayList<>(set.getItems());
        final int size = items.size();
        for (int index = 0; index < size; index++) {
            if (index > 0) {
                final Symbol symbol;
                if (index == size - 1) {
                    symbol = Symbol.LAST_ITEMS_SEPARATOR;
                } else {
                    symbol = Symbol.ITEMS_SEPARATOR;
                }
                appendSymbol(handler, symbol);
                appendSpace(handler, symbol, true);
            }
            appendItem(handler, items.get(index), type);
        }
        return this;
    }

    public FormattersCatalog appendSet(FormattingHandler handler,
                                       SItemSet set,
                                       Type type) {
        Checks.isNotNull(set, SET);

        if (set.isEmpty()) {
            appendSymbol(handler, Symbol.EMPTY_SET);
        } else {
            appendSymbol(handler, Symbol.OPEN_SET);
            appendSetContent(handler, set, type);
            appendSymbol(handler, Symbol.CLOSE_SET);
        }
        return this;
    }

    public FormattersCatalog appendInformalText(FormattingHandler handler,
                                                InformalText informal) {
        Checks.isNotNull(informal, INFORMAL);

        final InformalTextFormatter formatter = getInformalTextFormatter();
        final String text = formatter.getText(informal);
        handler.appendInformalText(informal, text);

        return this;
    }
}