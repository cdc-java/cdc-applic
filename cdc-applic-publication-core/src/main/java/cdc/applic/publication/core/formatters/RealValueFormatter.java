package cdc.applic.publication.core.formatters;

import java.util.Locale;

import cdc.applic.dictionaries.types.TypeKind;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.Value;
import cdc.util.lang.Checks;

/**
 * Formatter of real values.
 *
 * @author Damien Carbonne
 */
public class RealValueFormatter extends AbstractValueFormatter {
    private final String format;
    private final Locale locale;

    public static final RealValueFormatter DEFAULT = builder().build();

    RealValueFormatter(Builder builder) {
        this.format = builder.format;
        this.locale = builder.locale;
    }

    public String getFormat() {
        return format;
    }

    public Locale getLocale() {
        return locale;
    }

    @Override
    public boolean isSupported(TypeKind kind) {
        return kind == TypeKind.REAL;
    }

    @Override
    public String getText(Value value) {
        if (value instanceof final RealValue v) {
            return format(v.getNumber());
        } else {
            throw nonSupportedValue(value);
        }
    }

    public String format(double value) {
        return locale == null
                ? String.format(getFormat(), value)
                : String.format(locale, getFormat(), value);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String format = "%f";
        private Locale locale = null;

        Builder() {
        }

        public Builder format(String format) {
            Checks.isNotNull(format, "format");
            this.format = format;
            return this;
        }

        public Builder locale(Locale locale) {
            this.locale = locale;
            return this;
        }

        public RealValueFormatter build() {
            return new RealValueFormatter(this);
        }
    }
}