package cdc.applic.publication.core.formatters;

import cdc.applic.expressions.literals.EscapingMode;
import cdc.applic.expressions.literals.SName;
import cdc.applic.publication.SNameFormatter;
import cdc.util.lang.Checks;

/**
 * Formatter of SNames that returns the original name.
 *
 * @author Damien Carbonne
 */
public class IdentitySNameFormatter implements SNameFormatter {
    public static final IdentitySNameFormatter PROTECTED =
            builder().escapingMode(EscapingMode.PROTECTED)
                     .build();

    public static final IdentitySNameFormatter NON_ESCAPED =
            builder().escapingMode(EscapingMode.NON_ESCAPED)
                     .build();

    public static final IdentitySNameFormatter ESCAPED =
            builder().escapingMode(EscapingMode.ESCAPED)
                     .build();

    public static final IdentitySNameFormatter DEFAULT = PROTECTED;

    private final EscapingMode escapingMode;

    IdentitySNameFormatter(Builder builder) {
        this.escapingMode = builder.escapingMode;
    }

    public EscapingMode getEscapingMode() {
        return escapingMode;
    }

    @Override
    public String getText(SName name) {
        return name.getLiteral(escapingMode);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private EscapingMode escapingMode = EscapingMode.PROTECTED;

        Builder() {
        }

        public Builder escapingMode(EscapingMode escapingMode) {
            Checks.isNotNull(escapingMode, "escapingMode");

            this.escapingMode = escapingMode;
            return this;
        }

        public IdentitySNameFormatter build() {
            return new IdentitySNameFormatter(this);
        }
    }
}