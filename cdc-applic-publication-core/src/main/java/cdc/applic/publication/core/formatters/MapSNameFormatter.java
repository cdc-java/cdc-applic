package cdc.applic.publication.core.formatters;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import cdc.applic.expressions.literals.SName;
import cdc.applic.publication.SNameFormatter;

/**
 * Formatter of SNames based on a map.
 *
 * @author Damien Carbonne
 */
public class MapSNameFormatter implements SNameFormatter {
    private final Map<String, String> map;

    MapSNameFormatter(Builder builder) {
        this.map = Collections.unmodifiableMap(new HashMap<>(builder.map));
    }

    public Map<String, String> getMap() {
        return map;
    }

    @Override
    public String getText(SName name) {
        return map.getOrDefault(name.getNonEscapedLiteral(), name.getProtectedLiteral());
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private final Map<String, String> map = new HashMap<>();

        Builder() {
        }

        public Builder map(String literal,
                           String to) {
            this.map.put(literal, to);
            return this;
        }

        public MapSNameFormatter build() {
            return new MapSNameFormatter(this);
        }
    }
}