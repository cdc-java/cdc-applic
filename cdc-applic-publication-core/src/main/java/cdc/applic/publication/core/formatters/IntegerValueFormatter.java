package cdc.applic.publication.core.formatters;

import java.util.Locale;

import cdc.applic.dictionaries.types.TypeKind;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.Value;
import cdc.util.lang.Checks;

/**
 * Formatter of integer values.
 *
 * @author Damien Carbonne
 */
public class IntegerValueFormatter extends AbstractValueFormatter {
    private final String format;
    private final Locale locale;

    public static final IntegerValueFormatter DEFAULT = builder().build();

    IntegerValueFormatter(Builder builder) {
        this.format = builder.format;
        this.locale = builder.locale;
    }

    public String getFormat() {
        return format;
    }

    public Locale getLocale() {
        return locale;
    }

    @Override
    public boolean isSupported(TypeKind kind) {
        return kind == TypeKind.INTEGER;
    }

    @Override
    public String getText(Value value) {
        if (value instanceof final IntegerValue v) {
            return format(v.getNumber());
        } else {
            // This handles null
            throw nonSupportedValue(value);
        }
    }

    public String format(int value) {
        return locale == null
                ? String.format(getFormat(), value)
                : String.format(locale, getFormat(), value);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String format = "%d";
        private Locale locale = null;

        Builder() {
        }

        public Builder format(String format) {
            Checks.isNotNull(format, "format");
            this.format = format;
            return this;
        }

        public Builder locale(Locale locale) {
            this.locale = locale;
            return this;
        }

        public IntegerValueFormatter build() {
            return new IntegerValueFormatter(this);
        }
    }
}