package cdc.applic.publication.core.formatters;

import cdc.applic.expressions.content.Value;
import cdc.applic.publication.ValueFormatter;

/**
 * Abstract value formatter.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractValueFormatter implements ValueFormatter {
    protected IllegalArgumentException nonSupportedValue(Value value) {
        return new IllegalArgumentException("Non supported value " + value);
    }
}