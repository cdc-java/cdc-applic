package cdc.applic.publication.core.formatters;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import cdc.applic.expressions.literals.InformalText;
import cdc.applic.publication.InformalTextFormatter;
import cdc.util.lang.Checks;

/**
 * Formatter of InformalTexts based on a map.
 *
 * @author Damien Carbonne
 */
public class MapInformalTextFormatter implements InformalTextFormatter {
    private final Map<String, String> map;

    MapInformalTextFormatter(Builder builder) {
        this.map = Collections.unmodifiableMap(new HashMap<>(builder.map));
    }

    public Map<String, String> getMap() {
        return map;
    }

    @Override
    public String getText(InformalText text) {
        Checks.isNotNull(text, "text");
        return map.getOrDefault(text.getNonEscapedText(), text.getEscapedText());
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private final Map<String, String> map = new HashMap<>();

        Builder() {
        }

        public Builder map(String text,
                           String to) {
            this.map.put(text, to);
            return this;
        }

        public MapInformalTextFormatter build() {
            return new MapInformalTextFormatter(this);
        }
    }
}