package cdc.applic.publication.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.literals.EscapingMode;
import cdc.applic.publication.core.formatters.IdentityStringValueFormatter;

class StringIdentityFormatterTest {
    @Test
    void test() {
        final IdentityStringValueFormatter formatter = IdentityStringValueFormatter.DEFAULT;

        assertEquals("Hello", formatter.getText(StringValue.of("Hello")));
        assertEquals(EscapingMode.PROTECTED, formatter.getEscapingMode());

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         formatter.getText(null);
                     });
    }
}