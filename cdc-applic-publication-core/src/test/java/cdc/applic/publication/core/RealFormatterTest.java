package cdc.applic.publication.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Locale;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.RealValue;
import cdc.applic.publication.core.formatters.RealValueFormatter;

class RealFormatterTest {
    @Test
    void testDefault() {
        final RealValueFormatter formatter = RealValueFormatter.builder().locale(Locale.ENGLISH).build();

        assertEquals("%f", formatter.getFormat());

        assertEquals("10.000000", formatter.getText(RealValue.of(10.0)));
        assertEquals("-10.000000", formatter.getText(RealValue.of(-10.0)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         formatter.getText(null);
                     });
    }

    @Test
    void testNullLocale() {
        final RealValueFormatter formatter = RealValueFormatter.builder().build();
        // Coverage
        formatter.getText(RealValue.of(10.0));
    }

    @Test
    void testAll() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         RealValueFormatter.builder()
                                             .format(null)
                                             .build();
                     });

        final RealValueFormatter formatter =
                RealValueFormatter.builder()
                                    .format("%4.2f")
                                    .locale(Locale.ENGLISH)
                                    .build();

        assertEquals(Locale.ENGLISH, formatter.getLocale());
        assertEquals("%4.2f", formatter.getFormat());

        assertEquals("10.00", formatter.getText(RealValue.of(10.0)));
        assertEquals("1234.00", formatter.getText(RealValue.of(1234.0)));
        assertEquals("-10.00", formatter.getText(RealValue.of(-10.0)));
    }
}