package cdc.applic.publication.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.literals.InformalText;
import cdc.applic.publication.core.formatters.MapInformalTextFormatter;

class InformalTextMapFormatterTest {
    @Test
    void testEmpty() {
        final MapInformalTextFormatter formatter = MapInformalTextFormatter.builder().build();

        assertTrue(formatter.getMap().isEmpty());

        assertEquals("$Hello$", formatter.getText(InformalText.of("Hello")));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         formatter.getText(null);
                     });

    }

    @Test
    void testMap() {
        final MapInformalTextFormatter formatter =
                MapInformalTextFormatter.builder()
                                        .map("Hello", "HELLO")
                                        .build();

        assertEquals(1, formatter.getMap().size());
        assertEquals("HELLO", formatter.getText(InformalText.of("Hello")));
        assertEquals("$World$", formatter.getText(InformalText.of("World")));
    }
}