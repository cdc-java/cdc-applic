package cdc.applic.publication.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.StringValue;
import cdc.applic.publication.core.formatters.MapStringValueFormatter;

class StringMapFormatterTest {
    @Test
    void testEmpty() {
        final MapStringValueFormatter formatter = MapStringValueFormatter.builder().build();

        assertTrue(formatter.getMap().isEmpty());

        assertEquals("Hello", formatter.getText(StringValue.of("Hello")));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         formatter.getText(null);
                     });

    }

    @Test
    void testMap() {
        final MapStringValueFormatter formatter =
                MapStringValueFormatter.builder()
                                         .map("Hello", "HELLO")
                                         .build();

        assertEquals(1, formatter.getMap().size());
        assertEquals("HELLO", formatter.getText(StringValue.of("Hello")));
        assertEquals("World", formatter.getText(StringValue.of("World")));
    }
}