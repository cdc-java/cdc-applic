package cdc.applic.publication.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Locale;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.publication.core.formatters.IntegerValueFormatter;

class IntegerFormatterTest {
    @Test
    void test() {
        final IntegerValueFormatter formatter = IntegerValueFormatter.builder().build();

        assertEquals("%d", formatter.getFormat());

        assertEquals("10", formatter.getText(IntegerValue.of(10)));
        assertEquals("-10", formatter.getText(IntegerValue.of(-10)));
        assertEquals("10", formatter.getText(IntegerValue.of(10)));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         formatter.getText(null);
                     });
    }

    @Test
    void testAll() {
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         IntegerValueFormatter.builder()
                                              .format(null)
                                              .build();
                     });

        final IntegerValueFormatter formatter =
                IntegerValueFormatter.builder()
                                     .format("%+04d")
                                     .locale(Locale.ENGLISH)
                                     .build();

        assertEquals(Locale.ENGLISH, formatter.getLocale());
        assertEquals("%+04d", formatter.getFormat());

        assertEquals("+010", formatter.getText(IntegerValue.of(10)));
        assertEquals("+1234", formatter.getText(IntegerValue.of(1234)));
        assertEquals("-010", formatter.getText(IntegerValue.of(-10)));
    }
}