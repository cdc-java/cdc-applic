package cdc.applic.publication.core;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.types.TypeKind;
import cdc.applic.expressions.Spacing;
import cdc.applic.expressions.SymbolType;
import cdc.applic.expressions.literals.EscapingMode;
import cdc.applic.publication.InformalTextFormatter;
import cdc.applic.publication.SNameFormatter;
import cdc.applic.publication.SpaceFormatter;
import cdc.applic.publication.Symbol;
import cdc.applic.publication.SymbolFormatter;
import cdc.applic.publication.ValueFormatter;
import cdc.applic.publication.core.formatters.BooleanValueFormatter;
import cdc.applic.publication.core.formatters.FormattersCatalog;
import cdc.applic.publication.core.formatters.IdentityInformalTextFormatter;
import cdc.applic.publication.core.formatters.IdentitySNameFormatter;
import cdc.applic.publication.core.formatters.IdentityStringValueFormatter;
import cdc.applic.publication.core.formatters.IntegerValueFormatter;
import cdc.applic.publication.core.formatters.MapSNameFormatter;
import cdc.applic.publication.core.formatters.RealValueFormatter;
import cdc.applic.publication.core.formatters.SpaceFormatterImpl;
import cdc.applic.publication.core.formatters.SymbolFormatterImpl;
import cdc.applic.publication.core.formatters.io.FormattersCatalogXml;
import cdc.io.xml.XmlWriter;
import cdc.util.lang.FailureReaction;

class FormattersCatalogXmlTest {
    @Test
    void test() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        registry.booleanType().name("Bool").build();

        registry.property().name("B").type("Bool").ordinal(0).build();

        final FormattersCatalog catalog1 = new FormattersCatalog();

        final SpaceFormatter sf = SpaceFormatterImpl.builder()
                                                    .spacing(Spacing.NARROW)
                                                    .text("...")
                                                    .build();
        catalog1.setDefaultSpaceFormatter(sf);
        catalog1.setId(sf, "SPACE");

        final SymbolFormatter symf = SymbolFormatterImpl.builder(SymbolType.MATH)
                                                        .map(Symbol.AND, "AND", true)
                                                        .build();
        catalog1.setDefaultSymbolFormatter(symf);
        catalog1.setId(symf, "SYMBOL");

        final ValueFormatter bf = BooleanValueFormatter.builder()
                                                       .map(false, "FFF")
                                                       .map(true, "TTT")
                                                       .build();

        catalog1.setDefaultValueFormatter(TypeKind.BOOLEAN, bf);
        catalog1.setId(bf, "BOOLEAN");

        final ValueFormatter intf = IntegerValueFormatter.builder()
                                                         .format("%d")
                                                         .locale(Locale.ENGLISH)
                                                         .build();
        catalog1.setDefaultValueFormatter(TypeKind.INTEGER, intf);
        catalog1.setId(intf, "INTEGER");

        final ValueFormatter rf = RealValueFormatter.builder()
                                                    .format("%f")
                                                    .locale(Locale.ENGLISH)
                                                    .build();
        catalog1.setDefaultValueFormatter(TypeKind.REAL, rf);
        catalog1.setId(rf, "REAL");

        final ValueFormatter idcf =
                IdentityStringValueFormatter.builder()
                                            .escapingMode(EscapingMode.NON_ESCAPED)
                                            .build();
        catalog1.setDefaultValueFormatter(TypeKind.STRING, idcf);
        catalog1.setId(idcf, "ID_STRING");

        final SNameFormatter np =
                IdentitySNameFormatter.builder()
                                      .escapingMode(EscapingMode.NON_ESCAPED)
                                      .build();
        catalog1.setPrefixFormatter(np);
        catalog1.setId(np, "ID_PREFIX");

        final SNameFormatter nf =
                IdentitySNameFormatter.builder()
                                      .escapingMode(EscapingMode.NON_ESCAPED)
                                      .build();
        catalog1.setLocalNameFormatter(nf);
        catalog1.setId(nf, "ID_LOCAL_NAME");

        final SNameFormatter nf1 =
                MapSNameFormatter.builder()
                                 .map("X", "XXX")
                                 .build();
        catalog1.setLocalNameFormatter(nf1);
        catalog1.setId(nf1, "MAP_LOCAL_NAME");

        final InformalTextFormatter itf =
                IdentityInformalTextFormatter.builder()
                                             .escapingMode(EscapingMode.ESCAPED)
                                             .build();
        catalog1.setInformalTextFormatter(itf);
        catalog1.setId(itf, "ID_INFORMAL");

        assertTrue(catalog1.hasFormatter("ID_LOCAL_NAME"));
        assertTrue(catalog1.hasFormatter("MAP_LOCAL_NAME"));
        assertTrue(catalog1.hasFormatter("ID_INFORMAL"));

        catalog1.setTypeValueFormatter(registry.getType("Bool"), bf);
        catalog1.setItemValueFormatter(registry.getItem("B"), bf);

        final File file1 = new File("target/catalog1.xml");
        final FormattersCatalogXml.Printer printer = new FormattersCatalogXml.Printer();
        try (XmlWriter writer = new XmlWriter(file1)) {
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
            printer.write(writer, catalog1);
        }
        final FormattersCatalogXml.StAXLoader loader = new FormattersCatalogXml.StAXLoader(FailureReaction.WARN);
        final FormattersCatalog catalog2 = loader.load(file1);
        final File file2 = new File("target/catalog2.xml");
        try (XmlWriter writer = new XmlWriter(file2)) {
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
            printer.write(writer, catalog2);
        }

        assertTrue(true);
    }
}