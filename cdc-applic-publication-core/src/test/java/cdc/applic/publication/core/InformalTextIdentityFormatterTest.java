package cdc.applic.publication.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.literals.EscapingMode;
import cdc.applic.expressions.literals.InformalText;
import cdc.applic.publication.core.formatters.IdentityInformalTextFormatter;

class InformalTextIdentityFormatterTest {
    @Test
    void test() {
        assertEquals(EscapingMode.ESCAPED, IdentityInformalTextFormatter.ESCAPED.getEscapingMode());
        assertEquals(EscapingMode.NON_ESCAPED, IdentityInformalTextFormatter.NON_ESCAPED.getEscapingMode());

        assertEquals("$Hello$", IdentityInformalTextFormatter.DEFAULT.getText(InformalText.of("Hello")));
    }
}