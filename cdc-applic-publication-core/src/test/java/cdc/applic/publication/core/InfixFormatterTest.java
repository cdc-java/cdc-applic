package cdc.applic.publication.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.types.TypeKind;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.SymbolType;
import cdc.applic.publication.ExpressionFormatter;
import cdc.applic.publication.SNameFormatter;
import cdc.applic.publication.Symbol;
import cdc.applic.publication.core.formatters.BooleanValueFormatter;
import cdc.applic.publication.core.formatters.FormattersCatalog;
import cdc.applic.publication.core.formatters.InfixFormatter;
import cdc.applic.publication.core.formatters.MapSNameFormatter;
import cdc.applic.publication.core.formatters.SpaceFormatterImpl;
import cdc.applic.publication.core.formatters.SymbolFormatterImpl;

class InfixFormatterTest {
    private static final Logger LOGGER = LogManager.getLogger(InfixFormatterTest.class);

    private static void check(ExpressionFormatter formatter,
                              String expression,
                              String expected) {
        final String actual = formatter.formatAsString(new Expression(expression));
        LOGGER.debug("check(" + expression + "): " + actual);
        assertEquals(expected, actual);
    }

    @Test
    void testNarrowMath() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();

        registry.booleanType().name("Boolean").build();
        registry.integerType().name("Rank").frozen(true).domain("1~999").build();
        registry.enumeratedType().name("Version").frozen(false).literals("V1", "V2", "V3").build();

        registry.property().name("Rank").type("Rank").ordinal(0).build();
        registry.property().name("Version").type("Version").ordinal(1).build();
        registry.property().name("SB1").type("Boolean").ordinal(101).build();
        registry.property().name("SB2").type("Boolean").ordinal(102).build();
        registry.property().name("SB3").type("Boolean").ordinal(103).build();
        registry.property().name("SB4").type("Boolean").ordinal(104).build();

        final FormattersCatalog catalog = new FormattersCatalog();

        catalog.setDefaultSpaceFormatter(SpaceFormatterImpl.NARROW_SPACE_FORMATTER);
        catalog.setDefaultSymbolFormatter(SymbolFormatterImpl.MATH_SYMBOL_FORMATTER);
        catalog.setDefaultValueFormatter(TypeKind.BOOLEAN, BooleanValueFormatter.MATH_BOOLEAN_FORMATTER);

        final InfixFormatter formatter = new InfixFormatter(registry, catalog);

        assertEquals(registry, formatter.getRegistry());
        assertEquals(catalog, formatter.getCatalog());

        check(formatter, "true", "⊤");
        check(formatter, "false", "⊥");
        check(formatter, "Rank<:{1~10}", "Rank∈{1~10}");
        check(formatter, "Rank!<:{1~10}", "Rank∉{1~10}");
        check(formatter, "true or false", "⊤∨⊥");
        check(formatter, "true and false", "⊤∧⊥");
        check(formatter, "true or false or false", "⊤∨⊥∨⊥");
        check(formatter, "true and false and false", "⊤∧⊥∧⊥");
        check(formatter, "true or false or false and true", "⊤∨⊥∨⊥∧⊤");
        check(formatter, "(true or false) and true", "(⊤∨⊥)∧⊤");
    }

    @Test
    void testWideMath() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();

        registry.booleanType().name("Boolean").build();
        registry.integerType().name("Rank").frozen(true).domain("1~999").build();
        registry.enumeratedType().name("Version").frozen(false).literals("V1", "V2", "V3").build();

        registry.property().name("Rank").type("Rank").ordinal(0).build();
        registry.property().name("Version").type("Version").ordinal(1).build();
        registry.property().name("SB1").type("Boolean").ordinal(101).build();
        registry.property().name("SB2").type("Boolean").ordinal(102).build();
        registry.property().name("SB3").type("Boolean").ordinal(103).build();
        registry.property().name("SB4").type("Boolean").ordinal(104).build();

        final FormattersCatalog catalog = new FormattersCatalog();
        catalog.setDefaultSpaceFormatter(SpaceFormatterImpl.WIDE_SPACE_FORMATTER);
        catalog.setDefaultSymbolFormatter(SymbolFormatterImpl.MATH_SYMBOL_FORMATTER);
        catalog.setDefaultValueFormatter(TypeKind.BOOLEAN, BooleanValueFormatter.MATH_BOOLEAN_FORMATTER);

        final InfixFormatter formatter = new InfixFormatter(registry, catalog);

        check(formatter, "true", "⊤");
        check(formatter, "false", "⊥");
        check(formatter, "Rank<:{1~10}", "Rank ∈ {1~10}");
        check(formatter, "Rank!<:{1~10}", "Rank ∉ {1~10}");
        check(formatter, "true or false", "⊤ ∨ ⊥");
        check(formatter, "true and false", "⊤∧⊥");
        check(formatter, "true or false or false", "⊤ ∨ ⊥ ∨ ⊥");
        check(formatter, "true and false and false", "⊤∧⊥∧⊥");
        check(formatter, "true or false or false and true", "⊤ ∨ ⊥ ∨ ⊥∧⊤");
        check(formatter, "SB1 or SB2", "SB1 ∨ SB2");
        check(formatter, "SB1 = true", "SB1 = ⊤");
        check(formatter, "SB1 != true", "SB1 ≠ ⊤");
    }

    @Test
    void testDense() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").prefix("R").build();

        registry.booleanType().name("Boolean").build();
        registry.integerType().name("Rank").frozen(true).domain("1~999").build();
        registry.enumeratedType().name("Version").frozen(false).literals("V1", "V2", "V3").build();
        registry.patternType().name("PartNumber").frozen(false).pattern("PN[1-9][0-9]*").build();

        registry.property().name("Rank").type("Rank").ordinal(0).build();
        registry.property().name("Version").type("Version").ordinal(1).build();
        registry.property().name("SB1").type("Boolean").ordinal(101).build();
        registry.property().name("SB2").type("Boolean").ordinal(102).build();
        registry.property().name("SB3").type("Boolean").ordinal(103).build();
        registry.property().name("SB4").type("Boolean").ordinal(104).build();
        registry.property().name("PartNumber").type("PartNumber").ordinal(0).build();

        final FormattersCatalog catalog = new FormattersCatalog();
        final SymbolFormatterImpl symbolFormatter =
                SymbolFormatterImpl.builder(SymbolType.SHORT)
                                   .map(Symbol.AND, "&", false)
                                   .map(Symbol.OR, ",", false)
                                   .map(Symbol.TO, "-", false)
                                   .map(Symbol.CLOSE_SET, ")", false)
                                   .map(Symbol.OPEN_SET, "(", false)
                                   .map(Symbol.IN, "", false)
                                   .map(Symbol.NOT_IN, "^", false)
                                   .map(Symbol.NOT, "^", false)
                                   .build();

        final SNameFormatter prefixFormatter = MapSNameFormatter.builder()
                                                                .map("R", null)
                                                                .build();

        final SNameFormatter localNameFormatter = MapSNameFormatter.builder()
                                                                   .map("Rank", "")
                                                                   .map("PartNumber", "")
                                                                   .build();

        catalog.setPrefixFormatter(prefixFormatter);
        catalog.setLocalNameFormatter(localNameFormatter);
        catalog.setDefaultSpaceFormatter(SpaceFormatterImpl.NARROW_SPACE_FORMATTER);
        catalog.setDefaultSymbolFormatter(symbolFormatter);
        catalog.setDefaultValueFormatter(TypeKind.BOOLEAN, BooleanValueFormatter.SHORT_BOOLEAN_FORMATTER);

        final InfixFormatter formatter = new InfixFormatter(registry, catalog);

        check(formatter, "true", "true");
        check(formatter, "false", "false");
        check(formatter, "Rank<:{1~10}", "(1-10)");
        check(formatter, "Rank!<:{1~10}", "^(1-10)");
        check(formatter, "Rank<:{1~10,12}", "(1-10,12)");
        check(formatter, "Rank<:{1~10,12} & not SB2", "(1-10,12)&^SB2");
        check(formatter, "Rank<:{1~10,12} & not SB2 or Rank<:{20~999} & SB1", "(1-10,12)&^SB2,(20-999)&SB1");
        check(formatter, "PartNumber <:{PN1, PN2}", "(PN1,PN2)");
    }
}