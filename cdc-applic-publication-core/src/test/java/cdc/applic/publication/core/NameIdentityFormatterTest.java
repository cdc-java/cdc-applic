package cdc.applic.publication.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.literals.EscapingMode;
import cdc.applic.publication.core.formatters.IdentitySNameFormatter;

class NameIdentityFormatterTest {
    @Test
    void test() {
        assertEquals(EscapingMode.PROTECTED, IdentitySNameFormatter.PROTECTED.getEscapingMode());
    }
}