package cdc.applic.publication.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.publication.core.formatters.BooleanValueFormatter;
import cdc.applic.publication.core.formatters.SymbolFormatterImpl;

class BooleanFormatterTest {
    @Test
    void test() {
        final BooleanValueFormatter formatter =
                BooleanValueFormatter.builder()
                                       .symbolFormatter(SymbolFormatterImpl.SHORT_SYMBOL_FORMATTER)
                                       .build();

        assertEquals("true", formatter.getText(BooleanValue.TRUE));
        assertEquals("false", formatter.getText(BooleanValue.FALSE));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         formatter.getText(null);
                     });
    }

    @Test
    void testOnOff() {
        final String falseLiteral = "OFF";
        final String trueLiteral = "ON";
        final BooleanValueFormatter formatter =
                BooleanValueFormatter.builder()
                                       .map(false, falseLiteral)
                                       .map(true, trueLiteral)
                                       .build();

        assertEquals(trueLiteral, formatter.getText(BooleanValue.TRUE));
        assertEquals(falseLiteral, formatter.getText(BooleanValue.FALSE));
    }
}