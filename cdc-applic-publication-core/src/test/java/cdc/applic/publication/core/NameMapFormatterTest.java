package cdc.applic.publication.core;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.publication.core.formatters.MapSNameFormatter;

class NameMapFormatterTest {
    @Test
    void test() {
        final MapSNameFormatter formatter = MapSNameFormatter.builder().build();
        assertTrue(formatter.getMap().size() == 0);
    }
}