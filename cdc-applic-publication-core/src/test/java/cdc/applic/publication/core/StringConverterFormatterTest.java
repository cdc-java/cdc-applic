package cdc.applic.publication.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.content.StringValue;
import cdc.applic.publication.core.formatters.ConverterStringValueFormatter;

class StringConverterFormatterTest {
    @Test
    void testDefault() {
        final ConverterStringValueFormatter formatter =
                ConverterStringValueFormatter.builder().build();

        assertEquals("Hello", formatter.getText(StringValue.of("Hello")));

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         formatter.getText(null);
                     });
    }

    @Test
    void testToUpperCase() {
        final ConverterStringValueFormatter formatter =
                ConverterStringValueFormatter.builder()
                                               .converter(String::toUpperCase)
                                               .build();

        assertEquals("HELLO", formatter.getText(StringValue.of("Hello")));
    }
}