package cdc.applic.simplification;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.QualifiedValue;

/**
 * Interface implemented by objects that can simplify an {@link Expression}.
 * <p>
 * When expressions are computed, they can rapidly become complex and contain useless parts.<br>
 * Simplification may help in that case.
 * <p>
 * Simplification is a complex task, and result may be non satisfying for humans.<br>
 * In certain cases, several solutions are possible, and producing the 'expected' one is challenging.
 * <p>
 * Simplification result depends on and is controlled by {@link SimplifierFeatures}.
 *
 * @author Damien Carbonne
 */
public interface Simplifier {
    /**
     * @return The {@link DictionaryHandle}.
     */
    public DictionaryHandle getDictionaryHandle();

    /**
     * Returns the simplification of a {@link Node}.
     *
     * @param node The node.
     * @param features The features to apply.
     * @return The simplification of {@code node}, applying {@code features}.
     */
    public QualifiedValue<Node> simplify(Node node,
                                         SimplifierFeatures features);

    /**
     * Returns the simplification of an {@link Expression}.
     *
     * @param expression The expression.
     * @param features The features to apply.
     * @return The simplification of {@code expression}, applying {@code features}.
     */
    public QualifiedValue<Expression> simplify(Expression expression,
                                               SimplifierFeatures features);
}