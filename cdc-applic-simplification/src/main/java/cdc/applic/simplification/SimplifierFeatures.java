package cdc.applic.simplification;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.proofs.ProverFeatures;
import cdc.util.lang.Checks;

/**
 * Simplifier Features.
 *
 * @author Damien Carbonne
 */
public class SimplifierFeatures {
    public static final SimplifierFeatures ALL_HINTS_INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES =
            builder().allHints()
                     .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)
                     .build();

    public static final SimplifierFeatures ALL_HINTS_NO_FAIL_INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES =
            builder().allHints()
                     .clear(Hint.FAIL_ON_NON_REMOVABLE_NEGATION)
                     .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)
                     .build();

    public static final SimplifierFeatures ALL_HINTS_EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES =
            builder().allHints()
                     .proverFeatures(ProverFeatures.EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)
                     .build();

    public static final SimplifierFeatures ALL_HINTS_NO_FAIL_EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES =
            builder().allHints()
                     .clear(Hint.FAIL_ON_NON_REMOVABLE_NEGATION)
                     .proverFeatures(ProverFeatures.EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)
                     .build();

    public static final SimplifierFeatures ALL_HINTS_INCLUDE_ASSERTIONS_USER_DEFINED_RESERVES =
            builder().allHints()
                     .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_USER_DEFINED_RESERVES)
                     .build();

    public static final SimplifierFeatures ALL_HINTS_NO_FAIL_INCLUDE_ASSERTIONS_USER_DEFINED_RESERVES =
            builder().allHints()
                     .clear(Hint.FAIL_ON_NON_REMOVABLE_NEGATION)
                     .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_USER_DEFINED_RESERVES)
                     .build();

    public static final SimplifierFeatures ALL_HINTS_EXCLUDE_ASSERTIONS_USER_DEFINED_RESERVES =
            builder().allHints()
                     .proverFeatures(ProverFeatures.EXCLUDE_ASSERTIONS_USER_DEFINED_RESERVES)
                     .build();

    public static final SimplifierFeatures ALL_HINTS_NO_FAIL_EXCLUDE_ASSERTIONS_USER_DEFINED_RESERVES =
            builder().allHints()
                     .clear(Hint.FAIL_ON_NON_REMOVABLE_NEGATION)
                     .proverFeatures(ProverFeatures.EXCLUDE_ASSERTIONS_USER_DEFINED_RESERVES)
                     .build();

    public static final SimplifierFeatures ALL_HINTS_INCLUDE_ASSERTIONS_NO_RESERVES =
            builder().allHints()
                     .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES)
                     .build();

    public static final SimplifierFeatures ALL_HINTS_NO_FAIL_INCLUDE_ASSERTIONS_NO_RESERVES =
            builder().allHints()
                     .clear(Hint.FAIL_ON_NON_REMOVABLE_NEGATION)
                     .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES)
                     .build();

    public static final SimplifierFeatures ALL_HINTS_EXCLUDE_ASSERTIONS_NO_RESERVES =
            builder().allHints()
                     .proverFeatures(ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES)
                     .build();

    public static final SimplifierFeatures ALL_HINTS_NO_FAIL_EXCLUDE_ASSERTIONS_NO_RESERVES =
            builder().allHints()
                     .clear(Hint.FAIL_ON_NON_REMOVABLE_NEGATION)
                     .proverFeatures(ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES)
                     .build();

    private final ProverFeatures proverFeatures;
    private final Set<Hint> hints;
    private final int maxMillis;
    private final int maxIterations;
    private final NamingConvention namingConvention;

    SimplifierFeatures(Builder builder) {
        this.proverFeatures = Checks.isNotNull(builder.proverFeatures, "proverFeatures");
        this.hints = Collections.unmodifiableSet(builder.hints);
        this.maxMillis = builder.maxMillis;
        this.maxIterations = builder.maxIterations;
        this.namingConvention = Checks.isNotNull(builder.namingConvention, "namingConvention");
    }

    public enum Hint {
        /**
         * Removes negations if possible.
         * <p>
         * If {@link #FAIL_ON_NON_REMOVABLE_NEGATION} is enabled and a negation
         * cannot be removed, an exception is thrown.<br>
         * Otherwise, nothing is done.
         */
        REMOVE_NEGATION,

        /**
         * If a negation can not be removed, throws an exception.
         * <p>
         * This takes effect when {@link #REMOVE_NEGATION} is enabled.
         */
        FAIL_ON_NON_REMOVABLE_NEGATION,

        /**
         * Removes sections of the expression that always evaluate to true or false.
         * <p>
         * <b>WARNING:</b> this may take a very long time.
         */
        REMOVE_ALWAYS_TRUE_OR_FALSE,

        /**
         * Removes sections of the expression that are redundant.
         * <p>
         * <b>WARNING:</b> this may take a very long time.
         */
        REMOVE_REDUNDANT_SIBLINGS,

        /**
         * Removes inequalities and replaces them by set operations.
         */
        REMOVE_INEQUALITIES,

        /**
         * Removes useless prefixes.
         */
        REMOVE_USELESS_PREFIXES,

        /**
         * Use the B or not(B) for all boolean properties.
         */
        NORMALIZE_BOOLEAN_PROPERTIES,

        /**
         * Converts the expression to DNF.
         * <p>
         * <b>WARNING:</b> this may create many nodes and take a long time.
         */
        CONVERT_TO_DNF,

        /**
         * Use projections to remove values from leaf nodes by using assertions.
         * <p>
         * Assertions must be enabled for this pass to have any effect.
         */
        AUTO_PROJECT
    }

    /**
     * @return The {@link ProverFeatures} to use.
     */
    public ProverFeatures getProverFeatures() {
        return proverFeatures;
    }

    public Set<Hint> getHints() {
        return hints;
    }

    /**
     * Returns {@code true} if a {@link Hint} is enabled.
     *
     * @param hint The hint.
     * @return {@code true} if {@code hint} is enabled.
     */
    public boolean isEnabled(Hint hint) {
        return hints.contains(hint);
    }

    /**
     * @return The maximum time in milliseconds to spend in simplification.
     *         A negative value means no time limits.
     */
    public int getMaxMillis() {
        return maxMillis;
    }

    /**
     * @return The maximum number of iterations to spend in simplification.
     *         A negative value means no limits.
     */
    public int getMaxIterations() {
        return maxIterations;
    }

    public NamingConvention getNamingConvention() {
        return namingConvention;
    }

    @Override
    public int hashCode() {
        return Objects.hash(proverFeatures,
                            hints,
                            maxMillis,
                            maxIterations);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof SimplifierFeatures)) {
            return false;
        }
        final SimplifierFeatures other = (SimplifierFeatures) object;
        return proverFeatures.equals(other.proverFeatures)
                && hints.equals(other.hints)
                && maxMillis == other.maxMillis
                && maxIterations == other.maxIterations;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return getProverFeatures()
                + " " + getHints()
                + " " + getMaxMillis() + " ms"
                + " " + getMaxIterations() + " iterations";
    }

    public static class Builder {
        private ProverFeatures proverFeatures = ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES;
        private final Set<Hint> hints = EnumSet.noneOf(Hint.class);
        private int maxMillis = 1000;
        private int maxIterations = 1000;
        private NamingConvention namingConvention = NamingConvention.DEFAULT;

        public Builder proverFeatures(ProverFeatures proverFeatures) {
            Checks.isNotNull(proverFeatures, "proverFeatures");

            this.proverFeatures = proverFeatures;
            return this;
        }

        public Builder hint(Hint hint) {
            Checks.isNotNull(hint, "hint");

            this.hints.add(hint);
            return this;
        }

        public Builder hints(Hint... hints) {
            for (final Hint hint : hints) {
                hint(hint);
            }
            return this;
        }

        public Builder allHints() {
            return hints(Hint.values());
        }

        public Builder clear(Hint hint) {
            this.hints.remove(hint);
            return this;
        }

        public Builder maxMillis(int maxMillis) {
            this.maxMillis = maxMillis;
            return this;
        }

        public Builder maxIterations(int maxIterations) {
            this.maxIterations = maxIterations;
            return this;
        }

        public Builder noLimits() {
            this.maxMillis = -1;
            this.maxIterations = -1;
            return this;
        }

        public Builder namingConvention(NamingConvention namingConvention) {
            this.namingConvention = namingConvention;
            return this;
        }

        public SimplifierFeatures build() {
            return new SimplifierFeatures(this);
        }
    }
}