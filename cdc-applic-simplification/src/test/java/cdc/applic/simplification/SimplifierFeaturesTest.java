package cdc.applic.simplification;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.proofs.ProverFeatures;

class SimplifierFeaturesTest {
    @Test
    void testDefault() {
        final SimplifierFeatures features = SimplifierFeatures.builder().build();
        assertTrue(features.getHints().isEmpty());
        assertEquals(1000, features.getMaxIterations());
        assertEquals(1000, features.getMaxMillis());
        assertEquals(ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES, features.getProverFeatures());
        for (final SimplifierFeatures.Hint hint : SimplifierFeatures.Hint.values()) {
            assertFalse(features.isEnabled(hint));
        }

        assertEquals(features, features);
        assertNotEquals(features, null);
        assertNotEquals(features, "hello");
    }

    @Test
    void testEquals() {
        final SimplifierFeatures features0 =
                SimplifierFeatures.builder()
                                  .maxIterations(100)
                                  .maxMillis(500)
                                  .allHints()
                                  .proverFeatures(ProverFeatures.EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)
                                  .build();

        final SimplifierFeatures features00 =
                SimplifierFeatures.builder()
                                  .maxIterations(100)
                                  .maxMillis(500)
                                  .allHints()
                                  .proverFeatures(ProverFeatures.EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)
                                  .build();

        final SimplifierFeatures features1 =
                SimplifierFeatures.builder()
                                  .maxIterations(200)
                                  .maxMillis(500)
                                  .allHints()
                                  .proverFeatures(ProverFeatures.EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)
                                  .build();

        final SimplifierFeatures features2 =
                SimplifierFeatures.builder()
                                  .maxIterations(100)
                                  .maxMillis(750)
                                  .allHints()
                                  .proverFeatures(ProverFeatures.EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)
                                  .build();

        final SimplifierFeatures features3 =
                SimplifierFeatures.builder()
                                  .maxIterations(100)
                                  .maxMillis(500)
                                  .allHints()
                                  .clear(SimplifierFeatures.Hint.CONVERT_TO_DNF)
                                  .proverFeatures(ProverFeatures.EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)
                                  .build();

        final SimplifierFeatures features4 =
                SimplifierFeatures.builder()
                                  .maxIterations(100)
                                  .maxMillis(500)
                                  .allHints()
                                  .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)
                                  .build();

        assertEquals(features0, features00);
        assertNotEquals(features0, features1);
        assertNotEquals(features0, features2);
        assertNotEquals(features0, features3);
        assertNotEquals(features0, features4);

        assertEquals(features0.hashCode(), features00.hashCode());
        assertNotEquals(features0.hashCode(), features1.hashCode());

        assertEquals(features0.toString(), features00.toString());
    }
}