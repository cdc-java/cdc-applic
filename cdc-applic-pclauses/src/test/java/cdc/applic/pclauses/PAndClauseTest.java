package cdc.applic.pclauses;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.literals.Name;
import cdc.applic.proofs.core.ast.AtomVarNode;

class PAndClauseTest {
    private static void check(String expectedString,
                              String expectedExpression,
                              String... expressions) {
        final List<Node> nodes = new ArrayList<>();
        for (final String expression : expressions) {
            nodes.add(new Expression(expression).getRootNode());
        }

        final PAndClause clause = PAndClause.of(nodes.toArray(new Node[0]));
        assertEquals(expectedString, clause.toString());
        assertEquals(Connective.AND, clause.getConnective());

        assertEquals(expectedExpression, clause.toExpression().getContent());
    }

    @Test
    void test() {
        check("TRUE", "true");
        check("TRUE", "true", "true");
        check("FALSE", "false", "false");
        check("AND(TRUE,TRUE)", "true&true", "true", "true");
        check("AND(FALSE,FALSE)", "false&false", "false", "false");
        check("AND(REF(X),REF(Y))", "X&Y", "X", "Y");
        check("AND(REF(X),REF(Y),REF(X))", "X&Y&X", "X", "Y", "X");
        check("AND(REF(X),REF(Y),NOT(REF(X)))", "X&Y&!X", "X", "Y", "!X");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         check(null, null, "!(X or Y)");
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         check(null, null, "X or Y");
                     });

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         PAndClause.of(new AtomVarNode(Name.of("Hello")));
                     });
    }
}