package cdc.applic.pclauses;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;

class POrSentenceTest {
    private static void check(String expectedString,
                              String expectedExpression,
                              int expectedSize,
                              String expression) {

        final Expression x = new Expression(expression);
        final POrSentence sentence = POrSentence.of(x);

        assertEquals(expectedString, sentence.toString());
        assertEquals(Connective.OR, sentence.getConnective());
        assertEquals(expectedExpression, sentence.toExpression().getContent());
        assertEquals(expectedSize, sentence.getClauses().size());
        assertEquals(PAndClause.FALSE, sentence.getNeutralClause());
    }

    @Test
    void test() {
        check("TRUE", "true", 1, "true");
        check("FALSE", "false", 1, "false");
        check("OR[TRUE,FALSE]", "true|false", 2, "true or false");
        check("OR[FALSE,FALSE]", "false|false", 2, "!true or false"); // FIXME is this expected ?
        check("OR[REF(X),REF(Y)]", "X|Y", 2, "X or Y");
        check("OR[NOT(REF(X)),REF(Y)]", "!X|Y", 2, "!X or Y");
        check("OR[NOT(REF(X)),AND(REF(Y),REF(Z))]", "!X|Y&Z", 2, "!X or Y  and Z");
        check("AND(REF(X),REF(Y))", "X&Y", 1, "X and Y");
        assertEquals("FALSE", POrSentence.EMPTY.toString());
        assertEquals("false", POrSentence.EMPTY.toNode().compress());
    }
}