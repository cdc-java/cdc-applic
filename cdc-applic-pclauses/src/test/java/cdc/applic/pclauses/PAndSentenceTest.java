package cdc.applic.pclauses;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;

class PAndSentenceTest {
    private static void check(String expectedString,
                              String expectedExpression,
                              int expectedSize,
                              String expression) {

        final Expression x = new Expression(expression);
        final PAndSentence sentence = PAndSentence.of(x);

        assertEquals(expectedString, sentence.toString());
        assertEquals(Connective.AND, sentence.getConnective());
        assertEquals(expectedExpression, sentence.toExpression().getContent());
        assertEquals(expectedSize, sentence.getClauses().size());
        assertEquals(POrClause.TRUE, sentence.getNeutralClause());
    }

    @Test
    void test() {
        check("TRUE", "true", 1, "true");
        check("FALSE", "false", 1, "false");
        check("AND[TRUE,FALSE]", "true&false", 2, "true and false");
        check("AND[FALSE,FALSE]", "false&false", 2, "!true and false"); // FIXME is this expected ?
        check("AND[REF(X),REF(Y)]", "X&Y", 2, "X and Y");
        check("AND[NOT(REF(X)),REF(Y)]", "!X&Y", 2, "!X and Y");
        check("AND[NOT(REF(X)),OR(REF(Y),REF(Z))]", "!X&(Y|Z)", 2, "!X and (Y or Z)");
        check("OR(REF(X),REF(Y))", "X|Y", 1, "X or Y");
        assertEquals("TRUE", PAndSentence.EMPTY.toString());
        assertEquals("true", PAndSentence.EMPTY.toNode().compress());
    }
}