package cdc.applic.pclauses;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import cdc.applic.dictionaries.core.visitors.ConvertToCNF;
import cdc.applic.dictionaries.core.visitors.MoveNotInwards;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.ConvertToNary;

public final class PAndSentence extends PSentence {
    private final List<POrClause> clauses;

    public static final PAndSentence EMPTY = of();

    private PAndSentence(List<POrClause> clauses) {
        this.clauses = Collections.unmodifiableList(clauses);
    }

    public static PAndSentence of(List<POrClause> clauses) {
        return new PAndSentence(clauses);
    }

    public static PAndSentence of(POrClause... clauses) {
        return of(Arrays.asList(clauses));
    }

    public static PAndSentence of(Expression expression) {
        return of(convert(expression));
    }

    private static List<POrClause> convert(Expression expression) {
        final Node cnf = ConvertToCNF.execute(expression.getRootNode(), MoveNotInwards.Variant.USE_NEGATIVE_LEAVES);
        final Node root = ConvertToNary.execute(cnf, ConvertToNary.Variant.ALWAYS);
        final List<POrClause> result = new ArrayList<>();
        if (root instanceof NaryAndNode) {
            final NaryAndNode nary = (NaryAndNode) root;
            for (final Node n1 : nary.getChildren()) {
                if (n1 instanceof NaryOrNode) {
                    final NaryOrNode n2 = (NaryOrNode) n1;
                    final POrClause clause = POrClause.of(n2.getChildren());
                    result.add(clause);
                } else {
                    final POrClause clause = POrClause.of(n1);
                    result.add(clause);
                }
            }
        } else if (root instanceof NaryOrNode) {
            final NaryOrNode nary = (NaryOrNode) root;
            final POrClause clause = POrClause.of(nary.getChildren());
            result.add(clause);
        } else {
            final POrClause clause = POrClause.of(root);
            result.add(clause);
        }
        return result;
    }

    @Override
    public POrClause getNeutralClause() {
        return POrClause.TRUE;
    }

    @Override
    public List<POrClause> getClauses() {
        return clauses;
    }

    @Override
    public Connective getConnective() {
        return Connective.AND;
    }

    @Override
    public Node toNode() {
        final List<Node> nodes = new ArrayList<>();
        for (final PClause clause : getClauses()) {
            nodes.add(clause.toNode());
        }

        if (nodes.isEmpty()) {
            // TODO check
            return TrueNode.INSTANCE;
        } else {
            return NaryAndNode.createSimplestAnd(nodes);
        }
    }

    @Override
    public String toString() {
        return toString("TRUE");
    }
}