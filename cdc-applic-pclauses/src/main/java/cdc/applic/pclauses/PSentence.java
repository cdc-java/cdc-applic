package cdc.applic.pclauses;

import java.util.Arrays;
import java.util.List;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;

public abstract class PSentence {
    protected PSentence() {
    }

    /**
     * Returns a neutral clause, that is a clause that does not change significance of this
     * clauses set when added.
     *
     * @return A neutral clause.
     */
    public abstract PClause getNeutralClause();

    /**
     * @return the list of clauses that constitute this clause set.
     */
    public abstract List<? extends PClause> getClauses();

    public abstract Connective getConnective();

    public abstract Node toNode();

    public final Expression toExpression() {
        return toNode().toExpression();
    }

    public static PSentence of(Connective connective,
                               Expression expression) {
        if (connective == Connective.AND) {
            return PAndSentence.of(expression);
        } else {
            return POrSentence.of(expression);
        }
    }

    public static PSentence of(Connective connective,
                               List<? extends PClause> clauses) {
        if (connective == Connective.AND) {
            final List<POrClause> tmp = clauses.stream()
                                               .map(POrClause.class::cast)
                                               .toList();
            return PAndSentence.of(tmp);
        } else {
            final List<PAndClause> tmp = clauses.stream()
                                                .map(PAndClause.class::cast)
                                                .toList();
            return POrSentence.of(tmp);
        }
    }

    public static PSentence of(Connective connective,
                               PClause... clauses) {
        return of(connective, Arrays.asList(clauses));
    }

    protected String toString(String empty) {
        final StringBuilder builder = new StringBuilder();
        final List<? extends PClause> clauses = getClauses();

        if (clauses.isEmpty()) {
            builder.append(empty);
        } else if (clauses.size() == 1) {
            builder.append(clauses.get(0));
        } else {
            builder.append(getConnective());
            builder.append('[');
            boolean first = true;
            for (final PClause clause : getClauses()) {
                if (!first) {
                    builder.append(',');
                }
                builder.append(clause);
                first = false;
            }
            builder.append(']');
        }
        return builder.toString();
    }
}