package cdc.applic.pclauses;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import cdc.applic.dictionaries.core.visitors.ConvertToDNF;
import cdc.applic.dictionaries.core.visitors.MoveNotInwards;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.ConvertToNary;

public final class POrSentence extends PSentence {
    private final List<PAndClause> clauses;

    public static final POrSentence EMPTY = of();

    private POrSentence(List<PAndClause> clauses) {
        this.clauses = Collections.unmodifiableList(clauses);
    }

    public static POrSentence of(List<PAndClause> clauses) {
        return new POrSentence(clauses);
    }

    public static POrSentence of(PAndClause... clauses) {
        return of(Arrays.asList(clauses));
    }

    public static POrSentence of(Expression expression) {
        return of(convert(expression));
    }

    private static List<PAndClause> convert(Expression expression) {
        final Node dnf = ConvertToDNF.execute(expression.getRootNode(), MoveNotInwards.Variant.USE_NEGATIVE_LEAVES);
        final Node root = ConvertToNary.execute(dnf, ConvertToNary.Variant.ALWAYS);
        final List<PAndClause> result = new ArrayList<>();
        if (root instanceof NaryOrNode) {
            final NaryOrNode nary = (NaryOrNode) root;
            for (final Node n1 : nary.getChildren()) {
                if (n1 instanceof NaryAndNode) {
                    final NaryAndNode n2 = (NaryAndNode) n1;
                    final PAndClause clause = PAndClause.of(n2.getChildren());
                    result.add(clause);
                } else {
                    final PAndClause clause = PAndClause.of(n1);
                    result.add(clause);
                }
            }
        } else if (root instanceof NaryAndNode) {
            final NaryAndNode nary = (NaryAndNode) root;
            final PAndClause clause = PAndClause.of(nary.getChildren());
            result.add(clause);
        } else {
            final PAndClause clause = PAndClause.of(root);
            result.add(clause);
        }
        return result;
    }

    @Override
    public PAndClause getNeutralClause() {
        return PAndClause.FALSE;
    }

    @Override
    public List<PAndClause> getClauses() {
        return clauses;
    }

    @Override
    public Connective getConnective() {
        return Connective.OR;
    }

    @Override
    public Node toNode() {
        final List<Node> nodes = new ArrayList<>();
        for (final PClause clause : getClauses()) {
            nodes.add(clause.toNode());
        }

        if (nodes.isEmpty()) {
            // TODO check
            return FalseNode.INSTANCE;
        } else {
            return NaryOrNode.createSimplestOr(nodes);
        }
    }

    @Override
    public String toString() {
        return toString("FALSE");
    }
}