package cdc.applic.pclauses;

public enum Connective {
    AND,
    OR
}