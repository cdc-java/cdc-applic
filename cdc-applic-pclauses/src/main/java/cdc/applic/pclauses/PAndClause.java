package cdc.applic.pclauses;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.LeafNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.ParsingNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.TrueNode;

public final class PAndClause extends PClause {
    public static final PAndClause TRUE = of(TrueNode.INSTANCE);
    public static final PAndClause FALSE = of(FalseNode.INSTANCE);

    private PAndClause(List<Node> nodes) {
        super(filter(nodes));
    }

    public static PAndClause of(List<Node> nodes) {
        return new PAndClause(nodes);
    }

    public static PAndClause of(Node... nodes) {
        return of(Arrays.asList(nodes));
    }

    private static List<Node> filter(List<Node> nodes) {
        final List<Node> result = new ArrayList<>();
        for (final Node node : nodes) {
            if (node instanceof NotNode) {
                final AbstractUnaryNode unode = (AbstractUnaryNode) node;
                if (unode.getAlpha() instanceof RefNode) {
                    result.add(node);
                } else {
                    throw new IllegalArgumentException("Can not add negation of " + unode.getAlpha().getKind() + " nodes");
                }
            } else if (node instanceof ParsingNode && node instanceof LeafNode) {
                result.add(node);
            } else {
                throw new IllegalArgumentException("Can not add " + node.getKind() + " nodes");
            }
        }
        return result;
    }

    @Override
    public Connective getConnective() {
        return Connective.AND;
    }

    @Override
    public Node toNode() {
        if (nodes.isEmpty()) {
            return TrueNode.INSTANCE;
        } else {
            return NaryAndNode.createSimplestAnd(nodes);
        }
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        if (nodes.isEmpty()) {
            builder.append(TrueNode.INSTANCE);
        } else if (nodes.size() == 1) {
            builder.append(nodes.get(0));
        } else {
            builder.append(getConnective());
            builder.append('(');
            boolean first = true;
            for (final Node node : getNodes()) {
                if (!first) {
                    builder.append(',');
                }
                builder.append(node);
                first = false;
            }
            builder.append(')');
        }
        return builder.toString();
    }
}