package cdc.applic.dictionaries.core.writing.checks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.applic.dictionaries.checks.WritingRulePurpose;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.PolicyImpl;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.ApplicIssueType;
import cdc.applic.expressions.checks.CheckedData;
import cdc.issues.Diagnosis;

class WritingRuleCheckerTest {
    private static final Logger LOGGER = LogManager.getLogger(WritingRuleCheckerTest.class);

    private static class WRC extends WritingRuleChecker {
        public static final String RULE_NAME = "WRC";

        public WRC() {
            super();
        }

        public void register() {
            register(RULE_NAME, c -> new WRC());
        }

        @Override
        public String getRuleName() {
            return RULE_NAME;
        }

        @Override
        public boolean isCompliant(CheckedData data) {
            return false;
        }

        @Override
        public void check(CheckedData data,
                          List<ApplicIssue> issues) {
            // Ignore
        }

        @Override
        public WritingRulePurpose getPurpose() {
            return null;
        }
    }

    private static void check(String expression,
                              ApplicIssueType expected,
                              String... writingRuleNames) {
        LOGGER.debug("=================================");
        LOGGER.debug("check({})", expression);
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").prefix("R").build();
        registry.namingConvention().name("Convention1").build();
        registry.booleanType().name("B").build();
        registry.enumeratedType().name("E").frozen(false).literals("E1", "E2", "E3").build();
        registry.integerType().name("I").frozen(false).domain("1~999").build();
        registry.realType().name("R").frozen(false).domain("1.0~999.0").build();
        registry.patternType().name("P").frozen(false).pattern("[A-Z]{2}").build();

        registry.property().name("BA").synonym("Convention1", "ba").type("B").ordinal(0).build();
        registry.property().name("EA").type("E").ordinal(1).build();
        registry.property().name("IA").type("I").ordinal(2).build();
        registry.property().name("RA").type("R").ordinal(3).build();
        registry.property().name("PA").type("P").ordinal(4).build();
        registry.property().name("BB").type("B").ordinal(0).build();
        registry.property().name("EB").type("E").ordinal(1).build();
        registry.property().name("IB").type("I").ordinal(2).build();
        registry.property().name("RB").type("R").ordinal(3).build();
        registry.property().name("PB").type("P").ordinal(4).build();

        registry.createAssertion(new Expression("BA <-> !BB"));

        final PolicyImpl policyA = registry.policy().name("Policy A").build();
        // registry.print(OUT);

        policyA.setItemUsage("BA", DItemUsage.OPTIONAL);
        policyA.setItemUsage("BB", DItemUsage.OPTIONAL);
        policyA.setItemUsage("EA", DItemUsage.MANDATORY);
        policyA.setItemUsage("IA", DItemUsage.RECOMMENDED);
        policyA.setItemUsage("RA", DItemUsage.OPTIONAL);
        policyA.setItemUsage("PA", DItemUsage.MANDATORY);

        // TODO check disabled rules

        policyA.setWritingRuleEnabled(DefaultWritingRule.COMPLIES_WITH_PRESCRIBED_USAGE, false);
        policyA.setWritingRuleEnabled(DefaultWritingRule.DOES_NOT_CONTAIN_REMOVABLE_NODES_EANR, false);
        policyA.setWritingRuleEnabled(DefaultWritingRule.DOES_NOT_CONTAIN_REMOVABLE_NODES_EAUDR, false);
        policyA.setWritingRuleEnabled(DefaultWritingRule.DOES_NOT_CONTAIN_REMOVABLE_NODES_IANR, false);
        policyA.setWritingRuleEnabled(DefaultWritingRule.DOES_NOT_CONTAIN_REMOVABLE_NODES_IAUDR, false);
        policyA.setWritingRuleEnabled(DefaultWritingRule.DOES_NOT_CONTAIN_USELESS_PREFIXES, true);
        policyA.setWritingRuleEnabled(DefaultWritingRule.DOES_NOT_USE_DEEP_TRUE, true);
        policyA.setWritingRuleEnabled(DefaultWritingRule.DOES_NOT_USE_IMPLICATION_OR_EQUIVALENCE, true);
        policyA.setWritingRuleEnabled(DefaultWritingRule.DOES_NOT_USE_NEGATION, true);
        policyA.setWritingRuleEnabled(DefaultWritingRule.DOES_NOT_USE_OPERATORS, true);
        policyA.setWritingRuleEnabled(DefaultWritingRule.IS_DNF, true);
        policyA.setWritingRuleEnabled(DefaultWritingRule.IS_SATISFIABLE, true);
        policyA.setWritingRuleEnabled(DefaultWritingRule.IS_SINGLE_POINT, false);
        policyA.setWritingRuleEnabled(DefaultWritingRule.PROPERTIES_AND_ALIASES_ARE_WELL_ORDERED, true);
        policyA.setWritingRuleEnabled(DefaultWritingRule.SETS_ARE_NORMALIZED, true);
        policyA.setWritingRuleEnabled(DefaultWritingRule.USES_DEFAULT_NAMING_CONVENTION, true);

        final DictionaryHandle policyAHandle = new DictionaryHandle(policyA);

        final Expression x = new Expression(expression, false);

        final Diagnosis<ApplicIssue> diagnosis = WritingRuleChecker.check(policyAHandle, x);
        LOGGER.debug(expression);
        LOGGER.debug(diagnosis);
        final Set<String> actualNames = new HashSet<>();
        for (final ApplicIssue issue : diagnosis.getIssues()) {
            if ((issue instanceof WritingRuleIssue)) {
                actualNames.add(((WritingRuleIssue) issue).getRuleName());
            }
        }
        final Set<String> expectedNames = new HashSet<>();
        for (final String name : writingRuleNames) {
            expectedNames.add(name);
        }

        assertEquals(expected, ApplicIssue.getMostSevereType(diagnosis.getIssues()), "Found: " + diagnosis);
        assertEquals(expectedNames, actualNames, "Expected " + expectedNames);
        try {
            x.getRootNode();
            for (final String writingRuleName : policyAHandle.getDictionary().getWritingRuleNames()) {
                final WritingRuleChecker checker = WritingRuleChecker.create(writingRuleName, policyAHandle);
                final List<ApplicIssue> issues = new ArrayList<>();
                checker.check(x, issues);
                final boolean compliant = checker.isCompliant(x);
                assertEquals(compliant, issues.isEmpty());
                assertEquals(writingRuleName, checker.getRuleName());
                checker.getPurpose(); // for coverage
            }
        } catch (final Exception e) {
            // Ignore
        }

        // Coverage
        // WritingRuleChecker.check(policyAHandle, Expression.TRUE, new ArrayList<>());
    }

    @Test
    void testWRC() {
        final WRC checker = new WRC();
        checker.register();
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checker.register();
                     });
        final WritingRuleChecker c = WritingRuleChecker.create(WRC.RULE_NAME, null);
        assertEquals(WRC.class, c.getClass());
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         WritingRuleChecker.create("Hello", null);
                     });

        assertEquals(Diagnosis.OK_DIAGNOSIS, c.check(Expression.TRUE));
    }

    @Test
    void test() {
        check("ba",
              ApplicIssueType.WRITING_ISSUE,
              UsesDefaultNamingConventionChecker.RULE_NAME);
        check("BA",
              ApplicIssueType.NO_ISSUE);
        check("true",
              ApplicIssueType.NO_ISSUE);
        check("true or true",
              ApplicIssueType.WRITING_ISSUE,
              DoesNotUseDeepTrueChecker.RULE_NAME,
              DoesNotUseOperatorsChecker.RULE_NAME);

        check("EA = E1 -> true",
              ApplicIssueType.WRITING_ISSUE,
              DoesNotUseDeepTrueChecker.RULE_NAME,
              DoesNotUseOperatorsChecker.RULE_NAME,
              DoesNotUseImplicationOrEquivalenceChecker.RULE_NAME,
              IsDNFChecker.RULE_NAME);

        check("BA and EA = E1",
              ApplicIssueType.WRITING_ISSUE,
              DoesNotUseOperatorsChecker.RULE_NAME);
        check("EA = E1 and BA",
              ApplicIssueType.WRITING_ISSUE,
              DoesNotUseOperatorsChecker.RULE_NAME,
              NamesAreOrderedChecker.RULE_NAME);

        check("EA in EA", ApplicIssueType.SYNTACTIC_ISSUE);

        check("false",
              ApplicIssueType.WRITING_ISSUE,
              IsSatisfiableChecker.RULE_NAME,
              DoesNotUseNegationChecker.RULE_NAME);

        check("E = E3", ApplicIssueType.SEMANTIC_ISSUE);
        check("EA = E4", ApplicIssueType.SEMANTIC_ISSUE);
        check("EB = E1", ApplicIssueType.SEMANTIC_ISSUE);

        check("EA in {E3, E2}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("EA in {E3, E3}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);

        check("PA = A", ApplicIssueType.SEMANTIC_ISSUE);
        check("PA = AA", ApplicIssueType.NO_ISSUE);
        check("PA <: {AA}", ApplicIssueType.NO_ISSUE);
        check("PA <: {AB, AA}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("PA <: {AA, AA}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);

        check("IA = 10", ApplicIssueType.NO_ISSUE);
        check("IA in {9, 10}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("IA in {10, 8}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("IA in {10~10}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("IA in {10~9}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME,
              IsSatisfiableChecker.RULE_NAME);
        check("IA in {9, 9}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("IA in {9~10, 11}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("IA in {9~10, 10}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("IA in {9~10, 12}",
              ApplicIssueType.NO_ISSUE);
        check("IA in {8, 9~10}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("IA in {7~8, 9~10}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("IA in {8, 7~10}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("IA in {8, 8~10}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("IA in {8, 9~10}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("IA in {8, 10~11}", ApplicIssueType.NO_ISSUE);

        check("RA = 10.0", ApplicIssueType.NO_ISSUE);
        check("RA in {10.0, 8.0}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("RA in {10.0~10.0}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("RA in {10.0~9.0}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME,
              IsSatisfiableChecker.RULE_NAME);
        check("RA in {9.0, 9.0}",
              ApplicIssueType.WRITING_ISSUE,
              SetsAreNormalizedChecker.RULE_NAME);
        check("BA & BB",
              ApplicIssueType.WRITING_ISSUE,
              DoesNotUseOperatorsChecker.RULE_NAME,
              IsSatisfiableChecker.RULE_NAME);
        check("R.BA",
              ApplicIssueType.WRITING_ISSUE,
              DoesNotContainUselessPrefixesChecker.RULE_NAME);
    }
}