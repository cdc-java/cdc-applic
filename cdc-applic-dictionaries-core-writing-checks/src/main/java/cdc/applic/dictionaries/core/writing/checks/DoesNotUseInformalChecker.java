package cdc.applic.dictionaries.core.writing.checks;

import java.util.List;

import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.applic.dictionaries.checks.WritingRulePurpose;
import cdc.applic.expressions.ast.InformalNode;
import cdc.applic.expressions.ast.NodePredicates;
import cdc.applic.expressions.ast.visitors.PredicateMatcher;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.ExpressionLocation;

/**
 * A checker that checks that {@link InformalNode} is not used.
 *
 * @author Damien Carbonne
 */
public final class DoesNotUseInformalChecker extends WritingRuleChecker {
    public static final String RULE_NAME = "Does not use informal";
    public static final DoesNotUseInformalChecker INSTANCE =
            new DoesNotUseInformalChecker();

    static {
        register(RULE_NAME, c -> INSTANCE);
    }

    private DoesNotUseInformalChecker() {
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }

    @Override
    public WritingRulePurpose getPurpose() {
        return WritingRulePurpose.SATISFIABILITY;
    }

    @Override
    protected boolean doCheck(CheckedData data,
                              List<ApplicIssue> issues) {
        if (PredicateMatcher.matches(data.getNode(), NodePredicates.IS_INFORMAL)) {
            if (issues == null) {
                return false;
            }
            issues.add(new WritingRuleIssue(RULE_NAME,
                                            new ExpressionLocation(data.getContext(),
                                                                   data.getContent()),
                                            "Expression contains informal part(s)"));
        }
        return true;
    }
}