package cdc.applic.dictionaries.core.writing.checks;

import java.util.List;

import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.applic.dictionaries.checks.WritingRulePurpose;
import cdc.applic.expressions.ast.EquivalenceNode;
import cdc.applic.expressions.ast.ImplicationNode;
import cdc.applic.expressions.ast.NodePredicates;
import cdc.applic.expressions.ast.visitors.PredicateMatcher;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.ExpressionLocation;

/**
 * A checker that checks that {@link ImplicationNode} and {@link EquivalenceNode} are not used.
 *
 * @author Damien Carbonne
 */
public final class DoesNotUseImplicationOrEquivalenceChecker extends WritingRuleChecker {
    public static final String RULE_NAME = "Does not use implication or equivalence";
    public static final DoesNotUseImplicationOrEquivalenceChecker INSTANCE =
            new DoesNotUseImplicationOrEquivalenceChecker();

    static {
        register(RULE_NAME, c -> INSTANCE);
    }

    private DoesNotUseImplicationOrEquivalenceChecker() {
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }

    @Override
    public WritingRulePurpose getPurpose() {
        return WritingRulePurpose.COMPLEXITY;
    }

    @Override
    protected boolean doCheck(CheckedData data,
                              List<ApplicIssue> issues) {
        if (PredicateMatcher.matches(data.getNode(), NodePredicates.IS_IMPLICATION_OR_EQUIVALENCE)) {
            if (issues == null) {
                return false;
            }
            issues.add(new WritingRuleIssue(RULE_NAME,
                                            new ExpressionLocation(data.getContext(),
                                                                   data.getContent()),
                                            "Expression contains implication or equivalence"));
        }
        return true;
    }
}