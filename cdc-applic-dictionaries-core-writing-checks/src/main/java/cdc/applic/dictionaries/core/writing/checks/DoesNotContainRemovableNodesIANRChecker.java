package cdc.applic.dictionaries.core.writing.checks;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.proofs.ProverFeatures;

/**
 * Checker that detects presence of nodes that can be
 * removed (including assertions and no reserves).
 *
 * @author Damien Carbonne
 */
public final class DoesNotContainRemovableNodesIANRChecker extends AbstractDoesNotContainRemovableNodesChecker {
    public static final String RULE_NAME = "Does not contain removable nodes (including assertions, no reserves)";

    static {
        register(RULE_NAME, DoesNotContainRemovableNodesIANRChecker::new);
    }

    private DoesNotContainRemovableNodesIANRChecker(DictionaryHandle handle) {
        super(handle,
              ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES,
              RULE_NAME,
              "including assertions, no reserves");
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }
}