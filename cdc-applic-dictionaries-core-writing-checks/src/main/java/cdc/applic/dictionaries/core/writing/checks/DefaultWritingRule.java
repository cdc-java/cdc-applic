package cdc.applic.dictionaries.core.writing.checks;

import cdc.applic.dictionaries.WritingRuleNameSupplier;

/**
 * Enumeration of default writing rules.
 *
 * @author Damien Carbonne
 */
public enum DefaultWritingRule implements WritingRuleNameSupplier {
    COMPLIES_WITH_PRESCRIBED_USAGE(CompliesWithPrescribedUsageChecker.RULE_NAME),
    DOES_NOT_CONTAIN_REMOVABLE_NODES_EANR(DoesNotContainRemovableNodesEANRChecker.RULE_NAME),
    DOES_NOT_CONTAIN_REMOVABLE_NODES_EAUDR(DoesNotContainRemovableNodesEAUDRChecker.RULE_NAME),
    DOES_NOT_CONTAIN_REMOVABLE_NODES_IANR(DoesNotContainRemovableNodesIANRChecker.RULE_NAME),
    DOES_NOT_CONTAIN_REMOVABLE_NODES_IAUDR(DoesNotContainRemovableNodesIAUDRChecker.RULE_NAME),
    DOES_NOT_CONTAIN_USELESS_PREFIXES(DoesNotContainUselessPrefixesChecker.RULE_NAME),
    DOES_NOT_USE_DEEP_TRUE(DoesNotUseDeepTrueChecker.RULE_NAME),
    DOES_NOT_USE_INEQUALITIES(DoesNotUseInequalitiesChecker.RULE_NAME),
    DOES_NOT_USE_IMPLICATION_OR_EQUIVALENCE(DoesNotUseImplicationOrEquivalenceChecker.RULE_NAME),
    DOES_NOT_USE_INFORMAL(DoesNotUseInformalChecker.RULE_NAME),
    DOES_NOT_USE_NEGATION(DoesNotUseNegationChecker.RULE_NAME),
    DOES_NOT_USE_OPERATORS(DoesNotUseOperatorsChecker.RULE_NAME),
    DOES_NOT_USE_XOR(DoesNotUseXorChecker.RULE_NAME),
    IS_DNF(IsDNFChecker.RULE_NAME),
    IS_SATISFIABLE(IsSatisfiableChecker.RULE_NAME),
    IS_SINGLE_POINT(IsSinglePointChecker.RULE_NAME),
    PROPERTIES_AND_ALIASES_ARE_WELL_ORDERED(NamesAreOrderedChecker.RULE_NAME),
    SETS_ARE_NORMALIZED(SetsAreNormalizedChecker.RULE_NAME),
    USES_DEFAULT_NAMING_CONVENTION(UsesDefaultNamingConventionChecker.RULE_NAME);

    static {
        elaborate();
    }

    private final String ruleName;

    private DefaultWritingRule(String ruleName) {
        this.ruleName = ruleName;
    }

    @Override
    public String getRuleName() {
        return ruleName;
    }

    public static void elaborate() {
        CompliesWithPrescribedUsageChecker.elaborate();
        DoesNotContainRemovableNodesEANRChecker.elaborate();
        DoesNotContainRemovableNodesEAUDRChecker.elaborate();
        DoesNotContainRemovableNodesIANRChecker.elaborate();
        DoesNotContainRemovableNodesIAUDRChecker.elaborate();
        DoesNotContainUselessPrefixesChecker.elaborate();
        DoesNotUseDeepTrueChecker.elaborate();
        DoesNotUseInequalitiesChecker.elaborate();
        DoesNotUseImplicationOrEquivalenceChecker.elaborate();
        DoesNotUseInformalChecker.elaborate();
        DoesNotUseNegationChecker.elaborate();
        DoesNotUseOperatorsChecker.elaborate();
        DoesNotUseXorChecker.elaborate();
        IsDNFChecker.elaborate();
        IsSatisfiableChecker.elaborate();
        IsSinglePointChecker.elaborate();
        NamesAreOrderedChecker.elaborate();
        SetsAreNormalizedChecker.elaborate();
        UsesDefaultNamingConventionChecker.elaborate();
    }
}