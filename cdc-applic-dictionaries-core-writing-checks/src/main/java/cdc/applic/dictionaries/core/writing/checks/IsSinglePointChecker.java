package cdc.applic.dictionaries.core.writing.checks;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.applic.dictionaries.checks.WritingRulePurpose;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.ExpressionLocation;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.projections.Axis;
import cdc.applic.projections.Shadow;
import cdc.applic.projections.core.SingleAxisExpressionProjector;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.ProverMatching;

/**
 * A Checker that checks that with an expression, all allowed properties
 * or aliases have one and only one possible value.
 *
 * @author Damien Carbonne
 */
public final class IsSinglePointChecker extends WritingRuleChecker {
    public static final String RULE_NAME = "Is single point";
    private final List<SingleAxisExpressionProjector> projectors = new ArrayList<>();

    static {
        register(RULE_NAME, IsSinglePointChecker::new);
    }

    private IsSinglePointChecker(DictionaryHandle handle) {
        for (final NamedDItem item : handle.getDictionary().getAllowedItems()) {
            final SingleAxisExpressionProjector projector =
                    new SingleAxisExpressionProjector(handle,
                                                      ProverFeatures.INCLUDE_ASSERTIONS_USER_DEFINED_RESERVES,
                                                      new Axis(item));
            this.projectors.add(projector);
        }
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }

    @Override
    protected boolean doCheck(CheckedData data,
                              List<ApplicIssue> issues) {
        for (final SingleAxisExpressionProjector projector : projectors) {
            final Shadow shadow = projector.project(data.getNode());
            final SItemSet set = shadow.getAxisSet(projector.getAxis(),
                                                   ProverMatching.ALWAYS,
                                                   ProverMatching.SOMETIMES);
            if (!set.isSingleton()) {
                if (issues == null) {
                    return false;
                }
                if (set.isEmpty()) {
                    issues.add(new WritingRuleIssue(RULE_NAME,
                                                    new ExpressionLocation(data.getContext(),
                                                                           data.getContent()),
                                                    "Projection of expression on '" + projector.getAxis().getNameOrNull()
                                                            + "' is empty"));
                } else {
                    issues.add(new WritingRuleIssue(RULE_NAME,
                                                    new ExpressionLocation(data.getContext(),
                                                                           data.getContent()),
                                                    "Projection of expression on '" + projector.getAxis().getNameOrNull()
                                                            + "' is multi-valued (" + set + ")"));
                }
            }
        }
        return true;
    }

    @Override
    public WritingRulePurpose getPurpose() {
        return WritingRulePurpose.SATISFIABILITY;
    }
}