package cdc.applic.dictionaries.core.writing.checks;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.proofs.ProverFeatures;

/**
 * Checker that detects presence of nodes that can be
 * removed (including assertions and user-defined reserves).
 *
 * @author Damien Carbonne
 */
public final class DoesNotContainRemovableNodesIAUDRChecker extends AbstractDoesNotContainRemovableNodesChecker {
    public static final String RULE_NAME = "Does not contain removable nodes (including assertions, user-defined reserves)";

    static {
        register(RULE_NAME, DoesNotContainRemovableNodesIAUDRChecker::new);
    }

    private DoesNotContainRemovableNodesIAUDRChecker(DictionaryHandle handle) {
        super(handle,
              ProverFeatures.INCLUDE_ASSERTIONS_USER_DEFINED_RESERVES,
              RULE_NAME,
              "including assertions, user-defined reserves");
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }
}