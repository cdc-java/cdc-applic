package cdc.applic.dictionaries.core.writing.checks;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.applic.dictionaries.checks.WritingRulePurpose;
import cdc.applic.expressions.ast.AbstractOperatorNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NodePredicates;
import cdc.applic.expressions.ast.visitors.PredicateCollector;
import cdc.applic.expressions.ast.visitors.PredicateMatcher;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.ExpressionLocation;

/**
 * A checker that checks that {@link AbstractOperatorNode operators} are not used.
 *
 * @author Damien Carbonne
 */
public final class DoesNotUseOperatorsChecker extends WritingRuleChecker {
    public static final String RULE_NAME = "Does not use operators";
    public static final DoesNotUseOperatorsChecker INSTANCE =
            new DoesNotUseOperatorsChecker();

    static {
        register(RULE_NAME, c -> INSTANCE);
    }

    private DoesNotUseOperatorsChecker() {
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }

    @Override
    public WritingRulePurpose getPurpose() {
        return WritingRulePurpose.COMPLEXITY;
    }

    @Override
    protected boolean doCheck(CheckedData data,
                              List<ApplicIssue> issues) {
        if (issues == null) {
            return !PredicateMatcher.matches(data.getNode(), NodePredicates.IS_OPERATOR);
        } else {
            final List<Node> nodes = new ArrayList<>();
            PredicateCollector.collect(data.getNode(), NodePredicates.IS_OPERATOR, nodes);
            for (final Node node : nodes) {
                issues.add(new WritingRuleIssue(RULE_NAME,
                                                new ExpressionLocation(data.getContext(),
                                                                       data.getContent()),
                                                "Expression contains " + node.getKind() + " operator"));
            }
            return true;
        }
    }
}