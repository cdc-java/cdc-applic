package cdc.applic.dictionaries.core.writing.checks;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.applic.dictionaries.checks.WritingRulePurpose;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.ExpressionLocation;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.clauses.ProverImpl;

/**
 * A Checker that checks that an expression is satisfiable with and without assertions.
 * <p>
 * If an Expression is not satisfiable without assertions, not check with assertions is done.<br>
 * If an Expression is satisfiable without assertions, a check with assertions is done.
 *
 * @author Damien Carbonne
 */
public final class IsSatisfiableChecker extends WritingRuleChecker {
    public static final String RULE_NAME = "Is satisfiable";

    static {
        register(RULE_NAME, IsSatisfiableChecker::new);
    }

    /**
     * The prover without assertions.
     * <p>
     * We use a Prover instead of a SatSystemSolver to benefit from satisfiability caches.
     */
    private final Prover proverEANR;

    /**
     * The prover with assertions.
     * <p>
     * We use a Prover instead of a SatSystemSolver to benefit from satisfiability caches.
     */
    private final Prover proverIANR;

    private IsSatisfiableChecker(DictionaryHandle handle) {
        this.proverEANR = new ProverImpl(handle, ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES);
        this.proverIANR = new ProverImpl(handle, ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES);
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }

    @Override
    public WritingRulePurpose getPurpose() {
        return WritingRulePurpose.SATISFIABILITY;
    }

    @Override
    public boolean isCompliant(CheckedData data) {
        final List<ApplicIssue> issues = new ArrayList<>();
        check(data, issues);
        return issues.isEmpty();
    }

    @Override
    public void check(CheckedData data,
                      List<ApplicIssue> issues) {
        if (!proverEANR.isSometimesTrue(data.getNode())) {
            issues.add(new WritingRuleIssue(RULE_NAME,
                                            new ExpressionLocation(data.getContext(),
                                                                   data.getContent()),
                                            "Expression is not sastisfiable, excluding assertions"));
        } else if (!proverIANR.isSometimesTrue(data.getNode())) {
            issues.add(new WritingRuleIssue(RULE_NAME,
                                            new ExpressionLocation(data.getContext(),
                                                                   data.getContent()),
                                            "Expression is not sastisfiable, including assertions"));
        }
    }
}