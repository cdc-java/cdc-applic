package cdc.applic.dictionaries.core.writing.checks.issues;

import java.util.Objects;

import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.checks.ExpressionLocation;
import cdc.util.lang.Checks;

/**
 * Specialization of WritingRuleIssue dedicated to removable nodes.
 *
 * @author Damien Carbonne
 */
@Deprecated(since = "2024-12-21", forRemoval = true)
public final class RemovableNodeIssue extends WritingRuleIssue {
    private final Node expressionNode;
    private final Node removableNode;

    private RemovableNodeIssue(Builder builder) {
        super(builder);
        this.expressionNode = Checks.isNotNull(builder.expressionNode, "expressionNode");
        this.removableNode = Checks.isNotNull(builder.removableNode, "removableNode");
    }

    @Deprecated(since = "2024-12-21", forRemoval = true)
    public RemovableNodeIssue(String ruleName,
                              String description,
                              String context,
                              Node expressionNode,
                              Node removableNode) {
        this(builder().ruleName(ruleName)
                      .description(description)
                      .location(new ExpressionLocation(context, expressionNode))
                      .expressionNode(expressionNode)
                      .removableNode(removableNode));
        // super(ruleName,
        // new ExpressionLocation(context, expressionNode),
        // description);
        // this.expressionNode = expressionNode;
        // this.removableNode = removableNode;
    }

    /**
     * @return The node corresponding to the expression.
     */
    @Deprecated(since = "2024-12-21", forRemoval = true)
    public final Node getExpressionNode() {
        return expressionNode;
    }

    /**
     * @return The node that can be removed.
     */
    @Deprecated(since = "2024-12-21", forRemoval = true)
    public final Node getRemovableNode() {
        return removableNode;
    }

    @Override
    public int hashCode() {
        return 31 * super.hashCode()
                + Objects.hash(expressionNode,
                               removableNode);
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        final RemovableNodeIssue other = (RemovableNodeIssue) object;
        return Objects.equals(expressionNode, other.expressionNode)
                && Objects.equals(removableNode, other.removableNode);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append('[');
        builder.append(getClass().getSimpleName());
        builder.append(' ');
        builder.append(getIssueType());
        builder.append(' ');
        builder.append(getDescription());
        builder.append(" '");
        builder.append(getRemovableNode().toInfix(Formatting.SHORT_NARROW));
        builder.append("' in '");
        builder.append(getExpressionNode().toInfix(Formatting.SHORT_NARROW));
        builder.append("']");
        return builder.toString();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends WritingRuleIssue.Builder<Builder> {
        private Node expressionNode;
        private Node removableNode;

        protected Builder() {
        }

        public Builder expressionNode(Node expressionNode) {
            this.expressionNode = expressionNode;
            return self();
        }

        public Builder removableNode(Node removableNode) {
            this.removableNode = removableNode;
            return self();
        }

        @Override
        public RemovableNodeIssue build() {
            return new RemovableNodeIssue(this);
        }
    }
}