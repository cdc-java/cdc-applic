package cdc.applic.dictionaries.core.writing.checks;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.dictionaries.Registry;
import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.applic.dictionaries.checks.WritingRulePurpose;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AbstractPropertyNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.visitors.AbstractCollector;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.expressions.ast.visitors.DeleteNots;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.ExpressionLocation;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.literals.Named;
import cdc.util.lang.Checks;

/**
 * Checker that checks that Properties and Aliases are used in a compliant order.
 *
 * @author Damien Carbonne
 */
public final class NamesAreOrderedChecker extends WritingRuleChecker {
    public static final String RULE_NAME = "Names are ordered";

    static {
        register(RULE_NAME, NamesAreOrderedChecker::new);
    }

    private final Registry registry;

    private NamesAreOrderedChecker(DictionaryHandle handle) {
        this.registry = handle.getRegistry();
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }

    @Override
    public WritingRulePurpose getPurpose() {
        return WritingRulePurpose.HOMOGENEITY;
    }

    @Override
    public boolean isCompliant(CheckedData data) {
        final List<ApplicIssue> issues = new ArrayList<>();
        check(data, issues);
        return issues.isEmpty();
    }

    @Override
    public void check(CheckedData data,
                      List<ApplicIssue> issues) {
        Checks.isNotNull(data, "data");
        Checks.isNotNull(issues, "issues");

        final Checker visitor = new Checker(registry, data, issues);
        final Node node1 = ConvertToNary.execute(data.getNode(), ConvertToNary.Variant.ALWAYS);
        final Node node2 = DeleteNots.execute(node1);
        node2.accept(visitor);
    }

    private static class Checker extends AbstractCollector<ApplicIssue> {
        private final Registry registry;
        private final CheckedData data;

        public Checker(Registry registry,
                       CheckedData data,
                       List<ApplicIssue> issues) {
            super(issues);
            Checks.isNotNull(registry, "registry");
            this.registry = registry;
            this.data = data;
        }

        @Override
        public Void visitNary(AbstractNaryNode node) {
            final List<NamedDItem> items = new ArrayList<>();
            for (final Node child : node.getChildren()) {
                if (child instanceof AbstractPropertyNode || child instanceof RefNode) {
                    final Name name = ((Named) child).getName();
                    final NamedDItem item = registry.getItem(name);
                    items.add(item);
                }
            }
            checkOrder(items);
            return super.visitNary(node);
        }

        private void checkOrder(List<NamedDItem> items) {
            for (int index = 1; index < items.size(); index++) {
                final int previous = items.get(index - 1).getOrdinal();
                final int current = items.get(index).getOrdinal();
                if (current < previous) {
                    final StringBuilder description = new StringBuilder();
                    description.append("Expression does not follow recommanded order:");
                    for (final NamedDItem item : items) {
                        description.append(" ");
                        description.append(item.getName());
                    }
                    add(new WritingRuleIssue(RULE_NAME,
                                             new ExpressionLocation(data.getContext(),
                                                                    data.getContent()),
                                             description.toString()));
                    return;
                }
            }
        }
    }
}