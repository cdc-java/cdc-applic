package cdc.applic.dictionaries.core.writing.checks;

import java.util.List;
import java.util.function.Predicate;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.applic.dictionaries.checks.WritingRulePurpose;
import cdc.applic.dictionaries.core.visitors.IsCompliantWithNamingConvention;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.PredicateMatcher;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.ExpressionLocation;
import cdc.util.lang.Checks;

/**
 * A checker that checks that only {@link NamingConvention#DEFAULT} is used.
 *
 * @author Damien Carbonne
 */
public final class UsesDefaultNamingConventionChecker extends WritingRuleChecker {
    public static final String RULE_NAME = "Uses default naming convention";

    private final Predicate<Node> isCompliantWithDefaultNamingConvention;

    static {
        register(RULE_NAME, UsesDefaultNamingConventionChecker::new);
    }

    private UsesDefaultNamingConventionChecker(DictionaryHandle handle) {
        Checks.isNotNull(handle, "handle");
        this.isCompliantWithDefaultNamingConvention =
                n -> IsCompliantWithNamingConvention.test(n,
                                                          handle.getDictionary(),
                                                          NamingConvention.DEFAULT);
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }

    @Override
    public WritingRulePurpose getPurpose() {
        return WritingRulePurpose.HOMOGENEITY;
    }

    @Override
    protected boolean doCheck(CheckedData data,
                              List<ApplicIssue> issues) {
        if (!PredicateMatcher.matches(data.getNode(), isCompliantWithDefaultNamingConvention)) {
            if (issues == null) {
                return false;
            }
            issues.add(new WritingRuleIssue(RULE_NAME,
                                            new ExpressionLocation(data.getContext(),
                                                                   data.getContent()),
                                            "Expression is not compliant with default naming convention."));
        }
        return true;
    }
}