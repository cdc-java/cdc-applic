package cdc.applic.dictionaries.core.writing.checks;

import java.util.List;

import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.applic.dictionaries.checks.WritingRulePurpose;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.NodePredicates;
import cdc.applic.expressions.ast.NotEqualNode;
import cdc.applic.expressions.ast.NotInNode;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.visitors.PredicateMatcher;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.ExpressionLocation;

/**
 * A checker that checks that {@link NotNode}, {@link NotEqualNode},
 * {@link NotInNode} and {@link FalseNode} are not used.
 *
 * @author Damien Carbonne
 */
public final class DoesNotUseNegationChecker extends WritingRuleChecker {
    public static final String RULE_NAME = "Does not use negation";
    public static final DoesNotUseNegationChecker INSTANCE =
            new DoesNotUseNegationChecker();

    static {
        register(RULE_NAME, c -> INSTANCE);
    }

    private DoesNotUseNegationChecker() {
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }

    @Override
    public WritingRulePurpose getPurpose() {
        return WritingRulePurpose.COMPLEXITY;
    }

    @Override
    protected boolean doCheck(CheckedData data,
                              List<ApplicIssue> issues) {
        if (PredicateMatcher.matches(data.getNode(), NodePredicates.IS_NEGATIVE)) {
            if (issues == null) {
                return false;
            } else {
                issues.add(new WritingRuleIssue(RULE_NAME,
                                                new ExpressionLocation(data.getContext(),
                                                                       data.getContent()),
                                                "Expression contains negation"));
            }
        }
        return true;
    }
}