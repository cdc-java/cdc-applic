package cdc.applic.dictionaries.core.writing.checks;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import cdc.applic.dictionaries.Registry;
import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.applic.dictionaries.checks.WritingRulePurpose;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.EnumeratedValue;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.RealType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractSetNode;
import cdc.applic.expressions.ast.visitors.AbstractCollector;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.SItemSetLocation;
import cdc.applic.expressions.content.AbstractRange;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.Domain;
import cdc.applic.expressions.content.IntegerDomain;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.RealDomain;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.content.UncheckedSet;
import cdc.applic.expressions.content.Value;
import cdc.util.lang.Checks;

/**
 * A Checker that checks that sets are normalized.
 *
 * @author Damien Carbonne
 */
public final class SetsAreNormalizedChecker extends WritingRuleChecker {
    public static final String RULE_NAME = "Sets are normalized";

    private static final String INCORRECT_ORDER = "Incorrect order in ";
    private static final String DUPLICATE_ITEMS = "Duplicate items in ";
    private static final String ADJOINT_ITEMS = "Adjoint items in ";
    private static final String EMPTY_RANGE = "Empty range in ";
    private static final String SINGLETON_RANGE = "Singleton range in ";

    static {
        register(RULE_NAME, SetsAreNormalizedChecker::new);
    }

    private final Registry registry;

    private SetsAreNormalizedChecker(DictionaryHandle handle) {
        this.registry = handle.getRegistry();
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }

    @Override
    public WritingRulePurpose getPurpose() {
        return WritingRulePurpose.HOMOGENEITY;
    }

    @Override
    public boolean isCompliant(CheckedData data) {
        final List<ApplicIssue> issues = new ArrayList<>();
        check(data, issues);
        return issues.isEmpty();
    }

    @Override
    public void check(CheckedData data,
                      List<ApplicIssue> issues) {
        final Checker visitor = new Checker(registry, data.getContext(), issues);
        data.getNode().accept(visitor);
    }

    private static class Checker extends AbstractCollector<ApplicIssue> {
        private final Registry registry;
        private final String context;

        public Checker(Registry registry,
                       String context,
                       List<ApplicIssue> issues) {
            super(issues);
            Checks.isNotNull(registry, "registry");
            this.registry = registry;
            this.context = context;
        }

        @Override
        public Void visitLeaf(AbstractLeafNode node) {
            if (node instanceof AbstractSetNode) {
                checkSetNode((AbstractSetNode) node);
            }
            return super.visitLeaf(node);
        }

        private void checkSetNode(AbstractSetNode node) {
            final Property property = registry.getProperty(node.getName());
            final SItemSet set = node.getSet();
            final Type type = property.getType();
            checkSet(set, type);
        }

        private void checkSet(SItemSet set,
                              Type type) {
            SetsAreNormalizedChecker.checkSet(context, set, type, getCollection());
        }
    }

    /**
     * Checks that a set is correctly normalized.
     * <p>
     * {@link BooleanSet}, {@link IntegerSet} and {@link RealSet} are normalized by construction.<br>
     * {@link StringSet} and {@link UncheckedSet} need special verifications.
     *
     *
     * @param context The set context.
     * @param set The set to check.
     * @param type The associated type.<br>
     *            Necessary to check string content (pattern set and enumerated set).<br>
     *            Otherwise optional.<br>
     *            <b>WARNING:</b> Must be compliant with set content.
     * @param issues The list of detected issues.
     */
    public static void checkSet(String context,
                                SItemSet set,
                                Type type,
                                Collection<ApplicIssue> issues) {
        if (!set.isEmpty() && (set instanceof UncheckedSet || set instanceof StringSet)) {
            if (type instanceof EnumeratedType) {
                checkEnumeratedSet(context, set, (EnumeratedType) type, issues);
            } else if (type instanceof PatternType) {
                checkPatternSet(context, set, issues);
            } else if (type instanceof IntegerType || set.isCompliantWith(IntegerSet.class)) {
                checkDomainSet(context, set, IntegerDomain.INSTANCE, issues);
            } else if (type instanceof RealType || set.isCompliantWith(RealSet.class)) {
                checkDomainSet(context, set, RealDomain.INSTANCE, issues);
            }
        }
    }

    public static List<ApplicIssue> checkSet(String context,
                                             SItemSet set,
                                             Type type) {
        final List<ApplicIssue> issues = new ArrayList<>();
        checkSet(context, set, type, issues);
        return issues;
    }

    private static <V extends Value & Comparable<? super V>, R extends AbstractRange<V, R>> void checkDomainSet(String context,
                                                                                                                SItemSet set,
                                                                                                                Domain<V, R> domain,
                                                                                                                Collection<ApplicIssue> issues) {
        boolean wrongOrder = false;
        boolean emptyRange = false;
        boolean singletonRange = false;
        boolean adjointItems = false;
        V previous = null;

        for (final SItem item : set.getItems()) {
            if (domain.getValueClass().isInstance(item)) {
                final V value = domain.getValueClass().cast(item);
                if (previous != null) {
                    final int cmp = previous.compareTo(value);
                    if (cmp > 0) {
                        wrongOrder = true;
                    } else if (cmp == 0 || domain.succ(previous).equals(value)) {
                        adjointItems = true;
                    }
                }
                previous = value;
            } else {
                final R range = domain.getRangeClass().cast(item);
                if (range.isEmpty()) {
                    emptyRange = true;
                    // don't change previous
                } else {
                    if (range.isSingleton()) {
                        singletonRange = true;
                    }
                    if (previous != null) {
                        final int cmp = previous.compareTo(range.getMin());
                        if (cmp > 0) {
                            wrongOrder = true;
                        } else if (cmp == 0 || domain.succ(previous).equals(range.getMin())) {
                            adjointItems = true;
                        }
                    }
                    previous = range.getMax();
                }
            }
        }
        SItemSetLocation location = null;
        if (wrongOrder) {
            location = new SItemSetLocation(context, set);
            issues.add(new WritingRuleIssue(RULE_NAME,
                                            location,
                                            INCORRECT_ORDER + set));
        }
        if (emptyRange) {
            if (location == null) {
                location = new SItemSetLocation(context, set);
            }
            issues.add(new WritingRuleIssue(RULE_NAME,
                                            location,
                                            EMPTY_RANGE + set));
        }
        if (singletonRange) {
            if (location == null) {
                location = new SItemSetLocation(context, set);
            }
            issues.add(new WritingRuleIssue(RULE_NAME,
                                            location,
                                            SINGLETON_RANGE + set));
        }
        if (adjointItems) {
            if (location == null) {
                location = new SItemSetLocation(context, set);
            }
            issues.add(new WritingRuleIssue(RULE_NAME,
                                            location,
                                            ADJOINT_ITEMS + set));
        }
    }

    private static void checkEnumeratedSet(String context,
                                           SItemSet set,
                                           EnumeratedType type,
                                           Collection<ApplicIssue> issues) {
        final List<? extends SItem> items = set.getItems();
        boolean wrongOrder = false;
        for (int index = 0; index < items.size() - 1; index++) {
            final StringValue x = (StringValue) items.get(index);
            final StringValue y = (StringValue) items.get(index + 1);
            final EnumeratedValue vx = type.getValue(x);
            final EnumeratedValue vy = type.getValue(y);
            if (vx.getOrdinal() > vy.getOrdinal()) {
                wrongOrder = true;
            }
        }
        SItemSetLocation location = null;
        if (wrongOrder) {
            location = new SItemSetLocation(context, set);
            issues.add(new WritingRuleIssue(RULE_NAME,
                                            location,
                                            INCORRECT_ORDER + set));
        }
        final int setSize = set.getChecked().getItems().size();
        if (items.size() != setSize) {
            if (location == null) {
                location = new SItemSetLocation(context, set);
            }
            issues.add(new WritingRuleIssue(RULE_NAME,
                                            location,
                                            DUPLICATE_ITEMS + set));
        }
    }

    private static void checkPatternSet(String context,
                                        SItemSet set,
                                        Collection<ApplicIssue> issues) {
        final List<? extends SItem> items = set.getItems();
        boolean wrongOrder = false;
        for (int index = 0; index < items.size() - 1; index++) {
            final StringValue x = (StringValue) items.get(index);
            final StringValue y = (StringValue) items.get(index + 1);
            if (x.compareTo(y) > 0) {
                // Either wrong order or duplicates
                wrongOrder = true;
            }
        }
        SItemSetLocation location = null;
        if (wrongOrder) {
            location = new SItemSetLocation(context, set);
            issues.add(new WritingRuleIssue(RULE_NAME,
                                            location,
                                            INCORRECT_ORDER + set));
        }
        final int setSize = set.getChecked().getItems().size();
        if (items.size() != setSize) {
            if (location == null) {
                location = new SItemSetLocation(context, set);
            }
            issues.add(new WritingRuleIssue(RULE_NAME,
                                            location,
                                            DUPLICATE_ITEMS + set));
        }
    }
}