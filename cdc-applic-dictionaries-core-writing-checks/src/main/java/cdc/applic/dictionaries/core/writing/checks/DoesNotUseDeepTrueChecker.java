package cdc.applic.dictionaries.core.writing.checks;

import java.util.List;

import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.applic.dictionaries.checks.WritingRulePurpose;
import cdc.applic.expressions.ast.NodePredicates;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.PredicateCounter;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.ExpressionLocation;

/**
 * A Checker that checks that {@link TrueNode} is not used <b>deeply</b>.
 *
 * @author Damien Carbonne
 */
public final class DoesNotUseDeepTrueChecker extends WritingRuleChecker {
    public static final String RULE_NAME = "Does not use deep true";
    public static final DoesNotUseDeepTrueChecker INSTANCE =
            new DoesNotUseDeepTrueChecker();

    static {
        register(RULE_NAME, c -> INSTANCE);
    }

    private DoesNotUseDeepTrueChecker() {
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }

    @Override
    public WritingRulePurpose getPurpose() {
        return WritingRulePurpose.COMPLEXITY;
    }

    @Override
    protected boolean doCheck(CheckedData data,
                              List<ApplicIssue> issues) {
        final int count = PredicateCounter.count(data.getNode(), NodePredicates.IS_TRUE);
        if (count > 0 && !(data.getNode() instanceof TrueNode)) {
            if (issues == null) {
                return false;
            }
            issues.add(new WritingRuleIssue(RULE_NAME,
                                            new ExpressionLocation(data.getContext(),
                                                                   data.getContent()),
                                            "Expression contains deep true"));
        }
        return true;
    }
}