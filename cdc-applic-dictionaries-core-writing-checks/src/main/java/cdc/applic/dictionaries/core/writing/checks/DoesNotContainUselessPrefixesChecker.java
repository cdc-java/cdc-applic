package cdc.applic.dictionaries.core.writing.checks;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.applic.dictionaries.checks.WritingRulePurpose;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.RenamableNode;
import cdc.applic.expressions.ast.visitors.PredicateCollector;
import cdc.applic.expressions.ast.visitors.PredicateMatcher;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.ExpressionLocation;

/**
 * A checker that checks absence of useless prefixes.
 *
 * @author Damien Carbonne
 */
public final class DoesNotContainUselessPrefixesChecker extends WritingRuleChecker {
    public static final String RULE_NAME = "Does not contain useless prefixes";

    private final DictionaryHandle handle;

    static {
        register(RULE_NAME, DoesNotContainUselessPrefixesChecker::new);
    }

    private DoesNotContainUselessPrefixesChecker(DictionaryHandle handle) {
        this.handle = handle;
    }

    /**
     * @param dictionary The dictionary.
     * @return A Predicate that detects nodes containing a prefix
     *         and are declared in the registry of {@code dictionary}.
     */
    public static Predicate<Node> containsPrefix(Dictionary dictionary) {
        return node -> {
            if (dictionary.getPrefix().isPresent() && node instanceof final RenamableNode n) {
                if (!n.getName().hasPrefix()) {
                    // node has no prefix
                    return false;
                } else {
                    // Node has a prefix
                    // Node is locally declared
                    return dictionary.getRegistry().hasDeclaredItem(n.getName().getLocal());
                }
            } else {
                return false;
            }
        };
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }

    @Override
    public WritingRulePurpose getPurpose() {
        return WritingRulePurpose.HOMOGENEITY;
    }

    @Override
    protected boolean doCheck(CheckedData data,
                              List<ApplicIssue> issues) {
        final Predicate<Node> predicate = containsPrefix(handle.getDictionary());
        if (issues == null) {
            return !PredicateMatcher.matches(data.getNode(), predicate);
        } else {
            final List<Node> nodes = new ArrayList<>();
            PredicateCollector.collect(data.getNode(), predicate, nodes);
            for (final Node node : nodes) {
                issues.add(new WritingRuleIssue(RULE_NAME,
                                                new ExpressionLocation(data.getContext(),
                                                                       data.getContent()),
                                                "Expression contains useless prefix " + ((RenamableNode) node).getName()));
            }
            return true;
        }
    }
}