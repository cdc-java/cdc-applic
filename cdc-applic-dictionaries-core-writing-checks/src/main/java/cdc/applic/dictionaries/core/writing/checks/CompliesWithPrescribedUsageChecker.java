package cdc.applic.dictionaries.core.writing.checks;

import java.util.List;
import java.util.Set;

import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.applic.dictionaries.checks.WritingRulePurpose;
import cdc.applic.dictionaries.core.visitors.CollectNamedDItemNames;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.ExpressionLocation;
import cdc.applic.expressions.literals.Name;

/**
 * Checker that checks that usage of DItems (Properties and Aliases) is compliant with guidelines.
 *
 * @author Damien Carbonne
 */
public final class CompliesWithPrescribedUsageChecker extends WritingRuleChecker {
    public static final String RULE_NAME = "Complies with prescribed usage";
    private final Dictionary dictionary;

    static {
        register(RULE_NAME, CompliesWithPrescribedUsageChecker::new);
    }

    private CompliesWithPrescribedUsageChecker(DictionaryHandle handle) {
        this.dictionary = handle.getDictionary();
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }

    @Override
    public WritingRulePurpose getPurpose() {
        return WritingRulePurpose.HOMOGENEITY;
    }

    @Override
    protected boolean doCheck(CheckedData data,
                              List<ApplicIssue> issues) {
        final Node node = data.getNode();
        final Set<Name> names = CollectNamedDItemNames.collect(node);
        for (final NamedDItem item : dictionary.getAllowedItems(DItemUsage.MANDATORY)) {
            if (!names.contains(item.getName())) {
                if (issues == null) {
                    return false;
                }
                issues.add(new WritingRuleIssue(RULE_NAME,
                                                new ExpressionLocation(data.getContext(),
                                                                       data.getContent()),
                                                "Expression does not use MANDATORY '" + item.getName() + "'"));
            }
        }
        return true;
    }
}