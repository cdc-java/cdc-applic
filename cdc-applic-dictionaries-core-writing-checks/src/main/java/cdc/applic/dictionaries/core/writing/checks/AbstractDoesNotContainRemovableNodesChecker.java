package cdc.applic.dictionaries.core.writing.checks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.checks.WritingRulePurpose;
import cdc.applic.dictionaries.core.writing.checks.issues.RemovableNodeIssue;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.AbstractCollector;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.ProverMatching;
import cdc.applic.proofs.core.clauses.ProverImpl;

/**
 * Abstract Checker that detects presence of nodes that can be removed.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractDoesNotContainRemovableNodesChecker extends WritingRuleChecker {
    /**
     * The prover.
     * <p>
     * We use a Prover instead of a SatSystemSolver to benefit from satisfiability caches.
     */
    private final Prover prover;
    private final String ruleName;
    private final String features;

    protected AbstractDoesNotContainRemovableNodesChecker(DictionaryHandle handle,
                                                          ProverFeatures proverFeatures,
                                                          String ruleName,
                                                          String features) {
        this.prover = new ProverImpl(handle, proverFeatures);
        this.ruleName = ruleName;
        this.features = features;
    }

    @Override
    public final WritingRulePurpose getPurpose() {
        return WritingRulePurpose.COMPLEXITY;
    }

    @Override
    public final boolean isCompliant(CheckedData data) {
        final List<RemovableNodeIssue> issues = check(data.getContext(), data.getNode());
        return issues.isEmpty();
    }

    @Override
    public final void check(CheckedData data,
                            List<ApplicIssue> issues) {
        issues.addAll(check(data.getContext(), data.getNode()));
    }

    private List<RemovableNodeIssue> check(String context,
                                           Node node) {
        if (node instanceof TrueNode || node instanceof FalseNode) {
            return Collections.emptyList();
        } else {
            final Checker visitor = new Checker(context, node, prover, ruleName, features);
            node.accept(visitor);
            return visitor.getList();
        }
    }

    private static final class Checker extends AbstractCollector<RemovableNodeIssue> {
        private final String context;
        private final Node expressionNode;
        private final Prover prover;
        private final String ruleName;
        private final String features;

        public Checker(String context,
                       Node expressionNode,
                       Prover prover,
                       String ruleName,
                       String features) {
            super(new ArrayList<>());
            this.context = context;
            this.expressionNode = expressionNode;
            this.prover = prover;
            this.ruleName = ruleName;
            this.features = features;
        }

        private ProverMatching check(Node node) {
            final ProverMatching matching = prover.getMatching(node);
            if (matching == ProverMatching.ALWAYS) {
                add(new RemovableNodeIssue(ruleName,
                                           "Always true (" + features + ")",
                                           context,
                                           expressionNode,
                                           node));
            } else if (matching == ProverMatching.NEVER) {
                add(new RemovableNodeIssue(ruleName,
                                           "Always false (" + features + ")",
                                           context,
                                           expressionNode,
                                           node));
            }
            return matching;
        }

        @Override
        public Void visitLeaf(AbstractLeafNode node) {
            return null;
        }

        @Override
        public Void visitUnary(AbstractUnaryNode node) {
            final ProverMatching matching = check(node);
            if (matching == ProverMatching.SOMETIMES) {
                // Continue deeper
                return super.visitUnary(node);
            } else {
                // Interrupt
                return null;
            }
        }

        @Override
        public Void visitBinary(AbstractBinaryNode node) {
            final ProverMatching matching = check(node);
            if (matching == ProverMatching.SOMETIMES) {
                // Continue deeper
                return super.visitBinary(node);
            } else {
                // Interrupt
                return null;
            }
        }

        @Override
        public Void visitNary(AbstractNaryNode node) {
            final ProverMatching matching = check(node);
            if (matching == ProverMatching.SOMETIMES) {
                // Continue deeper
                return super.visitNary(node);
            } else {
                // Interrupt
                return null;
            }
        }
    }
}