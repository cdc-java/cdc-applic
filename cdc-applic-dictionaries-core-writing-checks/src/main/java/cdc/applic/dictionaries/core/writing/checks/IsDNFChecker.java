package cdc.applic.dictionaries.core.writing.checks;

import java.util.List;

import cdc.applic.dictionaries.checks.WritingRuleChecker;
import cdc.applic.dictionaries.checks.WritingRuleIssue;
import cdc.applic.dictionaries.checks.WritingRulePurpose;
import cdc.applic.expressions.ast.visitors.IsDNF;
import cdc.applic.expressions.checks.ApplicIssue;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.checks.ExpressionLocation;

/**
 * A Checker that checks that an expression is DNF.
 *
 * @author Damien Carbonne
 */
public final class IsDNFChecker extends WritingRuleChecker {
    public static final String RULE_NAME = "Is DNF";
    public static final IsDNFChecker INSTANCE =
            new IsDNFChecker();

    static {
        register(RULE_NAME, c -> INSTANCE);
    }

    private IsDNFChecker() {
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }

    @Override
    public WritingRulePurpose getPurpose() {
        return WritingRulePurpose.COMPLEXITY;
    }

    @Override
    protected boolean doCheck(CheckedData data,
                              List<ApplicIssue> issues) {
        if (!IsDNF.isDNF(data.getNode())) {
            if (issues == null) {
                return false;
            }
            issues.add(new WritingRuleIssue(RULE_NAME,
                                            new ExpressionLocation(data.getContext(),
                                                                   data.getContent()),
                                            "Expression is not DNF"));
        }
        return true;
    }
}