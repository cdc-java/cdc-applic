package cdc.applic.dictionaries.core.writing.checks;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.proofs.ProverFeatures;

/**
 * Checker that detects presence of nodes that can be removed
 * (excluding assertions and no reserves).
 *
 * @author Damien Carbonne
 */
public final class DoesNotContainRemovableNodesEANRChecker extends AbstractDoesNotContainRemovableNodesChecker {
    public static final String RULE_NAME = "Does not contain removable nodes (excluding assertions, no reserves)";

    static {
        register(RULE_NAME, DoesNotContainRemovableNodesEANRChecker::new);
    }

    private DoesNotContainRemovableNodesEANRChecker(DictionaryHandle handle) {
        super(handle,
              ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES,
              RULE_NAME,
              "exluding assertions, no reserves");
    }

    public static void elaborate() {
        // Ignore
    }

    @Override
    public String getRuleName() {
        return RULE_NAME;
    }
}