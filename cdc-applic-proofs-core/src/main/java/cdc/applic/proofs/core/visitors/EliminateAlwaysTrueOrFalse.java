package cdc.applic.proofs.core.visitors;

import java.util.function.Supplier;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.ast.visitors.QualifiedValue;
import cdc.applic.expressions.ast.visitors.QualifiedValue.Quality;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.ProverMatching;
import cdc.applic.proofs.core.clauses.ProverImpl;

/**
 * Converter that recursively check matching of nodes and applies those rewriting rules:
 * <ul>
 * <li>matching(&pi;)={@link ProverMatching#NEVER NEVER} &equiv; &perp;
 * <li>matching(&pi;)={@link ProverMatching#ALWAYS ALWAYS} &equiv; &top;
 * </ul>
 * <b>WARNING:</b>this is an expensive converter, useful for simplification.
 *
 * @author Damien Carbonne
 */
public final class EliminateAlwaysTrueOrFalse extends AbstractConverter {
    private static final Logger LOGGER = LogManager.getLogger(EliminateAlwaysTrueOrFalse.class);
    private final Prover prover;
    private final int maxMillis;
    private final long start;
    private boolean active = true;
    private QualifiedValue.Quality quality = Quality.SUCCESS;

    private EliminateAlwaysTrueOrFalse(DictionaryHandle handle,
                                       ProverFeatures proverFeatures,
                                       int maxMillis) {
        this.prover = new ProverImpl(handle, proverFeatures);
        this.maxMillis = maxMillis;
        this.start = System.nanoTime();
    }

    /**
     * If {@link #maxMillis} is greater or equal to 0, checks that elapsed time
     * has not been overtaken.<br>
     * If it is the case, {@link #active} is set to {@code false}.
     */
    private void checkElapsedTime() {
        if (maxMillis >= 0 && active) {
            final int elapsedMillis = (int) ((System.nanoTime() - start) / 1_000_000L);
            active = elapsedMillis < maxMillis;
            if (!active) {
                LOGGER.warn("Reached timeout {} ms", maxMillis);
                quality = QualifiedValue.Quality.PARTIAL;
            }
        }
    }

    public static QualifiedValue<Node> execute(Node node,
                                               DictionaryHandle handle,
                                               ProverFeatures proverFeatures,
                                               int maxMillis) {
        final EliminateAlwaysTrueOrFalse visitor =
                new EliminateAlwaysTrueOrFalse(handle,
                                               proverFeatures,
                                               maxMillis);
        final Node value = node.accept(visitor);
        return new QualifiedValue<>(value, visitor.quality);
    }

    private Node visit(Node node,
                       Supplier<Node> def) {
        checkElapsedTime();

        if (active) {
            final ProverMatching matching = prover.getMatching(node);
            if (matching == ProverMatching.NEVER) {
                return FalseNode.INSTANCE;
            } else if (matching == ProverMatching.ALWAYS) {
                return TrueNode.INSTANCE;
            } else {
                return def.get();
            }
        } else {
            return def.get();
        }
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        return visit(node, () -> super.visitLeaf(node));
    }

    @Override
    public Node visitUnary(AbstractUnaryNode node) {
        return visit(node, () -> super.visitUnary(node));
    }

    @Override
    public Node visitBinary(AbstractBinaryNode node) {
        return visit(node, () -> super.visitBinary(node));
    }

    @Override
    public Node visitNary(AbstractNaryNode node) {
        return visit(node, () -> super.visitNary(node));
    }
}