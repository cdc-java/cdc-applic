package cdc.applic.proofs.core.clauses;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.handles.ApplicCache;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.Alias;
import cdc.applic.dictionaries.items.Assertion;
import cdc.applic.dictionaries.items.DExpressed;
import cdc.applic.dictionaries.items.DItem;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.RealType;
import cdc.applic.expressions.ast.EquivalenceNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.proofs.core.utils.PropertyDefinition;
import cdc.util.debug.Verbosity;
import cdc.util.function.IterableUtils;

/**
 * A DictionarySentencesCache computes and stores data that only depend on a Dictionary.
 *
 * <h2>Definition sentences</h2>
 * A <b>definition {@link Sentence}</b> of Properties whose type is {@link EnumeratedType Enumerated} can be precomputed.
 * <p>
 * A {@link BooleanType Boolean} Property has no useful definition Sentence.<br>
 * Other Properties ({@link PatternType Pattern}, {@link IntegerType Integer} and {@link RealType Real}) can not have precomputed
 * definition Sentence.
 *
 * <h2>Other data</h2>
 * The following data are also cached for Expressed:
 * <ul>
 * <li>The sentence translation of the Expressed
 * <li>
 * </ul>
 *
 * @author Damien Carbonne
 */
public class DictionarySentencesCache extends ApplicCache {
    private boolean valid = false;

    /** Map from each Expressed to its corresponding sentence. */
    private final Map<DExpressed, Sentence> expressedSentences = new HashMap<>();

    /** Map from certain properties to their corresponding definition sentence. */
    @SuppressWarnings("unchecked")
    private final Map<Property, Sentence>[] propertyDefinitionSentences = new Map[3];

    public DictionarySentencesCache(DictionaryHandle handle) {
        super(handle, null);

        for (int index = 0; index < propertyDefinitionSentences.length; index++) {
            propertyDefinitionSentences[index] = new HashMap<>();
        }
    }

    public static DictionarySentencesCache get(DictionaryHandle handle) {
        return handle.computeIfAbsent(DictionarySentencesCache.class, DictionarySentencesCache::new);
    }

    public void validate() {
        checkSerial();
        if (!valid) {
            clear();

            int index = 0;

            // Build sentences associated to NamedItems
            for (final NamedDItem item : getDictionary().getRegistry().getAllItems()) {
                index++;
                fillSentence(item, index);
            }

            // Build sentences associated to Assertions
            for (final Assertion assertion : getDictionary().getAllAssertions()) {
                index++;
                fillSentence(assertion, index);
            }

            valid = true;
        }
    }

    @Override
    public void invalidate() {
        valid = false;
    }

    @Override
    public void clear() {
        valid = false;
        expressedSentences.clear();
        for (int index = 0; index < propertyDefinitionSentences.length; index++) {
            propertyDefinitionSentences[index].clear();
        }
    }

    private void fillSentence(DItem item,
                              int index) {
        if (item instanceof final Assertion assertion) {
            // Unit propagation ?
            final Sentence sentence = SentenceBuilder.convert(getRegistry(),
                                                              assertion.getQualifiedExpression().getRootNode(),
                                                              "#EXP" + index + "#");
            expressedSentences.put(assertion, sentence);
        } else if (item instanceof final Alias alias) {
            // Unit propagation ?
            final Node node = new EquivalenceNode(new RefNode(alias.getName()),
                                                  alias.getQualifiedExpression().getRootNode());
            final Sentence sentence = SentenceBuilder.convert(getRegistry(),
                                                              node,
                                                              "#EXP" + index + "#");
            expressedSentences.put(alias, sentence);
        } else {
            final Property property = (Property) item;
            if (property.getType() instanceof EnumeratedType
                    || property.getType() instanceof IntegerType
                    || property.getType() instanceof RealType) {
                for (final ReserveStrategy reserveStrategy : ReserveStrategy.values()) {
                    final Node node = PropertyDefinition.buildPropertyNodeCNF(property, reserveStrategy);
                    final Sentence sentence = SentenceBuilder.convertPrepared(node);
                    propertyDefinitionSentences[reserveStrategy.ordinal()].put(property, sentence);
                }
            }
        }
    }

    /**
     * Returns the precomputed sentence associated to an Expressed, or {@code null}.
     * <p>
     * Expressed have an associated sentence.<br>
     * Enumerated type properties have an associated sentence.
     *
     * @param expressed The PExpressed.
     * @return The precomputed sentence associated to {@code expressed}, or {@code null}.
     */
    public Sentence getPExpressedSentence(DExpressed expressed) {
        validate();
        return expressedSentences.get(expressed);
    }

    /**
     * Returns the precomputed definition sentence associated to a Property, or {@code null}.
     *
     * @param property The Property.
     * @param reserveStrategy The ReserveStrategy to adopt.
     * @return The precomputed definition sentence associated to {@code property} and {@code reserveStrategy}, or {@code null}.
     */
    public Sentence getDefinitionSentence(Property property,
                                          ReserveStrategy reserveStrategy) {
        validate();
        return propertyDefinitionSentences[reserveStrategy.ordinal()].get(property);
    }

    @Override
    public void print(PrintStream out,
                      int level,
                      Verbosity verbosity) {
        indent(out, level);
        out.println(valid ? "Valid" : "Invalid");

        if (valid) {
            indent(out, level);
            out.println("PExpressed Sentences (" + expressedSentences.size() + ")");
            if (verbosity != Verbosity.ESSENTIAL) {
                for (final DExpressed expressed : IterableUtils.toSortedList(expressedSentences.keySet(), DItem.COMPARATOR)) {
                    indent(out, level + 1);
                    out.println(expressed + ": " + getPExpressedSentence(expressed));
                }
            }

            for (final ReserveStrategy reserveStrategy : ReserveStrategy.values()) {
                indent(out, level);
                out.println("Properties Definition Sentences [" + reserveStrategy + "] ("
                        + propertyDefinitionSentences[reserveStrategy.ordinal()].keySet().size() + ")");
                if (verbosity != Verbosity.ESSENTIAL) {
                    for (final Property property : IterableUtils.toSortedList(propertyDefinitionSentences[reserveStrategy.ordinal()].keySet(),
                                                                              DItem.COMPARATOR)) {
                        indent(out, level + 1);
                        out.println(property + ": " + getDefinitionSentence(property, reserveStrategy));
                    }
                }
            }
        }
    }
}