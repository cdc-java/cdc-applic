package cdc.applic.proofs.core.sat4j;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;

import cdc.applic.proofs.SatSolverException;
import cdc.applic.proofs.core.SatSystemSolver;
import cdc.applic.proofs.core.clauses.SatSystem;

/**
 * Implementation of {@link SatSystemSolver} based on SAT4J.
 *
 * @author Damien Carbonne
 */
public class Sat4jSystemSolver implements SatSystemSolver {
    private static final Logger LOGGER = LogManager.getLogger(Sat4jSystemSolver.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.TRACE).buildPrintStream();

    private final ISolver instance = SolverFactory.newLight();

    public Sat4jSystemSolver() {
        instance.setLogPrefix("SAT4J");
    }

    private static SatSolverException newSatSolverException(String message,
                                                            Throwable e,
                                                            SatSystem system) {
        final StringBuilder builder = new StringBuilder();

        builder.append(message);
        builder.append("\n");
        builder.append("   features" + system.getProverFeatures() + "\n");
        builder.append("   main expression: " + system.getMainExpression() + "\n");
        builder.append("   main node: " + system.getMainNode() + "\n");
        builder.append("   main sentence: " + system.getMainSentence() + "\n");
        builder.append("   full sentence: " + system.getFullSentence());
        return new SatSolverException(builder.toString(), e);
    }

    @Override
    public boolean isSatisfiable(SatSystem system) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("isSatisfiable(...)");
            system.print(OUT, 1);
            system.getDictionaryHandle().print(OUT, 1);
        }

        // SAT4J does not accept negative values.
        // This does not seem to be documented
        if (system.getProverFeatures().getTimeoutMillis() >= 0) {
            instance.setTimeoutMs(system.getProverFeatures().getTimeoutMillis());
        } else {
            instance.setTimeoutMs(Integer.MAX_VALUE);
        }

        // Build data
        final int[][] numbers = system.getNumbers();

        // Convert data to SAT4J format
        instance.reset();
        instance.newVar(system.getNumberOfAtoms());

        for (final int[] cnumbers : numbers) {
            if (cnumbers.length == 0) {
                // Empty clause : contradiction
                instance.reset();
                return false;
            }
            final VecInt vec = new VecInt(cnumbers);
            try {
                instance.addClause(vec);
            } catch (final ContradictionException e) {
                // A trivial contradiction was found.
                instance.reset();
                return false;
            }
        }

        // Run SAT4J
        try {
            return instance.isSatisfiable();
        } catch (final TimeoutException e) {
            throw newSatSolverException("Timeout", e, system);
        } catch (final AssertionError e) {
            throw newSatSolverException("Unexpected assertion error", e, system);
        } catch (final RuntimeException e) {
            throw newSatSolverException("Unexpected exception", e, system);
        } finally {
            instance.reset();
        }
    }
}