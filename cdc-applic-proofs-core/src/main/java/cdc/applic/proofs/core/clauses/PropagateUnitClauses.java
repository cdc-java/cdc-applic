package cdc.applic.proofs.core.clauses;

import java.util.HashSet;
import java.util.Set;

import cdc.applic.expressions.ast.AtomNode;
import cdc.applic.proofs.core.ast.AtomVarNode;

/**
 * Utility to propagate unity clauses.
 * <p>
 * Current implementation is naive. There are much efficient algorithms.
 *
 * @author Damien Carbonne
 */
public final class PropagateUnitClauses {
    private static final PropagateUnitClauses[] INSTANCE = new PropagateUnitClauses[Variant.values().length];
    static {
        for (final Variant variant : Variant.values()) {
            INSTANCE[variant.ordinal()] = new PropagateUnitClauses(variant);
        }
    }

    private final Variant variant;

    public enum Variant {
        ONLY_VARS,
        ALL
    }

    private PropagateUnitClauses(Variant variant) {
        this.variant = variant;
    }

    public static Sentence execute(Sentence sentence,
                                   Variant variant) {
        final PropagateUnitClauses instance = INSTANCE[variant.ordinal()];
        return instance.execute(sentence);
    }

    private boolean isUnitClause(Clause clause) {
        if (clause.isUnitClause()) {
            if (variant == Variant.ONLY_VARS) {
                final Literal literal = clause.getUnitLiteral();
                final AtomNode atom = literal.getAtom();
                return atom instanceof AtomVarNode;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    private Sentence execute(Sentence sentence) {
        // TODO use a linear algorithm (Hantao Zhang algorithm ?)

        // Non unit clauses
        final Set<Clause> clauses = new HashSet<>();
        // Unit clauses
        final Set<Clause> units = new HashSet<>();

        for (final Clause clause : sentence.getClauses()) {
            if (isUnitClause(clause)) {
                final Clause opp = clause.getOpposite();
                if (units.contains(opp)) {
                    // Contradiction
                    return Sentence.FALSE;
                } else {
                    units.add(clause);
                }
            } else {
                clauses.add(clause);
            }
        }

        final Set<Clause> nextClauses = new HashSet<>();

        while (!units.isEmpty()) {
            final Clause unit = units.iterator().next();
            final Literal unitLiteral = unit.getUnitLiteral();
            for (final Clause clause : clauses) {
                final Clause c = clause.assertLiteral(unitLiteral);
                if (isUnitClause(c)) {
                    final Clause opp = c.getOpposite();
                    if (units.contains(opp)) {
                        // Contradiction
                        return Sentence.FALSE;
                    } else {
                        units.add(c);
                    }
                } else {
                    nextClauses.add(c);
                }
            }
            clauses.clear();
            clauses.addAll(nextClauses);
            nextClauses.clear();
            units.remove(unit);
        }

        return new Sentence(clauses);
    }
}