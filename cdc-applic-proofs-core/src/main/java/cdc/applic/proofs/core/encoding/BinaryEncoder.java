package cdc.applic.proofs.core.encoding;

import java.util.List;

import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.proofs.core.ast.AtomVarNode;
import cdc.util.lang.Checks;

public final class BinaryEncoder implements Encoder {
    public static final BinaryEncoder INSTANCE = new BinaryEncoder();

    private static boolean isSet(int value,
                                 int bit) {
        return (value & (1 << bit)) != 0;
    }

    @Override
    public void addAtMostOne(List<? extends Node> literals,
                             List<Node> clauses,
                             String prefix) {
        Checks.isNotNullOrEmpty(literals, "literals");

        // p being the smaller integer greater than log_2(n), this encoding adds p
        // variables and n*p clauses.
        final int n = literals.size();
        // Smaller integer greater than log_2(n)
        // Number of created variables
        // final int p = (int) Math.ceil(Math.log(n) / Math.log(2));
        // final int k = (int) Math.pow(2, p) - n;
        final int p = 32 - Integer.numberOfLeadingZeros(n - 1);
        final int k = (1 << p) - n;

        if (p == 0) {
            return;
        }

        final AtomVarNode y[] = new AtomVarNode[p];
        final NotNode ny[] = new NotNode[p];
        for (int index = 0; index < p; index++) {
            y[index] = new AtomVarNode(prefix + index);
            ny[index] = new NotNode(y[index]);
        }

        for (int i = 0; i < k; i++) {
            final int bin = i;
            final Node alpha = new NotNode(literals.get(i));

            for (int j = 0; j < p - 1; j++) {
                final Node beta;
                final int bit = p - 2 - j;
                final boolean bitset = isSet(bin, bit);
                if (bitset) {
                    beta = y[j];
                } else {
                    beta = ny[j];
                }
                clauses.add(new OrNode(alpha, beta));
            }
        }

        for (int i = k; i < n; i++) {
            final int bin = 2 * k + i - k;
            final Node alpha = new NotNode(literals.get(i));

            for (int j = 0; j < p; j++) {
                final Node beta;
                final int bit = p - 1 - j;
                final boolean bitset = isSet(bin, bit);
                if (bitset) {
                    beta = y[j];
                } else {
                    beta = ny[j];
                }
                clauses.add(new OrNode(alpha, beta));
            }
        }
    }
}