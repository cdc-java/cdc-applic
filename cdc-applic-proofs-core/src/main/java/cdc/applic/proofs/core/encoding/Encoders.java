package cdc.applic.proofs.core.encoding;

import java.util.List;

import cdc.applic.expressions.ast.Node;

/**
 * Utilities that can encode constraints such as {@code at most one}, {@code exactly one}
 * or {@code at least one}.
 * <p>
 * In some cases there are 2 variants of encoding:
 * <ul>
 * <li>An encoding that <em>CAN</em> be negated. It is guaranteed that result does not contain any variable.
 * <li>An encoding that <em>CANNOT</em> be negated. The result can contain variables.
 * </ul>
 * <b>Note:</b> Some transformations {@code T} (such as Tseitin) can introduce variables.
 * The obtained formula has the same satisfiability as the initial system: SAT(T(x)) = SAT(x).
 * However one should not inadvertently assume that SAT(not(T(x)) == SAT(not(x)).<br>
 * If such negation is necessary, on should use negatable versions of the encoders.
 *
 * @author Damien Carbonne
 */
public final class Encoders {
    private Encoders() {
    }

    /**
     * Returns the node corresponding to {@code 'at least one (nodes)'}.
     * <p>
     * The result can be negated: it does not include any variable.
     *
     * @param nodes The nodes.
     * @return The node corresponding to {@code 'at least one (nodes)'}.
     * @throws IllegalArgumentException When {@code nodes} is {@code null} or empty.
     */
    public static Node atLeastOne(List<? extends Node> nodes) {
        return Encoder.atLeastOne(nodes);
    }

    /**
     * Returns a negatable node corresponding to {@code 'at most one (nodes)'}.
     * <p>
     * The result can be negated: it does not include any variable.
     * It may not be the best one from a memory or SAT solving point of view.
     *
     * @param nodes The nodes.
     * @return The node corresponding to {@code 'at most one (nodes)'}.
     */
    public static Node atMostOneNegatable(List<? extends Node> nodes) {
        return BinomialEncoder.atMostOne(nodes);
    }

    /**
     * Returns a non-negatable node corresponding to {@code 'at most one (nodes)'}.
     * <p>
     * The result can not be negated: it can contain variables.
     * It is the user responsibility to make sure the result will never be negated.
     *
     * @param nodes The nodes.
     * @param prefix The prefix used to name created variables.
     * @return The node corresponding to {@code 'at most one (nodes)'}.
     */
    public static Node atMostOneNonNegatable(List<? extends Node> nodes,
                                             String prefix) {
        return CommanderEncoder.INSTANCE.atMostOne(nodes, prefix);
    }

    /**
     * Returns a negatable node corresponding to {@code 'exactly one (nodes)'}.
     * <p>
     * The result can be negated: it does not include any variable.
     * It may not be the best one from a memory or SAT solving point of view.
     *
     * @param nodes The nodes.
     * @return The node corresponding to {@code 'exactly one (nodes)'}.
     */
    public static Node exactlyOneNegatable(List<? extends Node> nodes) {
        return BinomialEncoder.exactlyOne(nodes);
    }

    /**
     * Returns a non-negatable node corresponding to {@code 'exactly one (nodes)'}.
     * <p>
     * The result can not be negated: it can contain variables.
     * It is the user responsibility to make sure the result will never be negated.
     *
     * @param nodes The nodes.
     * @param prefix The prefix used to name created variables.
     * @return The node corresponding to {@code 'exactly one (nodes)'}.
     */
    public static Node exactlyOneNonNegatable(List<? extends Node> nodes,
                                              String prefix) {
        return CommanderEncoder.INSTANCE.exactlyOne(nodes, prefix);
    }
}