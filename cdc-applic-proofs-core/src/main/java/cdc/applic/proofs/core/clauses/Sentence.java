package cdc.applic.proofs.core.clauses;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import cdc.util.debug.Printable;

/**
 * Conjunction of {@link Clause Clauses} (disjunctions of {@link Literal Literals}).
 *
 * @author Damien Carbonne
 */
public class Sentence implements Printable {
    private final Set<Clause> clauses;

    private Integer cachedHashCode = null;

    public static final Sentence FALSE = new Sentence(Clause.FALSE);

    private Sentence(Set<Clause> clauses) {
        this.clauses = Collections.unmodifiableSet(clauses);
    }

    public Sentence(Collection<Clause> clauses) {
        this(filter(new LinkedHashSet<>(clauses)));
    }

    public Sentence(Clause... clauses) {
        this(Arrays.asList(clauses));
    }

    private static Set<Clause> filter(Set<Clause> set) {
        Set<Clause> toRemove = null;
        for (final Clause clause : set) {
            if (clause.isTautology()) {
                if (toRemove == null) {
                    toRemove = new HashSet<>();
                }
                toRemove.add(clause);
            }
        }
        if (toRemove != null) {
            set.removeAll(toRemove);
        }
        return set;
    }

    public Set<Clause> getClauses() {
        return clauses;
    }

    public Sentence merge(Sentence other) {
        final Set<Clause> tmp = new LinkedHashSet<>();
        tmp.addAll(this.clauses);
        tmp.addAll(other.clauses);
        return new Sentence(tmp);
    }

    public String getContent() {
        final StringBuilder builder = new StringBuilder();
        final boolean parentheses = clauses.size() > 1;
        boolean first = true;
        for (final Clause clause : clauses) {
            if (!first) {
                builder.append(" AND ");
            }
            builder.append(clause.getContent(parentheses));
            first = false;
        }
        return builder.toString();
    }

    @Override
    public int hashCode() {
        if (cachedHashCode == null) {
            cachedHashCode = clauses.hashCode();
        }
        return cachedHashCode.intValue();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Sentence)) {
            return false;
        }
        final Sentence other = (Sentence) object;
        if (hashCode() != other.hashCode()) {
            return false;
        }
        return clauses.equals(other.clauses);
    }

    @Override
    public String toString() {
        return getContent();
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        for (final Clause clause : clauses) {
            clause.print(out, level);
        }
    }
}