package cdc.applic.proofs.core;

import cdc.applic.expressions.ast.Node;
import cdc.applic.proofs.SatSolverException;
import cdc.applic.proofs.core.clauses.SatSystem;

public interface SatSystemSolver {
    /**
     * Returns {@code true} if the expression represented by a {@link Node} is satisfiable.
     *
     * @param system The SAT system to solve.
     * @return {@code true} if {@code node} is satisfiable.
     * @throws IllegalArgumentException When {@code node} is {@code null} or otherwise invalid.
     * @throws SatSolverException When the solver overshoots the timeout or any other problem.
     */
    public boolean isSatisfiable(SatSystem system);
}