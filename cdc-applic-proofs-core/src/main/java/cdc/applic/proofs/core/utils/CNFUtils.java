package cdc.applic.proofs.core.utils;

import java.util.Collections;
import java.util.List;

import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractOperatorNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.EquivalenceNode;
import cdc.applic.expressions.ast.ImplicationNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.expressions.ast.XorNode;
import cdc.applic.proofs.core.ast.AtomVarNode;

/**
 * Utility class used to build CBF formulas base on Tseitin approach.
 *
 * @author Damien Carbonne
 */
public final class CNFUtils {
    private CNFUtils() {
    }

    /**
     * Adds, to a list of formulas, the simplest formulas corresponding to &alpha; &harr; &beta;,
     * where &alpha; is a {@link AtomVarNode} and &beta; an {@link AbstractOperatorNode}
     * <p>
     * &alpha;&harr;&beta; &equiv; &alpha;&and;&beta; &or; &not;&alpha;&and;&not;&beta; &equiv; (&alpha;&or;&not;&beta;) &and;
     * (&not;&alpha;&or;&beta;)
     * <p>
     * The following equivalent formulas are added to {@code nodes}, depending on the nature of {@code beta}:
     * <ul>
     * <li><b>Binary and</b> &alpha; &harr; (&beta;<sub>1</sub>&and;&beta;<sub>2</sub>)
     * <ul>
     * <li>&alpha;&or;&not;&beta;<sub>1</sub>&or;&not;&beta;<sub>2</sub>
     * <li>&not;&alpha;&or;&beta;<sub>1</sub>
     * <li>&not;&alpha;&or;&beta;<sub>2</sub>
     * </ul>
     * <li><b>Nary and</b> &alpha; &harr; (&beta;<sub>1</sub>&and;&beta;<sub>2</sub>&and;&beta;<sub>3</sub>...)
     * <ul>
     * <li>&alpha;&or;&not;&beta;<sub>1</sub>&or;&not;&beta;<sub>2</sub>&or;&not;&beta;<sub>3</sub>...
     * <li>&not;&alpha;&or;&beta;<sub>1</sub>
     * <li>&not;&alpha;&or;&beta;<sub>2</sub>
     * <li>&not;&alpha;&or;&beta;<sub>3</sub>
     * <li>...
     * </ul>
     * <li><b>Binary or</b> &alpha; &harr; (&beta;<sub>1</sub>&or;&beta;<sub>2</sub>)
     * <ul>
     * <li>&not;&alpha;&or;&beta;<sub>1</sub>&or;&beta;<sub>2</sub>
     * <li>&alpha;&or;&not;&beta;<sub>1</sub>
     * <li>&alpha;&or;&not;&beta;<sub>2</sub>
     * </ul>
     * <li><b>Nary or</b> &alpha; &harr; (&beta;<sub>1</sub>&or;&beta;<sub>2</sub>&or;&beta;<sub>3</sub>...)
     * <ul>
     * <li>&not;&alpha;&or;&beta;<sub>1</sub>&or;&beta;<sub>2</sub>&or;&beta;<sub>3</sub>...
     * <li>&alpha;&or;&not;&beta;<sub>1</sub>
     * <li>&alpha;&or;&not;&beta;<sub>2</sub>
     * <li>&alpha;&or;&not;&beta;<sub>3</sub>
     * <li>...
     * </ul>
     * <li><b>Equivalence</b> &alpha; &harr; (&beta;<sub>1</sub>&harr;&beta;<sub>2</sub>)
     * <ul>
     * <li>&not;&alpha;&or;&not;&beta;<sub>1</sub>&or;&beta;<sub>2</sub>
     * <li>&not;&alpha;&or;&beta;<sub>1</sub>&or;&not;&beta;<sub>2</sub>
     * <li>&alpha;&or;&beta;<sub>1</sub>&or;&beta;<sub>2</sub>
     * <li>&alpha;&or;&not;&beta;<sub>1</sub>&or;&not;&beta;<sub>2</sub>
     * </ul>
     * <li><b>Xor</b> &alpha; &harr; (&beta;<sub>1</sub>&nharr;&beta;<sub>2</sub>)
     * <ul>
     * <li>&not;&alpha;&or;&not;&beta;<sub>1</sub>&or;&not;&beta;<sub>2</sub>
     * <li>&not;&alpha;&or;&beta;<sub>1</sub>&or;&beta;<sub>2</sub>
     * <li>&alpha;&or;&beta;<sub>1</sub>&or;&not;&beta;<sub>2</sub>
     * <li>&alpha;&or;&not;&beta;<sub>1</sub>&or;&beta;<sub>2</sub>
     * </ul>
     * <li><b>Implication</b> &alpha; &harr; (&beta;<sub>1</sub>&rarr;&beta;<sub>2</sub>)
     * <ul>
     * <li>&not;&alpha;&or;&not;&beta;<sub>1</sub>&or;&beta;<sub>2</sub>
     * <li>&alpha;&or;&beta;<sub>1</sub>
     * <li>&alpha;&or;&not;&beta;<sub>2</sub>
     * </ul>
     * <li><b>Not</b> &alpha; &harr; (&not;&beta;<sub>1</sub>)
     * <ul>
     * <li>&not;&alpha;&or;&not;&beta;<sub>1</sub>
     * <li>&alpha;&or;&beta;<sub>1</sub>
     * </ul>
     * </ul>
     *
     * @param alpha The &alpha; {@link AtomVarNode}.
     * @param beta The &beta; {@link AbstractOperatorNode}.
     * @param nodes The list of nodes that must be filled with the formulas corresponding to {@code alpha} &harr; {@code beta}.
     */
    public static void addEquivalence(AtomVarNode alpha,
                                      AbstractOperatorNode beta,
                                      List<Node> nodes) {
        if (beta instanceof AbstractBinaryNode) {
            if (beta instanceof final AndNode n) {
                addEquivalenceWithAnd(alpha, n, nodes);
            } else if (beta instanceof final OrNode n) {
                addEquivalenceWithOr(alpha, n, nodes);
            } else if (beta instanceof final ImplicationNode n) {
                addEquivalenceWithImplication(alpha, n, nodes);
            } else if (beta instanceof final XorNode n) {
                addEquivalenceWithXor(alpha, n, nodes);
            } else {
                // node is necessarily an EquivalenceNode
                addEquivalenceWithEquivalence(alpha, (EquivalenceNode) beta, nodes);
            }
        } else if (beta instanceof final NotNode n) {
            addEquivalenceWithNot(alpha, n, nodes);
        } else if (beta instanceof final NaryAndNode n) {
            addEquivalenceWithNaryAnd(alpha, n, nodes);
        } else {
            // node is necessarily an NaryOrNode
            addEquivalenceWithNaryOr(alpha, (NaryOrNode) beta, nodes);
        }
    }

    public static Node equivalence(Node alpha,
                                   Node beta) {
        return and(or(alpha, not(beta)),
                   or(not(alpha), beta));
    }

    /**
     * Creates &not;&alpha;
     *
     * @param alpha The Alpha node.
     * @return not alpha.
     */
    private static Node not(Node alpha) {
        return NotNode.simplestNot(alpha);
    }

    /**
     * Creates &alpha; &or; &beta;
     *
     * @param alpha The alpha node.
     * @param beta The beta node.
     * @return alpha or beta
     */
    private static OrNode or(Node alpha,
                             Node beta) {
        return new OrNode(alpha, beta);
    }

    private static AndNode and(Node alpha,
                               Node beta) {
        return new AndNode(alpha, beta);
    }

    private static Node or(Node... children) {
        return NaryOrNode.createSimplestOr(children);
    }

    private static void addEquivalenceWithAnd(AtomVarNode alpha,
                                              AndNode beta,
                                              List<Node> nodes) {
        final Node notalpha = not(alpha);
        nodes.add(or(alpha, not(beta.getAlpha()), not(beta.getBeta())));
        nodes.add(or(notalpha, beta.getAlpha()));
        nodes.add(or(notalpha, beta.getBeta()));
    }

    private static void addEquivalenceWithNaryAnd(AtomVarNode alpha,
                                                  NaryAndNode beta,
                                                  List<Node> nodes) {

        final Node[] first = new Node[1 + beta.getChildrenCount()];
        first[0] = alpha;
        for (int index = 0; index < beta.getChildrenCount(); index++) {
            first[index + 1] = not(beta.getChildAt(index));
        }
        nodes.add(or(first));

        final Node notalpha = not(alpha);
        for (int index = 0; index < beta.getChildrenCount(); index++) {
            nodes.add(or(notalpha, beta.getChildAt(index)));
        }
    }

    private static void addEquivalenceWithOr(AtomVarNode alpha,
                                             OrNode beta,
                                             List<Node> nodes) {
        nodes.add(or(not(alpha), beta.getAlpha(), beta.getBeta()));
        nodes.add(or(alpha, not(beta.getAlpha())));
        nodes.add(or(alpha, not(beta.getBeta())));
    }

    private static void addEquivalenceWithNaryOr(AtomVarNode alpha,
                                                 NaryOrNode beta,
                                                 List<Node> nodes) {
        final Node[] first = new Node[1 + beta.getChildrenCount()];
        first[0] = not(alpha);
        for (int index = 0; index < beta.getChildrenCount(); index++) {
            first[index + 1] = beta.getChildAt(index);
        }
        nodes.add(or(first));
        for (int index = 0; index < beta.getChildrenCount(); index++) {
            nodes.add(or(alpha, not(beta.getChildAt(index))));
        }
    }

    private static void addEquivalenceWithImplication(AtomVarNode alpha,
                                                      ImplicationNode beta,
                                                      List<Node> nodes) {
        nodes.add(or(not(alpha), not(beta.getAlpha()), beta.getBeta()));
        nodes.add(or(alpha, beta.getAlpha()));
        nodes.add(or(alpha, not(beta.getBeta())));
    }

    private static void addEquivalenceWithEquivalence(AtomVarNode alpha,
                                                      EquivalenceNode beta,
                                                      List<Node> nodes) {
        final Node notalpha = not(alpha);
        final Node notbetaalpha = not(beta.getAlpha());
        final Node notbetabeta = not(beta.getBeta());
        nodes.add(or(notalpha, notbetaalpha, beta.getBeta()));
        nodes.add(or(notalpha, beta.getAlpha(), notbetabeta));
        nodes.add(or(alpha, beta.getAlpha(), beta.getBeta()));
        nodes.add(or(alpha, notbetaalpha, notbetabeta));
    }

    private static void addEquivalenceWithXor(AtomVarNode alpha,
                                              XorNode beta,
                                              List<Node> nodes) {
        final Node notalpha = not(alpha);
        final Node notbetaalpha = not(beta.getAlpha());
        final Node notbetabeta = not(beta.getBeta());
        nodes.add(or(notalpha, notbetaalpha, notbetabeta));
        nodes.add(or(notalpha, beta.getAlpha(), beta.getBeta()));
        nodes.add(or(alpha, beta.getAlpha(), notbetabeta));
        nodes.add(or(alpha, notbetaalpha, beta.getBeta()));
    }

    private static void addEquivalenceWithNot(AtomVarNode alpha,
                                              NotNode beta,
                                              List<Node> nodes) {
        nodes.add(or(not(alpha), beta));
        nodes.add(or(alpha, beta.getAlpha()));
    }

    /**
     * Adds, to a list of formulas, the simplest formulas corresponding to &top; &harr; &beta;,
     * where &beta; is an {@link AbstractOperatorNode}
     * <p>
     * &top; &harr; &beta; &equiv; &beta;
     * <p>
     * The following equivalent formulas are added to {@code nodes}, depending on the nature of {@code beta}:
     * <ul>
     * <li><b>Binary and</b> &top; &harr; (&beta;<sub>1</sub>&and;&beta;<sub>2</sub>)
     * <ul>
     * <li>&beta;<sub>1</sub>
     * <li>&beta;<sub>2</sub>
     * </ul>
     * <li><b>Nary and</b> &top; &harr; (&beta;<sub>1</sub>&and;&beta;<sub>2</sub>&and;&beta;<sub>3</sub>...)
     * <ul>
     * <li>&beta;<sub>1</sub>
     * <li>&beta;<sub>2</sub>
     * <li>&beta;<sub>3</sub>
     * <li>...
     * </ul>
     * <li><b>Binary or</b> &top; &harr; (&beta;<sub>1</sub>&or;&beta;<sub>2</sub>)
     * <ul>
     * <li>&beta; &equiv; &beta;<sub>1</sub>&or;&beta;<sub>2</sub>
     * </ul>
     * <li><b>Nary or</b> &top; &harr; (&beta;<sub>1</sub>&or;&beta;<sub>2</sub>&or;&beta;<sub>3</sub>...)
     * <ul>
     * <li>&beta; &equiv; &beta;<sub>1</sub>&or;&beta;<sub>2</sub>&or;&beta;<sub>3</sub>...
     * </ul>
     * <li><b>Equivalence</b> &top; &harr; (&beta;<sub>1</sub>&harr;&beta;<sub>2</sub>)
     * <ul>
     * <li>&not;&beta;<sub>1</sub>&or;&beta;<sub>2</sub>
     * <li>&beta;<sub>1</sub>&or;&not;&beta;<sub>2</sub>
     * </ul>
     * <li><b>Xor</b> &top; &harr; (&beta;<sub>1</sub>&nharr;&beta;<sub>2</sub>)
     * <ul>
     * <li>&not;&beta;<sub>1</sub>&or;&not;&beta;<sub>2</sub>
     * <li>&beta;<sub>1</sub>&or;&beta;<sub>2</sub>
     * </ul>
     * <li><b>Implication</b> &top; &harr; (&beta;<sub>1</sub>&rarr;&beta;<sub>2</sub>)
     * <ul>
     * <li>&not;&beta;<sub>1</sub>&or;&beta;<sub>2</sub>
     * </ul>
     * <li><b>Not</b> &top; &harr; (&not;&beta;<sub>1</sub>)
     * <ul>
     * <li>&beta; &equiv; &not;&beta;<sub>1</sub>
     * </ul>
     * </ul>
     *
     * @param beta The &beta; {@link AbstractOperatorNode}.
     * @param nodes The list of nodes that must be filled with the formulas corresponding to &top; &harr; {@code beta}.
     */
    public static void addTrueEquivalence(AbstractOperatorNode beta,
                                          List<Node> nodes) {
        if (beta instanceof AbstractBinaryNode) {
            if (beta instanceof final AndNode n) {
                addTrueEquivalenceWithAnd(n, nodes);
            } else if (beta instanceof OrNode) {
                nodes.add(beta);
            } else if (beta instanceof final ImplicationNode n) {
                addTrueEquivalenceWithImplication(n, nodes);
            } else if (beta instanceof final XorNode n) {
                addTrueEquivalenceWithXor(n, nodes);
            } else {
                // node is necessarily an EquivalenceNode
                addTrueEquivalenceWithEquivalence((EquivalenceNode) beta, nodes);
            }
        } else if (beta instanceof final NaryAndNode n) {
            addTrueEquivalenceWithNaryAnd(n, nodes);
        } else {
            // node is necessarily an NaryOrNode or NotNode
            nodes.add(beta);
        }
    }

    private static void addTrueEquivalenceWithAnd(AndNode beta,
                                                  List<Node> nodes) {
        nodes.add(beta.getAlpha());
        nodes.add(beta.getBeta());
    }

    private static void addTrueEquivalenceWithNaryAnd(NaryAndNode beta,
                                                      List<Node> nodes) {
        Collections.addAll(nodes, beta.getChildren());
    }

    private static void addTrueEquivalenceWithImplication(ImplicationNode beta,
                                                          List<Node> nodes) {
        nodes.add(or(not(beta.getAlpha()), beta.getBeta()));
    }

    private static void addTrueEquivalenceWithEquivalence(EquivalenceNode beta,
                                                          List<Node> nodes) {
        nodes.add(or(not(beta.getAlpha()), beta.getBeta()));
        nodes.add(or(beta.getAlpha(), not(beta.getBeta())));
    }

    private static void addTrueEquivalenceWithXor(XorNode beta,
                                                  List<Node> nodes) {
        nodes.add(or(not(beta.getAlpha()), not(beta.getBeta())));
        nodes.add(or(beta.getAlpha(), beta.getBeta()));
    }
}