package cdc.applic.proofs.core.encoding;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.expressions.ast.TrueNode;

/**
 * Binomial encoding of "at most one", "exactly one".
 * <p>
 * At most is implemented as: &and;<sub>i,j</sub> (&not;&lambda;<sub>i</sub>&or;&not;&lambda;<sub>j</sub>), with i &lt; j.<br>
 * This does not create any variable.<br>
 * This creates N*(N-1)/2 clauses.
 *
 * @author Damien Carbonne
 */
public final class BinomialEncoder implements Encoder {
    public static final BinomialEncoder INSTANCE = new BinomialEncoder();

    public static void addAtMostOne(Node node1,
                                    Node node2,
                                    Node node3,
                                    Node node4,
                                    List<Node> clauses) {
        final Node n1 = NotNode.simplestNot(node1);
        final Node n2 = NotNode.simplestNot(node2);
        final Node n3 = NotNode.simplestNot(node3);
        final Node n4 = NotNode.simplestNot(node4);
        clauses.add(new OrNode(n1, n2));
        clauses.add(new OrNode(n1, n3));
        clauses.add(new OrNode(n1, n4));
        clauses.add(new OrNode(n2, n3));
        clauses.add(new OrNode(n2, n4));
        clauses.add(new OrNode(n3, n4));
    }

    public static void addAtMostOne(List<? extends Node> nodes,
                                    List<Node> clauses) {
        final int size = nodes.size();
        if (size >= 2) {
            // Compute the negation of each node
            final Node[] notNodes = new Node[size];
            for (int index = 0; index < size; index++) {
                notNodes[index] = NotNode.simplestNot(nodes.get(index));
            }

            // All pairs (not Ai or not Aj)
            for (int i = 0; i < size - 1; i++) {
                for (int j = i + 1; j < size; j++) {
                    clauses.add(new OrNode(notNodes[i], notNodes[j]));
                }
            }
        }
    }

    public static Node atMostOne(List<? extends Node> nodes) {
        final List<Node> clauses = new ArrayList<>();
        addAtMostOne(nodes, clauses);
        if (clauses.isEmpty()) {
            return TrueNode.INSTANCE;
        } else {
            return NaryAndNode.createSimplestAnd(clauses);
        }
    }

    public static Node exactlyOne(List<? extends Node> nodes) {
        final List<Node> clauses = new ArrayList<>();
        addExactlyOne(nodes, clauses);
        return NaryAndNode.createSimplestAnd(clauses);
    }

    public static void addExactlyOne(List<? extends Node> nodes,
                                     List<Node> clauses) {
        Encoder.addAtLeastOne(nodes, clauses);
        addAtMostOne(nodes, clauses);
    }

    @Override
    public void addAtMostOne(List<? extends Node> nodes,
                             List<Node> clauses,
                             String prefix) {
        addAtMostOne(nodes, clauses);
    }
}