package cdc.applic.proofs.core.ast;

import java.io.PrintStream;
import java.util.Objects;

import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.content.RealRange;
import cdc.applic.expressions.content.RealSItem;
import cdc.applic.expressions.content.RealValue;
import cdc.util.debug.Printables;
import cdc.util.lang.Checks;

/**
 * Real range atom leaf node representing:
 * <ul>
 * <li><b>Range:</b> &pi;@[min,max].
 * </ul>
 * Used for encoding of real ranges.
 *
 * @author Damien Carbonne
 */
public final class AtomRealRangeNode extends AbstractAtomNode {
    public static final String KIND = "REAL_RANGE";

    private final RealValue min;
    private final RealValue max;

    public AtomRealRangeNode(Property property,
                             RealSItem item) {
        super(property);
        if (item instanceof final RealRange range) {
            Checks.assertFalse(range.isEmpty(), "Invalid empty range");
            this.min = range.getMin();
            this.max = range.getMax();
        } else {
            this.min = (RealValue) item;
            this.max = this.min;
        }
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public final void buildInfix(Formatting formatting,
                                 StringBuilder builder) {
        builder.append(getProperty().getName());
        builder.append("@");
        builder.append(getMin());
        builder.append("~");
        builder.append(getMax());
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        Printables.indent(out, level);
        out.println(getKind() + " " + getProperty().getName() + " " + getMin() + "~" + getMax());
    }

    public RealValue getMin() {
        return min;
    }

    public RealValue getMax() {
        return max;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof AtomRealRangeNode)) {
            return false;
        }
        final AtomRealRangeNode other = (AtomRealRangeNode) object;
        return min.equals(other.min)
                && max.equals(other.max)
                && getProperty() == other.getProperty();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProperty(),
                            getMin(),
                            getMax());
    }

    @Override
    public String toString() {
        return getKind() + "(" + getProperty().getName() + "," + getMin() + "," + getMax() + ")";
    }
}