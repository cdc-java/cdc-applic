package cdc.applic.proofs.core.clauses;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import cdc.applic.expressions.ast.AtomNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.util.debug.Printable;

/**
 * Disjunction (OR) of {@link Literal literals}.
 * <p>
 * There is one copy of each literal.<br>
 * The same atom can be present twice, once as a positive literal, once as a negative literal.
 * <p>
 * <b>Note</b>: This is an adaptation of AIMA Java implementation.
 *
 * @author Damien Carbonne
 */
public final class Clause implements Printable {
    /**
     * The set of literals.
     * This ensures uniqueness of literals.
     */
    private final Set<Literal> literals;

    private final int cachedHashCode;

    /**
     * Cache of negative atoms.
     */
    private final Set<AtomNode> negativeAtoms;

    /**
     * Cache of positive atoms.
     */
    private final Set<AtomNode> positiveAtoms;

    /**
     * If {@code true}, this OR clause is a tautology.<br>
     * It either contains TRUE, NOT FALSE, or simultaneously X and NOT X.
     */
    private final boolean isTautology;

    /**
     * The TRUE clause.
     */
    public static final Clause TRUE = new Clause(new Literal(TrueNode.INSTANCE, true));

    /**
     * The FALSE clause.
     */
    public static final Clause FALSE = new Clause();

    public Clause(Collection<Literal> literals) {
        final Set<Literal> tmpLiterals = new LinkedHashSet<>();
        final Set<AtomNode> tmpPositiveAtoms = new HashSet<>();
        final Set<AtomNode> tmpNegativeAtoms = new HashSet<>();

        boolean tautology = false;
        for (final Literal literal : literals) {
            if (literal.isAlwaysFalse()) {
                // Ignore it
            } else {
                final boolean added = tmpLiterals.add(literal);
                if (added) {
                    if (literal.isAlwaysTrue()) {
                        // TRUE or NOT FALSE
                        tautology = true;
                    }
                    if (literal.isPositive()) {
                        tmpPositiveAtoms.add(literal.getAtom());
                        if (!tautology && tmpNegativeAtoms.contains(literal.getAtom())) {
                            // Contains X and NOT X
                            tautology = true;
                        }
                    } else {
                        tmpNegativeAtoms.add(literal.getAtom());
                        if (!tautology && tmpPositiveAtoms.contains(literal.getAtom())) {
                            // Contains X and NOT X
                            tautology = true;
                        }
                    }
                }
            }
        }
        this.literals = tmpLiterals; // Collections.unmodifiableSet(tmpLiterals);
        this.negativeAtoms = tmpNegativeAtoms.isEmpty()
                ? Collections.emptySet()
                : Collections.unmodifiableSet(tmpNegativeAtoms);
        this.positiveAtoms = tmpPositiveAtoms.isEmpty()
                ? Collections.emptySet()
                : Collections.unmodifiableSet(tmpPositiveAtoms);
        this.isTautology = tautology;
        this.cachedHashCode = this.literals.hashCode();
    }

    public Clause(Literal... literals) {
        this(Arrays.asList(literals));
    }

    public Clause() {
        this(Collections.emptyList());
    }

    /**
     * Returns {@code true} when this clause is empty (It has no {@link Literal Literals}).
     * <p>
     * An empty OR clause is equivalent to {@link #FALSE}.
     *
     * @return {@code true} when this clause is empty.
     */
    public boolean isEmpty() {
        return literals.isEmpty();
    }

    public boolean isContradiction() {
        return isEmpty();
    }

    /**
     * Return {@code true} if this clause is a unit clause: it contains 1 {@link Literal}.
     *
     * @return {@code true} is this clause is a unit clause.
     */
    public boolean isUnitClause() {
        return literals.size() == 1;
    }

    /**
     * Returns {@code true} if this is a definite clause: it contains exactly one positive {@link Literal}.
     * It can also contain any number of negative {@link Literal Literals}.
     *
     * @return {@code true} if this is a definite clause.
     */
    public boolean isDefiniteClause() {
        return positiveAtoms.size() == 1;
    }

    /**
     * Returns {@code true} if this is an implication definite clause:<br>
     * It is definite (exactly one positive {@link Literal}) and contains at least one negative {@link Literal}.
     *
     * @return {@code true} if this is an implication definite clause.
     */
    public boolean isImplicationDefiniteClause() {
        return isDefiniteClause() && !negativeAtoms.isEmpty();
    }

    /**
     * Returns {@code true} if this is a Horn clause:<br>
     * It is non-empty and has at most one positive {@link Literal}.
     *
     * @return {@code true} if this is a Horn clause.
     */
    public boolean isHornClause() {
        return !isEmpty() && positiveAtoms.size() <= 1;
    }

    /**
     * Returns {@code true} if this is a goal clause:<br>
     * It has only (at least one) negative {@link Literal Literals}.
     *
     * @return {@code true} if this is a goal clause.
     */
    public boolean isGoalClause() {
        return !isEmpty() && positiveAtoms.isEmpty();
    }

    /**
     * Returns {@code true} is this clause is a tautology.
     * <p>
     * It contains at least one of:
     * <ul>
     * <li>... TRUE ...</li>
     * <li>... NOT FALSE ...</li>
     * <li>... X ... NOT X ...</li>
     * </ul>
     *
     * @return {@code true} is this clause is a tautology.
     */
    public boolean isTautology() {
        return isTautology;
    }

    /**
     * @return The number of {@link Literal Literals}.
     */
    public int getNumberOfLiterals() {
        return literals.size();
    }

    /**
     * @return The number of positive {@link Literal Literals} (and atoms).
     */
    public int getNumberOfPositiveLiterals() {
        return positiveAtoms.size();
    }

    /**
     * @return The number of negative {@link Literal Literals} (and atoms).
     */
    public int getNumberOfNegativeLiterals() {
        return negativeAtoms.size();
    }

    /**
     * @return The read-only set of {@link Literal Literals}.
     */
    public Set<Literal> getLiterals() {
        return literals;
    }

    public Literal getUnitLiteral() {
        if (isUnitClause()) {
            return literals.iterator().next();
        } else {
            throw new IllegalStateException("Not a unit clause");
        }
    }

    /**
     * @return The read-only set of positive atoms.
     */
    public Set<AtomNode> getPositiveAtoms() {
        return positiveAtoms;
    }

    /**
     * @return The read-only set of negative atoms.
     */
    public Set<AtomNode> getNegativeAtoms() {
        return negativeAtoms;
    }

    /**
     * Returns the negation of this clause.
     * <p>
     * If this clause is empty, returns {@link #TRUE}.
     *
     * @return The negation of this clause.
     */
    public Clause getOpposite() {
        if (isEmpty()) {
            return TRUE;
        } else {
            final List<Literal> tmp = new ArrayList<>();
            for (final Literal literal : getLiterals()) {
                tmp.add(literal.getOpposite());
            }
            return new Clause(tmp);
        }
    }

    public Clause assertTrueAtom(AtomNode atom) {
        if (positiveAtoms.contains(atom)) {
            return TRUE;
        } else if (getNegativeAtoms().contains(atom)) {
            final Set<Literal> lit = new HashSet<>(getLiterals());
            lit.remove(new Literal(atom, false));
            return new Clause(lit);
        } else {
            return this;
        }
    }

    public Clause assertFalseAtom(AtomNode atom) {
        if (negativeAtoms.contains(atom)) {
            return TRUE;
        } else if (getPositiveAtoms().contains(atom)) {
            final Set<Literal> lit = new HashSet<>(getLiterals());
            lit.remove(new Literal(atom, true));
            return new Clause(lit);
        } else {
            return this;
        }
    }

    public Clause assertLiteral(Literal literal) {
        if (literal.positive) {
            return assertTrueAtom(literal.atom);
        } else {
            return assertFalseAtom(literal.atom);
        }
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Clause)) {
            return false;
        }
        final Clause other = (Clause) object;
        if (hashCode() != other.hashCode()) {
            return false;
        }
        return literals.equals(other.literals);
    }

    @Override
    public int hashCode() {
        return cachedHashCode;
    }

    @Override
    public String toString() {
        return getContent(true);
    }

    public String getContent(boolean parentheses) {
        final StringBuilder builder = new StringBuilder();
        if (isContradiction()) {
            builder.append("FALSE");
        } else if (isTautology()) {
            builder.append("TRUE");
        } else {
            final boolean many = parentheses && literals.size() > 1;
            if (many) {
                builder.append('(');
            }
            boolean first = true;
            for (final Literal literal : literals) {
                if (!first) {
                    builder.append(" OR ");
                }
                builder.append(literal);
                first = false;
            }
            if (many) {
                builder.append(')');
            }
        }
        return builder.toString();
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        out.append('[');
        boolean first = true;
        for (final Literal literal : literals) {
            if (first) {
                first = false;
            } else {
                out.print(", ");
            }
            out.print(literal);
        }
        out.append(']');
        out.println();
    }
}