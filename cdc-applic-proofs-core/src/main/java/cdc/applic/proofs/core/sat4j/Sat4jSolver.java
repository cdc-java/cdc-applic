package cdc.applic.proofs.core.sat4j;

import java.util.Arrays;

import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;

import cdc.applic.proofs.SatSolverException;
import cdc.applic.proofs.core.sat.SatSolver;

public class Sat4jSolver implements SatSolver {
    private final ISolver instance = SolverFactory.newLight();

    public Sat4jSolver() {
        instance.setLogPrefix("SAT4J");
    }

    private static SatSolverException newSatSolverException(String message,
                                                            Throwable e,
                                                            int[][] sentence) {
        final StringBuilder builder = new StringBuilder();

        builder.append(message);
        builder.append("\n");
        builder.append("   sentence:\n");
        for (int index = 0; index < sentence.length; index++) {
            builder.append("   " + Arrays.toString(sentence[index]) + "\n");
        }
        return new SatSolverException(builder.toString(), e);
    }

    @Override
    public boolean isSatisfiable(int[][] sentence) {
        // Convert data to SAT4J format
        instance.reset();
        instance.newVar(sentence.length);

        for (final int[] clause : sentence) {
            if (clause.length == 0) {
                // Empty clause : contradiction
                instance.reset();
                return false;
            }
            final VecInt vec = new VecInt(clause);
            try {
                instance.addClause(vec);
            } catch (final ContradictionException e) {
                // A trivial contradiction was found.
                instance.reset();
                return false;
            }
        }

        // Run SAT4J
        try {
            return instance.isSatisfiable();
        } catch (final TimeoutException e) {
            throw newSatSolverException("Timeout", e, sentence);
        } catch (final AssertionError e) {
            throw newSatSolverException("Unexpected assertion error", e, sentence);
        } catch (final Throwable e) {
            throw newSatSolverException("Unexpected exception", e, sentence);
        } finally {
            instance.reset();
        }
    }
}