package cdc.applic.proofs.core.clauses;

import java.util.Comparator;

import cdc.applic.expressions.ast.AtomNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.TrueNode;
import cdc.util.lang.Checks;

/**
 * A Literal is either an {@link AtomNode atom} or a negated {@link AtomNode atom}.
 * <p>
 * <b>Note</b>: This is an adaptation of AIMA Java implementation.
 *
 * @author Damien Carbonne
 */
public final class Literal {
    public static final Comparator<Literal> COMPARATOR =
            Comparator.comparing(Literal::getAtom, Node.COMPARATOR)
                      .thenComparing(Literal::isPositive);

    final AtomNode atom;
    final boolean positive;

    /**
     * Build a literal.
     *
     * @param atom The {@link AtomNode atom}.
     * @param positive If {@code false}, the atom is negated.
     */
    public Literal(AtomNode atom,
                   boolean positive) {
        Checks.isNotNull(atom, "atom");
        this.atom = atom;
        this.positive = positive;
    }

    /**
     * @return The associated {@link AtomNode atom}.
     */
    public AtomNode getAtom() {
        return atom;
    }

    /**
     * @return The opposite {@link Literal}.
     */
    public Literal getOpposite() {
        return new Literal(atom, !positive);
    }

    /**
     * @return {@code true} if this is a positive {@link Literal}, {@code false} otherwise.
     */
    public boolean isPositive() {
        return positive;
    }

    /**
     * @return {@code true} if this is a negative {@link Literal}, {@code false} otherwise.
     */
    public boolean isNegative() {
        return !positive;
    }

    /**
     * @return {@code true} if this {@link Literal} is always {@code true} (TRUE or NOT FALSE).
     */
    public boolean isAlwaysTrue() {
        return positive
                ? atom instanceof TrueNode
                : atom instanceof FalseNode;
    }

    /**
     * @return {@code true} if this {@link Literal} is always {@code false} (FALSE or NOT TRUE).
     */
    public boolean isAlwaysFalse() {
        return positive
                ? atom instanceof FalseNode
                : atom instanceof TrueNode;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof Literal)) {
            return false;
        }
        final Literal other = (Literal) object;
        return positive == other.positive && atom.equals(other.atom);
    }

    @Override
    public int hashCode() {
        return atom.hashCode() + (positive ? 0 : 1);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        if (isNegative()) {
            builder.append("NOT ");
        }
        builder.append(atom);
        return builder.toString();
    }
}