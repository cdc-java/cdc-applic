package cdc.applic.proofs.core.ast;

import java.io.PrintStream;
import java.util.Objects;

import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.content.Value;
import cdc.util.debug.Printables;
import cdc.util.lang.Checks;

/**
 * Leaf atom Node representing:
 * <ul>
 * <li><b>Value:</b> &pi;@&upsilon;.
 * </ul>
 * It is used to represent each enumeration value of an enumerated property.<br>
 * It is also used to represent each used value of a pattern property.
 *
 * @author Damien Carbonne
 */
public final class AtomValueNode extends AbstractAtomNode {
    public static final String KIND = "VALUE";

    private final Value value;
    private final int cachedHashCode;

    public AtomValueNode(Property property,
                         Value value) {
        super(property);
        Checks.isNotNull(value, "value");
        this.value = value;
        this.cachedHashCode = Objects.hash(value, property);
    }

    @Override
    public String getKind() {
        return KIND;
    }

    public Value getValue() {
        return value;
    }

    @Override
    public final void buildInfix(Formatting formatting,
                                 StringBuilder builder) {
        builder.append(getProperty().getName());
        builder.append("@");
        builder.append(getValue());
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        Printables.indent(out, level);
        out.println(getKind() + " " + getProperty().getName() + " " + getValue());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof AtomValueNode)) {
            return false;
        }
        final AtomValueNode other = (AtomValueNode) object;
        return value.equals(other.value)
                && getProperty() == other.getProperty();
    }

    @Override
    public int hashCode() {
        return cachedHashCode;
    }

    @Override
    public String toString() {
        return getKind() + "(" + getProperty().getName() + "," + getValue() + ")";
    }
}