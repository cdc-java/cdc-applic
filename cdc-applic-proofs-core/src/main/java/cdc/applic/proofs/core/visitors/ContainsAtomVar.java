package cdc.applic.proofs.core.visitors;

import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.AbstractAnalyzer;
import cdc.applic.proofs.core.ast.AtomVarNode;
import cdc.util.lang.Checks;

/**
 * A Visitor that tells whether a Node contains an {@link AtomVarNode}.
 *
 * @author Damien Carbonne
 */
public final class ContainsAtomVar extends AbstractAnalyzer {
    public boolean found = false;

    private ContainsAtomVar() {
    }

    public static boolean execute(Node node) {
        Checks.isNotNull(node, "node");

        final ContainsAtomVar visitor = new ContainsAtomVar();
        node.accept(visitor);
        return visitor.found;
    }

    @Override
    public Void visitLeaf(AbstractLeafNode node) {
        if (node instanceof AtomVarNode) {
            found = true;
        }
        return null;
    }

    @Override
    public Void visitUnary(AbstractUnaryNode node) {
        if (!found) {
            node.getAlpha().accept(this);
        }
        return null;
    }

    @Override
    public Void visitBinary(AbstractBinaryNode node) {
        if (!found) {
            node.getAlpha().accept(this);
            if (found) {
                return null;
            }
            node.getBeta().accept(this);
        }
        return null;
    }

    @Override
    public Void visitNary(AbstractNaryNode node) {
        if (!found) {
            for (final Node child : node.getChildren()) {
                child.accept(this);
                if (found) {
                    return null;
                }
            }
        }
        return null;
    }
}