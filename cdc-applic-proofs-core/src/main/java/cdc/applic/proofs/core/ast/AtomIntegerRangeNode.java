package cdc.applic.proofs.core.ast;

import java.io.PrintStream;
import java.util.Objects;

import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerSItem;
import cdc.applic.expressions.content.IntegerValue;
import cdc.util.debug.Printables;
import cdc.util.lang.Checks;

/**
 * Integer range atom leaf node representing:
 * <ul>
 * <li><b>Range:</b> &pi;@[min,max].
 * </ul>
 * Used for encoding of integer ranges.
 *
 * @author Damien Carbonne
 */
public final class AtomIntegerRangeNode extends AbstractAtomNode {
    public static final String KIND = "INTEGER_RANGE";

    private final IntegerValue min;
    private final IntegerValue max;

    public AtomIntegerRangeNode(Property property,
                                IntegerSItem item) {
        super(property);
        if (item instanceof final IntegerRange range) {
            Checks.assertFalse(range.isEmpty(), "Invalid empty range");
            this.min = range.getMin();
            this.max = range.getMax();
        } else {
            this.min = (IntegerValue) item;
            this.max = this.min;
        }
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public final void buildInfix(Formatting formatting,
                                 StringBuilder builder) {
        builder.append(getProperty().getName());
        builder.append("@");
        builder.append(getMin());
        builder.append("~");
        builder.append(getMax());
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        Printables.indent(out, level);
        out.println(getKind() + " " + getProperty().getName() + " " + getMin() + "~" + getMax());
    }

    public IntegerValue getMin() {
        return min;
    }

    public IntegerValue getMax() {
        return max;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof AtomIntegerRangeNode)) {
            return false;
        }
        final AtomIntegerRangeNode other = (AtomIntegerRangeNode) object;
        return min.equals(other.min)
                && max.equals(other.max)
                && getProperty() == other.getProperty();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProperty(),
                            getMin(),
                            getMax());
    }

    @Override
    public String toString() {
        return getKind() + "(" + getProperty().getName() + "," + getMin() + "," + getMax() + ")";
    }
}