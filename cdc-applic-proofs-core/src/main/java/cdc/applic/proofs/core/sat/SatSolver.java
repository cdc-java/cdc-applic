package cdc.applic.proofs.core.sat;

/**
 * Low level interface of SatSolvers, for tests purpose.
 *
 * @author Damien Carbonne
 */
public interface SatSolver {
    public boolean isSatisfiable(int[][] sentence);
}