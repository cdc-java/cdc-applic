package cdc.applic.proofs.core.visitors;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AbstractOperatorNode;
import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.EquivalenceNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.literals.Name;
import cdc.applic.proofs.core.ast.AtomVarNode;
import cdc.applic.proofs.core.utils.CNFUtils;

/**
 * Apply Tseitin transformation to an AST (Node).
 * <p>
 * A {@link AtomVarNode} is associated to each non leaf node and used where possible.
 * The {@link AtomVarNode} is built from a fixed prefix and an incremented number.<br>
 * For this formula: &alpha; &and; &beta; &or; &not; &gamma;, and with prefix
 * '#' we obtain these conjunctions:<br>
 * <ul>
 * <li>variant = LEAVE_OPERATORS_UNCHANGED
 * <ul>
 * <li>#1 &harr; &alpha; &and; &beta;</li>
 * <li>#2 &harr; &not; &gamma;</li>
 * <li>#3 &harr; #1 &or; #2</li>
 * <li>#3</li>
 * </ul>
 * </li>
 * <li>variant = PREPARE_CNF
 * <ul>
 * <li>#1&or;&not;&alpha;&or;&not;&beta;</li>
 * <li>&not;#1&or;&alpha;</li>
 * <li>&not;#1&or;&beta;</li>
 * <li>&not;#2&or;&gamma;</li>
 * <li>#2&or;&not;&gamma;</li>
 * <li>#2&or;#1</li>
 * </ul>
 * Note that in the CNF case, last variable is omitted (direct unit propagation).
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class ConvertToTseitin extends AbstractConverter {
    /** Root node to which transformation is applied. */
    private final Node root;
    /** Variable name prefix. */
    private final String prefix;
    /** Variable name suffix. */
    private final String suffix;
    private final Variant variant;
    public final List<Node> clauses = new ArrayList<>();

    /** Number to use for next variable. */
    public int next = 0;

    private ConvertToTseitin(Node root,
                             String prefix,
                             String suffix,
                             Variant variant) {
        this.root = root;
        this.prefix = prefix == null ? "" : prefix;
        this.suffix = suffix == null ? "" : suffix;
        this.variant = variant;
    }

    public enum Variant {
        /**
         * Standard Tseitin transformation.
         * Operators are left unchanged and each operator has an equivalence with a variable.
         * If the operator is not a basic one (or, and, not), then this transformation result
         * may necessitate preparation and complementary work to be CNF compliant.
         */
        LEAVE_OPERATORS_UNCHANGED,

        /**
         * A Tseitin transformation whose result is CNF compliant.
         * Equivalence are constructed between variables and sets of clauses that are CNF compliant.
         */
        PREPARE_CNF
    }

    public static Node execute(Node node,
                               Variant variant,
                               String prefix,
                               String suffix) {
        final ConvertToTseitin converter = new ConvertToTseitin(node, prefix, suffix, variant);
        converter.next = 1;
        node.accept(converter);
        converter.terminate();
        return NaryAndNode.createSimplestAnd(converter.clauses);
    }

    private Name toName(int number) {
        final StringBuilder builder = new StringBuilder();
        builder.append(prefix);
        builder.append(number);
        builder.append(suffix);
        return Name.of(builder.toString(), false);
    }

    /**
     * Creates an equivalence with a target node.
     * <p>
     * A VarNode is created, with a new name.
     * The head conjunction is updated with the newly created equivalence.
     *
     * @param target The target node.
     * @return The created var node equivalent to {@code target}.
     */
    private AtomVarNode addEquivalence(AbstractOperatorNode target) {
        // Create a new var node
        /** Last created variable. */
        final AtomVarNode variable = new AtomVarNode(toName(next));
        next++;

        if (variant == Variant.LEAVE_OPERATORS_UNCHANGED) {
            // Create an equivalence between var and target
            final EquivalenceNode equivalence = new EquivalenceNode(variable, target);

            clauses.add(equivalence);
        } else {
            // Create an equivalence between var and target
            CNFUtils.addEquivalence(variable, target, clauses);
        }
        return variable;
    }

    private void addTrueEquivalence(AbstractOperatorNode target) {
        CNFUtils.addTrueEquivalence(target, clauses);
    }

    /**
     * After all equivalences have been created, if no equivalence has been created, the starting node is used as head.
     */
    private void terminate() {
        if (clauses.isEmpty()) {
            clauses.add(root);
        }
    }

    @Override
    public Node visitUnary(AbstractUnaryNode node) {
        final AbstractUnaryNode target = node.create(node.getAlpha().accept(this));
        if (node == root) {
            addTrueEquivalence(target);
            return null;
        } else {
            return addEquivalence(target);
        }
    }

    @Override
    public Node visitBinary(AbstractBinaryNode node) {
        final AbstractBinaryNode target = node.create(node.getAlpha().accept(this),
                                                      node.getBeta().accept(this));
        if (node == root) {
            addTrueEquivalence(target);
            return null;
        } else {
            return addEquivalence(target);
        }
    }

    @Override
    public Node visitNary(AbstractNaryNode node) {
        final Node[] subs = node.getChildren().clone();
        for (int index = 0; index < subs.length; index++) {
            subs[index] = subs[index].accept(this);
        }
        final AbstractNaryNode target = node.create(subs);

        if (node == root) {
            addTrueEquivalence(target);
            return null;
        } else {
            return addEquivalence(target);
        }
    }
}