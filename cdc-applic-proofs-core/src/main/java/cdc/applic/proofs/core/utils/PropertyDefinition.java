package cdc.applic.proofs.core.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.EnumeratedValue;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.RealType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ast.AtomNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerSItem;
import cdc.applic.expressions.content.RealSItem;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.proofs.core.ast.AtomIntegerRangeNode;
import cdc.applic.proofs.core.ast.AtomRealRangeNode;
import cdc.applic.proofs.core.ast.AtomReserveNode;
import cdc.applic.proofs.core.ast.AtomValueNode;
import cdc.applic.proofs.core.encoding.Encoders;
import cdc.util.lang.Checks;

/**
 * Utility dedicated to creation of the definition node of a property.
 * <p>
 * Such definition is necessary and included in the SAT system.
 *
 * @author Damien Carbonne
 */
public final class PropertyDefinition {
    private static final String PROPERTY = "property";

    private PropertyDefinition() {
    }

    private static String wrapName(Property property) {
        return "#" + property.getName() + "#ONE#";
    }

    /**
     * Builds the AST corresponding to an enumerated property.
     * <p>
     * It declares all possible combinations of associated atoms.
     *
     * @param property The property.
     * @param addReserve If {@code true}, a reserve is added.
     * @return The AST corresponding to property.
     */
    private static Node buildEnumeratedPropertyNodeCNF(Property property,
                                                       boolean addReserve) {
        Checks.isNotNull(property, PROPERTY);
        final EnumeratedType type = (EnumeratedType) property.getType();
        final List<AtomNode> atoms = new ArrayList<>();
        for (final EnumeratedValue v : type.getValues()) {
            final StringValue value = StringValue.of(v.getLiteral().getNonEscapedLiteral(), false);
            final AtomValueNode atom = new AtomValueNode(property, value);
            atoms.add(atom);
        }

        if (addReserve) {
            final AtomReserveNode reserve = new AtomReserveNode(property);
            atoms.add(reserve);
        }

        if (atoms.isEmpty()) {
            return FalseNode.INSTANCE;
        } else {
            return Encoders.exactlyOneNonNegatable(atoms, wrapName(property));
        }
    }

    /**
     * Build a definition of a pattern property, using collected items.
     *
     * @param property The property.
     * @param items The collected items.
     * @param addReserve If {@code true}, a reserve item is added.
     * @return The definition of {@code property} based on {@code items} and {@code addReserve}.
     */
    public static Node buildPatternPropertyNodeCNF(Property property,
                                                   Collection<SItem> items,
                                                   boolean addReserve) {
        Checks.isNotNull(property, PROPERTY);
        Checks.assertTrue(property.getType() instanceof PatternType, "Property is not a pattern");

        final List<AtomNode> atoms = new ArrayList<>();
        for (final SItem item : items) {
            if (item instanceof final StringValue x) {
                final AtomValueNode atom = new AtomValueNode(property, x);
                atoms.add(atom);
            } else {
                throw new IllegalArgumentException(item.getClass().getSimpleName() + " not supported for patterns");
            }
        }
        if (addReserve) {
            final AtomReserveNode reserve = new AtomReserveNode(property);
            atoms.add(reserve);
        }
        if (atoms.isEmpty()) {
            return FalseNode.INSTANCE;
        } else {
            return Encoders.exactlyOneNonNegatable(atoms, wrapName(property));
        }
    }

    /**
     * Builds the AST corresponding to a boolean property.
     * <p>
     * It declares all possible combinations of associated atoms.
     *
     * @param property The property.
     * @return The AST corresponding to property.
     */
    private static Node buildBooleanPropertyNodeCNF(Property property) {
        Checks.isNotNull(property, PROPERTY);
        final List<AtomNode> atoms = new ArrayList<>();
        for (final BooleanValue v : BooleanValue.FALSE_TRUE) {
            final StringValue value = StringValue.of(v.getProtectedLiteral(), false);
            final AtomValueNode atom = new AtomValueNode(property, value);
            atoms.add(atom);
        }
        return Encoders.exactlyOneNonNegatable(atoms, wrapName(property));
    }

    private static Node buildIntegerPropertyNodeCNF(Property property,
                                                    boolean addReserve) {
        Checks.isNotNull(property, PROPERTY);
        final IntegerType type = (IntegerType) property.getType();
        final List<AtomNode> atoms = new ArrayList<>();

        for (final IntegerSItem item : type.getDomain().getItems()) {
            final AtomIntegerRangeNode atom = new AtomIntegerRangeNode(property, item);
            atoms.add(atom);
        }

        if (addReserve) {
            final AtomReserveNode reserve = new AtomReserveNode(property);
            atoms.add(reserve);
        }

        if (atoms.isEmpty()) {
            return FalseNode.INSTANCE;
        } else {
            return Encoders.exactlyOneNonNegatable(atoms, wrapName(property));
        }
    }

    private static Node buildRealPropertyNodeCNF(Property property,
                                                 boolean addReserve) {
        Checks.isNotNull(property, PROPERTY);
        final RealType type = (RealType) property.getType();
        final List<AtomNode> atoms = new ArrayList<>();

        for (final RealSItem item : type.getDomain().getItems()) {
            final AtomRealRangeNode atom = new AtomRealRangeNode(property, item);
            atoms.add(atom);
        }

        if (addReserve) {
            final AtomReserveNode reserve = new AtomReserveNode(property);
            atoms.add(reserve);
        }

        if (atoms.isEmpty()) {
            return FalseNode.INSTANCE;
        } else {
            return Encoders.exactlyOneNonNegatable(atoms, wrapName(property));
        }
    }

    /**
     * Builds the AST corresponding to a property.
     * <p>
     * <b>WARNING:</b> Something is produced for all types, but is useless
     * for {@link BooleanType} and {@link PatternType}.
     *
     * @param property The property.
     * @param reserveStrategy The reserve strategy to use to create the AST.
     * @return The AST corresponding to property.
     */
    public static Node buildPropertyNodeCNF(Property property,
                                            ReserveStrategy reserveStrategy) {
        Checks.isNotNull(property, PROPERTY);

        // Type of the property
        final Type type = property.getType();
        // Should we add reserve ?
        final boolean addReserve = reserveStrategy.hasReserve(type);

        if (type instanceof EnumeratedType) {
            return buildEnumeratedPropertyNodeCNF(property, addReserve);
        } else if (type instanceof BooleanType) {
            // No reserve for booleans
            return buildBooleanPropertyNodeCNF(property);
        } else if (type instanceof PatternType) {
            return TrueNode.INSTANCE;
        } else if (type instanceof IntegerType) {
            return buildIntegerPropertyNodeCNF(property, addReserve);
        } else {
            return buildRealPropertyNodeCNF(property, addReserve);
        }
    }
}