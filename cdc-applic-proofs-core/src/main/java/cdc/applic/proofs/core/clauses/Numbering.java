package cdc.applic.proofs.core.clauses;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.applic.expressions.ast.AtomNode;
import cdc.util.debug.Printable;
import cdc.util.debug.Printables;

/**
 * Utility used to associate numbers to atoms and literals.
 *
 * @author Damien Carbonne
 */
public final class Numbering implements Printable {
    private final Map<AtomNode, Integer> numbers = new HashMap<>();

    /**
     * Resets numbering.
     * <p>
     * The first atom to be numbered after this call will be associated to 1.
     */
    public void clear() {
        numbers.clear();
    }

    /**
     * @return A set of Atoms associated to numbers.
     */
    public Set<AtomNode> getAtoms() {
        return numbers.keySet();
    }

    /**
     * @return The number of numbered atoms.
     */
    public int getSize() {
        return numbers.size();
    }

    public boolean contains(AtomNode atom) {
        return numbers.containsKey(atom);
    }

    /**
     * Allocates a number for an Atom.
     * <p>
     * If a number was already associated to {@code atom}, returns that number.
     * Otherwise, associate a new number to {@code atom} and returns it.
     *
     * @param atom The atom.
     * @return The positive number associated to an atom.
     */
    public int computeIfAbsent(AtomNode atom) {
        final Integer number = numbers.computeIfAbsent(atom, k -> numbers.size() + 1);
        return number.intValue();
    }

    public int getNumber(AtomNode atom) {
        final Integer number = numbers.get(atom);
        if (number != null) {
            return number.intValue();
        } else {
            throw new IllegalArgumentException("Unkonwn atom: " + atom);
        }
    }

    /**
     * Allocates (if necessary) and returns the number associated to a literal.
     * <p>
     * If the literal is positive it is the number associated to its atom.
     * Otherwise, it is the opposite of that number.
     *
     * @param literal the literal.
     * @return the number associated to literal.
     */
    public int computeIfAbsent(Literal literal) {
        final int number = computeIfAbsent(literal.getAtom());
        return literal.isPositive() ? number : -number;
    }

    public int[] computeNumbers(Clause clause) {
        final int[] result = new int[clause.getLiterals().size()];
        int index = 0;
        for (final Literal literal : clause.getLiterals()) {
            final int number = computeIfAbsent(literal);
            result[index] = number;
            index++;
        }
        return result;
    }

    public int[][] computeNumbers(Sentence sentence) {
        final int[][] result = new int[sentence.getClauses().size()][];
        int index = 0;
        for (final Clause clause : sentence.getClauses()) {
            result[index] = computeNumbers(clause);
            index++;
        }
        return result;
    }

    public void print(Sentence sentence,
                      PrintStream out,
                      int level) {
        final List<String> heads = new ArrayList<>();
        final List<Clause> clauses = new ArrayList<>();

        int longest = 0;
        for (final Clause clause : sentence.getClauses()) {
            final String head;
            if (clause.isEmpty()) {
                head = " ?";
            } else {
                final List<Integer> nums = new ArrayList<>();
                for (final Literal literal : clause.getLiterals()) {
                    nums.add(computeIfAbsent(literal));
                }
                final StringBuilder builder = new StringBuilder();
                boolean first = true;
                for (final int i : nums) {
                    if (first) {
                        first = false;
                    } else {
                        builder.append(',');
                    }
                    if (i > 0) {
                        builder.append(' ');
                    }
                    builder.append(i);
                }
                head = builder.toString();
            }
            heads.add(head);
            longest = Math.max(longest, head.length());
            clauses.add(clause);
        }
        final String format = "%-" + longest + "s %s";
        for (int index = 0; index < heads.size(); index++) {
            Printables.indent(out, level);
            out.println(String.format(format,
                                      heads.get(index),
                                      clauses.get(index).getContent(false)));
        }
    }

    public static void print(int[][] numbers,
                             PrintStream out,
                             int level) {
        for (final int[] row : numbers) {
            Printables.indent(out, level);
            boolean first = true;
            for (final int i : row) {
                if (first) {
                    first = false;
                } else {
                    out.print(',');
                }
                if (i > 0) {
                    out.print(' ');
                }
                out.print(i);
            }
            out.println();
        }
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        final AtomNode[] atoms = new AtomNode[numbers.size()];
        for (final AtomNode atom : getAtoms()) {
            atoms[getNumber(atom) - 1] = atom;
        }
        for (int index = 0; index < atoms.length; index++) {
            indent(out, level);
            out.println((index + 1) + ": " + atoms[index]);
        }
    }
}