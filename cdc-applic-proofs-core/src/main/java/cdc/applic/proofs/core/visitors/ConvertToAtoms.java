package cdc.applic.proofs.core.visitors;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.StringType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotEqualNode;
import cdc.applic.expressions.ast.NotInNode;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.RealRange;
import cdc.applic.expressions.content.RealSet;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.proofs.core.ast.AtomIntegerRangeNode;
import cdc.applic.proofs.core.ast.AtomRealRangeNode;
import cdc.applic.proofs.core.ast.AtomValueNode;

/**
 * Visitor that replaces all standard leaf nodes by atoms.
 * <p>
 * TRUE, FALSE, and Alias REF nodes are left unchanged.
 * <ul>
 * <li>A boolean property is associated to one value atom.<br>
 * <li>An enumerated property is associated to N value atoms, one for each property value.<br>
 * <li>An integer property is associated to P range atoms, where P depends on the number referenced values. This is a range
 * encoding.
 * </ul>
 * <b>WARNING:</b> AST nodes must be valid with regard to passed dictionary.
 *
 * @author Damien Carbonne
 */
public final class ConvertToAtoms extends AbstractConverter {
    private final Dictionary dictionary;

    private ConvertToAtoms(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    private static Node buildBooleanValueNode(Property property,
                                              BooleanValue value) {
        if (value.getValue()) {
            return new RefNode(property.getName());
        } else {
            return new NotNode(new RefNode(property.getName()));
        }
    }

    private static Node buildBooleansSetNode(Property property,
                                             BooleanSet set) {
        if (set.contains(BooleanValue.FALSE)) {
            if (set.contains(BooleanValue.TRUE)) {
                return TrueNode.INSTANCE;
            } else {
                return new NotNode(new RefNode(property.getName()));
            }
        } else {
            if (set.contains(BooleanValue.TRUE)) {
                return new RefNode(property.getName());
            } else {
                return FalseNode.INSTANCE;
            }
        }
    }

    /**
     * Creates the AST corresponding to &pi; = &upsilon; when the property is enumerated.
     *
     * @param property The property (&pi;).
     * @param value The value (&upsilon;) of property.
     * @return The AST corresponding to 'property = value'.
     */
    private static Node buildStringValueNode(Property property,
                                             StringValue value) {
        return new AtomValueNode(property, value);
    }

    /**
     * Creates the AST corresponding to &pi; &isin; &omega; when the property is enumerated.
     *
     * @param property The property (&pi;).
     * @param set The values set (&omega;) of the property.
     * @return The AST corresponding to 'property in values'.
     */
    private static Node buildStringsSetNode(Property property,
                                            StringSet set) {
        final List<AtomValueNode> atoms = new ArrayList<>();

        for (final StringValue value : set.getItems()) {
            atoms.add(new AtomValueNode(property, value));
        }

        if (atoms.isEmpty()) {
            // Property belongs to an empty set: there is no solution
            return FalseNode.INSTANCE;
        } else {
            return NaryOrNode.createSimplestOr(atoms);
        }
    }

    /**
     * Creates the AST corresponding to &pi; = &upsilon; when the property is integer.
     * <p>
     * Uses range encoding.
     *
     * @param property The property (&pi;).
     * @param value The value (&upsilon;) of property.
     * @return The AST corresponding to 'property = value'.
     */
    private static Node buildIntegerValueRangeNode(Property property,
                                                   IntegerValue value) {
        return new AtomIntegerRangeNode(property, value);
    }

    /**
     * Creates the AST corresponding to &pi; &isin; &omega; when the property is integer.
     *
     * @param property The property (&pi;).
     * @param set The set (&omega;)
     * @return The AST corresponding to 'property in set'.
     */
    private static Node buildIntegerSetRangeNode(Property property,
                                                 IntegerSet set) {
        final List<Node> nodes = new ArrayList<>();
        for (final IntegerRange range : set.getRanges()) {
            // range is supposed non empty
            final AtomIntegerRangeNode atom = new AtomIntegerRangeNode(property, range);
            nodes.add(atom);
        }
        if (nodes.isEmpty()) {
            return FalseNode.INSTANCE;
        } else {
            return NaryOrNode.createSimplestOr(nodes);
        }
    }

    private static Node buildRealValueRangeNode(Property property,
                                                RealValue value) {
        return new AtomRealRangeNode(property, value);
    }

    private static Node buildRealSetRangeNode(Property property,
                                              RealSet set) {
        final List<Node> nodes = new ArrayList<>();
        for (final RealRange range : set.getRanges()) {
            // range is supposed non empty
            final AtomRealRangeNode atom = new AtomRealRangeNode(property, range);
            nodes.add(atom);
        }
        if (nodes.isEmpty()) {
            return FalseNode.INSTANCE;
        } else {
            return NaryOrNode.createSimplestOr(nodes);
        }
    }

    public static Node execute(Node node,
                               Dictionary dictionary) {
        final ConvertToAtoms visitor = new ConvertToAtoms(dictionary);
        return node.accept(visitor);
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (node instanceof final EqualNode n) {
            final Property property = dictionary.getRegistry().getProperty(n.getName());
            final Type type = property.getType();
            if (type.isCompliant(n.getValue())) {
                if (type instanceof StringType) {
                    return buildStringValueNode(property, (StringValue) n.getValue());
                } else if (type instanceof BooleanType) {
                    return buildBooleanValueNode(property, (BooleanValue) n.getValue());
                } else if (type instanceof IntegerType) {
                    return buildIntegerValueRangeNode(property, (IntegerValue) n.getValue());
                } else {
                    // It must be a real type
                    return buildRealValueRangeNode(property, (RealValue) n.getValue());
                }
            } else {
                // The node value is not compliant with type
                throw new IllegalArgumentException("Value of " + node + " is not compliant with type, definition: "
                        + type.getDefinition());
            }
        } else if (node instanceof final NotEqualNode n) {
            return new NotNode(new EqualNode(n.getName(), n.getValue()).accept(this));
        } else if (node instanceof final InNode n) {
            final Property property = dictionary.getRegistry().getProperty(n.getName());
            final Type type = property.getType();
            if (type.isCompliant(n.getSet())) {
                // Should we add reserve ?
                if (type instanceof StringType) {
                    return buildStringsSetNode(property, n.getSet().toStringSet());
                } else if (type instanceof BooleanType) {
                    return buildBooleansSetNode(property, n.getSet().toBooleanSet());
                } else if (type instanceof IntegerType) {
                    return buildIntegerSetRangeNode(property, n.getSet().toIntegerSet());
                } else {
                    // It must be a real type
                    return buildRealSetRangeNode(property, n.getSet().toRealSet());
                }
            } else {
                // The node values are not compliant with type
                throw new IllegalArgumentException(n.getSet() + " is not compliant with " + type);
            }
        } else if (node instanceof final NotInNode n) {
            return new NotNode(new InNode(n.getName(), n.getSet()).accept(this));
        } else {
            return node;
        }
    }
}