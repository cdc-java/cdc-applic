package cdc.applic.proofs.core.sat;

import cdc.applic.proofs.core.sat4j.Sat4jSolver;

public class SatSolverFactory {
    public SatSolverFactory() {
        super();
    }

    public SatSolver newInstance() {
        return new Sat4jSolver();
    }
}