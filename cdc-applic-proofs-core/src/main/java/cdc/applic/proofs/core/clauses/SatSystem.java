package cdc.applic.proofs.core.clauses;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cdc.applic.dictionaries.checks.SemanticChecker;
import cdc.applic.dictionaries.core.NodeFocus;
import cdc.applic.dictionaries.core.utils.CutPointBuffers;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.DExpressed;
import cdc.applic.dictionaries.items.DItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.RealType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.checks.CheckedData;
import cdc.applic.expressions.content.AbstractRange;
import cdc.applic.expressions.content.Domain;
import cdc.applic.expressions.content.IntegerDomain;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerSItem;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.RealDomain;
import cdc.applic.expressions.content.RealRange;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.Value;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.ast.AbstractAtomNode;
import cdc.applic.proofs.core.ast.AtomIntegerRangeNode;
import cdc.applic.proofs.core.ast.AtomRealRangeNode;
import cdc.applic.proofs.core.ast.AtomReserveNode;
import cdc.applic.proofs.core.encoding.Encoders;
import cdc.applic.proofs.core.utils.CNFUtils;
import cdc.applic.proofs.core.utils.PropertyDefinition;
import cdc.util.debug.Printable;
import cdc.util.lang.Checks;

/**
 * @author Damien Carbonne
 *
 */
public class SatSystem implements Printable {

    /** Main node focus. */
    private final NodeFocus mainNodeFocus;

    /** The main expression (possibly {@code null}). */
    private final Expression mainExpression;

    /** The features to use. */
    private final ProverFeatures proverFeatures;

    /** Conversion of {@code mainNode} as a {@link Sentence}. */
    private final Sentence mainSentence;

    /**
     * Set of all Sentences related to {@link #getMainNode()}.
     * <p>
     * This is filled by {@link #buildRelatedSentences()}
     * <ul>
     * <li>If the related item is a {@link DExpressed}, it is the conversion of the expression to a Sentence.<br>
     * This can be obtained from cache.
     * <li>If the related Item is a {@link Property}, it is the definition of the Property:
     * <ul>
     * <li>When the Property has an EnumeratedType, IntegerType or RealType, this can be obtained from cache.
     * <li>When the Property has a PatternType, this must be computed from values used in the system.
     * <li>When the Property has a BooleanType, nothing is done.
     * </ul>
     * </ul>
     */
    private final Set<Sentence> relatedSentences = new HashSet<>();

    /** CutPointBuffers for integer properties. */
    private final CutPointBuffers<IntegerDomain, IntegerValue, IntegerRange> cpbInteger =
            new CutPointBuffers<>(IntegerDomain.INSTANCE);

    /** CutPointBuffers for real properties. */
    private final CutPointBuffers<RealDomain, RealValue, RealRange> cpbReal = new CutPointBuffers<>(RealDomain.INSTANCE);

    /**
     * Set of sentences that map the ranges used in related sentences to ranges belonging to CutPointBuffers.
     * <p>
     * For each property that needs it, it will contain the cut point ranges formula and all equivalences.
     */
    private final Set<Sentence> mappingSentences = new HashSet<>();

    /**
     * The full sentence, that is the concatenation of all sentences:
     * {@link #mainSentence}, {@link #relatedSentences} and {@link #mappingSentences}.
     */
    private final Sentence fullSentence;

    /**
     * Used to convert sentences using atoms to sentences using numbers.
     */
    private final Numbering numbering = new Numbering();

    /**
     * The main sentence formulated as a set of clauses of numbered atoms.
     */
    private final int[][] numbers;

    private SatSystem(DictionaryHandle handle,
                      Expression mainExpression,
                      Node mainNode,
                      ProverFeatures proverFeatures) {
        Checks.isNotNull(handle, "handle");
        Checks.isNotNull(mainNode, "mainNode");
        Checks.isNotNull(proverFeatures, "proverFeatures");

        this.mainNodeFocus = new NodeFocus(handle, mainNode, proverFeatures.getAssertionStrategy());
        this.mainExpression = mainExpression;
        this.proverFeatures = proverFeatures;

        // Check main node semantic
        final SemanticChecker checker = new SemanticChecker(handle.getDictionary());
        checker.checkCompliance(new CheckedData(null, mainNode));

        this.mainSentence = SentenceBuilder.convert(handle.getDictionary(), mainNode, "#");
        buildRelatedSentences();

        this.fullSentence = buildFullSentence();
        this.numbers = buildNumbers();
    }

    public SatSystem(DictionaryHandle handle,
                     Expression mainExpression,
                     ProverFeatures proverFeatures) {
        this(handle,
             mainExpression,
             mainExpression.getRootNode(),
             proverFeatures);
    }

    public SatSystem(DictionaryHandle handle,
                     Node mainNode,
                     ProverFeatures proverFeatures) {
        this(handle,
             null,
             mainNode,
             proverFeatures);
    }

    private void addRelatedSentence(Sentence sentence) {
        Checks.isNotNull(sentence, "sentence");
        relatedSentences.add(sentence);
    }

    /**
     * Build all sentences from related DItems.
     */
    private void buildRelatedSentences() {
        final DictionarySentencesCache cache = DictionarySentencesCache.get(getDictionaryHandle());
        int indexPattern = 0;
        int indexInteger = 0;
        int indexReal = 0;
        for (final DItem item : mainNodeFocus.getRelatedDItems()) {
            if (item instanceof final DExpressed expressed) {
                // item is an Alias or Assertion, retrieve the cached sentence
                final Sentence sentence = cache.getPExpressedSentence(expressed);
                addRelatedSentence(sentence);
            } else {
                // item is a Property
                final Property property = (Property) item;
                final Type type = property.getType();
                if (type instanceof EnumeratedType) {
                    final Sentence sentence = cache.getDefinitionSentence(property, proverFeatures.getReserveStrategy());
                    addRelatedSentence(sentence);
                } else if (type instanceof PatternType) {
                    indexPattern++;
                    final Sentence sentence = buildPatternPropertySentence(property, indexPattern);
                    addRelatedSentence(sentence);
                } else if (type instanceof IntegerType) {
                    indexInteger++;
                    final Sentence sentence = cache.getDefinitionSentence(property, proverFeatures.getReserveStrategy());
                    addRelatedSentence(sentence);
                    buildRangeMapping(property, indexInteger, cpbInteger, "I");
                } else if (type instanceof RealType) {
                    indexReal++;
                    final Sentence sentence = cache.getDefinitionSentence(property, proverFeatures.getReserveStrategy());
                    addRelatedSentence(sentence);
                    buildRangeMapping(property, indexReal, cpbReal, "R");
                }
                // Else boolean: nothing to do
            }
        }
    }

    private static AbstractAtomNode createAtomRange(Property property,
                                                    SItem item) {
        if (item instanceof IntegerSItem) {
            if (item instanceof final IntegerRange range) {
                return new AtomIntegerRangeNode(property, range);
            } else {
                return new AtomIntegerRangeNode(property, (IntegerValue) item);
            }
        } else {
            if (item instanceof final RealRange range) {
                return new AtomRealRangeNode(property, range);
            } else {
                return new AtomRealRangeNode(property, (RealValue) item);
            }
        }
    }

    private <D extends Domain<V, R>,
             V extends Value & Comparable<? super V>,
             R extends AbstractRange<V, R>,
             A extends AbstractAtomNode>
            void buildRangeMapping(Property property,
                                   int index,
                                   CutPointBuffers<D, V, R> cpb,
                                   String discriminator) {
        final Set<SItem> sitems = mainNodeFocus.collectRelatedSItems(property);

        // Build CutPointBuffer
        for (final SItem item : sitems) {
            cpb.add(property, item);
        }

        // Convert cut ranges to atoms
        final List<Node> nodes = new ArrayList<>();
        for (final R range : cpb.getCutRanges(property)) {
            nodes.add(createAtomRange(property, range));
        }

        if (proverFeatures.getReserveStrategy().hasReserve(property.getType())) {
            nodes.add(new AtomReserveNode(property));
        }

        // Create the 'exactly one' expression and sentence for cut ranges
        final Node node = Encoders.exactlyOneNonNegatable(nodes, "#" + discriminator + "DEF#" + index + "#");
        mappingSentences.add(SentenceBuilder.convertPrepared(node));

        // Map all items on CPB ranges
        int sub = 0;
        for (final SItem item : sitems) {
            final AbstractAtomNode arange = createAtomRange(property, item);
            final List<R> ranges = cpb.getEquivalence(property, item);
            if (ranges.size() > 1) {
                sub++;
                buildRangeEquivalence(arange, ranges, "#" + discriminator + "MAP#" + index + "." + sub + "#");
            }
        }
    }

    /**
     * Creates the equivalence between a range and a list of cut ranges.
     *
     * @param <V> The Value type.
     * @param <R> The Range type.
     * @param arange The range.
     * @param ranges The cut ranges.
     * @param prefix The prefix to use to create the CNF sentence.
     */
    private <V extends Value & Comparable<? super V>,
             R extends AbstractRange<V, R>>
            void buildRangeEquivalence(AbstractAtomNode arange,
                                       List<R> ranges,
                                       String prefix) {
        final List<AbstractAtomNode> nodes = new ArrayList<>();
        for (final R range : ranges) {
            final AbstractAtomNode atom = createAtomRange(arange.getProperty(), range);
            nodes.add(atom);
        }
        final Node beta = NaryOrNode.createSimplestOr(nodes);
        final Node node = CNFUtils.equivalence(arange, beta);
        mappingSentences.add(SentenceBuilder.convert(getDictionaryHandle().getDictionary(), node, prefix));
    }

    /**
     * Creates the definition sentence of a pattern property.
     *
     * @param property The property.
     * @param index The index used to created Tseitin identifiers
     * @return The definition sentence of {@code property}.
     */
    private Sentence buildPatternPropertySentence(Property property,
                                                  int index) {
        final Set<SItem> sitems = mainNodeFocus.collectRelatedSItems(property);

        // // If this pattern property never references any value, it must be considered as having a reserve
        // // This is the case when one write P in {}
        // final boolean hasItems = !sitems.isEmpty();
        // final boolean addReserve = hasItems
        // ? proverFeatures.getReserveStrategy().hasReserve(property.getType())
        // : true;
        final Node node = PropertyDefinition.buildPatternPropertyNodeCNF(property, sitems, true);
        return SentenceBuilder.convert(getDictionaryHandle().getDictionary(), node, "#PAT#" + index + "#");
    }

    private static void add(List<Clause> clauses,
                            Set<Sentence> sentences) {
        for (final Sentence sentence : sentences) {
            clauses.addAll(sentence.getClauses());
        }
    }

    private Sentence buildFullSentence() {
        final List<Clause> clauses = new ArrayList<>();
        clauses.addAll(mainSentence.getClauses());
        add(clauses, relatedSentences);
        add(clauses, mappingSentences);
        return new Sentence(clauses);
    }

    private int[][] buildNumbers() {
        return numbering.computeNumbers(getFullSentence());
    }

    public DictionaryHandle getDictionaryHandle() {
        return mainNodeFocus.getDictionaryHandle();
    }

    public Expression getMainExpression() {
        return mainExpression;
    }

    public Node getMainNode() {
        return mainNodeFocus.getMainNode();
    }

    public ProverFeatures getProverFeatures() {
        return proverFeatures;
    }

    public Sentence getMainSentence() {
        return mainSentence;
    }

    public Sentence getFullSentence() {
        return fullSentence;
    }

    public int[][] getNumbers() {
        return numbers;
    }

    public int getNumberOfAtoms() {
        return numbering.getSize();
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        out.println("Main Expression: " + getMainExpression());

        indent(out, level);
        out.println("Main Node Focus");
        mainNodeFocus.print(out, level + 1);

        indent(out, level);
        out.println("Prover Features: " + getProverFeatures());

        indent(out, level);
        out.println("Main Sentence: " + getMainSentence());
        for (final Clause clause : getMainSentence().getClauses()) {
            indent(out, level + 1);
            out.println(clause.getContent(false));
        }

        indent(out, level);
        out.println("Related Sentences " + relatedSentences.size());
        for (final Sentence sentence : relatedSentences) { // TODO sort
            indent(out, level + 1);
            out.println(sentence);
            for (final Clause clause : sentence.getClauses()) {
                indent(out, level + 2);
                out.println(clause.getContent(false));
            }
        }

        indent(out, level);
        out.println("Integer CPB");
        cpbInteger.print(out, level + 1);

        indent(out, level);
        out.println("Real CPB");
        cpbReal.print(out, level + 1);

        indent(out, level);
        out.println("Mapping Sentences " + mappingSentences.size());
        for (final Sentence sentence : mappingSentences) { // TODO sort
            indent(out, level + 1);
            out.println(sentence);
        }

        indent(out, level);
        out.println("Full Sentence: " + getFullSentence());
        for (final Clause clause : getFullSentence().getClauses()) {
            indent(out, level + 1);
            out.println(clause.getContent(false));
        }

        indent(out, level);
        out.println("Numbering");
        numbering.print(out, level + 1);

        indent(out, level);
        out.println("Clauses " + getFullSentence().getClauses().size());
        numbering.print(getFullSentence(), out, level + 1);
    }
}