package cdc.applic.proofs.core.clauses;

import java.util.Arrays;

import cdc.applic.dictionaries.handles.ApplicCacheId;
import cdc.applic.dictionaries.handles.ComputationCache;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.EquivalenceNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.ProverMatching;
import cdc.applic.proofs.core.SatSystemSolver;
import cdc.applic.proofs.core.SatSystemSolverFactory;
import cdc.applic.proofs.core.ast.AtomVarNode;
import cdc.applic.proofs.core.encoding.Encoders;
import cdc.applic.proofs.core.visitors.ContainsAtomVar;
import cdc.util.lang.Checks;

public class ProverImpl implements Prover {
    private final SatSystemSolver solver;
    private final DictionaryHandle handle;
    private final ProverFeatures features;
    private final SatisfiableCache satisfiableCache;
    private final ProverMatchingCache proverMatchingCache;

    private static class SatisfiableCache extends ComputationCache<Boolean> {
        protected SatisfiableCache(DictionaryHandle handle,
                                   ProverFeatures tag) {
            super(handle,
                  tag);
        }
    }

    private static class ProverMatchingCache extends ComputationCache<ProverMatching> {
        protected ProverMatchingCache(DictionaryHandle handle,
                                      ProverFeatures tag) {
            super(handle,
                  tag);
        }
    }

    public ProverImpl(DictionaryHandle handle,
                      ProverFeatures features) {
        Checks.isNotNull(handle, "handle");
        Checks.isNotNull(features, "features");

        final SatSystemSolverFactory factory = new SatSystemSolverFactory();
        this.solver = factory.newSolver();
        this.handle = handle;
        this.features = features;
        this.satisfiableCache = handle.computeIfAbsent(SatisfiableCache.class,
                                                       new ApplicCacheId(SatisfiableCache.class, features),
                                                       (c,
                                                        t) -> new SatisfiableCache(c, (ProverFeatures) t));
        this.proverMatchingCache = handle.computeIfAbsent(ProverMatchingCache.class,
                                                          new ApplicCacheId(ProverMatchingCache.class, features),
                                                          (c,
                                                           t) -> new ProverMatchingCache(c, (ProverFeatures) t));
    }

    /**
     * Checks that this node does not contain {@link AtomVarNode}s.
     *
     * @param node The node.
     * @throws IllegalArgumentException When {@code node} contains {@link AtomVarNode}s.
     */
    private static void checkNoVar(Node node) {
        if (ContainsAtomVar.execute(node)) {
            throw new IllegalArgumentException("Unexpected AtomVarNode found in " + node);
        }
    }

    @Override
    public DictionaryHandle getDictionaryHandle() {
        return handle;
    }

    @Override
    public ProverFeatures getFeatures() {
        return features;
    }

    /**
     * Returns {@code true} if a Node is satisfiable.
     *
     * @param node The node. Not checked for {@code null}.
     * @return {@code true} if {@code node} is satisfiable.
     */
    private boolean isSatisfiable(Node node) {
        return satisfiableCache.computeIfAbsent(node,
                                                n -> {
                                                    final SatSystem system = new SatSystem(handle, node, features);
                                                    return solver.isSatisfiable(system);
                                                });
    }

    @Override
    public boolean isNeverTrue(Node node) {
        Checks.isNotNull(node, "node");

        return !isSatisfiable(node);
    }

    @Override
    public boolean isNeverTrue(Expression expression) {
        Checks.isNotNull(expression, "expression");

        return isNeverTrue(expression.getRootNode());
    }

    @Override
    public boolean isSometimesTrue(Node node) {
        Checks.isNotNull(node, "node");

        return isSatisfiable(node);
    }

    @Override
    public boolean isSometimesTrue(Expression expression) {
        Checks.isNotNull(expression, "expression");

        return isSometimesTrue(expression.getRootNode());
    }

    @Override
    public boolean isAlwaysTrue(Node node) {
        Checks.isNotNull(node, "node");
        checkNoVar(node);

        return isSatisfiable(node)
                && !isSatisfiable(new NotNode(node));
    }

    @Override
    public boolean isAlwaysTrue(Expression expression) {
        Checks.isNotNull(expression, "expression");

        return isAlwaysTrue(expression.getRootNode());
    }

    @Override
    public boolean isNeverFalse(Node node) {
        Checks.isNotNull(node, "node");

        return !isSatisfiable(new NotNode(node));
    }

    @Override
    public boolean isNeverFalse(Expression expression) {
        Checks.isNotNull(expression, "expression");

        return isNeverFalse(expression.getRootNode());
    }

    @Override
    public boolean isSometimesFalse(Node node) {
        Checks.isNotNull(node, "node");

        return isSatisfiable(new NotNode(node));
    }

    @Override
    public boolean isSometimesFalse(Expression expression) {
        Checks.isNotNull(expression, "expression");

        return isSometimesFalse(expression.getRootNode());
    }

    @Override
    public boolean isAlwaysFalse(Node node) {
        Checks.isNotNull(node, "node");
        checkNoVar(node);

        return isSatisfiable(new NotNode(node))
                && !isSatisfiable(node);
    }

    @Override
    public boolean isAlwaysFalse(Expression expression) {
        Checks.isNotNull(expression, "expression");

        return isAlwaysFalse(expression.getRootNode());
    }

    @Override
    public boolean areAlwaysEquivalent(Node alpha,
                                       Node beta) {
        Checks.isNotNull(alpha, "alpha");
        Checks.isNotNull(beta, "beta");

        return isAlwaysTrue(new EquivalenceNode(alpha, beta));
    }

    @Override
    public boolean areAlwaysEquivalent(Expression alpha,
                                       Expression beta) {
        Checks.isNotNull(alpha, "alpha");
        Checks.isNotNull(beta, "beta");

        return areAlwaysEquivalent(alpha.getRootNode(), beta.getRootNode());
    }

    @Override
    public boolean intersects(Node alpha,
                              Node beta) {
        return isSometimesTrue(new AndNode(alpha, beta));
    }

    @Override
    public boolean intersects(Expression alpha,
                              Expression beta) {
        return intersects(alpha.getRootNode(), beta.getRootNode());
    }

    @Override
    public boolean contains(Node outer,
                            Node inner) {
        // VSAT(inner) included into VSAT(outer)
        // intersection(VSAT(inner), VSAT(!outer)) = empty
        // !SAT(inner & !outer)

        return !isSatisfiable(new AndNode(inner, new NotNode(outer)));
    }

    @Override
    public boolean contains(Expression outer,
                            Expression inner) {
        return contains(outer.getRootNode(), inner.getRootNode());
    }

    @Override
    public boolean containsNonEmpty(Node outer,
                                    Node inner) {
        return contains(outer, inner) && isSatisfiable(inner);
    }

    @Override
    public boolean containsNonEmpty(Expression outer,
                                    Expression inner) {
        return containsNonEmpty(outer.getRootNode(), inner.getRootNode());
    }

    @Override
    public boolean isContained(Node inner,
                               Node outer) {
        return contains(outer, inner);
    }

    @Override
    public boolean isContained(Expression inner,
                               Expression outer) {
        return contains(outer, inner);
    }

    @Override
    public boolean isNonEmptyContained(Node inner,
                                       Node outer) {
        return containsNonEmpty(outer, inner);
    }

    @Override
    public boolean isNonEmptyContained(Expression inner,
                                       Expression outer) {
        return containsNonEmpty(outer, inner);
    }

    @Override
    public boolean alwaysAtLeastOne(Node... nodes) {
        Checks.isNotNullOrEmpty(nodes, "nodes");

        return isAlwaysTrue(Encoders.atLeastOne(Arrays.asList(nodes)));
    }

    @Override
    public boolean alwaysAtLeastOne(Expression... expressions) {
        Checks.isNotNullOrEmpty(expressions, "expressions");

        return alwaysAtLeastOne(Expression.toNodeArray(expressions));
    }

    @Override
    public boolean alwaysAtMostOne(Node... nodes) {
        return isAlwaysTrue(Encoders.atMostOneNegatable(Arrays.asList(nodes)));
    }

    @Override
    public boolean alwaysAtMostOne(Expression... expressions) {
        return alwaysAtMostOne(Expression.toNodeArray(expressions));
    }

    @Override
    public boolean alwaysExactlyOne(Node... nodes) {
        return isAlwaysTrue(Encoders.exactlyOneNegatable(Arrays.asList(nodes)));
    }

    @Override
    public boolean alwaysExactlyOne(Expression... expressions) {
        return alwaysExactlyOne(Expression.toNodeArray(expressions));
    }

    @Override
    public boolean alwaysAtLeastOneInContext(Node context,
                                             Node... nodes) {
        return containsNonEmpty(Encoders.atLeastOne(Arrays.asList(nodes)), context);
    }

    @Override
    public boolean alwaysAtLeastOneInContext(Expression context,
                                             Expression... expressions) {
        return alwaysAtLeastOneInContext(context.getRootNode(),
                                         Expression.toNodeArray(expressions));
    }

    @Override
    public boolean alwaysAtMostOneInContext(Node context,
                                            Node... nodes) {
        return contains(Encoders.atMostOneNegatable(Arrays.asList(nodes)), context);
    }

    @Override
    public boolean alwaysAtMostOneInContext(Expression context,
                                            Expression... expressions) {
        return alwaysAtMostOneInContext(context.getRootNode(),
                                        Expression.toNodeArray(expressions));
    }

    @Override
    public boolean alwaysExactlyOneInContext(Node context,
                                             Node... nodes) {
        return containsNonEmpty(Encoders.exactlyOneNegatable(Arrays.asList(nodes)), context);
    }

    @Override
    public boolean alwaysExactlyOneInContext(Expression context,
                                             Expression... expressions) {
        return alwaysExactlyOneInContext(context.getRootNode(),
                                         Expression.toNodeArray(expressions));
    }

    @Override
    public ProverMatching getMatching(Node node) {
        Checks.isNotNull(node, "node");
        checkNoVar(node);

        return proverMatchingCache.computeIfAbsent(node,
                                                   n -> {
                                                       if (!isSatisfiable(n)) {
                                                           // n has no valid solutions
                                                           return ProverMatching.NEVER;
                                                       } else if (isSatisfiable(new NotNode(n))) {
                                                           // n has at least one valid solution
                                                           // !n has at least one valid solution.
                                                           return ProverMatching.SOMETIMES;
                                                       } else {
                                                           // n has at least one valid solution
                                                           // !n has no valid solutions
                                                           // So deduce that all points defined by n are valid solutions.
                                                           return ProverMatching.ALWAYS;
                                                       }
                                                   });
    }

    @Override
    public ProverMatching getMatching(Expression expression) {
        Checks.isNotNull(expression, "expression");

        return getMatching(expression.getRootNode());
    }

    @Override
    public ProverMatching getProjectedMatching(Node domain,
                                               Node on) {
        final Node intersection = new AndNode(domain, on);
        if (!isSatisfiable(intersection)) {
            // domain and on don't share any valid part
            return ProverMatching.NEVER;
        } else if (contains(intersection, on)) {
            // domain and on have a non empty valid intersection
            // valid part of on is totally included in valid intersection
            // so, valid part of on is totally included in valid part of domain
            return ProverMatching.ALWAYS;
        } else {
            // domain and on have a non empty valid intersection
            // valid part of on is outside of valid intersection
            return ProverMatching.SOMETIMES;
        }
    }

    @Override
    public ProverMatching getProjectedMatching(Expression domain,
                                               Expression on) {
        return getProjectedMatching(domain.getRootNode(),
                                    on.getRootNode());
    }
}