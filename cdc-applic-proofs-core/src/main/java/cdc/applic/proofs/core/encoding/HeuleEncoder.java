package cdc.applic.proofs.core.encoding;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.proofs.core.ast.AtomVarNode;

public final class HeuleEncoder implements Encoder {
    public static final HeuleEncoder INSTANCE = new HeuleEncoder();

    @Override
    public void addAtMostOne(List<? extends Node> literals,
                             List<Node> clauses,
                             String prefix) {
        final List<Node> x = new ArrayList<>(literals);
        addAtMostOne(x, clauses, prefix, 1);
    }

    private void addAtMostOne(List<Node> x,
                              List<Node> clauses,
                              String prefix,
                              int next) {
        final int n = x.size();
        if (n <= 4) {
            BinomialEncoder.addAtMostOne(x, clauses);
        } else {
            final Node y = new AtomVarNode(prefix + next);
            BinomialEncoder.addAtMostOne(x.get(0),
                                         x.get(1),
                                         x.get(2),
                                         y,
                                         clauses);
            x.set(2, new NotNode(y));
            addAtMostOne(x.subList(2, x.size()),
                         clauses,
                         prefix,
                         next + 1);
        }
    }
}