package cdc.applic.proofs.core;

import cdc.applic.proofs.core.sat4j.Sat4jSystemSolver;

/**
 * Factory of {@link SatSystemSolver}s.
 *
 * @author Damien Carbonne
 */
public class SatSystemSolverFactory {
    public SatSystemSolverFactory() {
        super();
    }

    public SatSystemSolver newSolver() {
        return new Sat4jSystemSolver();
    }
}