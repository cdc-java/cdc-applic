package cdc.applic.proofs.core.visitors;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.ast.AbstractBinaryNode;
import cdc.applic.expressions.ast.AbstractNaryNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.EquivalenceNode;
import cdc.applic.expressions.ast.ImplicationNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.expressions.ast.visitors.QualifiedValue;
import cdc.applic.expressions.ast.visitors.QualifiedValue.Quality;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.clauses.ProverImpl;

/**
 * Converters that applies the following rewriting rules:
 * <ul>
 * <li>&alpha;&or;&beta; with &alpha;&rarr;&beta; &equiv; &beta;
 * <li>&alpha;&or;&beta; with &beta;&rarr;&alpha; &equiv; &alpha;
 * <li>&alpha;&or;&beta; with &alpha;&harr;&beta; &equiv; &alpha; &equiv; &beta;<br>
 * In that case, choice should be controlled.
 *
 * <li>&alpha;&and;&beta; with &alpha;&rarr;&beta; &equiv; &alpha;
 * <li>&alpha;&and;&beta; with &beta;&rarr;&alpha; &equiv; &beta;
 * <li>&alpha;&and;&beta; with &alpha;&harr;&beta; &equiv; &alpha; &equiv; &beta;<br>
 * In that case, choice should be controlled.
 * </ul>
 * <b>Note:</b> {@link ConvertToNary} should be call before calling this converter.
 *
 * @author Damien Carbonne
 */
public final class EliminateRedundantSiblings extends AbstractConverter {
    private static final Logger LOGGER = LogManager.getLogger(EliminateRedundantSiblings.class);
    private final Prover prover;
    private final int maxMillis;
    private final long start;
    private boolean active = true;
    private QualifiedValue.Quality quality = Quality.SUCCESS;

    private EliminateRedundantSiblings(DictionaryHandle handle,
                                       ProverFeatures proverFeatures,
                                       int maxMillis) {
        this.prover = new ProverImpl(handle, proverFeatures);
        this.maxMillis = maxMillis;
        this.start = System.nanoTime();
    }

    /**
     * If {@link #maxMillis} is greater or equal to 0, checks that elapsed time
     * has not been overtaken.<br>
     * If it is the case, {@link #active} is set to {@code false}.
     */
    private void checkElapsedTime() {
        if (maxMillis >= 0 && active) {
            final int elapsedMillis = (int) ((System.nanoTime() - start) / 1_000_000L);
            active = elapsedMillis < maxMillis;
            if (!active) {
                LOGGER.warn("Reached timeout {} ms", maxMillis);
                quality = QualifiedValue.Quality.PARTIAL;
            }
        }
    }

    public static QualifiedValue<Node> execute(Node node,
                                               DictionaryHandle handle,
                                               ProverFeatures features,
                                               int maxMillis) {
        final EliminateRedundantSiblings visitor =
                new EliminateRedundantSiblings(handle,
                                               features,
                                               maxMillis);
        final Node value = node.accept(visitor);
        return new QualifiedValue<>(value, visitor.quality);
    }

    /**
     * Returns {@code true} when 2 nodes are always equivalent.
     *
     * @param alpha The first node.
     * @param beta The second node.
     * @return {@code true} when {@code alpha} and {@code beta} are always equivalent.
     */
    private boolean equivalent(Node alpha,
                               Node beta) {
        if (alpha.equals(beta)) {
            return true;
        } else {
            return prover.isAlwaysTrue(new EquivalenceNode(alpha, beta));
        }
    }

    /**
     * Returns {@code true} when a list of nodes contains a node that
     * is equivalent with a given node.
     *
     * @param nodes The nodes.
     * @param beta The tested node.
     * @return {@code true} when {@code nodes} contains a node that
     *         is equivalent with {@code beta}.
     */
    private boolean containsEquivalent(List<Node> nodes,
                                       Node beta) {
        for (final Node node : nodes) {
            if (equivalent(node, beta)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns {@code true} when a node always imply another one.
     *
     * @param alpha The first node.
     * @param beta The second node.
     * @return {@code true} when {@code alpha} always implies {@code beta}.
     */
    private boolean implies(Node alpha,
                            Node beta) {
        if (alpha.equals(beta)) {
            return true;
        } else {
            // We must handle FALSE -> X with care
            // as it is equivalent to TRUE!
            // Otherwise, we may produce unexpected results.
            final boolean result = prover.isSometimesTrue(alpha)
                    && prover.isAlwaysTrue(new ImplicationNode(alpha, beta));

            // TODO use containsNonEmpty(alpha, beta) ?
            return result;
        }
    }

    /**
     * Returns {@code true} when a list of nodes contains a node that
     * always implies a given node.
     *
     * @param nodes The nodes.
     * @param beta The tested node.
     * @return {@code true} when {@code nodes} contains a node that
     *         always implies {@code beta}.
     */
    private boolean containsImplies(List<Node> nodes,
                                    Node beta) {
        for (final Node node : nodes) {
            if (implies(node, beta)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Removes from {@code nodes} all nodes that are implied by {@code alpha}.
     *
     * @param alpha The alpha node.
     * @param nodes The nodes.
     * @return {@code true} when {@code nodes} is not empty in the end.
     */
    private boolean removeAllImplied(Node alpha,
                                     List<Node> nodes) {
        final List<Node> removed = new ArrayList<>();
        for (final Node beta : nodes) {
            if (implies(alpha, beta)) {
                removed.add(beta);
            }
        }
        nodes.removeAll(removed);
        return !removed.isEmpty();
    }

    /**
     *
     * @param alpha The alpha node.
     * @param nodes The tested nodes.
     * @return {@code true} if {@code nodes} contains nodes implied by {@code alpha}.
     */
    private boolean containsImplies(Node alpha,
                                    List<Node> nodes) {
        for (final Node beta : nodes) {
            if (implies(alpha, beta)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Removes from {@code nodes} all nodes that imply {@coe beta}.
     *
     * @param nodes The nodes.
     * @param beta The beta node.
     * @return {@code true} when {@code nodes} is not empty in the end.
     */
    private boolean removeAllImplies(List<Node> nodes,
                                     Node beta) {
        final List<Node> removed = new ArrayList<>();
        for (final Node alpha : nodes) {
            if (implies(alpha, beta)) {
                removed.add(alpha);
            }
        }
        nodes.removeAll(removed);
        return !removed.isEmpty();
    }

    @Override
    public Node visitBinary(AbstractBinaryNode node) {
        checkElapsedTime();

        if (active) {
            if (node instanceof AndNode) {
                // A and B
                if (equivalent(node.getAlpha(), node.getBeta())
                        || implies(node.getAlpha(), node.getBeta())) {
                    // A <-> B or A -> B replaced by A
                    return node.getAlpha();
                } else if (implies(node.getBeta(), node.getAlpha())) {
                    // B -> A replaced by B
                    return node.getBeta();
                }
            } else {
                // A or B
                if (equivalent(node.getAlpha(), node.getBeta())
                        || implies(node.getBeta(), node.getAlpha())) {
                    // A <-> B or B -> A replaced by A
                    return node.getAlpha();
                } else if (implies(node.getAlpha(), node.getBeta())) {
                    // A -> B replaced by B
                    return node.getBeta();
                }
            }
        }
        return super.visitBinary(node);
    }

    @Override
    public Node visitNary(AbstractNaryNode node) {
        checkElapsedTime();

        if (active) {
            boolean changed = false;
            // List of nodes that are kept
            final List<Node> filtered = new ArrayList<>();
            if (node instanceof NaryAndNode) {
                for (final Node child : node.getChildren()) {
                    if (containsEquivalent(filtered, child)
                            || containsImplies(filtered, child)) {
                        // No need to add child as it is equivalent to, or implied
                        // by, a node of filtered
                        changed = true;
                    } else {
                        // Remove from filtered all nodes that are implied by child
                        final boolean removed = removeAllImplied(child, filtered);
                        filtered.add(child);
                        if (removed) {
                            changed = true;
                        }
                    }
                }
            } else {
                for (final Node child : node.getChildren()) {
                    if (containsEquivalent(filtered, child)
                            || containsImplies(child, filtered)) {
                        // No need to add child as it is equivalent to, or implies
                        // a node of filtered
                        changed = true;
                    } else {
                        // Remove from filtered all nodes that imply child
                        final boolean removed = removeAllImplies(filtered, child);
                        filtered.add(child);
                        if (removed) {
                            changed = true;
                        }
                    }
                }
            }
            if (changed) {
                return node.create(filtered);
            } else {
                return super.visitNary(node);
            }
        } else {
            return super.visitNary(node);
        }
    }
}