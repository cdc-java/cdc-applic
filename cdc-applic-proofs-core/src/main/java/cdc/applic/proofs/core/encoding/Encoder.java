package cdc.applic.proofs.core.encoding;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.TrueNode;
import cdc.util.lang.Checks;

/**
 * Base interface used to encode 'at least', 'at most' and 'exactly one' constraints.
 * <p>
 * This can be generalized to 'at least k', 'at most k' and 'exactly k', but this is not useful at the moment.<br>
 * For generalization, see 'SAT Encodings of the At-Most-k Constraint' (Alan M. Frisch and Paul A. Giannaros).
 *
 * @author Damien Carbonne
 */
public interface Encoder {
    /**
     * Adds to a list of clauses, the clauses corresponding to {@code 'at least one (nodes)'}.
     * <p>
     * If {@code nodes} is empty, adds {@link FalseNode#INSTANCE}.<br>
     * Otherwise, adds the simplest or combination of {@code nodes}.
     *
     * @param nodes The nodes.
     * @param clauses The clauses.
     * @throws IllegalArgumentException When {@code nodes} is {@code null},
     *             or when {@code clauses} is {@code null}.
     */
    public static void addAtLeastOne(List<? extends Node> nodes,
                                     List<Node> clauses) {
        Checks.isNotNull(nodes, "nodes");
        Checks.isNotNull(clauses, "clauses");

        if (nodes.isEmpty()) {
            clauses.add(FalseNode.INSTANCE);
        } else {
            clauses.add(NaryOrNode.createSimplestOr(nodes));
        }
    }

    /**
     * Returns the node corresponding to {@code 'at least one (nodes)'}.
     * <p>
     * If {@code nodes} is empty, returns {@link FalseNode#INSTANCE}.<br>
     * Otherwise, returns the simplest or combination of {@code nodes}.
     *
     * @param nodes The nodes.
     * @return The node corresponding to {@code 'at least one (nodes)'}.
     * @throws IllegalArgumentException When {@code nodes} is {@code null}.
     */
    public static Node atLeastOne(List<? extends Node> nodes) {
        Checks.isNotNull(nodes, "nodes");

        if (nodes.isEmpty()) {
            return FalseNode.INSTANCE;
        } else {
            return NaryOrNode.createSimplestOr(nodes);
        }
    }

    /**
     * Adds to a list of clauses, the clauses corresponding to {@code 'at most one (nodes)'}.
     * <p>
     * If {@code nodes} is empty or contains one node, does nothing.
     *
     * @param nodes The nodes.
     * @param clauses The clauses.
     * @param prefix Prefix to use to create optional variables.
     */
    public void addAtMostOne(List<? extends Node> nodes,
                             List<Node> clauses,
                             String prefix);

    /**
     * Returns the node corresponding to {@code 'at most one (nodes)'}.
     * <p>
     * If {@code nodes} is empty or contains one node, returns {@link TrueNode#INSTANCE}.
     *
     * @param nodes The nodes.
     * @param prefix Prefix to use to create optional variables.
     * @return The node corresponding to {@code 'at most one (nodes)'}.
     */
    public default Node atMostOne(List<? extends Node> nodes,
                                  String prefix) {
        Checks.isNotNullOrEmpty(nodes, "nodes");

        final List<Node> clauses = new ArrayList<>();
        addAtMostOne(nodes, clauses, prefix);
        if (clauses.isEmpty()) {
            return TrueNode.INSTANCE;
        } else {
            return NaryAndNode.createSimplestAnd(clauses);
        }
    }

    /**
     * Adds to a list of clauses, the clauses corresponding to {@code 'exactly one (nodes)'}.
     *
     * @param nodes The nodes.
     * @param clauses The buffer of clauses.
     * @param prefix Prefix to use to create optional variables.
     */
    public default void addExactlyOne(List<? extends Node> nodes,
                                      List<Node> clauses,
                                      String prefix) {
        Checks.isNotNullOrEmpty(nodes, "nodes");
        Checks.isNotNull(clauses, "clauses");

        addAtLeastOne(nodes, clauses);
        addAtMostOne(nodes, clauses, prefix);
    }

    /**
     * Returns the node corresponding to {@code 'exactly one (nodes)'}.
     *
     * @param nodes The nodes.
     * @param prefix Prefix to use to create optional variables.
     * @return The node corresponding to {@code 'exactly one (nodes)'}.
     */
    public default Node exactlyOne(List<? extends Node> nodes,
                                   String prefix) {
        Checks.isNotNullOrEmpty(nodes, "nodes");

        final List<Node> clauses = new ArrayList<>();
        addExactlyOne(nodes, clauses, prefix);
        return NaryAndNode.createSimplestAnd(clauses);
    }
}