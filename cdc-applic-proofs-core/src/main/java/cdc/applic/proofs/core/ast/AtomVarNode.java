package cdc.applic.proofs.core.ast;

import java.io.PrintStream;

import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.AbstractNamedNode;
import cdc.applic.expressions.ast.AtomNode;
import cdc.applic.expressions.ast.NodeKerning;
import cdc.applic.expressions.literals.Name;
import cdc.util.debug.Printables;

/**
 * Atom leaf node representing a variable (&rho;).
 * <p>
 * Variables are created by some transformations algorithms, e.g., Tseitin
 * transformation or Commander encoding.<br>
 * They are also used to represent a boolean property.
 *
 * @author Damien Carbonne
 */
public final class AtomVarNode extends AbstractNamedNode implements AtomNode {
    public static final String KIND = "VAR";

    public AtomVarNode(Name name) {
        super(name);
    }

    public AtomVarNode(String literal,
                       boolean escaped) {
        super(Name.of(literal, escaped));
    }

    public AtomVarNode(String literal) {
        this(literal, false);
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_ATOM;
    }

    @Override
    public final void buildInfix(Formatting formatting,
                                 StringBuilder builder) {
        builder.append(getName());
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        Printables.indent(out, level);
        out.println(getKind() + " " + getName());
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof AtomVarNode)) {
            return false;
        }
        final AtomVarNode other = (AtomVarNode) object;
        return name.equals(other.name);
    }

    @Override
    public String toString() {
        return getKind() + "(" + getName() + ")";
    }
}