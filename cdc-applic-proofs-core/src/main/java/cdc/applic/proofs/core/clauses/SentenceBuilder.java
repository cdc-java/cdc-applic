package cdc.applic.proofs.core.clauses;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.core.visitors.ConvertInequalitiesToSets;
import cdc.applic.dictionaries.core.visitors.ConvertNamingConvention;
import cdc.applic.dictionaries.visitors.AddMissingPrefixes;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.AbstractUnaryNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.AtomNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.expressions.ast.visitors.IsCNF;
import cdc.applic.proofs.core.visitors.ConvertToAtoms;
import cdc.applic.proofs.core.visitors.ConvertToTseitin;
import cdc.util.lang.Checks;

/**
 * Class used to build a {@link Sentence} associated to an {@link Expression} and all related constraints.
 *
 * @author Damien Carbonne
 */
public final class SentenceBuilder {
    private SentenceBuilder() {
    }

    /**
     * Creates a {@link Clause} from {@link Literal Literals} (not atom or atom).
     * <p>
     * Must be called with nodes being NotNode(AtomNode) or AtomNode.
     *
     * @param nodes The nodes.
     * @return The created clause.
     * @throws IllegalArgumentException When unexpected nodes are encountered.
     *             This is the case when preparation was not done correctly.
     */
    private static Clause createClauseLow(Node... nodes) {
        final List<Literal> literals = new ArrayList<>();
        for (final Node node : nodes) {
            if (node instanceof NotNode) {
                final AbstractUnaryNode unode = (AbstractUnaryNode) node;
                if (unode.getAlpha() instanceof final AtomNode atom) {
                    literals.add(new Literal(atom, false));
                } else {
                    throw new IllegalArgumentException("Can not add negation of " + unode.getAlpha().getKind() + " nodes, in "
                            + Arrays.toString(nodes));
                }
            } else if (node instanceof final AtomNode atom) {
                literals.add(new Literal(atom, true));
            } else {
                throw new IllegalArgumentException("Can not add " + node + " node, expected not or atom, in "
                        + Arrays.toString(nodes));
            }
        }
        return new Clause(literals);
    }

    /**
     * Must be called with node being AndNode, NaryAndNode, NodeNode or AtomNode.
     *
     * @param node The node
     * @return The created clause.
     * @throws IllegalArgumentException When unexpected nodes are encountered.
     *             This is the case when preparation was not done correctly.
     */
    private static Clause createClauseHigh(Node node) {
        final Clause clause;
        if (node instanceof final NaryOrNode n) {
            clause = createClauseLow(n.getChildren());
        } else if (node instanceof final OrNode n) {
            clause = createClauseLow(n.getAlpha(), n.getBeta());
        } else {
            clause = createClauseLow(node);
        }
        return clause;
    }

    /**
     * Prepares a node for conversion to Sentence.
     * <p>
     * This is a CNF transformation using only atoms.
     * <ol>
     * <li>Missing prefixes are added for items belonging to {@code dictionary}.
     * <li>Default naming convention is used.
     * <li>All leaves are transformed to atoms.</li>
     * <li>Then Tseitin transformation is applied.</li>
     * <li>Finally, a conversion to CNF is applied.</li>
     * </ol>
     *
     * @param dictionary The dictionary.
     * @param node The node.
     * @param prefix The prefix to use for Tseitin application.
     * @return A CNF node, ready to be converted to a Sentence.
     */
    private static Node prepare(Dictionary dictionary,
                                Node node,
                                String prefix) {
        Node step = node;
        step = AddMissingPrefixes.execute(step, dictionary);
        step = ConvertNamingConvention.convertToDefault(step, dictionary);
        step = ConvertInequalitiesToSets.execute(step, dictionary);
        step = ConvertToAtoms.execute(step, dictionary);
        step = ConvertToNary.execute(step, ConvertToNary.Variant.WHEN_NECESSARY);
        if (!IsCNF.isCNF(step)) {
            step = ConvertToTseitin.execute(step, ConvertToTseitin.Variant.PREPARE_CNF, prefix, null);
        }
        return step;
    }

    /**
     * Converts a prepared node to a {@link Sentence}.
     * <p>
     * Prepared node should:
     * <ul>
     * <li>Always use prefixes.
     * <li>Always use default naming convention.
     * <li>Contains only atoms and operators.
     * <li>Have binary nodes converted to nary nodes when necessary.
     * <li>Be CNF ready (Tseitin conversion possibly).
     * </ul>
     *
     * @param node The node. Must have been prepared.
     * @return A Sentence view of node.
     * @throws IllegalArgumentException When preparation was not done correctly.
     */
    public static Sentence convertPrepared(Node node) {
        Checks.isNotNull(node, "node");
        final List<Clause> clauses = new ArrayList<>();

        if (node instanceof final NaryAndNode nary) {
            for (final Node child : nary.getChildren()) {
                clauses.add(createClauseHigh(child));
            }
        } else if (node instanceof final AndNode and) {
            clauses.add(createClauseHigh(and.getAlpha()));
            clauses.add(createClauseHigh(and.getBeta()));
        } else if (node instanceof final NaryOrNode nary) {
            clauses.add(createClauseLow(nary.getChildren()));
        } else if (node instanceof final OrNode or) {
            clauses.add(createClauseLow(or.getAlpha(), or.getBeta()));
        } else if (node instanceof TrueNode) {
            // Ignore
        } else {
            final Clause clause = createClauseLow(node);
            clauses.add(clause);
        }

        return new Sentence(clauses);
    }

    /**
     * Converts a non-prepared node to a {@link Sentence}.
     * <p>
     * This is equivalent to: <code>convert(prepare, dictionary, node, prefix)).</code>
     *
     * @param dictionary The dictionary.
     * @param node The node.
     * @param prefix The prefix to use for Tseitin application.
     * @return A Sentence view of node.
     */
    public static Sentence convert(Dictionary dictionary,
                                   Node node,
                                   String prefix) {
        final Node prepared = prepare(dictionary, node, prefix);
        return convertPrepared(prepared);
    }
}