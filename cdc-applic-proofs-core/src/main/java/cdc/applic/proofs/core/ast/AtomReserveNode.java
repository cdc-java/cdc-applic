package cdc.applic.proofs.core.ast;

import java.io.PrintStream;

import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.Formatting;
import cdc.applic.proofs.core.utils.PropertyDefinition;
import cdc.util.debug.Printables;

/**
 * Atom leaf Node representing the reserve value.
 * <p>
 * An atom reserve node is created for the {@link PropertyDefinition definition of properties}
 * that has a declared reserve.
 *
 * @author Damien Carbonne
 */
public final class AtomReserveNode extends AbstractAtomNode {
    public static final String KIND = "RESERVE";

    public AtomReserveNode(Property property) {
        super(property);
    }

    @Override
    public String getKind() {
        return KIND;
    }

    @Override
    public final void buildInfix(Formatting formatting,
                                 StringBuilder builder) {
        builder.append(getProperty().getName());
        builder.append("@R@");
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        Printables.indent(out, level);
        out.println(getKind() + " " + getProperty().getName());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof AtomReserveNode)) {
            return false;
        }
        final AtomReserveNode other = (AtomReserveNode) object;
        return getProperty() == other.getProperty();
    }

    @Override
    public int hashCode() {
        return getKind().hashCode()
                + 31 * getProperty().hashCode();
    }

    @Override
    public String toString() {
        return getKind() + "(" + getProperty().getName() + ")";
    }
}