package cdc.applic.proofs.core.encoding;

import java.util.ArrayList;
import java.util.List;

import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.proofs.core.ast.AtomVarNode;
import cdc.util.lang.Checks;

/**
 * Commander encoding of "at most one", "exactly one".
 * See "Efficient CNF Encoding for Selecting 1 from N Objects" (Will Klieber, Gihwon Kwon).
 *
 * @author Damien Carbonne
 */
public final class CommanderEncoder implements Encoder {
    public static final CommanderEncoder INSTANCE = new CommanderEncoder();

    @Override
    public void addExactlyOne(List<? extends Node> literals,
                              List<Node> clauses,
                              String prefix) {
        addRec(literals, clauses, 3, true, prefix, 1);
    }

    @Override
    public void addAtMostOne(List<? extends Node> literals,
                             List<Node> clauses,
                             String prefix) {
        addRec(literals, clauses, 3, false, prefix, 1);
    }

    /**
     * Recursive method, supporting at most one and exactly one variants.
     *
     * @param literals The list of literal for which clauses must be generated.
     * @param clauses The list of clauses to fill.
     * @param gsize The group size.
     * @param exactly If true, generate an exactly one formula. A at most one formula otherwise.
     * @param prefix The prefix to use for commander names.
     * @param from The first number to use for commanders
     */
    private void addRec(List<? extends Node> literals,
                        List<Node> clauses,
                        int gsize,
                        boolean exactly,
                        String prefix,
                        int from) {
        Checks.isNotNullOrEmpty(literals, "literals");
        Checks.isNotNull(clauses, "clauses");

        final int size = literals.size();

        if (size <= gsize) {
            if (exactly) {
                BinomialEncoder.INSTANCE.addExactlyOne(literals, clauses, prefix);
            } else {
                BinomialEncoder.INSTANCE.addAtMostOne(literals, clauses, prefix);
            }
        } else {
            // The number of groups
            // They all have the same size (gsize), except the last one that can be smaller (but at least 1)
            final int numGroups = (size + gsize - 1) / gsize;

            // Create one commander per group.
            final List<AtomVarNode> commanders = new ArrayList<>();
            for (int index = 0; index < numGroups; index++) {
                commanders.add(new AtomVarNode(prefix + (from + index)));
            }

            // Use for C -> X1 or X2 or ...
            final List<Node> buffer = new ArrayList<>();

            for (int groupIndex = 0; groupIndex < numGroups; groupIndex++) {
                // Offset for getting literals of the group
                final int offset = groupIndex * gsize;

                // Size of the group
                // Last group has a special size, possibly smaller than gsize
                // Other groups have the same size: gsize
                final int groupSize = groupIndex == (numGroups - 1) // last group ?
                        ? size - (numGroups - 1) * gsize // last group
                        : gsize; // other groups

                // Compute the negation of each literal
                final Node[] notLiterals = new Node[groupSize];
                for (int index = 0; index < groupSize; index++) {
                    notLiterals[index] = NotNode.simplestNot(literals.get(offset + index));
                }

                // At most one variable in a group can be true
                // All pairs (not Xi or not Xj) with 0 <= i < j < groupSize
                // If group contains one literal, nothing is generated
                for (int i = 0; i < groupSize - 1; i++) {
                    for (int j = i + 1; j < groupSize; j++) {
                        clauses.add(new OrNode(notLiterals[i],
                                               notLiterals[j]));
                    }
                }

                if (exactly) {
                    // If the commander variable C of a group is true, then at
                    // least one of the variables Xi in the group must be true
                    // C -> X1 or X2 or ...
                    // not C or X1 or X2 or ...
                    // This clause is added only when 'exactly one' is generated
                    buffer.clear();
                    buffer.add(new NotNode(commanders.get(groupIndex)));
                    for (int index = 0; index < groupSize; index++) {
                        buffer.add(literals.get(offset + index));
                    }
                    clauses.add(NaryOrNode.createSimplestOr(buffer));
                }

                // If the commander variable C of a group is false, then none of
                // the variables Xi in the group can be true
                // not C -> not (X1 or X2 or ...)
                // not C -> not X1 and not X2 and ...
                // C or (not X1 and not X2 and ...)
                // (C or not X1) and (C or not X2) and ...
                for (int index = 0; index < groupSize; index++) {
                    clauses.add(new OrNode(commanders.get(groupIndex),
                                           notLiterals[index]));
                }
            }

            // Exactly one or at most one of the commander variables is true
            // Recurse with created commanders
            addRec(commanders, clauses, gsize, exactly, prefix, from + numGroups);
        }
    }
}