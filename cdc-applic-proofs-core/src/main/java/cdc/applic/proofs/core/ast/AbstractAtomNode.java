package cdc.applic.proofs.core.ast;

import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AtomNode;
import cdc.applic.expressions.ast.NodeKerning;

/**
 * Base class of atoms that are defined specifically for proofs.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractAtomNode extends AbstractLeafNode implements AtomNode {
    private final Property property;

    protected AbstractAtomNode(Property property) {
        this.property = property;
    }

    @Override
    public NodeKerning getKerning() {
        return NodeKerning.KERNING_ATOM;
    }

    /**
     * @return The property.
     */
    public final Property getProperty() {
        return property;
    }
}