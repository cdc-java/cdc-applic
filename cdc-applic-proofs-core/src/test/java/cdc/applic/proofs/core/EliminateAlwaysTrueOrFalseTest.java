package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.visitors.EliminateAlwaysTrueOrFalse;

class EliminateAlwaysTrueOrFalseTest {
    private static final Logger LOGGER = LogManager.getLogger(EliminateAlwaysTrueOrFalseTest.class);
    private final RepositorySupport support = new RepositorySupport();

    private void check(String expression,
                       ProverFeatures features,
                       String expected) {
        LOGGER.debug("check({}, {})", expression, features);
        final Expression x = new Expression(expression);
        final Node node = EliminateAlwaysTrueOrFalse.execute(x.getRootNode(),
                                                             support.registryHandle,
                                                             features,
                                                             -1)
                                                    .getValue();
        LOGGER.debug("   {}", node.compress());
        assertEquals(expected, node.compress());
    }

    @Test
    void testAllPossibleReserves() {
        final ProverFeatures features = ProverFeatures.EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES;
        check("B<:{true}", features, "B<:{true}");
        check("B<:{true,false}", features, "true");
        check("E<:{E1}", features, "E<:{E1}");
        check("E<:{E1,E2,E3}", features, "E<:{E1,E2,E3}");
        check("I<:{1~10}", features, "I<:{1~10}");
        check("I<:{1~999}", features, "I<:{1~999}");
        check("R<:{1.0~10.0}", features, "R<:{1.0~10.0}");
        check("R<:{1.0~999.0}", features, "R<:{1.0~999.0}");
    }

    @Test
    void testNoReserves() {
        final ProverFeatures features = ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES;
        check("B<:{true}", features, "B<:{true}");
        check("B<:{true,false}", features, "true");
        check("E<:{E1}", features, "E<:{E1}");
        check("E<:{E1,E2,E3}", features, "true");
        check("I<:{1~10}", features, "I<:{1~10}");
        check("I<:{1~999}", features, "true");
        check("R<:{1.0~10.0}", features, "R<:{1.0~10.0}");
        check("R<:{1.0~999.0}", features, "true");
    }
}