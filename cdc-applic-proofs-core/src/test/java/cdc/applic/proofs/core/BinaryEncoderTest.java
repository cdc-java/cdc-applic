package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.proofs.core.ast.AtomVarNode;
import cdc.applic.proofs.core.encoding.BinaryEncoder;

class BinaryEncoderTest {
    private static Node n(int id) {
        return new AtomVarNode("V" + id);
    }

    private static void checkAddAtMostOne(String expected,
                                          int... ids) {

        final List<Node> n = new ArrayList<>();
        for (final int id : ids) {
            n.add(n(id));
        }
        final List<Node> clauses = new ArrayList<>();
        BinaryEncoder.INSTANCE.addAtMostOne(n, clauses, "B");
        final Node result = clauses.isEmpty() ? TrueNode.INSTANCE : NaryAndNode.createSimplestAnd(clauses);
        assertEquals(expected, result.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testAddAtMostOne() {
        checkAddAtMostOne("true", 1);
        checkAddAtMostOne("(!V1|!B0)"
                + "&(!V2|B0)", 1, 2);
        checkAddAtMostOne("(!V1|!B0)"
                + "&(!V2|B0)&(!V2|!B1)"
                + "&(!V3|B0)&(!V3|B1)", 1, 2, 3);
        checkAddAtMostOne("(!V1|!B0)&(!V1|!B1)"
                + "&(!V2|!B0)&(!V2|B1)"
                + "&(!V3|B0)&(!V3|!B1)"
                + "&(!V4|B0)&(!V4|B1)", 1, 2, 3, 4);
        checkAddAtMostOne("(!V1|!B0)&(!V1|!B1)"
                + "&(!V2|!B0)&(!V2|B1)"
                + "&(!V3|B0)&(!V3|!B1)"
                + "&(!V4|B0)&(!V4|B1)&(!V4|!B2)"
                + "&(!V5|B0)&(!V5|B1)&(!V5|B2)", 1, 2, 3, 4, 5);
        checkAddAtMostOne("(!V1|!B0)&(!V1|!B1)"
                + "&(!V2|!B0)&(!V2|B1)"
                + "&(!V3|B0)&(!V3|!B1)&(!V3|!B2)"
                + "&(!V4|B0)&(!V4|!B1)&(!V4|B2)"
                + "&(!V5|B0)&(!V5|B1)&(!V5|!B2)"
                + "&(!V6|B0)&(!V6|B1)&(!V6|B2)", 1, 2, 3, 4, 5, 6);
        checkAddAtMostOne("(!V1|!B0)&(!V1|!B1)"
                + "&(!V2|!B0)&(!V2|B1)&(!V2|!B2)"
                + "&(!V3|!B0)&(!V3|B1)&(!V3|B2)"
                + "&(!V4|B0)&(!V4|!B1)&(!V4|!B2)"
                + "&(!V5|B0)&(!V5|!B1)&(!V5|B2)"
                + "&(!V6|B0)&(!V6|B1)&(!V6|!B2)"
                + "&(!V7|B0)&(!V7|B1)&(!V7|B2)", 1, 2, 3, 4, 5, 6, 7);
        checkAddAtMostOne("(!V1|!B0)&(!V1|!B1)&(!V1|!B2)"
                + "&(!V2|!B0)&(!V2|!B1)&(!V2|B2)"
                + "&(!V3|!B0)&(!V3|B1)&(!V3|!B2)"
                + "&(!V4|!B0)&(!V4|B1)&(!V4|B2)"
                + "&(!V5|B0)&(!V5|!B1)&(!V5|!B2)"
                + "&(!V6|B0)&(!V6|!B1)&(!V6|B2)"
                + "&(!V7|B0)&(!V7|B1)&(!V7|!B2)"
                + "&(!V8|B0)&(!V8|B1)&(!V8|B2)", 1, 2, 3, 4, 5, 6, 7, 8);
        checkAddAtMostOne("(!V1|!B0)&(!V1|!B1)&(!V1|!B2)"
                + "&(!V2|!B0)&(!V2|!B1)&(!V2|B2)"
                + "&(!V3|!B0)&(!V3|B1)&(!V3|!B2)"
                + "&(!V4|!B0)&(!V4|B1)&(!V4|B2)"
                + "&(!V5|B0)&(!V5|!B1)&(!V5|!B2)"
                + "&(!V6|B0)&(!V6|!B1)&(!V6|B2)"
                + "&(!V7|B0)&(!V7|B1)&(!V7|!B2)"
                + "&(!V8|B0)&(!V8|B1)&(!V8|B2)&(!V8|!B3)"
                + "&(!V9|B0)&(!V9|B1)&(!V9|B2)&(!V9|B3)", 1, 2, 3, 4, 5, 6, 7, 8, 9);
    }
}