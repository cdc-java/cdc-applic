package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.clauses.ProverImpl;

class ProverImplNamingConventionsTest {
    private static Expression x(String s) {
        return new Expression(s);
    }

    @Test
    void test1() {
        final String conv1 = "Convention1";
        final String conv2 = "Convention2";
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry().name("r1").build();
        final DictionaryHandle h1 = new DictionaryHandle(r1);
        r1.namingConvention().name(conv1).build();
        r1.namingConvention().name(conv2).build();

        r1.booleanType().name("BooleanType").build();
        r1.integerType().name("IntegerType").domain("1~999").build();
        r1.realType().name("RealType").domain("0.0~1000.0").build();
        r1.enumeratedType()
          .name("EnumType")
          .frozen(true)
          .value()
          .literal("A")
          .synonym(conv1, "a")
          .back()
          .value()
          .literal("B")
          .synonym(conv1, "b")
          .back()
          .build();

        r1.property().name("B1").synonym(conv1, "b1").type("BooleanType").build();
        r1.property().name("I1").synonym(conv1, "i1").type("IntegerType").build();
        r1.property().name("R1").synonym(conv1, "r1").type("RealType").build();
        r1.property().name("E1").synonym(conv1, "e1").type("EnumType").build();
        r1.alias().name("A").synonym(conv1, "a").expression("B1").build();

        final Prover prover = new ProverImpl(h1, ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES);

        assertTrue(prover.isSometimesTrue(x("true")));
        assertTrue(prover.isSometimesTrue(x("B1")));
        assertTrue(prover.isSometimesTrue(x("b1")));
        assertTrue(prover.areAlwaysEquivalent(x("B1"), x("b1")));
        assertTrue(prover.isSometimesTrue(x("E1=A")));
        assertTrue(prover.isSometimesTrue(x("E1=a")));
        assertTrue(prover.isSometimesTrue(x("e1=A")));
        assertTrue(prover.isSometimesTrue(x("e1=a")));
        assertTrue(prover.areAlwaysEquivalent(x("E1=A"), x("e1=a")));
        assertFalse(prover.isSometimesTrue(x("E1<:{}")));
        assertFalse(prover.isSometimesTrue(x("e1<:{}")));
        assertFalse(prover.isSometimesTrue(x("E1!<:{A, B}")));
        assertFalse(prover.isSometimesTrue(x("e1!<:{a, b}")));
        assertTrue(prover.areAlwaysEquivalent(x("e1=a"), x("E1<:{A}")));
    }
}