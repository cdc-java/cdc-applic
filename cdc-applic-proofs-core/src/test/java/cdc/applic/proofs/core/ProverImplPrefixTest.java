package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositoryPrefixSupport;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.clauses.ProverImpl;

class ProverImplPrefixTest {
    private static final Logger LOGGER = LogManager.getLogger(ProverImplPrefixTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    private static Expression x(String s) {
        return new Expression(s);
    }

    @Test
    void testAssertionsAllPossibleReserves() {
        final RepositoryPrefixSupport support = new RepositoryPrefixSupport();
        support.repository.print(OUT);
        support.registryGlobalHandle.print(OUT);
        support.registryAllHandle.print(OUT);

        final Prover prover = new ProverImpl(support.registryAllHandle, ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES);
        support.registryGlobalHandle.print(OUT);
        support.registryAllHandle.print(OUT);
        assertTrue(prover.isSometimesTrue(x("Family = F1")));
        assertFalse(prover.isSometimesTrue(x("Family = F3")));
    }

    @Test
    void testAliasPrefix() {
        final RepositoryPrefixSupport support = new RepositoryPrefixSupport();
        support.registryP1.createAssertion("Rank10");
        support.registryP2.createAssertion("Rank10");
        support.repository.print(OUT);

        final Prover prover = new ProverImpl(support.registryAllHandle, ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES);
        support.registryAllHandle.print(OUT);
        assertTrue(prover.isSometimesTrue(x("P1.Rank10")));
        support.registryAllHandle.print(OUT);
    }
}