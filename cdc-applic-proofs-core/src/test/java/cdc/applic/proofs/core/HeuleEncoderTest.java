package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.proofs.core.ast.AtomVarNode;
import cdc.applic.proofs.core.encoding.HeuleEncoder;

class HeuleEncoderTest {
    private static final Logger LOGGER = LogManager.getLogger(HeuleEncoderTest.class);
    private static final String PREFIX = "X";

    private static List<Node> build(Object... literals) {
        final List<Node> result = new ArrayList<>();
        for (final Object literal : literals) {
            if (literal instanceof Integer) {
                final int value = ((Integer) literal).intValue();
                if (value < 0) {
                    result.add(new NotNode(new AtomVarNode("V" + (-value), false)));
                } else {
                    result.add(new AtomVarNode("V" + value, false));
                }
            } else {
                result.add((Node) literal);
            }
        }
        return result;
    }

    private static void checkAtMostOne(String expected,
                                       Object... literals) {
        final List<Node> tmp = build(literals);
        final Node node = HeuleEncoder.INSTANCE.atMostOne(tmp, PREFIX);
        LOGGER.debug(node.toInfix(Formatting.SHORT_NARROW));
        assertEquals(expected, node.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testAtMostOne() {
        checkAtMostOne("true", FalseNode.INSTANCE);
        checkAtMostOne("true", TrueNode.INSTANCE);
        checkAtMostOne("true", 1);
        checkAtMostOne("true", -1);
        checkAtMostOne("!V1|!V2", 1, 2);
        checkAtMostOne("V1|V2", -1, -2);
        checkAtMostOne("(!V1|!V2)&(!V1|!V3)&(!V2|!V3)", 1, 2, 3);
        checkAtMostOne("(!V1|!V2)&(!V1|!V3)&(!V1|!V4)&(!V2|!V3)&(!V2|!V4)&(!V3|!V4)", 1, 2, 3, 4);
        checkAtMostOne("(!V1|!V2)&(!V1|!V3)&(!V1|!X1)&(!V2|!V3)&(!V2|!X1)&(!V3|!X1)"
                + "&(X1|!V4)&(X1|!V5)&(!V4|!V5)", 1, 2, 3, 4, 5);
        checkAtMostOne("(!V1|!V2)&(!V1|!V3)&(!V1|!X1)&(!V2|!V3)&(!V2|!X1)&(!V3|!X1)"
                + "&(X1|!V4)&(X1|!V5)&(X1|!V6)&(!V4|!V5)&(!V4|!V6)&(!V5|!V6)", 1, 2, 3, 4, 5, 6);
        checkAtMostOne("(!V1|!V2)&(!V1|!V3)&(!V1|!X1)&(!V2|!V3)&(!V2|!X1)&(!V3|!X1)"
                + "&(X1|!V4)&(X1|!V5)&(X1|!X2)&(!V4|!V5)&(!V4|!X2)&(!V5|!X2)"
                + "&(X2|!V6)&(X2|!V7)&(!V6|!V7)", 1, 2, 3, 4, 5, 6, 7);
    }
}