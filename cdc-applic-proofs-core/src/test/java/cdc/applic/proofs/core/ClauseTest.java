package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.AtomNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.proofs.core.clauses.Clause;
import cdc.applic.proofs.core.clauses.Literal;

class ClauseTest {
    private static AtomNode ref(int value) {
        return new RefNode("Ref#" + Math.abs(value));
    }

    private static final Literal FP = new Literal(FalseNode.INSTANCE, true);
    private static final Literal FN = new Literal(FalseNode.INSTANCE, false);
    private static final Literal TP = new Literal(TrueNode.INSTANCE, true);
    private static final Literal TN = new Literal(TrueNode.INSTANCE, false);

    private static Literal literal(int value) {
        return new Literal(ref(value), value > 0);
    }

    private static Clause clause(Object... values) {
        final List<Literal> literals = new ArrayList<>();
        for (final Object value : values) {
            if (value instanceof Literal) {
                literals.add((Literal) value);
            } else {
                literals.add(literal((Integer) value));
            }
        }
        return new Clause(literals);
    }

    private static void checkAssertTrueAtom(Clause input,
                                            AtomNode atom,
                                            Clause expected) {
        final Clause output = input.assertTrueAtom(atom);
        assertEquals(expected, output);
    }

    @Test
    void testAssertTrueAtom() {
        checkAssertTrueAtom(clause(1, 2, 3), ref(1), Clause.TRUE);
        checkAssertTrueAtom(clause(1, 2, 3), ref(4), clause(1, 2, 3));
        checkAssertTrueAtom(clause(-1, 2, 3), ref(1), clause(2, 3));
    }

    private static void checkAssertFalseAtom(Clause input,
                                             AtomNode atom,
                                             Clause expected) {
        final Clause output = input.assertFalseAtom(atom);
        assertEquals(expected, output);
    }

    @Test
    void testAssertFalseAtom() {
        checkAssertFalseAtom(clause(1, 2, 3), ref(1), clause(2, 3));
        checkAssertFalseAtom(clause(1, 2, 3), ref(4), clause(1, 2, 3));
        checkAssertFalseAtom(clause(-1, 2, 3), ref(1), Clause.TRUE);
    }

    private static void checkAssertTrueLiteral(Clause input,
                                               Literal literal,
                                               Clause expected) {
        final Clause output = input.assertLiteral(literal);
        assertEquals(expected, output);
    }

    @Test
    void testAssertTrueLiteral() {
        checkAssertTrueLiteral(clause(1, 2, 3), literal(1), Clause.TRUE);
        checkAssertTrueLiteral(clause(1, 2, 3), literal(-1), clause(2, 3));
    }

    @Test
    void testIsContradiction() {
        assertFalse(clause(1).isContradiction());
        assertFalse(clause(-1).isContradiction());
        assertTrue(clause().isContradiction());
    }

    @Test
    void testIsUnitClause() {
        assertTrue(clause(1).isUnitClause());
        assertTrue(clause(-1).isUnitClause());
        assertFalse(clause().isUnitClause());
        assertFalse(clause(1, 2).isUnitClause());

        assertEquals(literal(1), clause(1).getUnitLiteral());

        assertThrows(IllegalStateException.class,
                     () -> {
                         clause(1, 2).getUnitLiteral();
                     });
    }

    @Test
    void testIsDefiniteClause() {
        assertTrue(clause(1).isDefiniteClause());
        assertFalse(clause(1, 2).isDefiniteClause());
        assertFalse(clause().isDefiniteClause());
        assertTrue(clause(-1, 1).isDefiniteClause());
    }

    @Test
    void testIsImplicationDefiniteClause() {
        assertFalse(clause(1).isImplicationDefiniteClause());
        assertFalse(clause(1, 2).isImplicationDefiniteClause());
        assertFalse(clause().isImplicationDefiniteClause());
        assertTrue(clause(-1, 1).isImplicationDefiniteClause());
        assertTrue(clause(-2, -1, 1).isImplicationDefiniteClause());
        assertFalse(clause(-2, -1, 1, 2).isImplicationDefiniteClause());
    }

    @Test
    void testIsHornClause() {
        assertFalse(clause().isHornClause());
        assertTrue(clause(1).isHornClause());
        assertFalse(clause(1, 2).isHornClause());
        assertTrue(clause(-1, 1).isHornClause());
        assertTrue(clause(-1).isHornClause());
        assertTrue(clause(-2, -1, 1).isHornClause());
        assertTrue(clause(-2, -1).isHornClause());
        assertFalse(clause(-2, -1, 1, 2).isHornClause());
    }

    @Test
    void testIsGoalClause() {
        assertFalse(clause().isGoalClause());
        assertFalse(clause(1).isGoalClause());
        assertFalse(clause(1, 2).isGoalClause());
        assertFalse(clause(-1, 1).isGoalClause());
        assertTrue(clause(-1).isGoalClause());
        assertFalse(clause(-2, -1, 1).isGoalClause());
        assertTrue(clause(-2, -1).isGoalClause());
        assertFalse(clause(-2, -1, 1, 2).isGoalClause());
    }

    @Test
    void testIsTautology() {
        assertFalse(clause().isTautology());
        assertTrue(clause(-1, 1).isTautology());
        assertFalse(clause(TN).isTautology());
        assertFalse(clause(FP).isTautology());
        assertTrue(clause(TP).isTautology());
        assertTrue(clause(TP, TP).isTautology());
        assertTrue(clause(FN).isTautology());
        assertTrue(clause(FN, FN).isTautology());
    }

    @Test
    void testGetNumber() {
        assertEquals(0, clause().getNumberOfLiterals());
        assertEquals(0, clause().getNumberOfPositiveLiterals());
        assertEquals(0, clause().getNumberOfNegativeLiterals());

        assertEquals(1, clause(1).getNumberOfLiterals());
        assertEquals(1, clause(1).getNumberOfPositiveLiterals());
        assertEquals(0, clause(1).getNumberOfNegativeLiterals());

        assertEquals(1, clause(1, 1).getNumberOfLiterals());
        assertEquals(1, clause(1, 1).getNumberOfPositiveLiterals());
        assertEquals(0, clause(1, 1).getNumberOfNegativeLiterals());

        assertEquals(2, clause(-1, 1).getNumberOfLiterals());
        assertEquals(1, clause(-1, 1).getNumberOfPositiveLiterals());
        assertEquals(1, clause(-1, 1).getNumberOfNegativeLiterals());
        assertEquals(1, clause(1, -1).getNumberOfNegativeLiterals());
    }

    @Test
    void testGetOpposite() {
        assertEquals(clause(TP), clause().getOpposite());
        assertEquals(clause(-1), clause(1).getOpposite());
    }

    @Test
    void testEquals() {
        assertEquals(clause(), clause());
        assertNotEquals(clause(1), clause());
        assertNotEquals(clause(1), null);
        assertNotEquals(clause(1), "Hello");
    }

    @Test
    void testHashCode() {
        assertEquals(Clause.TRUE.hashCode(), Clause.TRUE.hashCode());
        assertNotEquals(Clause.FALSE.hashCode(), Clause.TRUE.hashCode());
    }

    @Test
    void testToString() {
        assertEquals("TRUE", Clause.TRUE.toString());
        assertEquals("FALSE", Clause.FALSE.toString());
        assertEquals("REF(Ref#1)", clause(1).toString());
        assertTrue("(REF(Ref#1) OR REF(Ref#2))".equals(clause(1, 2).toString())
                || "(REF(Ref#2) OR REF(Ref#1))".equals(clause(1, 2).toString()));
    }
}