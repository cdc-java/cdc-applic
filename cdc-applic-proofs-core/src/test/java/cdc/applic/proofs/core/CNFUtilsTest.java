package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.AbstractOperatorNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.proofs.core.ast.AtomVarNode;
import cdc.applic.proofs.core.utils.CNFUtils;

class CNFUtilsTest {
    private static void checkAddEquivalence(String beta,
                                            String expected) {
        final AtomVarNode a = new AtomVarNode("A");
        final Expression b = new Expression(beta);
        final List<Node> nodes = new ArrayList<>();
        CNFUtils.addEquivalence(a,
                                (AbstractOperatorNode) ConvertToNary.execute(b.getRootNode(),
                                                                             ConvertToNary.Variant.WHEN_NECESSARY),
                                nodes);
        final Expression r = NaryAndNode.createSimplestAnd(nodes).toExpression(Formatting.SHORT_NARROW);
        assertEquals(expected, r.toString());
    }

    @Test
    void testAddEquivalence() {
        checkAddEquivalence("B1 and B2", "(A|!B1|!B2)&(!A|B1)&(!A|B2)");
        checkAddEquivalence("B1 and B2 and B3", "(A|!B1|!B2|!B3)&(!A|B1)&(!A|B2)&(!A|B3)");
        checkAddEquivalence("B1 or B2", "(!A|B1|B2)&(A|!B1)&(A|!B2)");
        checkAddEquivalence("B1 or B2 or B3", "(!A|B1|B2|B3)&(A|!B1)&(A|!B2)&(A|!B3)");
        checkAddEquivalence("B1 imp B2", "(!A|!B1|B2)&(A|B1)&(A|!B2)");
        checkAddEquivalence("B1 iff B2", "(!A|!B1|B2)&(!A|B1|!B2)&(A|B1|B2)&(A|!B1|!B2)");
        checkAddEquivalence("not B1", "(!A|!B1)&(A|B1)");
    }

    private static void checkAddTrueEquivalence(String beta,
                                                String expected) {
        final Expression b = new Expression(beta);
        final List<Node> nodes = new ArrayList<>();
        CNFUtils.addTrueEquivalence((AbstractOperatorNode) ConvertToNary.execute(b.getRootNode(),
                                                                                 ConvertToNary.Variant.WHEN_NECESSARY),
                                    nodes);
        final Expression r = NaryAndNode.createSimplestAnd(nodes).toExpression(Formatting.SHORT_NARROW);
        assertEquals(expected, r.toString());
    }

    @Test
    void testAddTrueEquivalence() {
        checkAddTrueEquivalence("B1 and B2", "B1&B2");
        checkAddTrueEquivalence("B1 and B2 and B3", "B1&B2&B3");
        checkAddTrueEquivalence("B1 or B2", "B1|B2");
        checkAddTrueEquivalence("B1 imp B2", "!B1|B2");
        checkAddTrueEquivalence("B1 iff B2", "(!B1|B2)&(B1|!B2)");
        checkAddTrueEquivalence("not B1", "!B1");
        checkAddTrueEquivalence("B1 or B2 or B3", "B1|B2|B3");
    }
}