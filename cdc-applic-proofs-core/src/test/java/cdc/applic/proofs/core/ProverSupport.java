package cdc.applic.proofs.core;

import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.ProverMatching;

interface ProverSupport {
    public static final ProverFeatures IN_NO = ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES;
    public static final ProverFeatures EX_NO = ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES;
    public static final ProverFeatures IN_ALL = ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES;
    public static final ProverFeatures EX_ALL = ProverFeatures.EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES;

    public static final ProverMatching N = ProverMatching.NEVER;
    public static final ProverMatching S = ProverMatching.SOMETIMES;
    public static final ProverMatching A = ProverMatching.ALWAYS;
}