package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.EliminateTrueFalse;
import cdc.applic.proofs.core.ast.AtomVarNode;
import cdc.applic.proofs.core.encoding.CommanderEncoder;
import cdc.applic.proofs.core.encoding.Encoder;

class CommanderEncoderTest {
    private static final Logger LOGGER = LogManager.getLogger(CommanderEncoderTest.class);
    private static final String CMD = "C";

    private static List<Node> build(Object... literals) {
        final List<Node> result = new ArrayList<>();
        for (final Object literal : literals) {
            if (literal instanceof Integer) {
                final int value = ((Integer) literal).intValue();
                if (value < 0) {
                    result.add(new NotNode(new AtomVarNode("V" + (-value), false)));
                } else {
                    result.add(new AtomVarNode("V" + value, false));
                }
            } else {
                result.add((Node) literal);
            }
        }
        return result;
    }

    private static void checkAtLeastOne(String expected,
                                        Object... literals) {
        final List<Node> tmp = build(literals);
        final Node node = Encoder.atLeastOne(tmp);
        assertEquals(expected, node.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testAtLeastOne() {
        checkAtLeastOne("V1", 1);
        checkAtLeastOne("!V1", -1);
        checkAtLeastOne("V1|V2", 1, 2);
        checkAtLeastOne("V1|V2|V3", 1, 2, 3);
    }

    private static void checkAtMostOne(String expected,
                                       Object... literals) {
        final List<Node> tmp = build(literals);
        final Node node = CommanderEncoder.INSTANCE.atMostOne(tmp, CMD);
        assertEquals(expected, node.toInfix(Formatting.SHORT_NARROW));
    }

    private static void atMostOne(Object... nodes) {
        LOGGER.debug("nodes: " + Arrays.toString(nodes));
        final Node node = CommanderEncoder.INSTANCE.atMostOne(build(nodes), CMD);
        LOGGER.debug("at most one: " + node.toInfix(Formatting.SHORT_NARROW));
        final Node node1 = EliminateTrueFalse.execute(node);
        LOGGER.debug("at most one simplified: " + node1.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testAtMostOne() {
        atMostOne(new RefNode("A1"), new RefNode("A2"), new RefNode("A3"));
        atMostOne(FalseNode.INSTANCE, FalseNode.INSTANCE, FalseNode.INSTANCE);
        atMostOne(FalseNode.INSTANCE, FalseNode.INSTANCE, TrueNode.INSTANCE);
        atMostOne(FalseNode.INSTANCE, TrueNode.INSTANCE, TrueNode.INSTANCE);
        atMostOne(TrueNode.INSTANCE, TrueNode.INSTANCE, TrueNode.INSTANCE);

        checkAtMostOne("true", 1);
        checkAtMostOne("true", -1);
        checkAtMostOne("!V1|!V2", 1, 2);
        checkAtMostOne("V1|V2", -1, -2);
        checkAtMostOne("(!V1|!V2)&(!V1|!V3)&(!V2|!V3)", 1, 2, 3);
        checkAtMostOne("(!V1|!V2)&(!V1|!V3)&(!V2|!V3)&(C1|!V1)&(C1|!V2)&(C1|!V3)&(C2|!V4)&(!C1|!C2)",
                       1,
                       2,
                       3,
                       4);
    }

    private static void checkExactlyOne(String expected,
                                        Object... literals) {
        final List<Node> tmp = build(literals);
        final Node node = CommanderEncoder.INSTANCE.exactlyOne(tmp, CMD);
        assertEquals(expected, node.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testExactlyOne() {
        checkExactlyOne("V1", 1);
        checkExactlyOne("(V1|V2)&(!V1|!V2)", 1, 2);
        checkExactlyOne("(V1|V2|V3)&(!V1|!V2)&(!V1|!V3)&(!V2|!V3)", 1, 2, 3);
        checkExactlyOne("(!V1|!V2)&(!V1|!V3)&(!V2|!V3)&(!C1|V1|V2|V3)&(C1|!V1)&(C1|!V2)&(C1|!V3)&(!C2|V4)&(C2|!V4)&(C1|C2)&(!C1|!C2)",
                        1,
                        2,
                        3,
                        4);
    }
}