package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.proofs.core.sat.SatSolver;
import cdc.applic.proofs.core.sat.SatSolverFactory;

class SatSolverTest {
    private static final Logger LOGGER = LogManager.getLogger(SatSolverTest.class);

    private static int[] c(int... literals) {
        return literals;
    }

    private static String toString(int[][] sentence) {
        final StringBuilder builder = new StringBuilder();
        builder.append('[');
        for (int index = 0; index < sentence.length; index++) {
            builder.append(Arrays.toString(sentence[index]));
        }
        builder.append(']');
        return builder.toString();
    }

    private static void check(boolean expected,
                              int[]... sentence) {
        final SatSolverFactory factory = new SatSolverFactory();
        final SatSolver solver = factory.newInstance();
        final boolean found = solver.isSatisfiable(sentence);
        if (expected != found) {
            LOGGER.info(toString(sentence) + " FAILED");
            assertEquals(expected, found);
        }
    }

    @Test
    void test() {
        check(true);
        check(false, c());
        check(true, c(1));
        check(true, c(1, -1));
        check(false, c(1), c(-1));
    }
}