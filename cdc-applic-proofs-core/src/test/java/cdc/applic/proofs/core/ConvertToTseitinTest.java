package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.proofs.core.visitors.ConvertToTseitin;

class ConvertToTseitinTest {
    private static void check(String expression,
                              ConvertToTseitin.Variant variant,
                              String expected) {
        final Expression xSep = new Expression(expression);
        final Node nodeSep = ConvertToTseitin.execute(ConvertToNary.execute(xSep.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY),
                                                      variant,
                                                      "#",
                                                      "#");
        assertEquals(expected, nodeSep.toInfix(Formatting.SHORT_NARROW));

        final Node nodeNoSep = ConvertToTseitin.execute(ConvertToNary.execute(xSep.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY),
                                                        variant,
                                                        null,
                                                        null);
        assertEquals(expected.replaceAll("#", "\""), nodeNoSep.toInfix(Formatting.SHORT_NARROW));
    }

    private static void checkLeaveOperatorsUnchanged(String expression,
                                                     String expected) {
        check(expression, ConvertToTseitin.Variant.LEAVE_OPERATORS_UNCHANGED, expected);
    }

    private static void checkPrepareCNF(String expression,
                                        String expected) {
        check(expression, ConvertToTseitin.Variant.PREPARE_CNF, expected);
    }

    @Test
    void testLeaveOperatorsUnchanged() {
        // FIXME we could avoid useless transformations that introduce useless variables
        checkLeaveOperatorsUnchanged("true", "true");
        checkLeaveOperatorsUnchanged("false", "false");
        checkLeaveOperatorsUnchanged("A", "A");
        checkLeaveOperatorsUnchanged("!A", "!A");
        checkLeaveOperatorsUnchanged("!!A", "(#1#<->!A)&!#1#");
        checkLeaveOperatorsUnchanged("A or B", "A|B");
        checkLeaveOperatorsUnchanged("A or B or C", "A|B|C");
        checkLeaveOperatorsUnchanged("A and B", "A&B");
        checkLeaveOperatorsUnchanged("A and B and C", "A&B&C");

        checkLeaveOperatorsUnchanged("!A or B", "(#1#<->!A)&(#1#|B)");
        checkLeaveOperatorsUnchanged("!A and B", "(#1#<->!A)&#1#&B");

        checkLeaveOperatorsUnchanged("!(A or B)", "(#1#<->A|B)&!#1#");
        checkLeaveOperatorsUnchanged("!(A or B or C)", "(#1#<->A|B|C)&!#1#");
    }

    @Test
    void testPrepareCNF() {
        checkPrepareCNF("true", "true");
        checkPrepareCNF("false", "false");
        checkPrepareCNF("A", "A");
        checkPrepareCNF("!A", "!A");
        checkPrepareCNF("A or B", "A|B");
        checkPrepareCNF("A and B", "A&B");
        checkPrepareCNF("A or B or C", "A|B|C");
        checkPrepareCNF("A and B and C", "A&B&C");
        checkPrepareCNF("!A or B", "(!#1#|!A)&(#1#|A)" + "&(#1#|B)");
        checkPrepareCNF("!A or !B", "(!#1#|!A)&(#1#|A)" + "&(!#2#|!B)&(#2#|B)" + "&(#1#|#2#)");
        checkPrepareCNF("!(A or B)", "(!#1#|A|B)&(#1#|!A)&(#1#|!B)" + "&!#1#");
        checkPrepareCNF("!(A and B)", "(#1#|!A|!B)&(!#1#|A)&(!#1#|B)" + "&!#1#");
        checkPrepareCNF("!(A or B or C)", "(!#1#|A|B|C)&(#1#|!A)&(#1#|!B)&(#1#|!C)" + "&!#1#");
        checkPrepareCNF("!!(A or B or C)", "(!#1#|A|B|C)&(#1#|!A)&(#1#|!B)&(#1#|!C)" + "&(!#2#|!#1#)&(#2#|#1#)" + "&!#2#");

        checkPrepareCNF("(!E1 or !E2) and (!X1 or E1 or E2) and (X1 or !E1) and (X1 or !E2) and (!X2 or E3) and (X2 or !E3) and (X1 or X2) and (!X1 or !X2)",
                        "(!#1#|!E1)&(#1#|E1)" // !E1
                                + "&(!#2#|!E2)&(#2#|E2)" // !E2
                                + "&(!#3#|#1#|#2#)&(#3#|!#1#)&(#3#|!#2#)" // #3 !E1 or !E2
                                + "&(!#4#|!X1)&(#4#|X1)" // !X1
                                + "&(!#5#|#4#|E1|E2)&(#5#|!#4#)&(#5#|!E1)&(#5#|!E2)" // #5 !X1 or E1 or E2
                                + "&(!#6#|!E1)&(#6#|E1)" // !E1
                                + "&(!#7#|X1|#6#)&(#7#|!X1)&(#7#|!#6#)" // #7 X1 or !E1
                                + "&(!#8#|!E2)&(#8#|E2)" // !E2
                                + "&(!#9#|X1|#8#)&(#9#|!X1)&(#9#|!#8#)" // #9 X1 or !E2
                                + "&(!#10#|!X2)&(#10#|X2)" // !X2
                                + "&(!#11#|#10#|E3)&(#11#|!#10#)&(#11#|!E3)" // #11 !X2 or E3
                                + "&(!#12#|!E3)&(#12#|E3)" // !E3
                                + "&(!#13#|X2|#12#)&(#13#|!X2)&(#13#|!#12#)" // #13 X2 or !E3
                                + "&(!#14#|X1|X2)&(#14#|!X1)&(#14#|!X2)" // #14 X1 or X2
                                + "&(!#15#|!X1)&(#15#|X1)" // !X1
                                + "&(!#16#|!X2)&(#16#|X2)" // !X2
                                + "&(!#17#|#15#|#16#)&(#17#|!#15#)&(#17#|!#16#)" // #17 !X1 or !X2
                                + "&#3#&#5#&#7#&#9#&#11#&#13#&#14#&#17#");
        checkPrepareCNF("!((!E1 or !E2) and (!X1 or E1 or E2) and (X1 or !E1) and (X1 or !E2) and (!X2 or E3) and (X2 or !E3) and (X1 or X2) and (!X1 or !X2))",
                        "(!#1#|!E1)&(#1#|E1)" // !E1
                                + "&(!#2#|!E2)&(#2#|E2)" // !E2
                                + "&(!#3#|#1#|#2#)&(#3#|!#1#)&(#3#|!#2#)" // #3 !E1 or !E2
                                + "&(!#4#|!X1)&(#4#|X1)" // !X1
                                + "&(!#5#|#4#|E1|E2)&(#5#|!#4#)&(#5#|!E1)&(#5#|!E2)" // #5 !X1 or E1 or E2
                                + "&(!#6#|!E1)&(#6#|E1)" // !E1
                                + "&(!#7#|X1|#6#)&(#7#|!X1)&(#7#|!#6#)" // #7 X1 or !E1
                                + "&(!#8#|!E2)&(#8#|E2)" // !E2
                                + "&(!#9#|X1|#8#)&(#9#|!X1)&(#9#|!#8#)" // #9 X1 or !E2
                                + "&(!#10#|!X2)&(#10#|X2)" // !X2
                                + "&(!#11#|#10#|E3)&(#11#|!#10#)&(#11#|!E3)" // #11 !X2 or E3
                                + "&(!#12#|!E3)&(#12#|E3)" // !E3
                                + "&(!#13#|X2|#12#)&(#13#|!X2)&(#13#|!#12#)" // #13 X2 or !E3
                                + "&(!#14#|X1|X2)&(#14#|!X1)&(#14#|!X2)" // #14 X1 or X2
                                + "&(!#15#|!X1)&(#15#|X1)" // !X1
                                + "&(!#16#|!X2)&(#16#|X2)" // !X2
                                + "&(!#17#|#15#|#16#)&(#17#|!#15#)&(#17#|!#16#)" // #17 !X1 or !X2
                                + "&(#18#|!#3#|!#5#|!#7#|!#9#|!#11#|!#13#|!#14#|!#17#)&(!#18#|#3#)&(!#18#|#5#)&(!#18#|#7#)&(!#18#|#9#)&(!#18#|#11#)&(!#18#|#13#)&(!#18#|#14#)&(!#18#|#17#)"
                                + "&!#18#");
    }
}