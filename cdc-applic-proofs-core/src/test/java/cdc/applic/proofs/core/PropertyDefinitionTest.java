package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.PrintStream;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.literals.Name;
import cdc.applic.expressions.parsing.SItemsParsing;
import cdc.applic.proofs.core.utils.PropertyDefinition;

class PropertyDefinitionTest {
    private static final Logger LOGGER = LogManager.getLogger(PropertyDefinitionTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();
    private final RepositorySupport support = new RepositorySupport();

    private void checkBuildPropertyNodeCNF(String propertyName,
                                           ReserveStrategy reserveStrategy) {
        LOGGER.debug(propertyName + " " + reserveStrategy);
        final Property property = support.registry.getProperty(Name.of(propertyName));
        final Node node = PropertyDefinition.buildPropertyNodeCNF(property, reserveStrategy);
        node.print(OUT, 1);
    }

    @Test
    void testBuildPropertyNodeCNF() {
        checkBuildPropertyNodeCNF("B", ReserveStrategy.ALL_POSSIBLE_RESERVES);
        checkBuildPropertyNodeCNF("B", ReserveStrategy.USER_DEFINED_RESERVES);
        checkBuildPropertyNodeCNF("B", ReserveStrategy.NO_RESERVES);

        checkBuildPropertyNodeCNF("I", ReserveStrategy.ALL_POSSIBLE_RESERVES);
        checkBuildPropertyNodeCNF("I", ReserveStrategy.USER_DEFINED_RESERVES);
        checkBuildPropertyNodeCNF("I", ReserveStrategy.NO_RESERVES);

        checkBuildPropertyNodeCNF("R", ReserveStrategy.ALL_POSSIBLE_RESERVES);
        checkBuildPropertyNodeCNF("R", ReserveStrategy.USER_DEFINED_RESERVES);
        checkBuildPropertyNodeCNF("R", ReserveStrategy.NO_RESERVES);

        checkBuildPropertyNodeCNF("E", ReserveStrategy.ALL_POSSIBLE_RESERVES);
        checkBuildPropertyNodeCNF("E", ReserveStrategy.USER_DEFINED_RESERVES);
        checkBuildPropertyNodeCNF("E", ReserveStrategy.NO_RESERVES);

        checkBuildPropertyNodeCNF("P", ReserveStrategy.ALL_POSSIBLE_RESERVES);
        checkBuildPropertyNodeCNF("P", ReserveStrategy.USER_DEFINED_RESERVES);
        checkBuildPropertyNodeCNF("P", ReserveStrategy.NO_RESERVES);

        // Create empty types (for coverage)
        support.registry.enumeratedType().name("WE").frozen(true).build();
        support.registry.integerType().name("WI").frozen(true).domain("").build();
        support.registry.realType().name("WR").frozen(true).domain("").build();

        support.registry.property().name("WE").type("WE").ordinal(0).build();
        support.registry.property().name("WI").type("WI").ordinal(0).build();
        support.registry.property().name("WR").type("WR").ordinal(0).build();

        checkBuildPropertyNodeCNF("WE", ReserveStrategy.ALL_POSSIBLE_RESERVES);
        checkBuildPropertyNodeCNF("WE", ReserveStrategy.USER_DEFINED_RESERVES);
        checkBuildPropertyNodeCNF("WE", ReserveStrategy.NO_RESERVES);

        checkBuildPropertyNodeCNF("WI", ReserveStrategy.ALL_POSSIBLE_RESERVES);
        checkBuildPropertyNodeCNF("WI", ReserveStrategy.USER_DEFINED_RESERVES);
        checkBuildPropertyNodeCNF("WI", ReserveStrategy.NO_RESERVES);

        checkBuildPropertyNodeCNF("WR", ReserveStrategy.ALL_POSSIBLE_RESERVES);
        checkBuildPropertyNodeCNF("WR", ReserveStrategy.USER_DEFINED_RESERVES);
        checkBuildPropertyNodeCNF("WR", ReserveStrategy.NO_RESERVES);
    }

    private void checkBuildPatternPropertyNodeCNF(String propertyName,
                                                  String content,
                                                  boolean addReserve) {
        LOGGER.debug(propertyName + " " + addReserve);
        final Property property = support.registry.getProperty(Name.of(propertyName));
        final List<SItem> sitems = SItemsParsing.toSItems(content);

        final Node node = PropertyDefinition.buildPatternPropertyNodeCNF(property, sitems, addReserve);
        node.print(OUT, 1);
    }

    @Test
    void testBuildPatternPropertyNodeCNF() {
        checkBuildPatternPropertyNodeCNF("P", "", false);
        checkBuildPatternPropertyNodeCNF("P", "", true);
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checkBuildPatternPropertyNodeCNF("P", "10", true);
                     });
    }
}