package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.clauses.SatSystem;

class SatSystemTest {
    private static final Logger LOGGER = LogManager.getLogger(SatSystemTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    @Test
    void test() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        registry.booleanType().name("B").build();
        registry.enumeratedType().name("E").frozen(false).literals("E1", "E2").build();
        registry.integerType().name("I").frozen(false).domain("1~99").build();
        registry.realType().name("R").frozen(false).domain("1.0~99.0").build();
        registry.patternType().name("P").frozen(false).pattern("[A-Z]{2}").build();

        registry.property().name("B").type("B").ordinal(0).build();
        registry.property().name("E").type("E").ordinal(1).build();
        registry.property().name("I").type("I").ordinal(2).build();
        registry.property().name("R").type("R").ordinal(3).build();
        registry.property().name("P").type("P").ordinal(4).build();

        registry.alias().name("X").expression("E = E1 or B").build();
        registry.alias().name("Y").expression("I in {1~10}").build();
        registry.createAssertion("X");

        final DictionaryHandle handle = new DictionaryHandle(registry);
        // final Expression expression = new Expression("I = 10");
        final Expression expression = new Expression("X");
        final SatSystem system = new SatSystem(handle,
                                               expression,
                                               ProverFeatures.builder()
                                                             .assertionStrategy(AssertionStrategy.EXCLUDE_ASSERTIONS)
                                                             .reserveStrategy(ReserveStrategy.NO_RESERVES)
                                                             .build());
        assertEquals(expression.getRootNode(), system.getMainNode());
        LOGGER.debug("System");
        system.print(OUT, 1);
    }
}