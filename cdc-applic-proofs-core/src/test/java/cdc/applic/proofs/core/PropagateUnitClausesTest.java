package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.AtomNode;
import cdc.applic.proofs.core.ast.AtomVarNode;
import cdc.applic.proofs.core.clauses.Clause;
import cdc.applic.proofs.core.clauses.Literal;
import cdc.applic.proofs.core.clauses.PropagateUnitClauses;
import cdc.applic.proofs.core.clauses.Sentence;

class PropagateUnitClausesTest {
    private static AtomNode var(int value) {
        return new AtomVarNode("Var#" + Math.abs(value));
    }

    private static Literal literal(int value) {
        return new Literal(var(value), value > 0);
    }

    private static Clause clause(int... values) {
        final List<Literal> literals = new ArrayList<>();
        for (final int value : values) {
            literals.add(literal(value));
        }
        return new Clause(literals);
    }

    private static Sentence sentence(Clause... clauses) {
        return new Sentence(clauses);
    }

    private static void checkOnlyVars(Sentence input,
                                      Sentence expected) {
        final Sentence output = PropagateUnitClauses.execute(input, PropagateUnitClauses.Variant.ONLY_VARS);
        assertEquals(expected, output);
    }

    @Test
    void testOnlyVars() {
        checkOnlyVars(sentence(clause(1)), sentence());
        checkOnlyVars(sentence(clause(-1)), sentence());

        checkOnlyVars(sentence(clause(1), clause(-1)), Sentence.FALSE);
        checkOnlyVars(sentence(clause(1), clause(2)), sentence());
        checkOnlyVars(sentence(clause(1), clause(-2)), sentence());
        checkOnlyVars(sentence(clause(-1), clause(-2)), sentence());
        checkOnlyVars(sentence(clause(-1), clause(2)), sentence());

        checkOnlyVars(sentence(clause(1), clause(1, 2)), sentence());
        checkOnlyVars(sentence(clause(1), clause(-1, 2)), sentence());
        checkOnlyVars(sentence(clause(1), clause(-1, -2)), sentence());
        checkOnlyVars(sentence(clause(1), clause(1, -2)), sentence());

        checkOnlyVars(sentence(clause(1), clause(1, 2, 3)), sentence());
        checkOnlyVars(sentence(clause(1), clause(2, 3)), sentence(clause(2, 3)));
        checkOnlyVars(sentence(clause(1), clause(-1, 2, 3)), sentence(clause(2, 3)));

        checkOnlyVars(sentence(clause(1), clause(1, 2), clause(1, 2, 3)), sentence());
        checkOnlyVars(sentence(clause(1), clause(2, 3), clause(1, 2, 3)), sentence(clause(2, 3)));
        checkOnlyVars(sentence(clause(1), clause(1, 2, 3)), sentence());
        checkOnlyVars(sentence(clause(-1), clause(1, 2, 3)), sentence(clause(2, 3)));
        checkOnlyVars(sentence(clause(1), clause(2), clause(-1, -2)), Sentence.FALSE);
        checkOnlyVars(sentence(clause(1), clause(2), clause(1, 2), clause(-1, -2)), Sentence.FALSE);
    }

    private static void checkAll(Sentence input,
                                 Sentence expected) {
        final Sentence output = PropagateUnitClauses.execute(input, PropagateUnitClauses.Variant.ALL);
        assertEquals(expected, output);
    }

    @Test
    void testAll() {
        checkAll(sentence(clause(1)), sentence());
        checkAll(sentence(clause(-1)), sentence());

        checkAll(sentence(clause(1), clause(-1)), Sentence.FALSE);
        checkAll(sentence(clause(1), clause(2)), sentence());
        checkAll(sentence(clause(1), clause(-2)), sentence());
        checkAll(sentence(clause(-1), clause(-2)), sentence());
        checkAll(sentence(clause(-1), clause(2)), sentence());

        checkAll(sentence(clause(1), clause(1, 2)), sentence());
        checkAll(sentence(clause(1), clause(-1, 2)), sentence());
        checkAll(sentence(clause(1), clause(-1, -2)), sentence());
        checkAll(sentence(clause(1), clause(1, -2)), sentence());

        checkAll(sentence(clause(1), clause(1, 2, 3)), sentence());
        checkAll(sentence(clause(1), clause(2, 3)), sentence(clause(2, 3)));
        checkAll(sentence(clause(1), clause(-1, 2, 3)), sentence(clause(2, 3)));

        checkAll(sentence(clause(1), clause(1, 2), clause(1, 2, 3)), sentence());
        checkAll(sentence(clause(1), clause(2, 3), clause(1, 2, 3)), sentence(clause(2, 3)));
        checkAll(sentence(clause(1), clause(1, 2, 3)), sentence());
        checkAll(sentence(clause(-1), clause(1, 2, 3)), sentence(clause(2, 3)));
        checkAll(sentence(clause(1), clause(2), clause(-1, -2)), Sentence.FALSE);
        checkAll(sentence(clause(1), clause(2), clause(1, 2), clause(-1, -2)), Sentence.FALSE);
    }
}