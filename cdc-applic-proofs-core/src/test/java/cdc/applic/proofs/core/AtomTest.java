package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.NodeKerning;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.RealRange;
import cdc.applic.expressions.content.RealValue;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.literals.Name;
import cdc.applic.proofs.core.ast.AtomIntegerRangeNode;
import cdc.applic.proofs.core.ast.AtomRealRangeNode;
import cdc.applic.proofs.core.ast.AtomReserveNode;
import cdc.applic.proofs.core.ast.AtomValueNode;
import cdc.applic.proofs.core.ast.AtomVarNode;

class AtomTest {
    private static final Logger LOGGER = LogManager.getLogger(AtomTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    private final RepositorySupport support = new RepositorySupport();

    private Property property(String name) {
        return support.registry.getProperty(Name.of(name));
    }

    private final AtomVarNode nVar = new AtomVarNode("A");
    private final AtomValueNode nValue = new AtomValueNode(property("E"), StringValue.of("E1"));
    private final AtomReserveNode nReserve = new AtomReserveNode(property("E"));
    private final AtomIntegerRangeNode nIntegerRange = new AtomIntegerRangeNode(property("I"), IntegerRange.of(10));
    private final AtomRealRangeNode nRealRange = new AtomRealRangeNode(property("R"), RealRange.of(10.0));

    @Test
    void testGetKind() {
        assertEquals(AtomVarNode.KIND, nVar.getKind());
        assertEquals(AtomValueNode.KIND, nValue.getKind());
        assertEquals(AtomReserveNode.KIND, nReserve.getKind());
        assertEquals(AtomIntegerRangeNode.KIND, nIntegerRange.getKind());
        assertEquals(AtomRealRangeNode.KIND, nRealRange.getKind());
    }

    @Test
    void testGetKerning() {
        assertEquals(NodeKerning.KERNING_ATOM, nVar.getKerning());
        assertEquals(NodeKerning.KERNING_ATOM, nValue.getKerning());
        assertEquals(NodeKerning.KERNING_ATOM, nReserve.getKerning());
        assertEquals(NodeKerning.KERNING_ATOM, nIntegerRange.getKerning());
        assertEquals(NodeKerning.KERNING_ATOM, nRealRange.getKerning());
    }

    @Test
    void testEqual() {
        assertEquals(nVar, nVar);
        assertEquals(nVar, new AtomVarNode("A"));
        assertNotEquals(nVar, null);
        assertNotEquals(nVar, "Hello");

        assertEquals(nValue, nValue);
        assertEquals(nValue, new AtomValueNode(property("E"), StringValue.of("E1")));
        assertNotEquals(nValue, new AtomValueNode(property("E"), StringValue.of("E2")));
        assertNotEquals(nValue, new AtomValueNode(property("B"), BooleanValue.TRUE));
        assertNotEquals(nValue, new AtomValueNode(property("B"), StringValue.of("E1")));
        assertNotEquals(nValue, null);
        assertNotEquals(nValue, "Hello");

        assertEquals(nReserve, nReserve);
        assertEquals(nReserve, new AtomReserveNode(property("E")));
        assertNotEquals(nReserve, new AtomReserveNode(property("B")));
        assertNotEquals(nReserve, null);
        assertNotEquals(nReserve, "Hello");

        assertEquals(nIntegerRange, nIntegerRange);
        assertEquals(nIntegerRange, new AtomIntegerRangeNode(property("I"), IntegerValue.of(10)));
        assertNotEquals(nIntegerRange, new AtomIntegerRangeNode(property("I"), IntegerRange.of(10, 11)));
        assertNotEquals(nIntegerRange, new AtomIntegerRangeNode(property("I"), IntegerRange.of(9, 10)));
        assertNotEquals(nIntegerRange, new AtomIntegerRangeNode(property("B"), IntegerValue.of(11)));
        assertNotEquals(nIntegerRange, new AtomIntegerRangeNode(property("B"), IntegerValue.of(10)));
        assertNotEquals(nIntegerRange, null);
        assertNotEquals(nIntegerRange, "Hello");

        assertEquals(nRealRange, nRealRange);
        assertEquals(nRealRange, new AtomRealRangeNode(property("R"), RealValue.of(10)));
        assertNotEquals(nRealRange, new AtomRealRangeNode(property("R"), RealRange.of(10, 11)));
        assertNotEquals(nRealRange, new AtomRealRangeNode(property("R"), RealRange.of(9, 10)));
        assertNotEquals(nRealRange, new AtomRealRangeNode(property("B"), RealValue.of(11)));
        assertNotEquals(nRealRange, new AtomRealRangeNode(property("B"), RealValue.of(10)));
        assertNotEquals(nRealRange, null);
        assertNotEquals(nRealRange, "Hello");
    }

    @Test
    void testHashCode() {
        assertEquals(nVar.hashCode(), nVar.hashCode());
        assertEquals(nValue.hashCode(), nValue.hashCode());
        assertEquals(nReserve.hashCode(), nReserve.hashCode());
        assertEquals(nIntegerRange.hashCode(), nIntegerRange.hashCode());
        assertEquals(nRealRange.hashCode(), nRealRange.hashCode());
    }

    @Test
    void testAtomVar() {
        assertEquals(Name.of("A"), nVar.getName());
        assertEquals("VAR(A)", nVar.toString());
        assertEquals("A", nVar.toInfix(Formatting.SHORT_NARROW));
        assertEquals(nVar, new AtomVarNode(Name.of("A")));
    }

    @Test
    void testAtomValue() {
        assertEquals(StringValue.of("E1"), nValue.getValue());
        assertEquals("VALUE(E,E1)", nValue.toString());
        assertEquals("E@E1", nValue.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testAtomReserve() {
        assertEquals("RESERVE(E)", nReserve.toString());
        assertEquals("E@R@", nReserve.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testAtomIntegerRange() {
        assertEquals(IntegerValue.of(10), nIntegerRange.getMin());
        assertEquals(IntegerValue.of(10), nIntegerRange.getMax());
        assertEquals("INTEGER_RANGE(I,10,10)", nIntegerRange.toString());
        assertEquals("I@10~10", nIntegerRange.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testAtomRealRange() {
        assertEquals(RealValue.of(10), nRealRange.getMin());
        assertEquals(RealValue.of(10), nRealRange.getMax());
        assertEquals("REAL_RANGE(R,10.0,10.0)", nRealRange.toString());
        assertEquals("R@10.0~10.0", nRealRange.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testPrint() {
        nVar.print(OUT, 0);
        nValue.print(OUT, 0);
        nReserve.print(OUT, 0);
        nIntegerRange.print(OUT, 0);
        nRealRange.print(OUT, 0);
        assertTrue(true);
    }
}