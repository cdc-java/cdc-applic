package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.clauses.SatSystem;

class SatSystemSolverTest {
    private static final Logger LOGGER = LogManager.getLogger(SatSystemSolverTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();
    private final RepositorySupport support = new RepositorySupport();

    private void check(String expression,
                       ProverFeatures features,
                       boolean expected) {
        LOGGER.debug("======================================================");
        final Expression x = new Expression(expression);
        final SatSystemSolverFactory factory = new SatSystemSolverFactory();
        final SatSystemSolver solver = factory.newSolver();
        final SatSystem system = new SatSystem(support.registryHandle, x, features);
        LOGGER.debug("System");
        system.print(OUT, 1);

        final boolean satisfiable = solver.isSatisfiable(system);
        LOGGER.debug(expression + ": " + (satisfiable ? "SAT" : "NOT SAT"));
        assertEquals(expected, satisfiable);
    }

    private void checkExcludeAssertionsNoReserves(String expression,
                                                  boolean expected) {
        check(expression,
              ProverFeatures.builder()
                            .assertionStrategy(AssertionStrategy.EXCLUDE_ASSERTIONS)
                            .reserveStrategy(ReserveStrategy.NO_RESERVES)
                            .build(),
              expected);
    }

    @Test
    void testExcludeAssertionsNoReserves() {
        checkExcludeAssertionsNoReserves("true", true);
        checkExcludeAssertionsNoReserves("false", false);

        checkExcludeAssertionsNoReserves("B", true);
        checkExcludeAssertionsNoReserves("not B", true);
        checkExcludeAssertionsNoReserves("B = true", true);
        checkExcludeAssertionsNoReserves("B = false", true);
        checkExcludeAssertionsNoReserves("B in {}", false);
        checkExcludeAssertionsNoReserves("B not in {}", true);
        checkExcludeAssertionsNoReserves("not (B in {})", true);

        checkExcludeAssertionsNoReserves("B <-> B = true", true);
        checkExcludeAssertionsNoReserves("not (B <-> B = true)", false);

        checkExcludeAssertionsNoReserves("E = E1", true);
        checkExcludeAssertionsNoReserves("E = E1 & E = E2", false);
        checkExcludeAssertionsNoReserves("E = E1 | E = E2", true);
        checkExcludeAssertionsNoReserves("E in {}", false);
        checkExcludeAssertionsNoReserves("E not in {}", true);
        checkExcludeAssertionsNoReserves("not (E in {})", true);

        checkExcludeAssertionsNoReserves("P = AA", true);
        checkExcludeAssertionsNoReserves("P != AA", true);
        checkExcludeAssertionsNoReserves("P = AA | P = BB", true);
        checkExcludeAssertionsNoReserves("P = AA & P = BB", false);
        checkExcludeAssertionsNoReserves("P in {}", false);
        checkExcludeAssertionsNoReserves("P not in {}", true);
        checkExcludeAssertionsNoReserves("not (P in {})", true);

        checkExcludeAssertionsNoReserves("I = 10", true);
        checkExcludeAssertionsNoReserves("I = 10 | I = 11", true);
        checkExcludeAssertionsNoReserves("I = 10 & I = 11", false);
        checkExcludeAssertionsNoReserves("I <: {10~12} & I <: {11~13}", true);
        checkExcludeAssertionsNoReserves("I in {}", false);
        checkExcludeAssertionsNoReserves("I not in {}", true);
        checkExcludeAssertionsNoReserves("not (I in {})", true);

        checkExcludeAssertionsNoReserves("R = 10.0", true);
        checkExcludeAssertionsNoReserves("R = 10.0 | R = 11.0", true);
        checkExcludeAssertionsNoReserves("R = 10.0 & R = 11.0", false);
        checkExcludeAssertionsNoReserves("R <: {10.0~12.0} & R <: {11.0~13.0}", true);
        checkExcludeAssertionsNoReserves("R in {}", false);
        checkExcludeAssertionsNoReserves("R not in {}", true);
        checkExcludeAssertionsNoReserves("not (R in {})", true);

        checkExcludeAssertionsNoReserves("$X$", true);
        checkExcludeAssertionsNoReserves("not $X$", true);
        checkExcludeAssertionsNoReserves("$X$ and not $X$", false);
        checkExcludeAssertionsNoReserves("$X$ and false", false);
        checkExcludeAssertionsNoReserves("not $X$ and false", false);
        checkExcludeAssertionsNoReserves("not ($X$ and false)", true);
    }
}