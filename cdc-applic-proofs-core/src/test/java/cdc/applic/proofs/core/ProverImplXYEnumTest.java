package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositoryXYEnumSupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.Node;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.ProverMatching;
import cdc.applic.proofs.core.clauses.ProverImpl;
import cdc.applic.proofs.core.encoding.Encoders;

class ProverImplXYEnumTest extends RepositoryXYEnumSupport implements ProverSupport {
    private static final Logger LOGGER = LogManager.getLogger(ProverImplXYEnumTest.class);

    private static <R> void checkUnary(Function<Expression, R> lambda,
                                       String lambdaName,
                                       String expression,
                                       R expected) {
        LOGGER.debug("==================================================");
        final R result = lambda.apply(new Expression(expression));
        assertEquals(expected, result, lambdaName + "(" + expression + ")");
    }

    private static <R> void checkBinary(BiFunction<Expression, Expression, R> lambda,
                                        String lambdaName,
                                        String alpha,
                                        String beta,
                                        R expected) {
        LOGGER.debug("==================================================");
        final R result = lambda.apply(new Expression(alpha), new Expression(beta));
        assertEquals(expected, result, lambdaName + "(" + alpha + ", " + beta + ")");
    }

    @FunctionalInterface
    private static interface Nary {
        public boolean apply(Expression... expressions);
    }

    private static void checkNary(Nary lambda,
                                  String lambdaName,
                                  boolean expected,
                                  String... alphas) {
        LOGGER.debug("==================================================");
        final Expression[] expressions = new Expression[alphas.length];
        for (int index = 0; index < alphas.length; index++) {
            expressions[index] = new Expression(alphas[index]);
        }
        final boolean result = lambda.apply(expressions);
        assertEquals(expected, result, lambdaName + "(" + Arrays.toString(alphas) + ")");
    }

    @FunctionalInterface
    private static interface ContextNary {
        public boolean apply(Expression context,
                             Expression... expressions);
    }

    private static void checkContextNary(ContextNary lambda,
                                         String lambdaName,
                                         boolean expected,
                                         String context,
                                         String... alphas) {
        LOGGER.debug("==================================================");
        final Expression[] expressions = new Expression[alphas.length];
        for (int index = 0; index < alphas.length; index++) {
            expressions[index] = new Expression(alphas[index]);
        }
        final boolean result = lambda.apply(new Expression(context), expressions);
        assertEquals(expected, result, lambdaName + "(" + context + ", " + Arrays.toString(alphas) + ")");
    }

    private void checkIsSometimesTrue(ProverFeatures features,
                                      String expression,
                                      boolean expected) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkUnary(prover::isSometimesTrue, "isSometimesTrue (" + features + ")", expression, expected);
    }

    /**
     * @param expression The expression.
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     */
    private void checkIsSometimesTrue(String expression,
                                      boolean expectedInNo,
                                      boolean expectedInAll,
                                      boolean expectedExNo,
                                      boolean expectedExAll) {
        checkIsSometimesTrue(IN_NO, expression, expectedInNo);
        checkIsSometimesTrue(IN_ALL, expression, expectedInAll);
        checkIsSometimesTrue(EX_NO, expression, expectedExNo);
        checkIsSometimesTrue(EX_ALL, expression, expectedExAll);
    }

    @Test
    void testIsSometimesTrue() {
        checkIsSometimesTrue(x(1), false, false, true, true);
        checkIsSometimesTrue(x(2), true, true, true, true);
        checkIsSometimesTrue(x(3), true, true, true, true);

        checkIsSometimesTrue(y(1), true, true, true, true);
        checkIsSometimesTrue(y(2), true, true, true, true);

        checkIsSometimesTrue(x(1, 2), true, true, true, true);
        checkIsSometimesTrue(x(1, 3), true, true, true, true);
        checkIsSometimesTrue(x(2, 3), true, true, true, true);

        checkIsSometimesTrue(x(1, 2, 3), true, true, true, true);

        checkIsSometimesTrue(y(1, 2), true, true, true, true);
    }

    private void checkIsNeverTrue(ProverFeatures features,
                                  String expression,
                                  boolean expected) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkUnary(prover::isNeverTrue, "isNeverTrue (" + features + ")", expression, expected);
    }

    /**
     * @param expression The expression.
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     */
    private void checkIsNeverTrue(String expression,
                                  boolean expectedInNo,
                                  boolean expectedInAll,
                                  boolean expectedExNo,
                                  boolean expectedExAll) {
        checkIsNeverTrue(IN_NO, expression, expectedInNo);
        checkIsNeverTrue(IN_ALL, expression, expectedInAll);
        checkIsNeverTrue(EX_NO, expression, expectedExNo);
        checkIsNeverTrue(EX_ALL, expression, expectedExAll);
    }

    @Test
    void testIsNeverTrue() {
        checkIsNeverTrue(x(1), true, true, false, false);
        checkIsNeverTrue(x(2), false, false, false, false);
        checkIsNeverTrue(x(3), false, false, false, false);

        checkIsNeverTrue(y(1), false, false, false, false);
        checkIsNeverTrue(y(2), false, false, false, false);

        checkIsNeverTrue(x(1, 2), false, false, false, false);
        checkIsNeverTrue(x(1, 3), false, false, false, false);
        checkIsNeverTrue(x(2, 3), false, false, false, false);

        checkIsNeverTrue(x(1, 2, 3), false, false, false, false);

        checkIsNeverTrue(y(1, 2), false, false, false, false);
    }

    private void checkIsAlwaysTrue(ProverFeatures features,
                                   String expression,
                                   boolean expected) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkUnary(prover::isAlwaysTrue, "isAlwaysTrue (" + features + ")", expression, expected);
    }

    /**
     * @param expression The expression.
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     */
    private void checkIsAlwaysTrue(String expression,
                                   boolean expectedInNo,
                                   boolean expectedInAll,
                                   boolean expectedExNo,
                                   boolean expectedExAll) {
        checkIsAlwaysTrue(IN_NO, expression, expectedInNo);
        checkIsAlwaysTrue(IN_ALL, expression, expectedInAll);
        checkIsAlwaysTrue(EX_NO, expression, expectedExNo);
        checkIsAlwaysTrue(EX_ALL, expression, expectedExAll);
    }

    @Test
    void testIsAlwaysTrue() {
        checkIsAlwaysTrue(x(1), false, false, false, false);
        checkIsAlwaysTrue(x(2), false, false, false, false);
        checkIsAlwaysTrue(x(3), false, false, false, false);

        checkIsAlwaysTrue(y(1), false, false, false, false);
        checkIsAlwaysTrue(y(2), false, false, false, false);

        checkIsAlwaysTrue(x(1, 2), false, false, false, false);
        checkIsAlwaysTrue(x(1, 3), false, false, false, false);
        checkIsAlwaysTrue(x(2, 3), true, false, false, false);

        checkIsAlwaysTrue(x(1, 2, 3), true, false, true, false);

        checkIsAlwaysTrue(y(1, 2), true, false, true, false);
    }

    private void checkIsSometimesFalse(ProverFeatures features,
                                       String expression,
                                       boolean expected) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkUnary(prover::isSometimesFalse, "isSometimesFalse (" + features + ")", expression, expected);
    }

    /**
     * @param expression The expression.
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     */
    private void checkIsSometimesFalse(String expression,
                                       boolean expectedInNo,
                                       boolean expectedInAll,
                                       boolean expectedExNo,
                                       boolean expectedExAll) {
        checkIsSometimesFalse(IN_NO, expression, expectedInNo);
        checkIsSometimesFalse(IN_ALL, expression, expectedInAll);
        checkIsSometimesFalse(EX_NO, expression, expectedExNo);
        checkIsSometimesFalse(EX_ALL, expression, expectedExAll);
    }

    @Test
    void testIsSometimesFalse() {
        checkIsSometimesFalse(x(1), true, true, true, true);
        checkIsSometimesFalse(x(2), true, true, true, true);
        checkIsSometimesFalse(x(3), true, true, true, true);

        checkIsSometimesFalse(y(1), true, true, true, true);
        checkIsSometimesFalse(y(2), true, true, true, true);

        checkIsSometimesFalse(x(1, 2), true, true, true, true);
        checkIsSometimesFalse(x(1, 3), true, true, true, true);
        checkIsSometimesFalse(x(2, 3), false, true, true, true);

        checkIsSometimesFalse(x(1, 2, 3), false, true, false, true);

        checkIsSometimesFalse(y(1, 2), false, true, false, true);
    }

    private void checkIsNeverFalse(ProverFeatures features,
                                   String expression,
                                   boolean expected) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkUnary(prover::isNeverFalse, "isNeverFalse (" + features + ")", expression, expected);
    }

    /**
     * @param expression The expression.
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     */
    private void checkIsNeverFalse(String expression,
                                   boolean expectedInNo,
                                   boolean expectedInAll,
                                   boolean expectedExNo,
                                   boolean expectedExAll) {
        checkIsNeverFalse(IN_NO, expression, expectedInNo);
        checkIsNeverFalse(IN_ALL, expression, expectedInAll);
        checkIsNeverFalse(EX_NO, expression, expectedExNo);
        checkIsNeverFalse(EX_ALL, expression, expectedExAll);
    }

    @Test
    void testIsNeverFalse() {
        checkIsNeverFalse(x(1), false, false, false, false);
        checkIsNeverFalse(x(2), false, false, false, false);
        checkIsNeverFalse(x(3), false, false, false, false);

        checkIsNeverFalse(y(1), false, false, false, false);
        checkIsNeverFalse(y(2), false, false, false, false);

        checkIsNeverFalse(x(1, 2), false, false, false, false);
        checkIsNeverFalse(x(1, 3), false, false, false, false);
        checkIsNeverFalse(x(2, 3), true, false, false, false);

        checkIsNeverFalse(x(1, 2, 3), true, false, true, false);

        checkIsNeverFalse(y(1, 2), true, false, true, false);
    }

    private void checkIsAlwaysFalse(ProverFeatures features,
                                    String expression,
                                    boolean expected) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkUnary(prover::isAlwaysFalse, "isAlwaysFalse (" + features + ")", expression, expected);
    }

    /**
     * @param expression The expression.
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     */
    private void checkIsAlwaysFalse(String expression,
                                    boolean expectedInNo,
                                    boolean expectedInAll,
                                    boolean expectedExNo,
                                    boolean expectedExAll) {
        checkIsAlwaysFalse(IN_NO, expression, expectedInNo);
        checkIsAlwaysFalse(IN_ALL, expression, expectedInAll);
        checkIsAlwaysFalse(EX_NO, expression, expectedExNo);
        checkIsAlwaysFalse(EX_ALL, expression, expectedExAll);
    }

    @Test
    void testIsAlwaysFalse() {
        checkIsAlwaysFalse(x(1), true, true, false, false);
        checkIsAlwaysFalse(x(2), false, false, false, false);
        checkIsAlwaysFalse(x(3), false, false, false, false);

        checkIsAlwaysFalse(y(1), false, false, false, false);
        checkIsAlwaysFalse(y(2), false, false, false, false);

        checkIsAlwaysFalse(x(1, 2), false, false, false, false);
        checkIsAlwaysFalse(x(1, 3), false, false, false, false);
        checkIsAlwaysFalse(x(2, 3), false, false, false, false);

        checkIsAlwaysFalse(x(1, 2, 3), false, false, false, false);

        checkIsAlwaysFalse(y(1, 2), false, false, false, false);
    }

    private void checkGetMatching(ProverFeatures features,
                                  String expression,
                                  ProverMatching expected) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkUnary(prover::getMatching, "getMatching (" + features + ")", expression, expected);
    }

    /**
     * @param expression The expression.
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     */
    private void checkGetMatching(String expression,
                                  ProverMatching expectedInNo,
                                  ProverMatching expectedInAll,
                                  ProverMatching expectedExNo,
                                  ProverMatching expectedExAll) {
        checkGetMatching(IN_NO, expression, expectedInNo);
        checkGetMatching(IN_ALL, expression, expectedInAll);
        checkGetMatching(EX_NO, expression, expectedExNo);
        checkGetMatching(EX_ALL, expression, expectedExAll);
    }

    @Test
    void testGetMatching() {
        checkGetMatching(x(1), N, N, S, S);
        checkGetMatching(x(2), S, S, S, S);
        checkGetMatching(x(3), S, S, S, S);

        checkGetMatching(y(1), S, S, S, S);
        checkGetMatching(y(2), S, S, S, S);

        checkGetMatching(x(1, 2), S, S, S, S);
        checkGetMatching(x(1, 3), S, S, S, S);
        checkGetMatching(x(2, 3), A, S, S, S);

        checkGetMatching(x(1, 2, 3), A, S, A, S);

        checkGetMatching(y(1, 2), A, S, A, S);
    }

    private void checkGetProjectedMatching(ProverFeatures features,
                                           String domain,
                                           String on,
                                           ProverMatching expected) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkBinary(prover::getProjectedMatching, "getProjectedMatching (" + features + ")", domain, on, expected);
    }

    /**
     * @param domain The source domain.
     * @param on The target domain.
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     */
    private void checkGetProjectedMatching(String domain,
                                           String on,
                                           ProverMatching expectedInNo,
                                           ProverMatching expectedInAll,
                                           ProverMatching expectedExNo,
                                           ProverMatching expectedExAll) {
        checkGetProjectedMatching(IN_NO, domain, on, expectedInNo);
        checkGetProjectedMatching(IN_ALL, domain, on, expectedInAll);
        checkGetProjectedMatching(EX_NO, domain, on, expectedExNo);
        checkGetProjectedMatching(EX_ALL, domain, on, expectedExAll);
    }

    @Test
    void testGetProjectedMatching() {
        checkGetProjectedMatching("true", "true", A, A, A, A);
        checkGetProjectedMatching("false", "true", N, N, N, N);

        checkGetProjectedMatching(x(), x(), N, N, N, N);
        checkGetProjectedMatching(x(), x(1), N, N, N, N);
        checkGetProjectedMatching(x(), x(2), N, N, N, N);
        checkGetProjectedMatching(x(), x(3), N, N, N, N);
        checkGetProjectedMatching(x(), x(1, 2), N, N, N, N);
        checkGetProjectedMatching(x(), x(1, 3), N, N, N, N);
        checkGetProjectedMatching(x(), x(2, 3), N, N, N, N);
        checkGetProjectedMatching(x(), x(1, 2, 3), N, N, N, N);

        checkGetProjectedMatching(x(1), x(), N, N, N, N);
        checkGetProjectedMatching(x(1), x(1), N, N, A, A);
        checkGetProjectedMatching(x(1), x(2), N, N, N, N);
        checkGetProjectedMatching(x(1), x(3), N, N, N, N);
        checkGetProjectedMatching(x(1), x(1, 2), N, N, S, S);
        checkGetProjectedMatching(x(1), x(1, 3), N, N, S, S);
        checkGetProjectedMatching(x(1), x(2, 3), N, N, N, N);
        checkGetProjectedMatching(x(1), x(1, 2, 3), N, N, S, S);

        checkGetProjectedMatching(x(2), x(), N, N, N, N);
        checkGetProjectedMatching(x(2), x(1), N, N, N, N);
        checkGetProjectedMatching(x(2), x(2), A, A, A, A);
        checkGetProjectedMatching(x(2), x(3), N, N, N, N);
        checkGetProjectedMatching(x(2), x(1, 2), A, A, S, S);
        checkGetProjectedMatching(x(2), x(1, 3), N, N, N, N);
        checkGetProjectedMatching(x(2), x(2, 3), S, S, S, S);
        checkGetProjectedMatching(x(2), x(1, 2, 3), S, S, S, S);

        checkGetProjectedMatching(x(3), x(), N, N, N, N);
        checkGetProjectedMatching(x(3), x(1), N, N, N, N);
        checkGetProjectedMatching(x(3), x(2), N, N, N, N);
        checkGetProjectedMatching(x(3), x(3), A, A, A, A);
        checkGetProjectedMatching(x(3), x(1, 2), N, N, N, N);
        checkGetProjectedMatching(x(3), x(1, 3), A, A, S, S);
        checkGetProjectedMatching(x(3), x(2, 3), S, S, S, S);
        checkGetProjectedMatching(x(3), x(1, 2, 3), S, S, S, S);

        checkGetProjectedMatching(x(1, 2, 3), x(1), N, N, A, A);
        checkGetProjectedMatching(x(1, 2, 3), x(2), A, A, A, A);
    }

    private void checkIntersects(ProverFeatures features,
                                 String alpha,
                                 String beta,
                                 boolean expected) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkBinary(prover::intersects, "intersects (" + features + ")", alpha, beta, expected);
    }

    /**
     * @param alpha The alpha expression.
     * @param beta The beta expression.
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     */
    private void checkIntersects(String alpha,
                                 String beta,
                                 boolean expectedInNo,
                                 boolean expectedInAll,
                                 boolean expectedExNo,
                                 boolean expectedExAll) {
        checkIntersects(IN_NO, alpha, beta, expectedInNo);
        checkIntersects(IN_ALL, alpha, beta, expectedInAll);
        checkIntersects(EX_NO, alpha, beta, expectedExNo);
        checkIntersects(EX_ALL, alpha, beta, expectedExAll);
    }

    @Test
    void testIntersects() {
        checkIntersects(x(1), x(1), false, false, true, true);
        checkIntersects(x(2), x(2), true, true, true, true);
        checkIntersects(x(1), y(1), false, false, true, true);
    }

    private void checkContains(ProverFeatures features,
                               String alpha,
                               String beta,
                               boolean expected) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkBinary(prover::contains, " contains(" + features + ")", alpha, beta, expected);
    }

    /**
     * @param alpha The alpha expression.
     * @param beta The beta expression.
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     */
    private void checkContains(String alpha,
                               String beta,
                               boolean expectedInNo,
                               boolean expectedInAll,
                               boolean expectedExNo,
                               boolean expectedExAll) {
        checkContains(IN_NO, alpha, beta, expectedInNo);
        checkContains(IN_ALL, alpha, beta, expectedInAll);
        checkContains(EX_NO, alpha, beta, expectedExNo);
        checkContains(EX_ALL, alpha, beta, expectedExAll);
    }

    @Test
    void testContains() {
        checkContains(x(1), x(1), true, true, true, true);
        checkContains(x(1), x(2), false, false, false, false);
        checkContains(x(2), x(2), true, true, true, true);
        checkContains(x(2), x(1, 2), true, true, false, false);
        checkContains(x(1, 2), x(1), true, true, true, true);
        checkContains(x(1, 2), x(2), true, true, true, true);
        checkContains(x(1, 2), x(1, 2), true, true, true, true);
        checkContains(x(1, 2, 3), x(1), true, true, true, true);
        checkContains(x(1, 2, 3), x(2), true, true, true, true);
        checkContains(x(1, 2, 3), x(3), true, true, true, true);
        checkContains(x(1, 2, 3), x(1, 2), true, true, true, true);
        checkContains(x(1, 2, 3), x(1, 3), true, true, true, true);
        checkContains(x(1, 2, 3), x(2, 3), true, true, true, true);
    }

    private void checkContainsNonEmpty(ProverFeatures features,
                                       String alpha,
                                       String beta,
                                       boolean expected) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkBinary(prover::containsNonEmpty, " containsNonEmpty(" + features + ")", alpha, beta, expected);
    }

    /**
     * @param alpha The alpha expression.
     * @param beta The beta expression.
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     */
    private void checkContainsNonEmpty(String alpha,
                                       String beta,
                                       boolean expectedInNo,
                                       boolean expectedInAll,
                                       boolean expectedExNo,
                                       boolean expectedExAll) {
        checkContainsNonEmpty(IN_NO, alpha, beta, expectedInNo);
        checkContainsNonEmpty(IN_ALL, alpha, beta, expectedInAll);
        checkContainsNonEmpty(EX_NO, alpha, beta, expectedExNo);
        checkContainsNonEmpty(EX_ALL, alpha, beta, expectedExAll);
    }

    @Test
    void testContainsNonEmpty() {
        checkContainsNonEmpty(x(1), x(1), false, false, true, true);
        checkContainsNonEmpty(x(1), x(2), false, false, false, false);
        checkContainsNonEmpty(x(2), x(2), true, true, true, true);
        checkContainsNonEmpty(x(2), x(1, 2), true, true, false, false);
        checkContainsNonEmpty(x(1, 2), x(1), false, false, true, true);
        checkContainsNonEmpty(x(1, 2), x(2), true, true, true, true);
        checkContainsNonEmpty(x(1, 2), x(1, 2), true, true, true, true);
        checkContainsNonEmpty(x(1, 2, 3), x(1), false, false, true, true);
        checkContainsNonEmpty(x(1, 2, 3), x(2), true, true, true, true);
        checkContainsNonEmpty(x(1, 2, 3), x(3), true, true, true, true);
        checkContainsNonEmpty(x(1, 2, 3), x(1, 2), true, true, true, true);
        checkContainsNonEmpty(x(1, 2, 3), x(1, 3), true, true, true, true);
        checkContainsNonEmpty(x(1, 2, 3), x(2, 3), true, true, true, true);
    }

    private void checkAlwaysAtLeastOne(ProverFeatures features,
                                       boolean expected,
                                       String... alphas) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkNary(prover::alwaysAtLeastOne, " alwaysAtLeastOne(" + features + ")", expected, alphas);
    }

    /**
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     * @param alphas The expressions.
     */
    private void checkAlwaysAtLeastOne(boolean expectedInNo,
                                       boolean expectedInAll,
                                       boolean expectedExNo,
                                       boolean expectedExAll,
                                       String... alphas) {
        checkAlwaysAtLeastOne(IN_NO, expectedInNo, alphas);
        checkAlwaysAtLeastOne(IN_ALL, expectedInAll, alphas);
        checkAlwaysAtLeastOne(EX_NO, expectedExNo, alphas);
        checkAlwaysAtLeastOne(EX_ALL, expectedExAll, alphas);
    }

    @Test
    void testAlwaysAtLeastOne() {
        checkAlwaysAtLeastOne(false, false, false, false, x(1));
        checkAlwaysAtLeastOne(true, false, true, false, x(1), x(2), x(3));
        checkAlwaysAtLeastOne(true, false, false, false, x(2), x(3));
        checkAlwaysAtLeastOne(true, false, false, false, x(2), x(2, 3));
    }

    private void checkAlwaysAtMostOne(ProverFeatures features,
                                      boolean expected,
                                      String... alphas) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkNary(prover::alwaysAtMostOne, " alwaysAtMostOne(" + features + ")", expected, alphas);
    }

    /**
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     * @param alphas The expressions.
     */
    private void checkAlwaysAtMostOne(boolean expectedInNo,
                                      boolean expectedInAll,
                                      boolean expectedExNo,
                                      boolean expectedExAll,
                                      String... alphas) {
        checkAlwaysAtMostOne(IN_NO, expectedInNo, alphas);
        checkAlwaysAtMostOne(IN_ALL, expectedInAll, alphas);
        checkAlwaysAtMostOne(EX_NO, expectedExNo, alphas);
        checkAlwaysAtMostOne(EX_ALL, expectedExAll, alphas);
    }

    @Test
    void testAlwaysAtMostOne() {
        checkAlwaysAtMostOne(true, true, true, true, x(1));
        checkAlwaysAtMostOne(true, true, true, true, x(1), x(2), x(3));
        checkAlwaysAtMostOne(true, true, true, true, x(2), x(3));
        checkAlwaysAtMostOne(true, true, false, false, x(1), x(1, 2));
    }

    private void checkAlwaysExactlyOne(ProverFeatures features,
                                       boolean expected,
                                       String... alphas) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkNary(prover::alwaysExactlyOne, " alwaysExactlyOne(" + features + ")", expected, alphas);
    }

    /**
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     * @param alphas The expressions.
     */
    private void checkAlwaysExactlyOne(boolean expectedInNo,
                                       boolean expectedInAll,
                                       boolean expectedExNo,
                                       boolean expectedExAll,
                                       String... alphas) {
        checkAlwaysExactlyOne(IN_NO, expectedInNo, alphas);
        checkAlwaysExactlyOne(IN_ALL, expectedInAll, alphas);
        checkAlwaysExactlyOne(EX_NO, expectedExNo, alphas);
        checkAlwaysExactlyOne(EX_ALL, expectedExAll, alphas);
    }

    @Test
    void testAlwaysExactlyOne() {
        checkAlwaysExactlyOne(false, false, false, false, x(1));
        checkAlwaysExactlyOne(true, false, true, false, x(1), x(2), x(3));
        checkAlwaysExactlyOne(true, false, false, false, x(2), x(3));
        checkAlwaysExactlyOne(true, false, false, false, x(2, 3));
        checkAlwaysExactlyOne(true, false, false, false, x(1), x(1, 2), x(3));
    }

    private void checkAlwaysAtLeastOneInContext(ProverFeatures features,
                                                boolean expected,
                                                String context,
                                                String... alphas) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkContextNary(prover::alwaysAtLeastOneInContext,
                         " alwaysAtLeastOneInContext(" + features + ")",
                         expected,
                         context,
                         alphas);
    }

    /**
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     * @param context The context expression.
     * @param alphas The expressions.
     */
    private void checkAlwaysAtLeastOneInContext(boolean expectedInNo,
                                                boolean expectedInAll,
                                                boolean expectedExNo,
                                                boolean expectedExAll,
                                                String context,
                                                String... alphas) {
        checkAlwaysAtLeastOneInContext(IN_NO, expectedInNo, context, alphas);
        checkAlwaysAtLeastOneInContext(IN_ALL, expectedInAll, context, alphas);
        checkAlwaysAtLeastOneInContext(EX_NO, expectedExNo, context, alphas);
        checkAlwaysAtLeastOneInContext(EX_ALL, expectedExAll, context, alphas);
    }

    @Test
    void testAlwaysAtLeastOneInContext() {
        checkAlwaysAtLeastOneInContext(false, false, true, true, x(1), x(1));
        checkAlwaysAtLeastOneInContext(true, true, true, true, x(2), x(2));
        checkAlwaysAtLeastOneInContext(true, true, false, false, x(1, 2), x(2));
        checkAlwaysAtLeastOneInContext(false, false, false, false, x(), x(1));
        checkAlwaysAtLeastOneInContext(true, true, true, true, x(1, 2, 3), x(1), x(2), x(3));
        checkAlwaysAtLeastOneInContext(true, true, false, false, x(1, 2, 3), x(2), x(3));
    }

    private void checkAlwaysAtMostOneInContext(ProverFeatures features,
                                               boolean expected,
                                               String context,
                                               String... alphas) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkContextNary(prover::alwaysAtMostOneInContext,
                         " alwaysAtMostOneInContext(" + features + ")",
                         expected,
                         context,
                         alphas);
    }

    /**
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     * @param context The context expression.
     * @param alphas The expressions.
     */
    private void checkAlwaysAtMostOneInContext(boolean expectedInNo,
                                               boolean expectedInAll,
                                               boolean expectedExNo,
                                               boolean expectedExAll,
                                               String context,
                                               String... alphas) {
        checkAlwaysAtMostOneInContext(IN_NO, expectedInNo, context, alphas);
        checkAlwaysAtMostOneInContext(IN_ALL, expectedInAll, context, alphas);
        checkAlwaysAtMostOneInContext(EX_NO, expectedExNo, context, alphas);
        checkAlwaysAtMostOneInContext(EX_ALL, expectedExAll, context, alphas);
    }

    @Test
    void testAlwaysAtMostOneInContext() {
        checkAlwaysAtMostOneInContext(true, true, true, true, x(1), x(1));
        checkAlwaysAtMostOneInContext(true, true, true, true, x(1), x(2));
        checkAlwaysAtMostOneInContext(true, true, true, true, x(2), x(2));
        checkAlwaysAtMostOneInContext(true, true, true, true, x(1, 2), x(2));
        checkAlwaysAtMostOneInContext(true, true, true, true, x(), x(1));
        checkAlwaysAtMostOneInContext(true, true, true, true, x(), x());
        checkAlwaysAtMostOneInContext(true, true, true, true, x(), x(), x());
        checkAlwaysAtMostOneInContext(true, true, true, true, x(1, 2, 3), x(1), x(2), x(3));
        checkAlwaysAtMostOneInContext(true, true, true, true, x(1, 2, 3), x(2), x(3));
        checkAlwaysAtMostOneInContext(true, true, false, false, x(1, 2, 3), x(1), x(1, 2));
        checkAlwaysAtMostOneInContext(true, true, false, false, x(1, 2, 3), x(1), x(1, 2), x(), x());
    }

    private void checkAlwaysExactlyInContext(ProverFeatures features,
                                             boolean expected,
                                             String context,
                                             String... alphas) {
        final Prover prover = new ProverImpl(registryHandle, features);
        checkContextNary(prover::alwaysExactlyOneInContext,
                         " alwaysExactlyOneInContext(" + features + ")",
                         expected,
                         context,
                         alphas);
    }

    /**
     * @param expectedInNo The expected result for include assertions and no reserves.
     * @param expectedInAll The expected result for include assertions and all possible reserves.
     * @param expectedExNo The expected result for exclude assertions and no reserves.
     * @param expectedExAll The expected result for exclude assertions and all possible reserves.
     * @param context The context expression.
     * @param alphas The expressions.
     */
    private void checkAlwaysExactlyInContext(boolean expectedInNo,
                                             boolean expectedInAll,
                                             boolean expectedExNo,
                                             boolean expectedExAll,
                                             String context,
                                             String... alphas) {
        checkAlwaysExactlyInContext(IN_NO, expectedInNo, context, alphas);
        checkAlwaysExactlyInContext(IN_ALL, expectedInAll, context, alphas);
        checkAlwaysExactlyInContext(EX_NO, expectedExNo, context, alphas);
        checkAlwaysExactlyInContext(EX_ALL, expectedExAll, context, alphas);
    }

    @Test
    void testAlwaysExactlyOneInContext() {
        checkAlwaysExactlyInContext(false, false, true, true, x(1), x(1));
        checkAlwaysExactlyInContext(false, false, false, false, x(1), x(2));
        checkAlwaysExactlyInContext(true, true, true, true, x(2), x(2));
        checkAlwaysExactlyInContext(true, true, false, false, x(1, 2), x(2));
        checkAlwaysExactlyInContext(false, false, false, false, x(), x(1));
        checkAlwaysExactlyInContext(false, false, false, false, x(), x());
        checkAlwaysExactlyInContext(false, false, false, false, x(), x(), x());
        checkAlwaysExactlyInContext(true, true, true, true, x(1, 2, 3), x(1), x(2), x(3));
        checkAlwaysExactlyInContext(true, true, false, false, x(1, 2, 3), x(2), x(3));
        checkAlwaysExactlyInContext(false, false, false, false, x(1, 2, 3), x(1), x(1, 2));
    }

    @Test
    void testMisc() {
        final Prover prover = new ProverImpl(registryHandle, ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES);
        assertEquals(registryHandle, prover.getDictionaryHandle());
        assertEquals(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES, prover.getFeatures());

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         final Expression x = new Expression(x(1));
                         final Node n = x.getRootNode();
                         final Node nodes[] = { n, n, n, n, n, n };
                         prover.isAlwaysTrue(Encoders.atMostOneNonNegatable(Arrays.asList(nodes), "C"));
                     });
    }
}