package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.proofs.core.clauses.Sentence;
import cdc.applic.proofs.core.clauses.SentenceBuilder;

class SentenceBuilderTest {
    private static final Logger LOGGER = LogManager.getLogger(SentenceBuilderTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();
    private final RepositorySupport support = new RepositorySupport();

    private void checkConvert(String expression) {
        final Expression x = new Expression(expression);
        final Sentence sentence = SentenceBuilder.convert(support.registry,
                                                          ConvertToNary.execute(x.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY),
                                                          "C");
        LOGGER.debug(expression + ":");
        sentence.print(OUT, 1);
    }

    @Test
    void testConvert() {
        checkConvert("true");
        checkConvert("false");
        checkConvert("I = 10");
        checkConvert("I = 10 and R = 10.0");
        checkConvert("I = 10 or R = 10.0");
        checkConvert("I = 10 and R = 10.0 and E = E1");
        checkConvert("I = 10 or R = 10.0 or E = E1");
        checkConvert("(I = 10 or R = 10.0) and (E = E1 or B)");
        checkConvert("(I = 10 and R = 10.0) or (E = E1 and B)");
    }

    private static void checkConvertPrepared(String expression) {
        final Expression x = new Expression(expression);
        SentenceBuilder.convertPrepared(ConvertToNary.execute(x.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY));
    }

    @Test
    void testConvertPrepared() {
        checkConvertPrepared("true");
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checkConvertPrepared("I = 10");
                     });
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checkConvertPrepared("not (not (I = 10))");
                     });
    }
}