package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.EnumeratedTypeImpl;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.literals.Name;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.clauses.ProverImpl;
import cdc.applic.proofs.core.encoding.BinaryEncoder;
import cdc.applic.proofs.core.encoding.BinomialEncoder;
import cdc.applic.proofs.core.encoding.CommanderEncoder;
import cdc.applic.proofs.core.encoding.Encoder;
import cdc.util.lang.UnexpectedValueException;

class EncoderSatSystemSolverTest {
    private static final Logger LOGGER = LogManager.getLogger(EncoderSatSystemSolverTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    private enum Algorithm {
        AT_LEAST_ONE,
        AT_MOST_ONE,
        EXACTLY_ONE
    }

    private static String toBinary(int value,
                                   int size) {
        return String.format("%" + size + "s", Integer.toBinaryString(value)).replaceAll(" ", "0");
    }

    private static void check(int size,
                              Encoder encoder,
                              Algorithm algorithm) {
        LOGGER.debug("==========================================================");
        LOGGER.debug("{} {} {}", size, encoder.getClass().getSimpleName(), algorithm);
        final int max = 1 << size;
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("registry").build();
        final DictionaryHandle handle = new DictionaryHandle(registry);
        final Prover prover = new ProverImpl(handle, ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES);
        // repository.createIntegerType("T", false, "1~" + size);
        final EnumeratedTypeImpl type = registry.enumeratedType().name("T").frozen(false).build();
        for (int index = 0; index < size; index++) {
            type.addValue("E" + (index + 1), index);
        }
        registry.property().name("P").type("T").build();

        final Name name = Name.of("P");

        for (int index = max - 1; index < max; index++) {
            final List<Node> nodes = new ArrayList<>();
            int bits = 0;
            for (int bit = 0; bit < size; bit++) {
                final int mask = 1 << bit;
                if ((index & mask) != 0) {
                    nodes.add(new EqualNode(name, StringValue.of("E" + (bit + 1))));
                    bits++;
                }
            }
            final String prefix = "XXX";

            final Node node;
            final boolean expected;
            switch (algorithm) {
            case AT_LEAST_ONE:
                node = nodes.isEmpty() ? FalseNode.INSTANCE : Encoder.atLeastOne(nodes);
                expected = bits == size;
                break;
            case AT_MOST_ONE:
                node = nodes.isEmpty() ? FalseNode.INSTANCE : encoder.atMostOne(nodes, prefix);
                expected = !nodes.isEmpty();
                break;
            case EXACTLY_ONE:
                node = nodes.isEmpty() ? FalseNode.INSTANCE : encoder.exactlyOne(nodes, prefix);
                expected = bits == size;
                break;
            default:
                throw new UnexpectedValueException(algorithm);
            }

            // final boolean result = prover.isAlwaysTrue(node.toExpression(SymbolType.SHORT, Spacing.NARROW));
            final boolean result = prover.isAlwaysTrue(node);
            final boolean sometimes = prover.isSometimesTrue(node);
            final boolean sometimesneg = prover.isSometimesTrue(new NotNode(node));
            LOGGER.debug("   {} {} {} {}", toBinary(index, size), algorithm, result, node);
            LOGGER.debug("   sometimes: {}", sometimes);
            LOGGER.debug("   sometimes neg: {}", sometimesneg);

            LOGGER.debug("      nodes: {}", nodes);
            node.print(OUT, 2);
            assertEquals(expected, result, algorithm + " " + index + "/" + size);
        }
    }

    private static void checkBinomial(Algorithm algorithm) {
        for (int size = 4; size < 5; size++) {
            check(size, BinomialEncoder.INSTANCE, algorithm);
        }
    }

    private static void checkCommander(Algorithm algorithm) {
        for (int size = 3; size < 5; size++) {
            check(size, CommanderEncoder.INSTANCE, algorithm);
        }
    }

    private static void checkBinary(Algorithm algorithm) {
        for (int size = 4; size < 5; size++) {
            check(size, BinaryEncoder.INSTANCE, algorithm);
        }
    }

    @Test
    void testBinomialAtLeastOne() {
        checkBinomial(Algorithm.AT_LEAST_ONE);
    }

    @Test
    void testBinomialAtMostOne() {
        checkBinomial(Algorithm.AT_MOST_ONE);
    }

    @Test
    void testBinomialExactlyOne() {
        checkBinomial(Algorithm.EXACTLY_ONE);
    }

    @Test
    void testCommanderAtLeastOne() {
        checkCommander(Algorithm.AT_LEAST_ONE);
    }

    @Test
    void testCommanderAtMostOne() {
        // This uses Atom Var nodes which are not supported
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checkCommander(Algorithm.AT_MOST_ONE);
                     });
    }

    @Test
    void testCommanderExactlyOne() {
        // This uses Atom Var nodes which are not supported
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checkCommander(Algorithm.EXACTLY_ONE);
                     });
    }

    @Test
    void testBinaryAtLeastOne() {
        checkBinary(Algorithm.AT_LEAST_ONE);
    }

    @Test
    void testBinaryAtMostOne() {
        // This uses Atom Var nodes which are not supported
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checkBinary(Algorithm.AT_MOST_ONE);
                     });
    }

    @Test
    void testBinaryExactlyOne() {
        // This uses Atom Var nodes which are not supported
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         checkBinary(Algorithm.EXACTLY_ONE);
                     });
    }
}