package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.proofs.core.ast.AtomVarNode;
import cdc.applic.proofs.core.encoding.BinomialEncoder;
import cdc.applic.proofs.core.encoding.Encoder;

class BinomialEncoderTest {
    private static List<Node> build(Object... literals) {
        final List<Node> result = new ArrayList<>();
        for (final Object literal : literals) {
            if (literal instanceof Integer) {
                final int value = ((Integer) literal).intValue();
                if (value < 0) {
                    result.add(new NotNode(new AtomVarNode("V" + (-value), false)));
                } else {
                    result.add(new AtomVarNode("V" + value, false));
                }
            } else {
                result.add((Node) literal);
            }
        }
        return result;
    }

    private static void checkAtLeastOne(String expected,
                                        Object... literals) {
        final List<Node> tmp = build(literals);
        final Node node = Encoder.atLeastOne(tmp);
        assertEquals(expected, node.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testAtLeastOne() {
        checkAtLeastOne("false", FalseNode.INSTANCE);
        checkAtLeastOne("true", TrueNode.INSTANCE);
        checkAtLeastOne("!false", new NotNode(FalseNode.INSTANCE));
        checkAtLeastOne("!true", new NotNode(TrueNode.INSTANCE));
        checkAtLeastOne("V1", 1);
        checkAtLeastOne("!V1", -1);
        checkAtLeastOne("V1|V2", 1, 2);
        checkAtLeastOne("V1|V2|V3", 1, 2, 3);
    }

    private static void checkAtMostOne(String expected,
                                       Object... literals) {
        final List<Node> tmp = build(literals);
        final Node node = BinomialEncoder.atMostOne(tmp);
        assertEquals(expected, node.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testAtMostOne() {
        checkAtMostOne("true", FalseNode.INSTANCE);
        checkAtMostOne("true", TrueNode.INSTANCE);
        checkAtMostOne("true", 1);
        checkAtMostOne("true", -1);
        checkAtMostOne("!V1|!V2", 1, 2);
        checkAtMostOne("V1|V2", -1, -2);
        checkAtMostOne("(!V1|!V2)&(!V1|!V3)&(!V2|!V3)", 1, 2, 3);
        checkAtMostOne("(!V1|!V2)&(!V1|!V3)&(!V1|!V4)&(!V2|!V3)&(!V2|!V4)&(!V3|!V4)", 1, 2, 3, 4);
    }

    private static void checkExactlyOne(String expected,
                                        Object... literals) {
        final List<Node> tmp = build(literals);
        final Node node = BinomialEncoder.exactlyOne(tmp);
        assertEquals(expected, node.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testExactlyOne() {
        checkExactlyOne("V1", 1);
        checkExactlyOne("(V1|V2)&(!V1|!V2)", 1, 2);
        checkExactlyOne("(V1|V2|V3)&(!V1|!V2)&(!V1|!V3)&(!V2|!V3)", 1, 2, 3);
    }
}