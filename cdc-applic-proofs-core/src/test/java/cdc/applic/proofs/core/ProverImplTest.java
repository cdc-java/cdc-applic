package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.clauses.ProverImpl;

class ProverImplTest {
    private static final Logger LOGGER = LogManager.getLogger(ProverImplTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    private final RepositorySupport support = new RepositorySupport();

    private static Expression x(String s) {
        return new Expression(s);
    }

    @Test
    void testInvalidExpression() {
        final Prover prover = new ProverImpl(support.registryHandle, ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES);
        assertThrows(ApplicException.class,
                     () -> {
                         prover.isSometimesTrue(x("Rank=10"));
                     });
        assertThrows(ApplicException.class,
                     () -> {
                         prover.isAlwaysTrue(x("Rank=10"));
                     });
        assertThrows(ApplicException.class,
                     () -> {
                         prover.isNeverTrue(x("Rank=10"));
                     });
        assertThrows(ApplicException.class,
                     () -> {
                         prover.getMatching(x("Rank=10"));
                     });
    }

    @Test
    void testXor() {
        final Prover prover = new ProverImpl(support.registryHandle, ProverFeatures.EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES);
        assertTrue(prover.areAlwaysEquivalent(x("not(I=10 iff B)"), x("I=10 xor B")));
    }

    @Test
    void testAssertionsNoReserves() {
        final Prover prover = new ProverImpl(support.registryHandle, ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES);
        assertTrue(prover.isSometimesTrue(x("P!=AA")));
        assertTrue(prover.isSometimesTrue(x("I=10")));
        assertTrue(prover.isSometimesTrue(x("I<:{1~999}")));
        assertTrue(prover.isAlwaysTrue(x("I<:{1~999}")));
    }

    @Test
    void testAssertionsAllPossibleReserves() {
        final Prover prover = new ProverImpl(support.registryHandle, ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES);
        assertTrue(prover.isSometimesTrue(x("P!=AA")));

        assertTrue(prover.isSometimesTrue(x("E=E1")));
        assertFalse(prover.isNeverTrue(x("E=E1")));
        assertFalse(prover.isAlwaysTrue(x("E=E1")));

        assertTrue(prover.isSometimesTrue(x("E<:{E1,E2,E3}")));
        assertFalse(prover.isNeverTrue(x("E<:{E1,E2,E3}")));
        assertFalse(prover.isAlwaysTrue(x("E<:{E1,E2,E3}")));

        assertTrue(prover.isSometimesTrue(x("B")));
        assertFalse(prover.isNeverTrue(x("B")));
        assertFalse(prover.isAlwaysTrue(x("B")));
        assertTrue(prover.isAlwaysTrue(x("B|!B")));

        assertTrue(prover.isSometimesTrue(x("I=10")));
        assertTrue(prover.isSometimesTrue(x("I<:{1~999}")));
        assertFalse(prover.isAlwaysTrue(x("I<:{1~999}")));
    }

    @Test
    void testRef() {
        final RepositorySupport support = new RepositorySupport();
        support.registry.alias().name("A").expression("I=10").build();
        final Prover prover = new ProverImpl(support.registryHandle,
                                             ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES);
        assertTrue(prover.isAlwaysTrue(new Expression("I=10 -> A")));
        assertTrue(prover.isAlwaysTrue(new Expression("A -> I=10")));
        assertTrue(prover.isAlwaysTrue(new Expression("A <-> I=10")));
    }

    @Test
    void testInequalities() {
        final Prover prover = new ProverImpl(support.registryHandle, ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES);
        assertTrue(prover.isSometimesTrue(x("E>=E1")));
        assertTrue(prover.isSometimesTrue(x("E<=E1")));
        assertTrue(prover.isSometimesTrue(x("E!>=E1")));
        assertTrue(prover.isSometimesTrue(x("E!<=E1")));
        assertFalse(prover.isSometimesTrue(x("E>E1")));
        assertFalse(prover.isSometimesTrue(x("E<E1")));
        assertTrue(prover.isSometimesTrue(x("E!>E1")));
        assertTrue(prover.isSometimesTrue(x("E!<E1")));
    }

    @Test
    void testPrefix() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("r").prefix("R").build();
        final DictionaryHandle handle = new DictionaryHandle(registry);
        registry.booleanType().name("Boolean").build();
        registry.property().name("B1").type("Boolean").ordinal(0).build();
        registry.property().name("B2").type("Boolean").ordinal(0).build();
        registry.createAssertion("B1 <-> !B2");

        final Prover prover = new ProverImpl(handle,
                                             ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES);
        assertTrue(prover.isSometimesTrue(x("B1")));
        assertTrue(prover.isSometimesTrue(x("B2")));
        assertFalse(prover.isSometimesTrue(x("B1 & B2")));
        assertTrue(prover.isSometimesTrue(x("R.B1")));
        assertTrue(prover.isSometimesTrue(x("R.B2")));
        handle.print(OUT);
        assertFalse(prover.isSometimesTrue(x("B1 & R.B2")));
        assertFalse(prover.isSometimesTrue(x("R.B1 & B2")));
        assertFalse(prover.isSometimesTrue(x("R.B1 & R.B2")));
    }
}