package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositoryXYZSupport;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.ProverMatching;
import cdc.applic.proofs.core.clauses.ProverImpl;

class ProverGetProjectedMatchingXYZTest extends RepositoryXYZSupport {
    private static final Logger LOGGER = LogManager.getLogger(ProverGetProjectedMatchingXYZTest.class);

    private void check(ProverFeatures proverFeatures,
                       ProverMatching expected,
                       String expression,
                       String on,
                       String message) {
        LOGGER.debug("test({}, {}, {}, {})", proverFeatures, expected, expression, on);
        final Prover prover = new ProverImpl(registryHandle, proverFeatures);
        final ProverMatching matching = prover.getProjectedMatching(new Expression(expression), new Expression(on));

        assertEquals(expected, matching, message + "getProjectedMatching(" + proverFeatures + ") (" + expression + ", " + on + ")");
    }

    private void check(ProverMatching expectedExcludeAssertions,
                       ProverMatching expectedIncludeAssertions,
                       String expression,
                       String on) {
        LOGGER.debug("==========================================");
        LOGGER.debug("test({}, {})", expression, on);
        check(ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES,
              expectedExcludeAssertions,
              expression,
              on,
              "Without Assertions");
        check(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES,
              expectedIncludeAssertions,
              expression,
              on,
              "With Assertions");
    }

    @Test
    void testFalseOnX() {
        check(ProverMatching.NEVER, ProverMatching.NEVER, "false", x(1));
        check(ProverMatching.NEVER, ProverMatching.NEVER, "false", x(2));
        check(ProverMatching.NEVER, ProverMatching.NEVER, "false", x(1, 2));
    }

    @Test
    void testTrueOnX() {
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, "true", x(1));
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, "true", x(2));
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, "true", x(1, 2));
    }

    @Test
    void testXOnX() {
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, x(1), x(1));
        check(ProverMatching.NEVER, ProverMatching.NEVER, x(2), x(1));
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, x(2), x(2));
        check(ProverMatching.NEVER, ProverMatching.NEVER, x(1), x(2));
    }

    @Test
    void testXOnZ() {
        check(ProverMatching.SOMETIMES, ProverMatching.NEVER, x(1), z(1));
        check(ProverMatching.SOMETIMES, ProverMatching.SOMETIMES, x(1), z(2));
        check(ProverMatching.SOMETIMES, ProverMatching.ALWAYS, x(2), z(1));
        check(ProverMatching.SOMETIMES, ProverMatching.SOMETIMES, x(2), z(2));

        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, x(1, 2), z(1));
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, x(1, 2), z(2));
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, x(1, 2), z(1, 2));
    }

    @Test
    void testZOnX() {
        check(ProverMatching.SOMETIMES, ProverMatching.NEVER, z(1), x(1));
        check(ProverMatching.SOMETIMES, ProverMatching.SOMETIMES, z(1), x(2));
        check(ProverMatching.SOMETIMES, ProverMatching.ALWAYS, z(2), x(1));
        check(ProverMatching.SOMETIMES, ProverMatching.SOMETIMES, z(2), x(2));

        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, z(1, 2), x(1));
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, z(1, 2), x(2));
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, z(1, 2), x(1, 2));
    }

    @Test
    void testYOnZ() {
        check(ProverMatching.SOMETIMES, ProverMatching.NEVER, y(1), z(1));
        check(ProverMatching.SOMETIMES, ProverMatching.SOMETIMES, y(1), z(2));
        check(ProverMatching.SOMETIMES, ProverMatching.ALWAYS, y(2), z(1));
        check(ProverMatching.SOMETIMES, ProverMatching.SOMETIMES, y(2), z(2));

        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, y(1, 2), z(1));
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, y(1, 2), z(2));
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, y(1, 2), z(1, 2));
    }

    @Test
    void testZOnY() {
        check(ProverMatching.SOMETIMES, ProverMatching.NEVER, z(1), y(1));
        check(ProverMatching.SOMETIMES, ProverMatching.SOMETIMES, z(1), y(2));
        check(ProverMatching.SOMETIMES, ProverMatching.ALWAYS, z(2), y(1));
        check(ProverMatching.SOMETIMES, ProverMatching.SOMETIMES, z(2), y(2));

        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, z(1, 2), y(1));
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, z(1, 2), y(2));
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, z(1, 2), y(1, 2));
    }

    @Test
    void testXOnY() {
        check(ProverMatching.SOMETIMES, ProverMatching.NEVER, x(1), y(1));
        check(ProverMatching.SOMETIMES, ProverMatching.SOMETIMES, x(1), y(2));
        check(ProverMatching.SOMETIMES, ProverMatching.ALWAYS, x(2), y(1));
        check(ProverMatching.SOMETIMES, ProverMatching.SOMETIMES, x(2), y(2));

        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, x(1, 2), y(1));
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, x(1, 2), y(2));
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, x(1, 2), y(1, 2));
    }

    @Test
    void testYOnX() {
        check(ProverMatching.SOMETIMES, ProverMatching.NEVER, y(1), x(1));
        check(ProverMatching.SOMETIMES, ProverMatching.SOMETIMES, y(1), x(2));
        check(ProverMatching.SOMETIMES, ProverMatching.ALWAYS, y(2), x(1));
        check(ProverMatching.SOMETIMES, ProverMatching.SOMETIMES, y(2), x(2));

        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, y(1, 2), x(1));
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, y(1, 2), x(2));
        check(ProverMatching.ALWAYS, ProverMatching.ALWAYS, y(1, 2), x(1, 2));
    }
}