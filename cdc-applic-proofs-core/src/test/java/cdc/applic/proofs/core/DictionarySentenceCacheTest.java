package cdc.applic.proofs.core;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.proofs.core.clauses.DictionarySentencesCache;

class DictionarySentenceCacheTest {
    private static final Logger LOGGER = LogManager.getLogger(DictionarySentenceCacheTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    @Test
    void testNP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        registry.booleanType().name("B").build();
        registry.enumeratedType().name("E").frozen(false).literals("E1", "E2").build();
        registry.integerType().name("I").frozen(false).domain("1~99").build();
        registry.realType().name("R").frozen(false).domain("1.0~99.0").build();
        registry.patternType().name("P").frozen(false).pattern("[A-Z]{2}").build();

        final DictionaryHandle handle = new DictionaryHandle(registry);
        handle.print(OUT);
        final DictionarySentencesCache cache = DictionarySentencesCache.get(handle);
        handle.print(OUT);
        cache.validate();
        handle.print(OUT);

        registry.property().name("B").type("B").ordinal(0).build();
        registry.property().name("E").type("E").ordinal(1).build();
        registry.property().name("I").type("I").ordinal(2).build();
        registry.property().name("R").type("R").ordinal(3).build();
        registry.property().name("P").type("P").ordinal(4).build();

        registry.alias().name("X").expression("E = E1 or B").ordinal(0).build();
        registry.alias().name("Y").expression("I in {1~10}").ordinal(0).build();
        registry.alias().name("Z").expression("E = E1").ordinal(0).build();
        registry.createAssertion("X");

        handle.invalidate();
        cache.validate();
        handle.print(OUT);
    }

    @Test
    void testP() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").prefix("R").build();
        registry.booleanType().name("B").build();
        registry.enumeratedType().name("E").frozen(false).literals("E1", "E2").build();
        registry.integerType().name("I").frozen(false).domain("1~99").build();
        registry.realType().name("R").frozen(false).domain("1.0~99.0").build();
        registry.patternType().name("P").frozen(false).pattern("[A-Z]{2}").build();

        final DictionaryHandle handle = new DictionaryHandle(registry);
        handle.print(OUT);
        final DictionarySentencesCache cache = DictionarySentencesCache.get(handle);
        handle.print(OUT);
        cache.validate();
        handle.print(OUT);

        registry.property().name("B").type("B").ordinal(0).build();
        registry.property().name("E").type("E").ordinal(1).build();
        registry.property().name("I").type("I").ordinal(2).build();
        registry.property().name("R").type("R").ordinal(3).build();
        registry.property().name("P").type("P").ordinal(4).build();

        registry.alias().name("X").expression("E = E1 or B").ordinal(0).build();
        registry.alias().name("Y").expression("I in {1~10}").ordinal(0).build();
        registry.alias().name("Z").expression("E = E1").ordinal(0).build();
        registry.createAssertion("X");

        handle.invalidate();
        cache.validate();
        handle.print(OUT);
    }
}