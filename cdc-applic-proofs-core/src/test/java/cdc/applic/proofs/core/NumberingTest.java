package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.proofs.core.clauses.Numbering;

class NumberingTest {
    private static final Logger LOGGER = LogManager.getLogger(NumberingTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    @Test
    void test() {
        final Numbering numbering = new Numbering();
        assertEquals(0, numbering.getSize());
        numbering.computeIfAbsent(FalseNode.INSTANCE);
        assertEquals(1, numbering.getSize());
        assertTrue(numbering.contains(FalseNode.INSTANCE));
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         numbering.getNumber(TrueNode.INSTANCE);
                     });

        numbering.clear();
        assertEquals(0, numbering.getSize());
        final int[][] numbers = { { 1, -1 } };
        Numbering.print(numbers, OUT, 0);
    }
}