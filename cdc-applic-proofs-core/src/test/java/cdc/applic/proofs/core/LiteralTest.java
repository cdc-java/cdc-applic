package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.proofs.core.ast.AtomVarNode;
import cdc.applic.proofs.core.clauses.Literal;

class LiteralTest {
    @Test
    void testTrue() {
        final Literal p = new Literal(TrueNode.INSTANCE, true);
        assertEquals(TrueNode.INSTANCE, p.getAtom());
        assertTrue(p.isPositive());
        assertFalse(p.isNegative());
        assertTrue(p.isAlwaysTrue());
        assertFalse(p.isAlwaysFalse());
        final Literal o = p.getOpposite();
        final Literal oo = o.getOpposite();
        assertEquals(p, oo);

        final Literal n = new Literal(TrueNode.INSTANCE, false);
        assertEquals(TrueNode.INSTANCE, n.getAtom());
        assertFalse(n.isPositive());
        assertTrue(n.isNegative());
        assertFalse(n.isAlwaysTrue());
        assertTrue(n.isAlwaysFalse());
    }

    @Test
    void testFalse() {
        final Literal p = new Literal(FalseNode.INSTANCE, true);
        assertEquals(FalseNode.INSTANCE, p.getAtom());
        assertTrue(p.isPositive());
        assertFalse(p.isNegative());
        assertFalse(p.isAlwaysTrue());
        assertTrue(p.isAlwaysFalse());

        final Literal n = new Literal(FalseNode.INSTANCE, false);
        assertEquals(FalseNode.INSTANCE, n.getAtom());
        assertFalse(n.isPositive());
        assertTrue(n.isNegative());
        assertTrue(n.isAlwaysTrue());
        assertFalse(n.isAlwaysFalse());
    }

    @Test
    void testVar() {
        final Literal p1 = new Literal(new AtomVarNode("V"), true);
        final Literal n1 = new Literal(new AtomVarNode("V"), false);
        assertTrue(p1.isPositive());
        assertFalse(p1.isNegative());
        assertFalse(p1.isAlwaysTrue());
        assertFalse(p1.isAlwaysFalse());

        final Literal p2 = new Literal(new AtomVarNode("V"), true);
        final Literal p3 = new Literal(new AtomVarNode("W"), true);
        final Literal n3 = new Literal(new AtomVarNode("W"), false);
        assertEquals(p1, p1);
        assertEquals(p1, p2);
        assertNotEquals(p1, null);
        assertNotEquals(p1, "Hello");
        assertNotEquals(p1, n1);
        assertNotEquals(p1, n3);
        assertNotEquals(p1, p3);

        assertEquals(p1.hashCode(), p1.hashCode());
        assertEquals(n1.hashCode(), n1.hashCode());
        assertNotEquals(p1.hashCode(), n1.hashCode());

        assertEquals("VAR(V)", p1.toString());
        assertEquals("NOT VAR(V)", n1.toString());
    }
}