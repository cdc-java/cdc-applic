package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.proofs.core.visitors.ConvertToAtoms;

class ConvertToAtomsTest {
    private final RepositorySupport support = new RepositorySupport();

    private void check(String expression,
                       String expected) {
        final Expression x1 = new Expression(expression);
        final Expression x2 = ConvertToAtoms.execute(ConvertToNary.execute(x1.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY),
                                                     support.registry)
                                            .toExpression(Formatting.SHORT_NARROW);
        assertEquals(expected, x2.toString());
    }

    @Test
    void testFalseTrue() {
        check("true", "true");
        check("not true", "!true");
        check("false", "false");
        check("not false", "!false");
    }

    @Test
    void testBooleanType() {
        check("B", "B");
        check("!B", "!B");
        check("B=true", "B");
        check("B=false", "!B");
        check("B in {}", "false");
        check("B in {false}", "!B");
        check("B in {true}", "B");
        check("B in {false, true}", "true");
        check("!(B in {true})", "!B");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         check("B = A", null);
                     });

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         check("B in {A}", null);
                     });
    }

    @Test
    void testEnumeratedType() {
        check("E = E1", "E@E1");
        check("E != E1", "!E@E1");
        check("E = E1 or E = E2", "E@E1|E@E2");
        check("E = E1 and E = E2", "E@E1&E@E2");
        check("E in {}", "false");
        check("E in {E1,E2}", "E@E1|E@E2");
        check("E not in {E1,E2}", "!(E@E1|E@E2)");
        check("E in {E1,E2,E3}", "E@E1|E@E2|E@E3");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         check("E = E4", null);
                     });
    }

    @Test
    void testIntegerType() {
        check("I = 10", "I@10~10");
        check("I != 10", "!I@10~10");
        check("I in {}", "false");
        check("!(I in {})", "!false");
        check("I in {10~11}", "I@10~11");
        check("I in {10~11,10~12}", "I@10~12");
        check("I in {10~9}", "false");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         check("I = -10", null);
                     });
    }

    @Test
    void testRealType() {
        check("R = 10.0", "R@10.0~10.0");
        check("R != 10.0", "!R@10.0~10.0");
        check("R in {}", "false");
        check("!(R in {})", "!false");
        check("R in {10.0~11.0}", "R@10.0~11.0");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         check("R = -10.0", null);
                     });
    }

    @Test
    void testPatternType() {
        check("P = AA", "P@AA");
        check("P = AA or P = BB", "P@AA|P@BB");
        check("P != AA", "!P@AA");
        check("P in {AA}", "P@AA");
        check("P in {AA, BB}", "P@AA|P@BB");
        check("P not in {AA, BB}", "!(P@AA|P@BB)");

        assertThrows(IllegalArgumentException.class,
                     () -> {
                         check("P = A", null);
                     });
    }
}