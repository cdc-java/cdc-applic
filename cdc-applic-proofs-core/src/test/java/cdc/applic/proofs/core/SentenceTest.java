package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.proofs.core.clauses.Sentence;
import cdc.applic.proofs.core.clauses.SentenceBuilder;

class SentenceTest {
    private final RepositorySupport support = new RepositorySupport();

    private Sentence s(String expression) {
        final Expression x = new Expression(expression);
        final Sentence sentence = SentenceBuilder.convert(support.registry,
                                                          ConvertToNary.execute(x.getRootNode(), ConvertToNary.Variant.WHEN_NECESSARY),
                                                          "C");

        return sentence;
    }

    @Test
    void testEquals() {
        final Sentence s1a = s("E = E1");
        final Sentence s1b = s("E = E1");
        final Sentence s2 = s("E = E2");

        assertEquals(s1a, s1a);
        assertEquals(s1a, s1b);
        assertNotEquals(s1a, null);
        assertNotEquals(s1a, s2);
        assertNotEquals(s1a, "Hello");
    }
}