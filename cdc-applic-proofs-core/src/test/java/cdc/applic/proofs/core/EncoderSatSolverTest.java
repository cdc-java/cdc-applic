package cdc.applic.proofs.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.visitors.EliminateTrueFalse;
import cdc.applic.proofs.core.clauses.Numbering;
import cdc.applic.proofs.core.clauses.Sentence;
import cdc.applic.proofs.core.clauses.SentenceBuilder;
import cdc.applic.proofs.core.encoding.BinaryEncoder;
import cdc.applic.proofs.core.encoding.BinomialEncoder;
import cdc.applic.proofs.core.encoding.CommanderEncoder;
import cdc.applic.proofs.core.encoding.Encoder;
import cdc.applic.proofs.core.encoding.HeuleEncoder;
import cdc.applic.proofs.core.sat.SatSolver;
import cdc.applic.proofs.core.sat.SatSolverFactory;

/**
 * Compares Satisfiability results of encoders to Binomial encoder.
 *
 * @author Damien Carbonne
 */
class EncoderSatSolverTest {
    protected static final Logger LOGGER = LogManager.getLogger(EncoderSatSolverTest.class);
    protected static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();
    protected static final PrintStream ERR = IoBuilder.forLogger(LOGGER).setLevel(Level.ERROR).buildPrintStream();

    private enum Algorithm {
        AT_MOST_ONE,
        EXACTLY_ONE
    }

    private static class Checker {
        private final int universeSize;
        private final Encoder encoder;
        protected final Algorithm algorithm;
        private final int max;
        protected final List<RefNode> atoms = new ArrayList<>();
        protected final Node definitionNode;
        protected final Sentence definitionSentence;
        private final SatSolverFactory factory = new SatSolverFactory();
        protected final SatSolver solver = factory.newInstance();
        protected final List<List<Node>> sets = new ArrayList<>();
        protected final List<Node> heads = new ArrayList<>();
        private final RepositorySupport support = new RepositorySupport();

        public Checker(int universeSize,
                       Encoder encoder,
                       Algorithm algorithm) {
            this.universeSize = universeSize;
            this.encoder = encoder;
            this.algorithm = algorithm;
            this.max = 1 << universeSize;
            for (int index = 0; index < universeSize; index++) {
                atoms.add(new RefNode("E" + index));
            }
            this.definitionNode = BinomialEncoder.INSTANCE.exactlyOne(atoms, "C");
            this.definitionSentence = SentenceBuilder.convertPrepared(definitionNode);

            LOGGER.debug("=====================================================");
            LOGGER.debug(algorithm);
            LOGGER.debug("   " + encoder.getClass().getSimpleName());
            LOGGER.debug("   Universe Size: " + universeSize);
            LOGGER.debug("   Definition node: " + definitionNode.toInfix(Formatting.SHORT_NARROW));
            LOGGER.debug("   Definition sentence:");
            definitionSentence.print(OUT, 2);

            for (int index = 0; index < max; index++) {
                final List<Node> set = toAtoms(index);
                sets.add(set);
                heads.add(set.isEmpty() ? FalseNode.INSTANCE : NaryOrNode.createSimplestOr(set));
            }

            LOGGER.debug("   Sets:");
            for (int index = 0; index < max; index++) {
                LOGGER.debug("      " + index + " " + sets.get(index));
            }
        }

        private List<Node> toAtoms(int index) {
            final List<Node> nodes = new ArrayList<>();
            for (int bit = 0; bit < universeSize; bit++) {
                final int mask = 1 << bit;
                if ((index & mask) != 0) {
                    nodes.add(atoms.get(bit));
                }
            }
            return nodes;
        }

        private class Data {
            private final Encoder encoder;
            private final Node formulaNode;
            private final Sentence posSentence;
            private final Sentence posFullSentence;
            private final Numbering numbering;
            private final int[][] numbers;
            private final boolean satisfiable;

            public Data(List<Integer> indices,
                        Encoder encoder) {
                this.encoder = encoder;
                this.formulaNode = EliminateTrueFalse.execute(toFormula(indices, encoder));
                this.posSentence = SentenceBuilder.convert(support.registry, formulaNode, "SET");
                this.posFullSentence = definitionSentence.merge(posSentence);
                this.numbering = new Numbering();
                this.numbers = numbering.computeNumbers(posFullSentence);
                this.satisfiable = solver.isSatisfiable(numbers);
            }

            private Node toFormula(List<Integer> indices,
                                   Encoder encoder) {
                final String prefix = "C";
                final List<Node> list = new ArrayList<>();
                for (final int index : indices) {
                    list.add(heads.get(index));
                }
                return switch (algorithm) {
                case AT_MOST_ONE -> list.isEmpty() ? FalseNode.INSTANCE : encoder.atMostOne(list, prefix);
                case EXACTLY_ONE -> list.isEmpty() ? FalseNode.INSTANCE : encoder.exactlyOne(list, prefix);
                };
            }

            public boolean isSatisfiable() {
                return satisfiable;
            }

            public void show() {
                LOGGER.error(encoder.getClass().getSimpleName());
                LOGGER.error("Definition: " + definitionNode.toInfix(Formatting.SHORT_NARROW));
                LOGGER.error("Definition Sentence:");
                definitionSentence.print(ERR, 1);
                LOGGER.error("Formula: " + formulaNode.toInfix(Formatting.SHORT_NARROW));
                LOGGER.error("Formula Sentence:");
                posSentence.print(ERR, 1);
                LOGGER.error("Formula Full Sentence:");
                posFullSentence.print(ERR, 1);
            }
        }

        public void executeRec(int numberOfSets,
                               int maxSetSize) {
            LOGGER.debug("=====================================================");
            LOGGER.debug("Number of sets: " + numberOfSets);
            LOGGER.debug("Max set size: " + maxSetSize);
            final List<Integer> indices = new ArrayList<>();
            executeRec(indices, numberOfSets, maxSetSize);
        }

        private void executeRec(List<Integer> indices,
                                int numberOfSets,
                                int maxSetSize) {
            if (indices.size() < numberOfSets) {
                for (int index = 0; index < max; index++) {
                    if (sets.get(index).size() <= maxSetSize) {
                        indices.add(index);
                        executeRec(indices, numberOfSets, maxSetSize);
                        indices.remove(indices.size() - 1);
                    }
                }
            } else {
                LOGGER.debug("--------------------------");
                LOGGER.debug("{} {}", algorithm, indices);

                for (final int index : indices) {
                    LOGGER.debug("  set: {}", heads.get(index).toInfix(Formatting.SHORT_NARROW));
                }

                final Data data = new Data(indices, encoder);
                final Data expected = new Data(indices, BinomialEncoder.INSTANCE);
                LOGGER.debug(" Satisfiable     : {}", data.isSatisfiable());
                LOGGER.debug(" Expected        : {}", expected.isSatisfiable());
                final boolean success = expected.isSatisfiable() == data.isSatisfiable();
                if (!success) {
                    LOGGER.error("FAILED");
                    data.show();
                }
                assertEquals(expected.isSatisfiable(), data.isSatisfiable());
            }
        }
    }

    private static final int UNIVERSE_MIN_SIZE = 1;
    private static final int UNIVERSE_MAX_SIZE = 3;
    private static final int NUMBER_OF_SETS = 6;
    private static final int MAX_SET_SIZE = 1;

    private static void checkCommander(Algorithm algorithm,
                                       int numberOfSets,
                                       int maxSetSize) {
        for (int universeSize = UNIVERSE_MIN_SIZE; universeSize <= UNIVERSE_MAX_SIZE; universeSize++) {
            final Checker checker = new Checker(universeSize, CommanderEncoder.INSTANCE, algorithm);
            checker.executeRec(numberOfSets, maxSetSize);
        }
    }

    private static void checkBinary(Algorithm algorithm,
                                    int numberOfSets,
                                    int maxSetSize) {
        for (int universeSize = UNIVERSE_MIN_SIZE; universeSize <= UNIVERSE_MAX_SIZE; universeSize++) {
            final Checker checker = new Checker(universeSize, BinaryEncoder.INSTANCE, algorithm);
            checker.executeRec(numberOfSets, maxSetSize);
        }
    }

    private static void checkHeule(Algorithm algorithm,
                                   int numberOfSets,
                                   int maxSetSize) {
        for (int universeSize = UNIVERSE_MIN_SIZE; universeSize <= UNIVERSE_MAX_SIZE; universeSize++) {
            final Checker checker = new Checker(universeSize, HeuleEncoder.INSTANCE, algorithm);
            checker.executeRec(numberOfSets, maxSetSize);
        }
    }

    @Test
    void testCommanderAtMostOne() {
        checkCommander(Algorithm.AT_MOST_ONE,
                       NUMBER_OF_SETS,
                       MAX_SET_SIZE);
    }

    @Test
    void testCommanderExactlyOne() {
        checkCommander(Algorithm.EXACTLY_ONE,
                       NUMBER_OF_SETS,
                       MAX_SET_SIZE);
    }

    @Test
    void testBinaryAtMostOne() {
        checkBinary(Algorithm.AT_MOST_ONE,
                    NUMBER_OF_SETS,
                    MAX_SET_SIZE);
    }

    @Test
    void testBinaryExactlyOne() {
        checkBinary(Algorithm.EXACTLY_ONE,
                    NUMBER_OF_SETS,
                    MAX_SET_SIZE);
    }

    @Test
    void testHeuleAtMostOne() {
        checkHeule(Algorithm.AT_MOST_ONE,
                   NUMBER_OF_SETS,
                   MAX_SET_SIZE);
    }

    @Test
    void testHeuleExactlyOne() {
        checkHeule(Algorithm.EXACTLY_ONE,
                   NUMBER_OF_SETS,
                   MAX_SET_SIZE);
    }
}