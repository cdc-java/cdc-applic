package cdc.applic.s1000d.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.Expression;
import cdc.io.data.Element;

class S1000DToExpressionConverterImplTest {
    private static final Logger LOGGER = LogManager.getLogger(S1000DToExpressionConverterImplTest.class);

    private static void convert(String expected,
                                String text,
                                Dictionary dictionary) throws IOException {
        LOGGER.info("convert({})", text);

        final Expression expression = new Expression(expected);
        final S1000DToExpressionConverterImpl<Element> converter = new S1000DToExpressionConverterImpl<>(dictionary);
        final StringXmlSource source = new StringXmlSource(text);
        final Expression actual = converter.convertToExpression(source.getRoot(), source);
        LOGGER.info("   actual: '{}'", actual);

        assertEquals(expression, actual);
    }

    private static void convert(String expected,
                                String text,
                                String actFilename) throws IOException {
        LOGGER.info("convert({})", text);

        final Expression expression = new Expression(expected);
        final File actFile = new File(actFilename);
        final S1000DToExpressionConverterImpl<Element> converter = new S1000DToExpressionConverterImpl<>(actFile);
        final StringXmlSource source = new StringXmlSource(text);
        final Expression actual = converter.convertToExpression(source.getRoot(), source);

        LOGGER.info("   actual: '{}'", actual);

        assertEquals(expression, actual);
    }

    private static String toAssert(String ident,
                                   String type,
                                   String values) {
        return "<assert applicPropertyIdent=\"" + ident
                + "\" applicPropertyType=\"" + type
                + "\" applicPropertyValues=\"" + values
                + "\"/>";
    }

    private static String toAssert(String informal) {
        return "<assert>" + informal + "</assert>";
    }

    @Test
    void testConvertWithDictionary() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        registry.enumeratedType()
                .name("VersionType")
                .literals("V1", "V2", "V3")
                .s1000DId("version-type-id")
                .s1000DPropertyType(S1000DPropertyType.PRODUCT_ATTRIBUTE)
                .build();
        registry.booleanType()
                .name("ChangeType")
                .s1000DId("change-tyoe-id")
                .s1000DPropertyType(S1000DPropertyType.PRODUCT_CONDITION)
                .build();
        registry.property()
                .name("Version")
                .type("VersionType")
                .s1000DId("version-id")
                .build();
        registry.property()
                .name("CH001")
                .type("ChangeType")
                .s1000DId("ch001-id")
                .build();

        convert("Version=V1",
                "<applic>" + toAssert("version-id", "prodattr", "V1") + "</applic>",
                registry);
        convert("Version<:{V1,V2}",
                "<applic>" + toAssert("version-id", "prodattr", "V1|V2") + "</applic>",
                registry);
        convert("Version=V1|Version=V2",
                "<applic><evaluate andOr='or'>"
                        + toAssert("version-id", "prodattr", "V1")
                        + toAssert("version-id", "prodattr", "V2")
                        + "</evaluate></applic>",
                registry);
        convert("Version=V1&Version=V2",
                "<applic><evaluate andOr='and'>"
                        + toAssert("version-id", "prodattr", "V1")
                        + toAssert("version-id", "prodattr", "V2")
                        + "</evaluate></applic>",
                registry);

        convert("$An informal condition$",
                "<applic>" + toAssert("An informal condition") + "</applic>",
                registry);

        assertThrows(ApplicException.class,
                     () -> {
                         convert("",
                                 "<applic>" + "</applic>",
                                 registry);
                     });
    }

    @Test
    void testIssue221() throws IOException {
        // Test duplicate ids in ACT and CCT
        convert("$An informal condition$",
                "<applic>" + toAssert("An informal condition") + "</applic>",
                "src/test/resources/issue221/DMC-XX-D-00-00-00-00A-00WA-A_221-00_EN-US.XML");
    }

    @Test
    void testIssue222() throws IOException {
        // Test duplicate names
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         convert("$An informal condition$",
                                 "<applic>" + toAssert("An informal condition") + "</applic>",
                                 "src/test/resources/issue222/DMC-XX-D-00-00-00-00A-00WA-A_222-00_EN-US.XML");
                     });
    }

}