package cdc.applic.s1000d.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.types.TypeKind;

class S1000DTypeConverterTest {
    private static S1000DTypeConverter.Proposal of(boolean different,
                                                   TypeKind kind,
                                                   String domain,
                                                   String pattern) {
        return S1000DTypeConverter.of(different, kind, domain, pattern);
    }

    private static void check(S1000DTypeConverter.Proposal expected,
                              String valueDataType,
                              String valuePattern,
                              String... applicPropertyValues) {
        final S1000DTypeConverter converter =
                S1000DTypeConverter.builder()
                                   .allHints()
                                   .build();

        final S1000DTypeConverter.Proposal result = converter.analyze(valueDataType,
                                                                      valuePattern,
                                                                      List.of(applicPropertyValues));

        assertEquals(expected, result);
    }

    private static void checkNoProposal(TypeKind kind,
                                        String valuePattern,
                                        String... applicPropertyValues) {
        final String domain = List.of(applicPropertyValues).stream().collect(Collectors.joining("|"));
        final S1000DTypeConverter.Proposal expected = of(false, kind, domain, valuePattern);
        check(expected, kind == null ? null : kind.getS1000DLiteral(), valuePattern, applicPropertyValues);
    }

    private static void checkBooleanLikeEnum(boolean different,
                                             String valueDataType,
                                             String... applicPropertyValues) {
        check(of(different, TypeKind.BOOLEAN, "false,true", null),
              valueDataType,
              null,
              applicPropertyValues);
    }

    private static void checkIntegerLikePattern(boolean different,
                                                String expectedDomain,
                                                String valueDataType,
                                                String valuePattern) {
        check(of(different, TypeKind.INTEGER, expectedDomain, null),
              valueDataType,
              valuePattern);
    }

    @Test
    void testBooleanLikeEnum() {
        checkBooleanLikeEnum(true, null, "false", "true");
        checkBooleanLikeEnum(true, null, "TRUE", "FALSE");
        checkBooleanLikeEnum(true, "string", "false", "true");
        checkBooleanLikeEnum(true, "string", "TRUE", "FALSE");
    }

    @Test
    void testIntegerLikePattern() {
        checkIntegerLikePattern(true, "0~9", null, "\\d");
        checkIntegerLikePattern(true, "0~9", null, "[0-9]");
        checkIntegerLikePattern(true, "0~9", null, "\\d{1,1}");
        checkIntegerLikePattern(true, "0~999", null, "\\d{1,3}");
        checkIntegerLikePattern(true, "0~999", null, "[0-9]{1,3}");
    }

    @Test
    void testNoPorposal() {
        checkNoProposal(null, null, "f", "t");
        checkNoProposal(null, null, "false");
    }
}