package cdc.applic.s1000d.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.applic.expressions.ApplicException;
import cdc.applic.expressions.Expression;

class ExpressionToS1000DConverterImplTest {
    private static final Logger LOGGER = LogManager.getLogger(ExpressionToS1000DConverterImplTest.class);

    private static void convert(String expected,
                                String expression,
                                Dictionary dictionary,
                                NamingConvention convention) throws IOException {
        LOGGER.info("convert({})", expression);
        final ExpressionToS1000DConverterImpl converter = new ExpressionToS1000DConverterImpl(dictionary, convention);
        final StringXmlHandler handler = new StringXmlHandler("", false);
        converter.convertToS1000D(new Expression(expression), handler);
        final String actual = handler.getText();
        LOGGER.info("   actual: '{}'", actual);

        assertEquals(expected, actual);
    }

    private static String toAssert(String ident,
                                   String type,
                                   String values) {
        return "<assert applicPropertyIdent=\"" + ident
                + "\" applicPropertyType=\"" + type
                + "\" applicPropertyValues=\"" + values
                + "\"/>";
    }

    private static String toAssert(String informal) {
        return "<assert>" + informal + "</assert>";
    }

    @Test
    void test() throws IOException {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        registry.enumeratedType()
                .name("VersionType")
                .literals("V1", "V2", "V3")
                .s1000DId("version-type-id")
                .s1000DPropertyType(S1000DPropertyType.PRODUCT_ATTRIBUTE)
                .build();
        registry.booleanType()
                .name("ChangeType")
                .s1000DId("change-tyoe-id")
                .s1000DPropertyType(S1000DPropertyType.PRODUCT_CONDITION)
                .build();
        registry.property()
                .name("Version")
                .type("VersionType")
                .s1000DId("version-id")
                .build();
        registry.property()
                .name("CH001")
                .type("ChangeType")
                .s1000DId("ch001-id")
                .build();

        convert("<applic>" + toAssert("version-id", "prodattr", "V1") + "</applic>",
                "Version = V1",
                registry,
                NamingConvention.DEFAULT);

        convert("<applic>" + toAssert("An informal condition") + "</applic>",
                "$An informal condition$",
                registry,
                NamingConvention.DEFAULT);

        convert("<applic>" + toAssert("version-id", "prodattr", "V1|V2") + "</applic>",
                "Version in {V1, V2}",
                registry,
                NamingConvention.DEFAULT);

        convert("<applic>" + toAssert("ch001-id", "condition", "true") + "</applic>",
                "CH001 = true",
                registry,
                NamingConvention.DEFAULT);

        convert("<applic><evaluate andOr=\"or\">"
                + toAssert("version-id", "prodattr", "V1")
                + toAssert("version-id", "prodattr", "V2")
                + "</evaluate></applic>",
                "Version = V1 or Version = V2",
                registry,
                NamingConvention.DEFAULT);

        convert("<applic><evaluate andOr=\"and\">"
                + toAssert("version-id", "prodattr", "V1")
                + toAssert("version-id", "prodattr", "V2")
                + "</evaluate></applic>",
                "Version = V1 and Version = V2",
                registry,
                NamingConvention.DEFAULT);

        convert("<applic><evaluate andOr=\"and\">"
                + toAssert("version-id", "prodattr", "V1")
                + toAssert("version-id", "prodattr", "V2")
                + toAssert("version-id", "prodattr", "V3")
                + "</evaluate></applic>",
                "Version = V1 and Version = V2 and Version = V3",
                registry,
                NamingConvention.DEFAULT);

        convert("<applic><evaluate andOr=\"or\">"
                + toAssert("version-id", "prodattr", "V1")
                + toAssert("version-id", "prodattr", "V2")
                + toAssert("version-id", "prodattr", "V3")
                + "</evaluate></applic>",
                "Version = V1 or Version = V2 or Version = V3",
                registry,
                NamingConvention.DEFAULT);

        assertThrows(ApplicException.class,
                     () -> convert("",
                                   "true",
                                   registry,
                                   NamingConvention.DEFAULT));

        assertThrows(ApplicException.class,
                     () -> convert("",
                                   "Version != V1",
                                   registry,
                                   NamingConvention.DEFAULT));

        assertThrows(ApplicException.class,
                     () -> convert("",
                                   "Version >= V1",
                                   registry,
                                   NamingConvention.DEFAULT));
    }
}