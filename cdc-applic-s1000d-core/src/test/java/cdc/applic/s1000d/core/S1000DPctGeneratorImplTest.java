package cdc.applic.s1000d.core;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.PropertyImpl;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.s1000d.S1000DProductIdentifier;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.literals.Name;
import cdc.applic.s1000d.S1000DActCctPctCollector;
import cdc.applic.s1000d.S1000DPctGenerator;
import cdc.issues.IssueSeverity;
import cdc.util.events.ProgressController;

class S1000DPctGeneratorImplTest {
    private final RepositoryImpl repository = new RepositoryImpl();
    private final RegistryImpl registry = repository.registry().name("Registry").build();
    private final DictionaryHandle registryHandle = new DictionaryHandle(registry);

    S1000DPctGeneratorImplTest() {
        registry.integerType()
                .name("Rank")
                .frozen(true)
                .domain("1~999")
                .s1000DPropertyType(S1000DPropertyType.PRODUCT_ATTRIBUTE)
                .s1000DProductIdentifier(S1000DProductIdentifier.PRIMARY)
                .build();
        registry.enumeratedType()
                .name("Version")
                .frozen(false)
                .literals("V1", "V2", "V3", "V4", "V5")
                .s1000DPropertyType(S1000DPropertyType.PRODUCT_ATTRIBUTE)
                .build();
        registry.enumeratedType()
                .name("Standard")
                .frozen(false)
                .literals("S1", "S2", "S3", "S4")
                .s1000DPropertyType(S1000DPropertyType.PRODUCT_CONDITION)
                .build();
        registry.patternType()
                .name("PartNumber")
                .frozen(true)
                .pattern("[A-Z0-5]{1,32}")
                .s1000DPropertyType(S1000DPropertyType.PRODUCT_ATTRIBUTE)
                .build();
        registry.booleanType()
                .name("Boolean")
                .s1000DPropertyType(S1000DPropertyType.PRODUCT_CONDITION)
                .build();
        registry.realType()
                .name("Temperature")
                .frozen(false)
                .domain("-10.0~100.0")
                .s1000DPropertyType(S1000DPropertyType.EXTERNAL_CONDITION)
                .build();

        registry.property().name("Rank").type("Rank").ordinal(0).build();
        registry.property().name("Version").type("Version").ordinal(0).build();
        registry.property().name("Standard").type("Standard").ordinal(0).build();
        registry.property().name("PartNumber").type("PartNumber").ordinal(0).build();
        registry.property().name("Boolean1").type("Boolean").ordinal(0).build();
        registry.property().name("Boolean2").type("Boolean").ordinal(0).build();
        registry.property().name("Temperature").type("Temperature").ordinal(0).build();

        registry.createAssertion("Version = V1 -> Rank in {1~100}");
        registry.createAssertion("Version = V2 -> Rank in {101~200}");
        registry.createAssertion("Version = V3 -> Rank in {201~300}");
        registry.createAssertion("Version = V4 -> Rank in {301~400}");
        registry.createAssertion("Version = V5 -> Rank in {401~500}");
        registry.createAssertion("Standard in {S1, S2} iff Version in {V1, V2}");
        registry.createAssertion("Standard in {S1, S2} -> Boolean2");
        registry.createAssertion("Standard in {S3, S4} iff Version in {V3, V4}");
        registry.createAssertion("Version = V2 -> Temperature in {0.0~50.0}");
        registry.createAssertion("Version = V1 -> PartNumber in {}");
        registry.createAssertion("Rank = 101 -> Boolean1 in {}");
        registry.createAssertion("Rank = 104 -> PartNumber = AAA104");
        registry.createAssertion("Rank = 105 -> PartNumber in {AAA105, BBB105}");
    }

    @Test
    void test() {
        final S1000DPctGenerator pctComputer = new S1000DPctGeneratorImpl();
        final S1000DActCctPctCollector handler = new S1000DActCctPctCollector();

        final Expression additionalAssertion = new Expression("Rank in {100~110}");
        final S1000DProperty masterProperty = registry.getOptionalAllowedProperty(Name.of("Rank")).orElse(null);

        pctComputer.generatePct(registryHandle,
                                additionalAssertion,
                                NamingConvention.DEFAULT,
                                masterProperty,
                                handler,
                                ProgressController.VOID);

        assertSame(3, handler.getAct().getProductAttributes().size());
        assertSame(15, handler.getIssuesCollector().getIssues(IssueSeverity.INFO).size());
        assertSame(0, handler.getIssuesCollector().getIssues(IssueSeverity.MINOR).size());
        assertSame(8, handler.getIssuesCollector().getIssues(IssueSeverity.CRITICAL).size());
        assertSame(0, handler.getIssuesCollector().getIssues(IssueSeverity.BLOCKER).size());
    }

    @Test
    void testRealNotSupported() {
        final S1000DPctGenerator pctComputer = new S1000DPctGeneratorImpl();
        final S1000DActCctPctCollector handler = new S1000DActCctPctCollector();

        final Expression additionalAssertion = Expression.TRUE;
        final S1000DProperty masterProperty = registry.getOptionalAllowedProperty(Name.of("Temperature")).orElse(null);
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         pctComputer.generatePct(registryHandle,
                                                 additionalAssertion,
                                                 NamingConvention.DEFAULT,
                                                 masterProperty,
                                                 handler,
                                                 ProgressController.VOID);
                     });
    }

    @Test
    void testPatternNotSupported() {
        final S1000DPctGenerator pctComputer = new S1000DPctGeneratorImpl();
        final S1000DActCctPctCollector handler = new S1000DActCctPctCollector();

        final Expression additionalAssertion = Expression.TRUE;
        final S1000DProperty masterProperty = registry.getOptionalAllowedProperty(Name.of("PartNumber")).orElse(null);
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         pctComputer.generatePct(registryHandle,
                                                 additionalAssertion,
                                                 NamingConvention.DEFAULT,
                                                 masterProperty,
                                                 handler,
                                                 ProgressController.VOID);
                     });
    }

    @Test
    void testConditionNotSupported() {
        final S1000DPctGenerator pctComputer = new S1000DPctGeneratorImpl();
        final S1000DActCctPctCollector handler = new S1000DActCctPctCollector();

        final Expression additionalAssertion = Expression.TRUE;
        final S1000DProperty masterProperty = registry.getOptionalAllowedProperty(Name.of("Standard")).orElse(null);
        assertThrows(IllegalArgumentException.class,
                     () -> {
                         pctComputer.generatePct(registryHandle,
                                                 additionalAssertion,
                                                 NamingConvention.DEFAULT,
                                                 masterProperty,
                                                 handler,
                                                 ProgressController.VOID);
                     });
    }

    @Test
    void testIssue215() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl r1 = repository.registry()
                                          .name("R1")
                                          .build();
        r1.enumeratedType()
          .name("V")
          .value().literal("V1").literal("V2").back()
          .s1000DPropertyType(S1000DPropertyType.PRODUCT_ATTRIBUTE)
          .s1000DProductIdentifier(S1000DProductIdentifier.PRIMARY)
          .build();
        final PropertyImpl v = r1.property()
                                 .name("V")
                                 .type("V")
                                 .build();
        final RegistryImpl r2 = repository.registry()
                                          .name("R2")
                                          .parents(r1)
                                          .build();
        final DictionaryHandle handle = new DictionaryHandle(r2);
        final S1000DPctGeneratorImpl generator = new S1000DPctGeneratorImpl();

        final S1000DActCctPctCollector collector = new S1000DActCctPctCollector();
        generator.generatePct(handle,
                              Expression.TRUE,
                              NamingConvention.DEFAULT,
                              v,
                              collector,
                              ProgressController.VERBOSE);
        assertTrue(true);
    }
}
