package cdc.applic.s1000d.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.s1000d.S1000DProductIdentifier;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.applic.dictionaries.s1000d.S1000DType;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.DomainedType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.IntegerRange;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.IntegerValue;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.content.Value;
import cdc.applic.projections.Axis;
import cdc.applic.projections.ExpressionProjector;
import cdc.applic.projections.Shadow;
import cdc.applic.proofs.ProverMatching;
import cdc.applic.s1000d.S1000DActHandler;
import cdc.applic.s1000d.S1000DCctHandler;
import cdc.applic.s1000d.S1000DPctGenerator;
import cdc.applic.s1000d.S1000DPctHandler;
import cdc.applic.s1000d.S1000DProfile;
import cdc.applic.s1000d.S1000DValues;
import cdc.applic.s1000d.S1000DValues.Filling;
import cdc.applic.s1000d.issues.S1000DIssueType;
import cdc.issues.Issue;
import cdc.util.events.ProgressController;
import cdc.util.lang.Checks;

public class S1000DPctGeneratorImpl implements S1000DPctGenerator {
    public S1000DPctGeneratorImpl() {
        super();
    }

    @Override
    public void generatePct(DictionaryHandle handle,
                            Expression additionalAssertion,
                            NamingConvention convention,
                            S1000DProperty masterProperty,
                            S1000DPctHandler handler,
                            ProgressController controller,
                            S1000DProfile profile) {
        final Generator generator = new Generator(handle,
                                                  additionalAssertion,
                                                  convention,
                                                  masterProperty,
                                                  handler,
                                                  controller,
                                                  profile);
        generator.execute();
    }

    private static class Generator extends S1000DAbstractGenerator<S1000DPctHandler> {
        private final S1000DProperty masterProperty;
        /** All properties, except master one and external conditions. */
        private final List<S1000DProperty> properties = new ArrayList<>();

        /**
         * Association of types to their full domain (which is a subset of the type domain).
         */
        private final Map<S1000DType, SItemSet> fullDomains = new HashMap<>();

        protected String product(Value masterValue) {
            return "Product(" + masterProperty.getName() + "=" + masterValue + ")";
        }

        protected String products(SItemSet masterValues) {
            return "Products(" + masterProperty.getName() + "<:" + masterValues + ")";
        }

        /**
         * ACT handler that traps certain events and delegates all of them to user handler.
         */
        private final S1000DActHandler actHandler = new S1000DActHandler() {
            @Override
            public void issue(Issue issue) {
                handler.issue(issue);
            }

            @Override
            public void beginAct(DictionaryHandle handle,
                                 Expression additinalAssertion,
                                 NamingConvention convention) {
                handler.beginAct(handle, additinalAssertion, convention);
            }

            @Override
            public void addActProductAttribute(S1000DProperty property,
                                               String pattern,
                                               SItemSet domain,
                                               NamingConvention convention) {
                if (domain != null) {
                    fullDomains.put(property.getType(), domain);
                }
                handler.addActProductAttribute(property, pattern, domain, convention);
            }

            @Override
            public void endAct() {
                handler.endAct();
            }
        };

        /**
         * CCT handler that traps certain events and delegates all of them to user handler.
         */
        private final S1000DCctHandler cctHandler = new S1000DCctHandler() {
            @Override
            public void issue(Issue issue) {
                handler.issue(issue);
            }

            @Override
            public void beginCct(DictionaryHandle handle,
                                 Expression additinalAssertion,
                                 NamingConvention convention) {
                handler.beginCct(handle, additinalAssertion, convention);
            }

            @Override
            public void beginCctConditionTypes() {
                handler.beginCctConditionTypes();
            }

            @Override
            public void addCctConditionType(S1000DType type,
                                            String pattern,
                                            SItemSet domain,
                                            NamingConvention convention) {
                if (domain != null) {
                    fullDomains.put(type, domain);
                }
                handler.addCctConditionType(type, pattern, domain, convention);
            }

            @Override
            public void endCctConditionTypes() {
                handler.endCctConditionTypes();
            }

            @Override
            public void beginCctConditions() {
                handler.beginCctConditions();
            }

            @Override
            public void beginCctCondition(S1000DProperty property,
                                          SItemSet domain,
                                          NamingConvention convention) {
                handler.beginCctCondition(property, domain, convention);
            }

            @Override
            public void addCctConditionDependency(S1000DProperty property,
                                                  SItemSet values,
                                                  S1000DProperty dependency,
                                                  SItemSet dependencyValues,
                                                  NamingConvention convention) {
                handler.addCctConditionDependency(property, values, dependency, dependencyValues, convention);
            }

            @Override
            public void endCctCondition() {
                handler.endCctCondition();
            }

            @Override
            public void endCctConditions() {
                handler.endCctConditions();
            }

            @Override
            public void endCct() {
                handler.endCct();
            }
        };

        public Generator(DictionaryHandle handle,
                         Expression additionalAssertion,
                         NamingConvention convention,
                         S1000DProperty masterProperty,
                         S1000DPctHandler handler,
                         ProgressController controller,
                         S1000DProfile profile) {
            super(handle, additionalAssertion, convention, handler, controller, profile);
            Checks.isTrue(masterProperty.getS1000DPropertyType() == S1000DPropertyType.PRODUCT_ATTRIBUTE,
                          masterProperty.getName() + " is not a product attribute");
            Checks.isTrue(masterProperty.getS1000DProductIdentifier() == S1000DProductIdentifier.PRIMARY
                    || masterProperty.getS1000DProductIdentifier() == S1000DProductIdentifier.SECONDARY,
                          masterProperty.getName() + " is not a primary or secondary product attribute");
            this.masterProperty = masterProperty;

            for (final Property property : handle.getDictionary().getAllowedProperties()) {
                if (property != masterProperty && property instanceof S1000DProperty) {
                    final S1000DProperty p = (S1000DProperty) property;
                    if (p.getS1000DPropertyType().isPctCandidate()) {
                        this.properties.add(p);
                    }
                }
            }
            Collections.sort(this.properties, S1000DProperty.COMPARATOR);
        }

        @Override
        protected void execute() {
            // Global checks
            final S1000DGlobalCheckerImpl globalChecker = new S1000DGlobalCheckerImpl();
            globalChecker.check(handle, additionalAssertion, namingConvention, handler, controller, profile);

            // Computes ACT and stored full domains of product attributes types
            final S1000DActGeneratorImpl actComputer = new S1000DActGeneratorImpl();
            actComputer.generateAct(handle, additionalAssertion, namingConvention, actHandler, controller, profile);

            // Computes CCT and stored full domains of condition types
            final S1000DCctGeneratorImpl cctComputer = new S1000DCctGeneratorImpl();
            cctComputer.generateCct(handle, additionalAssertion, namingConvention, cctHandler, controller, profile);

            // Computes PCT
            handler.beginPct(handle, additionalAssertion, namingConvention, masterProperty);
            issue(S1000DIssueType.GENERATE_PCT, "Generate PCT for " + wrapDictionary(false));

            // Number of calls of incrementValue is for each product
            final long k = 1L + properties.size();

            final S1000DType masterType = masterProperty.getType();
            if (masterType instanceof BooleanType) {
                supplier.reset(2 * k, "Process boolean type");
                supplier.setDetail("Process product " + masterProperty.getName() + "=false");
                supplier.fireProgress(true);
                processProduct(BooleanValue.FALSE, BooleanSet.FALSE_TRUE);
                supplier.incrementValue();
                supplier.setDetail("Process " + masterProperty.getName() + "=true");
                supplier.fireProgress(true);
                processProduct(BooleanValue.TRUE, BooleanSet.FALSE_TRUE);
                supplier.incrementValue();
            } else if (masterType instanceof EnumeratedType) {
                final StringSet validDomain = getValidDomain().toStringSet();
                supplier.reset(validDomain.getItems().size() * k, "Process enumerated type");
                if (validDomain.isEmpty()) {
                    issue(S1000DIssueType.CANNOT_SET_MASTER_PROPERTY,
                          usingAndAsserting() + ", no valid value exists for " + wrap(masterProperty, false) + ".",
                          masterProperty);
                }
                for (final StringValue value : validDomain.getItems()) {
                    supplier.setDetail("Process product " + masterProperty.getName() + "=" + value);
                    supplier.fireProgress(true);
                    processProduct(value, validDomain);
                    supplier.incrementValue();
                }
            } else if (masterType instanceof IntegerType) {
                final IntegerSet validDomain = getValidDomain().toIntegerSet();
                supplier.reset(validDomain.getExtent() * k, "Process integer type");
                if (validDomain.isEmpty()) {
                    issue(S1000DIssueType.CANNOT_SET_MASTER_PROPERTY,
                          usingAndAsserting() + ", no valid value exists for " + wrap(masterProperty, false) + ".",
                          masterProperty);
                }
                for (final IntegerRange range : validDomain.getRanges()) {
                    for (int value = range.getMin().getNumber(); value <= range.getMax().getNumber(); value++) {
                        supplier.setDetail("Process product " + masterProperty.getName() + "=" + value);
                        supplier.fireProgress(true);
                        processProduct(IntegerValue.of(value), validDomain);
                        supplier.incrementValue();
                    }
                }
                final IntegerSet requiredDomain = getRequiredDomain().toIntegerSet();
                final IntegerSet excludedDomain = requiredDomain.remove(validDomain);

                if (!excludedDomain.isEmpty()) {
                    issue(S1000DIssueType.EXCLUDED_PRODUCTS,
                          usingAndAsserting() + ", " + products(excludedDomain)
                                  + " were excluded because of contradictions in assertions.",
                          (Property) null);
                }
            } else {
                throw new IllegalArgumentException("Type of master property '" + masterProperty.getName()
                        + "' is not supported for PCT computation.");
            }

            issue(S1000DIssueType.GENERATED_PCT, "Generated PCT for " + wrapDictionary(false));
            handler.endPct();
        }

        private SItemSet getValidDomain() {
            return normalize(getPossibleDomain(masterProperty), masterProperty);
        }

        private SItemSet getRequiredDomain() {
            return normalize(getRequiredDomain(masterProperty), masterProperty);
        }

        /**
         * Processes one product instance.
         *
         * @param masterValue The master value that defines the product instance.
         * @param masterCutValues The set of possible master values (taking into account assertions and additional assertion).
         */
        private void processProduct(Value masterValue,
                                    SItemSet masterCutValues) {
            handler.beginPctProduct(masterValue, namingConvention);

            final Expression masterExpression =
                    new Expression(masterProperty.getName().getProtectedLiteral()
                            + " = "
                            + masterValue.getProtectedLiteral()
                            + " and "
                            + additionalAssertion.getContent());

            // Master is a domained type
            // By construction, the obtained set is a singleton
            handler.addPctProductProperty(masterProperty,
                                          new S1000DValues(masterCutValues,
                                                           SItemSetUtils.createBest(masterValue),
                                                           S1000DValues.Filling.PARTIAL,
                                                           S1000DValues.Interpretation.NORMAL),
                                          namingConvention);

            for (final S1000DProperty property : properties) {
                processProductProperty(masterExpression, masterValue, property);
                supplier.incrementValue();
            }

            handler.endPctProduct();
        }

        private void processProductProperty(Expression masterExpression,
                                            Value masterValue,
                                            S1000DProperty property) {

            // Ignore non-s1000d properties
            if (property.isS1000DCompliant()) {
                final ExpressionProjector projector = getProjectorIncludingAssertions(property);
                final Shadow shadow = projector.project(masterExpression);
                final Axis axis = new Axis(property);
                final SItemSet values =
                        normalize(shadow.getAxisSet(axis,
                                                    ProverMatching.ALWAYS,
                                                    ProverMatching.SOMETIMES),
                                  property);
                final SItemSet cutValues = normalize(shadow.getAxisSet(axis), property);

                final S1000DValues.Filling filling;
                final S1000DValues.Interpretation interpretation;
                if (values.isEmpty()) {
                    filling = S1000DValues.Filling.EMPTY;
                    // This property can never have a valid value
                    if (property.getType() instanceof DomainedType) {
                        interpretation = S1000DValues.Interpretation.NORMAL;
                    } else {
                        interpretation = S1000DValues.Interpretation.SPECIAL;
                    }
                } else if (property.getType() instanceof DomainedType) {
                    interpretation = S1000DValues.Interpretation.NORMAL;
                    final SItemSet fullDomain = fullDomains.get(property.getType());
                    if (fullDomain.remove(values).isEmpty()) {
                        // This property can have any value, possibly with additional conditions
                        filling = S1000DValues.Filling.FULL;
                    } else {
                        // This property can not have all values
                        filling = S1000DValues.Filling.PARTIAL;
                    }
                } else {
                    // This property can not have all values
                    if (((StringSet) values).containsPatternAny()) {
                        if (cutValues.hasSameContentAs(values)) {
                            filling = S1000DValues.Filling.FULL;
                        } else {
                            filling = S1000DValues.Filling.PARTIAL;
                        }
                    } else {
                        filling = S1000DValues.Filling.PARTIAL;
                    }
                    interpretation = S1000DValues.Interpretation.SPECIAL;
                }
                final S1000DValues v = new S1000DValues(cutValues, values, filling, interpretation);
                if (v.getFilling() == Filling.EMPTY) {
                    // Can this happen?
                    issue(S1000DIssueType.CANNOT_SET_PROPERTY,
                          product(masterValue) + ": cannot set any value for " + wrap(property, false) + ".",
                          property);
                } else if (property.getS1000DPropertyType().isActCandidate()
                        // && !(property.getType() instanceof PatternType)
                        && !v.isSingleton()) {
                    issue(S1000DIssueType.TOO_MANY_VALUES,
                          usingAndAsserting() + ", " + product(masterValue) + ": too many values are possible for "
                                  + wrap(property, false) + ".",
                          property);
                }
                checkSupportOfRanges(property, values);
                handler.addPctProductProperty(property, v, namingConvention);
            }
        }
    }
}
