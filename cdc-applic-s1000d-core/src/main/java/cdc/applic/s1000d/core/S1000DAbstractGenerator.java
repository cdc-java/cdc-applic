package cdc.applic.s1000d.core;

import java.util.Collection;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.core.utils.SItemSetOperations;
import cdc.applic.dictionaries.core.visitors.NormalizeSets;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.s1000d.S1000DProductIdentifier;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.dictionaries.types.DomainedType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.RealType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.UncheckedSet;
import cdc.applic.projections.Axis;
import cdc.applic.projections.ExpressionProjector;
import cdc.applic.projections.Shadow;
import cdc.applic.projections.core.SingleAxisExpressionProjector;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.ProverMatching;
import cdc.applic.s1000d.S1000DProfile;
import cdc.applic.s1000d.issues.S1000DIssueType;
import cdc.applic.s1000d.issues.S1000DIssues;
import cdc.issues.Issue;
import cdc.issues.IssuesHandler;
import cdc.issues.locations.DefaultLocation;
import cdc.util.events.ProgressController;
import cdc.util.events.ProgressSupplier;
import cdc.util.lang.Checks;

/**
 * Base class used to implement ACT, CCT and PCT generators.
 *
 * @author Damien Carbonne
 *
 * @param <H> The handler type.
 */
public abstract class S1000DAbstractGenerator<H extends IssuesHandler<Issue>> {
    protected final DictionaryHandle handle;
    protected final Expression additionalAssertion;
    protected final NamingConvention namingConvention;
    protected final H handler;
    protected final ProgressController controller;
    protected final S1000DProfile profile;
    protected final ProgressSupplier supplier;

    protected S1000DAbstractGenerator(DictionaryHandle handle,
                                      Expression additionalAssertion,
                                      NamingConvention convention,
                                      H handler,
                                      ProgressController controller,
                                      S1000DProfile profile) {
        Checks.isNotNull(handle, "handle");
        Checks.isNotNull(additionalAssertion, "additionalAssertion");
        Checks.isNotNull(convention, "convention");
        Checks.isNotNull(handler, "handler");
        Checks.isNotNull(controller, "controller");
        Checks.isNotNull(profile, "profile");

        this.handle = handle;
        this.additionalAssertion = additionalAssertion;
        this.namingConvention = convention;
        this.handler = handler;
        this.controller = controller;
        this.supplier = new ProgressSupplier(controller);
        this.profile = profile;
    }

    protected abstract void execute();

    protected ExpressionProjector getProjectorIncludingAssertions(Property property) {
        return new SingleAxisExpressionProjector(handle,
                                                 ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES,
                                                 new Axis(property));
    }

    protected ExpressionProjector getProjectorExcludingAssertions(Property property) {
        return new SingleAxisExpressionProjector(handle,
                                                 ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES,
                                                 new Axis(property));
    }

    /**
     * Returns the domain of possible values for a property.
     * <p>
     * Computation includes assertions and additionalAssertion.
     *
     * @param property The property.
     * @return The domain of possible values for {@code property}.
     */
    protected SItemSet getPossibleDomain(Property property) {
        Checks.isNotNull(property, "property");

        if (property.getType() instanceof DomainedType) {
            final ExpressionProjector projector = getProjectorIncludingAssertions(property);
            final Shadow shadow = projector.project(additionalAssertion);
            return normalize(shadow.getAxisSet(new Axis(property),
                                               ProverMatching.ALWAYS,
                                               ProverMatching.SOMETIMES),
                             property);
        } else {
            return null;
        }
    }

    /**
     * Computes the domain of possible values for a collection of properties that all have the same type.
     * <p>
     * It is possible that, in the context of a dictionary, taking into account assertions, some values are excluded.<br>
     * For a condition, the result will probably be the full set of the type.
     *
     * @param properties The properties
     * @param type The type of properties.
     * @param supplier The progress supplier.<br>
     *            It will be incremented by the size of {@code properties}.
     * @return The possible domain for {@code type}.
     */
    protected SItemSet getPossibleDomain(Collection<? extends Property> properties,
                                         Type type,
                                         ProgressSupplier supplier) {
        // Number of properties
        final int size = properties.size();
        // Current value of supplier
        // In the end, supplier should be value0 + size
        final long value0 = supplier.getValue();
        // Result set
        SItemSet set = UncheckedSet.EMPTY;
        for (final Property property : properties) {
            set = set.union(getPossibleDomain(property));
            supplier.incrementValue();
            if (SItemSetOperations.isFullSet(set, type)) {
                // We can stop iteration as set won't change anymore
                // isFullSet is much cheaper than getPossibleDomain(property)
                supplier.incrementValue(value0 + size - supplier.getValue());
                return set;
            }
        }
        return set;
    }

    protected SItemSet getRequiredDomain(Property property) {
        Checks.isNotNull(property, "property");

        if (property.getType() instanceof DomainedType) {
            final ExpressionProjector projector = getProjectorExcludingAssertions(property);
            final Shadow shadow = projector.project(additionalAssertion);
            return normalize(shadow.getAxisSet(new Axis(property),
                                               ProverMatching.ALWAYS,
                                               ProverMatching.SOMETIMES),
                             property);
        } else {
            return null;
        }
    }

    protected static SItemSet normalize(SItemSet set,
                                        Property property) {
        return NormalizeSets.normalizeSet(set, property.getType());
    }

    /**
     * @param property The property.
     * @param capital If {@code true}, capital case shall be used.
     * @return A sentence describing the category of {@code property}: {@code "[Pp]roperty|[Cc]ondition|..."}.
     */
    protected static String getCategory(Property property,
                                        boolean capital) {
        if (property instanceof S1000DProperty) {
            final S1000DProperty p = (S1000DProperty) property;
            if (p.getS1000DPropertyType().isActCandidate()) {
                if (p.getS1000DProductIdentifier() == S1000DProductIdentifier.PRIMARY) {
                    return (capital ? "P" : "p") + "rimary product attribute";
                } else if (p.getS1000DProductIdentifier() == S1000DProductIdentifier.SECONDARY) {
                    return (capital ? "S" : "s") + "econdary product attribute";
                } else {
                    return (capital ? "P" : "p") + "roduct attribute";
                }
            } else if (p.getS1000DPropertyType().isCctCandidate()) {
                return (capital ? "C" : "c") + "ondition";
            } else {
                return (capital ? "P" : "p") + "roperty";
            }
        } else {
            return (capital ? "P" : "p") + "roperty";
        }
    }

    /**
     *
     * @param property The property.
     * @param capital If {@code true}, capital case shall be used.
     * @return The sentence: {@code getCategory(property, capital) + " 'property name'"}.
     */
    protected static String wrap(Property property,
                                 boolean capital) {
        return getCategory(property, capital) + " '" + property.getName().getNonEscapedLiteral() + "'";
    }

    /**
     * @param type The type.
     * @param capital If {@code true}, capital case shall be used.
     * @return The sentence: {@code "[Tt]ype 'type name'"}.
     */
    protected static String wrap(Type type,
                                 boolean capital) {
        return (capital ? "T" : "t") + "ype '" + type.getName() + "'";
    }

    /**
     * @param capital If {@code true}, capital case shall be used.
     * @return The sentence: {@code "[Dd]ictionary 'dictionary name'"}.
     */
    protected String wrapDictionary(boolean capital) {
        return (capital ? "Dictionary '" : "dictionary '") + handle.getDictionary().getName() + "'";
    }

    /**
     * @return The sentence: {@code "Using 'dictionary name' and asserting 'additional assertion'"}.
     */
    protected String usingAndAsserting() {
        return "Using '" + handle.getDictionary().getName() + "' and asserting '" + additionalAssertion + "'";
    }

    /**
     * Triggers a global issue.
     *
     * @param type The issue type.
     * @param description The issue description.
     */
    protected void issue(S1000DIssueType type,
                         String description) {
        handler.issue(S1000DIssues.builder()
                                  .name(type)
                                  .severity(type.getSeverity())
                                  .description(description)
                                  .addLocation(DefaultLocation.builder()
                                                              .path(handle.getDictionary().getName())
                                                              .build())
                                  .build());
    }

    /**
     * Triggers an issue related to a property.
     *
     * @param type The issue type.
     * @param description The issue description.
     * @param property The property.
     */
    protected void issue(S1000DIssueType type,
                         String description,
                         Property property) {
        handler.issue(S1000DIssues.builder()
                                  .name(type)
                                  .severity(type.getSeverity())
                                  .description(description)
                                  .addLocation(DefaultLocation.builder()
                                                              .path(handle.getDictionary().getName())
                                                              .anchor(property == null
                                                                      ? null
                                                                      : property.getName().getNonEscapedLiteral())
                                                              .build())
                                  .build());
    }

    /**
     * Triggers an issue related to a property type.
     *
     * @param type The issue type.
     * @param description The issue description.
     * @param propertyType The property type.
     */
    protected void issue(S1000DIssueType type,
                         String description,
                         Type propertyType) {
        handler.issue(S1000DIssues.builder()
                                  .name(type)
                                  .severity(type.getSeverity())
                                  .description(description)
                                  .addLocation(DefaultLocation.builder()
                                                              .path(handle.getDictionary().getName())
                                                              .anchor(propertyType == null
                                                                      ? null
                                                                      : propertyType.getName()
                                                                                    .removePrefix(handle.getDictionary()
                                                                                                        .getPrefix())
                                                                                    .getProtectedLiteral())
                                                              .build())
                                  .build());
    }

    /**
     * @param type The type.
     * @param set The set.
     * @return {@code true} when {@code set} contains ranges and the profile does not allow them.
     */
    private boolean useRangesWhenNotSupported(Type type,
                                              SItemSet set) {
        return set.containsRanges()
                && ((type instanceof IntegerType && !profile.isSupported(S1000DProfile.Feature.INTEGER_RANGES))
                        || (type instanceof RealType && !profile.isSupported(S1000DProfile.Feature.REAL_RANGES)));
    }

    /**
     * Generates an Issue if a set uses ranges and the profile does not allow them.
     *
     * @param property The property.
     * @param set The set (that must be compliant with property).
     */
    protected void checkSupportOfRanges(Property property,
                                        SItemSet set) {
        final Type type = property.getType();
        if (useRangesWhenNotSupported(type, set)) {
            issue(S1000DIssueType.NON_SUPPORTED_FEATURE,
                  wrap(property, true) + " is defined using ranges, which is not supported: " + type.getDefinition() + ".",
                  property);
        }
    }

    /**
     * Generates an Issue if a set uses ranges and the profile does not allow them.
     *
     * @param type The type.
     * @param set The set (that must be compliant with property).
     */
    protected void checkSupportOfRanges(Type type,
                                        SItemSet set) {
        if (useRangesWhenNotSupported(type, set)) {
            issue(S1000DIssueType.NON_SUPPORTED_FEATURE,
                  wrap(type, true) + " is defined using ranges, wich is not supported: " + type.getDefinition() + ".",
                  type);
        }
    }
}