package cdc.applic.s1000d.core;

import java.io.IOException;

import cdc.applic.s1000d.XmlHandler;
import cdc.io.data.Element;

/**
 * Implementation of {@link XmlHandler} that creates an {@link Element} tree.
 *
 * @author Damien Carbonne
 */
public class DataXmlHandler implements XmlHandler {
    private Element root;
    private Element current;

    public DataXmlHandler() {
        super();
    }

    public void clear() {
        this.root = null;
        this.current = null;
    }

    /**
     * @return The root element.
     */
    public Element getRoot() {
        return root;
    }

    @Override
    public void beginElement(String name) {
        current = new Element(current, name);
        if (root == null) {
            root = current;
        }
    }

    @Override
    public void addAttribute(String name,
                             String value) {
        current.addAttribute(name, value);
    }

    @Override
    public void addElementContent(String content) throws IOException {
        current.addText(content, true);
    }

    @Override
    public void endElement() {
        current = (Element) current.getParent();
    }
}