package cdc.applic.s1000d.core;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import cdc.applic.s1000d.XmlHandler;
import cdc.io.xml.XmlWriter;

public class StringXmlHandler implements XmlHandler {
    private final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    private final XmlWriter writer;

    public StringXmlHandler(String indentString,
                            boolean prettyPrint)
            throws IOException {
        this.writer = new XmlWriter(buffer);
        this.writer.setIndentString(indentString);
        this.writer.setEnabled(XmlWriter.Feature.ALLOW_PARTIAL_XML);
        if (prettyPrint) {
            this.writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);
        }
    }

    public String getText() throws IOException {
        writer.flush();
        return buffer.toString();
    }

    @Override
    public void beginElement(String name) throws IOException {
        writer.beginElement(name);
    }

    @Override
    public void addAttribute(String name,
                             String value) throws IOException {
        writer.addAttribute(name, value);
    }

    @Override
    public void addElementContent(String content) throws IOException {
        writer.addElementContent(content);
    }

    @Override
    public void endElement() throws IOException {
        writer.endElement();
    }
}