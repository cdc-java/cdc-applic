package cdc.applic.s1000d.core;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import cdc.applic.dictionaries.s1000d.S1000DProductIdentifier;
import cdc.applic.dictionaries.types.TypeKind;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.content.StringSet;
import cdc.applic.expressions.content.StringValue;
import cdc.io.data.Element;
import cdc.io.data.xml.XmlDataReader;
import cdc.util.strings.StringUtils;

public final class S1000DSupport {
    private S1000DSupport() {
    }

    private static final String DASH = "-";
    private static final Pattern SEP_PATTERN = Pattern.compile(" *\\| *");

    /**
     * Loads a DM XML file.
     * <p>
     * <b>WARNING:</b> external entities are ignored.
     *
     * @param file The XML file to load.
     * @return The root element of {@code file}.
     * @throws IOException When an IO error occurs.
     */
    public static Element loadDmRoot(File file) throws IOException {
        // External entities are ignored
        // TODO Support resolution of external entities
        return XmlDataReader.loadRoot(file,
                                      XmlDataReader.Feature.ALLOW_MIXED_CONTENT,
                                      XmlDataReader.Feature.DUMMY_ENTITY_RESOLVER);
    }

    /**
     * Builds the DMC string from the dmc element.
     *
     * @param element The DMC element.
     * @return The DMC base file name.
     */
    public static String getDmc(Element element) {
        final Element dmCode = element.getElement(S1000DNames.DM_REF,
                                                  S1000DNames.DM_REF_IDENT,
                                                  S1000DNames.DM_CODE);

        final StringBuilder builder = new StringBuilder();
        builder.append("DMC-")
               // Model Identification Code
               .append(dmCode.getAttributeValue(S1000DNames.MODEL_IDENT_CODE))
               .append(DASH)
               // System Difference Code
               .append(dmCode.getAttributeValue(S1000DNames.SYSTEM_DIFF_CODE))
               .append(DASH)
               // Standard Numbering System
               .append(dmCode.getAttributeValue(S1000DNames.SYSTEM_CODE))
               .append(DASH)
               .append(dmCode.getAttributeValue(S1000DNames.SUB_SYSTEM_CODE))
               .append(dmCode.getAttributeValue(S1000DNames.SUB_SUB_SYSTEM_CODE))
               .append(DASH)
               // Unit or Assembly
               .append(dmCode.getAttributeValue(S1000DNames.ASSY_CODE))
               .append(DASH)
               // Disassembly Code and Disassembly Code Variant
               .append(dmCode.getAttributeValue(S1000DNames.DISASSY_CODE))
               .append(dmCode.getAttributeValue(S1000DNames.DISASSY_CODE_VARIANT))
               .append(DASH)
               // Information Code and Information Code Variant
               .append(dmCode.getAttributeValue(S1000DNames.INFO_CODE))
               .append(dmCode.getAttributeValue(S1000DNames.INFO_CODE_VARIANT))
               .append(DASH)
               // Item Location Code
               .append(dmCode.getAttributeValue(S1000DNames.ITEM_LOCATION_CODE));
        return builder.toString();
    }

    /**
     * @param actFile The ACT file.
     * @param dmc The data module code
     * @return The file corresponding to {@code dmc}, if one is found,
     *         or {@code null} if none is found or too many are found.
     */
    public static File findDm(File actFile,
                              String dmc) {
        final File dir = actFile.getParentFile();
        final File[] files = dir.listFiles((d,
                                            name) -> name.startsWith(dmc));
        if (files.length == 1) {
            return files[0];
        } else {
            return null;
        }
    }

    public static S1000DProductIdentifier toS1000DProductIdentifier(String text) {
        if (S1000DNames.PRIMARY.equals(text)) {
            return S1000DProductIdentifier.PRIMARY;
        } else if (S1000DNames.SECONDARY.equals(text)) {
            return S1000DProductIdentifier.SECONDARY;
        } else {
            return S1000DProductIdentifier.NONE;
        }
    }

    /**
     * @param text The text (S1000D convention: '~' and '|').
     * @return An array of values and ranges.
     */
    public static String[] getLiterals(String text) {
        return SEP_PATTERN.split(text);
    }

    /**
     * Converts an S1000D string enumeration declaration to a Set.
     *
     * @param text The text (S1000D convention: '~' and '|').
     * @return The set corresponding to {@code text}.
     */
    public static SItemSet toStringSet(String text) {
        if (StringUtils.isNullOrEmpty(text)) {
            return StringSet.EMPTY;
        } else {
            return SItemSetUtils.createBest(List.of(getLiterals(text)).stream()
                                                .map(StringValue::of)
                                                .toList());
        }
    }

    /**
     * @param text The text (S1000D convention: '~' and '|').
     * @return The number domain corresponding to {@code text}.
     */
    public static String getNumberDomain(String text) {
        return text.replace("|", ",");
    }

    /**
     * Converts an S1000D number declaration to a Set.
     *
     * @param text The text (S1000D convention: '~' and '|').
     * @return The set corresponding to {@code text}.
     */
    public static SItemSet toNumberSet(String text) {
        return SItemSetUtils.createBest(getNumberDomain(text));
    }

    /**
     * @param kind The type kind.
     * @param text The text (S1000D convention: '~' and '|').
     * @return The set corresponding to {@code text} and {@code kind}.
     */
    public static SItemSet toBestSet(TypeKind kind,
                                     String text) {
        if (kind == null) {
            kind = TypeKind.STRING;
        }
        return switch (kind) {
        case STRING -> S1000DSupport.toStringSet(text);
        case BOOLEAN -> BooleanSet.FALSE_TRUE;
        case INTEGER, REAL -> S1000DSupport.toNumberSet(text);
        };
    }
}