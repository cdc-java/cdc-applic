package cdc.applic.s1000d.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cdc.applic.dictionaries.ConversionException;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.applic.dictionaries.types.StringType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.dictionaries.types.TypeKind;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Expressions;
import cdc.applic.expressions.content.RangeUtils;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.content.ValueUtils;
import cdc.applic.s1000d.S1000DToExpressionConverter;
import cdc.applic.s1000d.XmlSource;
import cdc.io.data.Element;
import cdc.util.lang.Checks;

/**
 * Implementation of {@link S1000DToExpressionConverter}.
 *
 * @author Damien Carbonne
 *
 * @param <E> The element type.
 */
public class S1000DToExpressionConverterImpl<E> implements S1000DToExpressionConverter<E> {
    private static final Expressions EXPRESSIONS = Expressions.SHORT_NARROW_NO_SIMPLIFY;
    private static final Pattern SPLIT_PATTERN = Pattern.compile("\\|");
    private static final Pattern RANGE_PATTERN = Pattern.compile("^(?<low>.*)~(?<high>.*)$");

    public enum PropertyKind {
        PRODUCT_ATTRIBUTE,
        CONDITION
    }

    public record IdKind(String id,
                         PropertyKind kind) {
    }

    /**
     * Interface describing information to retrieve to convert an S1000D Expression to an Expression.
     */
    public static interface S1000DPropertyInformer {
        /**
         * @param idk The S100D0 (identifier, kind) of the property.
         * @return {@code true} if {@code s1000dId} is the S1000D Id of a string property.
         */
        public boolean isS1000DIdKindOfStringProperty(IdKind idk);

        /**
         * @param idk The S100D0 (identifier, kind) of the property.
         * @return The name of the property whose S1000D Id is {@code s1000dId}.
         */
        public String getPropertyNameWithS1000DIdKind(IdKind idk);

        public static IllegalArgumentException unknownPropertyIdKind(IdKind idk) {
            return new IllegalArgumentException("Unknown property idk: " + idk);
        }

        public static IllegalArgumentException duplicatePropertyIdKind(IdKind idk) {
            return new IllegalArgumentException("Duplicate property idk: " + idk);
        }

        public static IllegalArgumentException duplicatePropertyName(String name,
                                                                     IdKind idk) {
            return new IllegalArgumentException("Duplicate property name: " + name + " " + idk);
        }
    }

    /**
     * The informer to use.
     */
    private final S1000DPropertyInformer informer;

    public S1000DToExpressionConverterImpl(S1000DPropertyInformer informer) {
        this.informer = informer;
    }

    /**
     * Creates a converter from a Dictionary.
     *
     * @param dictionary The {@link Dictionary}. It is used to retrieve type information.
     */
    public S1000DToExpressionConverterImpl(Dictionary dictionary) {
        this(new DictionaryInformer(dictionary));
    }

    /**
     * Creates a converter from an ACT file.
     *
     * @param actFile The ACT file. The CCT must be accessible.
     * @param transformTypes If {@code true}, some safe type transformations are performed.
     * @throws IOException When an IO error occurs.
     */
    public S1000DToExpressionConverterImpl(File actFile,
                                           boolean transformTypes)
            throws IOException {
        this(new ActCctInformer(actFile, transformTypes));
    }

    /**
     * Creates a converter from an ACT file.
     *
     * @param actFile The ACT file. The CCT must be accessible.
     * @throws IOException When an IO error occurs.
     */
    public S1000DToExpressionConverterImpl(File actFile) throws IOException {
        this(actFile, false);
    }

    @Override
    public Expression convertToExpression(E applic,
                                          XmlSource<E> source) {
        Checks.isNotNull(applic, "applic");
        Checks.isNotNull(source, "source");

        Expression result = null;
        boolean unexpected = false;

        for (final E child : source.getChildren(applic)) {
            if (S1000DNames.ASSERT.equals(source.getName(child))) {
                if (result == null) {
                    result = convertAssert(child, source);
                } else {
                    unexpected = true;
                }
            } else if (S1000DNames.EVALUATE.equals(source.getName(child))) {
                if (result == null) {
                    result = convertEvaluate(child, source);
                } else {
                    unexpected = true;
                }
            }
        }
        if (unexpected) {
            // Too many assert or evaluate children
            throw new ConversionException(source.getName(applic) + " has unexpected structure.");
        } else if (result == null) {
            throw new ConversionException(source.getName(applic) + " can not be converted.");
        } else {
            return result;
        }
    }

    private static PropertyKind convert(String propertyType) {
        return switch (propertyType) {
        case S1000DNames.PRODATTR -> PropertyKind.PRODUCT_ATTRIBUTE;
        case S1000DNames.CONDITION -> PropertyKind.CONDITION;
        default -> null;
        };
    }

    private Expression convertAssert(E element,
                                     XmlSource<E> source) {
        if (source.hasElementContent(element)) {
            final String content = source.getElementContent(element);
            return EXPRESSIONS.informal(content);
        } else {
            final String propertyIdent = source.getAttributeValue(element, S1000DNames.APPLIC_PROPERTY_IDENT, null);
            final String propertyType = source.getAttributeValue(element, S1000DNames.APPLIC_PROPERTY_TYPE, null);
            final String propertyValues = source.getAttributeValue(element, S1000DNames.APPLIC_PROPERTY_VALUES, null);
            final PropertyKind propertyKind = convert(propertyType);
            final IdKind idk = new IdKind(propertyIdent, propertyKind);
            final String propertyName = informer.getPropertyNameWithS1000DIdKind(idk);

            final SItemSet set = toSItemSet(idk, propertyValues);

            if (set.isSingleton()) {
                return EXPRESSIONS.equal(propertyName, set.getSingletonValue());
            } else {
                return EXPRESSIONS.in(propertyName, set);
            }
        }
    }

    private SItemSet toSItemSet(IdKind idk,
                                String text) {
        final boolean isString = informer.isS1000DIdKindOfStringProperty(idk);
        final List<SItem> items = new ArrayList<>();
        for (final String part : SPLIT_PATTERN.split(text)) {
            final SItem item = toSItem(part, isString);
            items.add(item);
        }
        return SItemSetUtils.createBest(items);
    }

    private static SItem toSItem(String text,
                                 boolean isString) {
        final Matcher m = RANGE_PATTERN.matcher(text);
        if (m.matches()) {
            final String low = m.group("low");
            final String high = m.group("high");
            return RangeUtils.create(toValue(low, isString), toValue(high, isString));
        } else {
            return toValue(text, isString);
        }
    }

    private static Value toValue(String text,
                                 boolean isString) {
        if (isString) {
            return ValueUtils.create("\"" + text + "\"");
        } else {
            return ValueUtils.create(text);
        }
    }

    private Expression convertEvaluate(E element,
                                       XmlSource<E> source) {
        final String andOr = source.getAttributeValue(element, S1000DNames.AND_OR, null);
        final List<Expression> children = new ArrayList<>();

        for (final E child : source.getChildren(element)) {
            if (S1000DNames.ASSERT.equals(source.getName(child))) {
                children.add(convertAssert(child, source));
            } else if (S1000DNames.EVALUATE.equals(source.getName(child))) {
                children.add(convertEvaluate(child, source));
            } else {
                throw new ConversionException("Conversion of " + source.getName(child) + " is not supported.");
            }
        }
        if (S1000DNames.AND.equals(andOr)) {
            return EXPRESSIONS.and(children);
        } else if (S1000DNames.OR.equals(andOr)) {
            return EXPRESSIONS.or(children);
        } else {
            throw new ConversionException("Unexpected evaluate@andOr value: " + andOr + ".");
        }
    }

    /**
     * Implementation of {@link S1000DPropertyInformer} based on a {@link Dictionary}.
     */
    private static final class DictionaryInformer implements S1000DPropertyInformer {
        /** ((S1000D Id, Kind), property) map. */
        private final Map<IdKind, Property> map = new HashMap<>();

        private static PropertyKind convert(S1000DPropertyType type) {
            if (type == null) {
                return null;
            } else {
                return switch (type) {
                case EXTERNAL_CONDITION, PRODUCT_CONDITION -> PropertyKind.CONDITION;
                case PRODUCT_ATTRIBUTE -> PropertyKind.PRODUCT_ATTRIBUTE;
                case UNDEFINED -> null;
                };
            }
        }

        public DictionaryInformer(Dictionary dictionary) {
            Checks.isNotNull(dictionary, "dictionary");
            for (final Property property : dictionary.getAllProperties()) {
                final S1000DProperty p = (S1000DProperty) property;
                final PropertyKind kind = convert(p.getS1000DPropertyType());
                if (kind != null) {
                    final String id = p.getS1000DId();
                    final IdKind idk = new IdKind(id, kind);
                    if (map.containsKey(idk)) {
                        throw S1000DPropertyInformer.duplicatePropertyIdKind(idk);
                    }
                    map.put(idk, property);
                }
            }
        }

        @Override
        public boolean isS1000DIdKindOfStringProperty(IdKind idk) {
            final Property property = map.get(idk);
            if (property == null) {
                throw S1000DPropertyInformer.unknownPropertyIdKind(idk);
            }
            final Type type = property.getType();
            return type instanceof StringType;
        }

        @Override
        public String getPropertyNameWithS1000DIdKind(IdKind idk) {
            final Property property = map.get(idk);
            if (property == null) {
                throw S1000DPropertyInformer.unknownPropertyIdKind(idk);
            }
            return property.getName().getProtectedLiteral();
        }
    }

    /**
     * Implementation of {@link S1000DPropertyInformer} based on ACT and CCT.
     */
    private static final class ActCctInformer implements S1000DPropertyInformer {
        private final S1000DTypeConverter converter;

        /** (Property (id, kind), Bucket) map. */
        private final Map<IdKind, Bucket> idkToBuckets = new HashMap<>();

        /** (Property name, Bucket) map. */
        private final Map<String, Bucket> nameToBuckets = new HashMap<>();

        /**
         * @param id The property id.
         * @param kind The property kind.
         * @param name The property name
         * @param isString Is the property a string?
         */
        private record Bucket(String id,
                              PropertyKind kind,
                              String name,
                              boolean isString) {
        }

        public ActCctInformer(File actFile,
                              boolean transformTypes)
                throws IOException {
            this.converter = transformTypes ? S1000DTypeConverter.ALL : S1000DTypeConverter.NONE;

            final Element root = S1000DSupport.loadDmRoot(actFile);

            // Retrieve the relevant ACT elements
            final Element content = root.getElementNamed(S1000DNames.CONTENT);
            final Element pal = content.getElement(S1000DNames.APPLIC_CROSS_REF_TABLE,
                                                   S1000DNames.PRODUCT_ATTRIBUTE_LIST);

            // analyze the product attribute list element
            loadProductAttributeList(pal);

            // Retrieve the CCT ref and search the corresponding file
            final Element cctRef = content.getElement(S1000DNames.APPLIC_CROSS_REF_TABLE,
                                                      S1000DNames.COND_CROSS_REF_TABLE_REF);
            final String cctDmc = S1000DSupport.getDmc(cctRef);
            final File cctFile = S1000DSupport.findDm(actFile, cctDmc);

            // Load the CCT file
            loadCct(cctFile);
        }

        private void loadCct(File file) throws IOException {
            final Element root = S1000DSupport.loadDmRoot(file);

            final Element content = root.getElementNamed(S1000DNames.CONTENT);
            final Element ctl = content.getElement(S1000DNames.COND_CROSS_REF_TABLE,
                                                   S1000DNames.COND_TYPE_LIST);
            final Element cl = content.getElement(S1000DNames.COND_CROSS_REF_TABLE,
                                                  S1000DNames.COND_LIST);

            // Set of condition type ids that are strings
            final Set<String> stringCondTypeIds = new HashSet<>();
            loadConditionTypeList(ctl, stringCondTypeIds);
            loadConditionList(cl, stringCondTypeIds);
        }

        private S1000DTypeConverter.Proposal loadType(Element child) {
            final String valueDataType = child.getAttributeValue(S1000DNames.VALUE_DATA_TYPE, S1000DNames.STRING);
            final String valuePattern = child.getAttributeValue(S1000DNames.VALUE_PATTERN, null);
            // There can be any number of enumeration
            // We merge them as if they all were defined in one element
            final StringBuilder enumerationValuesBuilder = new StringBuilder();
            for (final Element enumeration : child.getElementsNamed(S1000DNames.ENUMERATION)) {
                if (enumerationValuesBuilder.length() > 0) {
                    enumerationValuesBuilder.append("|");
                }
                enumerationValuesBuilder.append(enumeration.getAttributeValue(S1000DNames.APPLIC_PROPERTY_VALUES));
            }
            final String enumerationValues = enumerationValuesBuilder.toString();
            return converter.analyze(valueDataType, valuePattern, enumerationValues);
        }

        private void loadProductAttributeList(Element list) {
            final PropertyKind paKind = PropertyKind.PRODUCT_ATTRIBUTE;
            for (final Element child : list.getChildren(Element.class, Element.named(S1000DNames.PRODUCT_ATTRIBUTE))) {
                final String paName = child.getElementNamedText(S1000DNames.NAME);
                final String paId = child.getAttributeValue(S1000DNames.ID, null);

                final S1000DTypeConverter.Proposal proposal = loadType(child);
                final boolean typeIsString = proposal.getTargetType() == TypeKind.STRING;

                final IdKind idk = new IdKind(paId, paKind);
                if (idkToBuckets.containsKey(idk)) {
                    throw S1000DPropertyInformer.duplicatePropertyIdKind(idk);
                } else if (nameToBuckets.containsKey(paName)) {
                    throw S1000DPropertyInformer.duplicatePropertyName(paName, idk);
                } else {
                    final Bucket bucket = new Bucket(paId, paKind, paName, typeIsString);
                    idkToBuckets.put(idk, bucket);
                    nameToBuckets.put(paName, bucket);
                }
            }
        }

        /**
         * Load the condition types and stores ids of those that are string.
         *
         * @param list The condTypeList element.
         * @param stringCondTypeIds The set of condition type ids that are strings, filled by this method.
         */
        private void loadConditionTypeList(Element list,
                                           Set<String> stringCondTypeIds) {
            for (final Element child : list.getChildren(Element.class, Element.named(S1000DNames.COND_TYPE))) {
                final String condTypeId = child.getAttributeValue(S1000DNames.ID);

                final S1000DTypeConverter.Proposal proposal = loadType(child);
                final boolean condTypeIsString = proposal.getTargetType() == TypeKind.STRING;

                if (condTypeIsString) {
                    stringCondTypeIds.add(condTypeId);
                }
            }
        }

        private void loadConditionList(Element list,
                                       Set<String> stringCondTypeIds) {
            final PropertyKind condKind = PropertyKind.CONDITION;
            for (final Element child : list.getChildren(Element.class, Element.named(S1000DNames.COND))) {
                final String condTypeRefId = child.getAttributeValue(S1000DNames.COND_TYPE_REF_ID);
                final String condName = child.getElementNamedText(S1000DNames.NAME);
                final String condId = child.getAttributeValue(S1000DNames.ID, null);
                final boolean condIsString = stringCondTypeIds.contains(condTypeRefId);
                final IdKind idk = new IdKind(condId, condKind);
                if (idkToBuckets.containsKey(idk)) {
                    throw S1000DPropertyInformer.duplicatePropertyIdKind(idk);
                } else if (nameToBuckets.containsKey(condName)) {
                    throw S1000DPropertyInformer.duplicatePropertyName(condName, idk);
                } else {
                    final Bucket bucket = new Bucket(condId, condKind, condName, condIsString);
                    idkToBuckets.put(idk, bucket);
                    nameToBuckets.put(condName, bucket);
                }
            }
        }

        @Override
        public boolean isS1000DIdKindOfStringProperty(IdKind idk) {
            final Bucket bucket = idkToBuckets.get(idk);
            if (bucket == null) {
                throw S1000DPropertyInformer.unknownPropertyIdKind(idk);
            } else {
                return bucket.isString;
            }
        }

        @Override
        public String getPropertyNameWithS1000DIdKind(IdKind idk) {
            final Bucket bucket = idkToBuckets.get(idk);
            if (bucket == null) {
                throw S1000DPropertyInformer.unknownPropertyIdKind(idk);
            } else {
                return bucket.name;
            }
        }
    }
}