package cdc.applic.s1000d.core;

import java.io.IOException;

import cdc.applic.s1000d.XmlHandler;
import cdc.io.xml.XmlWriter;

/**
 * Implementation of {@link XmlHandler} that uses an {@link XmlWriter}.
 *
 * @author Damien Carbonne
 */
public class XmlWriterXmlHandler implements XmlHandler {
    private final XmlWriter writer;

    public XmlWriterXmlHandler(XmlWriter writer) {
        this.writer = writer;
    }

    public XmlWriter getWriter() {
        return writer;
    }

    @Override
    public void beginElement(String name) throws IOException {
        writer.beginElement(name);
    }

    @Override
    public void addAttribute(String name,
                             String value) throws IOException {
        writer.addAttribute(name, value);
    }

    @Override
    public void addElementContent(String content) throws IOException {
        writer.addElementContent(content);
    }

    @Override
    public void endElement() throws IOException {
        writer.endElement();
    }
}