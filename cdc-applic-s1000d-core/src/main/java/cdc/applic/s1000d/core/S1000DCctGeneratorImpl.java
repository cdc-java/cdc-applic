package cdc.applic.s1000d.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.dictionaries.s1000d.S1000DType;
import cdc.applic.dictionaries.types.DomainedType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.SItemSetUtils;
import cdc.applic.expressions.literals.Named;
import cdc.applic.projections.Axis;
import cdc.applic.projections.ExpressionProjector;
import cdc.applic.projections.Shadow;
import cdc.applic.projections.core.utils.AxisCut;
import cdc.applic.proofs.ProverMatching;
import cdc.applic.s1000d.S1000DCctGenerator;
import cdc.applic.s1000d.S1000DCctHandler;
import cdc.applic.s1000d.S1000DProfile;
import cdc.applic.s1000d.issues.S1000DIssueType;
import cdc.util.events.ProgressController;
import cdc.util.function.IterableUtils;

/**
 * Implementation of S1000DCctComputer.
 *
 * @author Damien Carbonne
 */
public class S1000DCctGeneratorImpl implements S1000DCctGenerator {
    private static final Comparator<Map.Entry<SItemSet, SItemSet>> VALUE_COMPARATOR =
            Comparator.comparing(Map.Entry::getValue, SItemSet.COMPARATOR);
    private static final Comparator<Map.Entry<SItemSet, SItemSet>> VALUE_KEY_COMPARATOR =
            VALUE_COMPARATOR.thenComparing(Map.Entry::getKey, SItemSet.COMPARATOR);

    public S1000DCctGeneratorImpl() {
        super();
    }

    @Override
    public void generateCct(DictionaryHandle handle,
                            Expression additionalAssertion,
                            NamingConvention convention,
                            S1000DCctHandler handler,
                            ProgressController controller,
                            S1000DProfile profile) {
        final Generator generator = new Generator(handle,
                                                  additionalAssertion,
                                                  convention,
                                                  handler,
                                                  controller,
                                                  profile);
        generator.execute();
    }

    private static class Generator extends S1000DAbstractGenerator<S1000DCctHandler> {
        public Generator(DictionaryHandle handle,
                         Expression additionalAssertion,
                         NamingConvention convention,
                         S1000DCctHandler handler,
                         ProgressController controller,
                         S1000DProfile profile) {
            super(handle, additionalAssertion, convention, handler, controller, profile);
        }

        @Override
        protected void execute() {
            handler.beginCct(handle, additionalAssertion, namingConvention);
            issue(S1000DIssueType.GENERATE_CCT, "Generate CCT for " + wrapDictionary(false));

            supplier.reset(IterableUtils.size(handle.getDictionary().getAllowedProperties()),
                           "Collect condition types");

            // Collect and sort condition types and conditions
            final Map<S1000DType, List<S1000DProperty>> typeToConditions = new HashMap<>();
            // List of conditions
            final List<S1000DProperty> conditions = new ArrayList<>();
            for (final Property property : handle.getDictionary().getAllowedProperties()) {
                if (property instanceof S1000DProperty) {
                    final S1000DProperty p = (S1000DProperty) property;
                    if (p.getS1000DPropertyType().isCctCandidate()) {
                        // List of conditions typed with type of p
                        final List<S1000DProperty> list = typeToConditions.computeIfAbsent(p.getType(), k -> new ArrayList<>());
                        list.add(p);
                        conditions.add(p);
                    }
                }
                supplier.incrementValue();
            }
            Collections.sort(conditions, Named.NAME_COMPARATOR);
            final List<S1000DType> types = new ArrayList<>(typeToConditions.keySet());
            Collections.sort(types, Type.NAME_COMPARATOR);

            // 1 incrementValue for each type and 1 for each condition (getPossibleDomain)
            supplier.reset(types.size() + (long) conditions.size(),
                           "Process condition types");

            // Generate condition types
            handler.beginCctConditionTypes();
            for (final S1000DType type : types) {
                final String pattern;
                final SItemSet domain;
                if (type instanceof DomainedType) {
                    pattern = null;
                    domain = getPossibleDomain(typeToConditions.get(type), type, supplier);
                    checkSupportOfRanges(type, domain);
                } else {
                    pattern = ((PatternType) type).getPattern().pattern();
                    domain = null;
                }

                handler.addCctConditionType(type, pattern, domain, namingConvention);
                supplier.incrementValue();
            }
            handler.endCctConditionTypes();

            if (profile.isEnabled(S1000DProfile.Hint.NO_CONDITION_DEPENDENCIES)) {
                supplier.reset(conditions.size(),
                               "Process conditions");
            } else {
                supplier.reset(conditions.size() * (1 + conditions.size()),
                               "Process conditions");
            }

            // Generate conditions and dependencies
            handler.beginCctConditions();
            for (final S1000DProperty condition : conditions) {
                supplier.setDetail("Process conditions (" + condition.getName() + ")");
                final SItemSet domain = getPossibleDomain(condition);
                handler.beginCctCondition(condition, domain, namingConvention);
                if (!profile.isEnabled(S1000DProfile.Hint.NO_CONDITION_DEPENDENCIES)) {
                    computeDependencies(condition, conditions);
                }
                handler.endCctCondition();
                supplier.incrementValue();
            }
            handler.endCctConditions();

            issue(S1000DIssueType.GENERATED_CCT, "Generated CCT for " + wrapDictionary(false));
            handler.endCct();
        }

        /**
         * Computes the dependencies of a condition.
         * <p>
         * We project {@code condition} on other {@code conditions}.<br>
         * If projected shadow contains a piece that is never true,
         * then (always or sometimes) pieces of the shadow are necessary.
         *
         * @param condition The condition.
         * @param dependencies All conditions.
         */
        private void computeDependencies(S1000DProperty condition,
                                         List<S1000DProperty> dependencies) {
            // Create a partition/cut of condition
            final AxisCut conditionCut = AxisCut.create(handle,
                                                        AssertionStrategy.INCLUDE_ASSERTIONS,
                                                        new Axis(condition),
                                                        TrueNode.INSTANCE);
            for (final S1000DProperty dependency : dependencies) {
                if (condition != dependency) {
                    // Projector on possible dependency
                    final ExpressionProjector projector = getProjectorIncludingAssertions(dependency);

                    // The dependency axis
                    final Axis dependencyAxis = new Axis(dependency);

                    // Map from dependency values to condition values
                    final Map<SItemSet, SItemSet> dependencyValuesToConditionValues = new HashMap<>();

                    for (int index = 0; index < conditionCut.size(); index++) {
                        // Project piece of condition onto possible dependency
                        final Shadow shadow = projector.project(conditionCut.getCutNode(index));

                        // Retrieve the set corresponding to never
                        final SItemSet never = shadow.getAxisSet(dependencyAxis, ProverMatching.NEVER);

                        if (!never.isEmpty()) {
                            // This dependency is relevant
                            final SItemSet alwaysOrSometimes =
                                    shadow.getAxisSet(dependencyAxis, ProverMatching.ALWAYS, ProverMatching.SOMETIMES);

                            // Converts the cut SItem to a set
                            final SItemSet newSet = SItemSetUtils.createBest(conditionCut.getCutSItem(index));
                            final SItemSet oldSet = dependencyValuesToConditionValues.get(alwaysOrSometimes);

                            if (oldSet == null) {
                                dependencyValuesToConditionValues.put(alwaysOrSometimes, newSet);
                            } else {
                                dependencyValuesToConditionValues.put(alwaysOrSometimes, oldSet.union(newSet));
                            }
                        }
                    }
                    // Sort result
                    final List<Map.Entry<SItemSet, SItemSet>> entries =
                            new ArrayList<>(dependencyValuesToConditionValues.entrySet());
                    entries.sort(VALUE_KEY_COMPARATOR);

                    if (!entries.isEmpty() && !profile.isSupported(S1000DProfile.Feature.CONDITION_DEPENDENCIES)) {
                        issue(S1000DIssueType.NON_SUPPORTED_FEATURE,
                              wrap(condition, true) + " has condition dependencies, which is not supported.",
                              condition);
                    }

                    // We can now transfer results
                    for (final Map.Entry<SItemSet, SItemSet> entry : entries) {
                        handler.addCctConditionDependency(condition,
                                                          entry.getValue(),
                                                          dependency,
                                                          entry.getKey(),
                                                          namingConvention);
                    }
                }
                supplier.incrementValue();
            }
        }
    }
}