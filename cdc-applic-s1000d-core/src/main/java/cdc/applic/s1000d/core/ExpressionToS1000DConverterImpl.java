package cdc.applic.s1000d.core;

import java.io.IOException;

import cdc.applic.dictionaries.ConversionException;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.applic.dictionaries.s1000d.S1000DType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.EnumeratedValue;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.AbstractPropertyNode;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.InformalNode;
import cdc.applic.expressions.ast.NaryAndNode;
import cdc.applic.expressions.ast.NaryOrNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.OrNode;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.expressions.content.SItem;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.StringValue;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.s1000d.ExpressionToS1000DConverter;
import cdc.applic.s1000d.XmlHandler;
import cdc.util.lang.Checks;

/**
 * Implementation of {@link ExpressionToS1000DConverter}.
 *
 * @author Damien Carbonne
 */
public class ExpressionToS1000DConverterImpl implements ExpressionToS1000DConverter {
    private final Dictionary dictionary;
    private final NamingConvention convention;

    /**
     * Creates an instance of ExpressionToS1000DConverterImpl.
     *
     * @param dictionary The {@link Dictionary}. It is used to retrieve type information.
     * @param convention The {@link NamingConvention}. It is used to use the appropriate convention.
     */
    public ExpressionToS1000DConverterImpl(Dictionary dictionary,
                                           NamingConvention convention) {
        Checks.isNotNull(dictionary, "dictionary");
        Checks.isNotNull(convention, "convention");

        this.dictionary = dictionary;
        this.convention = convention;
    }

    /**
     * Creates an instance of ExpressionToS1000DConverterImpl using default naming convention.
     *
     * @param dictionary The {@link Dictionary}. It is used to retrieve type information.
     */
    public ExpressionToS1000DConverterImpl(Dictionary dictionary) {
        this(dictionary, NamingConvention.DEFAULT);
    }

    /**
     * @return The used {@link Dictionary}.
     */
    public Dictionary getDictionary() {
        return dictionary;
    }

    /**
     * @return The used {@link NamingConvention}.
     */
    public NamingConvention getNamingConvention() {
        return convention;
    }

    @Override
    public void convertToS1000D(Expression expression,
                                XmlHandler handler) throws IOException {
        Checks.isNotNull(expression, "expression");
        Checks.isNotNull(handler, "handler");

        final Node node = ConvertToNary.execute(expression.getRootNode(),
                                                ConvertToNary.Variant.WHEN_NECESSARY);
        handler.beginElement(S1000DNames.APPLIC);
        process(node, handler);
        handler.endElement();
    }

    private void process(Node node,
                         XmlHandler handler) throws IOException {
        if (node instanceof final EqualNode n) {
            final Value value = n.getValue();
            handler.beginElement(S1000DNames.ASSERT);
            final S1000DType type = addAssertAttributes(n, handler);
            handler.addAttribute(S1000DNames.APPLIC_PROPERTY_VALUES, toS1000DValues(type, value));
            handler.endElement();
        } else if (node instanceof final InNode n) {
            final SItemSet set = n.getCheckedSet();
            handler.beginElement(S1000DNames.ASSERT);
            final S1000DType type = addAssertAttributes(n, handler);
            handler.addAttribute(S1000DNames.APPLIC_PROPERTY_VALUES, toS1000DValues(type, set));
            handler.endElement();
        } else if (node instanceof final InformalNode n) {
            handler.beginElement(S1000DNames.ASSERT);
            handler.addElementContent(n.getText().getNonEscapedText());
            handler.endElement();
        } else if (node instanceof final OrNode n) {
            handler.beginElement(S1000DNames.EVALUATE);
            handler.addAttribute(S1000DNames.AND_OR, S1000DNames.OR);
            process(n.getAlpha(), handler);
            process(n.getBeta(), handler);
            handler.endElement();
        } else if (node instanceof final NaryOrNode n) {
            handler.beginElement(S1000DNames.EVALUATE);
            handler.addAttribute(S1000DNames.AND_OR, S1000DNames.OR);
            for (final Node child : n.getChildren()) {
                process(child, handler);
            }
            handler.endElement();
        } else if (node instanceof final AndNode n) {
            handler.beginElement(S1000DNames.EVALUATE);
            handler.addAttribute(S1000DNames.AND_OR, S1000DNames.AND);
            process(n.getAlpha(), handler);
            process(n.getBeta(), handler);
            handler.endElement();
        } else if (node instanceof final NaryAndNode n) {
            handler.beginElement(S1000DNames.EVALUATE);
            handler.addAttribute(S1000DNames.AND_OR, S1000DNames.AND);
            for (final Node child : n.getChildren()) {
                process(child, handler);
            }
            handler.endElement();
        } else {
            throw new ConversionException("Cannot convert " + node.getKind()
                    + " to S1000D. Conversion of non supported features should be applied first.");
        }
    }

    /**
     * Converts a single value to S1000D syntax.
     * <p>
     * The target convention is used when relevant ({@code type} is an {@link EnumeratedType}).
     *
     * @param type The value type.
     * @param value The value.
     * @return The S1000D representation of {@code value}.
     */
    private String toS1000DValues(Type type,
                                  Value value) {
        if (type instanceof final EnumeratedType t) {
            final StringValue v = (StringValue) value;
            final EnumeratedValue ev = t.getValue(v);
            final StringValue targetValue = ev.getLiterals().getValue(convention);
            return targetValue.getNonEscapedLiteral();
        } else {
            return value.getNonEscapedLiteral();
        }
    }

    /**
     * Converts a set to S1000D syntax.
     * <p>
     * The target convention is used when relevant ({@code type} is an {@link EnumeratedType}).
     *
     * @param type The set type.
     * @param set The set.
     * @return The S1000D representation of {@code set}.
     */
    private String toS1000DValues(Type type,
                                  SItemSet set) {
        final StringBuilder builder = new StringBuilder();
        if (type instanceof final EnumeratedType t) {
            boolean first = true;
            for (final SItem item : set.getItems()) {
                if (first) {
                    first = false;
                } else {
                    builder.append("|");
                }
                final StringValue v = (StringValue) item;
                final EnumeratedValue ev = t.getValue(v);
                final StringValue targetValue = ev.getLiterals().getValue(convention);
                builder.append(targetValue.getNonEscapedLiteral());
            }
        } else {
            // This must work for values and ranges
            boolean first = true;
            for (final SItem item : set.getItems()) {
                if (first) {
                    first = false;
                } else {
                    builder.append("|");
                }
                builder.append(item.getNonEscapedLiteral());
            }
        }

        return builder.toString();
    }

    /**
     * Adds common assert attributes for a property: identifier and property type.
     *
     * @param node The property node.
     * @param handler The XML handler.
     * @return The node type.
     * @throws IOException When an IO error occurs.
     */
    private S1000DType addAssertAttributes(AbstractPropertyNode node,
                                           XmlHandler handler) throws IOException {
        final Name name = node.getName();
        final S1000DProperty property = (S1000DProperty) dictionary.getRegistry().getProperty(name);
        final String s1000DId = property.getS1000DId();
        final S1000DType s1000DType = property.getType();
        final S1000DPropertyType s1000DPropertyType = s1000DType.getS1000DPropertyType();

        // applic property ident
        handler.addAttribute(S1000DNames.APPLIC_PROPERTY_IDENT, s1000DId);

        // applic property type
        if (s1000DPropertyType == S1000DPropertyType.PRODUCT_ATTRIBUTE) {
            handler.addAttribute(S1000DNames.APPLIC_PROPERTY_TYPE, S1000DNames.PRODATTR);
        } else if (s1000DPropertyType == S1000DPropertyType.EXTERNAL_CONDITION ||
                s1000DPropertyType == S1000DPropertyType.PRODUCT_CONDITION) {
            handler.addAttribute(S1000DNames.APPLIC_PROPERTY_TYPE, S1000DNames.CONDITION);
        } else {
            throw new ConversionException("Cannot convert " + node.getKind()
                    + " to S1000D. S1000D Property Type is not set.");
        }

        return s1000DType;
    }
}