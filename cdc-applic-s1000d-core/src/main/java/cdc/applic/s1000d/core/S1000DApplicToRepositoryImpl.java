package cdc.applic.s1000d.core;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import cdc.applic.dictionaries.impl.AbstractTypeImpl;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.s1000d.S1000DProductIdentifier;
import cdc.applic.dictionaries.s1000d.S1000DPropertyType;
import cdc.applic.dictionaries.s1000d.S1000DType;
import cdc.io.data.Element;
import cdc.util.lang.UnexpectedValueException;
import cdc.util.strings.StringUtils;

/**
 * Utility to convert S1000D applicability DM (ACT and its related CCT) to a dictionary.
 * <p>
 * The obtained dictionary may need some adjustments.
 * For example, if pattern is used to represent a number, fixing the result may be necessary.
 * <p>
 * <b>LIMITATIONS:</b> currently, PCT is not analyzed and assertions are not generated.<br>
 * Product conditions are created, even for external conditions.
 * A condition, that is not listed in PCT should/could be considered as an external condition.
 *
 * @author Damien Carbonne
 */
public class S1000DApplicToRepositoryImpl {
    private final File actFile;
    private final Locale descriptionLocale;
    private final boolean transformTypes;
    private final RepositoryImpl repository = new RepositoryImpl();
    private final RegistryImpl registry;

    /**
     * Creates the converter.
     *
     * @param actFile The input ACT file.
     * @param descriptionLocale The locale to use for descriptions.
     * @param registryName The name of the created registry.
     * @param transformTypes If {@code true}, some common patterns are recognized and transformed to other types.
     */
    public S1000DApplicToRepositoryImpl(File actFile,
                                        Locale descriptionLocale,
                                        String registryName,
                                        boolean transformTypes) {
        this.actFile = actFile;
        this.descriptionLocale = descriptionLocale;
        this.transformTypes = transformTypes;
        this.registry = repository.registry().name(registryName).build();
    }

    /**
     * Do the conversion.
     * <p>
     * <b>WARNING:</b> this should be called once.
     *
     * @throws IOException When an IO error occurs.
     */
    public void execute() throws IOException {
        loadAct(actFile);
    }

    /**
     * @return The generated repository.
     */
    public RepositoryImpl getRepository() {
        return repository;
    }

    /**
     * Loads the ACT file and related CCT/PCT.
     *
     * @param file The ACT file.
     * @throws IOException When an IO error occurs.
     */
    private void loadAct(File file) throws IOException {
        // Load the ACT file
        final Element root = S1000DSupport.loadDmRoot(file);

        // Retrieve the relevant ACT elements
        final Element content = root.getElementNamed(S1000DNames.CONTENT);
        final Element pal = content.getElement(S1000DNames.APPLIC_CROSS_REF_TABLE,
                                               S1000DNames.PRODUCT_ATTRIBUTE_LIST);

        // analyze the product attribute list element
        loadProductAttributeList(pal);

        // Retrieve the CCT ref and search the corresponding file
        final Element cctRef = content.getElement(S1000DNames.APPLIC_CROSS_REF_TABLE,
                                                  S1000DNames.COND_CROSS_REF_TABLE_REF);
        final String cctDmc = S1000DSupport.getDmc(cctRef);
        final File cctFile = S1000DSupport.findDm(actFile, cctDmc);

        // Load the CCT file
        loadCct(cctFile);
    }

    private void loadCct(File file) throws IOException {
        final Element root = S1000DSupport.loadDmRoot(file);

        final Element content = root.getElementNamed(S1000DNames.CONTENT);
        final Element ctl = content.getElement(S1000DNames.COND_CROSS_REF_TABLE,
                                               S1000DNames.COND_TYPE_LIST);
        final Element cl = content.getElement(S1000DNames.COND_CROSS_REF_TABLE,
                                              S1000DNames.COND_LIST);

        loadConditionTypeList(ctl);
        loadConditionList(cl);
    }

    /**
     * Converts an element to a type.
     *
     * @param child The element.
     * @param productAttribute If {@code true}, a product attribute is created. Otherwise a product condition is created.
     * @return A new type.
     */
    private AbstractTypeImpl createType(Element child,
                                        boolean productAttribute) {
        final String s1000DId;
        final String typeName;
        final S1000DPropertyType propertyType;
        final S1000DProductIdentifier productIdentifier;
        if (productAttribute) {
            propertyType = S1000DPropertyType.PRODUCT_ATTRIBUTE;
            productIdentifier =
                    S1000DSupport.toS1000DProductIdentifier(child.getAttributeValue(S1000DNames.PRODUCT_IDENTIFIER, null));
            typeName = child.getElementNamedText(S1000DNames.NAME) + "Type";
            s1000DId = typeName + "Id";
        } else {
            propertyType = S1000DPropertyType.PRODUCT_CONDITION;
            productIdentifier = S1000DProductIdentifier.NOT_APPLICABLE;
            typeName = child.getElementNamedText(S1000DNames.NAME);
            s1000DId = child.getAttributeValue(S1000DNames.ID);
        }

        final String valueDataType = child.getAttributeValue(S1000DNames.VALUE_DATA_TYPE, S1000DNames.STRING);
        final String valuePattern = child.getAttributeValue(S1000DNames.VALUE_PATTERN, null);
        final String descr = child.getElementNamedText(S1000DNames.DESCR, null);

        // There can be any number of enumeration
        // We merge them as if they all were defined in one element
        final StringBuilder enumerationValuesBuilder = new StringBuilder();
        for (final Element enumeration : child.getElementsNamed(S1000DNames.ENUMERATION)) {
            if (enumerationValuesBuilder.length() > 0) {
                enumerationValuesBuilder.append("|");
            }
            enumerationValuesBuilder.append(enumeration.getAttributeValue(S1000DNames.APPLIC_PROPERTY_VALUES));
        }
        final String enumerationValues = enumerationValuesBuilder.toString();

        final S1000DTypeConverter.Proposal proposal;
        if (transformTypes) {
            proposal = S1000DTypeConverter.ALL.analyze(valueDataType, valuePattern, enumerationValues);
        } else {
            proposal = S1000DTypeConverter.NONE.analyze(valueDataType, valuePattern, enumerationValues);
        }

        if (proposal.isDifferent()) {
            return switch (proposal.getTargetType()) {
            case BOOLEAN -> registry.booleanType()
                                    .name(typeName)
                                    .s1000DId(s1000DId)
                                    .description(descriptionLocale, descr)
                                    .s1000DPropertyType(propertyType)
                                    .s1000DProductIdentifier(productIdentifier)
                                    .build();
            case INTEGER -> registry.integerType()
                                    .name(typeName)
                                    .s1000DId(s1000DId)
                                    .description(descriptionLocale, descr)
                                    .s1000DPropertyType(propertyType)
                                    .s1000DProductIdentifier(productIdentifier)
                                    .domain(proposal.getTargetDomain().orElseThrow().toIntegerSet())
                                    .build();
            default -> null;
            };
        } else {
            switch (valueDataType) {
            case S1000DNames.STRING:
                if (StringUtils.isNullOrEmpty(enumerationValues)) {
                    if (StringUtils.isNullOrEmpty(valuePattern)) {
                        // No pattern, no enumeration: any string pattern
                        return registry.patternType()
                                       .name(typeName)
                                       .s1000DId(s1000DId)
                                       .description(descriptionLocale, descr)
                                       .s1000DPropertyType(propertyType)
                                       .s1000DProductIdentifier(productIdentifier)
                                       .pattern(".*")
                                       .build();
                    } else {
                        return registry.patternType()
                                       .name(typeName)
                                       .s1000DId(s1000DId)
                                       .description(descriptionLocale, descr)
                                       .s1000DPropertyType(propertyType)
                                       .s1000DProductIdentifier(productIdentifier)
                                       .pattern(valuePattern)
                                       .build();
                    }
                } else {
                    // Enumeration: Enumerated type
                    // Silently ignore pattern?
                    return registry.enumeratedType()
                                   .name(typeName)
                                   .s1000DId(s1000DId)
                                   .description(descriptionLocale, descr)
                                   .s1000DPropertyType(propertyType)
                                   .s1000DProductIdentifier(productIdentifier)
                                   .literals(S1000DSupport.getLiterals(enumerationValues))
                                   .build();
                }

            case S1000DNames.BOOLEAN:
                // Boolean type
                return registry.booleanType()
                               .name(typeName)
                               .s1000DId(s1000DId)
                               .description(descriptionLocale, descr)
                               .s1000DPropertyType(propertyType)
                               .s1000DProductIdentifier(productIdentifier)
                               .build();

            case S1000DNames.INTEGER:
                // Integer type
                return registry.integerType()
                               .name(typeName)
                               .s1000DId(s1000DId)
                               .description(descriptionLocale, descr)
                               .s1000DPropertyType(propertyType)
                               .s1000DProductIdentifier(productIdentifier)
                               .domain(S1000DSupport.getNumberDomain(enumerationValues))
                               .build();

            case S1000DNames.REAL:
                // Real type
                return registry.realType()
                               .name(typeName)
                               .s1000DId(s1000DId)
                               .description(descriptionLocale, descr)
                               .s1000DPropertyType(propertyType)
                               .s1000DProductIdentifier(productIdentifier)
                               .domain(S1000DSupport.getNumberDomain(enumerationValues))
                               .build();

            default:
                throw new UnexpectedValueException(valueDataType);
            }
        }
    }

    private void loadProductAttributeList(Element list) {
        for (final Element child : list.getChildren(Element.class, Element.named(S1000DNames.PRODUCT_ATTRIBUTE))) {
            final String name = child.getElementNamedText(S1000DNames.NAME);
            final String s1000DId = child.getAttributeValue(S1000DNames.ID);
            // Create the type
            final AbstractTypeImpl type = createType(child, true);
            // Create the property
            registry.property()
                    .name(name)
                    .s1000DId(s1000DId)
                    .description(descriptionLocale, type.getDescription().getContent(descriptionLocale))
                    .type(type)
                    .build();
        }
    }

    private void loadConditionTypeList(Element list) {
        for (final Element child : list.getChildren(Element.class, Element.named(S1000DNames.COND_TYPE))) {
            // Create the type
            createType(child, false);
        }
    }

    private void loadConditionList(Element list) {
        for (final Element child : list.getChildren(Element.class, Element.named(S1000DNames.COND))) {
            final String condTypeRefId = child.getAttributeValue(S1000DNames.COND_TYPE_REF_ID);
            final String name = child.getElementNamedText(S1000DNames.NAME);
            final String s1000DId = child.getAttributeValue(S1000DNames.ID);
            final String descr = child.getElementNamedText(S1000DNames.DESCR, null);
            final S1000DType type = registry.getTypeWithS1000DId(condTypeRefId);

            // Create the property
            registry.property()
                    .name(name)
                    .s1000DId(s1000DId)
                    .description(descriptionLocale, descr)
                    .type((AbstractTypeImpl) type)
                    .build();
        }
    }
}