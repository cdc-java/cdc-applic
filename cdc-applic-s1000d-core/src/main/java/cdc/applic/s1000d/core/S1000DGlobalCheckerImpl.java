package cdc.applic.s1000d.core;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.dictionaries.s1000d.S1000DType;
import cdc.applic.dictionaries.types.BooleanType;
import cdc.applic.dictionaries.types.EnumeratedType;
import cdc.applic.dictionaries.types.IntegerType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.dictionaries.types.RealType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.s1000d.S1000DGlobalChecker;
import cdc.applic.s1000d.S1000DGlobalCheckerHandler;
import cdc.applic.s1000d.S1000DProfile;
import cdc.applic.s1000d.issues.S1000DIssueType;
import cdc.util.events.ProgressController;
import cdc.util.function.IterableUtils;

public class S1000DGlobalCheckerImpl implements S1000DGlobalChecker {
    public S1000DGlobalCheckerImpl() {
        super();
    }

    @Override
    public void check(DictionaryHandle handle,
                      Expression additionalAssertion,
                      NamingConvention convention,
                      S1000DGlobalCheckerHandler handler,
                      ProgressController controller,
                      S1000DProfile profile) {
        final Checker checker = new Checker(handle,
                                            additionalAssertion,
                                            convention,
                                            handler,
                                            controller,
                                            profile);
        checker.execute();
    }

    private static class Checker extends S1000DAbstractGenerator<S1000DGlobalCheckerHandler> {
        public Checker(DictionaryHandle handle,
                       Expression additionalAssertion,
                       NamingConvention convention,
                       S1000DGlobalCheckerHandler handler,
                       ProgressController controller,
                       S1000DProfile profile) {
            super(handle, additionalAssertion, convention, handler, controller, profile);
        }

        @Override
        protected void execute() {
            handler.beginGlobalChecks(handle, additionalAssertion);
            issue(S1000DIssueType.CHECK_DICTIONARY, "Check " + wrapDictionary(false));

            supplier.reset(IterableUtils.size(handle.getDictionary().getAllowedProperties()), "Global checks");

            // Check that all allowed properties have a compliant S1000D Property Type
            final List<Property> okProperties = new ArrayList<>();
            int total = 0;
            for (final Property property : handle.getDictionary().getAllowedProperties()) {
                total++;
                boolean ok = true;
                if (property instanceof final S1000DProperty p) {
                    if (!p.isS1000DCompliant()) {
                        issue(S1000DIssueType.NON_COMPLIANT_PROPERTY,
                              wrap(property, true) + " is not S1000D compliant.",
                              property);
                        ok = false;
                    }
                    checkTypeSupport(p);

                    final SItemSet possibleDomain = getPossibleDomain(property);
                    if (possibleDomain != null) {
                        if (possibleDomain.isEmpty()) {
                            issue(S1000DIssueType.CANNOT_SET_PROPERTY,
                                  usingAndAsserting() + ", " + wrap(property, false) + " can not be set.",
                                  property);
                            ok = false;
                        } else {
                            final SItemSet requiredDomain = getRequiredDomain(property);
                            final SItemSet excludedDomain = requiredDomain.remove(possibleDomain);
                            if (!excludedDomain.isEmpty()) {
                                issue(S1000DIssueType.EXCLUDED_VALUES,
                                      usingAndAsserting() + ", values " + excludedDomain + " are excluded for "
                                              + wrap(property, false) + ".",
                                      property);
                                ok = false;
                            }
                        }
                    }
                } else {
                    issue(S1000DIssueType.NON_S1000D_PROPERTY,
                          wrap(property, true) + " is not an S1000D property.",
                          property);
                    ok = false;
                }
                if (ok) {
                    okProperties.add(property);
                }
                supplier.incrementValue();
            }
            if (okProperties.isEmpty()) {
                issue(S1000DIssueType.CANNOT_SET_ANY_PROPERTY,
                      "Cannnot set any property.",
                      (Property) null);
            } else {
                issue(S1000DIssueType.NON_RESTRICTED_PROPERTIES,
                      usingAndAsserting() + ", " + okProperties.size() + "/" + total
                              + " properties can be set without any restriction ("
                              + okProperties.stream().map(p -> p.getName().toString()).sorted().collect(Collectors.joining(", "))
                              + ").",
                      (Property) null);
            }

            issue(S1000DIssueType.CHECKED_DICTIONARY, "Checked " + wrapDictionary(false));
            handler.endGlobalChecks();
        }

        private static String nonSupported(Type type,
                                           String typename) {
            return wrap(type, true) + " is " + typename + " type, which is not supported.";
        }

        private void checkTypeSupport(S1000DProperty property) {
            final S1000DType type = property.getType();
            if (type instanceof BooleanType && !profile.isSupported(S1000DProfile.Feature.BOOLEAN_TYPES)) {
                issue(S1000DIssueType.NON_SUPPORTED_FEATURE,
                      nonSupported(type, "a boolean"),
                      property);
            } else if (type instanceof IntegerType && !profile.isSupported(S1000DProfile.Feature.INTEGER_TYPES)) {
                issue(S1000DIssueType.NON_SUPPORTED_FEATURE,
                      nonSupported(type, "an integer"),
                      property);
            } else if (type instanceof RealType && !profile.isSupported(S1000DProfile.Feature.REAL_TYPES)) {
                issue(S1000DIssueType.NON_SUPPORTED_FEATURE,
                      nonSupported(type, "a real"),
                      property);
            } else if (type instanceof EnumeratedType && !profile.isSupported(S1000DProfile.Feature.STRING_TYPES_ENUMS)) {
                issue(S1000DIssueType.NON_SUPPORTED_FEATURE,
                      nonSupported(type, "an enumerated"),
                      property);
            } else if (type instanceof PatternType && !profile.isSupported(S1000DProfile.Feature.STRING_TYPES_PATTERNS)) {
                issue(S1000DIssueType.NON_SUPPORTED_FEATURE,
                      nonSupported(type, "a pattern"),
                      property);
            }
        }
    }
}