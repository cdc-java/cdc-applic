package cdc.applic.s1000d.core;

import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import cdc.applic.dictionaries.types.TypeKind;
import cdc.applic.expressions.content.BooleanSet;
import cdc.applic.expressions.content.IntegerSet;
import cdc.applic.expressions.content.SItemSet;
import cdc.util.lang.Checks;
import cdc.util.strings.StringUtils;

/**
 * Class used to recognize some S1000D type definitions that could be defined in a better way.
 * <p>
 * This will only detect some specific declarations.
 * There are infinite possibilities to declare things in a wrong way.<br>
 * Only situations when changing type declaration does not change its usage are handled.<br>
 * Examples of detected situations:
 * <ul>
 * <li>pattern \d : integer 0~9
 * <li>pattern [0-9] : integer 0~9
 * <li>pattern \d{1,3} : integer 0~999
 * <li>pattern [0-9]{1,2} : integer 0~99
 * <li>enumeration true|false : boolean
 * <li>enumeration FALSE|TRUE : boolean
 * </ul>
 */
public final class S1000DTypeConverter {
    /** Pattern like <code>\d{1,4}</code> */
    private static final Pattern P2I_PATTERN1 =
            Pattern.compile("^(\\\\d|\\[0\\-9\\])\\{(?<min>\\d),(?<max>\\d)\\}$");
    /** Pattern like <code>\d</code> */
    private static final Pattern P2I_PATTERN2 =
            Pattern.compile("^((\\\\d)|(\\[0\\-9\\]))$");
    /** Enumeration like <code>0|1</code> or <code>1|0</code> */
    private static final Pattern E2B_PATTERN1 =
            Pattern.compile("^(([tT][rR][uU][eE]\\|[fF][aA][lL][sS][eE])|([fF][aA][lL][sS][eE]\\|[tT][rR][uU][eE]))$");

    private static final List<Function<String, Proposal>> P2I =
            List.of(S1000DTypeConverter::tryP2I1,
                    S1000DTypeConverter::tryP2I2);
    private static final List<Function<String, Proposal>> E2B =
            List.of(S1000DTypeConverter::tryE2B1);

    private final Set<Hint> hints;

    /**
     * Instance of {@link S1000DTypeConverter} that applies all available transformations.
     */
    public static final S1000DTypeConverter ALL =
            S1000DTypeConverter.builder()
                               .allHints()
                               .build();

    /**
     * Instance of {@link S1000DTypeConverter} that does nothing.
     */
    public static final S1000DTypeConverter NONE =
            S1000DTypeConverter.builder()
                               .build();

    /**
     * Description of the conversion proposal.
     */
    public static interface Proposal {
        /**
         * @return {@code true} if the proposal is different from original data.
         */
        public boolean isDifferent();

        /**
         * @return The target type.
         */
        public TypeKind getTargetType();

        /**
         * @return The target domain proposal.
         *         In that case, the target type is anything.
         */
        public Optional<SItemSet> getTargetDomain();

        /**
         * @return The target pattern.
         *         In that case, the target type is a string.
         */
        public Optional<String> getTargetPattern();
    }

    /**
     * Creates a proposal.
     *
     * @param different Is this proposal different?
     * @param kind The target type.
     * @param domain The target domain (may be null).
     * @param pattern The target pattern (may be null).
     * @return A new instance of proposal.
     */
    static Proposal of(boolean different,
                       TypeKind kind,
                       String domain,
                       String pattern) {
        return new ProposalImpl(different,
                                kind == null ? TypeKind.STRING : kind,
                                S1000DSupport.toBestSet(kind, domain),
                                pattern);
    }

    public enum Hint {
        /** Recognize some integer-like patterns. */
        PATTERN_TO_INTEGER,
        /** Recognize some boolean-like enumerations. */
        ENUMERATION_TO_BOOLEAN
    }

    private S1000DTypeConverter(Builder builder) {
        this.hints = EnumSet.copyOf(builder.hints);
    }

    /**
     * @return The enabled hints.
     */
    public Set<Hint> getHints() {
        return hints;
    }

    /**
     * Analyzes data and makes a proposal.
     *
     * @param valueDataType The S1000D value data type (null, string, boolean, ...)
     * @param valuePattern The S1000D value pattern.
     * @param applicPropertyValues The list of S1000D applic property values.
     * @return The conversion.
     */
    public Proposal analyze(String valueDataType,
                            String valuePattern,
                            List<String> applicPropertyValues) {
        Proposal result = null;

        final TypeKind sourceTypeKind = toTypeKind(valueDataType);
        final String enumerationValues =
                applicPropertyValues.stream()
                                    .collect(Collectors.joining("|"));
        if (!hints.isEmpty() && sourceTypeKind == TypeKind.STRING) {
            if (!StringUtils.isNullOrEmpty(valuePattern)) {
                if (hints.contains(Hint.PATTERN_TO_INTEGER)) {
                    result = tryAll(valuePattern, P2I);
                }
            } else if (!StringUtils.isNullOrEmpty(enumerationValues)
                    && hints.contains(Hint.ENUMERATION_TO_BOOLEAN)) {
                result = tryAll(enumerationValues, E2B);
            }
        }

        if (result == null) {
            return new ProposalImpl(false,
                                    sourceTypeKind,
                                    S1000DSupport.toBestSet(sourceTypeKind, enumerationValues),
                                    valuePattern);
        } else {
            return result;
        }
    }

    /**
     * Analyzes data and makes a proposal.
     *
     * @param valueDataType The S1000D value data type (null, string, boolean, ...)
     * @param valuePattern The S1000D value pattern.
     * @param applicPropertyValues The S1000D applic property values.
     * @return The conversion.
     */
    public Proposal analyze(String valueDataType,
                            String valuePattern,
                            String... applicPropertyValues) {
        return analyze(valueDataType, valuePattern, List.of(applicPropertyValues));
    }

    private static Proposal tryAll(String s,
                                   List<Function<String, Proposal>> l) {
        for (final Function<String, Proposal> f : l) {
            final Proposal result = f.apply(s);
            if (result != null) {
                return result;
            }
        }
        return null;
    }

    private static Proposal tryP2I1(String valuePattern) {
        final Matcher m = P2I_PATTERN1.matcher(valuePattern);
        if (m.matches()) {
            // min is ignored:
            // e.g., if min is 3, one could write 000 instead of 0
            final int max = Integer.parseInt(m.group("max"));
            final int high = ((int) Math.pow(10.0, max)) - 1;

            return new ProposalImpl(true, TypeKind.INTEGER, IntegerSet.of("0~" + high), null);
        } else {
            return null;
        }
    }

    private static Proposal tryP2I2(String valuePattern) {
        final Matcher m = P2I_PATTERN2.matcher(valuePattern);
        if (m.matches()) {
            return new ProposalImpl(true, TypeKind.INTEGER, IntegerSet.of("0~9"), null);
        } else {
            return null;
        }
    }

    private static Proposal tryE2B1(String enumerationValues) {
        final Matcher m = E2B_PATTERN1.matcher(enumerationValues);
        if (m.matches()) {
            return new ProposalImpl(true, TypeKind.BOOLEAN, BooleanSet.FALSE_TRUE, null);
        } else {
            return null;
        }
    }

    private static TypeKind toTypeKind(String valueDataType) {
        if (valueDataType == null) {
            return TypeKind.STRING;
        } else {
            return switch (valueDataType) {
            case S1000DNames.STRING -> TypeKind.STRING;
            case S1000DNames.INTEGER -> TypeKind.INTEGER;
            case S1000DNames.REAL -> TypeKind.REAL;
            case S1000DNames.BOOLEAN -> TypeKind.BOOLEAN;
            default -> throw new IllegalArgumentException("Invalid value data type: " + valueDataType);
            };
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private final Set<Hint> hints = EnumSet.noneOf(Hint.class);

        private Builder() {
        }

        public Builder hint(Hint hint) {
            this.hints.add(hint);
            return this;
        }

        public Builder hint(Hint hint,
                            boolean enabled) {
            if (enabled) {
                this.hints.add(hint);
            } else {
                this.hints.remove(hint);
            }
            return this;
        }

        public Builder allHints() {
            Collections.addAll(hints, Hint.values());
            return this;
        }

        public S1000DTypeConverter build() {
            return new S1000DTypeConverter(this);
        }
    }
}

record ProposalImpl(boolean different,
                    TypeKind targetType,
                    SItemSet targetDomain,
                    String targetPattern)
        implements S1000DTypeConverter.Proposal {
    public ProposalImpl {
        Checks.isNotNull(targetType, "targetType");
    }

    @Override
    public boolean isDifferent() {
        return different;
    }

    @Override
    public TypeKind getTargetType() {
        return targetType;
    }

    @Override
    public Optional<SItemSet> getTargetDomain() {
        return Optional.ofNullable(targetDomain);
    }

    @Override
    public Optional<String> getTargetPattern() {
        return Optional.ofNullable(targetPattern);
    }
}