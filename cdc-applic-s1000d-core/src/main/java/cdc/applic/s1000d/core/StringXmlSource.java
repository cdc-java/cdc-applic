package cdc.applic.s1000d.core;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import cdc.applic.s1000d.XmlSource;
import cdc.io.data.Element;
import cdc.io.data.xml.XmlDataReader;

/**
 * Implementation of {@link XmlSource} based on {@link Element} and initialized from a String.
 *
 * @author Damien Carbonne
 */
public class StringXmlSource extends DataXmlSource {
    private final String text;
    private final Charset charset;
    private final Element root;

    public StringXmlSource(String text,
                           Charset charset)
            throws IOException {
        this.text = text;
        this.charset = charset;
        this.root = XmlDataReader.loadRoot(text, charset);
    }

    public StringXmlSource(String text) throws IOException {
        this(text, StandardCharsets.UTF_8);
    }

    public String getText() {
        return text;
    }

    public Charset getCharset() {
        return charset;
    }

    /**
     * @return The root element.
     */
    public Element getRoot() {
        return root;
    }
}