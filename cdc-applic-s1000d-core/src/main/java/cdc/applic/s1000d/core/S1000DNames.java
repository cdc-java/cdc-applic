package cdc.applic.s1000d.core;

public final class S1000DNames {
    private S1000DNames() {
    }

    public static final String AND = "and";
    public static final String AND_OR = "andOr";
    public static final String APPLIC = "applic";
    public static final String APPLIC_CROSS_REF_TABLE = "applicCrossRefTable";
    public static final String APPLIC_PROPERTY_IDENT = "applicPropertyIdent";
    public static final String APPLIC_PROPERTY_TYPE = "applicPropertyType";
    public static final String APPLIC_PROPERTY_VALUES = "applicPropertyValues";
    public static final String ASSERT = "assert";
    public static final String ASSY_CODE = "assyCode";
    public static final String BOOLEAN = "boolean";
    public static final String COND = "cond";
    public static final String CONDITION = "condition";
    public static final String COND_CROSS_REF_TABLE = "condCrossRefTable";
    public static final String COND_CROSS_REF_TABLE_REF = "condCrossRefTableRef";
    public static final String COND_LIST = "condList";
    public static final String COND_TYPE = "condType";
    public static final String COND_TYPE_LIST = "condTypeList";
    public static final String COND_TYPE_REF_ID = "condTypeRefId";
    public static final String CONTENT = "content";
    public static final String DESCR = "descr";
    public static final String DISASSY_CODE = "disassyCode";
    public static final String DISASSY_CODE_VARIANT = "disassyCodeVariant";
    public static final String DMODULE = "dmodule";
    public static final String DM_CODE = "dmCode";
    public static final String DM_REF = "dmRef";
    public static final String DM_REF_IDENT = "dmRefIdent";
    public static final String ENUMERATION = "enumeration";
    public static final String EVALUATE = "evaluate";
    public static final String ID = "id";
    public static final String INFO_CODE = "infoCode";
    public static final String INFO_CODE_VARIANT = "infoCodeVariant";
    public static final String INTEGER = "integer";
    public static final String ITEM_LOCATION_CODE = "itemLocationCode";
    public static final String MODEL_IDENT_CODE = "modelIdentCode";
    public static final String NAME = "name";
    public static final String OR = "or";
    public static final String PRIMARY = "primary";
    public static final String PRODATTR = "prodattr";
    public static final String PRODUCT_ATTRIBUTE = "productAttribute";
    public static final String PRODUCT_ATTRIBUTE_LIST = "productAttributeList";
    public static final String PRODUCT_IDENTIFIER = "productIdentifier";
    public static final String REAL = "real";
    public static final String SECONDARY = "secondary";
    public static final String SUB_SUB_SYSTEM_CODE = "subSubSystemCode";
    public static final String SUB_SYSTEM_CODE = "subSystemCode";
    public static final String STRING = "string";
    public static final String SYSTEM_CODE = "systemCode";
    public static final String SYSTEM_DIFF_CODE = "systemDiffCode";
    public static final String VALUE_DATA_TYPE = "valueDataType";
    public static final String VALUE_PATTERN = "valuePattern";

}