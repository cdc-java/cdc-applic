package cdc.applic.s1000d.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cdc.applic.dictionaries.NamingConvention;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.s1000d.S1000DProperty;
import cdc.applic.dictionaries.s1000d.S1000DType;
import cdc.applic.dictionaries.types.DomainedType;
import cdc.applic.dictionaries.types.PatternType;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.literals.Named;
import cdc.applic.s1000d.S1000DActGenerator;
import cdc.applic.s1000d.S1000DActHandler;
import cdc.applic.s1000d.S1000DProfile;
import cdc.applic.s1000d.issues.S1000DIssueType;
import cdc.util.events.ProgressController;
import cdc.util.function.IterableUtils;

/**
 * Implementation of S1000DActComputer.
 *
 * @author Damien Carbonne
 */
public class S1000DActGeneratorImpl implements S1000DActGenerator {
    public S1000DActGeneratorImpl() {
        super();
    }

    @Override
    public void generateAct(DictionaryHandle handle,
                            Expression additionalAssertion,
                            NamingConvention convention,
                            S1000DActHandler handler,
                            ProgressController controller,
                            S1000DProfile profile) {
        final Generator generator = new Generator(handle,
                                                  additionalAssertion,
                                                  convention,
                                                  handler,
                                                  controller,
                                                  profile);
        generator.execute();
    }

    private static class Generator extends S1000DAbstractGenerator<S1000DActHandler> {
        public Generator(DictionaryHandle handle,
                         Expression additionalAssertion,
                         NamingConvention convention,
                         S1000DActHandler handler,
                         ProgressController controller,
                         S1000DProfile profile) {
            super(handle, additionalAssertion, convention, handler, controller, profile);
        }

        @Override
        protected void execute() {
            handler.beginAct(handle, additionalAssertion, namingConvention);
            issue(S1000DIssueType.GENERATE_ACT, "Generate ACT for " + wrapDictionary(false));

            supplier.reset(IterableUtils.size(handle.getDictionary().getAllowedProperties()),
                           "Collect product attributes");

            // Collect and sort product attributes
            final List<S1000DProperty> properties = new ArrayList<>();
            for (final Property property : handle.getDictionary().getAllowedProperties()) {
                if (property instanceof final S1000DProperty p && p.getS1000DPropertyType().isActCandidate()) {
                    properties.add(p);
                }
                supplier.incrementValue();
            }
            Collections.sort(properties, Named.NAME_COMPARATOR);

            supplier.reset(properties.size(),
                           "Process product attributes");
            // Iterate on all product attributes
            for (final S1000DProperty property : properties) {
                final S1000DType type = property.getType();
                final String pattern;
                final SItemSet domain;
                if (type instanceof DomainedType) {
                    pattern = null;
                    domain = getPossibleDomain(property);
                    checkSupportOfRanges(property, domain);
                } else {
                    pattern = ((PatternType) type).getPattern().pattern();
                    domain = null;
                }

                handler.addActProductAttribute(property, pattern, domain, namingConvention);
                supplier.incrementValue();
            }

            issue(S1000DIssueType.GENERATED_ACT, "Generated ACT for " + wrapDictionary(false));
            handler.endAct();
        }
    }
}