package cdc.applic.s1000d.core;

import cdc.applic.s1000d.XmlSource;
import cdc.io.data.Element;

/**
 * Implementation of {@link XmlSource} based on {@link Element}.
 *
 * @author Damien Carbonne
 */
public class DataXmlSource implements XmlSource<Element> {
    public DataXmlSource() {
        super();
    }

    @Override
    public Iterable<Element> getChildren(Element parent) {
        return parent.getChildren(Element.class);
    }

    @Override
    public String getName(Element element) {
        return element.getName();
    }

    @Override
    public String getAttributeValue(Element element,
                                    String name,
                                    String def) {
        return element.getAttributeValue(name, def);
    }

    @Override
    public String getElementContent(Element element) {
        return element.getText();
    }
}