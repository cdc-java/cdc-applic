package cdc.applic.predicates;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.ProverMatching;

class ProverMatchingPredicateTest {
    private final RepositorySupport support = new RepositorySupport();

    public ProverMatchingPredicateTest() {
        support.registry.alias().name("A").expression("E=E1 | E=E2").build();
    }

    @Test
    void testEmpty() {
        final ProverMatchingPredicate predicate = new ProverMatchingPredicate(support.registryHandle,
                                                                              true,
                                                                              new Expression("E=E1"),
                                                                              ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES);
        assertEquals(support.registryHandle, predicate.getDictionaryHandle());
        assertEquals(true, predicate.getDefault());
        assertTrue(predicate.getProverMatchings().isEmpty());

        assertTrue(predicate.test("true"));
        assertTrue(predicate.test("E=E1"));
        assertTrue(predicate.test("P=AA or A or E=E1"));
        assertEquals(new Expression("E=E1"), predicate.getFilter());
        assertEquals(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES, predicate.getProverFeatures());
    }

    @Test
    void testNever() {
        final ProverMatchingPredicate predicate = new ProverMatchingPredicate(support.registryHandle,
                                                                              true,
                                                                              new Expression("E=E1"),
                                                                              ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES,
                                                                              ProverMatching.NEVER);
        assertEquals(support.registryHandle, predicate.getDictionaryHandle());
        assertEquals(true, predicate.getDefault());
        assertSame(1, predicate.getProverMatchings().size());

        assertFalse(predicate.test("true"));
        assertFalse(predicate.test("E=E1"));
        assertFalse(predicate.test("P=AA or A or E=E1"));
    }

    @Test
    void testSometimes() {
        final ProverMatchingPredicate predicate = new ProverMatchingPredicate(support.registryHandle,
                                                                              true,
                                                                              new Expression("E=E1"),
                                                                              ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES,
                                                                              ProverMatching.SOMETIMES);
        assertEquals(support.registryHandle, predicate.getDictionaryHandle());
        assertEquals(true, predicate.getDefault());
        assertSame(1, predicate.getProverMatchings().size());

        assertTrue(predicate.test("true"));
        assertTrue(predicate.test("E=E1"));
        assertTrue(predicate.test("P=AA or A or E=E1"));
    }

    @Test
    void testAlways() {
        final ProverMatchingPredicate predicate = new ProverMatchingPredicate(support.registryHandle,
                                                                              true,
                                                                              new Expression("E=E1"),
                                                                              ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES,
                                                                              ProverMatching.ALWAYS);
        assertEquals(support.registryHandle, predicate.getDictionaryHandle());
        assertEquals(true, predicate.getDefault());
        assertSame(1, predicate.getProverMatchings().size());

        assertFalse(predicate.test("true"));
        assertFalse(predicate.test("E=E1"));
        assertFalse(predicate.test("P=AA or A or E=E1"));
    }

    @Test
    void testNeverSometimes() {
        final ProverMatchingPredicate predicate = new ProverMatchingPredicate(support.registryHandle,
                                                                              true,
                                                                              new Expression("E=E1"),
                                                                              ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES,
                                                                              ProverMatching.NEVER,
                                                                              ProverMatching.SOMETIMES);
        assertEquals(support.registryHandle, predicate.getDictionaryHandle());
        assertEquals(true, predicate.getDefault());
        assertSame(2, predicate.getProverMatchings().size());

        assertTrue(predicate.test("true"));
        assertTrue(predicate.test("E=E1"));
        assertTrue(predicate.test("P=AA or A or E=E1"));
    }

    @Test
    void testNeverAlways() {
        final ProverMatchingPredicate predicate = new ProverMatchingPredicate(support.registryHandle,
                                                                              true,
                                                                              new Expression("E=E1"),
                                                                              ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES,
                                                                              ProverMatching.NEVER,
                                                                              ProverMatching.ALWAYS);
        assertEquals(support.registryHandle, predicate.getDictionaryHandle());
        assertEquals(true, predicate.getDefault());
        assertSame(2, predicate.getProverMatchings().size());

        assertFalse(predicate.test("true"));
        assertFalse(predicate.test("E=E1"));
        assertFalse(predicate.test("P=AA or A or E=E1"));
    }

    @Test
    void testSometimesAlways() {
        final ProverMatchingPredicate predicate = new ProverMatchingPredicate(support.registryHandle,
                                                                              true,
                                                                              new Expression("E=E1"),
                                                                              ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES,
                                                                              ProverMatching.SOMETIMES,
                                                                              ProverMatching.ALWAYS);
        assertEquals(support.registryHandle, predicate.getDictionaryHandle());
        assertEquals(true, predicate.getDefault());
        assertSame(2, predicate.getProverMatchings().size());

        assertTrue(predicate.test("true"));
        assertTrue(predicate.test("E=E1"));
        assertTrue(predicate.test("P=AA or A or E=E1"));
    }

    @Test
    void testNeverSometimesAlways() {
        final ProverMatchingPredicate predicate = new ProverMatchingPredicate(support.registryHandle,
                                                                              true,
                                                                              new Expression("E=E1"),
                                                                              ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES,
                                                                              ProverMatching.NEVER,
                                                                              ProverMatching.SOMETIMES,
                                                                              ProverMatching.ALWAYS);
        assertEquals(support.registryHandle, predicate.getDictionaryHandle());
        assertEquals(true, predicate.getDefault());
        assertSame(3, predicate.getProverMatchings().size());

        assertTrue(predicate.test("true"));
        assertTrue(predicate.test("E=E1"));
        assertTrue(predicate.test("P=AA or A or E=E1"));
    }
}