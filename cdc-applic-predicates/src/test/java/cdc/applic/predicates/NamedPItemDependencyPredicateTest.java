package cdc.applic.predicates;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.literals.Name;

class NamedDItemDependencyPredicateTest {
    private final RepositorySupport support = new RepositorySupport();

    public NamedDItemDependencyPredicateTest() {
        support.registry.alias().name("A").expression("E=E1 | E=E2").build();
    }

    @Test
    void testEmpty() {
        final NamedDItemDependencyPredicate predicate = new NamedDItemDependencyPredicate(support.registryHandle,
                                                                                          true);
        assertEquals(support.registryHandle, predicate.getDictionaryHandle());
        assertEquals(true, predicate.getDefault());
        assertTrue(predicate.getItems().isEmpty());

        assertTrue(predicate.test("true"));
        assertTrue(predicate.test("E=E1"));
        assertTrue(predicate.test("P=AA or A or E=E1"));
        assertTrue(predicate.test("P=AA or A"));
        assertTrue(predicate.test("P=AA"));
    }

    @Test
    void test() {
        final NamedDItemDependencyPredicate predicate = new NamedDItemDependencyPredicate(support.registryHandle,
                                                                                          false,
                                                                                          Name.of("E"));
        assertEquals(support.registryHandle, predicate.getDictionaryHandle());
        assertEquals(false, predicate.getDefault());
        assertSame(1, predicate.getItems().size());

        assertFalse(predicate.test("true"));
        assertTrue(predicate.test("E=E1"));
        assertTrue(predicate.test("P=AA or A or E=E1"));
        assertFalse(predicate.test("P=AA"));
    }
}