package cdc.applic.predicates;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.literals.Name;

class TypeDependencyPredicateTest {
    private final RepositorySupport support = new RepositorySupport();

    public TypeDependencyPredicateTest() {
        support.registry.alias().name("A").expression("E=E1 | E=E2").build();
        support.registry.alias().name("A1").expression("B").build();
        support.registry.alias().name("A2").expression("A1").build();

        support.registry.createAssertion("B or A");
    }

    @Test
    void testEmpty() {
        final TypeDependencyPredicate predicate = new TypeDependencyPredicate(support.registryHandle,
                                                                              true);
        assertEquals(support.registryHandle, predicate.getDictionaryHandle());
        assertEquals(true, predicate.getDefault());
        assertTrue(predicate.getTypes().isEmpty());

        assertTrue(predicate.test("true"));
        assertTrue(predicate.test("E=E1"));
        assertTrue(predicate.test("P=AA or A or E=E1"));
    }

    @Test
    void test() {
        final TypeDependencyPredicate predicate = new TypeDependencyPredicate(support.registryHandle,
                                                                              false,
                                                                              support.registry.getType(Name.of("E")));
        assertEquals(support.registryHandle, predicate.getDictionaryHandle());
        assertEquals(false, predicate.getDefault());
        assertSame(1, predicate.getTypes().size());

        assertFalse(predicate.test("true"));
        assertTrue(predicate.test("E=E1"));
        assertTrue(predicate.test("P=AA or A or E=E1"));
        assertFalse(predicate.test("P=AA"));
        assertTrue(predicate.test("A"));
        assertFalse(predicate.test("A1"));
        assertFalse(predicate.test("I=10"));
        assertFalse(predicate.test("A1"));
        assertFalse(predicate.test("A2"));
        assertFalse(predicate.test("B"));
    }
}