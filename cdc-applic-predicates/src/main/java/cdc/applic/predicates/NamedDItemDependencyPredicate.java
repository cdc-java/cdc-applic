package cdc.applic.predicates;

import java.util.HashSet;
import java.util.Set;

import cdc.applic.dictionaries.core.DictionaryDependencyCache;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.literals.Name;
import cdc.util.lang.Checks;

/**
 * Predicate yielding the dependency of an {@link Expression} on a set of {@link NamedDItem}s.
 *
 * @author Damien Carbonne
 */
public class NamedDItemDependencyPredicate implements ExpressionPredicate {
    /** Handle used to find dependencies. */
    private final DictionaryHandle handle;
    /** The value to return when items is empty. */
    private final boolean def;
    /** The set of searched items. */
    private final Set<NamedDItem> items = new HashSet<>();

    /**
     * Create a predicate from an array of names.
     * <p>
     * Names that don't correspond to an existing {@link NamedDItem} are ignored.
     *
     * @param handle The handle.
     * @param def The value to return when the set of searched {@link NamedDItem}s is empty.
     * @param names The names.
     */
    public NamedDItemDependencyPredicate(DictionaryHandle handle,
                                         boolean def,
                                         Name... names) {
        Checks.isNotNull(handle, "handle");

        this.handle = handle;
        this.def = def;

        for (final Name name : names) {
            final NamedDItem item = handle.getRegistry().getItem(name);
            this.items.add(item);
        }
    }

    public DictionaryHandle getDictionaryHandle() {
        return handle;
    }

    /**
     * @return The returned value when no items are searched.
     */
    public boolean getDefault() {
        return def;
    }

    /**
     * @return A Set of searched items.
     */
    public Set<NamedDItem> getItems() {
        return new HashSet<>(items);
    }

    /**
     * @param expression The tested Expression.
     * @return {@code def} when the set of named items is empty,<br>
     *         or when {@code expression} depends directly or indirectly on at least one item.
     */
    @Override
    public boolean test(Expression expression) {
        Checks.isNotNull(expression, "expression");

        if (items.isEmpty()) {
            return def;
        } else {
            final DictionaryDependencyCache cache = DictionaryDependencyCache.get(handle);
            final Set<NamedDItem> dependencies = cache.getDependencies(expression.getRootNode());
            for (final NamedDItem item : items) {
                if (dependencies.contains(item)) {
                    return true;
                }
            }
            return false;
        }
    }
}