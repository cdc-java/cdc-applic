package cdc.applic.predicates;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.ast.AndNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.ProverMatching;
import cdc.applic.proofs.core.clauses.ProverImpl;
import cdc.util.lang.Checks;

/**
 * An Expression predicate that evaluates the matching of an Expression with a reference Expression.
 * <p>
 * This can be used in the following scenario.<br>
 * Some objects have an applicability formulated with Expressions and one wants to search or filter
 * all of them whose applicability matches a reference expression.
 *
 * @author Damien Carbonne
 */
public class ProverMatchingPredicate implements ExpressionPredicate {
    /** The prover to use. */
    private final Prover prover;
    /** The value to return when matchings is empty. */
    private final boolean def;
    /** The reference expression to match. */
    private final Expression filter;
    /** The expected matchings. */
    private final Set<ProverMatching> matchings;
    /** Set to true when matchings is {ALWAYS,SOMETIMES}. */
    private final boolean matchingsIsSAT;
    /** Set to true when matchings is {NEVER}. */
    private final boolean matchingsIsNotSAT;

    /**
     * Creates a predicate.
     *
     * @param handle The handle, used to create a {@link Prover}.
     * @param def The value to return when the set of searched {@link ProverMatching}s is empty.
     * @param filter The filtering {@link Expression}.
     * @param proverFeatures The features to use to create the used {@link Prover}.<br>
     *            This has an influence on the result.
     * @param matchings The set of searched {@link ProverMatching}s.
     */
    public ProverMatchingPredicate(DictionaryHandle handle,
                                   boolean def,
                                   Expression filter,
                                   ProverFeatures proverFeatures,
                                   ProverMatching... matchings) {
        Checks.isNotNull(handle, "handle");
        Checks.isNotNull(filter, "filter");
        Checks.isNotNull(proverFeatures, "proverFeatures");

        this.prover = new ProverImpl(handle, proverFeatures);
        this.def = def;
        this.filter = filter;

        final EnumSet<ProverMatching> tmpMatchings = EnumSet.noneOf(ProverMatching.class);
        Collections.addAll(tmpMatchings, matchings);
        this.matchings = Collections.unmodifiableSet(tmpMatchings);

        this.matchingsIsSAT = this.matchings.contains(ProverMatching.ALWAYS)
                && this.matchings.contains(ProverMatching.SOMETIMES)
                && !this.matchings.contains(ProverMatching.NEVER);

        this.matchingsIsNotSAT = !this.matchings.contains(ProverMatching.ALWAYS)
                && !this.matchings.contains(ProverMatching.SOMETIMES)
                && this.matchings.contains(ProverMatching.NEVER);
    }

    public DictionaryHandle getDictionaryHandle() {
        return prover.getDictionaryHandle();
    }

    /**
     * @return The returned value when the set of searched prover matchings is empty.
     */
    public boolean getDefault() {
        return def;
    }

    /**
     * @return The filtering {@link Expression}.
     */
    public Expression getFilter() {
        return filter;
    }

    /**
     * @return The used {@link ProverFeatures}.
     */
    public ProverFeatures getProverFeatures() {
        return prover.getFeatures();
    }

    /**
     * @return The set of searched {@link ProverMatching ProverMatchings}.
     */
    public Set<ProverMatching> getProverMatchings() {
        return matchings;
    }

    @Override
    public boolean test(Expression expression) {
        Checks.isNotNull(expression, "expression");
        if (matchings.isEmpty()) {
            return def;
        } else {
            final Node node = new AndNode(expression.getRootNode(), filter.getRootNode());
            if (matchingsIsSAT) {
                // When matchings is {ALWAYS,SOMETIMES}, a one step computation is enough
                return prover.isSometimesTrue(node);
            } else if (matchingsIsNotSAT) {
                // When matchings is {NEVER}, a one step computation is enough
                return prover.isNeverTrue(node);
            } else {
                // General case, a general 2-step computation is necessary
                final ProverMatching matching = prover.getMatching(node);
                return matchings.contains(matching);
            }
        }
    }
}