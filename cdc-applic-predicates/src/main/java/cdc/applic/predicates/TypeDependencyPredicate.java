package cdc.applic.predicates;

import java.util.HashSet;
import java.util.Set;

import cdc.applic.dictionaries.core.DictionaryDependencyCache;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.NamedDItem;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.Expression;
import cdc.util.lang.Checks;

/**
 * Predicate yielding the dependency of an {@link Expression} on a set of {@link Type}s.
 *
 * @author Damien Carbonne
 */
public class TypeDependencyPredicate implements ExpressionPredicate {
    /** Handle used to find dependencies. */
    private final DictionaryHandle handle;
    /** The value to return when types is empty. */
    private final boolean def;
    /** The set of searched types. */
    private final Set<Type> types = new HashSet<>();

    /**
     * Create a predicate from an array of types.
     *
     * @param handle The handle, used to compute dependencies.
     * @param def The value to return when the set of searched {@link Type}s is empty.
     * @param types The types.
     */
    public TypeDependencyPredicate(DictionaryHandle handle,
                                   boolean def,
                                   Type... types) {
        Checks.isNotNull(handle, "handle");

        this.handle = handle;
        this.def = def;

        for (final Type type : types) {
            this.types.add(type);
        }
    }

    public DictionaryHandle getDictionaryHandle() {
        return handle;
    }

    /**
     * @return The value to return when the set of searched types is empty.
     */
    public boolean getDefault() {
        return def;
    }

    /**
     * @return The set of searched types.
     */
    public Set<Type> getTypes() {
        return new HashSet<>(types);
    }

    /**
     * @param expression The tested Expression.
     * @return {@code def} when the set of types is empty,<br>
     *         or when {@code expression} depends directly or indirectly on at least one type.
     */
    @Override
    public boolean test(Expression expression) {
        Checks.isNotNull(expression, "expression");

        if (types.isEmpty()) {
            return def;
        } else {
            final DictionaryDependencyCache cache = DictionaryDependencyCache.get(handle);
            final Set<NamedDItem> dependencies = cache.getDependencies(expression.getRootNode());
            for (final NamedDItem item : dependencies) {
                if (item instanceof Property
                        && types.contains(((Property) item).getType())) {
                    return true;
                }
            }
            return false;
        }
    }
}