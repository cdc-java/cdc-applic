package cdc.applic.predicates;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.core.clauses.ProverImpl;
import cdc.util.lang.Checks;

/**
 * Base class of Predicates based on a Prover.
 *
 * @author Damien Carbonne
 */
public abstract class AbstractProverPredicate implements ExpressionPredicate {
    /** The prover to use. */
    protected final Prover prover;

    protected AbstractProverPredicate(DictionaryHandle handle,
                                      ProverFeatures proverFeatures) {
        Checks.isNotNull(handle, "handle");
        Checks.isNotNull(proverFeatures, "proverFeatures");

        this.prover = new ProverImpl(handle, proverFeatures);
    }

    /**
     * @return The prover.
     */
    public final Prover getProver() {
        return prover;
    }
}