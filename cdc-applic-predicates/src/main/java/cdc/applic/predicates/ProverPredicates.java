package cdc.applic.predicates;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.expressions.Expression;
import cdc.applic.proofs.ProverFeatures;
import cdc.util.lang.Checks;

/**
 * Utilities used create basic predicates based on provers.
 *
 * @author Damien Carbonne
 */
public final class ProverPredicates {
    private static final String EXPRESSION = "expression";

    private ProverPredicates() {
    }

    public static AbstractProverPredicate isSometimesTrue(DictionaryHandle handle,
                                                          ProverFeatures proverFeatures) {
        return new AbstractProverPredicate(handle, proverFeatures) {
            @Override
            public boolean test(Expression expression) {
                Checks.isNotNull(expression, EXPRESSION);
                return prover.isSometimesTrue(expression);
            }
        };
    }

    public static AbstractProverPredicate isAlwaysTrue(DictionaryHandle handle,
                                                       ProverFeatures proverFeatures) {
        return new AbstractProverPredicate(handle, proverFeatures) {
            @Override
            public boolean test(Expression expression) {
                Checks.isNotNull(expression, EXPRESSION);
                return prover.isAlwaysTrue(expression);
            }
        };
    }

    public static AbstractProverPredicate isNeverTrue(DictionaryHandle handle,
                                                      ProverFeatures proverFeatures) {
        return new AbstractProverPredicate(handle, proverFeatures) {
            @Override
            public boolean test(Expression expression) {
                Checks.isNotNull(expression, EXPRESSION);
                return prover.isNeverTrue(expression);
            }
        };
    }

    public static AbstractProverPredicate isSometimesFalse(DictionaryHandle handle,
                                                           ProverFeatures proverFeatures) {
        return new AbstractProverPredicate(handle, proverFeatures) {
            @Override
            public boolean test(Expression expression) {
                Checks.isNotNull(expression, EXPRESSION);
                return prover.isSometimesFalse(expression);
            }
        };
    }

    public static AbstractProverPredicate isAlwaysFalse(DictionaryHandle handle,
                                                        ProverFeatures proverFeatures) {
        return new AbstractProverPredicate(handle, proverFeatures) {
            @Override
            public boolean test(Expression expression) {
                Checks.isNotNull(expression, EXPRESSION);
                return prover.isAlwaysFalse(expression);
            }
        };
    }

    public static AbstractProverPredicate isNeverFalse(DictionaryHandle handle,
                                                       ProverFeatures proverFeatures) {
        return new AbstractProverPredicate(handle, proverFeatures) {
            @Override
            public boolean test(Expression expression) {
                Checks.isNotNull(expression, EXPRESSION);
                return prover.isNeverFalse(expression);
            }
        };
    }
}