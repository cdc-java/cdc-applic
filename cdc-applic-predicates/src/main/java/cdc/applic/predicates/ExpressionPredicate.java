package cdc.applic.predicates;

import java.util.function.Predicate;

import cdc.applic.expressions.Expression;

/**
 * Base interface of [@link Predicate Predicates} on {@link Expression Expressions}.
 *
 * @author Damien Carbonne
 */
public interface ExpressionPredicate extends Predicate<Expression> {
    /**
     * Evaluates this predicate on the expression.
     *
     * @param expression The expression content.
     * @return {@code true} if the input argument matches the predicate, otherwise {@code false}.
     */
    public default boolean test(String expression) {
        return test(new Expression(expression));
    }
}