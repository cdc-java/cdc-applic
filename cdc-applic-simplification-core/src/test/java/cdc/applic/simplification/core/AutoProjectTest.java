package cdc.applic.simplification.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;

import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ast.Node;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.simplification.core.visitors.AutoProject;

class AutoProjectTest {
    private static final Logger LOGGER = LogManager.getLogger(AutoProjectTest.class);

    private static void check(String expected,
                              String expression,
                              DictionaryHandle handle,
                              ProverFeatures features) {
        LOGGER.info("=============================================");
        LOGGER.info("Features: {}", features);
        LOGGER.info("expression:{}", expression);
        final Expression x = new Expression(expression);
        final Node actualNode = AutoProject.execute(x.getRootNode(), handle, features);
        final Expression actualExpression = actualNode.toExpression(Formatting.SHORT_NARROW);
        LOGGER.info("expected:{}", expected);
        LOGGER.info("actual:{}", actualExpression);
        assertEquals(expected, actualNode.toInfix(Formatting.SHORT_NARROW));
    }

    @Test
    void testIntegerNoAssertionsNoReserves() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final DictionaryHandle registryHandle = new DictionaryHandle(registry);
        registry.integerType().name("I").frozen(false).domain("1~999").build();
        registry.property().name("I").type("I").build();
        registry.createAssertion("I != 1");
        final ProverFeatures features = ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES;

        check("I=1", "I = 1", registryHandle, features);
        check("I<:{1}", "I in {1}", registryHandle, features);
        check("I<:{1,2}", "I in {1, 2}", registryHandle, features);
        check("I<:{1~999}", "I in {1~999}", registryHandle, features);
    }

    @Test
    void testIntegerAssertionsNoReserves() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final DictionaryHandle registryHandle = new DictionaryHandle(registry);
        registry.integerType().name("I").frozen(false).domain("1~999").build();
        registry.property().name("I").type("I").build();
        registry.createAssertion("I != 1");
        final ProverFeatures features = ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES;

        check("false", "I = 1", registryHandle, features);
        check("true", "I != 1", registryHandle, features);
        check("I<:{}", "I in {1}", registryHandle, features);
        check("I<:{2}", "I in {1, 2}", registryHandle, features);
        check("I<:{2~999}", "I in {1~999}", registryHandle, features);
        check("I!<:{}", "I not in {1}", registryHandle, features);
        check("I!<:{2}", "I not in {1, 2}", registryHandle, features);
        check("I!<:{2~999}", "I not in {1~999}", registryHandle, features);
    }

    @Test
    void testIntegerAssertionsAllPossibleReserves() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final DictionaryHandle registryHandle = new DictionaryHandle(registry);
        registry.integerType().name("I").frozen(false).domain("1~999").build();
        registry.property().name("I").type("I").build();
        registry.createAssertion("I != 1");
        final ProverFeatures features = ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES;

        check("false", "I = 1", registryHandle, features);
        check("I<:{}", "I in {1}", registryHandle, features);
        check("I<:{2}", "I in {1, 2}", registryHandle, features);
        check("I<:{2~999}", "I in {1~999}", registryHandle, features);
    }

    @Test
    void testEnumNoAssertionsNoReserves() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final DictionaryHandle registryHandle = new DictionaryHandle(registry);
        registry.enumeratedType().name("E").frozen(false).literals("A", "B", "C", "D", "E").build();
        registry.property().name("E").type("E").build();
        registry.createAssertion("E != A");
        final ProverFeatures features = ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES;

        check("E=A", "E = A", registryHandle, features);
        check("E<:{A}", "E in {A}", registryHandle, features);
        check("E<:{A,B}", "E in {A, B}", registryHandle, features);
        check("E<:{A,B,C,D,E}", "E in {A,B,C,D,E}", registryHandle, features);
    }

    @Test
    void testEnumAssertionsNoReserves() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final DictionaryHandle registryHandle = new DictionaryHandle(registry);
        registry.enumeratedType().name("E").frozen(false).literals("A", "B", "C", "D", "E").build();
        registry.property().name("E").type("E").build();
        registry.createAssertion("E != A");
        final ProverFeatures features = ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES;

        check("false", "E = A", registryHandle, features);
        check("E<:{}", "E in {A}", registryHandle, features);
        check("E<:{B}", "E in {A, B}", registryHandle, features);
        check("E<:{B,C,D,E}", "E in {A,B,C,D,E}", registryHandle, features);
    }

    @Test
    void testEnumAssertionsAllPossibleReserves() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final DictionaryHandle registryHandle = new DictionaryHandle(registry);
        registry.enumeratedType().name("E").frozen(false).literals("A", "B", "C", "D", "E").build();
        registry.property().name("E").type("E").build();
        registry.createAssertion("E != A");
        final ProverFeatures features = ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES;

        check("false", "E = A", registryHandle, features);
        check("E<:{}", "E in {A}", registryHandle, features);
        check("E<:{B}", "E in {A, B}", registryHandle, features);
        check("E<:{B,C,D,E}", "E in {A,B,C,D,E}", registryHandle, features);
    }

    @Test
    void testBooleanNoAssertions() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final DictionaryHandle registryHandle = new DictionaryHandle(registry);
        registry.booleanType().name("B").build();
        registry.property().name("B").type("B").build();
        registry.createAssertion("B");
        final ProverFeatures features = ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES;

        check("B=true", "B=true", registryHandle, features);
        check("B", "B", registryHandle, features);
    }

    @Test
    void testBooleanAssertions() {
        // Reserves have no effect on boolean types
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final DictionaryHandle registryHandle = new DictionaryHandle(registry);
        registry.booleanType().name("B").build();
        registry.property().name("B").type("B").build();
        registry.createAssertion("B");
        final ProverFeatures features = ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES;

        check("B=true", "B=true", registryHandle, features);
        check("true", "B", registryHandle, features);
    }
}