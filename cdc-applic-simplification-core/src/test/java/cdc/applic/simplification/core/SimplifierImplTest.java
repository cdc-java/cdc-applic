package cdc.applic.simplification.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.io.IoBuilder;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.dictionaries.impl.RepositoryPrefixSupport;
import cdc.applic.dictionaries.impl.RepositorySupport;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.IllegalOperationException;
import cdc.applic.expressions.ast.visitors.QualifiedValue;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.simplification.Simplifier;
import cdc.applic.simplification.SimplifierFeatures;
import cdc.applic.simplification.SimplifierFeatures.Hint;
import cdc.util.strings.StringUtils;
import cdc.util.time.Chronometer;

class SimplifierImplTest {
    private static final Logger LOGGER = LogManager.getLogger(SimplifierImplTest.class);
    private static final PrintStream OUT = IoBuilder.forLogger(LOGGER).setLevel(Level.DEBUG).buildPrintStream();

    private final RepositorySupport support = new RepositorySupport();
    private final RepositoryPrefixSupport supportP = new RepositoryPrefixSupport();

    SimplifierImplTest() {
        support.registry.alias().name("A").expression("P=AA").ordinal(10).build();
    }

    private void check(String expected,
                       String expression,
                       SimplifierFeatures features) {
        LOGGER.debug("====================================================");
        LOGGER.debug("check({}, {})", expression, features);
        final Expression x = new Expression(expression);
        final Simplifier simplifier = new SimplifierImpl(support.registryHandle);
        final Expression actual = simplifier.simplify(x, features).getValue();
        LOGGER.debug("   -> {}", actual);
        support.registryHandle.print(OUT);

        if (!expected.equals(actual.toInfix(Formatting.SHORT_NARROW))) {
            LOGGER.warn("====================================================");
            LOGGER.warn("Expression: {}", expression);
            LOGGER.warn("Expected: {}", expected);
            LOGGER.warn("Actual: {}", actual.toInfix(Formatting.SHORT_NARROW));
        }
        assertEquals(expected, actual.toInfix(Formatting.SHORT_NARROW));
    }

    private static void check(String expected,
                              String expression,
                              Dictionary dictionary,
                              SimplifierFeatures features) {
        LOGGER.debug("====================================================");
        LOGGER.debug("check({}, {})", expression, features);
        final DictionaryHandle handle = new DictionaryHandle(dictionary);
        final Simplifier simplifier = new SimplifierImpl(handle);
        final Expression x = new Expression(expression);
        final Expression actual = simplifier.simplify(x, features).getValue();
        LOGGER.debug("   -> {}", actual);
        if (!expected.equals(actual.toInfix(Formatting.SHORT_NARROW))) {
            LOGGER.warn("====================================================");
            LOGGER.warn("Expression: {}", expression);
            LOGGER.warn("Expected: {}", expected);
            LOGGER.warn("Actual: {}", actual.toInfix(Formatting.SHORT_NARROW));
        }
        assertEquals(expected, actual.toInfix(Formatting.SHORT_NARROW));
    }

    private void checkP(String expected,
                        String expression,
                        SimplifierFeatures features,
                        RepositoryPrefixSupport.Registry registry) {
        LOGGER.debug("====================================================");
        LOGGER.debug("checkP({}, {})", expression, features);
        final Expression x = new Expression(expression);
        final Simplifier simplifier = new SimplifierImpl(supportP.getHandle(registry));
        final Expression actual = simplifier.simplify(x, features).getValue();
        LOGGER.debug("   -> {}", actual);
        support.registryHandle.print(OUT);
        if (!expected.equals(actual.toInfix(Formatting.SHORT_NARROW))) {
            LOGGER.warn("====================================================");
            LOGGER.warn("Expression: {}", expression);
            LOGGER.warn("Expected: {}", expected);
            LOGGER.warn("Actual: {}", actual.toInfix(Formatting.SHORT_NARROW));
        }
        assertEquals(expected, actual.toInfix(Formatting.SHORT_NARROW));
    }

    private void checkIncludeAssertionsNoReservesAllHints(String expected,
                                                          String expression) {
        final SimplifierFeatures features =
                SimplifierFeatures.builder()
                                  .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES)
                                  .allHints()
                                  .build();
        check(expected, expression, features);
    }

    private void checkIncludeAssertionsAllPossibleReservesAllHints(String expected,
                                                                   String expression) {
        final SimplifierFeatures features =
                SimplifierFeatures.builder()
                                  .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)
                                  .allHints()
                                  .build();
        check(expected, expression, features);
    }

    private void checkPIncludeAssertionsNoReservesAllHints(String expected,
                                                           String expression,
                                                           RepositoryPrefixSupport.Registry registry) {
        final SimplifierFeatures features =
                SimplifierFeatures.builder()
                                  .proverFeatures(ProverFeatures.INCLUDE_ASSERTIONS_NO_RESERVES)
                                  .allHints()
                                  .build();
        checkP(expected, expression, features, registry);
    }

    @Test
    void testIncludeAssertionsNoReservesAllHints() {
        checkIncludeAssertionsNoReservesAllHints("true", "true");
        checkIncludeAssertionsNoReservesAllHints("E<:{E1,E2}", "E = E1 or E = E2");
        checkIncludeAssertionsNoReservesAllHints("E<:{E1,E2}", "E = E2 or E = E1");
        checkIncludeAssertionsNoReservesAllHints("false", "E = E1 and E = E2");
        checkIncludeAssertionsNoReservesAllHints("E=E1", "E = E1 and E = E1");
        checkIncludeAssertionsNoReservesAllHints("true", "E = E1 or true");
        checkIncludeAssertionsNoReservesAllHints("E=E1", "E = E1 or false");
        checkIncludeAssertionsNoReservesAllHints("E=E1", "E = E1 and true");
        checkIncludeAssertionsNoReservesAllHints("false", "E = E1 and false");
        checkIncludeAssertionsNoReservesAllHints("P=AA", "P=AA");
        checkIncludeAssertionsNoReservesAllHints("I=1&P=AA", "I=1 & P=AA");
        checkIncludeAssertionsNoReservesAllHints("B&P=AA", "B & P=AA");
        checkIncludeAssertionsNoReservesAllHints("B&I=10&R=11.0&P=AA&E=E1", "E = E1 and B and P=AA and I=10 and R=11.0");

        checkIncludeAssertionsNoReservesAllHints("false", "!true");
        checkIncludeAssertionsNoReservesAllHints("B", "B = true");
        checkIncludeAssertionsNoReservesAllHints("!B", "B != true");
        checkIncludeAssertionsNoReservesAllHints("!B", "B = false");
        checkIncludeAssertionsNoReservesAllHints("B", "B != false");
        checkIncludeAssertionsNoReservesAllHints("!B", "!(B = true)");
        checkIncludeAssertionsNoReservesAllHints("B", "!(B = false)");
        checkIncludeAssertionsNoReservesAllHints("B", "!(B != true)");
        checkIncludeAssertionsNoReservesAllHints("!B", "!(B != false)");
        checkIncludeAssertionsNoReservesAllHints("E<:{E2,E3}", "E != E1");
        checkIncludeAssertionsNoReservesAllHints("I<:{1~9,11~999}", "I != 10");
        checkIncludeAssertionsNoReservesAllHints("!A", "not A");

        checkIncludeAssertionsNoReservesAllHints("I=10&E=E2|I<:{10~11}&E=E1", "I=10 & E=E1 | I=11 & E=E1 | I=10 & E=E2");
        checkIncludeAssertionsNoReservesAllHints("$X$", "$X$");
        checkIncludeAssertionsNoReservesAllHints("!$X$", "not $X$");
        checkIncludeAssertionsNoReservesAllHints("false", "$X$ and not $X$");
        checkIncludeAssertionsNoReservesAllHints("true", "$X$ or true");
        checkIncludeAssertionsNoReservesAllHints("false", "$X$ and false");
    }

    @Test
    void testIncludeAssertionsAllPossibleReservesAllHints() {
        checkIncludeAssertionsAllPossibleReservesAllHints("true", "true");
        checkIncludeAssertionsAllPossibleReservesAllHints("E<:{E1,E2}", "E = E1 or E = E2");
        checkIncludeAssertionsAllPossibleReservesAllHints("E<:{E1,E2}", "E = E2 or E = E1");
        checkIncludeAssertionsAllPossibleReservesAllHints("false", "E = E1 and E = E2");
        checkIncludeAssertionsAllPossibleReservesAllHints("E=E1", "E = E1 and E = E1");
        checkIncludeAssertionsAllPossibleReservesAllHints("true", "E = E1 or true");
        checkIncludeAssertionsAllPossibleReservesAllHints("E=E1", "E = E1 or false");
        checkIncludeAssertionsAllPossibleReservesAllHints("E=E1", "E = E1 and true");
        checkIncludeAssertionsAllPossibleReservesAllHints("false", "E = E1 and false");
        checkIncludeAssertionsAllPossibleReservesAllHints("B&I=10&R=11.0&P=AA&E=E1", "E = E1 and B and P=AA and I=10 and R=11.0");
        checkIncludeAssertionsAllPossibleReservesAllHints("P=AA&E<:{E1,E2}", "E=E1 and P=AA or E=E2 and P=AA");
        checkIncludeAssertionsAllPossibleReservesAllHints("P=AA&E<:{E1,E2}", "E=E2 and P=AA or E=E1 and P=AA");
        checkIncludeAssertionsAllPossibleReservesAllHints("P=AA&E<:{E1,E3}|E<:{E1,E2}", "(E=E2 or E=E1) or E in {E3,E1} and P=AA");
        checkIncludeAssertionsAllPossibleReservesAllHints("E<:{E1,E2,E3}", "E in {E1,E2} or E in {E2,E3}");
        checkIncludeAssertionsAllPossibleReservesAllHints("E=E2", "E in {E1,E2} and E in {E2,E3}");
        checkIncludeAssertionsAllPossibleReservesAllHints("E=E2&A", "E in {E1,E2} and A and E in {E2,E3}");
        checkIncludeAssertionsAllPossibleReservesAllHints("I=10&E<:{E1,E2,E3}", "(E in {E1,E2} or E in {E2,E3}) and I = 10");
        checkIncludeAssertionsAllPossibleReservesAllHints("I=10&E<:{E1,E2,E3}",
                                                          "E in {E1,E2} and I = 10 or E in {E2,E3} and I = 10");
        checkIncludeAssertionsAllPossibleReservesAllHints("I=10&R=10.0&E<:{E1,E2,E3}",
                                                          "E in {E1,E2} and I = 10 and R = 10.0 or E in {E2,E3} and R = 10.0 and I = 10");

        checkIncludeAssertionsAllPossibleReservesAllHints("false", "!true");
        checkIncludeAssertionsAllPossibleReservesAllHints("B", "B = true");
        checkIncludeAssertionsAllPossibleReservesAllHints("!B", "B != true");
        checkIncludeAssertionsAllPossibleReservesAllHints("!B", "B = false");
        checkIncludeAssertionsAllPossibleReservesAllHints("B", "B != false");
        checkIncludeAssertionsAllPossibleReservesAllHints("!B", "!(B = true)");
        checkIncludeAssertionsAllPossibleReservesAllHints("B", "!(B = false)");
        checkIncludeAssertionsAllPossibleReservesAllHints("B", "!(B != true)");
        checkIncludeAssertionsAllPossibleReservesAllHints("!B", "!(B != false)");
        assertThrows(IllegalOperationException.class,
                     () -> {
                         checkIncludeAssertionsAllPossibleReservesAllHints(null, "E != E1");
                     });
        assertThrows(IllegalOperationException.class,
                     () -> {
                         checkIncludeAssertionsAllPossibleReservesAllHints(null, "I != 10");
                     });
        assertThrows(IllegalOperationException.class,
                     () -> {
                         checkIncludeAssertionsAllPossibleReservesAllHints(null, "R != 10.0");
                     });

        checkIncludeAssertionsAllPossibleReservesAllHints("!A", " not A");
        checkIncludeAssertionsAllPossibleReservesAllHints("I=10&E<:{E1,E2,E3}",
                                                          "E = E1 and I=10 or E=E2 and I=10 or E=E3 and I=10");

        checkIncludeAssertionsAllPossibleReservesAllHints("$X$", "$X$");
        checkIncludeAssertionsAllPossibleReservesAllHints("!$X$", "not $X$");
        checkIncludeAssertionsAllPossibleReservesAllHints("false", "$X$ and not $X$");
    }

    @Test
    void testPIncludeAssertionsNoReservesAllHints() {
        checkPIncludeAssertionsNoReservesAllHints("Rank=10", "P1.Rank = 10", RepositoryPrefixSupport.Registry.P1);
        checkPIncludeAssertionsNoReservesAllHints("P1.Rank=10", "P1.Rank = 10", RepositoryPrefixSupport.Registry.ALL);
    }

    @Test
    void testBug1() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final DictionaryHandle registryHandle = new DictionaryHandle(registry);
        registry.integerType().name("Rank").frozen(true).domain("1~999").build();
        registry.booleanType().name("Boolean").build();
        registry.property().name("Rank").type("Rank").ordinal(0).build();
        registry.property().name("SB1").type("Boolean").ordinal(1).build();
        registry.property().name("SB2").type("Boolean").ordinal(2).build();

        final Simplifier simplifier = new SimplifierImpl(registryHandle);
        final SimplifierFeatures features =
                SimplifierFeatures.builder()
                                  .proverFeatures(ProverFeatures.EXCLUDE_ASSERTIONS_NO_RESERVES)
                                  .allHints()
                                  .build();

        // (Rank<:{1~10}|Rank<:{11~20}) & !(Rank<:{21~30} | Rank<:{1~10}&SB1 | Rank<:{11~20}&SB2 | Rank<:{31~999})
        // Rank<:{1~20} & !(Rank<:{21~30} | Rank<:{1~10}&SB1 | Rank<:{11~20}&SB2 | Rank<:{31~999})
        // Rank<:{1~20} & !(Rank<:{21~999} | Rank<:{1~10}&SB1 | Rank<:{11~20}&SB2)
        // Rank<:{1~20} & !(Rank<:{21~999}) & !(Rank<:{1~10}&SB1) & !(Rank<:{11~20}&SB2)
        // Rank<:{1~20} & !(Rank<:{1~10}&SB1) & !(Rank<:{11~20}&SB2)
        // Rank<:{1~20} & !(Rank<:{1~10}&SB1) & !(Rank<:{11~20}&SB2)
        final Expression expression =
                new Expression("(Rank<:{1~10}|Rank<:{11~20})&!(Rank<:{21~30}|Rank<:{1~10}&SB1|Rank<:{11~20}&SB2|Rank<:{31~999})");
        final Expression result = simplifier.simplify(expression, features).getValue();

        LOGGER.debug("result: " + result);
        assertTrue(true);
    }

    @Test
    void testBug2() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final DictionaryHandle registryHandle = new DictionaryHandle(registry);
        registry.integerType().name("Rank").frozen(true).domain("1~999").build();
        registry.property().name("Rank").type("Rank").ordinal(0).build();

        final Simplifier simplifier = new SimplifierImpl(registryHandle);
        final SimplifierFeatures features =
                SimplifierFeatures.builder()
                                  .proverFeatures(ProverFeatures.EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES)
                                  .allHints()
                                  .build();

        final Expression expression = new Expression("(Rank<:{1~10}|Rank<:{11~20}|Rank<:{21~30}|Rank<:{31~999})&!false");
        final Expression result = simplifier.simplify(expression, features).getValue();

        LOGGER.debug("result: " + result);
        assertEquals("Rank<:{1~999}", result.compress().getContent());
    }

    @Test
    void testTimeout() {
        final int count = 15;
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        final DictionaryHandle registryHandle = new DictionaryHandle(registry);
        final String[] values = new String[count];
        for (int index = 0; index < count; index++) {
            values[index] = "V" + index;
        }

        registry.enumeratedType().name("Version").frozen(false).literals(values).build();
        registry.integerType().name("Rank").frozen(true).domain("0~99999").build();
        registry.property().name("V").type("Version").ordinal(0).build();
        registry.property().name("R").type("Rank").ordinal(0).build();
        for (int index = 0; index < count; index++) {
            registry.createAssertion("V = V" + index + " -> R in {" + (index * 100)
                    + "~" + (index * 100 + 99) + "}");
        }

        final Simplifier simplifier = new SimplifierImpl(registryHandle);
        final SimplifierFeatures features =
                SimplifierFeatures.builder()
                                  .proverFeatures(ProverFeatures.builder()
                                                                .assertionStrategy(AssertionStrategy.INCLUDE_ASSERTIONS)
                                                                .reserveStrategy(ReserveStrategy.NO_RESERVES)
                                                                .timeoutMillis(-1)
                                                                .build())
                                  .hint(Hint.CONVERT_TO_DNF)
                                  .hint(Hint.NORMALIZE_BOOLEAN_PROPERTIES)
                                  .hint(Hint.REMOVE_ALWAYS_TRUE_OR_FALSE)
                                  .hint(Hint.REMOVE_INEQUALITIES)
                                  .hint(Hint.REMOVE_NEGATION)
                                  .hint(Hint.REMOVE_REDUNDANT_SIBLINGS)
                                  .maxMillis(10000)
                                  .build();

        final StringBuilder x = new StringBuilder();
        x.append("V<:{");
        for (int index = 0; index < count - 1; index++) {
            if (index > 0) {
                x.append(", ");
            }
            x.append("V" + index);
        }
        x.append("} & !(");
        for (int index = 0; index < count; index++) {
            if (index > 0) {
                x.append(" | ");
            }
            x.append("V=V" + index + "&R<:{" + (index * 100) + "~" + (index * 100 + 50) + "}");
        }
        x.append(")");

        LOGGER.info("x:{}", x);
        final Chronometer chrono = new Chronometer();
        final Expression expression =
                new Expression(x.toString());
        chrono.start();
        final QualifiedValue<Expression> result = simplifier.simplify(expression, features);
        chrono.suspend();

        LOGGER.info("{}\n result: {} {}",
                    chrono,
                    result.getQuality(),
                    StringUtils.extract(result.getValue().toString(), 1000));
        assertTrue(true);
    }

    @Test
    void testSynonyms() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").prefix("R").build();
        registry.namingConvention()
                .name("C1")
                .build();
        registry.enumeratedType()
                .name("Version")
                .synonym("C1", "version")
                .value()
                .literal("V1")
                .synonym("C1", "v1")
                .back()
                .value()
                .literal("V2")
                .synonym("C1", "v2")
                .back()
                .value()
                .literal("V3")
                .synonym("C1", "v3")
                .back()
                .build();
        registry.property()
                .name("Version")
                .type("Version")
                .synonym("C1", "version")
                .build();
        final SimplifierFeatures features = SimplifierFeatures.ALL_HINTS_EXCLUDE_ASSERTIONS_ALL_POSSIBLE_RESERVES;

        check("Version=V1", "Version=V1", registry, features);
        check("Version=V1", "Version=v1", registry, features);
        check("Version=V1", "version=V1", registry, features);
        check("Version=V1", "version=v1", registry, features);

        check("Version=V1", "Version<:{V1, V1}", registry, features); // Duplicate values are removed
        check("Version=V1", "Version<:{v1, v1}", registry, features); // Duplicate values are removed
        check("Version=V1", "Version<:{V1, v1}", registry, features); // Default convention is preferred
        check("Version=V1", "Version<:{v1, V1}", registry, features); // Default convention is preferred

        check("Version=V1", "version<:{V1, V1}", registry, features); // Duplicate values are removed
        check("Version=V1", "version<:{v1, v1}", registry, features); // Duplicate values are removed
        check("Version=V1", "version<:{V1, v1}", registry, features); // Default convention is preferred
        check("Version=V1", "version<:{v1, V1}", registry, features); // Default convention is preferred

        check("Version=V1", "Version=V1 or Version=V1", registry, features);
        check("Version=V1", "Version=V1 or version=V1", registry, features); // First prop convention is preferred
        check("Version=V1", "version=V1 or Version=V1", registry, features); // First prop convention is preferred
        check("Version=V1", "version=V1 or version=V1", registry, features);

        check("Version=V1", "Version=V1 or version=v1", registry, features); // First prop & value
        check("Version=V1", "version=V1 or Version=v1", registry, features); // First prop & value
        check("Version=V1", "Version=v1 or version=V1", registry, features); // First prop & value
        check("Version=V1", "version=v1 or Version=V1", registry, features); // First prop & value
    }

    @Test
    void testIssue184() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        registry.patternType().name("Std").frozen(false).pattern(".*").build();
        registry.property().name("Std").type("Std").build();

        final ProverFeatures proverFeatures =
                ProverFeatures.builder()
                              .assertionStrategy(AssertionStrategy.EXCLUDE_ASSERTIONS)
                              .reserveStrategy(ReserveStrategy.NO_RESERVES)
                              .build();

        final SimplifierFeatures features =
                SimplifierFeatures.builder()
                                  .proverFeatures(proverFeatures)
                                  // .allHints()
                                  .hints(Hint.CONVERT_TO_DNF,
                                         Hint.NORMALIZE_BOOLEAN_PROPERTIES,
                                         Hint.REMOVE_ALWAYS_TRUE_OR_FALSE,
                                         Hint.REMOVE_NEGATION,
                                         Hint.REMOVE_REDUNDANT_SIBLINGS,
                                         Hint.REMOVE_INEQUALITIES,
                                         Hint.REMOVE_USELESS_PREFIXES)
                                  .noLimits()
                                  .build();

        // Expected failure
        // Missed simplification
        assertThrows(AssertionFailedError.class,
                     () -> check("Std<:{X1}", "Std in {X1, X2} and not (Std in {X2})", registry, features));
    }

    @Test
    void testIssue208() {
        final RepositoryImpl repository = new RepositoryImpl();
        final RegistryImpl registry = repository.registry().name("Registry").build();
        registry.integerType().name("Rank").frozen(false).domain("1~999").build();
        registry.property().name("Rank").type("Rank").build();
        registry.createAssertion("Rank != 1");

        final ProverFeatures proverFeatures =
                ProverFeatures.builder()
                              .assertionStrategy(AssertionStrategy.INCLUDE_ASSERTIONS)
                              .reserveStrategy(ReserveStrategy.USER_DEFINED_RESERVES)
                              .build();

        final SimplifierFeatures features =
                SimplifierFeatures.builder()
                                  .proverFeatures(proverFeatures)
                                  .allHints()
                                  .noLimits()
                                  .build();

        check("false", "Rank=1", registry, features);
        check("false", "Rank in {1}", registry, features);

        check("Rank<:{2~999}", "Rank in {1~999}", registry, features);
        check("Rank<:{3~999}", "Rank in {1, 3~999}", registry, features);
    }
}