package cdc.applic.simplification.core.visitors;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.DictionaryUtils;
import cdc.applic.dictionaries.core.visitors.CollectPropertyNames;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.items.Property;
import cdc.applic.dictionaries.types.DomainedType;
import cdc.applic.dictionaries.types.Type;
import cdc.applic.expressions.ast.AbstractLeafNode;
import cdc.applic.expressions.ast.AbstractSetNode;
import cdc.applic.expressions.ast.EqualNode;
import cdc.applic.expressions.ast.FalseNode;
import cdc.applic.expressions.ast.InNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NotEqualNode;
import cdc.applic.expressions.ast.RefNode;
import cdc.applic.expressions.ast.TrueNode;
import cdc.applic.expressions.ast.visitors.AbstractConverter;
import cdc.applic.expressions.content.BooleanValue;
import cdc.applic.expressions.content.SItemSet;
import cdc.applic.expressions.content.UncheckedSet;
import cdc.applic.expressions.content.Value;
import cdc.applic.expressions.literals.Name;
import cdc.applic.projections.Axis;
import cdc.applic.projections.Shadow;
import cdc.applic.projections.core.SingleAxisExpressionProjector;
import cdc.applic.proofs.ProverFeatures;
import cdc.applic.proofs.ProverMatching;
import cdc.util.lang.Checks;

public final class AutoProject extends AbstractConverter {
    private static final Logger LOGGER = LogManager.getLogger(AutoProject.class);
    private final Dictionary dictionary;
    /**
     * Associate to each relevant property, the set NEVER and ALWAYS projections.
     */
    private final Map<Property, NeverAlways> shadows = new HashMap<>();

    private record NeverAlways(SItemSet never,
                               SItemSet always) {
    }

    private static final NeverAlways EMPTY_EMPTY =
            new NeverAlways(UncheckedSet.EMPTY, UncheckedSet.EMPTY);

    private AutoProject(DictionaryHandle handle,
                        ProverFeatures features,
                        Node node) {
        Checks.isNotNull(handle, "handle");
        LOGGER.trace("AutoProject({})", node);

        this.dictionary = handle.getDictionary();
        // Collect all property names referenced in the expression
        final Set<Name> names = CollectPropertyNames.collect(node, dictionary);
        final Set<Property> properties = names.stream()
                                              .map(dictionary::getProperty)
                                              .collect(Collectors.toSet());
        for (final Property property : properties) {
            LOGGER.trace("{}", property);
            final Axis axis = new Axis(property);
            final SingleAxisExpressionProjector projector =
                    new SingleAxisExpressionProjector(handle, features, axis);
            final Type type = property.getType();
            if (type instanceof final DomainedType dt) {
                final SItemSet domain = dt.getDomain();
                // FIXME: prefix?
                final Node n = new InNode(property.getName(), domain);
                final Shadow shadow = projector.project(n);
                LOGGER.trace("Shadow({}):{}", property, shadow);

                final NeverAlways na =
                        new NeverAlways(shadow.getAxisSet(axis, ProverMatching.NEVER),
                                        shadow.getAxisSet(axis, ProverMatching.ALWAYS));
                shadows.put(property, na);
                LOGGER.trace("NA({}):{}", property, na);
            } else {
                shadows.put(property, EMPTY_EMPTY);
            }
        }
    }

    public static Node execute(Node node,
                               DictionaryHandle handle,
                               ProverFeatures features) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(handle, "handle");
        final AutoProject visitor = new AutoProject(handle, features, node);
        return node.accept(visitor);
    }

    @Override
    public Node visitLeaf(AbstractLeafNode node) {
        if (node instanceof final AbstractSetNode n) {
            // Remove all excluded values from the set
            // Whether the set node is IN or NOT IN
            // FIXME really?
            final Property property = dictionary.getProperty(n.getName());
            final SItemSet exclude = shadows.get(property).never;
            if (exclude != null) {
                final SItemSet set = n.getSet();
                final SItemSet reducedSet = set.remove(exclude);
                return n.create(n.getName(), reducedSet);
            }
        } else if (node instanceof final EqualNode n) {
            final Property property = dictionary.getProperty(n.getName());
            final SItemSet never = shadows.get(property).never; // may be null
            final Value value = n.getValue();
            if (never != null && never.contains(value)) {
                // Replace the equal node by FALSE
                return FalseNode.INSTANCE;
            }
        } else if (node instanceof final NotEqualNode n) {
            final Property property = dictionary.getProperty(n.getName());
            final SItemSet never = shadows.get(property).never; // may be null
            final Value value = n.getValue();
            if (never != null && never.contains(value)) {
                // Replace the not equal node by TRUE
                return TrueNode.INSTANCE;
            }
        } else if (node instanceof final RefNode n
                && DictionaryUtils.isAvailableBooleanProperty(n.getName(), dictionary)) {
            // A boolean has 2 values
            final Property property = dictionary.getProperty(n.getName());
            final SItemSet never = shadows.get(property).never; // may be null
            final SItemSet always = shadows.get(property).always; // may be null
            if (never != null && always != null) {
                if (always.contains(BooleanValue.TRUE) && never.contains(BooleanValue.FALSE)) {
                    return TrueNode.INSTANCE;
                } else if (always.contains(BooleanValue.FALSE) && never.contains(BooleanValue.TRUE)) {
                    return FalseNode.INSTANCE;
                }
            }
        }
        return node;
    }
}