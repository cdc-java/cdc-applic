package cdc.applic.simplification.core;

import java.util.function.Function;
import java.util.function.UnaryOperator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.dictionaries.AssertionStrategy;
import cdc.applic.dictionaries.Dictionary;
import cdc.applic.dictionaries.ReserveStrategy;
import cdc.applic.dictionaries.core.visitors.ConvertInequalitiesToSets;
import cdc.applic.dictionaries.core.visitors.ConvertNamingConvention;
import cdc.applic.dictionaries.core.visitors.ConvertToDNF;
import cdc.applic.dictionaries.core.visitors.EliminateFullAndEmptyInequalities;
import cdc.applic.dictionaries.core.visitors.EliminateFullAndEmptySets;
import cdc.applic.dictionaries.core.visitors.MergeGroups;
import cdc.applic.dictionaries.core.visitors.MoveNotInwards;
import cdc.applic.dictionaries.core.visitors.NormalizeBooleanProperties;
import cdc.applic.dictionaries.core.visitors.NormalizeSets;
import cdc.applic.dictionaries.core.visitors.SortGroups;
import cdc.applic.dictionaries.core.visitors.SortLeaves;
import cdc.applic.dictionaries.handles.ApplicCacheId;
import cdc.applic.dictionaries.handles.ComputationCache;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.visitors.RemovePrefixes;
import cdc.applic.expressions.Expression;
import cdc.applic.expressions.Formatting;
import cdc.applic.expressions.ImplementationException;
import cdc.applic.expressions.ast.EquivalenceNode;
import cdc.applic.expressions.ast.Node;
import cdc.applic.expressions.ast.NodePredicates;
import cdc.applic.expressions.ast.visitors.ConvertEqualitiesToSingletons;
import cdc.applic.expressions.ast.visitors.ConvertSingletonsToEquals;
import cdc.applic.expressions.ast.visitors.ConvertToNary;
import cdc.applic.expressions.ast.visitors.EliminateTrueFalse;
import cdc.applic.expressions.ast.visitors.MergeSets;
import cdc.applic.expressions.ast.visitors.PredicateCounter;
import cdc.applic.expressions.ast.visitors.QualifiedValue;
import cdc.applic.proofs.Prover;
import cdc.applic.proofs.core.clauses.ProverImpl;
import cdc.applic.proofs.core.visitors.EliminateAlwaysTrueOrFalse;
import cdc.applic.proofs.core.visitors.EliminateRedundantSiblings;
import cdc.applic.simplification.Simplifier;
import cdc.applic.simplification.SimplifierFeatures;
import cdc.applic.simplification.core.visitors.AutoProject;
import cdc.util.lang.Checks;
import cdc.util.strings.StringUtils;

/**
 * Implementation of {@link Simplifier}.
 *
 * @author Damien Carbonne
 */
public class SimplifierImpl implements Simplifier {
    private static final Logger LOGGER = LogManager.getLogger(SimplifierImpl.class);
    private final DictionaryHandle handle;

    private static final class SimplificationCache extends ComputationCache<QualifiedValue<Node>> {
        private SimplificationCache(DictionaryHandle handle,
                                    SimplifierFeatures tag) {
            super(handle,
                  tag);
        }

        public static SimplificationCache get(DictionaryHandle handle,
                                              SimplifierFeatures features) {
            return handle.computeIfAbsent(SimplificationCache.class,
                                          new ApplicCacheId(SimplificationCache.class, features),
                                          (c,
                                           t) -> new SimplificationCache(c, (SimplifierFeatures) t));
        }
    }

    public SimplifierImpl(DictionaryHandle handle) {
        Checks.isNotNull(handle, "handle");
        this.handle = handle;
    }

    @Override
    public DictionaryHandle getDictionaryHandle() {
        return handle;
    }

    @Override
    public QualifiedValue<Node> simplify(Node node,
                                         SimplifierFeatures features) {
        Checks.isNotNull(node, "node");
        Checks.isNotNull(features, "features");
        final SimplificationCache cache = SimplificationCache.get(handle, features);
        return cache.computeIfAbsent(node, n -> Implementation.doSimplify(n, handle, features, false));
    }

    @Override
    public QualifiedValue<Expression> simplify(Expression expression,
                                               SimplifierFeatures features) {
        final QualifiedValue<Node> x = simplify(expression.getRootNode(), features);
        return new QualifiedValue<>(x.getValue().toExpression(Formatting.SHORT_NARROW), x.getQuality());
    }

    private static final class Implementation {
        /** Start time of computation */
        private final long start = System.nanoTime();
        private final DictionaryHandle handle;
        private final SimplifierFeatures features;
        private final boolean check;
        private final Prover prover;
        private QualifiedValue.Quality quality = QualifiedValue.Quality.SUCCESS;

        /**
         * The max number of milliseconds to spend in the simplification.
         * <p>
         * A negative value means infinite.
         */
        private final int maxMillis;

        /**
         * The remaining time available for simplification.
         * <p>
         * A negative value means infinite.
         */
        private int remainingMillis = -1;
        private boolean active = true;

        private Implementation(DictionaryHandle handle,
                               SimplifierFeatures features,
                               boolean check) {
            this.handle = handle;
            this.features = features;
            this.check = check;
            this.prover = new ProverImpl(handle, features.getProverFeatures());
            this.maxMillis = features.getMaxMillis();
        }

        public static QualifiedValue<Node> doSimplify(Node node,
                                                      DictionaryHandle handle,
                                                      SimplifierFeatures features,
                                                      boolean check) {
            final Implementation implementation = new Implementation(handle, features, check);
            return implementation.execute(node);
        }

        private void checkElapsedTime() {
            /** The time spent in simplification. */
            final int elapsedMillis = (int) ((System.nanoTime() - start) / 1_000_000L);
            if (maxMillis >= 0 && active) {
                active = elapsedMillis < maxMillis;
                if (elapsedMillis <= maxMillis) {
                    remainingMillis = maxMillis - elapsedMillis;
                } else {
                    remainingMillis = 0;
                }
                if (!active) {
                    LOGGER.warn("Reached timeout {} ms", maxMillis);
                    quality = quality.merge(QualifiedValue.Quality.PARTIAL);
                }
            }
            LOGGER.debug("check time, elapsed {} ms remaining {} ms", elapsedMillis, remainingMillis);
        }

        /**
         * @param hint The hint.
         * @return {@code true} if {@code hint} has been enabled by code
         *         and is not disabled by command line.
         */
        private boolean isEnabled(SimplifierFeatures.Hint hint) {
            return features.isEnabled(hint) && !SimplifierConfig.isDisabled(hint);
        }

        private QualifiedValue<Node> execute(Node node) {
            final Dictionary dictionary = handle.getDictionary();
            final ReserveStrategy reserveStrategy = features.getProverFeatures().getReserveStrategy();
            final int maxIterations = features.getMaxIterations();

            final MoveNotInwards.Variant moveNotInwardsVariant;

            if (isEnabled(SimplifierFeatures.Hint.REMOVE_NEGATION)) {
                if (isEnabled(SimplifierFeatures.Hint.FAIL_ON_NON_REMOVABLE_NEGATION)) {
                    moveNotInwardsVariant = MoveNotInwards.Variant.DONT_USE_NEGATION_THROW_WHEN_IMPOSSIBLE;
                } else {
                    moveNotInwardsVariant = MoveNotInwards.Variant.DONT_USE_NEGATION_IGNORE_WHEN_IMPOSSIBLE;
                }
            } else {
                moveNotInwardsVariant = MoveNotInwards.Variant.USE_NEGATIVE_LEAVES;
            }

            Node result = node;

            if (isEnabled(SimplifierFeatures.Hint.REMOVE_USELESS_PREFIXES)) {
                result = convert(result,
                                 n -> RemovePrefixes.execute(n, dictionary),
                                 "RemovePrefixes");
            }

            if (isEnabled(SimplifierFeatures.Hint.REMOVE_INEQUALITIES)) {
                result = convert(result,
                                 n -> ConvertInequalitiesToSets.execute(n, dictionary),
                                 "ConvertInequalitiesToSets");
            }

            result = convert(result,
                             n -> MoveNotInwards.execute(n, moveNotInwardsVariant, dictionary, reserveStrategy),
                             "MoveNotInwards'" + moveNotInwardsVariant);

            result = convert(result,
                             n -> ConvertToNary.execute(n, ConvertToNary.Variant.WHEN_NECESSARY),
                             "ConvertToNary'WHEN_NECESSARY");

            result = convert(result,
                             n -> EliminateFullAndEmptySets.execute(n, dictionary, reserveStrategy),
                             "EliminateFullAndEmptySets'" + reserveStrategy);

            if (!isEnabled(SimplifierFeatures.Hint.REMOVE_INEQUALITIES)) {
                result = convert(result,
                                 n -> EliminateFullAndEmptyInequalities.execute(n, dictionary, reserveStrategy),
                                 "EliminateFullAndEmptyInequalities'" + reserveStrategy);
            }

            result = convert(result,
                             ConvertEqualitiesToSingletons::execute,
                             "ConvertEqualsToSingletons");

            if (isEnabled(SimplifierFeatures.Hint.AUTO_PROJECT)
                    && features.getProverFeatures().getAssertionStrategy() == AssertionStrategy.INCLUDE_ASSERTIONS) {
                result = convert(result,
                                 n -> AutoProject.execute(n, handle, features.getProverFeatures()),
                                 "AutoProject");
            }

            if (isEnabled(SimplifierFeatures.Hint.CONVERT_TO_DNF)) {
                result = convert(result,
                                 n -> ConvertToDNF.execute(n, moveNotInwardsVariant, dictionary, reserveStrategy),
                                 "ConvertToDNF'" + moveNotInwardsVariant);
                result = convert(result,
                                 n -> ConvertToNary.execute(n, ConvertToNary.Variant.WHEN_NECESSARY),
                                 "ConvertToNary'WHEN_NECESSARY");
                result = convert(result,
                                 MergeSets::execute,
                                 "MergeSets");
            }

            // Number of iterations
            int iterations = 0;

            // true if iteration does not change result
            boolean stable = false;

            // Iterate
            while ((maxIterations < 0 || iterations <= maxIterations)
                    && active
                    && !stable) {
                final String prefix = "Loop " + (iterations + 1) + " ";
                // simplification result in the iteration step
                Node iterationResult = result;

                iterationResult = convert(iterationResult,
                                          EliminateTrueFalse::execute,
                                          prefix + "EliminateTrueFalse");

                iterationResult = convert(iterationResult,
                                          n -> EliminateFullAndEmptySets.execute(n, dictionary, reserveStrategy),
                                          prefix + "EliminateFullAndEmptySets'" + reserveStrategy);

                if (!isEnabled(SimplifierFeatures.Hint.REMOVE_INEQUALITIES)) {
                    result = convert(result,
                                     n -> EliminateFullAndEmptyInequalities.execute(n, dictionary, reserveStrategy),
                                     "EliminateFullAndEmptyInequalities'" + reserveStrategy);
                }

                iterationResult = convert(iterationResult,
                                          MergeSets::execute,
                                          prefix + "MergeSets");

                if (isEnabled(SimplifierFeatures.Hint.REMOVE_ALWAYS_TRUE_OR_FALSE)) {
                    // remaining millis is not exact, but that should be enough
                    iterationResult = convertq(iterationResult,
                                               n -> EliminateAlwaysTrueOrFalse.execute(n,
                                                                                       handle,
                                                                                       features.getProverFeatures(),
                                                                                       remainingMillis),
                                               prefix + "EliminateAlwaysTrueOrFalse");
                }

                if (isEnabled(SimplifierFeatures.Hint.REMOVE_REDUNDANT_SIBLINGS)) {
                    // remaining millis is not exact, but that should be enough
                    iterationResult = convertq(iterationResult,
                                               n -> EliminateRedundantSiblings.execute(n,
                                                                                       handle,
                                                                                       features.getProverFeatures(),
                                                                                       remainingMillis),
                                               prefix + "EliminateRedundantSiblings");
                }

                iterationResult = convert(iterationResult,
                                          n -> ConvertToNary.execute(n, ConvertToNary.Variant.WHEN_NECESSARY),
                                          prefix + "ConvertToNary'WHEN_NECESSARY");

                iterationResult = convert(iterationResult,
                                          n -> MergeGroups.execute(n, dictionary),
                                          prefix + "MergeGroups");

                // Update counters
                iterations++;

                // Did this iteration change anything ?
                stable = result.equals(iterationResult);

                // Update result
                result = iterationResult;
            }

            result = convert(result,
                             n -> NormalizeSets.execute(n, dictionary),
                             "NormalizeSets");

            result = convert(result,
                             ConvertSingletonsToEquals::execute,
                             "ConvertSingletonsToEquals");

            result = convert(result,
                             n -> SortLeaves.execute(n, dictionary),
                             "SortLeaves");

            result = convert(result,
                             n -> SortGroups.execute(n, dictionary),
                             "SortGroups");

            if (isEnabled(SimplifierFeatures.Hint.NORMALIZE_BOOLEAN_PROPERTIES)) {
                result = convert(result,
                                 n -> NormalizeBooleanProperties.execute(n, dictionary),
                                 "NormalizeBooleanProperties");
            }

            result = convert(result,
                             n -> ConvertNamingConvention.execute(n, dictionary, features.getNamingConvention()),
                             "ConvertNamingConvention");

            // Using prefix of used dictionary is useless
            result = convert(result,
                             n -> RemovePrefixes.execute(n, dictionary),
                             "RemovePrefixes");

            // In the end, check equivalence of result with node
            try {
                checkEquivalence("Simplification failed", false, node, result);
                return new QualifiedValue<>(result, quality);
            } catch (final ImplementationException e) {
                if (!check) {
                    // Launch simplification a second time, but with all checks enabled
                    return doSimplify(node, handle, features, true);
                } else {
                    throw e;
                }
            }
        }

        /**
         * Converts a node using a converter, and checks equivalence if asked.
         *
         * @param node The node to convert.
         * @param converter The converter to use.
         * @param message The message to print in case of error.
         * @return The conversion of {@code node} using {@code converter}.
         */
        private Node convert(Node node,
                             UnaryOperator<Node> converter,
                             String message) {
            checkElapsedTime();
            if (active) {
                LOGGER.debug("convert({}, {})", () -> message, () -> toString(node));
                final long s = System.nanoTime();
                final Node n = converter.apply(node);
                final long e = System.nanoTime();
                final int micros = (int) ((e - s) / 1000L);
                final double millis = micros / 1000.0;
                LOGGER.debug("    {} ms, {} -> {} nodes",
                             () -> millis,
                             () -> PredicateCounter.count(node, NodePredicates.IS_NODE),
                             () -> PredicateCounter.count(n, NodePredicates.IS_NODE));
                if (check) {
                    checkEquivalence(message, check, node, n);
                }
                return n;
            } else {
                LOGGER.warn("Skip convert({})", () -> message);
                return node;
            }
        }

        private Node convertq(Node node,
                              Function<Node, QualifiedValue<Node>> converter,
                              String message) {
            checkElapsedTime();
            if (active) {
                LOGGER.debug("convert({}, {})", () -> message, () -> toString(node));
                final long s = System.nanoTime();
                final QualifiedValue<Node> qn = converter.apply(node);
                final long e = System.nanoTime();
                final int micros = (int) ((e - s) / 1000L);
                final double millis = micros / 1000.0;
                LOGGER.debug("    {} {} ms, {} -> {} nodes",
                             qn::getQuality,
                             () -> millis,
                             () -> PredicateCounter.count(node, NodePredicates.IS_NODE),
                             () -> PredicateCounter.count(qn.getValue(), NodePredicates.IS_NODE));
                if (check) {
                    checkEquivalence(message, check, node, qn.getValue());
                }
                quality = quality.merge(qn.getQuality());
                return qn.getValue();
            } else {
                LOGGER.warn("Skip convert({})", () -> message);
                return node;
            }
        }

        /**
         * Checks that 2 nodes are equivalent.
         *
         * @param message The message to print in case of error.
         * @param debug If {@code true} print traces, even if nodes are equivalent.
         * @param alpha The first node.
         * @param beta The second node.
         */
        private void checkEquivalence(String message,
                                      boolean debug,
                                      Node alpha,
                                      Node beta) {
            final boolean equiv = prover.isAlwaysTrue(new EquivalenceNode(alpha, beta));
            if (!equiv) {
                if (LOGGER.isFatalEnabled()) {
                    LOGGER.fatal("   {}: {} --> {}", message, toString(alpha), toString(beta));
                    LOGGER.fatal("{}: {} and {} are not equivalent", message, toString(alpha), toString(beta));
                    LOGGER.fatal("   Prover features: {}", prover.getFeatures());
                }
                throw new ImplementationException(message + ": " + toString(alpha) + " and " + toString(beta)
                        + " are not equivalent");
            } else if (debug && LOGGER.isInfoEnabled()) {
                LOGGER.info("   {}: {} --> {}", message, toString(alpha), toString(beta));
            }
        }

        private static String toString(Node node) {
            return StringUtils.extract(node.toInfix(Formatting.SHORT_NARROW), 1000);
        }
    }
}