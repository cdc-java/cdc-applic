package cdc.applic.simplification.core;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cdc.applic.simplification.SimplifierFeatures;

/**
 * Class used to configure simplification by overriding code.
 * <p>
 * One must define properties with:
 * <ul>
 * <li>{@code -Dapplic.simplifier.disable.xxx}
 * <li>{@code -Dapplic.simplifier.disable.xxx=true}
 * <li>{@code -Dapplic.simplifier.disable.xxx=false}
 * </ul>
 * where {@code xxx} is the name of a {@link cdc.applic.simplification.SimplifierFeatures.Hint SimplifierFeatures.Hint}.
 * Case does not matter.
 */
final class SimplifierConfig {
    /** The prefix used to designate recognized properties. */
    private static final String PREFIX = "applic.simplifier.disable.";
    /** Set of disabled hints. */
    private final Set<SimplifierFeatures.Hint> disabled = EnumSet.noneOf(SimplifierFeatures.Hint.class);

    private SimplifierConfig() {
        final Map<String, SimplifierFeatures.Hint> map = new HashMap<>();
        for (final SimplifierFeatures.Hint hint : SimplifierFeatures.Hint.values()) {
            map.put(hint.name().toLowerCase(), hint);
        }

        for (final Object key : System.getProperties().keySet()) {
            if (key instanceof final String s && s.startsWith(PREFIX)) {
                final String lchint = s.substring(PREFIX.length()).toLowerCase();
                final String value = System.getProperty(s).toLowerCase();
                final boolean disable = value.isEmpty() || value.equals("true");

                final SimplifierFeatures.Hint hint = map.get(lchint);
                if (hint != null && disable) {
                    disabled.add(hint);
                }
            }
        }
    }

    private static class Holder {
        private static final SimplifierConfig INSTANCE = new SimplifierConfig();

    }

    /**
     * @param hint The hint.
     * @return {@code true} if the hint is disabled by external means.
     */
    public static boolean isDisabled(SimplifierFeatures.Hint hint) {
        return Holder.INSTANCE.disabled.contains(hint);
    }
}