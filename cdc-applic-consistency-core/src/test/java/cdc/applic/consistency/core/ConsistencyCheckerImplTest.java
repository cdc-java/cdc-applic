package cdc.applic.consistency.core;

import static cdc.applic.consistency.core.ConsistencySupport.r;

import org.junit.jupiter.api.Test;

import cdc.applic.consistency.Composition;
import cdc.applic.consistency.issues.GlobalIssueType;
import cdc.applic.consistency.issues.NodeIssueType;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.expressions.checks.ApplicIssueType;

class ConsistencyCheckerImplTest {
    @Test
    void testRegistries() {
        final ConsistencySupport support = new ConsistencySupport();
        final RegistryImpl registry2 = support.repository.registry().name("Registry2").build();
        support.data.createRootBlock("B1", "B1", support.registry, "true", Composition.ANY);
        support.data.createRootBlock("B2", "B2", registry2, "true", Composition.ANY);

        support.check(r(null, GlobalIssueType.REGISTRY_CONSISTENCY),
                      r(null, ApplicIssueType.DICTIONARY_ISSUE));
    }

    @Test
    void testNullDictionary() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", null, "Version<:{V1,V2,V3}", Composition.ANY);

        support.check(r("B1", NodeIssueType.NULL_DICTIONARY));
    }

    @Test
    void testDictionarytIssue() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy11, "true", Composition.ANY);

        support.check(r(null, ApplicIssueType.DICTIONARY_ISSUE));
    }

    @Test
    void testNullTarget() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy1, "true", Composition.ANY);
        support.data.createReference("B1", "R1.1", "R1.1", support.policy1, "true", null);

        support.check(r("R1.1", NodeIssueType.NULL_TARGET));
    }

    @Test
    void testTarget() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy1, "true", Composition.ANY);
        support.data.createReference("B1", "R1.1", "R1.1", support.policy1, "true", "B2");
        support.data.createRootBlock("B2", "B2", support.policy1, "Version=V1", Composition.ANY);

        support.check(r("R1.1", NodeIssueType.ACTUAL_REF_TARGET_COMPLIANCE));
    }

    // FIXME
    // @Test
    void testLocalApplicCompliance() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy2, "PartNumber=PN1", Composition.ANY);

        support.check(r("B1", ApplicIssueType.SEMANTIC_ISSUE));
    }

    @Test
    void testAnyEmpty() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy1, "Version<:{V1,V2,V3}", Composition.ANY);

        support.check();
    }

    @Test
    void testAtLeastOneEmpty() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy1, "Version<:{V1,V2,V3}", Composition.AT_LEAST_ONE);

        support.check(r("B1", NodeIssueType.AT_LEAST_ONE_COMPLIANCE));
    }

    @Test
    void testAtLeastOneA() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy1, "Version<:{V1,V2,V3}", Composition.AT_LEAST_ONE);
        support.data.createBlockDef("B1", "B1.1", "B1.1", support.policy1, "true", Composition.ANY);
        support.data.createBlockDef("B1", "B1.2", "B1.2", support.policy1, "true", Composition.ANY);

        support.check();
    }

    @Test
    void testAtLeastOneB() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy1, "Version<:{V1,V2,V3}", Composition.AT_LEAST_ONE);
        support.data.createBlockDef("B1", "B1.1", "B1.1", support.policy1, "Version=V1", Composition.ANY);
        support.data.createBlockDef("B1", "B1.2", "B1.2", support.policy1, "Version=V2", Composition.ANY);

        support.check(r("B1", NodeIssueType.AT_LEAST_ONE_COMPLIANCE));
    }

    @Test
    void testExactlyOneEmpty() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy1, "Version<:{V1,V2,V3}", Composition.EXACTLY_ONE);

        support.check(r("B1", NodeIssueType.EXACTLY_ONE_COMPLIANCE));
    }

    @Test
    void testExactlyOneA() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy1, "Version<:{V1,V2,V3}", Composition.EXACTLY_ONE);
        support.data.createBlockDef("B1", "B1.1", "B1.1", support.policy1, "true", Composition.ANY);
        support.data.createBlockDef("B1", "B1.2", "B1.2", support.policy1, "true", Composition.ANY);

        support.check(r("B1", NodeIssueType.EXACTLY_ONE_COMPLIANCE));
    }

    @Test
    void testExactlyOneB() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy1, "Version<:{V1,V2,V3}", Composition.EXACTLY_ONE);
        support.data.createBlockDef("B1", "B1.1", "B1.1", support.policy1, "Version<:{V1,V2}", Composition.ANY);
        support.data.createBlockDef("B1", "B1.2", "B1.2", support.policy1, "Version=V3", Composition.ANY);

        support.check();
    }

    @Test
    void testAtMostOneEmpty() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy1, "Version<:{V1,V2,V3}", Composition.AT_MOST_ONE);

        support.check();
    }

    @Test
    void testAtMostOneA() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy1, "Version<:{V1,V2,V3}", Composition.AT_MOST_ONE);
        support.data.createBlockDef("B1", "B1.1", "B1.1", support.policy1, "true", Composition.ANY);
        support.data.createBlockDef("B1", "B1.2", "B1.2", support.policy1, "true", Composition.ANY);

        support.check(r("B1", NodeIssueType.AT_MOST_ONE_COMPLIANCE));
    }

    @Test
    void testAtMostOneB() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy1, "Version<:{V1,V2,V3}", Composition.AT_MOST_ONE);
        support.data.createBlockDef("B1", "B1.1", "B1.1", support.policy1, "Version<:{V1,V2}", Composition.ANY);
        support.data.createBlockDef("B1", "B1.2", "B1.2", support.policy1, "Version=V3", Composition.ANY);

        support.check();
    }

    @Test
    void testAtMostOneC() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy1, "Version<:{V1,V2,V3}", Composition.AT_MOST_ONE);
        support.data.createBlockDef("B1", "B1.1", "B1.1", support.policy1, "Version<:{V1,V2}", Composition.ANY);
        support.data.createBlockDef("B1", "B1.2", "B1.2", support.policy1, "Version<:{V2,V3}", Composition.ANY);

        support.check(r("B1", NodeIssueType.AT_MOST_ONE_COMPLIANCE));
    }

    @Test
    void testNonEmptyIntersectionA() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy1, "Version<:{V1,V2,V3}", Composition.ANY);
        support.data.createBlockDef("B1", "B1.1", "B1.1", support.policy1, "Version=V4", Composition.ANY);

        support.check(r("B1.1", NodeIssueType.ACTUAL_CHILD_PARENT_COMPLIANCE));
    }

    @Test
    void testNonEmptyIntersectionB() {
        final ConsistencySupport support = new ConsistencySupport();
        support.data.createRootBlock("B1", "B1", support.policy1, "Version<:{V1,V2,V3}", Composition.ANY);
        support.data.createBlockDef("B1", "B1.1", "B1.1", support.policy1, "Version<:{V1,V2,V3,V4}", Composition.ANY);
        support.data.createReference("B1", "R1.1", "R1.1", support.policy1, "Version=V4", "B1.1");

        support.check(r("R1.1", NodeIssueType.ACTUAL_CHILD_PARENT_COMPLIANCE));
    }
}