package cdc.applic.consistency.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.applic.consistency.impl.Block;
import cdc.applic.consistency.impl.ConsistencyDataImpl;
import cdc.applic.consistency.impl.Node;
import cdc.applic.consistency.impl.Reference;
import cdc.applic.consistency.issues.ConsistencyDataLocation;
import cdc.applic.dictionaries.DItemUsage;
import cdc.applic.dictionaries.handles.DictionaryHandle;
import cdc.applic.dictionaries.impl.PolicyImpl;
import cdc.applic.dictionaries.impl.RegistryImpl;
import cdc.applic.dictionaries.impl.RepositoryImpl;
import cdc.applic.expressions.literals.Name;
import cdc.issues.Issue;

class ConsistencySupport {
    private static final Logger LOGGER = LogManager.getLogger(ConsistencySupport.class);
    final RepositoryImpl repository = new RepositoryImpl();
    final RegistryImpl registry = repository.registry().name("Registry").build();
    final DictionaryHandle registryHandle = new DictionaryHandle(registry);
    final PolicyImpl policy1 = registry.policy().name("Policy1").build();
    final PolicyImpl policy11 = policy1.policy().name("Policy1.1").build();
    final PolicyImpl policy12 = policy1.policy().name("Policy1.2").build();
    final PolicyImpl policy2 = registry.policy().name("Policy2").build();

    final ConsistencyDataImpl data = new ConsistencyDataImpl("TestData", registry);
    final ConsistencyCheckerImpl<Node, String, Block, Reference> checker = new ConsistencyCheckerImpl<>();

    ConsistencySupport() {
        registry.integerType().name("Rank").frozen(false).domain("1~999").build();
        registry.booleanType().name("Boolean").build();
        registry.enumeratedType().name("Version").frozen(false).literals("V1", "V2", "V3", "V4").build();
        registry.enumeratedType().name("Standard").frozen(false).literals("S1", "S2", "S3").build();
        registry.patternType().name("PartNumber").frozen(false).pattern("PN[1-9][0-9]*").build();

        registry.property().name("Rank").type("Rank").ordinal(0).build();
        registry.property().name("Version").type("Version").ordinal(1).build();
        registry.property().name("Standard").type("Standard").ordinal(2).build();
        registry.property().name("SB1").type("Boolean").ordinal(100).build();
        registry.property().name("SB2").type("Boolean").ordinal(101).build();
        registry.property().name("SB3").type("Boolean").ordinal(102).build();
        registry.property().name("SB4").type("Boolean").ordinal(103).build();
        registry.property().name("PartNumber").type("PartNumber").ordinal(5).build();

        policy1.setItemUsage(Name.of("Rank"), DItemUsage.OPTIONAL);
        policy1.setItemUsage(Name.of("Version"), DItemUsage.OPTIONAL);
        policy1.setItemUsage(Name.of("Standard"), DItemUsage.OPTIONAL);
        policy1.setTypeUsage("Boolean", DItemUsage.OPTIONAL);

        policy2.setItemUsage(Name.of("Rank"), DItemUsage.OPTIONAL);
        policy2.setItemUsage(Name.of("Version"), DItemUsage.OPTIONAL);
        policy2.setItemUsage(Name.of("Standard"), DItemUsage.OPTIONAL);
        policy2.setTypeUsage("Boolean", DItemUsage.OPTIONAL);
    }

    static class Record {
        protected final String value0;
        protected final String value1;

        public Record(String value0,
                      Object value1) {
            this.value0 = value0;
            this.value1 = value1.toString();
        }

        @Override
        public boolean equals(Object other) {
            if (this == other) {
                return true;
            }
            if (!(other instanceof Record)) {
                return false;
            }
            final Record o = (Record) other;
            return Objects.equals(value0, o.value0)
                    && Objects.equals(value1, o.value1);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value0,
                                value1);
        }

        @Override
        public String toString() {
            return "[" + Objects.toString(value0) + ", "
                    + Objects.toString(value1) + "]";
        }
    }

    static Record r(String s,
                    Object r) {
        return new Record(s, r);
    }

    void check(Record... expecteds) {
        LOGGER.debug("================================");
        final Set<Record> expected = new HashSet<>();
        for (final Record p : expecteds) {
            LOGGER.debug("   {}", p);
            expected.add(p);
        }
        final List<Issue> issues = checker.check(data);
        final Set<Record> actual = new HashSet<>();
        for (final Issue issue : issues) {
            if (issue.getLocationAt(0) instanceof ConsistencyDataLocation) {
                final String nodeId = ((ConsistencyDataLocation) issue.getLocationAt(0)).getNodeId();
                actual.add(r(nodeId, issue.getName()));
            } else {
                actual.add(r(null, issue.getName()));
            }
            LOGGER.debug("   {}", issue);
        }
        assertEquals(expected, actual);
    }
}