package cdc.applic.factorization;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Objects;
import java.util.Set;

import cdc.applic.expressions.Formatting;
import cdc.util.lang.Checks;

public class FactorizationFeatures {
    private final Set<Hint> hints;
    private final Formatting formatting;

    public static final FactorizationFeatures SIMPLIFY_CHECK_SHORT_NARROW = FactorizationFeatures.builder()
                                                                                                 .hint(Hint.SIMPLIFY)
                                                                                                 .hint(Hint.CHECK)
                                                                                                 .formatting(Formatting.SHORT_NARROW)
                                                                                                 .build();
    public static final FactorizationFeatures SIMPLIFY_NO_CHECK_SHORT_NARROW = FactorizationFeatures.builder()
                                                                                                    .hint(Hint.SIMPLIFY)
                                                                                                    .formatting(Formatting.SHORT_NARROW)
                                                                                                    .build();

    public enum Hint {
        /**
         * If enabled, expressions are simplified.
         */
        SIMPLIFY,

        /**
         * If enabled and meaningful, input data are checked.
         * <p>
         * <b>WARNING:</b> that should be enabled by default, otherwise
         * results may be wrong without notifications.
         */
        CHECK

        // /**
        // * If enabled, events that are can be ignored are not generated.
        // * <p>
        // * This is typically the case when an event indicates that an object is unchanged.
        // */
        // NO_IGNORABLE
    }

    FactorizationFeatures(Set<Hint> hints,
                          Formatting formatting) {
        this.hints = Collections.unmodifiableSet(hints);
        this.formatting = formatting;
    }

    public Set<Hint> getHints() {
        return hints;
    }

    /**
     * Returns {@code true} if a {@link Hint} is enabled.
     *
     * @param hint The hint.
     * @return {@code true} if {@code hint} is enabled.
     */
    public boolean isEnabled(Hint hint) {
        return hints.contains(hint);
    }

    public Formatting getFormatting() {
        return formatting;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hints,
                            formatting);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof FactorizationFeatures)) {
            return false;
        }
        final FactorizationFeatures other = (FactorizationFeatures) object;
        return hints.equals(other.hints)
                && formatting == other.formatting;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return getHints()
                + " " + getFormatting();
    }

    public static class Builder {
        private final Set<Hint> hints = EnumSet.noneOf(Hint.class);
        private Formatting formatting = Formatting.SHORT_NARROW;

        public Builder hint(Hint hint) {
            Checks.isNotNull(hint, "hint");

            this.hints.add(hint);
            return this;
        }

        public Builder hints(Hint... hints) {
            for (final Hint hint : hints) {
                hint(hint);
            }
            return this;
        }

        public Builder formatting(Formatting formatting) {
            this.formatting = formatting;
            return this;
        }

        public FactorizationFeatures build() {
            return new FactorizationFeatures(hints,
                                             formatting);
        }
    }
}