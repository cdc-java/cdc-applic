package cdc.applic.factorization.events;

import java.util.Objects;

import cdc.applic.expressions.Expression;

public final class MergeEvent<T> {
    private final Type type;
    private final T object;
    private final Expression applicability;
    private final T replacement;

    public enum Type {
        CHANGE_OBJECT_APPLICABILITY,

        REMOVE_OBJECT
    }

    private MergeEvent(Type type,
                       T object,
                       Expression expression,
                       T replacement) {
        this.type = type;
        this.object = object;
        this.applicability = expression;
        this.replacement = replacement;
    }

    public static <T> MergeEvent<T> newChangeObjectApplicability(T object,
                                                                 Expression applicability) {
        return new MergeEvent<>(Type.CHANGE_OBJECT_APPLICABILITY, object, applicability, null);
    }

    public static <T> MergeEvent<T> newRemoveObject(T object,
                                                    T replacement) {
        return new MergeEvent<>(Type.REMOVE_OBJECT, object, null, replacement);
    }

    public Type getType() {
        return type;
    }

    public T getObject() {
        return object;
    }

    public Expression getApplicability() {
        return applicability;
    }

    public T getReplacement() {
        return replacement;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type,
                            object,
                            applicability,
                            replacement);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof MergeEvent)) {
            return false;
        }
        final MergeEvent<?> other = (MergeEvent<?>) object;
        return this.type == other.type
                && Objects.equals(this.object, other.object)
                && Objects.equals(this.applicability, other.applicability)
                && Objects.equals(this.replacement, other.replacement);
    }

    @Override
    public String toString() {
        return getType().name();
    }
}