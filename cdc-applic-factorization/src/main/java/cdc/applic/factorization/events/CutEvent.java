package cdc.applic.factorization.events;

import java.util.Objects;

import cdc.applic.expressions.Expression;

public final class CutEvent<T> {
    private final Type type;
    private final T object;
    private final Expression excluded;
    private final Expression included;

    public enum Type {
        EXCLUDE_OBJECT_APPLICABILITY,
        INCLUDE_OBJECT_APPLICABILITY,
        CUT_OBJECT_APPLICABILITY,
        DEGENERATE_OBJECT_APPLICABILITY
    }

    private CutEvent(Type type,
                     T object,
                     Expression excluded,
                     Expression included) {
        this.type = type;
        this.object = object;
        this.excluded = excluded;
        this.included = included;
    }

    public static <T> CutEvent<T> newExcludeObjectApplicability(T object,
                                                                Expression excluded) {
        return new CutEvent<>(Type.EXCLUDE_OBJECT_APPLICABILITY, object, excluded, Expression.FALSE);
    }

    public static <T> CutEvent<T> newIncludeObjectApplicability(T object,
                                                                Expression included) {
        return new CutEvent<>(Type.INCLUDE_OBJECT_APPLICABILITY, object, Expression.FALSE, included);
    }

    public static <T> CutEvent<T> newCutObjectApplicability(T object,
                                                            Expression excluded,
                                                            Expression included) {
        return new CutEvent<>(Type.CUT_OBJECT_APPLICABILITY, object, excluded, included);
    }

    public static <T> CutEvent<T> newDegeneratedObjectApplicability(T object) {
        return new CutEvent<>(Type.DEGENERATE_OBJECT_APPLICABILITY, object, Expression.FALSE, Expression.FALSE);
    }

    public Type getType() {
        return type;
    }

    public T getObject() {
        return object;
    }

    public Expression getExcludedApplicability() {
        return excluded;
    }

    public Expression getIncludedApplicability() {
        return included;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type,
                            object,
                            excluded,
                            included);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof CutEvent)) {
            return false;
        }
        final CutEvent<?> other = (CutEvent<?>) object;
        return this.type == other.type
                && Objects.equals(this.object, other.object)
                && Objects.equals(this.excluded, other.excluded)
                && Objects.equals(this.included, other.included);
    }

    @Override
    public String toString() {
        return getType().name() + " " + object + " " + excluded + " " + included;
    }
}