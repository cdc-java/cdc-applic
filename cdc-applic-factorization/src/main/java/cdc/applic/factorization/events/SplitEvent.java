package cdc.applic.factorization.events;

import java.util.Objects;

import cdc.applic.expressions.Expression;

public final class SplitEvent<T> {
    private final Type type;
    private final T object;
    private final Expression applicability;
    private final T replacement;

    public enum Type {
        /**
         * The object is kept unchanged.
         */
        KEEP_OBJECT,

        /**
         * The object is kept and its applicability is reduced.
         */
        REDUCE_OBJECT_APPLICABILITY,

        /**
         * An object is created.
         */
        CREATE_OBJECT,

        /**
         * The object is reused.
         */
        REUSE_OBJECT,

        /**
         * The object is removed.
         */
        REMOVE_OBJECT
    }

    private SplitEvent(Type type,
                       T object,
                       Expression expression,
                       T replacement) {
        this.type = type;
        this.object = object;
        this.applicability = expression;
        this.replacement = replacement;
    }

    public static <T> SplitEvent<T> newKeepObject(T object) {
        return new SplitEvent<>(Type.KEEP_OBJECT, object, null, null);
    }

    public static <T> SplitEvent<T> newReduceObjectApplicability(T object,
                                                                 Expression applicability) {
        return new SplitEvent<>(Type.REDUCE_OBJECT_APPLICABILITY, object, applicability, null);
    }

    public static <T> SplitEvent<T> newReuseObject(T object,
                                                   Expression applicability) {
        return new SplitEvent<>(Type.REUSE_OBJECT, object, applicability, null);
    }

    public static <T> SplitEvent<T> newCreateObject(Expression applicability) {
        return new SplitEvent<>(Type.CREATE_OBJECT, null, applicability, null);
    }

    public static <T> SplitEvent<T> newRemoveObject(T object,
                                                    T replacement) {
        return new SplitEvent<>(Type.REMOVE_OBJECT, object, null, replacement);
    }

    /**
     * @return The event type.
     */
    public Type getType() {
        return type;
    }

    /**
     * Returns the associated object.
     * <p>
     * Valid when type is {@link Type#REUSE_OBJECT},
     * {@link Type#REDUCE_OBJECT_APPLICABILITY} and {@link Type#REMOVE_OBJECT}.
     *
     * @return The object.
     */
    public T getObject() {
        return object;
    }

    /**
     * Returns the applicability to use.
     * <p>
     * Valid when type is {@link Type#REUSE_OBJECT},
     * {@link Type#REDUCE_OBJECT_APPLICABILITY} and {@link Type#CREATE_OBJECT}.
     *
     * @return The applicability.
     */
    public Expression getApplicability() {
        return applicability;
    }

    /**
     * Returns the replacement object.
     * <p>
     * Valid when type is {@link Type#REMOVE_OBJECT}.
     *
     * @return the replacement object.
     */
    public T getReplacement() {
        return replacement;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type,
                            object,
                            applicability,
                            replacement);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof SplitEvent)) {
            return false;
        }
        final SplitEvent<?> other = (SplitEvent<?>) object;
        return this.type == other.type
                && Objects.equals(this.object, other.object)
                && Objects.equals(this.applicability, other.applicability)
                && Objects.equals(this.replacement, other.replacement);
    }

    @Override
    public String toString() {
        return getType().name() + " " + object + " " + applicability + " " + replacement;
    }
}