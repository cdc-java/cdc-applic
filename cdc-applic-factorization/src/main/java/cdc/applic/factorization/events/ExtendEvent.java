package cdc.applic.factorization.events;

import java.util.Objects;

import cdc.applic.expressions.Expression;

public final class ExtendEvent<T> {
    private final Type type;
    private final T object;
    private final Expression applicability;

    public enum Type {
        CHANGE_OBJECT_APPLICABILITY,
        IGNORE_OBJECT
    }

    private ExtendEvent(Type type,
                        T object,
                        Expression expression) {
        this.type = type;
        this.object = object;
        this.applicability = expression;
    }

    public static <T> ExtendEvent<T> newChangeObjectApplicability(T object,
                                                                  Expression applicability) {
        return new ExtendEvent<>(Type.CHANGE_OBJECT_APPLICABILITY, object, applicability);
    }

    public static <T> ExtendEvent<T> newIgnoreObject(T object) {
        return new ExtendEvent<>(Type.IGNORE_OBJECT, object, null);
    }

    public Type getType() {
        return type;
    }

    public T getObject() {
        return object;
    }

    public Expression getApplicability() {
        return applicability;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type,
                            object,
                            applicability);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof ExtendEvent)) {
            return false;
        }
        final ExtendEvent<?> other = (ExtendEvent<?>) object;
        return this.type == other.type
                && Objects.equals(this.object, other.object)
                && Objects.equals(this.applicability, other.applicability);
    }

    @Override
    public String toString() {
        return getType().name() + " " + object + " " + applicability;
    }
}