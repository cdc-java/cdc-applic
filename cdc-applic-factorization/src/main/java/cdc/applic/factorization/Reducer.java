package cdc.applic.factorization;

import java.util.List;

import cdc.applic.expressions.Expression;
import cdc.applic.factorization.events.ReduceEvent;
import cdc.applic.factorization.handlers.ReduceHandler;

public interface Reducer {
    public <T> void reduce(List<? extends T> objects,
                           Expression targetApplicability,
                           ApplicabilityExtractor<? super T> extractor,
                           FactorizationFeatures features,
                           ReduceHandler<T> handler);

    public <T> List<ReduceEvent<T>> reduce(List<? extends T> objects,
                                           Expression targetApplicability,
                                           ApplicabilityExtractor<? super T> extractor,
                                           FactorizationFeatures features);
}