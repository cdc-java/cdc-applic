package cdc.applic.factorization;

import java.util.List;

import cdc.applic.factorization.events.MergeEvent;
import cdc.applic.factorization.handlers.MergeHandler;

public interface Merger {
    public <T> void merge(List<? extends T> objects,
                          DefinitionAnalyzer<T> analyzer,
                          ApplicabilityExtractor<? super T> extractor,
                          FactorizationFeatures features,
                          MergeHandler<T> handler);

    public <T> List<MergeEvent<T>> merge(List<? extends T> objects,
                                         DefinitionAnalyzer<T> analyzer,
                                         ApplicabilityExtractor<? super T> extractor,
                                         FactorizationFeatures features);
}