package cdc.applic.factorization;

import java.util.List;
import java.util.Set;

import cdc.applic.expressions.Expression;
import cdc.util.debug.Printable;

/**
 * Given a set of input expressions Ei, a Partioner will return a set of
 * output expressions Eo, such that:
 * <ul>
 * <li>Eo are not empty.
 * <li>Eo don't overlap.
 * <li>Union of Eo equals union of Ei
 * <li>Each Eo is a subset of at least one Ei.
 * </ul>
 * <b>Warning:</b> solving this problem may yield 2<sup>N</sup> output
 * expressions, where N is the number of input expressions.
 *
 * @author Damien Carbonne
 */
public interface Partitioner {
    public static interface Result extends Printable {
        /**
         * @return The input expressions.
         */
        public List<Expression> getInputExpressions();

        /**
         * @return The set of output expressions.
         */
        public Set<Expression> getOutputExpressions();

        /**
         *
         * @param output The output expression.
         * @return The indices of input expressions.
         */
        public Set<Integer> getInputIndices(Expression output);
    }

    /**
     * Partition a list of input expressions.
     *
     * @param inputs The input expressions.
     * @param features The features.
     * @return The partitioning of {@code inputs}.
     */
    public Result partition(List<Expression> inputs,
                            FactorizationFeatures features);

    /**
     * Partition a list of input expressions.
     * <p>
     * Some additional knowledge is passed to improve performances.
     * <p>
     * <b>WARNING:</b> If promise is invalid, result will be erroneous.
     *
     * @param inputs The input expressions.
     * @param inputPartitions A list of sets of indices of input
     *            expressions with the promise that input expressions
     *            designated in each set don't overlap.<br>
     *            <b>WARNING:</b> all integers should be
     *            in range [0, inputs.size()[
     * @param features The features.
     * @return The partitioning of {@code inputs}.
     */
    public Result partition(List<Expression> inputs,
                            List<Set<Integer>> inputPartitions,
                            FactorizationFeatures features);
}