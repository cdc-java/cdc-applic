package cdc.applic.factorization;

import java.util.List;

import cdc.applic.expressions.Expression;
import cdc.applic.factorization.events.ExtendEvent;
import cdc.applic.factorization.handlers.ExtendHandler;

/**
 * An Extender is used to extend the applicability of objects.
 *
 * @author Damien Carbonne
 */
public interface Extender {
    /**
     *
     * @param <T> The object type.
     * @param objects The objects.
     * @param extensionApplicability The extension applicability.
     * @param extractor The applicability extractor.
     * @param features The features to apply.
     * @param handler The event handler.
     */
    public <T> void extend(List<? extends T> objects,
                           Expression extensionApplicability,
                           ApplicabilityExtractor<? super T> extractor,
                           FactorizationFeatures features,
                           ExtendHandler<T> handler);

    public <T> List<ExtendEvent<T>> extend(List<? extends T> objects,
                                           Expression extensionApplicability,
                                           ApplicabilityExtractor<? super T> extractor,
                                           FactorizationFeatures features);
}