package cdc.applic.factorization.handlers;

/**
 * Base interface of handlers.
 *
 * @author Damien Carbonne
 */
public interface Handler {
    /**
     * Invoked once at process beginning.
     */
    public default void processBegin() {
        // Ignore
    }

    /**
     * Invoked once at process end.
     */
    public default void processEnd() {
        // Ignore
    }
}