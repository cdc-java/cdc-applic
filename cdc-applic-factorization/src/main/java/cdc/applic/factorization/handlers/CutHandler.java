package cdc.applic.factorization.handlers;

import cdc.applic.expressions.Expression;

/**
 * Handler of cut events.
 *
 * @author Damien Carbonne
 *
 * @param <T> The object type.
 */
public interface CutHandler<T> extends Handler {
    public void processExcludeObjectApplicability(T object);

    public void processIncludeObjectApplicability(T object);

    public void processCutObjectApplicability(T object,
                                              Expression excludedApplicability,
                                              Expression includedApplicability);

    public void processDegenerateObjectApplicability(T object);
}