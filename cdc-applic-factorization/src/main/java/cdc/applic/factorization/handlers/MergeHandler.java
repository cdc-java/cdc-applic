package cdc.applic.factorization.handlers;

import cdc.applic.expressions.Expression;

public interface MergeHandler<T> extends Handler {
    public void processChangeObjectApplicability(T object,
                                                 Expression applicability);

    public void processRemoveObject(T object,
                                    T replacement);
}