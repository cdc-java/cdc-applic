package cdc.applic.factorization.handlers;

import cdc.applic.expressions.Expression;

public interface ExtendHandler<T> extends Handler {
    public void processChangeObjectApplicability(T object,
                                                 Expression applicability);
}