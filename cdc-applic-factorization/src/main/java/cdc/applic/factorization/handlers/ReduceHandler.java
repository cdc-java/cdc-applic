package cdc.applic.factorization.handlers;

import cdc.applic.expressions.Expression;

public interface ReduceHandler<T> extends Handler {
    public void processChangeObjectApplicability(T object,
                                                 Expression applicability);

    public void processRemoveObject(T object);
}