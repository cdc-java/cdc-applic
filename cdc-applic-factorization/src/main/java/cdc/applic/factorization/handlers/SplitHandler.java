package cdc.applic.factorization.handlers;

import cdc.applic.expressions.Expression;

public interface SplitHandler<T> extends Handler {
    public void processKeepObject(T object);

    public void processReduceObjectApplicability(T object,
                                                 Expression applicability);

    public void processRemoveObject(T object,
                                    T replacement);

    public void processReuseObject(T object,
                                   Expression applicability);

    public void processCreateObject(Expression applicability);
}