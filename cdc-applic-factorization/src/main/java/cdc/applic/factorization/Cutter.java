package cdc.applic.factorization;

import java.util.List;

import cdc.applic.expressions.Expression;
import cdc.applic.factorization.events.CutEvent;
import cdc.applic.factorization.handlers.CutHandler;

/**
 * Cutter is used to cut objects that have an applicability.
 * <p>
 * Given a target applicability, each object can be:
 * <ul>
 * <li><b>included</b> when its applicability is a non empty subset of target applicability
 * <li><b>excluded</b> when its applicability is a non empty subset of complement of target applicability
 * <li><b>cut</b> when its applicability intersects both target applicability and its complement.
 * <li><b>degenerated</b> when its applicability intersects neither target applicability nor its complement.
 * </ul>
 *
 * @author Damien Carbonne
 */
public interface Cutter {
    /**
     * Cut the applicability of a list of objects in 2 parts.
     * <p>
     * For each object in {@code objects}, when applicability of object:
     * <ul>
     * <li>is a non-empty subset of {@code targetApplicability}, invokes {@link CutHandler#processIncludeObjectApplicability(Object)}.
     * <li>is a non-empty subset of complement of {@code targetApplicability}, invokes {@link CutHandler#processExcludeObjectApplicability(Object)}.
     * <li>straddles {@code targetApplicability} and its complement, invokes {@link CutHandler#processCutObjectApplicability(Object, Expression, Expression)}.
     * <li>intersects neither {@code targetApplicability} nor its complement, invokes {@link CutHandler#processDegenerateObjectApplicability(Object)}.
     * </ul>
     *
     * @param <T> The object type.
     * @param objects The objects.
     * @param targetApplicability The target applicability.
     * @param applicabilityExtractor The applicability extractor.
     * @param features The features to apply.
     * @param handler The event handler.
     */
    public <T> void cut(List<? extends T> objects,
                        Expression targetApplicability,
                        ApplicabilityExtractor<? super T> applicabilityExtractor,
                        FactorizationFeatures features,
                        CutHandler<T> handler);

    public <T> List<CutEvent<T>> cut(List<? extends T> objects,
                                     Expression targetApplicability,
                                     ApplicabilityExtractor<? super T> applicabilityExtractor,
                                     FactorizationFeatures features);
}