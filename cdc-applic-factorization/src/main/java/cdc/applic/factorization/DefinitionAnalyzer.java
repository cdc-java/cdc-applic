package cdc.applic.factorization;

public interface DefinitionAnalyzer<T> {
    /**
     * Returns a hash of the definition of an object.
     * 
     * @param object The object.
     * @return A hash of the definition of {@code object}.
     */
    public int getDefinitionHash(T object);

    /**
     * Returns {@code true} if 2 objects have the same definition.
     *
     * @param object1 The first object.
     * @param object2 The second object.
     * @return {@code true} if {@code object1} and {@code object2} have the same definition.
     */
    public boolean haveSameDefinition(T object1,
                                      T object2);
}