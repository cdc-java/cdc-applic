package cdc.applic.factorization;

import java.util.List;

import cdc.applic.expressions.Expression;
import cdc.applic.factorization.events.SplitEvent;
import cdc.applic.factorization.handlers.SplitHandler;

/**
 * Given a set of objects that have disjoint applicabilities and a target applicability,
 * this is used to produce a new set where exactly one object has a target applicability
 * and all other have an applicability that does not intersect with target applicability.
 *
 * @author Damien Carbonne
 */
public interface Splitter {
    /**
     * Split a set of objects that have disjoint applicabilities.
     * <p>
     * This can:
     * <ul>
     * <li>Change (reduce) that applicability of an object
     * <li>Remove an object
     * <li>Create an object, which can happen at most once.
     * <li>Reuse an object (that is the merge of remove/create and can happen at most once).
     * </ul>
     *
     * @param <T> The object type.
     * @param objects The objects.
     * @param targetApplicability The target applicability.
     * @param applicabilityExtractor The applicability extractor.
     * @param features The features to apply.
     * @param handler The event handler.
     */
    public <T> void split(List<? extends T> objects,
                          Expression targetApplicability,
                          ApplicabilityExtractor<? super T> applicabilityExtractor,
                          FactorizationFeatures features,
                          SplitHandler<T> handler);

    public <T> List<SplitEvent<T>> split(List<? extends T> objects,
                                         Expression targetApplicability,
                                         ApplicabilityExtractor<? super T> applicabilityExtractor,
                                         FactorizationFeatures features);
}