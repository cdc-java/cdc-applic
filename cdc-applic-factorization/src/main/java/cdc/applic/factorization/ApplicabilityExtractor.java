package cdc.applic.factorization;

import cdc.applic.expressions.Expression;

@FunctionalInterface
public interface ApplicabilityExtractor<T> {
    public Expression getApplicability(T object);
}