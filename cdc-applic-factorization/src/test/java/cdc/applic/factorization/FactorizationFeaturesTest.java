package cdc.applic.factorization;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.applic.expressions.Formatting;
import cdc.applic.factorization.FactorizationFeatures.Hint;

class FactorizationFeaturesTest {
    @Test
    void test() {
        final FactorizationFeatures features = FactorizationFeatures.builder()
                                                                    .hint(Hint.SIMPLIFY)
                                                                    .hint(Hint.CHECK)
                                                                    .formatting(Formatting.MATH_WIDE)
                                                                    .build();

        assertEquals(FactorizationFeatures.Hint.values().length, features.getHints().size());
        assertEquals(Formatting.MATH_WIDE, features.getFormatting());
        assertTrue(features.isEnabled(FactorizationFeatures.Hint.SIMPLIFY));

        assertEquals(features, features);
        assertNotEquals(features, null);
        assertNotEquals(features, "Hello");
        assertNotEquals(features, FactorizationFeatures.SIMPLIFY_CHECK_SHORT_NARROW);

        final FactorizationFeatures features1A = FactorizationFeatures.builder()
                                                                      .formatting(Formatting.MATH_WIDE)
                                                                      .build();
        final FactorizationFeatures features1B = FactorizationFeatures.builder()
                                                                      .formatting(Formatting.MATH_WIDE)
                                                                      .build();
        final FactorizationFeatures features2 = FactorizationFeatures.builder()
                                                                     .formatting(Formatting.LONG_WIDE)
                                                                     .build();
        final FactorizationFeatures features3 = FactorizationFeatures.builder()
                                                                     .formatting(Formatting.MATH_NARROW)
                                                                     .build();
        final FactorizationFeatures features4 = FactorizationFeatures.builder()
                                                                     .hint(Hint.SIMPLIFY)
                                                                     .formatting(Formatting.MATH_WIDE)
                                                                     .build();
        assertNotEquals(features, features1A);
        assertEquals(features1A, features1B);
        assertNotEquals(features1A, features2);
        assertNotEquals(features1A, features3);
        assertNotEquals(features1A, features4);

        assertEquals(features1A.hashCode(), features1B.hashCode());
        assertEquals(features1A.toString(), features1B.toString());
    }
}